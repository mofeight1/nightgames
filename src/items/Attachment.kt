package items

import characters.Character

enum class Attachment : Item {
    Excalibur2("Grand Sexcalibur", 1500, "An improved version of the Excalibur with better components", ""),
    Excalibur3("Masterwork Sexcalibur", 5000, "", ""),
    Excalibur4("Legendary Sexcalibur", 10000, "", ""),
    Excalibur5(
        "Perfect Sexcalibur",
        25000,
        "This sex toy is a masterpiece, beyond the possibility of further upgrading",
        ""
    ),
    ExcaliburArcane(
        "Arcane Runes",
        3000,
        "The enchanted runes inscribed on the handle can steal mana from the target",
        ""
    ),
    ExcaliburKi("Restoring Grip", 3000, "Grasping the handle of this toy recovers a small amount of stamina", "a "),
    ExcaliburDark("Lust Curse", 3000, "The cursed toy is more effective on Horny targets", "a "),
    ExcaliburAnimism("Furry Head", 3000, "The vibrating head is covered with very soft fur", "a "),
    ExcaliburFetish("Tentacles", 3000, "The tentacles embedded in the toy react to the user's arousal", ""),
    ExcaliburScience(
        "Dissolver",
        3000,
        "A small sprayer uses Dissolving Solution to penetrate protective clothing",
        "a "
    ),
    ExcaliburNinjutsu("Concealed Holster", 3000, "The device is holstered in a hidden pocket.", "a "),

    DildoLube("Dildo Luber", 2000, "The tips sprays an oily lubricant when pressure is applied.", "a "),
    DildoSlimy("Slime Dildo", 4000, "A living sex toy made of the material from the slime girl", "a "),
    OnaholeVibe(
        "Onahole Vibrator",
        2000,
        "A small vibrator is embedded in the onahole to give the target more pleasure",
        "an "
    ),
    OnaholeSlimy("Slime Onahole", 4000, "A living vagina from a slime girl", "a "),
    CropKeen("Keen Crop", 2000, "The Treasure Hunter has been upgraded for better accuracy.", "a "),
    CropShocker(
        "Shock Crop",
        4000,
        "This upgraded crop delivers a painful, but harmless electric shock on contact.",
        "a "
    ),
    TicklerPheromones("Tickler Powder", 2000, "The pheromone powder in the tickler can cause arousal over time", ""),
    TicklerFluffy(
        "Fluffy Tickler",
        4000,
        "A Tickler made from the fluffy tail of a mythical fox tail. It's supernaturally soft",
        "a "
    ),
    ;
    override val price: Int
    /**
     * The Item's display name.
     */
    override val desc: String
    override val prefix: String
    private val iName: String
    constructor(name: String, price: Int, desc: String, prefix: String
    ) {
        iName = name
        this.price = price
        this.desc = desc
        this.prefix = prefix

    }

    override fun getName(): String {
        return iName
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    override val listed = false

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override val recipe: ArrayList<Item>
        get() {
        val recipe = ArrayList<Item>()
        when (this) {
            Excalibur2, OnaholeVibe -> recipe.add(Component.HGMotor)
            Excalibur3 -> recipe.add(Component.Titanium)
            Excalibur4 -> recipe.add(Component.DragonBone)
            Excalibur5 -> {
                recipe.add(Component.PBlueprint)
                recipe.add(Component.PVibrator)
                recipe.add(Component.PHandle)
            }

            ExcaliburArcane -> recipe.add(Consumable.FaeScroll)
            ExcaliburKi -> recipe.add(Consumable.powerband)
            ExcaliburDark -> recipe.add(Consumable.Talisman)
            ExcaliburFetish -> recipe.add(Component.Totem)
            ExcaliburScience, DildoLube -> recipe.add(Component.MSprayer)
            DildoSlimy, OnaholeSlimy -> recipe.add(Component.SlimeCore)
            CropKeen, ExcaliburNinjutsu -> recipe.add(Component.Skin)
            CropShocker -> recipe.add(Component.Capacitor)
            TicklerPheromones -> recipe.add(Flask.PAphrodisiac)
            TicklerFluffy, ExcaliburAnimism -> recipe.add(Component.Foxtail)
        }
        return recipe
    }
}

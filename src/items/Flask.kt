package items

import characters.Character
import combat.Combat
import status.Dissolving
import status.Drowsy
import status.Horny
import status.Hypersensitive
import status.Oiled
import status.Status
import status.Stsflag

enum class Flask : Item {
    Lubricant(
        "Lubricant",
        20,
        "Helps you pleasure your opponent, but makes her hard to hang on to",
        "some ",
        "oily",
        Oiled(null),
        "The liquid clings to you and makes your whole body slippery.",
        "She's covered in slick lubricant."
    ),
    Aphrodisiac(
        "Aphrodisiac",
        40,
        "Can be thrown like a 'horny bomb'",
        "an ",
        "sweet-smelling",
        Horny(null, 6.0, 3),
        "An unnatural warmth spreads through your body and gathers in your dick like a fire.",
        "For a second, she's just surprised, but gradually a growing desire starts to make her weak in the knees"
    ),
    Sedative(
        "Sedative",
        25,
        "Tires out your opponent, but can also make her numb",
        "a ",
        "milky",
        Drowsy(null, 3.0),
        "The fog seems to fill your head and your body feels heavy.",
        "She stumbles for a moment, trying to clear the drowsiness from her head."
    ),
    SPotion(
        "Sensitivity Flask",
        25,
        "Who knows what's in this stuff, but it makes any skin it touches tingle",
        "a ",
        "fizzy",
        Hypersensitive(null),
        "Your skin becomes hot, but goosebumps appear anyway. Even the air touching your skin makes you shiver.",
        "She shivers as it takes hold and heightens her sense of touch."
    ),
    DisSol(
        "Dissolving Solution",
        30,
        "Destroys clothes, but completely non-toxic",
        "a ",
        "clear",
        Dissolving(null),
        "Your clothes start to disintegrate where ever the liquid lands.",
        "Her clothes start to vanish, exposing more of her soft skin."
    ),
    PAphrodisiac(
        "Potent Aphrodisiac",
        400,
        "More like a 'horny atomic bomb'",
        "a ",
        "intoxicating",
        Horny(null, 12.0, 3),
        "You feel momentarily light-headed as all the blood in your body rushes to your penis.",
        "She flushes deeply and instinctively reaches down to touch herself. She manages to restrain herself, but just barely."
    ),
    ;

    private val iName: String
    override val price: Int
    /**
     * The Item's display name.
     */
    override val desc: String
    override val prefix: String
    val color: String
    val effect: Status
    private val receivetext: String
    private val dealtext: String

    constructor(
        name: String,
        price: Int,
        desc: String,
        prefix: String,
        color: String,
        effect: Status,
        receivetext: String,
        dealtext: String
    ) {
        iName = name
        this.price = price
        this.desc = desc
        this.prefix = prefix
        this.color = color
        this.effect = effect
        this.receivetext = receivetext
        this.dealtext = dealtext
    }

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override fun getName(): String {
        return iName
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    fun getText(affected: Character): String {
        return if (affected.human()) {
            receivetext
        } else {
            dealtext
        }
    }

    fun canUse(c: Combat, user: Character, target: Character): Boolean {
        return when (this) {
            Lubricant -> target.isNude && !target.has(Stsflag.oiled)
            SPotion -> target.isNude && !target.has(Stsflag.hypersensitive)
            DisSol -> !target.isNude
            else -> true
        }
    }

    override val listed = true

    override val recipe = ArrayList<Item>()
}

package items

import characters.Character

enum class Consumable : Item {
    ZipTie("Heavy Zip Tie", 5, "A thick heavy tie suitable for binding someone's hands", "a "),
    Handcuffs("Handcuffs", 200, "Strong steel restraints, hard to escape from", ""),
    Talisman("Dark Talisman", 100, "This unholy trinket can cloud a target's mind", "a "),
    FaeScroll(
        "Summoning Scroll",
        150,
        "A magic scroll imbued with summoning magic. It can briefly support a large number of faeries",
        "a "
    ),
    needle("Drugged Needle", 10, "A small throwing needle coated in a potent aphrodisiac", "a "),
    smoke("Smoke Bomb", 20, "This harmless bomb provides cover for a quick escape", "a "),
    powerband(
        "Power Band",
        250,
        "This headband is a catalyst of great power, temporarily unleashing the wearer's maximum Ki",
        "a "
    ),
    ;
    private val iName: String
    override val price: Int
    /**
     * The Item's display name.
     */
    override val desc: String
    override val prefix: String

    constructor(name: String, price: Int, desc: String, prefix: String) {
        iName = name
        this.price = price
        this.desc = desc
        this.prefix = prefix
    }

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override fun getName(): String {
        return iName
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    override val listed = true

    override val recipe = ArrayList<Item>()
}

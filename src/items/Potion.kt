package items

import actions.Movement
import characters.Attribute
import characters.Character
import skills.Tactics
import status.Abuff
import status.Beastform
import status.Buzzed
import status.Caffeinated
import status.Status

enum class Potion : Item {
    Beer(
        "Beer",
        10,
        "Tastes like horse piss, but it'll numb your senses",
        "a can of ",
        Movement.beer,
        Buzzed(null),
        Tactics.status,
        "You pop open a beer and chug it down, feeling buzzed and a bit sluggish.",
        " opens up a can of cheap beer and chugs the whole thing."
    ),
    EnergyDrink(
        "Energy Drink",
        20,
        "It'll either kill you or restore your stamina",
        "an ",
        Movement.energydrink,
        Caffeinated(null, 3, 5.0),
        Tactics.recovery,
        "You chug down the unpleasant drink. Your tiredness immediately starts to recede.",
        " opens up an energy drink and downs the whole can."
    ),
    SuperEnergyDrink(
        "Super Energy Drink",
        50,
        "Mara's special blend",
        "a ",
        Movement.energydrink,
        Caffeinated(null, 3, 15.0),
        Tactics.recovery,
        "You chug down the bizarre drink. Energy surges through your body.",
        " opens up an energy drink and downs the whole bottle."
    ),
    Fox(
        "Fox Potion",
        100,
        "A strange mixture that can temporarily boost your Cunning",
        "a ",
        Movement.potion,
        Abuff(null, Attribute.Cunning, 3.0, 10),
        Tactics.status,
        "You drink down the potion and feel your mind quicken.",
        " swallows an unidentifiable potion. Her eyes take on a noticeable sharpness."
    ),
    Bull(
        "Bull Potion",
        100,
        "A strange mixture that can temporarily boost your Power",
        "a ",
        Movement.potion,
        Abuff(null, Attribute.Power, 3.0, 10),
        Tactics.status,
        "You drink down the potion and feel strength flow into your muscles.",
        " swallows an unidentifiable potion. You can see her muscles tighten slightly."
    ),
    Nymph(
        "Nymph Potion",
        100,
        "A strange mixture that can temporarily boost your Seduction",
        "a ",
        Movement.potion,
        Abuff(null, Attribute.Seduction, 3.0, 10),
        Tactics.status,
        "You drink down the potion. You feel sexier than usual.",
        " swallows an unidentifiable potion. She seems to take on a more sultry attitude."
    ),
    Cat(
        "Cat Potion",
        100,
        "A strange mixture that can temporarily boost your Speed",
        "a ",
        Movement.potion,
        Abuff(null, Attribute.Speed, 2.0, 10),
        Tactics.status,
        "You drink down the potion and feel an extra spring in your step.",
        " swallows an unidentifiable potion. You see her nimbly hop in place."
    ),
    Furlixir(
        "Furry Elixir",
        300,
        "A strange, musky mixture that can temporarily give you the strength and speed of a beast",
        "a ",
        Movement.potion,
        Beastform(null),
        Tactics.status,
        "You swallow the elixir and feel an intense itch all over your body. Dog ears and a tail force their way out of your body, and fur rapidly grows "
                + "on your limbs. You feel like a wild beast, and your physical capabilities have improved to match.",
        " swallows a strange potion. She shudders for a moment and "
                + "begins to transform. Animalistic feature emerge from her body like some kind of werewolf. She gives you an almost predatory smile that tells you to be wary of "
                + "her improved physical capability."
    ),
    ;

    private val iName: String
    override val price: Int
    /**
     * The Item's display name.
     */
    override val desc: String
    override val prefix: String
    val action: Movement
    private val effect: Status
    val tactic: Tactics
    private val resolution: String
    private val othertext: String

    constructor(
        name: String,
        price: Int,
        desc: String,
        prefix: String,
        action: Movement,
        effect: Status,
        tactic: Tactics,
        resolution: String,
        othertext: String
    ) {
        iName = name
        this.price = price
        this.desc = desc
        this.prefix = prefix
        this.action = action
        this.effect = effect
        this.tactic = tactic
        this.resolution = resolution
        this. othertext = othertext
    }

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override fun getName(): String {
        return name
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    fun getMessage(user: Character): String {
        return if (user.human()) {
            resolution
        } else {
            othertext
        }
    }

    fun getEffect(target: Character): Status {
        return effect.copy(target)
    }

    override val listed = true

    override val recipe = ArrayList<Item>()
}

package items

import characters.Character

interface Loot {
    fun getName(): String
    val prefix: String
    val price: Int
    fun pickup(owner: Character)
}

package items

import characters.Character

enum class Toy : Item {
    Dildo("Dildo", 250, "Big rubber cock: not a weapon", "a "),
    Crop("Riding Crop", 200, "Delivers a painful sting to instill discipline", "a "),
    Onahole("Onahole", 300, "An artificial vagina, but you can probably find some real ones pretty easily", "an "),
    Tickler("Tickler", 300, "Tickles and pleasures your opponent's sensitive areas", "a "),
    Strapon("Strap-on Dildo", 600, "Penis envy much?", "a "),
    ShockGlove("Shock Glove", 800, "Delivers a safe, but painful electric shock", "a "),
    Aersolizer("Aerosolizer", 500, "Turns a liquid into an unavoidable cloud of mist", "an "),
    Dildo2("Sonic Dildo", 2000, "Apparently vibrates at the ideal frequency to produce pleasure", "a "),
    Crop2("Hunting Crop", 1500, "Equipped with the fearsome Treasure Hunter attachment", "a "),
    Crop3("Mistress Crop", 10000, "An exquisitely crafted riding crop that looks as expensive as it is painful", "a "),
    Onahole2("Wet Onahole", 3000, "As hot and wet as the real thing", "an "),
    Tickler2("Enhanced Tickler", 3000, "Coated with a substance that can increase sensitivity", "an "),
    Strapon2("Flex-O-Peg", 2500, "A more flexible and versatile strapon with a built in vibrator", "the patented "),
    Excalibur("Sexcalibur", 1000, "A highly customizable toy ", ""),
    Paddle("Paddle", 1000, "", "a "),
    AnalBeads("Anal Beads", 1000, "", "a "),
    nippleclamp("Nipple Clamps", 1000, "", ""),
    bloodhound("Bloodhound", 2000, "An app that gives you your opponents' positions at set intervals", "");

    private val iName: String
    override val price: Int
    /**
     * The Item's display name.
     */
    override val desc: String
    override val prefix: String
    constructor(name: String, price: Int, desc: String, prefix: String) {
        iName = name
        this.desc = desc
        this.price = price
        this.prefix = prefix
    }

    override fun getFullDesc(owner: Character): String {
        var result = desc
        for (upgrade in renames) {
            if (owner.has(upgrade)) {
                result = upgrade.desc
                break
            }
        }
        for (addon in addons) {
            if (owner.has(addon) && addon.desc !== "") {
                result += ". " + addon.desc
            }
        }
        return result
    }

    override fun getName(): String {
        return iName
    }

    override fun getFullName(owner: Character): String {
        for (upgrade in renames) {
            if (owner.has(upgrade)) {
                return upgrade.getFullName(owner)
            }
        }
        return getName()
    }

    private val renames: ArrayList<Attachment>
        get() {
            val renames = ArrayList<Attachment>()
            when (this) {
                Excalibur -> {
                    renames.add(Attachment.Excalibur4)
                    renames.add(Attachment.Excalibur3)
                    renames.add(Attachment.Excalibur2)
                }

                Dildo, Dildo2 -> renames.add(Attachment.DildoSlimy)
                Onahole, Onahole2 -> renames.add(Attachment.OnaholeSlimy)
                Crop, Crop2 -> renames.add(Attachment.CropShocker)
                Tickler, Tickler2 -> renames.add(Attachment.TicklerFluffy)
                else -> {}
            }
            return renames
        }
    private val addons: ArrayList<Attachment>
        get() {
            val addons = ArrayList<Attachment>()
            when (this) {
                Excalibur -> {
                    addons.add(Attachment.ExcaliburArcane)
                    addons.add(Attachment.ExcaliburScience)
                    addons.add(Attachment.ExcaliburKi)
                    addons.add(Attachment.ExcaliburDark)
                    addons.add(Attachment.ExcaliburFetish)
                    addons.add(Attachment.ExcaliburAnimism)
                    addons.add(Attachment.ExcaliburNinjutsu)
                }

                Dildo, Dildo2 -> addons.add(Attachment.DildoLube)
                Onahole, Onahole2 -> addons.add(Attachment.OnaholeVibe)
                Crop, Crop2 -> addons.add(Attachment.CropKeen)
                Tickler, Tickler2 -> addons.add(Attachment.TicklerPheromones)
                else -> {}
            }
            return addons
        }

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    override val listed = true

    private fun linkAttachments() {
    }

    override val recipe = ArrayList<Item>()
}

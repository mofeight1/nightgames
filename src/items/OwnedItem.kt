package items

import characters.Character

data class OwnedItem<I: Loot> (val item: I, val owner: Character) {
    override fun toString(): String {
        return item.toString()
    }
}

package items

import characters.Character

enum class Trophy : Item {
    CassieTrophy("Cassie's Panties", "Cute and simple panties"),
    MaraTrophy("Mara's Underwear", "She wears boys underwear?"),
    AngelTrophy("Angel's Thong", "There's barely anything here"),
    JewelTrophy("Jewel's Panties", "Surprisingly lacy"),
    ReykaTrophy("Reyka's Clit Ring", "What else can you take from someone who goes commando?"),
    PlayerTrophy("Your Boxers", "How did you end up with these?"),
    EveTrophy("Eve's 'Panties'", "Crotchless and of no practical use"),
    KatTrophy("Kat's Panties", "Cute pink panties. Unfortunately there's no cat motif"),
    YuiTrophy("Yui's Panties", "White and innocent, not unlike their owner"),
    MayaTrophy("Maya's Panties", "Black lace. Very sexy"),
    SamanthaTrophy("Samantha's Thong", "A lacy red thong, translucent in all but the most delicate areas."),
    ValerieTrophy("Valerie's Panties", "Silky and high class."),
    SofiaTrophy("Sofia's Panties", "These otherwise plain white panties are unexpectedly narrow wear it matters.");

    private val iName: String
    /**
     * The Item's display name.
     */
    override val desc: String

    constructor(name: String, desc: String) {
        iName = name
        this.desc = desc
    }

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override val price =  0

    override fun getName(): String {
        return name
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override val prefix = ""

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    override val listed = true

    override val recipe = ArrayList<Item>()
}

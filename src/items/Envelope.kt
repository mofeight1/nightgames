package items

import characters.Character
import global.Challenge

class Envelope(private val goal: Challenge) : Item {
    override val desc: String = goal.message()

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override val price = 0

    override fun getName(): String {
        return "Challenge: " + goal.summary()
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override val prefix= "a "

    override val listed = true

    override val recipe = ArrayList<Item>()

    override fun pickup(owner: Character) {
        // TODO Auto-generated method stub
    }
}

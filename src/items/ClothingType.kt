package items

enum class ClothingType {
    TOPUNDER,
    TOP,
    TOPOUTER,
    UNDERWEAR,
    BOTOUTER,
    SPECIAL
}


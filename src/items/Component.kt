package items

import characters.Character

enum class Component : Item {
    Tripwire("Trip Wire", 10, "A strong wire used to trigger traps", "a "),
    Spring("Spring", 20, "A component for traps", "a "),
    Rope("Rope", 15, "A component for traps", "a "),
    Phone("Phone", 30, "A cheap disposable phone with a programable alarm", "a "),
    Sprayer("Sprayer", 30, "Necessary for making traps that use liquids", "a "),
    MSprayer("Micro Sprayer", 300, "A tiny, but reliable sprayer that can be hidden in a device", "a "),
    Totem("Fetish Totem", 150, "A small penis shaped totem that can summon tentacles", "a "),
    Capacitor(
        "Prime Capacitor",
        700,
        "This high qulity capacitor can reliably store and discharge a significant electrical charge",
        "a "
    ),
    SlimeCore("Slime Core", 500, "This semi-solid ball pulses as if it's alive", "a "),
    Semen("Semen", 0, "A small bottle filled with cum. Kinda gross", "a bottle of "),
    Battery("Battery", 0, "Available energy to power electronic equipment", "a "),
    Foxtail("Fox Tail", 500, "One of the tails of the mythical nine-tailed fox", "a "),
    Skin("Serpent Skin", 600, "Thin and flexible, but extremely tough", "some "),
    HGMotor(
        "Micro Motor",
        500,
        "This small motor would be a dramatic improvement for compatible store-bought toys",
        "a "
    ),
    Titanium("Titanium Alloy", 1000, "A valuable crafting material. What could you use it for?", "a block of "),
    DragonBone(
        "Dragon Bone",
        3000,
        "Supposedly a long bone from a dragon. Not much use unless you're planning to forge a legendary weapon",
        "a "
    ),
    PBlueprint("Prototype Blueprint", 4000, "Theoretical blueprint for the ultimate sex toy", "a "),
    PVibrator("Prototype Vibrator", 4000, "A critical component of the ultimate sex toy", "a "),
    PHandle("Prototype Handle", 4000, "A critical component of the ultimate sex toy", "a "),
    ;

    private val iName: String
    override val price: Int
    /**
     * The Item's display name.
     */
    override val desc: String
    override val prefix: String
    constructor(name: String, price: Int,desc: String, prefix: String) {
        iName = name
        this.price = price
        this.desc = desc
        this.prefix = prefix
    }

    override fun getFullDesc(owner: Character): String {
        return desc
    }

    override fun getName(): String {
        return iName
    }

    override fun getFullName(owner: Character): String {
        return getName()
    }

    override fun pickup(owner: Character) {
        owner.gain(this)
    }

    override val listed= true

    override val recipe = ArrayList<Item>()
}

package items

import characters.Character

interface Item : Loot {
    val desc: String
    fun getFullDesc(owner: Character): String
    fun getFullName(owner: Character): String
    val recipe: ArrayList<Item>
    val listed: Boolean
}

package items

import characters.Character
import characters.Trait

enum class Clothing : Loot {
    none("", ClothingType.SPECIAL, 0, "", Trait.none, Trait.none, 0),

    //Outerwear
    jacket("jacket", "Jacket", ClothingType.TOPOUTER, 2, "a ", Trait.none, Trait.none, 400),
    cloak("cloak", "Mystic Cloak", ClothingType.TOPOUTER, 2, "a ", Trait.mystic, Trait.none, 750),
    labcoat("lab coat", "Lab Coat", ClothingType.TOPOUTER, 3, "a ", Trait.geeky, Trait.none, 750),
    trenchcoat("trenchcoat", "Trenchcoat", ClothingType.TOPOUTER, 7, "a ", Trait.none, Trait.bulky, 1000),
    blazer("blazer", "Blazer", ClothingType.TOPOUTER, 3, "a ", Trait.none, Trait.none, 750),
    windbreaker("windbreaker", "Windbreaker", ClothingType.TOPOUTER, 1, "a ", Trait.none, Trait.none, 500),
    halfcloak("half-cloak", "Ninja's Half-Cloak", ClothingType.TOPOUTER, 2, "a ", Trait.stealthy, Trait.none, 800),
    furcoat("furcoat", "Fur Coat", ClothingType.TOPOUTER, 3, "a ", Trait.furry, Trait.none, 750),
    techarmor("tech armor", "High-Tech Armor", ClothingType.TOPOUTER, 10, "", Trait.geeky, Trait.stylish, 3000),
    noblecloak("noble cloak", "Noble Cloak", ClothingType.TOPOUTER, 5, "a ", Trait.none, Trait.stylish, 2000),
    herorobe("heroic robe", "Heroic Robe", ClothingType.TOPOUTER, 5, "a ", Trait.mystic, Trait.stylish, 3000),

    //Top
    shirt("shirt", "Simple Shirt", ClothingType.TOP, 5, "a ", Trait.none, Trait.none, 75),
    tanktop("tank top", "Tank Top", ClothingType.TOP, 1, "a ", Trait.none, Trait.none, 75),
    Tshirt("T-shirt", "T-Shirt", ClothingType.TOP, 5, "a ", Trait.none, Trait.none, 100),
    blouse("blouse", "Blouse", ClothingType.TOP, 5, "a ", Trait.none, Trait.none, 60),
    sweatshirt("sweatshirt", "Sweatshirt", ClothingType.TOP, 5, "", Trait.none, Trait.none, 50),
    sweater("sweater", "Ugly Sweater", ClothingType.TOP, 15, "a ", Trait.lame, Trait.none, 200),
    gi("gi", "Kung-Fu Gi", ClothingType.TOP, 5, "a ", Trait.martial, Trait.none, 500),
    gothshirt("goth shirt", "Goth-Style Shirt", ClothingType.TOP, 5, "a ", Trait.broody, Trait.none, 700),
    silkShirt("silk shirt", "Stylish Silk Shirt", ClothingType.TOP, 4, "a ", Trait.stylish, Trait.none, 1200),
    latextop("latex top", "Kinky Latex Top", ClothingType.TOPUNDER, 4, "", Trait.kinky, Trait.skimpy, 1000),
    blackdress("dress", "Short Violet Dress", ClothingType.TOP, 8, "a ", Trait.none, Trait.none, 400),
    kunoichitop("kunoichi top", "Kunoichi Top", ClothingType.TOP, 7, "a ", Trait.stealthy, Trait.skimpy, 1200),
    shinobitop("shinobi top", "Shinobi Top", ClothingType.TOP, 10, "a ", Trait.stealthy, Trait.none, 1200),
    lbustier("leather bustier", "Leather Bustier", ClothingType.TOP, 12, "a ", Trait.none, Trait.skimpy, 900),
    chestpiece("chest piece", "Chest Piece", ClothingType.TOP, 10, "a ", Trait.geeky, Trait.none, 2000),
    noblevest("noble vest", "Noble Vest", ClothingType.TOP, 10, "a ", Trait.none, Trait.stylish, 1500),
    shrinerobe("shrine robe", "Shrine Robe", ClothingType.TOP, 10, "a ", Trait.none, Trait.caste, 1500),
    legendshirt("legendary T-shirt", "Legendary T-Shirt", ClothingType.TOP, 15, "the ", Trait.legend, Trait.none, 5000),
    magetop("mage top", "Mage Top", ClothingType.TOP, 10, "a ", Trait.mystic, Trait.stylish, 2000),
    ruffledress("ruffled dress", "Ruffled Violet Dress", ClothingType.TOP, 10, "a ", Trait.stylish, Trait.none, 2000),

    //Pants
    pants("pants", "Plain Pants", ClothingType.BOTOUTER, 15, "", Trait.none, Trait.none, 100),
    skirt("skirt", "Simple Skirt", ClothingType.BOTOUTER, 12, "a ", Trait.none, Trait.none, 60),
    shorts("shorts", "Shorts", ClothingType.BOTOUTER, 15, "", Trait.none, Trait.none, 100),
    jeans("jeans", "Blue Jeans", ClothingType.BOTOUTER, 17, "", Trait.none, Trait.none, 200),
    sweatpants("sweatpants", "Baggy Sweatpants", ClothingType.BOTOUTER, 15, "", Trait.none, Trait.lame, 50),
    miniskirt("miniskirt", "Skimpy Miniskirt", ClothingType.BOTOUTER, 12, "a ", Trait.none, Trait.none, 400),
    lminiskirt("leather miniskirt", "Leather Miniskirt", ClothingType.BOTOUTER, 17, "a ", Trait.none, Trait.none, 800),
    latexpants("latex pants", "Kinky Latex Pants", ClothingType.BOTOUTER, 15, "", Trait.kinky, Trait.skimpy, 1100),
    dresspants("dress pants", "Stylish Dress Pants", ClothingType.BOTOUTER, 15, "", Trait.stylish, Trait.none, 900),
    kungfupants("kung-fu pants", "Kung-Fu Pants", ClothingType.BOTOUTER, 12, "", Trait.martial, Trait.none, 600),
    gothpants("goth pants", "Black Goth Pants", ClothingType.BOTOUTER, 15, "", Trait.broody, Trait.none, 800),
    cutoffs("cut-off jeans", "Cut-Off Jean Shorts", ClothingType.BOTOUTER, 16, "", Trait.none, Trait.none, 150),
    ninjapants("ninja leggings", "Ninja Leggings", ClothingType.BOTOUTER, 12, "", Trait.stealthy, Trait.none, 800),
    kilt("kilt", "Kilt", ClothingType.BOTOUTER, 10, "a ", Trait.mighty, Trait.none, 600),
    shrineskirt("shrine skirt", "Shrine Skirt", ClothingType.BOTOUTER, 10, "a ", Trait.caste, Trait.none, 800),
    techpants("tech leggings", "High-Tech Leggings", ClothingType.BOTOUTER, 15, "", Trait.geeky, Trait.stylish, 1500),
    tdresspants(
        "tailored dress pants",
        "Tailored Dress Pants",
        ClothingType.BOTOUTER,
        15,
        "",
        Trait.stylish,
        Trait.none,
        1500
    ),
    skirt2("skirt", "Refined Skirt", ClothingType.BOTOUTER, 13, "a ", Trait.none, Trait.none, 600),


    //Underwear
    bra("bra", "Bra", ClothingType.TOPUNDER, 5, "a ", Trait.none, Trait.none, 200),
    wrap("chest wrap", "Bra", ClothingType.TOPUNDER, 15, "a ", Trait.skimpy, Trait.none, 200),
    panties("panties", "Panties", ClothingType.UNDERWEAR, 15, "", Trait.none, Trait.none, 250),
    boxers("boxers", "Boxer Shorts", ClothingType.UNDERWEAR, 15, "", Trait.none, Trait.none, 200),
    underwear("underwear", "Plain Underwear", ClothingType.UNDERWEAR, 15, "", Trait.none, Trait.none, 200),
    briefs("briefs", "Tight Briefs", ClothingType.UNDERWEAR, 15, "", Trait.none, Trait.none, 200),
    thong("thong", "Thong", ClothingType.UNDERWEAR, 15, "a ", Trait.none, Trait.skimpy, 400),
    bikinitop("bikini top", "Skimpy Bikini Top", ClothingType.TOPUNDER, 1, "a ", Trait.none, Trait.skimpy, 500),
    bikinibottoms(
        "bikini bottom",
        "Skimpy Bikini Bottom",
        ClothingType.UNDERWEAR,
        5,
        "",
        Trait.none,
        Trait.skimpy,
        500
    ),
    undershirt("undershirt", "White Undershirt", ClothingType.TOPUNDER, 5, "a ", Trait.lame, Trait.none, 60),
    camisole("camisole", "Thin Camisole", ClothingType.TOPUNDER, 10, "a ", Trait.none, Trait.none, 700),
    lacebra("lacy bra", "Lacy Bra", ClothingType.TOPUNDER, 5, "a ", Trait.none, Trait.none, 275),
    lacepanties("lace panties", "Lace Panties", ClothingType.UNDERWEAR, 17, "", Trait.stylish, Trait.skimpy, 1000),
    lacythong("lacy thong", "Lacy Thong", ClothingType.UNDERWEAR, 7, "a ", Trait.stylish, Trait.skimpy, 275),
    garters("garters", "Lace Garters", ClothingType.BOTOUTER, 5, "", Trait.none, Trait.skimpy, 400),
    speedo("banana hammock", "Banana Hammock", ClothingType.UNDERWEAR, 10, "a ", Trait.none, Trait.skimpy, 500),
    loincloth("loincloth", "Loincloth", ClothingType.UNDERWEAR, 5, "a ", Trait.none, Trait.accessible, 50),
    nipplecover("nipple covers", "Nipple Covers", ClothingType.TOPUNDER, 10, "", Trait.geeky, Trait.skimpy, 1500),
    groincover("groin cover", "Groin Cover", ClothingType.UNDERWEAR, 10, "a ", Trait.geeky, Trait.skimpy, 1500),
    ofuda("ofuda", "Ofuda", ClothingType.UNDERWEAR, 2, "a ", Trait.antihorny, Trait.skimpy, 1500),

    //Misc
    strapon("strap-on dildo", ClothingType.UNDERWEAR, 10, "a ", Trait.strapped, Trait.none, 0),
    chastitybelt("chastity belt", ClothingType.UNDERWEAR, 24, "a ", Trait.armored, Trait.indestructible, 0),
    cup("groin protector", ClothingType.UNDERWEAR, 3, "a ", Trait.armored, Trait.bulky, 1000),
    warpaint("war paint", ClothingType.TOPUNDER, 0, "", Trait.none, Trait.ineffective, 120),
    crotchlesspanties("crotchless panties", ClothingType.UNDERWEAR, 15, "", Trait.kinky, Trait.ineffective, 1200),
    pouchlessbriefs("pouchless briefs", ClothingType.UNDERWEAR, 15, "", Trait.kinky, Trait.ineffective, 1200),

    //Temporary
    fabloincloth("fabricated loincloth", ClothingType.UNDERWEAR, 10, "", Trait.none, Trait.skimpy, 0, true),
    fabwrap("fabricated wrap", ClothingType.TOP, 5, "a ", Trait.none, Trait.none, 0, true),
    magethong("magic thong", ClothingType.UNDERWEAR, 10, "", Trait.none, Trait.skimpy, 0, true),
    bharness("leather harness", ClothingType.UNDERWEAR, 15, "", Trait.kinky, Trait.skimpy, 0, true),
    bstraps("leather straps", ClothingType.TOPUNDER, 5, "", Trait.kinky, Trait.skimpy, 0, true),
    ;

    val properName: String
    val buff: Trait?
    val attribute: Trait?
    override val price: Int
    val type: ClothingType?
    val isTemp: Boolean
    private val iName: String
    val dc: Int
    override val prefix: String

    constructor(name: String, dc: Int, prefix: String) {
        this.iName = name
        this.dc = dc
        this.prefix = prefix
        properName = name
        this.buff = null
        this.attribute = null
        this.price = 0
        this.type = null
        this.isTemp = false
    }

    constructor(
        name: String,
        type: ClothingType,
        dc: Int,
        prefix: String,
        buff: Trait,
        attribute: Trait,
        price: Int
    ) {
        this.iName = name
        this.dc = dc
        this.prefix = prefix
        properName = name
        this.buff = buff
        this.attribute = attribute
        this.price = price
        this.type = type
        this.isTemp = false
    }

    constructor(
        name: String,
        proper: String,
        type: ClothingType,
        dc: Int,
        prefix: String,
        buff: Trait,
        attribute: Trait,
        price: Int
    ) {
        this.iName = name
        this.dc = dc
        this.prefix = prefix
        properName = proper
        this.buff = buff
        this.attribute = attribute
        this.price = price
        this.type = type
        this.isTemp = false
    }

    constructor(
        name: String,
        type: ClothingType,
        dc: Int,
        prefix: String,
        buff: Trait,
        attribute: Trait,
        price: Int,
        temp: Boolean
    ) {
        this.iName = name
        this.dc = dc
        this.prefix = prefix
        properName = name
        this.buff = buff
        this.attribute = attribute
        this.price = price
        this.type = type
        this.isTemp = temp
    }

    override fun getName(): String {
        return iName
    }

    val fullDesc: String
        get() {
            if (this == none) {
                return "Nothing"
            }
            var desc = "$properName ($dc)"
            if (this.buff != Trait.none || this.attribute != Trait.none) {
                desc += " $specialDescription"
            }
            return desc
        }

    val specialDescription: String
        get() {
            var desc = ""
            if (this.buff != Trait.none) {
                desc += buff!!.desc
                if (this.attribute != Trait.none) {
                    desc = desc + ", " + attribute!!.desc
                }
            } else {
                desc = if (this.attribute != Trait.none) {
                    desc + ", " + attribute!!.desc
                } else {
                    "No special properties"
                }
            }
            return desc
        }

    val isTop get() =
        type == ClothingType.TOP ||
            type == ClothingType.TOPOUTER ||
            type == ClothingType.TOPUNDER
    fun adds(test: Trait): Boolean {
        return test == buff
    }

    fun stripOffenseBonus(attacker: Character): Double {
        var result = 1.0
        if (attacker.has(Trait.bramaster) && (type == ClothingType.TOP || type == ClothingType.TOPOUTER || type == ClothingType.TOPUNDER)) {
            result *= 2
        }
        if (attacker.has(Trait.pantymaster) && (type == ClothingType.BOTOUTER || type == ClothingType.UNDERWEAR)) {
            result *= 2
        }
        return result
    }

    override fun pickup(owner: Character) {
        if (!owner.has(this)) {
            owner.gain(this)
        }
    }

    override fun toString(): String {
        return getName()
    }
}

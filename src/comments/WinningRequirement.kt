package comments

import characters.Character
import combat.Combat

class WinningRequirement : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        var score = INITIAL_WINNING_SCORE
        score += (other.arousal.percent() - self.arousal.percent()) / 2
        if (c.stance.dom(self)) score += 10
        if (c.stance.dom(other)) score -= 10
        return score >= 20
    }

    companion object {
        private const val INITIAL_WINNING_SCORE = 0.0
    }
}
package comments

import characters.Character
import combat.Combat

class DomRequirement : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.stance.dom(self)
    }
}
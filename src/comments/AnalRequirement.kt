package comments

import characters.Character
import combat.Combat
import stance.Stance

class AnalRequirement(private val anal: Boolean) : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.stance.penetration(other) && (anal == (c.stance.en == Stance.anal))
    }
}
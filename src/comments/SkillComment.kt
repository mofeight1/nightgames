package comments

import characters.Character
import combat.Combat
import combat.Result
import combat.Tag

class SkillComment(
    override val priority: Int,
    override val probability: Int,
    private val tag: Tag,
    private val received: Boolean
) : CommentTrigger {
    constructor(tag: Tag) : this(DEFAULTPRIORITY, DEFAULTPROBABILITY, tag, false)
    constructor(tag: Tag, received: Boolean) : this(DEFAULTPRIORITY, DEFAULTPROBABILITY, tag, received)

    override fun isApplicable(c: Combat, self: Character, other: Character): Boolean {
        if (received) {
            if (c.getEvents(self).any { it.event == Result.receiveskill && it.skill!!.hasTag(tag) })
                return true
        } else {
            if (c.getEvents(self).any { it.event == Result.useskill && it.skill!!.hasTag(tag) })
                return true
        }
        return false
    }

    override fun equals(other: Any?): Boolean {
        if (other !is SkillComment) {
            return false
        }
        return other.tag === tag && other.received == received
    }

    companion object {
        private const val DEFAULTPRIORITY = 3
        private const val DEFAULTPROBABILITY = 80
    }
}

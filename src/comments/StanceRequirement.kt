package comments

import characters.Character
import combat.Combat
import stance.Stance

class StanceRequirement(var stance: Stance) : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.stance.en == stance
    }
}

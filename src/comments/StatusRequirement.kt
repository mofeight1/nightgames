package comments

import characters.Character
import combat.Combat
import status.Stsflag
import utilities.enumValueOfOrNull

class StatusRequirement(flag: String) : CustomRequirement {
    private val flag: Stsflag? = enumValueOfOrNull<Stsflag>(flag)

    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        if (flag == null) return false

        return self.getStatus(flag) != null
    }
}
package comments

import characters.Character
import combat.Combat
import global.Global
import stance.Stance
import comments.CommentSituationTools.rev

enum class CommentSituation(
    override val priority: Int,
    override val probability: Int,
    vararg reqs: CustomRequirement) :
    CommentTrigger {
    // Fucking
    VAG_DOM_PITCH_WIN(
        2, 40, InsertedRequirement(true),
        rev(AnalRequirement(false)), DomRequirement(),
        WinningRequirement()
    ),
    VAG_DOM_PITCH_LOSE(
        2, 40, InsertedRequirement(true),
        rev(AnalRequirement(false)), DomRequirement(),
        rev(WinningRequirement())
    ),
    VAG_DOM_CATCH_WIN(
        2, 40, rev(InsertedRequirement(true)),
        AnalRequirement(false), DomRequirement(),
        WinningRequirement()
    ),
    VAG_DOM_CATCH_LOSE(
        2, 40, rev(InsertedRequirement(true)),
        AnalRequirement(false), DomRequirement(),
        rev(WinningRequirement())
    ),
    VAG_SUB_PITCH_WIN(
        2, 40, InsertedRequirement(true),
        rev(AnalRequirement(false)), SubRequirement(),
        WinningRequirement()
    ),
    VAG_SUB_PITCH_LOSE(
        2, 40, InsertedRequirement(true),
        rev(AnalRequirement(false)), SubRequirement(),
        rev(WinningRequirement())
    ),
    VAG_SUB_CATCH_WIN(
        2, 40, rev(InsertedRequirement(true)),
        AnalRequirement(false), SubRequirement(),
        WinningRequirement()
    ),
    VAG_SUB_CATCH_LOSE(
        2, 40, rev(InsertedRequirement(true)),
        AnalRequirement(false), SubRequirement(),
        rev(WinningRequirement())
    ),
    ANAL_PITCH_WIN(
        2, 40, InsertedRequirement(true),
        rev(AnalRequirement(true)),
        WinningRequirement()
    ),
    ANAL_PITCH_LOSE(
        2, 40, InsertedRequirement(true),
        rev(AnalRequirement(true)),
        rev(WinningRequirement())
    ),
    ANAL_CATCH_WIN(
        2, 40, rev(InsertedRequirement(true)),
        AnalRequirement(true),
        WinningRequirement()
    ),
    ANAL_CATCH_LOSE(
        2, 40, rev(InsertedRequirement(true)),
        AnalRequirement(true),
        rev(WinningRequirement())
    ),

    // Stances
    BEHIND_DOM_WIN(
        1, 30, StanceRequirement(Stance.behind), DomRequirement(),
        WinningRequirement()
    ),
    BEHIND_DOM_LOSE(
        1, 30, StanceRequirement(Stance.behind), DomRequirement(),
        rev(WinningRequirement())
    ),
    BEHIND_SUB_WIN(
        1, 30, StanceRequirement(Stance.behind), SubRequirement(),
        WinningRequirement()
    ),
    BEHIND_SUB_LOSE(
        1, 30, StanceRequirement(Stance.behind), SubRequirement(),
        rev(WinningRequirement())
    ),
    SIXTYNINE_WIN(1, 30, StanceRequirement(Stance.sixnine), WinningRequirement()),
    SIXTYNINE_LOSE(1, 30, StanceRequirement(Stance.sixnine), rev(WinningRequirement())),
    MOUNT_DOM_WIN(
        1, 30, StanceRequirement(Stance.mount), DomRequirement(),
        WinningRequirement()
    ),
    MOUNT_DOM_LOSE(
        1, 30, StanceRequirement(Stance.mount), DomRequirement(),
        rev(WinningRequirement())
    ),
    MOUNT_SUB_WIN(
        1, 30, StanceRequirement(Stance.mount), SubRequirement(),
        WinningRequirement()
    ),
    MOUNT_SUB_LOSE(
        1, 30, StanceRequirement(Stance.mount), SubRequirement(),
        rev(WinningRequirement())
    ),
    PIN_DOM_WIN(
        1, 30, StanceRequirement(Stance.pin), DomRequirement(),
        WinningRequirement()
    ),
    PIN_DOM_LOSE(
        1, 30, StanceRequirement(Stance.pin), DomRequirement(),
        rev(WinningRequirement())
    ),
    PIN_SUB_WIN(
        1, 30, StanceRequirement(Stance.pin), SubRequirement(),
        WinningRequirement()
    ),
    PIN_SUB_LOSE(
        1, 30, StanceRequirement(Stance.pin), SubRequirement(),
        rev(WinningRequirement())
    ),
    FACESIT_DOM_WIN(
        1, 30, StanceRequirement(Stance.facesitting), DomRequirement(),
        WinningRequirement()
    ),
    FACESIT_DOM_LOSE(
        1, 30, StanceRequirement(Stance.facesitting), DomRequirement(),
        rev(WinningRequirement())
    ),
    FACESIT_SUB_WIN(
        1, 30, StanceRequirement(Stance.facesitting), SubRequirement(),
        WinningRequirement()
    ),
    FACESIT_SUB_LOSE(
        1, 30, StanceRequirement(Stance.facesitting), SubRequirement(),
        rev(WinningRequirement())
    ),

    // Statuses
    SELF_BOUND(0, 30, StatusRequirement("bound")),
    OTHER_BOUND(0, 30, rev(StatusRequirement("bound"))),
    OTHER_STUNNED(0, 30, rev(StatusRequirement("stunned"))),
    SELF_CHARMED(0, 30, StatusRequirement("charmed")),
    OTHER_CHARMED(0, 30, rev(StatusRequirement("charmed"))),
    OTHER_ENTHRALLED(0, 30, rev(StatusRequirement("enthralled"))),
    SELF_HORNY(0, 30, StatusRequirement("horny")),
    OTHER_HORNY(0, 30, rev(StatusRequirement("horny"))),
    SELF_OILED(0, 30, StatusRequirement("oiled")),
    OTHER_OILED(0, 30, rev(StatusRequirement("oiled"))),
    SELF_SHAMED(0, 30, StatusRequirement("shamed")),
    OTHER_SHAMED(0, 30, rev(StatusRequirement("shamed"))),
    NO_COMMENT(-1, 0),


    //Event
    SELF_BUSTED(5, 100, LowBlowRequirement()),
    OTHER_BUSTER(5, 50, rev(LowBlowRequirement())),
    SELF_PET(1, 30, PetRequirement()),
    OTHER_PET(1, 30, rev(PetRequirement()));


    private val reqs: Set<CustomRequirement> = reqs.toHashSet()

    override fun isApplicable(c: Combat, self: Character, other: Character): Boolean {
        return reqs.none { !it.meets(c, self, other) }
    }

    companion object {
        fun getApplicableComments(
            c: Combat,
            self: Character, other: Character
        ): Set<CommentSituation> {
            val comments = entries.filter { it.isApplicable(c, self, other) }.toSet()
            if (comments.isEmpty()) return setOf(NO_COMMENT)
            return comments
        }

        fun getBestComment(
            offered: CommentGroup, c: Combat,
            self: Character, other: Character
        ): String? {
            var best: CommentTrigger? = null
            for (trigger in offered.triggers) {
                if (trigger.isApplicable(c, self, other) && offered[trigger].isNotEmpty()) {
                    if (best == null || trigger.priority > best.priority) {
                        best = trigger
                    }
                }
            }
            if (best == null || Global.random(100) > best.probability) {
                return null
            }
            return offered[best].random()
        }

        /*private fun rev(req: CustomRequirement): CustomRequirement {
            return ReverseRequirement(listOf(req))
        }*/
    }
}
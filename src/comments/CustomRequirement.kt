package comments

import characters.Character
import combat.Combat

interface CustomRequirement {
    fun meets(c: Combat, self: Character, other: Character): Boolean
}

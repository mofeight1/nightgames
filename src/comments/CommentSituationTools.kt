package comments

import characters.Character
import combat.Combat
import comments.CommentSituation.NO_COMMENT
import comments.CommentSituation.entries
import global.Global

object CommentSituationTools {
    fun getApplicableComments(
        c: Combat,
        self: Character, other: Character
    ): Set<CommentSituation> {
        val comments: HashSet<CommentSituation> = HashSet()
        for (comment in entries) {
            if (comment.isApplicable(c, self, other)) {
                comments.add(comment)
            }
        }
        if (comments.isEmpty()) return setOf(NO_COMMENT)
        return comments
    }

    fun getBestComment(
        offered: CommentGroup, c: Combat,
        self: Character, other: Character
    ): String? {
        var best: CommentTrigger? = null
        for (comment in offered.triggers) {
            if (comment.isApplicable(c, self, other) && offered[comment].isNotEmpty()) {
                if (best == null || comment.priority > best.priority) {
                    best = comment
                }
            }
        }
        if (best == null || Global.random(100) > best.probability) {
            return null
        }
        return offered[best].random()
    }

    fun rev(req: CustomRequirement): CustomRequirement {
        return ReverseRequirement(listOf(req))
    }
}
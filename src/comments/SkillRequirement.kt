package comments

import characters.Character
import combat.Combat
import skills.Skill

class SkillRequirement(private val skill: Skill) : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.lastact(self) != null
    }
}

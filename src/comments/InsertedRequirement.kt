package comments

import characters.Character
import combat.Combat

class InsertedRequirement(var inserted: Boolean) : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.stance.penetration(self) == inserted
    }
}

package comments

import characters.Character
import combat.Combat

interface CommentTrigger {
    fun isApplicable(c: Combat, self: Character, other: Character): Boolean
    val priority: Int
    val probability: Int
}

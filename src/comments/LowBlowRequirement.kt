package comments

import characters.Anatomy
import characters.Character
import combat.Combat
import combat.Result

class LowBlowRequirement : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.getEvents(self).any { it.event == Result.receivepain && it.part == Anatomy.genitals }
    }
}

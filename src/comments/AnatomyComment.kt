package comments

import characters.Anatomy
import characters.Character
import combat.Combat
import combat.Result

class AnatomyComment(
    override val priority: Int,
    override val probability: Int,
    private val event: Result,
    private val part: Anatomy
) : CommentTrigger {
    constructor(event: Result, part: Anatomy) : this(DEFAULTPRIORITY, DEFAULTPROBABILITY, event, part)

    override fun isApplicable(c: Combat, self: Character, other: Character): Boolean {
        return c.getEvents(self).any { it.event == event && it.part == part }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is AnatomyComment) {
            return false
        }
        return other.event == event && other.part == part
    }

    companion object {
        private const val DEFAULTPRIORITY = 2
        private const val DEFAULTPROBABILITY = 50
    }
}

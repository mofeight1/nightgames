package comments

import characters.Character
import combat.Combat
import combat.Result

class EventComment(
    override val priority: Int,
    override val probability: Int,
    private val event: Result
) : CommentTrigger {
    constructor(event: Result) : this(DEFAULTPRIORITY, DEFAULTPROBABILITY, event)

    override fun isApplicable(c: Combat, self: Character, other: Character): Boolean {
        return c.getEvents(self).any { it.event == event }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is EventComment) return false
        return other.event == event
    }

    companion object {
        private const val DEFAULTPRIORITY = 3
        private const val DEFAULTPROBABILITY = 100
    }
}

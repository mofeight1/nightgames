package comments

import characters.Character
import combat.Combat

class ReverseRequirement(private val reqs: List<CustomRequirement>) : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return reqs.any { !it.meets(c, other, self) }
    }
}

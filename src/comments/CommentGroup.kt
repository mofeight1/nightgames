package comments

class CommentGroup {
    val group = HashMap<CommentTrigger, ArrayList<String>>()
    val triggers get() = group.keys

    operator fun set(situation: CommentTrigger, comment: String) {
        if (!group.containsKey(situation)) {
            group[situation] = ArrayList()
        }
        group[situation]!!.add(comment)
    }
    operator fun get(situation: CommentTrigger): ArrayList<String> {
        return group[situation]!!
    }
}

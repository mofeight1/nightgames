package comments

import characters.Character
import combat.Combat

class PetRequirement : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return self.pet != null
    }
}

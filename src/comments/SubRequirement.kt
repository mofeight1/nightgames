package comments

import characters.Character
import combat.Combat

class SubRequirement : CustomRequirement {
    override fun meets(c: Combat, self: Character, other: Character): Boolean {
        return c.stance.sub(self)
    }
}

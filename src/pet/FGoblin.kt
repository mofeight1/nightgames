package pet

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import global.Global
import status.BD
import status.Masochistic
import status.Shamed

class FGoblin(owner: Character, pow: Double, ac: Int) : Pet("Fetish Goblin", owner, Ptype.fgoblin, pow, ac) {
    override fun describe(): String {
        return ""
    }

    override fun act(c: Combat, target: Character) {
        if (target.human()) {
            when (pickSkill(c, target)) {
                PetSkill.VIBRATOR -> {
                    c.write(
                        owner,
                        String.format(
                            "%s's goblin pulls the vibrator out of her wet hole and thrusts it between your legs.",
                            owner.name
                        )
                    )
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                }

                PetSkill.MASOCHISM -> {
                    c.write(
                        owner, String.format(
                            "%s's fetish goblin draws a riding crop and hits her own balls with it. She shivers with delight at the pain and you can "
                                    + "feel an aura of masochism radiate off her.", owner.name
                        )
                    )
                    owner.add(Masochistic(owner), c)
                    target.add(Masochistic(target), c)
                }

                PetSkill.BONDAGE -> {
                    c.write(
                        owner, String.format(
                            "%s's fetish goblin pulls the bondage straps tighter around herself. You can see the leather and latex digging into her skin as "
                                    + "her bondage fascination begins to affect both you and %s.",
                            owner.name,
                            owner.name
                        )
                    )
                    owner.add(BD(owner), c)
                    target.add(BD(target), c)
                }

                PetSkill.DENIAL -> {
                    c.write(
                        owner, String.format(
                            "%s's fetish goblin suddenly appears to turn against %s and slaps %s sensitive testicles. You're momentarily confused, but you "
                                    + "realize the shock probably undid your efforts to make %s cum.",
                            owner.name,
                            owner.pronounTarget(false),
                            owner.possessive(false),
                            owner.pronounTarget(false)
                        )
                    )
                    owner.pain(3.0 * Global.random(power.toInt()), Anatomy.genitals, c)
                    owner.calm((3 + Global.random(5)) * power, c)
                }

                PetSkill.FACEFUCK -> {
                    c.write(
                        owner, String.format(
                            "%s's fetish goblin straddles your head, giving you an eyeful of her assorted genitals. She pulls the vibrator out of her "
                                    + "pussy, causing a rain of love juice to splash your face. She then wipes her leaking cock on your forehead, smearing you with precum. You feel "
                                    + "your face flush with shame as she marks you with her fluids.", owner.name
                        )
                    )
                    target.add(Shamed(target), c)
                }

                PetSkill.ANALDILDO -> {
                    c.write(
                        owner, String.format(
                            "You jump in surprise as you suddenly feel something solid penetrating your asshole. %s's fetish goblin got behind you during "
                                    + "the fight and delivered a sneak attack with an anal dildo. Before you can retaliate she withdraws the toy and retreats to safety.",
                            owner.name
                        )
                    )
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.ass, combat = c)
                }

                PetSkill.ANALBEADS -> {
                    c.write(
                        owner, String.format(
                            "%s's fetish goblin takes advantage of your helplessness and positions herself behind you. She produces a string on anal beads "
                                    + "and proceeds to insert them one bead at a time into your anus. She manages to get five beads in while you're unable to defend yourself. When she "
                                    + "pulls them out, it feels like they're turning you inside out.", owner.name
                        )
                    )
                    target.pleasure(5.0 * Global.random(power.toInt()), Anatomy.ass, combat = c)
                }

                else -> c.write(
                    owner,
                    String.format(
                        "%s's fetish goblin stays at the edge of battle and touches herself absentmindedly.",
                        owner.name
                    )
                )
            }
        } else {
            when (pickSkill(c, target)) {
                PetSkill.VIBRATOR -> {
                    c.write(
                        owner,
                        String.format(
                            "Your fetish goblin removes the humming vibrator from her own wet pussy and shoves it into %s's.",
                            target.name
                        )
                    )
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                }

                PetSkill.MASOCHISM -> {
                    c.write(
                        owner, String.format(
                            "Your fetish goblin draws a riding crop and hits her own balls with it. She shivers with delight at the pain and you can "
                                    + "feel an aura of masochism radiate off her."
                        )
                    )
                    owner.add(Masochistic(owner), c)
                    target.add(Masochistic(target), c)
                }

                PetSkill.BONDAGE -> {
                    c.write(
                        owner, String.format(
                            "Your fetish goblin pulls the bondage straps tighter around herself. You can see the leather and latex digging into her skin as "
                                    + "her bondage fascination begins to affect both you and %s.", target.name
                        )
                    )
                    owner.add(BD(owner), c)
                    target.add(BD(target), c)
                }

                PetSkill.DENIAL -> {
                    c.write(
                        owner, String.format(
                            "As you feel your arousal build, your fetish goblin suddenly turns and slaps you sharply on the balls. You wince in pain and "
                                    + "let out a yelp of protest. You can't see the goblin's expression through her mask, but her eyes seem to be scolding you for your lack of self control."
                        )
                    )
                    owner.pain(3.0 * Global.random(power.toInt()), Anatomy.genitals, c)
                    owner.calm((3 + Global.random(5)) * power, c)
                }

                PetSkill.FACEFUCK -> {
                    c.write(
                        owner, String.format(
                            "Your fetish goblin leaps onto %s's face as %s's lying on the floor. The goblin rubs her cock and balls on %s face, humiliating %s",
                            target.name,
                            target.pronounSubject(false),
                            target.possessive(false),
                            target.pronounTarget(false)
                        )
                    )
                    target.add(Shamed(target), c)
                }

                PetSkill.ANALDILDO -> {
                    c.write(
                        owner, String.format(
                            "Your fetish goblin manages to sneak up on %s and stick a dildo in her ass. %s lets out a shriek of surprise at the sudden "
                                    + "sensation and your goblin gets away before %s can catch her",
                            target.name,
                            target.pronounSubject(true),
                            target.name
                        )
                    )
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.ass, combat = c)
                }

                PetSkill.ANALBEADS -> {
                    c.write(
                        owner, String.format(
                            "Your fetish goblin takes advantage of %s's defenselessness to push a string of anal beads into her butt. She lets out a "
                                    + "whimper of protest as each bead goes in and a moan of pleasure as they're all pulled out.",
                            target.name
                        )
                    )
                    target.pleasure(5.0 * Global.random(power.toInt()), Anatomy.ass, combat = c)
                }

                else -> c.write(
                    owner,
                    String.format("Your fetish goblin stays at the edge of battle and touches herself absentmindedly.")
                )
            }
        }
    }

    override fun vanquish(c: Combat, opponent: Pet) {
        when (opponent.type()) {
            Ptype.fairyfem -> c.write(
                owner, String.format(
                    "%s faerie girl flies low, aiming for %s fetish goblin's weak spot, but the goblin knocks the little fae out of the air "
                            + "with a swing of her hefty girl-cock. The fetish goblin grabs the dazed faerie and finishes her off by pressing a vibrator against her tiny "
                            + "slit.", opponent.own(), own()
                )
            )

            Ptype.fairymale -> c.write(
                owner, String.format(
                    "%s fetish goblin manages to catch %s faerie as he carelessly flies too close. She shoves the tiny male between her heavy "
                            + "boobs, completely engulfing him within her cleavage. You don't know if or when he desummoned, but he's clearly not coming out of there.",
                    own(),
                    opponent.own()
                )
            )

            Ptype.impfem -> c.write(
                owner, String.format(
                    "%s fetish goblin overpowers %s imp girl and quickly binds her wrists. The goblin bends the helpless imp over her knee "
                            + "and slides a suspiciously wet vibrator into her exposes pussy. The imp moans and squirms, but is unable to get away. The goblin punishes the "
                            + "escape attempt with ten hard slaps on the imp's upturned ass. By the final slap, the imp is practically screaming in orgasm and quickly vanishes "
                            + "in a puff of brimstone.", own(), opponent.own()
                )
            )

            Ptype.impmale -> c.write(
                owner, String.format(
                    "%s fetish goblin and %s imp grapple with each other, vying for dominance. The larger goblin gains the upper hand and "
                            + "staggers the imp with a quick knee to the groin. The imp doubles over in pain, giving the hermaphroditic goblin time to get behind him. She "
                            + "lines up her cock with the imp's unprotected ass and penetrates him with a firm thrust. %s imp squeals in alarm as being suddenly fucked "
                            + "from behind. The goblin pegs the imp steadily and reaches around to stroke his cock. Soon, the imp sprays cum into the air and disappears.",
                    own(), opponent.own(), opponent.own()
                )
            )

            Ptype.slime -> c.write(
                owner, String.format(
                    "%s fetish goblin removes her boots and steps barefoot into %s slime. The amorphous creature squirms happily around the "
                            + "goblin's feet, but makes no attempt to attack. It seems to be completely fascinated with her feet. The goblin wiggles her toes and the slime "
                            + "trembles with delight, before melting into a content puddle.", own(), opponent.own()
                )
            )

            Ptype.fgoblin -> c.write(
                owner, String.format(
                    "%s fetish goblin tackles %s goblin, pinning her arms behind her back. %s goblin ties up the other, using some spare ropes "
                            + "and bandage straps. The poor, bound herm is left completely immobile and vulnerable, but seems to be getting very aroused by her situation. The "
                            + "dominant fetish goblin takes her time getting her helpless opponent off, but eventually %s goblin is whimpering and twitching in orgasms",
                    own(), opponent.own(), own(), opponent.own()
                )
            )
        }
        opponent.remove()
    }

    override fun caught(c: Combat, captor: Character) {
        if (owner.human()) {
            c.write(
                captor, String.format(
                    "%s gets a short running start and delivers a powerful punt to your fetish goblin's dangling balls. The impact lifts the short "
                            + "hermaphrodite completely off the floor as you cringe in sympathetic pain. Apparently it's too much even for the masochistic creature, because she "
                            + "collapses and disappears.", captor.name
                )
            )
        } else {
            c.write(
                captor, String.format(
                    "You manage to catch %s's fetish goblin by her bondage gear, keeping her from escaping. It's not immediately clear how you can "
                            + "finish off the overstimulated goblin. There's not much you can do to her genitals beyond what she's already doing with her 'accessories.' You need "
                            + "a strong enough stimulus to push her over the threshold. You grab the end of the anal beads sticking out of her ass and yank them out all at once. "
                            + "The goblin shudders and the flow of liquid leaking out of her holes signals her orgasm before she vanishes.",
                    owner.name
                )
            )
        }
        remove()
    }

    override fun gender(): Trait {
        return Trait.herm
    }

    fun pickSkill(c: Combat, target: Character): PetSkill {
        val available = ArrayList<PetSkill>()
        available.add(PetSkill.IDLE)
        available.add(PetSkill.MASOCHISM)
        available.add(PetSkill.BONDAGE)
        if (owner.hasBalls && owner.arousal.percent() >= 80) {
            available.add(PetSkill.DENIAL)
        }
        if (target.hasPussy && target.isPantsless) {
            available.add(PetSkill.VIBRATOR)
        }
        if (target.isPantsless && target.canAct()) {
            available.add(PetSkill.ANALDILDO)
        }
        if (target.isPantsless && !target.canAct()) {
            available.add(PetSkill.ANALBEADS)
        }
        if (c.stance.prone(target)) {
            available.add(PetSkill.FACEFUCK)
        }
        return available.random()
    }

    enum class PetSkill {
        VIBRATOR,
        MASOCHISM,
        BONDAGE,
        DENIAL,
        FACEFUCK,
        ANALDILDO,
        ANALBEADS,
        IDLE,
    }
}

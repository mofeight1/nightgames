package pet

import characters.Character
import characters.Trait
import combat.Combat

abstract class Pet(
    private val name: String,
    val owner: Character,
    private val type: Ptype,
    protected var power: Double,
    protected var ac: Int
) {
    override fun toString(): String {
        return name
    }

    fun own(): String {
        return if (owner.human()) {
            "Your "
        } else {
            owner.name + "'s "
        }
    }

    abstract fun describe(): String?
    abstract fun act(c: Combat, target: Character)
    abstract fun vanquish(c: Combat, opponent: Pet)
    abstract fun caught(c: Combat, captor: Character)
    abstract fun gender(): Trait?
    fun remove() {
        owner.pet = null
    }

    fun type(): Ptype {
        return type
    }

    fun power(): Double {
        return power
    }

    fun ac(): Int {
        return ac
    }
}

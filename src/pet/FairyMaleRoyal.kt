package pet

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import global.Global
import stance.Neutral
import status.Bound
import status.Sensitive
import kotlin.math.roundToInt

class FairyMaleRoyal : Pet {
    private var mana: Int


    constructor(owner: Character) : super("faerie prince", owner, Ptype.fairymale, 5.0, 8) {
        mana = 50
    }

    constructor(owner: Character, power: Double, ac: Int) : super("faerie prince", owner, Ptype.fairymale, power, ac) {
        mana = 50
    }

    override fun describe(): String {
        return ""
    }

    override fun act(c: Combat, target: Character) {
        if (owner.human()) {
            if (mana < 10) {
                c.write(owner, own() + "faerie stops moving for a few seconds to recover his magical energy.")
                ac -= 2
                mana = 50
            } else {
                val available = ArrayList<attack>()
                available.add(attack.vibe)
                available.add(attack.empower)
                if (mana >= 20 && target.canAct()) {
                    available.add(attack.bind)
                }
                if (mana >= 12) {
                    available.add(attack.illusion)
                }
                if (mana >= 15 && c.stance.sub(owner)) {
                    available.add(attack.rescue)
                }
                if (mana >= 10) {
                    available.add(attack.sensitize)
                }
                when (getRandom(available)) {
                    attack.vibe -> {
                        if (target.isPantsless) {
                            if (target.hasPussy) {
                                c.write(
                                    owner,
                                    own() + "faerie conjures a vibrating orb and sticks it in " + target.name + "'s exposed pussy. "
                                            + "She bats the little prince away and fishes out the arcane sphere, which is already covered with her wetness."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie conjures a vibrating orb and presses it against the sensitive head of " + target.name + "'s penis."
                                )
                            }
                            target.pleasure(2 * power + Global.random(5), Anatomy.genitals, combat = c)
                        } else {
                            c.write(
                                owner,
                                own() + "faerie conjures a vibrating orb and tosses it down the front of " + target.name + "'s "
                                        + target.bottoms.last() + ". She scrambles to remove the magic vibrator as it stimulates her."
                            )
                            target.pleasure(2 * power, Anatomy.genitals, combat = c)
                        }

                        mana -= 5
                    }

                    attack.bind -> {
                        c.write(
                            owner,
                            own() + "faerie flies in front of " + target.name + " and starts casting a spell. She tries to grab him, "
                                    + "but as she reaches out, a magical seal binds her hands together."
                        )
                        target.add(Bound(target, (3 * power).roundToInt(), "Magic seal"))
                        mana -= 20
                    }

                    attack.illusion -> {
                        c.write(
                            owner,
                            own() + "faerie casts a spell and creates multiple illusory copies around himself."
                        )
                        mana -= 12
                        ac += 1
                    }

                    attack.rescue -> {
                        c.write(
                            owner,
                            own() + "faerie casts a spell and teleports you out of " + target.name + "'s grasp. You give the "
                                    + "royal pixie a quick thanks and square off against your opponent again."
                        )
                        mana -= 15
                        c.stance = Neutral(owner, target)
                    }

                    attack.sensitize -> {
                        c.write(
                            owner,
                            own() + "faerie flies forward and tags " + target.name + " on the clit, leaving a small glowing handprint. "
                                    + "He returns to you, pointing and chattering. You can't understand the language, but the message is clear: "
                                    + "\"Aim for the glowing spot.\""
                        )
                        mana -= 10
                        target.add(Sensitive(target, 6, Anatomy.genitals, 1.1))
                    }

                    attack.empower -> {
                        c.write(
                            owner,
                            own() + "faerie chants a magical incantation and touches your shoulder. You feel Arcane energy flow into you."
                        )
                        owner.buildMojo(30)
                        mana -= 8
                    }
                }
            }
        }
    }

    override fun vanquish(c: Combat, opponent: Pet) {
        when (opponent.type()) {
            Ptype.fairyfem -> c.write(
                owner,
                own() + "faerie prince chases " + opponent.own() + "faerie and tags her on the belly, leaving a glowing mark where he touched her. The faerie girl is immediately paralyzed by laughter, writhing as if "
                        + "being tickled and futilely grasping at the magic spot. The little prince smirks confidently and places identical marks on the helpless female's breasts and groin. The faerie girl writhes in inescapable "
                        + "pleasure as her erogenous zones are magically stimulated. She shrieks in orgasm and vanishes."
            )

            Ptype.fairymale -> c.write("")
            Ptype.impfem -> c.write(
                owner,
                own() + " faerie prince gets under " + opponent.own() + "imp's guard and fires a burst of magic to her comparatively large clitoris. The imp shrieks in pain and collapses before " +
                        "vanishing."
            )

            Ptype.impmale -> c.write("")
            Ptype.slime -> c.write(
                owner,
                own() + " fae glows as her surrounds himself with magic before charging at " + opponent.own() + "slime like a tiny missile. The slime splashes more than it explodes, it's pieces " +
                        "only shudder once before going still."
            )

            Ptype.fgoblin -> c.write(
                owner, String.format(
                    "%s faerie prince flies down between %s fetish goblin's legs, targeting the vibrator sticking out of her wet pussy. He gives it a shot of magic, which causes it to go into "
                            + "overdrive. The goblin's knees tremble as she's overwhelmed by the intense sensations from the enchanted toy. A stifled whimper escapes her as she orgasms and vanishes.",
                    own(),
                    opponent.own()
                )
            )
        }
        opponent.remove()
    }

    override fun caught(c: Combat, captor: Character) {
        if (captor.human()) {
            c.write(
                captor,
                "You snag " + own() + "faerie out of the air. She squirms in your hand, but has no chance of breaking free. You lick the fae from pussy to breasts and the little thing squeals " +
                        "in pleasure. The taste is surprisingly sweet and makes your tongue tingle. You continue lapping up the flavor until she climaxes and disappears."
            )
        } else {
            c.write(
                captor,
                captor.name + " snatches your faerie out of the air and flicks his royal jewels with her finger. You wince in sympathy as the tiny male curls up in the fetal position " +
                        "and vanishes."
            )
        }
        remove()
    }

    override fun gender(): Trait {
        return Trait.male
    }

    private enum class attack {
        illusion,
        empower,
        rescue,
        bind,
        sensitize,
        vibe
    }

    private fun getRandom(available: ArrayList<attack>): attack {
        return available.random()
    }
}


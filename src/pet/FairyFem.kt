package pet

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import global.Global

class FairyFem : Pet {
    constructor(owner: Character) : super("faerie", owner, Ptype.fairyfem, 2.0, 4)
    constructor(owner: Character, power: Double, ac: Int) : super("faerie", owner, Ptype.fairyfem, power, ac)

    override fun describe(): String {
        return ""
    }

    override fun act(c: Combat, target: Character) {
        if (target.human()) {
            when (Global.random(4)) {
                3 -> if (target.isPantsless) {
                    c.write(
                        owner,
                        own() + "faerie flies at you and kicks you in the balls. She doesn't have a lot of weight to put behind it, but it still hurts like hell"
                    )
                    target.pain(3.0 + 2 * Global.random(power.toInt()), Anatomy.genitals, c)
                } else {
                    c.write(owner, own() + "faerie flies around the edge of the fight looking for an opening")
                }

                2 -> if (c.stance.penetration(target)) {
                    c.write(owner, own() + "faerie flies around the edge of the fight looking for an opening")
                } else if (target.isPantsless) {
                    c.write(
                        owner,
                        own() + "faerie hugs your dick and rubs it with her entire body until you pull her off"
                    )
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                } else {
                    c.write(
                        owner,
                        own() + "faerie slips into your " + target.bottoms.last() +
                                " and plays with your penis until you manage to remove her."
                    )
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                }

                1 -> {
                    c.write(owner, own() + "faerie flies around " + owner.name + ", channelling energy into her.")
                    owner.buildMojo(20)
                }

                else -> {
                    c.write(
                        owner,
                        own() + "faerie rains magic energy on " + owner.name + ", restoring her strength"
                    )
                    owner.heal(power + Global.random(10), c)
                }
            }
        } else {
            when (Global.random(4)) {
                3 -> if (target.isTopless) {
                    c.write(
                        owner,
                        "Your faerie lands on " + target.name + "'s tit and plays with her sensitive nipple"
                    )
                    target.pleasure(3.0 + 2 * Global.random(power.toInt()), Anatomy.chest, combat = c)
                } else if (target.hasDick && !c.stance.penetration(target)) {
                    if (target.isPantsless) {
                        c.write(
                            owner,
                            own() + "faerie jumps on " + target.name + "'s exposed dick and rubs her tiny body against it."
                        )
                    } else {
                        c.write(
                            owner,
                            own() + "faerie darts into " + target.name + "'s " + target.bottoms.last() +
                                    " and teases her genitals until she is forceably removed."
                        )
                    }
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                } else {
                    c.write(owner, own() + "faerie flies around the edge of the fight looking for an opening")
                }

                2 -> run {
                    if (target.hasBalls && target.isPantsless) {
                        c.write(
                            owner,
                            own() + "faerie flies in from below and kicks " + target.name + " in the balls."
                        )
                        target.pain(3.0 + 2 * Global.random(power.toInt()), Anatomy.genitals, c)
                    } else {
                        c.write(owner, own() + "faerie flies around the edge of the fight looking for an opening")
                        return@run
                    }
                    c.write(
                        owner,
                        "Your faerie circles around your with a faint glow and kisses you on the cheek. You feel energy building inside you."
                    )
                    owner.buildMojo(20)
                }

                1 -> {
                    c.write(
                        owner,
                        "Your faerie circles around your with a faint glow and kisses you on the cheek. You feel energy building inside you."
                    )
                    owner.buildMojo(20)
                }

                else -> {
                    c.write(
                        owner,
                        "Your faerie flies next to your ear in speaks to you. The words aren't in English and you have no idea what she said, but somehow you feel your fatigue drain away."
                    )
                    owner.heal(power + Global.random(10), c)
                }
            }
        }
    }

    override fun vanquish(c: Combat, opponent: Pet) {
        when (opponent.type()) {
            Ptype.fairyfem -> c.write(
                owner,
                "The two faeries circle around each other vying for the upper hand. " + own() + "faerie catches " + opponent.own() + "faerie by the hips and starts to eat her out. " +
                        opponent.own() + " fae struggles to break free, but can barely keep flying as she rapidly reaches orgasm and vanishes."
            )

            Ptype.fairymale -> c.write(
                owner,
                "The faeries zip through the air like a couple dogfighting planes. " + opponent.own() + "male manages to catch the female's hands, but you see her foot shoot up decisively " +
                        "between his legs. The stricken male tumbles lazily toward the floor and vanishes in midair."
            )

            Ptype.impfem -> c.write(
                owner,
                own() + " faerie flies between the legs of " + opponent.own() + "imp, slipping both arms into the larger pussy. The imp trembles and falls to the floor as the faerie puts her entire " +
                        "upper body into pleasuring her. " + own() + "faerie is forcefully expelled by the imp's orgasm just before the imp vanishes."
            )

            Ptype.impmale -> c.write(
                owner,
                opponent.own() + "imp grabs at " + own() + "faerie, but the nimble sprite changes direction in midair and darts between the imp's legs. You can't see exactly what happens next, " +
                        "but the imp clutches his groin in pain and disappears."
            )

            Ptype.slime -> c.write(
                owner,
                own() + "fae glows with magic as it circles " + opponent.own() + "slime rapidly. The slime begins to tremble and slowly elongates into the shape of a crude phallis. It shudders " +
                        "violently and sprays liquid from the tip until the entire creature is a puddle on the floor."
            )

            Ptype.fgoblin -> c.write(
                owner, String.format(
                    "%s faerie flies down to the base of %s futanari goblin's cock. There's a brief magic glow as she breaks the band constricting the goblin's member. With the "
                            + "obstacle removed, it only takes a single playful stroke to set off the pent-up herm.",
                    own(),
                    opponent.own()
                )
            )
        }
        opponent.remove()
    }

    override fun caught(c: Combat, captor: Character) {
        if (captor.human()) {
            c.write(
                captor,
                "You snag " + own() + "faerie out of the air. She squirms in your hand, but has no chance of breaking free. You lick the fae from pussy to breasts and the little thing squeals " +
                        "in pleasure. The taste is surprisingly sweet and makes your tongue tingle. You continue lapping up the flavor until she climaxes and disappears."
            )
        } else {
            c.write(
                captor,
                captor.name + " manages to catch your faerie and starts pleasuring her with the tip of her finger. The sensitive fae clings to the probing finger desperately as she thrashes " +
                        "in ecstasy. Before you can do anything to help, your faerie vanishes in a burst of orgasmic magic."
            )
        }
        remove()
    }

    override fun gender(): Trait {
        return Trait.female
    }
}

package pet

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import global.Global
import status.Bound
import kotlin.math.roundToInt

class FairyFemRoyal(owner: Character, power: Double = 5.0, ac: Int = 8) :
    Pet("faerie princess", owner, Ptype.fairyfem, power, ac) {
    private var mana = 50
    private var capturespell = false

    override fun describe(): String {
        return ""
    }

    override fun act(c: Combat, target: Character) {
        if (target.human()) {
            if (capturespell) {
                if (mana < 10) {
                    c.write(
                        owner,
                        own() + "faerie shinks the totem until it fits easily into her hands. She dropkicks the magical genitals away as if she's "
                                + "bored with it and flies back to the fight. You wince in pain at that parting blow, but at least you're free from her "
                                + "unavoidable assault for now."
                    )
                    ac -= 4
                    target.pain(2 * power, Anatomy.genitals, c)
                    capturespell = false
                } else {
                    mana -= 10
                    when (Global.random(4)) {
                        2 -> {
                            if (target.hasBalls) {
                                c.write(
                                    owner,
                                    own() + "faerie pats the penis totem to make sure she has your attention, then jumps in the air, "
                                            + "landing on a testicle with all her weight. You groan as sudden pain shoots through your ball."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie draws a glowing target on the clitoris totem with her finger. She backs up a few "
                                            + "paces, then delivers a running kick to the same spot. You cringe and your knees tremble as an intense "
                                            + "pain strikes your most sensitive part."
                                )
                            }
                            target.pain(2.0 * Global.random(power.toInt()) + Global.random(5), Anatomy.genitals, c)
                        }

                        1 -> {
                            if (target.hasDick) {
                                c.write(
                                    owner,
                                    own() + "faerie wraps her arms around the comparatively large penis totem and rubs her body up and down "
                                            + "its shaft. The pleasurable rubbing arouses you, and there's nothing you can do to escape it."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie rubs her hands and body over the spherical totem, trying to stimulate as much of it as "
                                            + "possible. You try to keep your mind on the fight, but it's impossible to ignore the intense pleasure on your "
                                            + "love bud."
                                )
                            }
                            target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                        }

                        else -> {
                            if (target.hasDick) {
                                c.write(
                                    owner,
                                    own() + "faerie angles the penis totem toward herself and starts to lick the plump head. She focuses her "
                                            + "attention around the urethra, even sticking her tongue inside. You moan and instinctively jerk your hips back "
                                            + "from the intense sensation, but there's no escape from her oral attack."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie lavishes the clit totem with her tongue, kissing and licking as much of the surface area as "
                                            + "she can. You whimper in pleasure and try your hardest to endure the oral attack."
                                )
                            }
                            target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                        }
                    }
                }
            } else {
                if (mana < 10) {
                    c.write(
                        owner, own() + "faerie chants a few words and refreshes her fading aura, "
                                + "but she seems slightly diminished afterward."
                    )
                    mana = 50
                    power--
                } else {
                    val available = ArrayList<attack>()
                    available.add(attack.heal)
                    available.add(attack.tickle)
                    available.add(attack.growth)
                    if (mana >= 30 && target.canAct()) {
                        available.add(attack.bind)
                    }
                    if (!target.isPantsless) {
                        available.add(attack.strip)
                    }
                    if (mana >= 35) {
                        available.add(attack.capture)
                    }
                    when (getRandom(available)) {
                        attack.capture -> {
                            if (target.hasDick) {
                                c.write(
                                    owner,
                                    own() + "faerie touches you lightly on the groin before flying a safe distance away from the fight. "
                                            + "She casts an elaborate spell and a totem in the shape of a familiar penis and balls appears in front of her. She gives you "
                                            + "a wicked smile and runs a hand down the shaft. You shiver as your feel the touch on your own dick. Uh oh."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie touches you lightly on the groin before flying a safe distance away from the fight. "
                                            + "She casts an elaborate spell and a sphere about the same size she is appears in front of her. She gives "
                                            + "you a confident look plants a kiss on the sphere. You jump as the sensation is transmitted to your clit."
                                )
                            }
                            ac += 4
                            capturespell = true
                            mana -= 10
                        }

                        attack.heal -> {
                            c.write(
                                owner,
                                own() + "faerie flies in front of her and kisses her softly on the lips. " + target.name + " seems caught by "
                                        + "surprise, but also seems to perk up a bit."
                            )
                            mana -= 5
                            owner.tempt(5.0, combat = c)
                            owner.heal(5 * power, c)
                            owner.buildMojo((5 * power).roundToInt())
                        }

                        attack.tickle -> {
                            if (target.isNude) {
                                c.write(
                                    owner,
                                    own() + "faerie executes several hit and run attacks, darting in to tickle your sensitive parts with her hands and "
                                            + "wings. She always manages to escape before you can grab her."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie slips under your clothing and starts tickling your sensitive skin. You desperately try to trap "
                                            + "her in the clothes, but she escapes at the last second."
                                )
                            }
                            target.tempt(power + Global.random(10), combat = c)
                            target.weaken(power + Global.random(10), c)
                        }

                        attack.bind -> {
                            c.write(
                                owner,
                                own() + "faerie flies close to your face and blows you a kiss. You try to catch the nimble fae, but she "
                                        + "casts a spell on your outstretched hands, binding them in place."
                            )
                            target.add(Bound(target, (3 * power).roundToInt(), "Magic seal"))
                            mana -= 30
                        }

                        attack.strip -> {
                            c.write(
                                owner,
                                own() + "faerie points accusingly at your " + target.bottoms.last() + " and chatters at you. She seems to disapprove of you "
                                        + "covering yourself with clothing. She casts a spell before you can stop her, and the garment vanishes in a shower of flower "
                                        + "petals."
                            )
                            target.shred(Character.OUTFITBOTTOM)
                            mana -= 10
                        }

                        attack.growth -> {
                            c.write(
                                owner,
                                own() + "faerie stops for a moment and focues her power. She suddenly seems to grow slightly and "
                                        + "her magical aura becomes brighter."
                            )
                            mana -= 10
                            ac -= 1
                            power += 2
                        }
                    }
                }
            }
        } else {
            if (capturespell) {
                if (mana < 10) {
                    c.write(
                        owner,
                        own() + "faerie shinks the totem until it fits easily into her hands. She dropkicks the magical genitals away as if she's "
                                + "bored with it and flies back to the fight. " + target.name + " lets out a yelp and flinches. Apparently she felt that last kick too."
                    )
                    ac -= 4
                    target.pain(2 * power, Anatomy.genitals, c)
                    capturespell = false
                } else {
                    mana -= 10
                    when (Global.random(4)) {
                        2 -> {
                            if (target.hasBalls) {
                                c.write(
                                    owner,
                                    own() + "faerie pats the penis totem to make sure she has " + target.name + "'s attention, then jumps in the air, "
                                            + "landing on a testicle with all her weight. " + target.name + " groans and holds her groin. That looks painful."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie draws a glowing target on the clitoris totem with her finger. She backs up a few "
                                            + "paces, then delivers a running kick to the same spot. " + target.name + " lets out a pained whimper and you see "
                                            + "her eyes water."
                                )
                            }
                            target.pain(2.0 * Global.random(power.toInt()) + Global.random(5), Anatomy.genitals, c)
                        }

                        1 -> {
                            if (target.hasDick) {
                                c.write(
                                    owner,
                                    own() + "faerie wraps her arms around the comparatively large penis totem and rubs her body up and down "
                                            + "its shaft. " + target.name + " lets out a little moan and her knees tremble."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie rubs her hands and body over the spherical totem, trying to stimulate as much of it as "
                                            + "possible. " + target.name + " shivers and moans softly."
                                )
                            }
                            target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                        }

                        else -> {
                            if (target.hasDick) {
                                c.write(
                                    owner,
                                    own() + "faerie angles the penis totem toward herself and starts to lick the plump head. She focuses her "
                                            + "attention around the urethra, even sticking her tongue inside. " + target.name + " yelps and covers her dick, "
                                            + "but it makes no difference."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie lavishes the clit totem with her tongue, kissing and licking as much of the surface area as "
                                            + "she can. " + target.name + " moans at the strong sensation."
                                )
                            }
                            target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                        }
                    }
                }
            } else {
                if (mana < 10) {
                    c.write(
                        owner, own() + "faerie chants a few words and refreshes her fading aura, "
                                + "but she seems slightly diminished afterward."
                    )
                    mana = 50
                    power--
                } else {
                    val available = ArrayList<attack>()
                    available.add(attack.heal)
                    available.add(attack.tickle)
                    available.add(attack.growth)
                    if (mana >= 30 && target.canAct()) {
                        available.add(attack.bind)
                    }
                    if (!target.isPantsless) {
                        available.add(attack.strip)
                    }
                    if (mana >= 35) {
                        available.add(attack.capture)
                    }
                    when (getRandom(available)) {
                        attack.capture -> {
                            if (target.hasDick) {
                                c.write(
                                    owner,
                                    own() + "faerie touches " + target.name + " lightly on the groin before flying a safe distance away from the fight. "
                                            + "She casts an elaborate spell and a totem in the shape of a familiar penis and balls appears in front of her. She gives you "
                                            + "a wink and runs a hand down the shaft. " + target.name + " lets out a quiet whimper and covers her groin in surprise. Her eyes "
                                            + "widen as she realizes the situation she's in."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie touches " + target.name + " lightly on the groin before flying a safe distance away from the fight. "
                                            + "She casts an elaborate spell and a sphere about the same size she is appears in front of her. She gives "
                                            + "you a confident look and plants a kiss on the sphere. " + target.name + " lets out a quiet whimper and covers her groin in surprise. Her eyes "
                                            + "widen as she realizes the situation she's in."
                                )
                            }
                            ac += 4
                            capturespell = true
                            mana -= 10
                        }

                        attack.heal -> {
                            c.write(
                                owner,
                                own() + "faerie flies in front of you and gives you a soft kiss on the lips. You feel a magical surge of energy "
                                        + "wash away your exhaustion. Some of the energy seems to collect in your groin. An unavoidable side effect, you suppose."
                            )
                            mana -= 5
                            owner.tempt(5.0, combat = c)
                            owner.heal(5 * power, c)
                            owner.buildMojo((5 * power).roundToInt())
                        }

                        attack.tickle -> {
                            if (target.isNude) {
                                c.write(
                                    owner,
                                    own() + "faerie launches a tickling offensive against " + target.name + ", targeting her sensitive areas and flying "
                                            + "away before she can catch her."
                                )
                            } else {
                                c.write(
                                    owner,
                                    own() + "faerie disappears under " + target.name + "'s clothing. She yelps and scrambles to retrieve the little "
                                            + "fae."
                                )
                            }

                            target.tempt(power + Global.random(10), combat = c)
                            target.weaken(power + Global.random(10), c)
                        }

                        attack.bind -> {
                            c.write(
                                owner,
                                own() + "faerie flies in front of " + target.name + " and starts casting a spell. She tries to grab the little fae, "
                                        + "but as she reaches out, a magical seal binds her hands together."
                            )
                            target.add(Bound(target, (3 * power).roundToInt(), "Magic seal"))
                            mana -= 30
                        }

                        attack.strip -> {
                            c.write(
                                owner,
                                own() + "faerie points at " + target.name + "'s " + target.bottoms.last() + " and chatters at you in her indecipherable "
                                        + "language. You don't have a clue what she's trying to communicate until she gestures to her own naked body. Apparently, "
                                        + "she feels very strongly that genitals should be exposed. You nod in agreement. She obliterates the " + target.bottoms.last() + " "
                                        + "with a spell and gives you a tiny high five."
                            )
                            target.shred(Character.OUTFITBOTTOM)
                            mana -= 10
                        }

                        attack.growth -> {
                            c.write(
                                owner,
                                own() + "faerie stops for a moment and focues her power. She suddenly seems to grow slightly and "
                                        + "her magical aura becomes brighter."
                            )
                            mana -= 10
                            ac -= 1
                            power += 2
                        }
                    }
                }
            }
        }
    }

    override fun vanquish(c: Combat, opponent: Pet) {
        when (opponent.type()) {
            Ptype.fairyfem -> c.write(
                owner,
                "The two faeries circle around each other vying for the upper hand. " + own() + "faerie princess hits " + opponent.own() + "faerie with a quick magic flash, throwing the spell-struck girl "
                        + "into a horny daze. " + opponent.own() + "faerie gives the princess a lustful look and presents her needy pussy. " + own() + "fae teases the horny girl for a little while before suddenly shoving "
                        + "two fingers into the wet hole to finish her off."
            )

            Ptype.fairymale -> c.write(
                owner,
                "The faeries zip through the air like a couple dogfighting planes, with " + opponent.own() + "male chasing the female. Suddenly " + own() + "faerie princess turns and freezes her pursuer "
                        + "in midair with a burst of magic. The princess takes a long, deliberate wind-up before kicking the helpless male in the balls. The poor faerie boy can't even collapse until she releases "
                        + "the spell."
            )

            Ptype.impfem -> c.write(
                owner,
                own() + " faerie princess flies between the legs of " + opponent.own() + "imp, slipping both arms into the larger pussy. The imp trembles and falls to the floor as the faerie puts her entire " +
                        "upper body into pleasuring her. " + own() + "faerie is forcefully expelled by the imp's orgasm just before the imp vanishes."
            )

            Ptype.impmale -> c.write(
                owner,
                opponent.own() + "imp grabs at " + own() + "faerie princess, but the nimble sprite changes direction in midair and darts between the imp's legs. You can't see exactly what happens next, " +
                        "but the imp clutches his groin in pain and disappears."
            )

            Ptype.slime -> c.write(
                owner,
                own() + "fae glows with magic as it circles " + opponent.own() + "slime rapidly. The slime begins to tremble and slowly elongates into the shape of a crude phallis. It shudders " +
                        "violently and sprays liquid from the tip until the entire creature is a puddle on the floor."
            )

            Ptype.fgoblin -> c.write(
                owner, String.format(
                    "%s faerie princess flies down to the base of %s futanari goblin's cock. There's a brief magic glow as she breaks the band constricting the goblin's member. With the "
                            + "obstacle removed, it only takes a single playful stroke to set off the pent-up herm.",
                    own(),
                    opponent.own()
                )
            )
        }
        opponent.remove()
    }

    override fun caught(c: Combat, captor: Character) {
        if (captor.human()) {
            c.write(
                captor,
                "You snag " + own() + "faerie princess out of the air. She squirms in your hand, but has no chance of breaking free. You lick the fae from pussy to breasts and the little thing squeals " +
                        "in pleasure. The taste is surprisingly sweet and makes your tongue tingle. You continue lapping up the flavor until she climaxes and disappears."
            )
        } else {
            c.write(
                captor,
                captor.name + " manages to catch your faerie princess and starts pleasuring her with the tip of her finger. The sensitive fae clings to the probing finger desperately as she thrashes " +
                        "in ecstasy. Before you can do anything to help, your faerie vanishes in a burst of orgasmic magic."
            )
        }
        remove()
    }

    override fun gender(): Trait {
        return Trait.female
    }

    private enum class attack {
        capture,
        heal,
        tickle,
        bind,
        strip,
        growth,
    }

    private fun getRandom(available: ArrayList<attack>): attack {
        return available.random()
    }
}

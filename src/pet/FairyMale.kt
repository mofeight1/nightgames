package pet

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import global.Global
import status.Flatfooted
import status.Shield

class FairyMale : Pet {
    constructor(owner: Character) : super("faerie", owner, Ptype.fairymale, 2.0, 4)
    constructor(owner: Character, power: Double, ac: Int) : super("faerie", owner, Ptype.fairymale, power, ac)

    override fun describe(): String? {
        return null
    }

    override fun act(c: Combat, target: Character) {
        if (owner.human()) {
            when (Global.random(4)) {
                3 -> {
                    if (target.isPantsless) {
                        c.write(
                            owner,
                            "Your faerie flies between " + target.name + "'s legs and rubs her sensitive clit with both his tiny hands."
                        )
                    } else {
                        c.write(
                            owner,
                            "Your faerie crawls into " + target.name + "'s " + target.bottoms.last() +
                                    " and fondles her until she fishes him out."
                        )
                    }
                    target.pleasure(2.0 + 3 * Global.random(power.toInt()), Anatomy.genitals, combat = c)
                }

                2 -> {
                    c.write(
                        owner,
                        "Your faerie flies in front of you and creates a magic barrier, reducing the physical damage you take."
                    )
                    owner.add(Shield(owner, 2 * power), c)
                }

                1 -> if (Global.random(3) == 0) {
                    c.write(
                        owner,
                        "Your faerie flies behind " + target.name + " and creates a small flash of light, distracting her."
                    )
                    target.add(Flatfooted(target, 1), c)
                } else {
                    c.write(owner, "Your faerie flies at " + target.name + ", but she swats him away.")
                }

                else -> {
                    c.write(owner, "Your faerie lands on your shoulder and casts a spell, restoring your vitality.")
                    owner.heal(power + Global.random(10), c)
                }
            }
        }
    }

    override fun vanquish(c: Combat, opponent: Pet) {
        when (opponent.type()) {
            Ptype.fairyfem -> c.write(
                owner,
                own() + "faerie boy chases " + opponent.own() + "faerie and catches her from behind. He plays with the faerie girl's pussy and nipples while she's unable to retaliate. As she " +
                        "orgasms, she vanishes with a sparkle."
            )

            Ptype.fairymale -> c.write("")
            Ptype.impfem -> c.write(
                owner,
                own() + " faerie gets under " + opponent.own() + "imp's guard and punches her squarely in her comparatively large clitoris. The imp shrieks in pain and collapses before " +
                        "vanishing."
            )

            Ptype.impmale -> c.write("")
            Ptype.slime -> c.write(
                owner,
                own() + " fae glows as her surrounds himself with magic before charging at " + opponent.own() + "slime like a tiny missile. The slime splashes more than it explodes, it's pieces " +
                        "only shudder once before going still."
            )

            Ptype.fgoblin -> c.write(
                owner, String.format(
                    "%s faerie flies down between %s fetish goblin's legs, targeting the vibrator sticking out of her wet pussy. He gives it a shot of magic, which causes it to go into "
                            + "overdrive. The goblin's knees tremble as she's overwhelmed by the intense sensations from the enchanted toy. A stifled whimper escapes her as she orgasms and vanishes.",
                    own(),
                    opponent.own()
                )
            )
        }
        opponent.remove()
    }

    override fun caught(c: Combat, captor: Character) {
        if (captor.human()) {
            c.write(
                captor,
                "You snag " + own() + "faerie out of the air. She squirms in your hand, but has no chance of breaking free. You lick the fae from pussy to breasts and the little thing squeals " +
                        "in pleasure. The taste is surprisingly sweet and makes your tongue tingle. You continue lapping up the flavor until she climaxes and disappears."
            )
        } else {
            c.write(
                captor,
                captor.name + " snatches your faerie out of the air and flicks his little testicles with her finger. You wince in sympathy as the tiny male curls up in the fetal position " +
                        "and vanishes."
            )
        }
        remove()
    }

    override fun gender(): Trait {
        return Trait.male
    }
}

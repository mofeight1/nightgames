package pet

enum class Ptype {
    fairyfem,
    fairymale,
    impfem,
    impmale,
    slime,
    fgoblin,
}

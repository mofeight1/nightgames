package areas

import actions.Movement
import characters.Character
import combat.Encounter
import global.Match
import trap.Trap

class Area(var name: String, var description: String?, val areaId: Movement, drawHint: MapDrawHint, val match: Match?) {
    var adjacent: HashSet<Area> = HashSet()
    var shortcut: HashSet<Area> = HashSet()
    var jump: HashSet<Area> = HashSet()
    var present: ArrayList<Character> = ArrayList()
    var encounter: Encounter?
    var alarm: Boolean = false
    var env: ArrayList<Deployable> = ArrayList()
    var traces: ArrayList<Trace> = ArrayList()

    var drawHint: MapDrawHint
    var isPinged: Boolean = false

    constructor(name: String, description: String?, enumerator: Movement, match: Match?) : this(
        name,
        description,
        enumerator,
        MapDrawHint(),
        match
    )

    init {
        encounter = null
        this.drawHint = drawHint
    }

    fun link(adj: Area) {
        adjacent.add(adj)
    }

    fun shortcut(adj: Area) {
        shortcut.add(adj)
    }

    fun jump(adj: Area) {
        jump.add(adj)
    }

    val open get() = areaId == Movement.quad

    val corridor get() = areaId == Movement.bridge || areaId == Movement.tunnel

    val materials get() = areaId == Movement.workshop || areaId == Movement.storage

    val potions get() = areaId == Movement.lab || areaId == Movement.kitchen

    val bath get() = areaId == Movement.shower || areaId == Movement.pool

    val resupply get() = areaId == Movement.dorm || areaId == Movement.union

    val recharge get() = areaId == Movement.workshop

    val mana get() = areaId == Movement.la

    fun ping(listener: Character): Boolean {
        if (encounter != null) {
            return true
        }
        //if (open) return true
        if (present.any { listener.listenCheck(it.sneakValue()) }) return true
        /*for (c in present) {
            if (listener.listenCheck(c.sneakValue()) || open) {
                return true
            }
        }*/
        return alarm
    }

    fun enter(p: Character) {
        present.add(p)
        triggerDeployables(p)
    }

    fun triggerDeployables(p: Character) {
        val found = getEnv()
        found?.resolve(p)
    }

    fun encounter(player: Character): Boolean {
        if (encounter != null &&
            encounter!!.getPlayer(1) !== player &&
            encounter!!.getPlayer(2) !== player
            ) {
            player.intervene(
                encounter!!,
                encounter!!.getPlayer(1),
                encounter!!.getPlayer(2)
            )
        } else {
            val opponent = present.firstOrNull { it !== player }
            if (opponent != null) {
                encounter = Encounter(player, opponent, this)
                return encounter!!.spotCheck()
            }
        }
        return false
    }

    fun opportunity(target: Character, trap: Trap): Boolean {
        val opponent = present.firstOrNull { it !== target }
        if (opponent != null) {
            if (encounter == null && target.eligible(opponent) && opponent.eligible(target)) {
                encounter = Encounter(opponent, target, this)
                opponent.promptTrap(encounter!!, target, trap)
                return true
            }
        }
        remove(trap)
        return false
    }

    fun humanPresent(): Boolean {
        return present.any { it.human() }
    }

    fun exit(p: Character) {
        present.remove(p)
        if (encounter != null && (encounter!!.getPlayer(1) === p || encounter!!.getPlayer(2) === p)) {
            endEncounter()
        }
    }

    fun endEncounter() {
        encounter = null
    }

    fun getEnv(): Deployable? {
        // if (env.isEmpty()) {
        //     return null
        //}
        val highest = env.maxByOrNull { it.priority }
        return highest
        //if (highest != null && highest.priority >= 0)
        //    return highest
        // return env[0]
    }

    fun place(thing: Deployable) {
        env.add(thing)
    }

    fun remove(triggered: Deployable) {
        env.remove(triggered)
    }

    operator fun get(type: Deployable): Deployable? {
        return env.firstOrNull { it == type } ?:
        env.firstOrNull { it.javaClass == type.javaClass }
    }

    fun getDeployables(type: Deployable) = env.filter { it.javaClass == type.javaClass }

    fun hasStash(owner: Character): Boolean {
        return env.isNotEmpty() && env.any { it is NinjaStash && it.owner === owner }
    }

    fun hasTrap(trapper: Character): Boolean {
        return env.isNotEmpty() && env.any { it is Trap && it.owner === trapper }
    }

    override fun toString(): String {
        return name
    }

    fun leaveTrace(person: Character?, act: Movement?) {
        traces.add(Trace(person!!, act!!))
    }

    fun age() {
        if (encounter != null && encounter!!.isOver) {
            leaveTrace(encounter!!.getPlayer(1), Movement.fight)
            leaveTrace(encounter!!.getPlayer(2), Movement.fight)
            endEncounter()
        }
        for (trace in traces.toList()) {
            if (trace.age()) {
                traces.remove(trace)
            }
        }
    }
}

package areas

import characters.Character

interface Deployable {
    fun resolve(active: Character)
    var owner: Character?
    val priority: Int
}

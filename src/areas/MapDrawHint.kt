package areas

import java.awt.Rectangle

class MapDrawHint(
    var rect: Rectangle = Rectangle(0, 0, 0, 0),
    var label: String = "",
    var vertical: Boolean = false
)

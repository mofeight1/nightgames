package areas

import characters.Attribute
import characters.Character
import characters.State
import characters.Trait
import global.Flag
import global.Global
import items.Clothing
import items.Component
import items.Consumable
import items.Flask
import items.Loot
import items.Potion

class Cache(rank: Int) : Deployable {
    override var owner: Character? = null
    private var dc: Int
    private var test: Attribute? = null
    private var secondary: Attribute? = null
    private val reward = ArrayList<Loot?>()
    private val prizelist = HashMap<Loot?, Int>()

    init {
        dc = 10 * (rank + 1)
        when (Global.random(4)) {
            3 -> {
                test = Attribute.Seduction
                secondary = Attribute.Dark
            }

            2 -> {
                test = Attribute.Cunning
                secondary = Attribute.Science
            }

            1 -> {
                test = Attribute.Perception
                secondary = Attribute.Arcane
                dc = 13 + rank
            }

            else -> {
                test = Attribute.Power
                secondary = Attribute.Ki
            }
        }
    }

    override fun resolve(active: Character) {
        if (active.state == State.ready) {
            calcReward(active)
            if (active.has(Trait.treasureSeeker)) {
                dc = (dc * 0.75).toInt()
            }
            if (active.check(test!!, dc)) {
                if (active.human()) {
                    when (test) {
                        Attribute.Cunning -> Global.gui.message(
                            "<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like " +
                                    "the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. " +
                                    "It would probably be a problem to someone less clever. You quickly solve the puzzle and the box opens.<p>"
                        )

                        Attribute.Perception -> Global.gui.message(
                            "<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. You spot a carefully hidden, but " +
                                    "nonetheless out-of-place package. It's not sealed and the contents seem like they could be useful, so you help yourself.<p>"
                        )

                        Attribute.Power -> Global.gui.message(
                            "<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare " +
                                    "hands if you're strong enough. With a considerable amount of exertion, you manage to force the lid open. Hopefully the contents are worth it.<p>"
                        )

                        Attribute.Seduction -> Global.gui.message(
                            "<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the " +
                                    "box is a hole that's too dark to see into and barely big enough to stick a finger into. Fortunately, you're very good with your fingers. With a bit of poking " +
                                    "around, you feel some intricate mechanisms and maneuver them into place, allowing you to slide the top of the box off.<p>"
                        )

                        else -> {}
                    }
                }

                if (reward.isEmpty()) {
                    active.money += 200 * active.rank
                }
                for (i in reward) {
                    i!!.pickup(active)
                }
            } else if (active.check(secondary!!, dc - 5)) {
                if (active.human()) {
                    when (test) {
                        Attribute.Cunning -> Global.gui.message(
                            "<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like " +
                                    "the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. " +
                                    "Looks unnecessarily complicated. You pull off the touchscreen instead and short the connectors, causing the box to open so you can collect the contents.<p>"
                        )

                        Attribute.Perception -> Global.gui.message(
                            "<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. You summon a minor spirit to search the " +
                                    "area. It's not much good in a fight, but pretty decent at finding hidden objects. It leads you to a small hidden box of goodies.<p>"
                        )

                        Attribute.Power -> Global.gui.message(
                            "<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare " +
                                    "hands if you're strong enough. You're about to attempt to lift the cover, but then you notice the box is not quite as sturdy as it initially looked. You focus " +
                                    "your ki and strike the weakest point on the crate, which collapses the side. Hopefully no one's going to miss the box. You're more interested in what's inside.<p>"
                        )

                        Attribute.Seduction -> Global.gui.message(
                            "<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the " +
                                    "box is a hole that's too dark to see into and barely big enough to stick a finger into. However, the dark works to your advantage. You take control of the " +
                                    "shadows inside the box, giving them physical form and using them to force the box open. Time to see what's inside.<p>"
                        )

                        else -> {}
                    }
                }
                if (reward.isEmpty()) {
                    active.money += 200 * active.rank
                }
                for (i in reward) {
                    i!!.pickup(active)
                }
            } else {
                if (active.human()) {
                    when (test) {
                        Attribute.Cunning -> Global.gui.message(
                            "<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like " +
                                    "the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. " +
                                    "You do your best to decode it, but after a couple failed attempts, the screen turns off and stops responding.<p>"
                        )

                        Attribute.Perception -> Global.gui
                            .message("<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. Probably nothing.<p>")

                        Attribute.Power -> Global.gui.message(
                            "<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare " +
                                    "hands if you're strong enough. You try to pry the box open, but it's even heavier than it looks. You lose your grip and almost lose your fingertips as the lid " +
                                    "slams firmly into place. No way you're getting it open without a crowbar.<p>"
                        )

                        Attribute.Seduction -> Global.gui.message(
                            "<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the " +
                                    "box is a hole that's too dark to see into and barely big enough to stick a finger into. You feel around inside, but make no progress in opening it. Maybe " +
                                    "you'd have better luck with some precision tools.<p>"
                        )

                        else -> {}
                    }
                }
            }
            active.location.remove(this)
        }
    }

    fun calcReward(receiver: Character) {
        val rank = receiver.rank
        val value = (rank + 1) * 10
        if (rank >= 4) {
            if (Global.checkFlag(Flag.excalibur)) {
                prizelist[Component.PBlueprint] = 50
                prizelist[Component.PVibrator] = 50
                prizelist[Component.PHandle] = 50
            }
        }
        if (rank >= 3) {
            prizelist[Component.Capacitor] = 45
            prizelist[Component.SlimeCore] = 45
            prizelist[Component.Foxtail] = 45
            prizelist[Component.DragonBone] = 50
            if (receiver.human()) {
                if (!receiver.has(Clothing.shinobitop)) {
                    prizelist[Clothing.shinobitop] = 25
                }
                if (!receiver.has(Clothing.ninjapants)) {
                    prizelist[Clothing.ninjapants] = 25
                }
                if (!receiver.has(Clothing.halfcloak)) {
                    prizelist[Clothing.halfcloak] = 25
                }
                if (!receiver.has(Clothing.pouchlessbriefs)) {
                    prizelist[Clothing.pouchlessbriefs] = 30
                }
            }
        }
        if (rank >= 2) {
            prizelist[Consumable.Talisman] = 30
            prizelist[Consumable.FaeScroll] = 30
            prizelist[Consumable.powerband] = 30
            prizelist[Component.MSprayer] = 35
            prizelist[Component.Totem] = 25
            prizelist[Consumable.Handcuffs] = 20
            prizelist[Component.Semen] = 15
            prizelist[Potion.SuperEnergyDrink] = 10
            prizelist[Component.Titanium] = 35
            prizelist[Flask.PAphrodisiac] = 15
            prizelist[Component.Skin] = 35
            if (receiver.human()) {
                if (!receiver.has(Clothing.warpaint)) {
                    prizelist[Clothing.warpaint] = 35
                }
                if (!receiver.has(Clothing.gi)) {
                    prizelist[Clothing.gi] = 25
                }
                if (!receiver.has(Clothing.furcoat)) {
                    prizelist[Clothing.furcoat] = 20
                }
            }
        }
        if (rank >= 1) {
            if (receiver.human()) {
                if (!receiver.has(Clothing.cup)) {
                    prizelist[Clothing.cup] = 20
                }
                if (!receiver.has(Clothing.trenchcoat)) {
                    prizelist[Clothing.trenchcoat] = 25
                }
                if (!receiver.has(Clothing.kilt)) {
                    prizelist[Clothing.kilt] = 25
                }
            }
            prizelist[Component.HGMotor] = 20
            prizelist[Potion.Fox] = 10
            prizelist[Potion.Bull] = 10
            prizelist[Potion.Nymph] = 10
            prizelist[Potion.Cat] = 10
            if (Global.checkFlag(Flag.Kat)) {
                prizelist[Potion.Furlixir] = 15
            }
        }
        if (receiver.human()) {
            if (!receiver.has(Clothing.speedo)) {
                prizelist[Clothing.speedo] = 15
            }
            if (!receiver.has(Clothing.loincloth)) {
                prizelist[Clothing.loincloth] = 15
            }
        }
        if (!Global.checkFlag(Flag.CacheNoBasics)) {
            prizelist[Component.Sprayer] = 7
            prizelist[Flask.Aphrodisiac] = 5
            prizelist[Flask.DisSol] = 5
            prizelist[Flask.Sedative] = 14
            prizelist[Flask.SPotion] = 10
            if (receiver.human()) {
                if (!receiver.has(Clothing.latextop)) {
                    prizelist[Clothing.latextop] = 15
                }
                if (!receiver.has(Clothing.latexpants)) {
                    prizelist[Clothing.latexpants] = 15
                }
                if (!receiver.has(Clothing.windbreaker)) {
                    prizelist[Clothing.windbreaker] = 10
                }
            }
        }
        if (prizelist.isNotEmpty()) {
            var total = 0
            val bag = prizelist.keys.toTypedArray()
            var pick: Loot?
            while (total < value && reward.size < 4) {
                pick = bag[Global.random(bag.size)]
                reward.add(pick)
                total += prizelist[pick]!!
            }
        }
    }

    override val priority = 1
}

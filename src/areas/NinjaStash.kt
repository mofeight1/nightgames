package areas

import characters.Character
import global.Global
import items.Consumable
import items.Item

class NinjaStash(override var owner: Character?) : Deployable {
    private val contents = ArrayList<Item>()

    init {
        for (i in 0..3) {
            when (Global.random(3)) {
                0 -> {
                    contents.add(Consumable.needle)
                    contents.add(Consumable.needle)
                }

                1 -> contents.add(Consumable.smoke)
            }
        }
    }

    override fun resolve(active: Character) {
        if (owner === active && active.human()) {
            Global.gui
                .message("You have a carefully hidden stash of emergency supplies here. You can replace your clothes and collect the items if you need to.")
        }
    }

    fun collect(active: Character) {
        for (i in contents) {
            active.gain(i)
        }
        contents.clear()
    }

    override fun toString(): String {
        return "Ninja Stash"
    }

    override val priority = 0
}

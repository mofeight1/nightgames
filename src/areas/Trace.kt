package areas

import actions.Movement
import characters.Character
import global.Constants
import global.Global

class Trace(val person: Character, private val event: Movement) {
    private var time = 0

    fun age(): Boolean {
        time++
        return time >= 12
    }

    fun track(tracking: Int): String {
        val score = tracking - difficulty()
        return if (score > 0) {
            getIdentity(score) + getAct(score) + getTime(score)
        } else {
            ""
        }
    }

    private fun getIdentity(score: Int): String {
        return if (Global.random(score) >= 5) {
            person.name
        } else {
            "Someone"
        }
    }

    private fun getAct(score: Int): String {
        return if (Global.random(score) >= 5) {
            event.traceDesc
        } else {
            " was here "
        }
    }

    private fun getTime(score: Int): String {
        return if (Global.random(score) >= 10) {
            if (time > 6) {
                "more than 30 minutes ago."
            } else {
                "within the past " + (time * 5) + " minutes."
            }
        } else if (Global.random(score) >= 5) {
            if (time > 6) {
                "a while ago."
            } else {
                "recently."
            }
        } else {
            "at some point."
        }
    }

    private fun difficulty(): Int {
        return Constants.BASETRACKDIFF + time * 5
    }
}

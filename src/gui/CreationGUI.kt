package gui

import characters.Attribute
import characters.Player
import characters.Trait
import global.Constants
import global.Flag
import global.Global
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.GridLayout
import java.awt.Insets
import java.awt.SystemColor
import java.util.Hashtable
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.ButtonGroup
import javax.swing.JButton
import javax.swing.JCheckBox
import javax.swing.JComboBox
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JSeparator
import javax.swing.JSlider
import javax.swing.JTextField
import javax.swing.JTextPane
import javax.swing.ScrollPaneConstants
import javax.swing.SwingConstants
import kotlin.math.roundToInt

class CreationGUI(window: GUI) : JPanel() {
    private val powerfield: JTextField = JTextField()
    private val seductionfield: JTextField = JTextField()
    private val cunningfield: JTextField = JTextField()
    private val attPoints: JTextField = JTextField()
    private val namefield: JTextField = JTextField()
    private var stamina: Int
    private var arousal: Int
    private var mojo: Int
    private var speed: Int
    private val perception: Int
    private var powerpoints: Int
    private var seductionpoints: Int
    private var cunningpoints: Int
    private var remaining: Int
    private var money: Int
    private var startScale: Double
    private var xpScale: Double
    private var npcStartScale: Double
    private var npcXpScale: Double
    private val btnPowMin: JButton = JButton("-")
    private val btnPowPlus: JButton = JButton("+")
    private val btnSedMin: JButton = JButton("-")
    private val btnSedPlus: JButton = JButton("+")
    private val btnCunMin: JButton = JButton("-")
    private val btnCunPlus: JButton = JButton("+")
    private val textPane: JTextPane = JTextPane()
    private val scrollPane: JScrollPane = JScrollPane()
    private val difficulty: ButtonGroup = ButtonGroup()
    private val separator_1: JSeparator = JSeparator()
    private val verticalBox: Box = Box.createVerticalBox()
    private val lblStrength: JLabel = JLabel("Strength")
    private val StrengthBox: JComboBox<Trait> = JComboBox()
    private val StrengthDescription: JTextPane = JTextPane()
    private val separator_2: JSeparator = JSeparator()
    private val lblWeakness: JLabel = JLabel("Weakness")
    private val WeaknessBox: JComboBox<Trait> = JComboBox()
    private val WeaknessDescription: JTextPane = JTextPane()
    private val textColor: Color = Constants.PRIMARYTEXTCOLOR
    private val frameColor: Color = Constants.PRIMARYFRAMECOLOR
    private val backgroundColor: Color = Constants.PRIMARYBGCOLOR
    private val charPanel: JPanel = JPanel()
    private val bioPanel: JPanel = JPanel()
    private val lblSpeed: JLabel = JLabel("Speed")
    private val speedField: JTextField = JTextField()
    private val lblPerception: JLabel = JLabel("Perception")
    private val perceptionField: JTextField = JTextField()
    private val btnReset: JButton = JButton("Reset")
    private val lblMale: JLabel = JLabel("Male")
    private val lblStaminaMax: JLabel = JLabel("Stamina Max")
    private val lblStaminaValue: JTextField = JTextField("35")
    private val lblArousalMax: JLabel = JLabel("Arousal Max")
    private val lblArousalValue: JTextField = JTextField("50")
    private val lblMojoMax: JLabel = JLabel("Mojo Max")
    private val lblMojoValue: JTextField = JTextField("30")
    private val chckbxSkipTutorial: JCheckBox = JCheckBox("Skip Tutorial")
    private val lblModifiers: JLabel = JLabel("Game Modifiers")
    private val chckbxChallengeMode: JCheckBox = JCheckBox("Challenge Mode")
    private val chckbxShortMatches: JCheckBox = JCheckBox("Short Matches")
    private val chckbxGentle: JCheckBox = JCheckBox("Gentle Fights")
    private val playerStart: JSlider = JSlider(JSlider.HORIZONTAL, 1, 12, 1)
    private val playerScale: JSlider = JSlider(JSlider.HORIZONTAL, 1, 12, 1)
    private val npcStart: JSlider = JSlider(JSlider.HORIZONTAL, 1, 12, 1)
    private val npcScale: JSlider = JSlider(JSlider.HORIZONTAL, 1, 12, 1)
    private val dispplayerstart: JLabel
    private val dispplayerscale: JLabel
    private val dispnpcstart: JLabel
    private val dispnpcscale: JLabel
    private val panel_1: JPanel = JPanel()
    private val miscPanel: JPanel = JPanel()
    private val lblCreation: JLabel = JLabel("Character Creation")

    init {
        background = frameColor
        foreground = textColor
        layout = BoxLayout(this, BoxLayout.X_AXIS)

        startScale = 1.0
        xpScale = 1.0
        npcStartScale = 1.0
        npcXpScale = 1.0
        stamina = (Constants.STARTINGSTAMINA * startScale).roundToInt()
        arousal = (Constants.STARTINGAROUSAL * startScale).roundToInt()
        mojo = (Constants.STARTINGMOJO * startScale).roundToInt()
        speed = (Constants.STARTINGSPEED * startScale).roundToInt()
        perception = Constants.STARTINGPERCEPTION
        money = (Constants.STARTINGCASH * startScale).roundToInt()
        powerpoints = 0
        seductionpoints = 0
        cunningpoints = 0
        remaining = Constants.STARTINGSTATPOINTS

        add(scrollPane)
        scrollPane.alignmentX = LEFT_ALIGNMENT
        scrollPane.horizontalScrollBarPolicy = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER

        scrollPane.setViewportView(textPane)
        textPane.foreground = SystemColor.textText
        textPane.background = SystemColor.inactiveCaptionBorder
        textPane.font = Font("Georgia", Font.PLAIN, 18)
        textPane.isEditable = false
        textPane.text = Global.intro

        val creationPanel = JPanel()
        creationPanel.background = frameColor
        creationPanel.foreground = textColor

        add(creationPanel)
        creationPanel.layout = BorderLayout(0, 0)

        creationPanel.add(charPanel, BorderLayout.CENTER)
        charPanel.layout = BoxLayout(charPanel, BoxLayout.X_AXIS)

        charPanel.add(bioPanel)
        bioPanel.alignmentX = LEFT_ALIGNMENT
        bioPanel.alignmentY = TOP_ALIGNMENT
        bioPanel.background = backgroundColor
        val gbl_bioPanel = GridBagLayout()
        gbl_bioPanel.columnWidths = intArrayOf(84, 50, 60, 42, 0)
        gbl_bioPanel.rowHeights = intArrayOf(32, 32, 32, 32, 32, 32, 32, 32, 0, 0, 0)
        gbl_bioPanel.columnWeights = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0)
        gbl_bioPanel.rowWeights = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        bioPanel.layout = gbl_bioPanel

        val lblName = JLabel("Name")
        val gbc_lblName = GridBagConstraints()
        gbc_lblName.fill = GridBagConstraints.BOTH
        gbc_lblName.insets = Insets(0, 0, 5, 5)
        gbc_lblName.gridx = 0
        gbc_lblName.gridy = 0
        bioPanel.add(lblName, gbc_lblName)
        lblName.foreground = textColor
        lblName.font = Font("Georgia", Font.PLAIN, 18)

        val gbc_namefield = GridBagConstraints()
        gbc_namefield.gridwidth = 3
        gbc_namefield.fill = GridBagConstraints.BOTH
        gbc_namefield.insets = Insets(0, 0, 5, 5)
        gbc_namefield.gridx = 1
        gbc_namefield.gridy = 0
        bioPanel.add(namefield, gbc_namefield)
        namefield.font = Font("Georgia", Font.PLAIN, 18)
        namefield.columns = 10
        namefield.foreground = textColor

        val gbc_lblMale = GridBagConstraints()
        gbc_lblMale.fill = GridBagConstraints.BOTH
        gbc_lblMale.insets = Insets(0, 0, 5, 5)
        gbc_lblMale.gridx = 0
        gbc_lblMale.gridy = 1
        lblMale.font = Font("Georgia", Font.PLAIN, 18)
        lblMale.foreground = textColor
        bioPanel.add(lblMale, gbc_lblMale)

        val gbc_lblStaminaMax = GridBagConstraints()
        gbc_lblStaminaMax.fill = GridBagConstraints.BOTH
        gbc_lblStaminaMax.insets = Insets(0, 0, 5, 0)
        gbc_lblStaminaMax.gridx = 0
        gbc_lblStaminaMax.gridy = 2
        lblStaminaMax.font = Font("Georgia", Font.PLAIN, 18)
        lblStaminaMax.foreground = textColor
        bioPanel.add(lblStaminaMax, gbc_lblStaminaMax)

        lblStaminaValue.font = Font("Georgia", Font.PLAIN, 18)
        lblStaminaValue.isEditable = false
        lblStaminaValue.background = backgroundColor
        lblStaminaValue.foreground = textColor
        val gbc_lblStaminaValue = GridBagConstraints()
        gbc_lblStaminaValue.fill = GridBagConstraints.BOTH
        gbc_lblStaminaValue.insets = Insets(0, 0, 5, 5)
        gbc_lblStaminaValue.gridx = 1
        gbc_lblStaminaValue.gridy = 2
        bioPanel.add(lblStaminaValue, gbc_lblStaminaValue)

        val gbc_lblArousalMax = GridBagConstraints()
        gbc_lblArousalMax.fill = GridBagConstraints.BOTH
        gbc_lblArousalMax.insets = Insets(0, 0, 5, 5)
        gbc_lblArousalMax.gridx = 0
        gbc_lblArousalMax.gridy = 3
        lblArousalMax.font = Font("Georgia", Font.PLAIN, 18)
        lblArousalMax.foreground = textColor
        bioPanel.add(lblArousalMax, gbc_lblArousalMax)

        lblArousalValue.font = Font("Georgia", Font.PLAIN, 18)
        lblArousalValue.isEditable = false
        lblArousalValue.background = backgroundColor
        lblArousalValue.foreground = textColor
        val gbc_lblArousalValue = GridBagConstraints()
        gbc_lblArousalValue.fill = GridBagConstraints.BOTH
        gbc_lblArousalValue.insets = Insets(0, 0, 5, 5)
        gbc_lblArousalValue.gridx = 1
        gbc_lblArousalValue.gridy = 3
        bioPanel.add(lblArousalValue, gbc_lblArousalValue)

        lblMojoMax.foreground = textColor
        val gbc_lblMojoMax = GridBagConstraints()
        gbc_lblMojoMax.fill = GridBagConstraints.BOTH
        gbc_lblMojoMax.insets = Insets(0, 0, 5, 0)
        gbc_lblMojoMax.gridx = 0
        gbc_lblMojoMax.gridy = 4
        lblMojoMax.font = Font("Georgia", Font.PLAIN, 18)
        bioPanel.add(lblMojoMax, gbc_lblMojoMax)

        lblMojoValue.font = Font("Georgia", Font.PLAIN, 18)
        lblMojoValue.isEditable = false
        lblMojoValue.foreground = textColor
        lblMojoValue.background = backgroundColor
        val gbc_lblMojoValue = GridBagConstraints()
        gbc_lblMojoValue.fill = GridBagConstraints.BOTH
        gbc_lblMojoValue.insets = Insets(0, 0, 5, 5)
        gbc_lblMojoValue.gridx = 1
        gbc_lblMojoValue.gridy = 4
        bioPanel.add(lblMojoValue, gbc_lblMojoValue)

        val lblPower = JLabel("Power")
        val gbc_lblPower = GridBagConstraints()
        gbc_lblPower.fill = GridBagConstraints.BOTH
        gbc_lblPower.insets = Insets(0, 0, 5, 5)
        gbc_lblPower.gridx = 0
        gbc_lblPower.gridy = 5
        bioPanel.add(lblPower, gbc_lblPower)
        lblPower.foreground = textColor
        lblPower.font = Font("Georgia", Font.PLAIN, 18)

        powerfield.isEditable = false
        val gbc_powerfield = GridBagConstraints()
        gbc_powerfield.fill = GridBagConstraints.BOTH
        gbc_powerfield.insets = Insets(0, 0, 5, 5)
        gbc_powerfield.gridx = 1
        gbc_powerfield.gridy = 5
        bioPanel.add(powerfield, gbc_powerfield)
        powerfield.background = backgroundColor
        powerfield.foreground = textColor
        powerfield.font = Font("Georgia", Font.BOLD, 18)

        val gbc_btnPowPlus = GridBagConstraints()
        gbc_btnPowPlus.fill = GridBagConstraints.BOTH
        gbc_btnPowPlus.insets = Insets(0, 0, 5, 0)
        gbc_btnPowPlus.gridx = 2
        gbc_btnPowPlus.gridy = 5
        bioPanel.add(btnPowPlus, gbc_btnPowPlus)
        btnPowPlus.font = Font("Georgia", Font.PLAIN, 18)
        btnPowPlus.addActionListener {
            powerpoints++
            remaining--
            refresh()
        }

        val gbc_btnPowMin = GridBagConstraints()
        gbc_btnPowMin.fill = GridBagConstraints.BOTH
        gbc_btnPowMin.insets = Insets(0, 0, 5, 5)
        gbc_btnPowMin.gridx = 3
        gbc_btnPowMin.gridy = 5
        bioPanel.add(btnPowMin, gbc_btnPowMin)
        btnPowMin.font = Font("Georgia", Font.PLAIN, 18)
        btnPowMin.addActionListener {
            powerpoints--
            remaining++
            refresh()
        }

        val lblSeduction = JLabel("Seduction")
        val gbc_lblSeduction = GridBagConstraints()
        gbc_lblSeduction.fill = GridBagConstraints.BOTH
        gbc_lblSeduction.insets = Insets(0, 0, 5, 5)
        gbc_lblSeduction.gridx = 0
        gbc_lblSeduction.gridy = 6
        bioPanel.add(lblSeduction, gbc_lblSeduction)
        lblSeduction.foreground = textColor
        lblSeduction.font = Font("Georgia", Font.PLAIN, 18)

        val gbc_seductionfield = GridBagConstraints()
        gbc_seductionfield.fill = GridBagConstraints.BOTH
        gbc_seductionfield.insets = Insets(0, 0, 5, 5)
        gbc_seductionfield.gridx = 1
        gbc_seductionfield.gridy = 6
        bioPanel.add(seductionfield, gbc_seductionfield)
        seductionfield.background = backgroundColor
        seductionfield.foreground = textColor
        seductionfield.font = Font("Georgia", Font.BOLD, 18)
        seductionfield.isEditable = false
        seductionfield.columns = 2

        val gbc_btnSedPlus = GridBagConstraints()
        gbc_btnSedPlus.fill = GridBagConstraints.BOTH
        gbc_btnSedPlus.insets = Insets(0, 0, 5, 0)
        gbc_btnSedPlus.gridx = 2
        gbc_btnSedPlus.gridy = 6
        bioPanel.add(btnSedPlus, gbc_btnSedPlus)
        btnSedPlus.font = Font("Georgia", Font.PLAIN, 18)
        btnSedPlus.addActionListener {
            seductionpoints++
            remaining--
            refresh()
        }

        val gbc_btnSedMin = GridBagConstraints()
        gbc_btnSedMin.fill = GridBagConstraints.BOTH
        gbc_btnSedMin.insets = Insets(0, 0, 5, 5)
        gbc_btnSedMin.gridx = 3
        gbc_btnSedMin.gridy = 6
        bioPanel.add(btnSedMin, gbc_btnSedMin)
        btnSedMin.font = Font("Georgia", Font.PLAIN, 18)
        btnSedMin.addActionListener {
            seductionpoints--
            remaining++
            refresh()
        }

        val lblCunning = JLabel("Cunning")
        val gbc_lblCunning = GridBagConstraints()
        gbc_lblCunning.fill = GridBagConstraints.BOTH
        gbc_lblCunning.insets = Insets(0, 0, 5, 5)
        gbc_lblCunning.gridx = 0
        gbc_lblCunning.gridy = 7
        bioPanel.add(lblCunning, gbc_lblCunning)
        lblCunning.foreground = textColor
        lblCunning.font = Font("Georgia", Font.PLAIN, 18)

        val gbc_cunningfield = GridBagConstraints()
        gbc_cunningfield.fill = GridBagConstraints.BOTH
        gbc_cunningfield.insets = Insets(0, 0, 5, 5)
        gbc_cunningfield.gridx = 1
        gbc_cunningfield.gridy = 7
        bioPanel.add(cunningfield, gbc_cunningfield)
        cunningfield.background = backgroundColor
        cunningfield.foreground = textColor
        cunningfield.font = Font("Georgia", Font.BOLD, 18)
        cunningfield.isEditable = false
        cunningfield.columns = 2

        val gbc_btnCunPlus = GridBagConstraints()
        gbc_btnCunPlus.fill = GridBagConstraints.BOTH
        gbc_btnCunPlus.insets = Insets(0, 0, 5, 0)
        gbc_btnCunPlus.gridx = 2
        gbc_btnCunPlus.gridy = 7
        bioPanel.add(btnCunPlus, gbc_btnCunPlus)
        btnCunPlus.font = Font("Georgia", Font.PLAIN, 18)
        btnCunPlus.addActionListener {
            cunningpoints++
            remaining--
            refresh()
        }

        val gbc_btnCunMin = GridBagConstraints()
        gbc_btnCunMin.fill = GridBagConstraints.BOTH
        gbc_btnCunMin.insets = Insets(0, 0, 5, 5)
        gbc_btnCunMin.gridx = 3
        gbc_btnCunMin.gridy = 7
        bioPanel.add(btnCunMin, gbc_btnCunMin)
        btnCunMin.font = Font("Georgia", Font.PLAIN, 18)
        btnCunMin.addActionListener {
            cunningpoints--
            remaining++
            refresh()
        }

        val gbc_lblSpeed = GridBagConstraints()
        gbc_lblSpeed.fill = GridBagConstraints.BOTH
        gbc_lblSpeed.insets = Insets(0, 0, 5, 5)
        gbc_lblSpeed.gridx = 0
        gbc_lblSpeed.gridy = 8
        lblSpeed.font = Font("Georgia", Font.PLAIN, 18)
        lblSpeed.foreground = textColor
        bioPanel.add(lblSpeed, gbc_lblSpeed)

        speedField.isEditable = false
        val gbc_speedField = GridBagConstraints()
        gbc_speedField.fill = GridBagConstraints.BOTH
        gbc_speedField.insets = Insets(0, 0, 5, 5)
        gbc_speedField.gridx = 1
        gbc_speedField.gridy = 8
        speedField.font = Font("Georgia", Font.PLAIN, 18)
        speedField.background = backgroundColor
        speedField.foreground = textColor
        bioPanel.add(speedField, gbc_speedField)
        speedField.columns = 2

        val gbc_lblPerception = GridBagConstraints()
        gbc_lblPerception.fill = GridBagConstraints.BOTH
        gbc_lblPerception.insets = Insets(0, 0, 5, 0)
        gbc_lblPerception.gridx = 0
        gbc_lblPerception.gridy = 9
        lblPerception.font = Font("Georgia", Font.PLAIN, 18)
        lblPerception.foreground = textColor
        bioPanel.add(lblPerception, gbc_lblPerception)

        perceptionField.isEditable = false
        val gbc_perceptionField = GridBagConstraints()
        gbc_perceptionField.fill = GridBagConstraints.BOTH
        gbc_perceptionField.insets = Insets(0, 0, 0, 5)
        gbc_perceptionField.gridx = 1
        gbc_perceptionField.gridy = 9
        perceptionField.font = Font("Georgia", Font.PLAIN, 18)
        perceptionField.background = backgroundColor
        perceptionField.foreground = textColor
        bioPanel.add(perceptionField, gbc_perceptionField)
        perceptionField.columns = 2

        val lblAttributePoints = JLabel("Remaining")
        val gbc_lblAttributePoints = GridBagConstraints()
        gbc_lblAttributePoints.fill = GridBagConstraints.BOTH
        gbc_lblAttributePoints.insets = Insets(0, 0, 0, 5)
        gbc_lblAttributePoints.gridx = 0
        gbc_lblAttributePoints.gridy = 10
        bioPanel.add(lblAttributePoints, gbc_lblAttributePoints)
        lblAttributePoints.foreground = textColor
        lblAttributePoints.font = Font("Georgia", Font.BOLD, 18)

        val gbc_attPoints = GridBagConstraints()
        gbc_attPoints.fill = GridBagConstraints.BOTH
        gbc_attPoints.insets = Insets(0, 0, 0, 5)
        gbc_attPoints.gridx = 1
        gbc_attPoints.gridy = 10
        bioPanel.add(attPoints, gbc_attPoints)
        attPoints.foreground = textColor
        attPoints.background = backgroundColor
        attPoints.font = Font("Georgia", Font.BOLD, 18)
        attPoints.isEditable = false
        attPoints.columns = 2

        val gbc_btnReset = GridBagConstraints()
        gbc_btnReset.gridwidth = 2
        gbc_btnReset.fill = GridBagConstraints.BOTH
        gbc_btnReset.gridx = 2
        gbc_btnReset.gridy = 10
        bioPanel.add(btnReset, gbc_btnReset)
        btnReset.addActionListener { reset() }

        verticalBox.alignmentY = TOP_ALIGNMENT
        verticalBox.background = frameColor
        charPanel.add(verticalBox)

        lblStrength.horizontalAlignment = SwingConstants.CENTER
        lblStrength.font = Font("Georgia", Font.BOLD, 18)
        lblStrength.foreground = textColor
        lblStrength.background = backgroundColor
        verticalBox.add(lblStrength)

        StrengthBox.background = frameColor
        StrengthBox.foreground = textColor
        StrengthBox.addItem(Trait.romantic)
        StrengthBox.addItem(Trait.dexterous)
        StrengthBox.addItem(Trait.experienced)
        StrengthBox.addItem(Trait.wrassler)
        StrengthBox.addItem(Trait.streaker)
        StrengthBox.addItem(Trait.pimphand)
        StrengthBox.addItem(Trait.brassballs)
        StrengthBox.addItem(Trait.bramaster)
        StrengthBox.addItem(Trait.pantymaster)
        StrengthBox.addItem(Trait.toymaster)
        StrengthBox.addActionListener { StrengthDescription.text = (StrengthBox.selectedItem as Trait).desc }
        verticalBox.add(StrengthBox)

        StrengthDescription.background = backgroundColor
        StrengthDescription.preferredSize = Dimension(100, 100)
        StrengthDescription.isEditable = false
        StrengthDescription.font = Font("Georgia", Font.PLAIN, 18)
        StrengthDescription.text = (StrengthBox.selectedItem as Trait).desc
        verticalBox.add(StrengthDescription)

        verticalBox.add(separator_2)

        lblWeakness.horizontalAlignment = SwingConstants.CENTER
        lblWeakness.font = Font("Georgia", Font.BOLD, 18)
        lblWeakness.foreground = textColor
        lblWeakness.background = backgroundColor
        verticalBox.add(lblWeakness)

        WeaknessBox.background = frameColor
        WeaknessBox.foreground = textColor
        WeaknessBox.addItem(Trait.insatiable)
        WeaknessBox.addItem(Trait.imagination)
        WeaknessBox.addItem(Trait.achilles)
        WeaknessBox.addItem(Trait.ticklish)
        WeaknessBox.addItem(Trait.lickable)
        WeaknessBox.addItem(Trait.hairtrigger)
        WeaknessBox.addActionListener { WeaknessDescription.text = (WeaknessBox.selectedItem as Trait).desc }
        verticalBox.add(WeaknessBox)
        WeaknessDescription.background = backgroundColor
        WeaknessDescription.preferredSize = Dimension(100, 100)
        WeaknessDescription.isEditable = false
        WeaknessDescription.font = Font("Georgia", Font.PLAIN, 18)
        WeaknessDescription.text = (WeaknessBox.selectedItem as Trait).desc
        verticalBox.add(WeaknessDescription)

        verticalBox.add(separator_1)

        creationPanel.add(miscPanel, BorderLayout.SOUTH)

        val labelTable: Hashtable<Any?, Any?> = Hashtable()
        labelTable[1] = JLabel("0.25")
        labelTable[4] = JLabel("1")
        labelTable[8] = JLabel("2")
        labelTable[12] = JLabel("3")

        val lblplayerstart = JLabel("Player Starting Stats")
        playerStart.value = 4
        playerStart.minorTickSpacing = 1
        playerStart.labelTable = labelTable
        playerStart.paintTicks = true
        playerStart.paintLabels = true
        playerStart.addChangeListener { e ->
            val source = e.source as JSlider
            if (!source.valueIsAdjusting) {
                startScale = source.value * .25
                refresh()
            }
        }
        dispplayerstart = JLabel("x$startScale")

        val lblplayerscale = JLabel("Player Progression Speed")
        playerScale.value = 4
        playerScale.minorTickSpacing = 1
        playerScale.paintTicks = true
        playerScale.labelTable = labelTable
        playerScale.paintLabels = true
        playerScale.addChangeListener { e ->
            val source = e.source as JSlider
            if (!source.valueIsAdjusting) {
                xpScale = source.value * .25
                refresh()
            }
        }
        dispplayerscale = JLabel("x$xpScale")

        val lblnpcstart = JLabel("NPC Starting Stats")
        npcStart.value = 4
        npcStart.minorTickSpacing = 1
        npcStart.paintTicks = true
        npcStart.labelTable = labelTable
        npcStart.paintLabels = true
        npcStart.addChangeListener { e ->
            val source = e.source as JSlider
            if (!source.valueIsAdjusting) {
                npcStartScale = source.value * .25
                refresh()
            }
        }
        dispnpcstart = JLabel("x$npcStartScale")

        val lblnpcscale = JLabel("NPC Progression Speed")
        npcScale.value = 4
        npcScale.minorTickSpacing = 1
        npcScale.paintTicks = true
        npcScale.labelTable = labelTable
        npcScale.paintLabels = true
        npcScale.addChangeListener { e ->
            val source = e.source as JSlider
            if (!source.valueIsAdjusting) {
                npcXpScale = source.value * .25
                refresh()
            }
        }
        dispnpcscale = JLabel("x$npcXpScale")

        val scalePanel = Box.createVerticalBox()

        verticalBox.add(scalePanel)

        scalePanel.add(lblplayerstart)
        val pstartbox = Box.createHorizontalBox()
        pstartbox.add(playerStart)
        pstartbox.add(dispplayerstart)
        scalePanel.add(pstartbox)

        scalePanel.add(lblplayerscale)
        val pscalebox = Box.createHorizontalBox()
        pscalebox.add(playerScale)
        pscalebox.add(dispplayerscale)
        scalePanel.add(pscalebox)

        scalePanel.add(lblnpcstart)
        val npcstartbox = Box.createHorizontalBox()
        npcstartbox.add(npcStart)
        npcstartbox.add(dispnpcstart)
        scalePanel.add(npcstartbox)

        scalePanel.add(lblnpcscale)
        val npcscalebox = Box.createHorizontalBox()
        npcscalebox.add(npcScale)
        npcscalebox.add(dispnpcscale)
        scalePanel.add(npcscalebox)

        val optionsPanel = JPanel()
        miscPanel.add(optionsPanel)
        optionsPanel.foreground = textColor
        optionsPanel.layout = GridLayout(0, 2, 0, 0)

        lblModifiers.font = Font("Georgia", Font.BOLD, 18)
        optionsPanel.add(lblModifiers)
        
        optionsPanel.add(panel_1)

        chckbxChallengeMode.toolTipText = "Player will be assigned a handicap each match, but only get normal pay"
        chckbxChallengeMode.font = Font("Georgia", Font.PLAIN, 18)
        optionsPanel.add(chckbxChallengeMode)

        chckbxShortMatches.toolTipText =
            "Matches will last 2 hours instead of 3. Good for players who want to play in short session"
        chckbxShortMatches.font = Font("Georgia", Font.PLAIN, 18)
        optionsPanel.add(chckbxShortMatches)

        chckbxSkipTutorial.toolTipText = "Go directly to first match after character creation"
        chckbxSkipTutorial.font = Font("Georgia", Font.PLAIN, 18)
        optionsPanel.add(chckbxSkipTutorial)

        chckbxGentle.toolTipText = "Painful skills are disabled (not balanced)"
        chckbxGentle.font = Font("Georgia", Font.PLAIN, 18)
        optionsPanel.add(chckbxGentle)

        val btnStart = JButton("Start")
        miscPanel.add(btnStart)
        btnStart.font = Font("Georgia", Font.PLAIN, 27)

        lblCreation.font = Font("Georgia", Font.PLAIN, 26)
        creationPanel.add(lblCreation, BorderLayout.NORTH)
        btnStart.addActionListener {
            if (namefield.text.isNotEmpty()) {
                val one = Player(namefield.text.replace("\\s".toRegex(), ""))
                one[Attribute.Power] = (Constants.STARTINGPOWER * startScale).roundToInt() + powerpoints
                one[Attribute.Seduction] = (Constants.STARTINGSEDUCTION * startScale).roundToInt() + seductionpoints
                one[Attribute.Cunning] = (Constants.STARTINGCUNNING * startScale).roundToInt() + cunningpoints
                one[Attribute.Speed] = speed
                one.stamina.setMax(stamina)
                one.arousal.setMax(arousal)
                one.mojo.setMax(mojo)
                one.add(StrengthBox.selectedItem as Trait)
                one.add(WeaknessBox.selectedItem as Trait)
                one.money = money
                one.rest()
                if (chckbxChallengeMode.isSelected) {
                    Global.flag(Flag.challengemode)
                }
                if (chckbxShortMatches.isSelected) {
                    Global.flag(Flag.shortmatches)
                }
                if (chckbxGentle.isSelected) {
                    Global.flag(Flag.nopain)
                }
                Global.setCounter(Flag.PlayerBaseStrength, startScale)
                Global.setCounter(Flag.PlayerScaling, xpScale)
                Global.setCounter(Flag.NPCBaseStrength, npcStartScale)
                Global.setCounter(Flag.NPCScaling, npcXpScale)

                Global.newGame(one, !chckbxSkipTutorial.isSelected)
            }
        }

        window.disableOptions()
        window.pack()
        refresh()
    }

    private fun refresh() {
        dispplayerstart.text = "x$startScale"
        dispplayerscale.text = "x$xpScale"
        dispnpcstart.text = "x$npcStartScale"
        dispnpcscale.text = "x$npcXpScale"
        powerfield.text =
            "" + ((Constants.STARTINGPOWER * startScale).roundToInt() + powerpoints)
        seductionfield.text =
            "" + ((Constants.STARTINGSEDUCTION * startScale).roundToInt() + seductionpoints)
        cunningfield.text =
            "" + ((Constants.STARTINGCUNNING * startScale).roundToInt() + cunningpoints)
        stamina = (Constants.STARTINGSTAMINA * startScale).roundToInt()
        arousal = (Constants.STARTINGAROUSAL * startScale).roundToInt()
        mojo = (Constants.STARTINGMOJO * startScale).roundToInt()
        speed = (Constants.STARTINGSPEED * startScale).roundToInt()
        money = (Constants.STARTINGCASH * startScale).roundToInt()
        lblStaminaValue.text = "" + stamina
        lblArousalValue.text = "" + arousal
        lblMojoValue.text = "" + mojo
        speedField.text = "" + speed
        perceptionField.text = "" + perception

        attPoints.text = "" + remaining
        if (remaining <= 0) {
            btnPowPlus.isEnabled = false
            btnSedPlus.isEnabled = false
            btnCunPlus.isEnabled = false
        } else {
            btnPowPlus.isEnabled = true
            btnSedPlus.isEnabled = true
            btnCunPlus.isEnabled = true
        }
        if (powerpoints <= 0) {
            btnPowMin.isEnabled = false
        } else {
            btnPowMin.isEnabled = true
        }
        if (seductionpoints <= 0) {
            btnSedMin.isEnabled = false
        } else {
            btnSedMin.isEnabled = true
        }
        if (cunningpoints <= 0) {
            btnCunMin.isEnabled = false
        } else {
            btnCunMin.isEnabled = true
        }
    }

    fun reset() {
        powerpoints = 0
        seductionpoints = 0
        cunningpoints = 0
        remaining = Constants.STARTINGSTATPOINTS
        refresh()
    }
}

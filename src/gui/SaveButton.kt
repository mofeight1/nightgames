package gui

import global.SaveManager
import java.awt.Font

class SaveButton : KeyableButton("Save") {
    init {
        font = Font("Georgia", Font.PLAIN, 18)
        addActionListener { SaveManager.save(false) }
    }
}
package gui

import javax.swing.JButton

abstract class KeyableButton(private val text: String) : JButton(text) {
    fun call() {
        doClick()
    }

    open fun setHotkeyTextTo(string: String?) {
        setText("$text $string")
    }

    fun clearHotkeyText() {
        setText(text)
    }
}
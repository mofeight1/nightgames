package gui

import combat.Combat
import skills.Skill
import skills.Tactics
import java.awt.Color
import java.awt.Font

class SkillButton(action: Skill, combat: Combat) : KeyableButton(action.toString()) {
    protected var action: Skill
    protected var combat: Combat

    init {
        font = Font("Georgia", Font.PLAIN, 18)
        this.action = action
        toolTipText = action.describe()
        if (action.type() == Tactics.damage) {
            background = Color(150, 0, 0)
            foreground = Color.WHITE
        } else if (action.type() == Tactics.pleasure || action.type() == Tactics.fucking) {
            background = Color.PINK
        } else if (action.type() == Tactics.positioning) {
            background = Color(0, 100, 0)
            foreground = Color.WHITE
        } else if (action.type() == Tactics.stripping) {
            background = Color(0, 100, 0)
            foreground = Color.WHITE
        } else if (action.type() == Tactics.status) {
            background = Color.CYAN
        } else if (action.type() == Tactics.recovery || action.type() == Tactics.calming) {
            background = Color.WHITE
        } else if (action.type() == Tactics.summoning) {
            background = Color.YELLOW
        } else {
            background = Color(200, 200, 200)
        }
        this.combat = combat
        addActionListener { this@SkillButton.combat.act(this@SkillButton.action.user, this@SkillButton.action) }
    }
}

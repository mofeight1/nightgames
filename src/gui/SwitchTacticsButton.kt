package gui

import global.Global
import skills.TacticGroup
import skills.Tactics
import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import javax.swing.border.LineBorder

class SwitchTacticsButton(group: TacticGroup) : KeyableButton(group.name) {
    private val group: TacticGroup

    init {
        isBorderPainted = false
        isOpaque = true
        this.group = group
        var bgColor = Color(80, 220, 120)
        for (tactic in Tactics.entries) {
            if (tactic.group == group) {
                bgColor = tactic.color
                break
            }
        }

        background = bgColor
        minimumSize = Dimension(0, 20)
        foreground = foregroundColor(bgColor)
        font = Font("Georgia", 0, 16)
        border = LineBorder(background, 3)
        val nSkills = Global.gui.nSkillsForGroup(group)
        text = group.name + " [" + nSkills + "]"
        if (nSkills == 0 && group != TacticGroup.All) {
            isEnabled = false
            foreground = Color.WHITE
            background = background.darker()
        }

        addActionListener { Global.gui.switchTactics(this@SwitchTacticsButton.group) }
    }

    companion object {
        private fun foregroundColor(bgColor: Color): Color {
            val hsb = FloatArray(3)
            Color.RGBtoHSB(bgColor.red, bgColor.green, bgColor.red, hsb)
            return if (hsb[2] < .6) {
                Color.WHITE
            } else {
                Color.BLACK
            }
        }
    }
}
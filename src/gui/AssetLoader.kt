package gui

import java.net.URL

object AssetLoader {
    val isNewFolder: Boolean by lazy {
        this.javaClass.getResource("/res/assets/Title.png") != null
    }

    fun getResource(name: String): URL? {
        return if (isNewFolder) this.javaClass.getResource("/res/$name")
        else this.javaClass.getResource("/gui/$name")
    }
}
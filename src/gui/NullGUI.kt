package gui

import characters.Character
import characters.Player
import combat.Combat
import combat.CombatPhase
import combat.Encounter
import skills.Skill
import java.util.Observable

class NullGUI : GUI() {
    init {
        isVisible = false
    }

    override fun beginCombat(player: Character, enemy: Character): Combat {
        combat = Combat(player, enemy, ArrayList())
        combat!!.addObserver(this)
        combat!!.addWatcher(this)
        return combat!!
    }

    override fun populatePlayer(player: Player) {
    }

    override fun clearText() {
    }

    override fun message(text: String?) {
    }

    override fun clearCommand() {
    }

    fun addSkill(action: Skill?, com: Combat?) {
    }

    override var player: Player?
        get() = super.player
        set(player) {
        }

    override fun next(combat: Combat) {
    }

    fun promptFF(enc: Encounter) {
    }

    override fun promptAmbush(enc: Encounter, target: Character) {
    }

    override fun update(arg0: Observable, arg1: Any?) {
        if (combat == null) return

        if (combat!!.cPhase == CombatPhase.Upkeep || combat!!.cPhase == CombatPhase.End) {
            combat!!.next()
        }
    }

    override fun update() {
        if (combat == null) return

        if (combat!!.cPhase == CombatPhase.Upkeep || combat!!.cPhase == CombatPhase.End) {
            combat!!.next()
        }
    }

    override fun endCombat() {
        combat = null
    }
}

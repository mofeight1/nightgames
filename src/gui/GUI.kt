package gui

import actions.Action
import characters.Attribute
import characters.Character
import characters.Meter
import characters.Player
import characters.Trait
import combat.Combat
import combat.CombatPhase
import combat.Encounter
import combat.Encs
import daytime.Activity
import daytime.Store
import global.Constants
import global.Flag
import global.Global
import global.SaveManager
import global.Scheduler
import items.Clothing
import items.ClothingType
import items.Consumable
import items.Flask
import items.Item
import skills.Skill
import skills.TacticGroup
import trap.Trap
import utilities.EnumMap
import java.awt.BorderLayout
import java.awt.CardLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import java.awt.Graphics2D
import java.awt.GridLayout
import java.awt.Panel
import java.awt.Toolkit
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.awt.image.BufferedImage
import java.io.IOException
import java.time.LocalTime
import java.util.EnumMap
import java.util.Hashtable
import java.util.Observable
import java.util.Observer
import javax.imageio.ImageIO
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.ButtonGroup
import javax.swing.ImageIcon
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JProgressBar
import javax.swing.JRadioButton
import javax.swing.JScrollPane
import javax.swing.JSeparator
import javax.swing.JSlider
import javax.swing.JSpinner
import javax.swing.JTextPane
import javax.swing.JToggleButton
import javax.swing.Painter
import javax.swing.SpinnerNumberModel
import javax.swing.SwingConstants
import javax.swing.SwingUtilities
import javax.swing.UIDefaults
import javax.swing.UIManager
import javax.swing.border.CompoundBorder
import javax.swing.border.SoftBevelBorder
import javax.swing.text.BadLocationException
import javax.swing.text.DefaultCaret
import javax.swing.text.html.HTMLDocument
import javax.swing.text.html.HTMLEditorKit
import kotlin.math.max

open class GUI : JFrame(), Observer {
    protected var combat: Combat? = null
    open var player: Player? = null
    private var skills: EnumMap<TacticGroup, ArrayList<SkillButton>> = EnumMap()
    private var flasks: ArrayList<SkillButton> = ArrayList()
    private var potions: ArrayList<SkillButton> = ArrayList()
    private var demands: ArrayList<SkillButton> = ArrayList()
    private var bodytext: String? = null
    private var commandPanel: CommandPanel = CommandPanel(0, 0)
    private var groupBox: Box = Box.createHorizontalBox()
    private var textPane: JTextPane = JTextPane()
    private val titlePanel: Title
    private var stamina: JLabel = JLabel()
    private var arousal: JLabel = JLabel()
    private var mojo: JLabel = JLabel()
    private var lvl: JLabel = JLabel()
    private var xp: JLabel = JLabel()
    private var staminaBar: JProgressBar = JProgressBar()
    private var arousalBar: JProgressBar = JProgressBar()
    private var mojoBar: JProgressBar = JProgressBar()
    private var topPanel: JPanel = JPanel()
    private var loclbl: JLabel = JLabel()
    private var timelbl: JLabel = JLabel()
    private var cashlbl: JLabel = JLabel()
    private var daylbl: JLabel = JLabel()
    private var panel0: Panel = Panel()
    private lateinit var creation: CreationGUI
    private var textScroll: JScrollPane = JScrollPane()
    private var mainpanel: JPanel = JPanel()
    private var invbtn: JToggleButton = JToggleButton()
    private var stsbtn: JToggleButton = JToggleButton()
    private var inventoryPanel: JPanel = JPanel()
    private var statusPanel: JPanel = JPanel()
    private var centerPanel: JPanel = JPanel()
    private var clothesPanel: JPanel = JPanel()
    private var clothesdisplay: JLabel = JLabel()
    private var optionspanel: JPanel = JPanel()
    private var portraitPanel: JPanel = JPanel()
    private var portrait: JLabel = JLabel()
    private var sprite: JLabel = JLabel()
    private var map: JComponent = MapComponent()
    private var imgPanel: JLabel = JLabel()

    //Options
    private var rdnormal: JRadioButton = JRadioButton()
    private var rdhard: JRadioButton = JRadioButton()
    private var rdautosaveon: JRadioButton = JRadioButton()
    private var rdautosaveoff: JRadioButton = JRadioButton()
    private var rdpor1: JRadioButton = JRadioButton()
    private var rdpor2: JRadioButton = JRadioButton()
    private var rdpor3: JRadioButton = JRadioButton()
    private var rdimgon: JRadioButton = JRadioButton()
    private var rdimgoff: JRadioButton = JRadioButton()
    private var rdimgexact: JRadioButton = JRadioButton()
    private var rdstsimgon: JRadioButton = JRadioButton()
    private var rdstsimgoff: JRadioButton = JRadioButton()
    private var rdgentleon: JRadioButton = JRadioButton()
    private var rdgentleoff: JRadioButton = JRadioButton()
    private var fontspinner: JSpinner = JSpinner()
    private var rdrpt1: JRadioButton = JRadioButton()
    private var rdrpt2: JRadioButton = JRadioButton()
    private var rdcol1: JRadioButton = JRadioButton()
    private var rdcol2: JRadioButton = JRadioButton()
    private var playerScale: JSlider = JSlider()
    private var npcScale: JSlider = JSlider()

    //ColorScheme
    private var textColor: Color? = null
    private var frameColor: Color? = null
    private var backgroundColor: Color? = null
    private var width = 0
    private var height = 0
    var fontsize: Int = 0

    private var mntmNewgame = JMenuItem("New Game")
    private var mntmQuitMatch: JMenuItem = JMenuItem()
    private var mntmOptions: JMenuItem = JMenuItem()
    private var midPanel: JPanel = JPanel()
    private lateinit var closet: ClothesChangeGUI
    private var currentTactics: TacticGroup = TacticGroup.All
    private var lowres = false

    private var groupPanel: JPanel = JPanel()

    var barrierFilter: BufferedImage? = null
    var bondageFilter: BufferedImage? = null
    var boundFilter: BufferedImage? = null
    var bracedFilter: BufferedImage? = null
    var buzzedFilter: BufferedImage? = null
    var caffeineFilter: BufferedImage? = null
    var charmedFilter: BufferedImage? = null
    var dissolvingFilter: BufferedImage? = null
    var distractedFilter: BufferedImage? = null
    var drowsyFilter: BufferedImage? = null
    var enthralledFilter: BufferedImage? = null
    var feralFilter: BufferedImage? = null
    var fireformFilter: BufferedImage? = null
    var hornyFilter: BufferedImage? = null
    var hypersensitiveFilter: BufferedImage? = null
    var iceformFilter: BufferedImage? = null
    var masochismFilter: BufferedImage? = null
    var oiledFilter: BufferedImage? = null
    var shamedFilter: BufferedImage? = null
    var stoneformFilter: BufferedImage? = null
    var waterformFilter: BufferedImage? = null
    var windedFilter: BufferedImage? = null

    init {
        // If Nimbus is not available, you can set the GUI to another look and feel.
        for (info in UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus" == info.name) {
                UIManager.setLookAndFeel(info.className)
                break
            }
        }

        background = Color.GRAY

        // frame title
        title = "Night Games"

        // closing operation
        defaultCloseOperation = 3

        // resolution resolver
        if (Toolkit.getDefaultToolkit().screenSize.getHeight() >= 900) {
            height = 900
        } else {
            height = 720
            lowres = true
        }
        width = if (Toolkit.getDefaultToolkit().screenSize.getWidth() >= 1600) {
            1600
        } else {
            1024
        }
        preferredSize = Dimension(width, height)

        // center the window on the monitor
        val y = Toolkit.getDefaultToolkit().screenSize.getHeight().toInt()
        val x = Toolkit.getDefaultToolkit().screenSize.getWidth().toInt()

        val x1 = x / 2 - width / 2
        val y1 = y / 2 - height / 2

        val centerX = x1
        val centerY = y1

        setLocation(centerX, centerY)

        titlePanel = Title(this, width, height)
        contentPane.add(titlePanel)
        pack()
        isVisible = true
    }

    fun BuildGUI() {
        contentPane.remove(titlePanel)
        // Colors
        textColor = Constants.PRIMARYTEXTCOLOR
        frameColor = Constants.PRIMARYFRAMECOLOR
        backgroundColor = Constants.PRIMARYBGCOLOR


        // menu bar
        contentPane.layout = BoxLayout(contentPane, 1)
        val menuBar = JMenuBar()
        jMenuBar = menuBar
        mntmNewgame = JMenuItem("New Game")
        mntmNewgame.foreground = textColor
        mntmNewgame.background = frameColor
        mntmNewgame.horizontalAlignment = SwingConstants.CENTER
        mntmNewgame.addActionListener {
            val result = JOptionPane.showConfirmDialog(
                this@GUI,
                "Do you want to restart the game? You'll lose any unsaved progress.", "Start new game?",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE
            )
            if (result == JOptionPane.OK_OPTION) {
                Global.reset()
                clearCommand()
                createCharacter()
                isVisible = true
                pack()
            }
        }
        menuBar.add(mntmNewgame)
        val mntmLoad = JMenuItem("Load")
        mntmLoad.foreground = textColor
        mntmLoad.background = frameColor
        mntmLoad.horizontalAlignment = SwingConstants.CENTER
        menuBar.add(mntmLoad)
        mntmLoad.addActionListener { SaveManager.load() }
        // menu bar - options
        mntmOptions = JMenuItem("Options")
        mntmOptions.foreground = textColor
        mntmOptions.background = frameColor
        menuBar.add(mntmOptions)
        // options submenu creator
        optionspanel = JPanel()
        optionspanel.layout = GridLayout(0, 3, 0, 0)
        val lbldif = JLabel("AI Difficulty")
        val dif = ButtonGroup()
        rdnormal = JRadioButton("Normal")
        rdhard = JRadioButton("Hard")
        dif.add(rdnormal)
        dif.add(rdhard)
        optionspanel.add(lbldif)
        optionspanel.add(rdnormal)
        optionspanel.add(rdhard)

        val lblgen = JLabel("Combat Restrictions")
        val gentle = ButtonGroup()
        rdgentleoff = JRadioButton("Normal")
        rdgentleon = JRadioButton("Gentle(Not Balanced)")
        optionspanel.add(lblgen)
        optionspanel.add(rdgentleoff)
        optionspanel.add(rdgentleon)

        val lblauto = JLabel("Autosave (saves to auto.sav)")
        val auto = ButtonGroup()
        rdautosaveon = JRadioButton("On")
        rdautosaveoff = JRadioButton("Off")
        auto.add(rdautosaveon)
        auto.add(rdautosaveoff)
        optionspanel.add(lblauto)
        optionspanel.add(rdautosaveon)
        optionspanel.add(rdautosaveoff)

        val portype = ButtonGroup()
        rdpor1 = JRadioButton("Sprite")
        rdpor2 = JRadioButton("Portrait")
        rdpor3 = JRadioButton("None")
        portype.add(rdpor1)
        portype.add(rdpor2)
        portype.add(rdpor3)
        optionspanel.add(rdpor1)
        optionspanel.add(rdpor2)
        optionspanel.add(rdpor3)

        val image = ButtonGroup()
        rdimgon = JRadioButton("All Skill Images")
        rdimgoff = JRadioButton("No Skill Images ")
        rdimgexact = JRadioButton("Match Character")
        image.add(rdimgon)
        image.add(rdimgoff)
        image.add(rdimgexact)
        optionspanel.add(rdimgon)
        optionspanel.add(rdimgoff)
        optionspanel.add(rdimgexact)
        val lblstsimg = JLabel("Status Effects on Sprites")
        val stsimg = ButtonGroup()
        rdstsimgon = JRadioButton("On")
        rdstsimgoff = JRadioButton("Off")
        stsimg.add(rdstsimgon)
        stsimg.add(rdstsimgoff)
        optionspanel.add(lblstsimg)
        optionspanel.add(rdstsimgon)
        optionspanel.add(rdstsimgoff)
        val lblfnt = JLabel("Font Size")
        fontspinner = JSpinner(SpinnerNumberModel(6, 5, 16, 1))

        optionspanel.add(lblfnt)
        optionspanel.add(fontspinner)
        optionspanel.add(JPanel())
        val lblrpt = JLabel("Combat Reports")
        val reports = ButtonGroup()
        rdrpt1 = JRadioButton("On")
        rdrpt2 = JRadioButton("Off")
        reports.add(rdrpt1)
        reports.add(rdrpt2)
        optionspanel.add(lblrpt)
        optionspanel.add(rdrpt1)
        optionspanel.add(rdrpt2)

        val lblcol = JLabel("Color Set")
        val colors = ButtonGroup()
        rdcol1 = JRadioButton("Default")
        rdcol2 = JRadioButton("Dark")
        colors.add(rdcol1)
        colors.add(rdcol2)
        optionspanel.add(lblcol)
        optionspanel.add(rdcol1)
        optionspanel.add(rdcol2)

        val labelTable: Hashtable<Any?, Any?> = Hashtable<Any?, Any?>()
        labelTable[1] = JLabel("0.25")
        labelTable[4] = JLabel("1")
        labelTable[8] = JLabel("2")
        labelTable[12] = JLabel("3")

        val lblplayerscale = JLabel("Player Progression Speed")
        playerScale = JSlider(JSlider.HORIZONTAL, 1, 12, 1)
        playerScale.value = 4
        playerScale.minorTickSpacing = 1
        playerScale.paintTicks = true
        playerScale.labelTable = labelTable
        playerScale.paintLabels = true

        val lblnpcscale = JLabel("NPC Progression Speed")
        npcScale = JSlider(JSlider.HORIZONTAL, 1, 12, 1)
        npcScale.value = 4
        npcScale.minorTickSpacing = 1
        npcScale.paintTicks = true
        npcScale.labelTable = labelTable
        npcScale.paintLabels = true

        optionspanel.add(lblplayerscale)
        optionspanel.add(playerScale)
        optionspanel.add(JPanel())
        optionspanel.add(lblnpcscale)
        optionspanel.add(npcScale)
        optionspanel.add(JPanel())

        mntmOptions.addActionListener {
            if (Global.checkFlag(Flag.hardmode)) {
                rdhard.isSelected = true
            } else {
                rdnormal.isSelected = true
            }
            if (Global.checkFlag(Flag.nopain)) {
                rdgentleon.isSelected = true
            } else {
                rdgentleoff.isSelected = true
            }
            if (Global.checkFlag(Flag.autosave)) {
                rdautosaveon.isSelected = true
            } else {
                rdautosaveoff.isSelected = true
            }
            if (Global.checkFlag(Flag.noportraits)) {
                rdpor3.isSelected = true
            } else if (Global.checkFlag(Flag.portraits2)) {
                rdpor2.isSelected = true
            } else {
                rdpor1.isSelected = true
            }
            if (Global.checkFlag(Flag.noimage)) {
                rdimgoff.isSelected = true
            } else if (Global.checkFlag(Flag.exactimages)) {
                rdimgexact.isSelected = true
            } else {
                rdimgon.isSelected = true
            }
            if (Global.checkFlag(Flag.statussprites)) {
                rdstsimgon.isSelected = true
            } else {
                rdstsimgoff.isSelected = true
            }
            fontspinner.model.value = Math.round(Global.getValue(Flag.fontsize))
            if (Global.checkFlag(Flag.noReports)) {
                rdrpt2.isSelected = true
            } else {
                rdrpt1.isSelected = true
            }
            if (Global.checkFlag(Flag.altcolors)) {
                rdcol2.isSelected = true
            } else {
                rdcol1.isSelected = true
            }
            playerScale.value = Math.round(Global.getValue(Flag.PlayerScaling) / 0.25f).toInt()
            npcScale.value = Math.round(Global.getValue(Flag.NPCScaling) / 0.25f).toInt()
            val result = JOptionPane.showConfirmDialog(
                this,
                optionspanel,
                "Options",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.INFORMATION_MESSAGE
            )
            if (result == JOptionPane.OK_OPTION) {
                if (rdnormal.isSelected) {
                    Global.unflag(Flag.hardmode)
                } else {
                    Global.flag(Flag.hardmode)
                }
                if (rdgentleon.isSelected) {
                    Global.flag(Flag.nopain)
                } else {
                    Global.unflag(Flag.nopain)
                }
                if (rdautosaveoff.isSelected) {
                    Global.unflag(Flag.autosave)
                } else {
                    Global.flag(Flag.autosave)
                }
                if (rdpor2.isSelected) {
                    Global.flag(Flag.portraits2)
                    Global.unflag(Flag.noportraits)
                } else if (rdpor3.isSelected) {
                    Global.flag(Flag.noportraits)
                    Global.unflag(Flag.portraits2)
                    showNone()
                } else {
                    Global.unflag(Flag.portraits2)
                    Global.unflag(Flag.noportraits)
                }
                if (rdimgon.isSelected) {
                    Global.unflag(Flag.noimage)
                    Global.unflag(Flag.exactimages)
                } else if (rdimgexact.isSelected) {
                    Global.unflag(Flag.noimage)
                    Global.flag(Flag.exactimages)
                } else {
                    Global.flag(Flag.noimage)
                    Global.unflag(Flag.exactimages)
                }
                if (rdstsimgon.isSelected) {
                    Global.flag(Flag.statussprites)
                } else {
                    Global.unflag(Flag.statussprites)
                }
                Global.setCounter(Flag.fontsize, (fontspinner.model.value as Int).toDouble())
                fontsize = fontspinner.model.value as Int
                if (rdrpt2.isSelected) {
                    Global.flag(Flag.noReports)
                } else {
                    Global.unflag(Flag.noReports)
                }
                Global.setCounter(Flag.PlayerScaling, playerScale.value * 0.25)
                Global.setCounter(Flag.NPCScaling, npcScale.value * 0.25)
                if (rdcol2.isSelected) {
                    Global.flag(Flag.altcolors)
                    textColor = Constants.ALTTEXTCOLOR
                    frameColor = Constants.ALTFRAMECOLOR
                    backgroundColor = Constants.ALTBGCOLOR
                    refreshColors()
                } else {
                    Global.unflag(Flag.altcolors)
                    textColor = Constants.PRIMARYTEXTCOLOR
                    frameColor = Constants.PRIMARYFRAMECOLOR
                    backgroundColor = Constants.PRIMARYBGCOLOR
                    refreshColors()
                }
            }
        }


        // menu bar - credits
        val mntmCredits = JMenuItem("Credits")
        mntmCredits.foreground = textColor
        mntmCredits.background = backgroundColor
        menuBar.add(mntmCredits)


        // menu bar - supporters
        val mntmPatrons = JMenuItem("Supporters")
        mntmPatrons.foreground = textColor
        mntmPatrons.background = backgroundColor
        menuBar.add(mntmPatrons)


        // menu bar - quit match
        mntmQuitMatch = JMenuItem("Quit Match")
        mntmQuitMatch.isEnabled = false
        mntmQuitMatch.foreground = textColor
        mntmQuitMatch.background = backgroundColor
        mntmQuitMatch.addActionListener {
            val result = JOptionPane.showConfirmDialog(
                this@GUI,
                "Do you want to quit for the night? Your opponents will continue to fight and gain exp.",
                "Retire early?",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.INFORMATION_MESSAGE
            )
            if (result == JOptionPane.OK_OPTION) {
                Scheduler.match!!.quit()
            }
        }
        menuBar.add(mntmQuitMatch)
        mntmCredits.addActionListener {
            JOptionPane
                .showMessageDialog(
                    this@GUI,
                    """Design:
    The Silver Bard

Art:
    AimlessArt,
    Fujin Hitokiri,
    Sky Relyks,
    Phanaxial

Development:
    The Silver Bard,
    Nergantre,
    dndw,
    Jos,
    InvalidCharacter,
    GruntledDev

Additional Characters by:
    dndw,
    Napebaf,
    J

Writing:
    The Silver Bard,
    dndw,
    Onyxdime,
    Legion,
    GruntedDev,
    Jotor,
    Vica,
    Sakruff,
    KajunKrust,
    Marcus Koen,
    CK,
    Rex,
    DJ Writer,
    WT Cash,
    KVernik,
    banana,
    Anon64,
    Napebaf,
    Whitman,
    Game Salamander,
    Guardian Soul, 
    Scrublord,
    Dosrev,
    Fapworthy Diction,
    GM Kcalb,
    J"""
                )
        }
        mntmPatrons.addActionListener {
            JOptionPane
                .showMessageDialog(
                    this@GUI,
                    """The Darklord, Jay S, Richard Penner, bob bobberson, John Aspuria, Brownell Combs, Unsung, Jaryd, Logan Aragon, Evil_kiwi,
 Game Salamander, CJ, Maisson Phelps, Tyler, Somedude, Kasev, Quintin Blake, Mark Owen, Banana, Ramon Diaz, S Fly,
 Akira Reinheart, Josh, CorrupterAxel, Rob Brance, Jasmine, Kou Mao, BabylonKing, SillyGordo, Bob Jones, PureKaos,
 fake name, Drew Smith, derek floyd, John Doe, Shames90210, Jacob Ware, KitsunesGame, Mistor Games, Maurice Spicer,
 Hahn Ackles, Rolfo, onenine, Glenn McGurrin, pebismani, Piotr Januszewski, Portent, Marcus Doyle, Somira, dawewq,
 Sylvester Marciniak, TheMuseBattle, Phil Bill, Trisx, Mark Griffin, John Bridges, XanderPre Development, Brian Hobbs,
 Aaron, Aghjil, Heliosbull, Grnd91, Bryan Gillis, Jason Joers, Jeff Hungerford, Matthew McConnell, Gul4sch, turkeymann,
 Kaerea Shine, Chad Rast, MB, Musasy, Dan, M, JC, Rune Johansen, MacBuchi, AimlessArt, Tenten1010, Apoc326, Justin,
 Quite Shallow, D, Bavari, TheApplebane, Drew, Jahkari Robinson, finn, Reginald C Dawson, Shettern, Derp Derpersson,
 Horizon, Rasmus Victor Johnsson, Zivich, Dustin Parrish, Kevin, Brian Sheppard, Chit, Gia Trang, Anomander Rake,
 Nicholas Diamante, BearPerson, Some Guy, Azione, Cameron, Dave McCl, griege, DragonPiggy, Bob Fruman, The HiGHDOLM@STER,
 Jacob Grindstaff, Bannhammer, Pezantri, Ilsig, Foridin, karumi kaizen, james ramage, Ryan Krause, Zeke, Arcadion,
 Derek Cheong, Dylan Devenny, John Echols, Hippoblue64, Ken Arthur, Clap Hands, Tai Parry, Christopher Nicdao, Rick Fox,
 Evan Haukenfrers, Aureate_Folly, AeronGreva, cs, Aaron Hand, Jeremy Drakeford, A D, JaB, kaze31, Deveroux02, Flintfire,
 kemious, Ker, RHR, SirSquidjr, 64bitrobot, K, not a name, Makido205, Brian Timothy, Invert_87, ph, Big Daddy Cool, SocomSeal,
 Alex Lopez, Joshua Smith, Kirby, dommsdominickchung, Loser4Ever, Kagith, Michael Pechawer, William Reynolds, Thomas Gray,
 brian, Justin Smith, Apestativ, enemygunman, Cheap Shot, Aederrex, Jamie, Toast Baron, tjd, TMoD36, James (Burnthemage),
 Cameron Kulper, Jake Stirkul, dumbname, Richard Arsonault, Laughterglow, Bartholemew Case, Dvspuss, Kaine, Qianwei,
 Andrew Findlay, Zach, AzureShade, Yuan, nobody, MePe, Necro Vee, Znewbie R, Cloudmoon, Alan, Job, nic611, Nathan Lindy,
 Nforte, narori sayo, katro, SD, Jim Stauble, Nick Pazer, Scott Throgmorton, Apprentice Tinkerer, LordJerle, 0, Jayson tremblay,
 EzarTek, Roger Faux, Taco Joe, Smith, Johan Hedlund, Justin Boley, """
                )
        }


        // panel layouts

        // mainpanel - everything is contained within it
        mainpanel = JPanel()
        mainpanel.background = backgroundColor
        contentPane.add(mainpanel)
        mainpanel.layout = BoxLayout(mainpanel, 1)


        // panel0 - invisible, only handles topPanel
        panel0 = Panel()
        mainpanel.add(panel0)
        panel0.layout = BoxLayout(panel0, 0)

        // topPanel - invisible, menus
        topPanel = JPanel()
        panel0.add(topPanel)
        topPanel.layout = BoxLayout(topPanel, BoxLayout.X_AXIS)

        // centerPanel - invisible, body of GUI
        centerPanel = JPanel()
        mainpanel.add(centerPanel)
        centerPanel.layout = BorderLayout(0, 0)

        // inventoryPanel - visible, items and clothing
        inventoryPanel = JPanel()
        inventoryPanel.layout = BorderLayout(0, 0)
        inventoryPanel.background = frameColor
        inventoryPanel.maximumSize = Dimension(100, height)

        // statusPanel - visible, character status
        statusPanel = JPanel()
        statusPanel.layout = BoxLayout(statusPanel, BoxLayout.X_AXIS)
        statusPanel.background = frameColor

        //clothesPanel - visible, contains clothing diagram
        clothesPanel = JPanel()


        // portraitPanel
        portraitPanel = JPanel()
        centerPanel.add(portraitPanel, BorderLayout.WEST)
        portraitPanel.layout = CardLayout()
        portraitPanel.background = backgroundColor
        portrait = JLabel("")
        portrait.verticalAlignment = SwingConstants.TOP
        sprite = JLabel("")
        sprite.verticalAlignment = SwingConstants.BOTTOM
        portraitPanel.add(portrait, USE_PORTRAIT)
        portraitPanel.add(sprite, USE_SPRITE)

        map = MapComponent()
        portraitPanel.add(map, USE_MAP)
        val logo = JLabel("")
        portraitPanel.add(logo, USE_NONE)
        val logoimg: BufferedImage? = ImageIO.read(AssetLoader.getResource("assets/logo.png"))
        if (logoimg != null) {
            logo.icon = ImageIcon(logoimg)
        }
        map.preferredSize = Dimension(300, 385)

        midPanel = JPanel()
        midPanel.layout = CardLayout()
        centerPanel.add(midPanel, BorderLayout.CENTER)


        // imgPanel - visible, contains imgLabel
        imgPanel = JLabel()
        imgPanel.layout = BorderLayout(2, 2)
        imgPanel.background = backgroundColor


        // textScroll
        textScroll = JScrollPane()
        textScroll.isOpaque = false
        //		imgPanel.add(textScroll, BorderLayout.CENTER);		
        midPanel.add(textScroll, USE_MAIN_TEXT_UI)


        // textPane
        textPane = JTextPane()
        val caret = textPane.caret as DefaultCaret
        caret.updatePolicy = DefaultCaret.ALWAYS_UPDATE
        textPane.foreground = textColor
        textPane.background = backgroundColor
        textPane.preferredSize = Dimension(width, 400)
        textPane.isEditable = false
        textPane.contentType = "text/html"
        bodytext = ""
        textScroll.setViewportView(textPane)


        fontsize = Math.round(Global.getValue(Flag.fontsize)).toInt()

        val debug = JButton("Debug")
        debug.addActionListener { Scheduler.unpause() }


        // commandPanel - visible, contains the player's command buttons
        groupBox = Box.createHorizontalBox()
        groupBox.background = frameColor
        groupBox.border = CompoundBorder()
        groupPanel = JPanel()
        mainpanel.add(groupPanel)

        commandPanel = CommandPanel(width, height)
        groupPanel.add(groupBox)
        groupPanel.add(commandPanel)
        commandPanel.background = frameColor

        groupPanel.background = frameColor
        groupPanel.border = CompoundBorder()
        skills = EnumMap()
        currentTactics = TacticGroup.All
        flasks = ArrayList()
        potions = ArrayList()
        demands = ArrayList()
        clearCommand()
        createCharacter()
        isVisible = true
        pack()

        val panel = contentPane as JPanel
        panel.isFocusable = true
        panel.addKeyListener(object : KeyListener {
            /**
             * Space bar will select the first option, unless they are in the default actions list.
             */
            override fun keyReleased(e: KeyEvent) {
                val button = commandPanel.getButtonForHotkey(e.keyChar)
                button?.call()
            }

            override fun keyTyped(e: KeyEvent) {}

            override fun keyPressed(e: KeyEvent) {}
        })
    }

    fun showMap() {
        map.preferredSize = Dimension(300, 385)
        val portraitLayout = portraitPanel.layout as CardLayout
        portraitLayout.show(portraitPanel, USE_MAP)
        portraitPanel.repaint()
        portraitPanel.revalidate()
        //		pack();
    }

    fun showPortrait() {
        val portraitLayout = portraitPanel.layout as CardLayout
        if (Global.checkFlag(Flag.portraits2)) {
            portrait.preferredSize = Dimension(300, 385)
            portraitLayout.show(portraitPanel, USE_PORTRAIT)
        } else {
            sprite.preferredSize = Dimension(300, 385)
            portraitLayout.show(portraitPanel, USE_SPRITE)
        }
        portraitPanel.repaint()
        portraitPanel.revalidate()
        //		pack();
    }

    fun showNone() {
        val portraitLayout = portraitPanel.layout as CardLayout
        portraitLayout.show(portraitPanel, USE_NONE)
        midPanel.repaint()
        midPanel.revalidate()
        //		pack();
    }

    // image loader
    fun displayImage(path: String, artist: String?) {
        if (Global.checkFlag(Flag.noimage)) {
            return
        }
        val imgsrc = AssetLoader.getResource(
            "assets/$path"
        )

        if (imgsrc != null) {
            message("<img src=\"$imgsrc\" title=\"$artist\"/>")
        }
    }

    fun resetPortrait() {
        portrait.icon = null
    }


    // portrait loader
    fun loadPortrait(player: Character, enemy: Character) {
        if (!Global.checkFlag(Flag.noportraits)) {
            var imagepath: String? = null
            var spriteImg: BufferedImage? = null
            if (!player.human()) {
                imagepath = player.portrait
                spriteImg = player.spriteImage
            } else if (!enemy.human()) {
                imagepath = enemy.portrait
                spriteImg = enemy.spriteImage
            }
            sprite.icon = null
            if (imagepath != null && Global.checkFlag(Flag.portraits2)) {
                var face: BufferedImage? = null
                try {
                    val imgurl = AssetLoader.getResource(imagepath)
                    if (imgurl != null) {
                        face = ImageIO.read(imgurl)
                    }
                } catch (_: IOException) {
                }
                if (face != null) {
                    portrait.icon = null
                    portrait.icon = ImageIcon(face)
                    val portraitLayout = portraitPanel.layout as CardLayout
                    portraitLayout.show(portraitPanel, USE_PORTRAIT)
                }
            } else if (spriteImg != null && !Global.checkFlag(Flag.portraits2)) {
                sprite.icon = ImageIcon(spriteImg)
                val portraitLayout = portraitPanel.layout as CardLayout
                portraitLayout.show(portraitPanel, USE_SPRITE)
            } else if (imagepath != null) {
                var face: BufferedImage? = null
                try {
                    val imgurl = AssetLoader.getResource(imagepath)
                    if (imgurl != null) {
                        face = ImageIO.read(imgurl)
                    }
                } catch (_: IOException) {
                }
                if (face != null) {
                    portrait.icon = null
                    portrait.icon = ImageIcon(face)
                    val portraitLayout = portraitPanel.layout as CardLayout
                    portraitLayout.show(portraitPanel, USE_PORTRAIT)
                }
            }
            //			centerPanel.revalidate();
//			portraitPanel.revalidate();
            portraitPanel.repaint()
        }
    }

    fun loadTwoPortraits(first: Character, second: Character) {
        if (!Global.checkFlag(Flag.noportraits)) {
            var imagepath: String? = null
            var spriteImg: BufferedImage? = null
            var imagepath2: String? = null
            var spriteImg2: BufferedImage? = null
            var sprites: BufferedImage? = null
            if (!first.human()) {
                imagepath = first.portrait
                spriteImg = first.spriteImage
            }
            if (!second.human()) {
                imagepath2 = second.portrait
                spriteImg2 = second.spriteImage
            }
            sprite.icon = null
            if ((spriteImg != null || spriteImg2 != null) && !Global.checkFlag(Flag.portraits2)) {
                if (spriteImg != null && spriteImg2 != null) {
                    sprites = BufferedImage(spriteImg.width, spriteImg.height, spriteImg.type)
                    val g = sprites.createGraphics()
                    g.drawImage(spriteImg, -60, 0, null)
                    g.drawImage(spriteImg2, 60, 0, null)
                } else if (spriteImg != null) {
                    sprites = spriteImg
                } else {
                    sprites = spriteImg2
                }
                sprite.icon = ImageIcon(sprites)
                val portraitLayout = portraitPanel.layout as CardLayout
                portraitLayout.show(portraitPanel, USE_SPRITE)
            } else if (imagepath != null || imagepath2 != null) {
                var face: BufferedImage? = null
                var face2: BufferedImage? = null
                var faces: BufferedImage? = null
                var height = 0
                var width = 0
                if (imagepath != null) {
                    try {
                        val imgurl = AssetLoader.getResource(imagepath)
                        if (imgurl != null) {
                            face = ImageIO.read(imgurl)
                        }
                    } catch (_: IOException) {
                    }
                    if (face != null) {
                        height += face.height
                        width = face.width
                    }
                }
                if (imagepath2 != null) {
                    try {
                        val imgurl2 = AssetLoader.getResource(imagepath2)
                        if (imgurl2 != null) {
                            face2 = ImageIO.read(imgurl2)
                        }
                    } catch (_: IOException) {
                    }
                    if (face2 != null) {
                        height += face2.height
                        width = max(face2.width, width)
                    }
                }
                if (face != null && face2 != null) {
                    faces = BufferedImage(width, height, face.type)
                    val g = faces.createGraphics()
                    g.drawImage(face, 0, 0, null)
                    g.drawImage(face2, 0, face.height, null)
                } else if (face != null) {
                    faces = face
                } else if (face2 != null) {
                    faces = face2
                }
                if (faces != null) {
                    portrait.icon = null
                    portrait.icon = ImageIcon(faces)
                    val portraitLayout = portraitPanel.layout as CardLayout
                    portraitLayout.show(portraitPanel, USE_PORTRAIT)
                }
            }
            //			centerPanel.revalidate();
//			portraitPanel.revalidate();
            portraitPanel.repaint()
        }
    }

    fun loadStatusFilters(NPC: Character): BufferedImage {
        val filter = BufferedImage(350, 540, BufferedImage.TYPE_INT_ARGB)
        val g = filter.createGraphics()
        val statuses = NPC.listStatus()
        if (statuses.contains("Feral")) {
            if (feralFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Feral_Status.png"
                    )
                    if (imgurl != null) {
                        feralFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(feralFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(feralFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Dissolving")) {
            if (dissolvingFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Dissolving_Status.png"
                    )
                    if (imgurl != null) {
                        dissolvingFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(dissolvingFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(dissolvingFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Oiled")) {
            if (oiledFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Oiled_Status.png"
                    )
                    if (imgurl != null) {
                        oiledFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(oiledFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(oiledFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Drowsy")) {
            if (drowsyFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Drowsy_Status.png"
                    )
                    if (imgurl != null) {
                        drowsyFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(drowsyFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(drowsyFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Caffeinated")) {
            if (caffeineFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Caffeine_Status.png"
                    )
                    if (imgurl != null) {
                        caffeineFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(caffeineFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(caffeineFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Buzzed")) {
            if (buzzedFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Buzzed_Status.png"
                    )
                    if (imgurl != null) {
                        buzzedFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(buzzedFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(buzzedFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Bondage")) {
            if (bondageFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Bondage_Status.png"
                    )
                    if (imgurl != null) {
                        bondageFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(bondageFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(bondageFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Masochism")) {
            if (masochismFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Masochism_Status.png"
                    )
                    if (imgurl != null) {
                        masochismFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(masochismFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(masochismFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Shamed")) {
            if (shamedFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Shamed_Status.png"
                    )
                    if (imgurl != null) {
                        shamedFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(shamedFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(shamedFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Charmed")) {
            if (charmedFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Charmed_Status.png"
                    )
                    if (imgurl != null) {
                        charmedFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(charmedFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(charmedFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Enthralled")) {
            if (enthralledFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Enthralled_Status.png"
                    )
                    if (imgurl != null) {
                        enthralledFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(enthralledFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(enthralledFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Hypersensitive")) {
            if (hypersensitiveFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Hypersensitive_Status.png"
                    )
                    if (imgurl != null) {
                        hypersensitiveFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(hypersensitiveFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(hypersensitiveFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Shield")) {
            if (barrierFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Barrier_Status.png"
                    )
                    if (imgurl != null) {
                        barrierFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(barrierFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(barrierFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Braced")) {
            if (bracedFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Braced_Status.png"
                    )
                    if (imgurl != null) {
                        bracedFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(bracedFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(bracedFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Tied Up")) {
            if (boundFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Bound_Status.png"
                    )
                    if (imgurl != null) {
                        boundFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(boundFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(boundFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Flat-Footed")) {
            if (distractedFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Distracted_Status.png"
                    )
                    if (imgurl != null) {
                        distractedFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(distractedFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(distractedFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Winded")) {
            if (windedFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Winded_Status.png"
                    )
                    if (imgurl != null) {
                        windedFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(windedFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(windedFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Horny")) {
            if (hornyFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/Horny_Status.png"
                    )
                    if (imgurl != null) {
                        hornyFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(hornyFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(hornyFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Stone Form")) {
            if (stoneformFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/StoneForm_Status.png"
                    )
                    if (imgurl != null) {
                        stoneformFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(stoneformFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(stoneformFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Water Form")) {
            if (waterformFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/WaterForm_Status.png"
                    )
                    if (imgurl != null) {
                        waterformFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(waterformFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(waterformFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Ice Form")) {
            if (iceformFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/IceForm_Status.png"
                    )
                    if (imgurl != null) {
                        iceformFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(iceformFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(iceformFilter, 0, 0, null)
            }
        }
        if (statuses.contains("Fire Form")) {
            if (fireformFilter == null) {
                try {
                    val imgurl = AssetLoader.getResource(
                        "assets/FireForm_Status.png"
                    )
                    if (imgurl != null) {
                        fireformFilter = ImageIO.read(imgurl)
                    }
                    g.drawImage(fireformFilter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(fireformFilter, 0, 0, null)
            }
        }
        return filter
    }

    // combat GUI
    open fun beginCombat(player: Character, enemy: Character): Combat {
        combat = Scheduler.match!!.buildCombat(player, enemy)
        combat!!.addObserver(this)
        combat!!.addWatcher(this)
        resetPortrait()
        if (!Global.checkFlag(Flag.noportraits)) {
            loadPortrait(player, enemy)

            showPortrait()
        }
        return combat!!
    }

    fun watchCombat(c: Combat) {
        combat = c
        combat!!.addWatcher(this)
        combat!!.addObserver(this)
    }

    // getLabelString - handles all the meters (bars)
    fun getLabelString(meter: Meter): String {
        return meter.current.toString() + "/" + meter.max
    }

    open fun populatePlayer(player: Player) {
        if (Global.checkFlag(Flag.altcolors)) {
            textColor = Constants.ALTTEXTCOLOR
            frameColor = Constants.ALTFRAMECOLOR
            backgroundColor = Constants.ALTBGCOLOR
        }
        contentPane.remove(creation)
        mntmNewgame.isEnabled = true
        contentPane.add(mainpanel)
        contentPane.validate()
        this.player = player
        player.gui = this
        player.addObserver(this)
        val meter = JPanel()
        meter.background = frameColor
        topPanel.add(meter)
        meter.layout = GridLayout(0, 3, 0, 0)
        fontsize = Math.round(Global.getValue(Flag.fontsize)).toInt()

        stamina = JLabel("Stamina: " + getLabelString(player.stamina))
        stamina.font = Font("Georgia", 1, 15)
        stamina.horizontalAlignment = 0
        stamina.foreground = Color(200, 14, 12)
        stamina.toolTipText =
            "Stamina represents your endurance and ability to keep fighting. If it drops to zero, you'll be temporarily stunned."
        meter.add(stamina)
        arousal = JLabel("Arousal: " + getLabelString(player.arousal))
        arousal.font = Font("Georgia", 1, 15)
        arousal.horizontalAlignment = 0
        arousal.foreground = Color(254, 1, 107)
        arousal.toolTipText =
            "Arousal is raised when your opponent pleasures or seduces you. If it hits your max, you'll orgasm and lose the fight."
        meter.add(arousal)
        mojo = JLabel("Mojo: " + getLabelString(player.mojo))
        mojo.font = Font("Georgia", 1, 15)
        mojo.horizontalAlignment = 0
        mojo.foreground = Color(51, 153, 255)
        mojo.toolTipText =
            "Mojo is the abstract representation of your momentum and style. It increases with normal techniques and is used to power special moves"
        meter.add(mojo)

        staminaBar = JProgressBar()
        val staminaOver = UIDefaults()
        staminaOver.putAll(UIManager.getLookAndFeelDefaults())
        staminaOver["ProgressBar[Enabled+Finished].foregroundPainter"] = MyPainter(Color(200, 14, 12, 180))
        staminaOver["ProgressBar[Enabled].foregroundPainter"] = MyPainter(Color(200, 14, 12, 180))
        staminaOver["ProgressBar[Enabled+Indeterminate].foregroundPainter"] = MyPainter(Color(200, 14, 12, 180))
        staminaBar.putClientProperty("Nimbus.Overrides", staminaOver)
        staminaBar.border = SoftBevelBorder(1, null, null, null, null)
        meter.add(staminaBar)
        staminaBar.maximum = player.stamina.max
        staminaBar.value = player.stamina.current

        arousalBar = JProgressBar()
        arousalBar.border = SoftBevelBorder(1, null, null, null, null)
        arousalBar.foreground = Color(254, 1, 107)
        val arousalOver = UIDefaults()
        arousalOver.putAll(UIManager.getLookAndFeelDefaults())
        arousalOver["ProgressBar[Enabled+Finished].foregroundPainter"] = MyPainter(Color(254, 1, 107, 180))
        arousalOver["ProgressBar[Enabled].foregroundPainter"] = MyPainter(Color(254, 1, 107, 180))
        arousalOver["ProgressBar[Enabled+Indeterminate].foregroundPainter"] = MyPainter(Color(254, 1, 107, 180))
        arousalBar.putClientProperty("Nimbus.Overrides", arousalOver)
        meter.add(arousalBar)
        arousalBar.maximum = player.arousal.max
        arousalBar.value = player.arousal.current

        mojoBar = JProgressBar()
        mojoBar.border = SoftBevelBorder(1, null, null, null, null)
        mojoBar.foreground = Color(51, 153, 255)
        mojoBar.background = frameColor
        val mojoOver = UIDefaults()
        mojoOver.putAll(UIManager.getLookAndFeelDefaults())
        mojoOver["ProgressBar[Enabled+Finished].foregroundPainter"] = MyPainter(Color(51, 153, 255, 180))
        mojoOver["ProgressBar[Enabled].foregroundPainter"] = MyPainter(Color(51, 153, 255, 180))
        mojoOver["ProgressBar[Enabled+Indeterminate].foregroundPainter"] = MyPainter(Color(51, 153, 255, 180))
        mojoBar.putClientProperty("Nimbus.Overrides", mojoOver)
        meter.add(mojoBar)
        mojoBar.maximum = player.mojo.max
        mojoBar.value = player.mojo.current

        val bio = JPanel()
        topPanel.add(bio)
        bio.layout = GridLayout(2, 0, 0, 0)
        bio.background = frameColor

        val name = JLabel(player.name)
        name.horizontalAlignment = 2
        name.font = Font("Georgia", 1, 15)
        name.foreground = textColor
        //		bio.add(name);
        lvl = JLabel("Lvl: " + player.level)
        lvl.font = Font("Georgia", 1, 15)
        lvl.foreground = textColor

        //		bio.add(lvl);
        xp = JLabel("XP: " + player.xp)
        xp.font = Font("Georgia", 1, 15)
        xp.foreground = textColor

        //		bio.add(xp);
        timelbl = JLabel()
        timelbl.font = Font("Georgia", 1, 16)
        timelbl.foreground = textColor
        bio.add(timelbl)

        daylbl = JLabel()
        daylbl.font = Font("Georgia", 1, 16)
        daylbl.foreground = textColor
        bio.add(daylbl)

        UIManager.put("ToggleButton.select", Color(75, 88, 102))
        stsbtn = JToggleButton("Status")
        stsbtn.background = frameColor
        stsbtn.foreground = textColor

        stsbtn.addActionListener {
            if (stsbtn.isSelected) {
                centerPanel.remove(portraitPanel)
                centerPanel.add(statusPanel, BorderLayout.WEST)
            } else {
                centerPanel.remove(statusPanel)
                centerPanel.add(portraitPanel, BorderLayout.WEST)
            }
            refresh()
            centerPanel.repaint()
            //centerPanel.validate();
        }
        bio.add(stsbtn)

        cashlbl = JLabel()
        cashlbl.font = Font("Georgia", 1, 16)
        cashlbl.foreground = textColor
        bio.add(cashlbl)


        loclbl = JLabel()
        loclbl.font = Font("Georgia", 1, 16)
        loclbl.foreground = textColor
        bio.add(loclbl)


        invbtn = JToggleButton("Inventory")
        invbtn.background = frameColor
        invbtn.foreground = textColor
        invbtn.addActionListener {
            if (invbtn.isSelected) {
                centerPanel.add(inventoryPanel, "East")
            } else {
                centerPanel.remove(inventoryPanel)
            }
            refresh()
            //centerPanel.validate();
        }
        bio.add(invbtn)
        closet = ClothesChangeGUI(player)
        midPanel.add(closet, USE_CLOSET_UI)
        showNone()
        enableOptions()
        commandPanel.background = frameColor
        groupPanel.background = frameColor
        portraitPanel.background = backgroundColor
        imgPanel.background = backgroundColor
        mainpanel.background = backgroundColor
        topPanel.validate()
    }

    fun createCharacter() {
        if (::creation.isInitialized) contentPane.remove(creation)
        contentPane.remove(mainpanel)
        creation = CreationGUI(this)
        contentPane.add(creation)
        mntmNewgame.isEnabled = false

        //getContentPane().validate();
    }

    fun purgePlayer() {
        contentPane.remove(mainpanel)
        clearText()
        clearCommand()
        showNone()
        if (::closet.isInitialized) {
            midPanel.remove(closet)
        }
        mntmQuitMatch.isEnabled = false
        combat = null
        topPanel.removeAll()
    }

    open fun clearText() {
        bodytext = ""
        textPane.text = ""
    }

    open fun message(text: String?) {
        if (text == null || text === "") {
            return
        }
        if (text.trim { it <= ' ' }.isEmpty()) {
            return
        }
        val doc = textPane.document as HTMLDocument
        val editorKit = textPane.editorKit as HTMLEditorKit
        bodytext = "$bodytext$text<br>"
        if (Global.checkFlag(Flag.altcolors)) {
            editorKit.insertHTML(
                doc,
                doc.length,
                "<font face='Georgia'><font color='black'><font size='$fontsize'>$text<br>",
                0,
                0,
                null
            )
        } else {
            editorKit.insertHTML(
                doc,
                doc.length,
                "<font face='Georgia'><font color='black'><font size='$fontsize'>$text<br>",
                0,
                0,
                null
            )
        }

        SwingUtilities.invokeLater { textScroll.verticalScrollBar.value = 0 }
    }

    fun messageHead(text: String?) {
        if (text == null || text === "") {
            return
        }
        if (text.trim { it <= ' ' }.isEmpty()) {
            return
        }
        val newtext = text + bodytext
        clearText()
        message(newtext)
    }

    fun combatMessage(text: String) {
        val doc = textPane.document as HTMLDocument
        val editorKit = textPane.editorKit as HTMLEditorKit
        bodytext = "$bodytext$text<br>"
        if (Global.checkFlag(Flag.altcolors)) {
            editorKit.insertHTML(
                doc,
                doc.length,
                "<font face='Georgia'><font color='black'><font size='$fontsize'>$text<br>",
                0,
                0,
                null
            )
        } else {
            editorKit.insertHTML(
                doc,
                doc.length,
                "<font face='Georgia'><font color='black'><font size='$fontsize'>$text<br>",
                0,
                0,
                null
            )
        }
        SwingUtilities.invokeLater { textScroll.verticalScrollBar.value = 1 }
    }

    open fun clearCommand() {
        commandPanel.reset()
        skills.clear()
        flasks.clear()
        potions.clear()
        demands.clear()
        for (tactic in TacticGroup.entries) {
            skills[tactic] = ArrayList()
        }
        groupBox.removeAll()
        commandPanel.refresh()
    }

    fun addFlasks(action: Skill, com: Combat) {
        skills[TacticGroup.Flask]!!.add(SkillButton(action, com))
    }

    fun addPotions(action: Skill, com: Combat) {
        skills[TacticGroup.Potion]!!.add(SkillButton(action, com))
    }

    fun addDemands(action: Skill, com: Combat) {
        skills[TacticGroup.Demand]!!.add(SkillButton(action, com))
    }

    fun addSkill(com: Combat, action: Skill) {
        val btn = SkillButton(action, com)
        skills[action.type().group]!!.add(btn)
    }

    fun showSkills() {
        commandPanel.reset()
        var i = 1
        for (group in TacticGroup.entries) {
            val tacticsButton = SwitchTacticsButton(group)
            commandPanel.register((i % 10).digitToChar(), tacticsButton)
            groupBox.add(tacticsButton)
            groupBox.add(Box.createHorizontalStrut(4))
            i += 1
        }
        val flatList = ArrayList<SkillButton>()
        for (group in TacticGroup.entries) {
            skills[group]!!.sortWith{ button1, button2 -> button1.text.compareTo(button2.text) }
            for (b in skills[group]!!) {
                flatList.add(b)
            }
        }
        if (currentTactics == TacticGroup.All || flatList.size <= 6) {
            for (b in flatList) {
                addToCommandPanel(b)
            }
        } else {
            for (button in skills[currentTactics]!!) {
                addToCommandPanel(button)
            }
        }
        Scheduler.pause()
        commandPanel.refresh()
    }

    private fun addToCommandPanel(button: KeyableButton) {
        commandPanel.addButton(button)
    }

    fun addAction(action: Action, user: Character) {
        commandPanel.addButton(ActionButton(action, user))
        Scheduler.pause()
        commandPanel.refresh()
    }

    fun addActivity(act: Activity) {
        if (act.tooltip().isEmpty()) {
            commandPanel.addButton(ActivityButton(act))
        } else {
            commandPanel.addButton(ActivityButton(act, act.tooltip()))
        }
        commandPanel.refresh()
    }

    open fun next(combat: Combat) {
        refresh()
        clearCommand()
        commandPanel.addButton(NextButton(combat))
        Scheduler.pause()
        commandPanel.refresh()
    }

    fun next(event: Activity) {
        event.next()
        clearCommand()
        commandPanel.addButton(EventButton(event, "Next"))
        commandPanel.refresh()
    }

    fun choose(choice: String) {
        commandPanel.addButton(SceneButton(choice))
        commandPanel.refresh()
    }

    fun choose(choice: String, description: String?) {
        commandPanel.addButton(SceneButton(choice, description))
        commandPanel.refresh()
    }

    fun unchoose(choice: String, description: String?) {
        val button = SceneButton(choice, description)
        button.isEnabled = false
        commandPanel.addButton(button)
        commandPanel.refresh()
    }

    fun choose(event: Activity, choice: String) {
        commandPanel.addButton(EventButton(event, choice))
        commandPanel.refresh()
    }

    fun choose(event: Activity, choice: String, description: String?) {
        commandPanel.addButton(EventButton(event, choice, description))
        commandPanel.refresh()
    }

    fun unchoose(event: Activity, choice: String, description: String?) {
        val button = EventButton(event, choice, description)
        button.isEnabled = false
        commandPanel.addButton(button)
        commandPanel.refresh()
    }

    fun sale(shop: Store, i: Item) {
        commandPanel.addButton(ItemButton(shop, i))
        commandPanel.refresh()
    }

    fun sale(shop: Store, i: Clothing) {
        commandPanel.addButton(ItemButton(shop, i))
        commandPanel.refresh()
    }

    fun promptFF(enc: Encounter, target: Character) {
        clearCommand()
        commandPanel.addButton(EncounterButton("Fight", enc, target, Encs.fight))
        commandPanel.addButton(EncounterButton("Flee", enc, target, Encs.flee))
        if (player!!.has(Consumable.smoke)) {
            commandPanel.addButton(EncounterButton("Smoke Bomb", enc, target, Encs.smokebomb))
        }
        Scheduler.pause()
        commandPanel.refresh()
    }

    open fun promptAmbush(enc: Encounter, target: Character) {
        clearCommand()
        commandPanel.addButton(EncounterButton("Attack " + target.name, enc, target, Encs.ambush))
        Scheduler.pause()
        commandPanel.refresh()
    }

    fun promptOpportunity(enc: Encounter, target: Character, trap: Trap) {
        clearCommand()
        commandPanel.addButton(EncounterButton("Attack " + target.name, enc, target, Encs.capitalize, trap))
        Scheduler.pause()
        commandPanel.refresh()
    }

    fun promptShower(encounter: Encounter, target: Character) {
        clearCommand()
        commandPanel.addButton(EncounterButton("Suprise Her", encounter, target, Encs.showerattack))
        if (!target.isNude) {
            commandPanel.addButton(EncounterButton("Steal Clothes", encounter, target, Encs.stealclothes))
        }
        if (player!!.has(Flask.Aphrodisiac)) {
            commandPanel.addButton(EncounterButton("Use Aphrodisiac", encounter, target, Encs.aphrodisiactrick))
        }
        commandPanel.addButton(EncounterButton("Do Nothing", encounter, target, Encs.wait))
        Scheduler.pause()
        commandPanel.refresh()
    }

    fun promptIntervene(enc: Encounter, p1: Character, p2: Character) {
        clearCommand()
        commandPanel.addButton(InterveneButton(enc, p1))
        commandPanel.addButton(InterveneButton(enc, p2))
        commandPanel.addButton(EncounterButton("Watch", enc, p1, Encs.watch))
        Scheduler.pause()
        commandPanel.refresh()
    }

    fun prompt(message: String = "", choices: ArrayList<KeyableButton>) {
        clearCommand()
        if (message !== "") {
            clearText()
            message(message)
        }
        for (button in choices) {
            commandPanel.addButton(button)
        }
        commandPanel.refresh()
    }

    fun ding() {
        if (player!!.attpoints > 0) {
            message(player!!.attpoints.toString() + " Attribute Points remain.\n")
            clearCommand()
            for (att in player!!.att.keys) {
                if (att.isTrainable) {
                    commandPanel.addButton(AttributeButton(att))
                }
            }
            Scheduler.pause()
            commandPanel.refresh()
        } else if (player!!.countFeats() < player!!.level / 4) {
            message("You've earned a new perk. Select one below.")
            clearCommand()
            val feats = Global.availableFeats(player!!)
            val available = feats.any()
            for (feat in feats) {
                commandPanel.addButton(FeatButton(feat))
            }
            commandPanel.refresh()
            if (!available) {
                clearCommand()
                Global.gainSkills(player!!)
                endCombat()
            }
        } else {
            clearCommand()
            Global.gainSkills(player!!)
            endCombat()
        }
    }

    open fun endCombat() {
        combat = null
        clearText()
        resetPortrait()
        showMap()
        centerPanel.revalidate()
        portraitPanel.revalidate()
        portraitPanel.repaint()
        Scheduler.unpause()
    }

    fun startMatch() {
        mntmQuitMatch.isEnabled = true
        showMap()
    }

    fun endMatch() {
        clearCommand()
        Global.flag(Flag.night)
        mntmQuitMatch.isEnabled = false
        commandPanel.addButton(SleepButton())
        commandPanel.addButton(SaveButton())
        commandPanel.refresh()
    }

    fun refresh() {
        stamina.text = "Stamina: " + getLabelString(player!!.stamina)
        arousal.text = "Arousal: " + getLabelString(player!!.arousal)
        mojo.text = "Mojo: " + getLabelString(player!!.mojo)
        lvl.text = "Lvl: " + player!!.level
        xp.text = "XP: " + player!!.xp
        staminaBar.maximum = player!!.stamina.max
        staminaBar.value = player!!.stamina.current
        arousalBar.maximum = player!!.arousal.max
        arousalBar.value = player!!.arousal.current
        mojoBar.maximum = player!!.mojo.max
        mojoBar.value = player!!.mojo.current
        loclbl.text = player!!.location.name
        displayInventory()
        displayStatus()
        map.repaint()
        refreshLite()
    }

    fun refreshLite() {
        var day: String? = ""
        if (Scheduler.time.isBefore(LocalTime.of(2, 0))) {
            day = Scheduler.getDayString(Scheduler.date + 1)
        }
        day = Scheduler.getDayString(Scheduler.date)
        daylbl.text = day
        timelbl.text = Scheduler.timeString
        cashlbl.text = "$" + player!!.money
    }

    fun refreshColors() {
        contentPane.remove(mainpanel)
        topPanel.removeAll()
        populatePlayer(Global.player)
        displayInventory()
        displayStatus()
        inventoryPanel.background = frameColor
        statusPanel.background = frameColor
        commandPanel.background = frameColor
        textPane.foreground = textColor
        textPane.background = backgroundColor
    }

    fun displayInventory() {
        inventoryPanel.removeAll()
        inventoryPanel.repaint()
        val itemPane = JPanel()
        val itemScroll = JScrollPane()
        itemPane.background = backgroundColor
        itemPane.layout = BoxLayout(itemPane, 1)
        itemScroll.setViewportView(itemPane)

        val clothingPane = JPanel()
        clothingPane.background = frameColor
        inventoryPanel.add(itemScroll)
        inventoryPanel.add(clothingPane, BorderLayout.SOUTH)

        val items = player!!.listinventory()
        var count = 0
        val itmlbls = ArrayList<JLabel>()
        for (i in items.keys) {
            if (i.listed) {
                itmlbls.add(count, JLabel(i.getName() + ": " + items[i]))
                itmlbls[count].foreground = textColor
                itmlbls[count].font = Font("Georgia", 1, 16)
                itmlbls[count].toolTipText = i.getFullDesc(player!!)
                itemPane.add(itmlbls[count])
                count++
            }
        }
        var clothesicon: BufferedImage? = null
        if (player!!.tops.isEmpty()) {
            if (player!!.bottoms.isEmpty()) {
                try {
                    val clothesurl = AssetLoader.getResource("clothes0.png")
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            } else if (player!!.bottoms.last().item.type == ClothingType.BOTOUTER) {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes4.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            } else {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes2.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            }
        } else if (player!!.tops.last().item.type == ClothingType.TOPOUTER) {
            if (player!!.bottoms.isEmpty()) {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes6.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            } else if (player!!.bottoms.last().item.type == ClothingType.BOTOUTER) {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes8.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            } else {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes7.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            }
        } else {
            if (player!!.bottoms.isEmpty()) {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes1.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            } else if (player!!.bottoms.last().item.type == ClothingType.BOTOUTER) {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes5.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            } else {
                try {
                    val clothesurl = AssetLoader.getResource(
                        "clothes3.png"
                    )
                    if (clothesurl != null) {
                        clothesicon = ImageIO.read(clothesurl)
                    }
                } catch (_: IOException) {
                }
            }
        }
        if (clothesicon != null) {
            if (Global.checkFlag(Flag.statussprites)) {
                val sts = loadStatusFilters(player!!)
                val scaled = sts.getScaledInstance(clothesicon.width, clothesicon.height, BufferedImage.SCALE_FAST)
                clothesicon.createGraphics().drawImage(scaled, 0, 0, null)
            }
            clothesdisplay = JLabel(ImageIcon(clothesicon))
            clothingPane.add(clothesdisplay)
        }
        centerPanel.revalidate()
        inventoryPanel.revalidate()
        inventoryPanel.repaint()
    }

    fun displayStatus() {
        statusPanel.removeAll()
        statusPanel.minimumSize = Dimension(500, centerPanel.height)
        statusPanel.preferredSize = Dimension(500, centerPanel.height)

        if (width < 720) {
            statusPanel.maximumSize = Dimension(width / 3, height)
            println("STATUS PANEL")
        }

        val statsLeft = JPanel()
        val statsRight = JPanel()
        statusPanel.add(statsLeft)
        statusPanel.add(statsRight)
        statsLeft.minimumSize = Dimension(250, centerPanel.height)
        statsRight.minimumSize = Dimension(250, centerPanel.height)

        statsLeft.layout = BoxLayout(statsLeft, BoxLayout.Y_AXIS)
        statsLeft.alignmentY = TOP_ALIGNMENT
        statsRight.layout = BoxLayout(statsRight, BoxLayout.Y_AXIS)
        statsRight.alignmentY = TOP_ALIGNMENT

        val statsPanel = JPanel()
        statsPanel.layout = BoxLayout(statsPanel, BoxLayout.Y_AXIS)
        statsPanel.minimumSize = Dimension(statsRight.width, centerPanel.height / 3)

        //		statsPanel.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/2));
        val currentStatusPanel = JPanel()
        currentStatusPanel.layout = BoxLayout(currentStatusPanel, BoxLayout.Y_AXIS)
        currentStatusPanel.minimumSize = Dimension(statsRight.width, centerPanel.height / 3)
        //		currentStatusPanel.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/2));
        val traitPanel = JPanel()
        traitPanel.layout = BoxLayout(traitPanel, BoxLayout.Y_AXIS)
        traitPanel.minimumSize = Dimension(statsLeft.width, centerPanel.height / 2)
        //		traitPanel.setPreferredSize(new Dimension(statsLeft.getWidth(), centerPanel.getHeight()));
        val bioPanel = JPanel()
        bioPanel.layout = BoxLayout(bioPanel, BoxLayout.Y_AXIS)
        bioPanel.minimumSize = Dimension(statsLeft.width, centerPanel.height / 3)
        val statsScroll = JScrollPane()
        statsScroll.preferredSize = Dimension(statsRight.width, centerPanel.height / 2)
        val traitScroll = JScrollPane()
        traitScroll.preferredSize = Dimension(statsRight.width, 3 * centerPanel.height / 2)
        val statusScroll = JScrollPane()
        statusScroll.preferredSize = Dimension(statsRight.width, centerPanel.height / 2)

        var sep = JSeparator()
        sep.preferredSize = Dimension(statsLeft.width, 2)
        statsLeft.add(sep)
        val biolbl = JLabel("Player Info")
        biolbl.foreground = textColor
        biolbl.font = Font("Georgia", 1, 24)
        statsLeft.add(biolbl)
        sep = JSeparator()
        sep.preferredSize = Dimension(statsLeft.width, 2)
        statsLeft.add(sep)

        val name = JLabel(player!!.name)
        name.foreground = textColor
        name.font = Font("Georgia", 1, 20)
        val level = JLabel("Level: " + player!!.level)
        level.foreground = textColor
        level.font = Font("Georgia", 1, 20)
        val exp = JLabel("XP: " + player!!.xp)
        exp.foreground = textColor
        exp.font = Font("Georgia", 1, 20)
        val rank = JLabel("Rank: " + player!!.rank)
        rank.foreground = textColor
        rank.font = Font("Georgia", 1, 20)
        bioPanel.add(name)
        bioPanel.add(level)
        bioPanel.add(exp)
        bioPanel.add(rank)
        statsLeft.add(bioPanel)

        sep = JSeparator()
        sep.preferredSize = Dimension(statsRight.width, 2)
        statsRight.add(sep)
        val attibuteslbl = JLabel("Attributes")
        attibuteslbl.foreground = textColor
        attibuteslbl.font = Font("Georgia", 1, 24)
        statsRight.add(attibuteslbl)
        statsRight.add(statsScroll)
        statsScroll.setViewportView(statsPanel)

        sep = JSeparator()
        sep.preferredSize = Dimension(statsLeft.width, 2)
        statsLeft.add(sep)
        val traitslbl = JLabel("Traits")
        traitslbl.foreground = textColor
        traitslbl.font = Font("Georgia", 1, 24)
        statsLeft.add(traitslbl)
        statsLeft.add(traitScroll)
        traitScroll.setViewportView(traitPanel)

        sep = JSeparator()
        sep.preferredSize = Dimension(statsRight.width, 2)
        statsRight.add(sep)
        val statuslbl = JLabel("Current Status")
        statuslbl.foreground = textColor
        statuslbl.font = Font("Georgia", 1, 24)
        statsRight.add(statuslbl)
        statsRight.add(statusScroll)
        statusScroll.setViewportView(currentStatusPanel)

        statsLeft.background = backgroundColor
        statsRight.background = backgroundColor
        bioPanel.background = backgroundColor
        traitPanel.background = backgroundColor
        currentStatusPanel.background = backgroundColor
        statsPanel.background = backgroundColor

        var count = 0
        val attlbls = ArrayList<JLabel>()
        for (a in player!!.att.keys) {
            val amt = player!![a]
            if (amt > 0) {
                val dirtyTrick = JLabel("$a: $amt")

                dirtyTrick.foreground = textColor
                dirtyTrick.font = Font("Georgia", 1, 16)
                dirtyTrick.horizontalAlignment = SwingConstants.CENTER
                dirtyTrick.toolTipText = a.description
                attlbls.add(count, dirtyTrick)

                statsPanel.add(attlbls[count])
                count++
            }
        }
        count = 0
        val traitslbls = ArrayList<JLabel>()
        for (t in player!!.traits) {
            val dirtyTrick = JLabel(t.toString())
            dirtyTrick.foreground = textColor
            dirtyTrick.font = Font("Georgia", 1, 16)
            dirtyTrick.horizontalAlignment = SwingConstants.CENTER
            dirtyTrick.toolTipText = t.desc
            traitslbls.add(count, dirtyTrick)

            traitPanel.add(traitslbls[count])
            count++
        }
        count = 0
        val stslbls = ArrayList<JLabel>()
        for (status in player!!.status) {
            val dirtyTrick = JLabel(status.toString())
            dirtyTrick.foreground = textColor
            dirtyTrick.font = Font("Georgia", 1, 16)
            dirtyTrick.horizontalAlignment = SwingConstants.CENTER
            dirtyTrick.toolTipText = status.tooltip
            stslbls.add(count, dirtyTrick)

            currentStatusPanel.add(stslbls[count])
            count++
        }
        if (player!!.listStatus().isEmpty()) {
            val noStatus = JLabel("No status effects")
            noStatus.foreground = textColor
            noStatus.font = Font("Georgia", 1, 18)
            currentStatusPanel.add(noStatus)
        }
        statusPanel.repaint()
        statusPanel.revalidate()
        centerPanel.revalidate()
    }

    open fun update() {
        refresh()
        if (combat == null) return
        if (combat!!.cPhase != CombatPhase.End) {
            resetPortrait()
            loadPortrait(combat!!.p1, combat!!.p2)
        }
        combatMessage(combat!!.message)
        combat!!.showImage()
        if (combat!!.cPhase == CombatPhase.Upkeep || combat!!.cPhase == CombatPhase.End) {
            next(combat!!)
        }
    }

    override fun update(arg0: Observable, arg1: Any?) {
        refresh()
        if (combat == null) return
        if (combat!!.cPhase != CombatPhase.End) {
            resetPortrait()
            loadPortrait(combat!!.p1, combat!!.p2)
        }
        combatMessage(combat!!.message)
        combat!!.showImage()
        if (combat!!.cPhase == CombatPhase.Upkeep || combat!!.cPhase == CombatPhase.End) {
            next(combat!!)
        }
    }

    fun nSkillsForGroup(group: TacticGroup?): Int {
        return skills[group]!!.size
    }

    fun switchTactics(group: TacticGroup) {
        groupBox.removeAll()
        currentTactics = group
        Global.gui.showSkills()
    }

    fun disableOptions() {
        mntmOptions.isEnabled = false
    }

    fun enableOptions() {
        mntmOptions.isEnabled = true
    }

    var isOptionsEnabled
        get() = mntmOptions.isEnabled
        set(value) { mntmOptions.isEnabled = value }


    fun lowRes(): Boolean {
        return lowres
    }

    private inner class NextButton(combat: Combat) : KeyableButton("Next") {
        private val combat: Combat

        init {
            font = Font("Georgia", 0, 18)
            this@NextButton.combat = combat
            addActionListener {
                this@GUI.clearCommand()
                if (this@NextButton.combat.cPhase == CombatPhase.Upkeep) {
                    this@NextButton.combat.clear()
                    this@GUI.clearText()
                    this@NextButton.combat.turn()
                } else if (this@NextButton.combat.cPhase == CombatPhase.End) {
                    if (!this@NextButton.combat.end()) {
                        this@GUI.endCombat()
                    }
                }
            }
        }
    }

    private open inner class EventButton(event: Activity, choice: String) : KeyableButton(choice) {
        protected var event: Activity
        protected var choice: String

        init {
            font = Font("Georgia", 0, 18)
            this.event = event
            this.choice = choice
            addActionListener {
                this@EventButton.event.visit(this@EventButton.choice)
                Global.gui.refreshLite()
            }
        }

        constructor(event: Activity, choice: String, description: String?) : this(event, choice) {
            toolTipText = description
        }
    }

    private inner class ItemButton : EventButton {
        constructor(event: Activity, i: Item) : super(event, i.getName()) {
            font = Font("Georgia", 0, 18)
            toolTipText = i.desc
        }

        constructor(event: Activity, i: Clothing) : super(event, i.properName) {
            font = Font("Georgia", 0, 18)
            toolTipText = i.specialDescription
        }
    }

    private inner class AttributeButton(att: Attribute) : KeyableButton(att.toString()) {
        private val att: Attribute

        init {
            font = Font("Georgia", 0, 18)
            this.att = att
            toolTipText = att.description
            addActionListener {
                player!!.mod(this@AttributeButton.att, 1)
                player!!.attpoints -= 1
                ding()
            }
        }
    }

    private inner class FeatButton(feat: Trait) : KeyableButton(feat.toString()) {
        private val feat: Trait

        init {
            font = Font("Georgia", 0, 18)
            this@FeatButton.feat = feat
            toolTipText = feat.desc
            addActionListener {
                player!!.add(this@FeatButton.feat)
                Global.gainSkills(player!!)
                endCombat()
            }
        }
    }


    private inner class InterveneButton(enc: Encounter, assist: Character) :
        KeyableButton("Help " + assist.name) {
        private val enc: Encounter
        private val assist: Character

        init {
            font = Font("Georgia", 0, 18)
            this@InterveneButton.enc = enc
            this@InterveneButton.assist = assist
            addActionListener {
                this@InterveneButton.enc.intrude(
                    player!!,
                    this@InterveneButton.assist
                )
            }
        }
    }

    private inner class ActivityButton(act: Activity) : KeyableButton(act.toString()) {
        private val act: Activity

        init {
            font = Font("Georgia", 0, 18)
            this@ActivityButton.act = act
            addActionListener { this@ActivityButton.act.visit("Start") }
        }

        constructor(act: Activity, description: String?) : this(act) {
            toolTipText = description
        }
    }

    private inner class SleepButton : KeyableButton("Go to sleep") {
        init {
            font = Font("Georgia", 0, 18)
            addActionListener { Scheduler.dawn() }
        }
    }

    private inner class PageButton(label: String, page: Int) : KeyableButton(label) {
        private val page: Int

        init {
            font = Font("Georgia", 0, 18)
            this.page = page
            addActionListener {
                commandPanel.reset()
                showSkills()
            }
        }
    }

    private inner class MyPainter(private val color: Color) : Painter<JProgressBar?> {
        override fun paint(gd: Graphics2D, t: JProgressBar?, width: Int, height: Int) {
            gd.color = color
            gd.fillRect(0, 0, width, height)
        }
    }

    fun changeClothes(player: Character?, event: Activity?) {
        closet.update(event)
        val midLayout = midPanel.layout as CardLayout
        midLayout.show(midPanel, USE_CLOSET_UI)
        midPanel.repaint()
    }

    fun removeClosetGUI() {
        val midLayout = midPanel.layout as CardLayout
        midLayout.show(midPanel, USE_MAIN_TEXT_UI)
        midPanel.repaint()
        displayStatus()
    }

    companion object {
        private const val USE_PORTRAIT = "PORTRAIT"
        private const val USE_MAP = "MAP"
        private const val USE_NONE = "NONE"
        private const val USE_SPRITE = "SPRITE"
        private const val USE_MAIN_TEXT_UI = "MAIN_TEXT"
        private const val USE_CLOSET_UI = "CLOSET"
    }
}
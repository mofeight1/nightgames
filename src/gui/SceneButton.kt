package gui

import global.Global
import java.awt.Font

class SceneButton(label: String) : KeyableButton(label) {
    private val choice: String

    init {
        font = Font("Georgia", 0, 18)
        this.choice = label
        addActionListener {
            Global.current!!.respond(this@SceneButton.choice)
        }
    }

    constructor(label: String, description: String?) : this(label) {
        this.toolTipText = description
    }
}

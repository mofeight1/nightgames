package gui

import java.awt.Font

internal class RunnableButton(private val text: String, private val runnable: Runnable) :
    KeyableButton(formatHTMLMultiline(text, "")
) {
    init {
        resetFontSize()
        addActionListener { runnable.run() }
    }

    private fun resetFontSize() {
        font = if (text.contains("<br/>")) {
            Font("Georgia", Font.PLAIN, 14)
        } else {
            Font("Georgia", Font.PLAIN, 18)
        }
    }

    override fun getText(): String {
        return text
    }

    override fun setHotkeyTextTo(string: String?) {
        setText(formatHTMLMultiline(text," [${resetFontSize()}]"))
    }

    companion object {
        private fun formatHTMLMultiline(original: String, hotkeyExtra: String): String {
            // do not word wrap the hotkey extras, since it looks pretty bad.
            return "<html><center>$original$hotkeyExtra</center></html>"
        }
    }
}
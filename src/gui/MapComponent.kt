package gui

import areas.MapDrawHint
import global.Constants
import global.Global
import global.Scheduler
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Rectangle
import java.awt.RenderingHints
import java.awt.font.FontRenderContext
import java.awt.geom.AffineTransform
import javax.swing.JComponent
import kotlin.math.max

class MapComponent : JComponent() {
    private val borderWidth = 3

    fun centerString(g: Graphics, r: Rectangle, drawHint: MapDrawHint, font: Font) {
        val frc = FontRenderContext(null, true, true)
        val r2D = font.getStringBounds(drawHint.label, frc)

        val rWidth = Math.round(r2D.width).toInt()
        val rHeight = Math.round(r2D.height).toInt()
        val rX = Math.round(r2D.x).toInt()
        val rY = Math.round(r2D.y).toInt()

        val a = (r.width / 2) - (rWidth / 2) - rX
        val b = (r.height / 2) - (rHeight / 2) - rY
        var orig: AffineTransform? = null
        if (drawHint.vertical && g is Graphics2D) {
            orig = g.transform
            g.rotate(Math.PI / 2, (r.x + a) + rWidth * .5, (r.y + b - rHeight / 4).toDouble())
        }
        g.font = font
        g.drawString(drawHint.label, r.x + a, r.y + b)

        if (orig != null) {
            val g2d = g as Graphics2D
            g2d.transform = orig
        }
    }

    override fun paint(g: Graphics) {
        if (Scheduler.match == null) {
            return
        }
        if (g is Graphics2D) {
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        }

        val multiplier = 10
        val width = max(width, 25 * multiplier)
        val height = max(height, 19 * multiplier)
        val mapBorder = 12
        val yOffset = 0
        g.color = Constants.ALTFRAMECOLOR
        g.fillRect(0, 0, width, height)
        g.color = Color.WHITE
        g.drawRect(borderWidth, borderWidth, width - borderWidth * 2, height - borderWidth * 2)
        val rooms = Scheduler.match!!.areas
        for (area in rooms) {
            if (area.drawHint.rect.width == 0 || area.drawHint.rect.height == 0) {
                return
            }
            val rect = Rectangle(
                area.drawHint.rect.x * multiplier + mapBorder,
                area.drawHint.rect.y * multiplier + yOffset + mapBorder,
                area.drawHint.rect.width * multiplier, area.drawHint.rect.height * multiplier
            )
            if (!area.humanPresent()) {
                if (Global.player.opponentDetected(area)) {
                    g.color = Color(150, 45, 60)
                } else {
                    g.color = Color(0, 34, 100)
                }
            } else {
                g.color = Color(25, 74, 120)
            }
            g.fillRect(rect.x, rect.y, rect.width, rect.height)
        }
        for (area in rooms) {
            if (area.drawHint.rect.width == 0 || area.drawHint.rect.height == 0) {
                return
            }
            val rect = Rectangle(
                area.drawHint.rect.x * multiplier + mapBorder,
                area.drawHint.rect.y * multiplier + yOffset + mapBorder,
                area.drawHint.rect.width * multiplier, area.drawHint.rect.height * multiplier
            )
            g.color = Color(50, 100, 200)
            g.drawRect(rect.x, rect.y, rect.width, rect.height)
        }
        for (area in rooms) {
            if (area.drawHint.rect.width == 0 || area.drawHint.rect.height == 0) {
                return
            }
            val rect = Rectangle(
                area.drawHint.rect.x * multiplier + mapBorder,
                area.drawHint.rect.y * multiplier + yOffset + mapBorder,
                area.drawHint.rect.width * multiplier, area.drawHint.rect.height * multiplier
            )
            g.color = Color.WHITE
            centerString(g, rect, area.drawHint, Companion.font)
        }
    }

    companion object {
        private val font = Font("Courier", Font.BOLD, 11)
    }
}

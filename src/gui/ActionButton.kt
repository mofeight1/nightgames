package gui

import actions.Action
import characters.Character
import global.Global
import global.Scheduler
import java.awt.Font

class ActionButton(action: Action, user: Character) : KeyableButton(action.toString()) {
    protected var action: Action
    protected var user: Character

    init {
        font = Font("Georgia", Font.PLAIN, 18)
        this.action = action
        this.user = user
        if (action.tooltip.isNotEmpty()) {
            this.toolTipText = action.tooltip
        }
        background = action.consider().color
        addActionListener {
            Global.gui.clearText()
            this@ActionButton.action.execute(this@ActionButton.user)
            if (!this@ActionButton.action.freeAction) Scheduler.unpause()
        }
    }
}

package gui

import java.awt.Color
import java.awt.Dimension
import java.util.Locale
import javax.swing.BorderFactory
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JPanel
import javax.swing.border.CompoundBorder

class CommandPanel(width: Int, height: Int) : JPanel() {
    //    private JPanel panel;
    private var index: Int
    private var page = 0
    private var perPage = 0
    private var rowLimit = 6
    private val hotkeyMapping: HashMap<Char, KeyableButton>
    private val buttons: ArrayList<KeyableButton>
    private val rows: Array<JPanel>

    init {
//        panel = new JPanel();
        this.background = Color(245, 245, 245)
        if (height > 768) {
            this.preferredSize = Dimension(width, 150)
            this.minimumSize = Dimension(width, 150)
        } else {
            this.preferredSize = Dimension(width, 100)
            this.minimumSize = Dimension(width, 100)
        }
        this.border = CompoundBorder()
        hotkeyMapping = HashMap()
        if (width <= 1024) {
            rowLimit = 4
        }
        perPage = if (height > 768) {
            3 * rowLimit
        } else {
            2 * rowLimit
        }
        rows = arrayOf(JPanel(), JPanel(), JPanel())
        for (row in rows) {
            //FlowLayout layout;
            //layout = new FlowLayout();
            val layout = BoxLayout(row, BoxLayout.LINE_AXIS)
            row.layout = layout
            row.isOpaque = false
            row.border = BorderFactory.createEmptyBorder()
            this.add(row)
        }
        val layout = BoxLayout(this, BoxLayout.Y_AXIS)
        this.layout = layout
        this.alignmentY = TOP_ALIGNMENT
        this.add(Box.createVerticalGlue())
        this.add(Box.createVerticalStrut(15))
        buttons = ArrayList()
        index = 0
    }

    val panel: JPanel
        get() = this

    fun reset() {
        buttons.clear()
        hotkeyMapping.clear()
        clear()
        refresh()
    }

    private fun clear() {
        for (row in rows) {
            row.removeAll()
        }
        if (rowLimit == 4) {
            for (mapping in CRAMPED_HOTKEYS) {
                hotkeyMapping.remove(mapping)
            }
        } else {
            for (mapping in POSSIBLE_HOTKEYS) {
                hotkeyMapping.remove(mapping)
            }
        }
        index = 0
    }

    fun refresh() {
        repaint()
        revalidate()
    }

    fun addButton(button: KeyableButton) {
        page = 0
        buttons.add(button)
        use(button)
    }

    private fun use(button: KeyableButton) {
        val effectiveIndex = index - page * perPage
        val currentPage = page
        if (effectiveIndex in 0..<perPage) {
            val rowIndex = (effectiveIndex / rowLimit).coerceAtMost(rows.size - 1)
            val row = rows[rowIndex]
            row.add(button)
            row.add(Box.createHorizontalStrut(8))
            val hotkey = if (rowLimit == 4) {
                CRAMPED_HOTKEYS[effectiveIndex]
            } else {
                POSSIBLE_HOTKEYS[effectiveIndex]
            }
            register(hotkey, button)
            if (DEFAULT_CHOICES.contains(button.text) && !hotkeyMapping.containsKey(' ')) {
                hotkeyMapping[' '] = button
            }
        } else if (effectiveIndex == -1) {
            val leftPage: KeyableButton = RunnableButton("<<<") { setPage(currentPage - 1) }
            rows[0].add(leftPage, 0)
            register('~', leftPage)
        } else if (effectiveIndex == perPage) {
            val rightPage: KeyableButton = RunnableButton(">>>") { setPage(currentPage + 1) }
            rows[0].add(rightPage)
            register('`', rightPage)
        }
        index += 1
    }

    fun setPage(page: Int) {
        this.page = page
        clear()
        for (b in buttons) {
            use(b)
        }
        refresh()
    }

    fun getButtonForHotkey(keyChar: Char): KeyableButton? {
        return hotkeyMapping[keyChar]
    }

    fun register(hotkey: Char, button: KeyableButton) {
        button.setHotkeyTextTo(hotkey.toString().uppercase(Locale.getDefault()))
        hotkeyMapping[hotkey] = button
    } /*	public void setBackground(Color frameColor) {
		setBackground(frameColor);
	}*/

    companion object {
        private val POSSIBLE_HOTKEYS: List<Char> = arrayListOf(
            'q', 'w', 'e', 'r', 't', 'y',
            'a', 's', 'd', 'f', 'g', 'h',
            'z', 'x', 'c', 'v', 'b', 'n'
        )
        private val CRAMPED_HOTKEYS: List<Char> = arrayListOf(
            'q', 'w', 'e', 'r',
            'a', 's', 'd', 'f',
            'z', 'x', 'c', 'v'
        )
        private val DEFAULT_CHOICES: Set<String> = HashSet(arrayListOf("Wait", "Nothing", "Next", "Leave", "Back"))
    }
}
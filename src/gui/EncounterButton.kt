package gui

import characters.Character
import combat.Encounter
import combat.Encs
import global.Scheduler
import trap.Trap
import java.awt.Font

class EncounterButton : KeyableButton {
    private var enc: Encounter
    private var target: Character
    private var choice: Encs
    private var trap: Trap? = null

    constructor(label: String, enc: Encounter, target: Character, choice: Encs) : super(label) {
        font = Font("Baskerville Old Face", 0, 18)
        this.enc = enc
        this.target = target
        this.choice = choice
        addActionListener {
            this@EncounterButton.enc.parse(this@EncounterButton.choice, this@EncounterButton.target)
            Scheduler.unpause()
        }
    }

    constructor(label: String, enc: Encounter, target: Character, choice: Encs, trap: Trap) : super(label) {
        font = Font("Baskerville Old Face", 0, 18)
        this.enc = enc
        this.target = target
        this.choice = choice
        this.trap = trap
        addActionListener {
            this@EncounterButton.enc.parse(
                this@EncounterButton.choice,
                this@EncounterButton.target,
                this@EncounterButton.trap!!
            )
            Scheduler.unpause()
        }
    }
}

package gui

import global.Global
import global.SaveManager
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Image
import java.io.IOException
import javax.imageio.ImageIO
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JButton
import javax.swing.JPanel
import kotlin.system.exitProcess

class Title(private val gui: GUI, private val width: Int, private val height: Int) : JPanel() {
    val titleImage: Image? by lazy {
        var image: Image? = null

        val titleurl = AssetLoader.getResource(
            "assets/Title.png"
        )
        if (titleurl != null) {
            image = ImageIO.read(titleurl)
        }
        image = image?.getScaledInstance(width, height - 30, Image.SCALE_FAST)

        image
    }

    init {
        background = Color(0, 6, 18)

        layout = BoxLayout(this, BoxLayout.Y_AXIS)

        val verticalStrut = Box.createVerticalStrut(400)
        add(verticalStrut)

        val horizontalBox = Box.createHorizontalBox()
        add(horizontalBox)

        val btnNewGame = JButton("New Game")
        btnNewGame.font = Font("Georgia", Font.BOLD, 18)
        horizontalBox.add(btnNewGame)
        btnNewGame.alignmentX = CENTER_ALIGNMENT

        val horizontalStrut = Box.createHorizontalStrut(60)
        horizontalBox.add(horizontalStrut)

        val btnLoad = JButton("Load")
        btnLoad.font = Font("Georgia", Font.BOLD, 18)
        horizontalBox.add(btnLoad)
        btnLoad.addActionListener {
            Global.init(gui)
            SaveManager.load()
            isVisible = false
        }
        btnLoad.alignmentX = CENTER_ALIGNMENT

        val horizontalStrut_1 = Box.createHorizontalStrut(60)
        horizontalBox.add(horizontalStrut_1)

        val btnExit = JButton("Exit")
        btnExit.font = Font("Georgia", Font.BOLD, 18)
        horizontalBox.add(btnExit)
        btnExit.alignmentX = CENTER_ALIGNMENT
        btnExit.addActionListener { exitProcess(0) }
        btnNewGame.addActionListener {
            Global.init(gui)
            isVisible = false
        }
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        g.drawImage(titleImage, 0, 0, null)
    }
}

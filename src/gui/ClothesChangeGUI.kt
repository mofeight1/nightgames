package gui

import characters.Character
import daytime.Activity
import global.Global
import global.Modifier
import items.Clothing
import items.ClothingType
import java.awt.Font
import java.awt.GridLayout
import java.awt.image.BufferedImage
import java.io.IOException
import javax.imageio.ImageIO
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.ImageIcon
import javax.swing.JButton
import javax.swing.JComboBox
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JSeparator
import javax.swing.SwingConstants

class ClothesChangeGUI(private val player: Character) : JPanel() {
    private var resume: Activity? = null
    private val menuPane: JPanel
    private val imgPane: JPanel
    private val img: JLabel
    private val TopOut: ArrayList<Clothing>
    private val TopMid: ArrayList<Clothing>
    private val TopIn: ArrayList<Clothing>
    private val BotOut: ArrayList<Clothing>
    private val BotIn: ArrayList<Clothing>
    private val noneString = ""
    private val TOBox: JComboBox<String>
    private val TMBox: JComboBox<String>
    private val TIBox: JComboBox<String>
    private val BOBox: JComboBox<String>
    private val BIBox: JComboBox<String>
    private val translator = HashMap<String, Clothing>()
    private var freeze = true

    init {
        for (article in Clothing.entries) {
            translator[article.fullDesc] = article
        }
        layout = BoxLayout(this, BoxLayout.LINE_AXIS)

        menuPane = JPanel()
        imgPane = JPanel()
        img = JLabel()
        imgPane.add(img)
        add(imgPane)
        add(menuPane)

        menuPane.layout = GridLayout(0, 1, 0, 0)

        TopOut = ArrayList()
        TopMid = ArrayList()
        TopIn = ArrayList()
        BotOut = ArrayList()
        BotIn = ArrayList()

        val separator_1 = JSeparator()

        //menuPane.add(separator_1);
        val lblTopOuter = JLabel("Outerwear")
        lblTopOuter.horizontalAlignment = SwingConstants.CENTER
        lblTopOuter.font = Font("Georgia", Font.PLAIN, 20)
        menuPane.add(lblTopOuter)

        TOBox = JComboBox()
        TOBox.font = Font("Georgia", Font.PLAIN, 22)
        menuPane.add(TOBox)
        TOBox.addItem(Clothing.none.fullDesc)
        TOBox.selectedItem = Clothing.none.fullDesc
        TOBox.addActionListener {
            if (!freeze) {
                loadImage()
            }
        }

        val lblTop = JLabel("Tops")
        lblTop.horizontalAlignment = SwingConstants.CENTER
        lblTop.font = Font("Georgia", Font.PLAIN, 20)
        menuPane.add(lblTop)

        TMBox = JComboBox()
        TMBox.font = Font("Georgia", Font.PLAIN, 22)
        menuPane.add(TMBox)
        TMBox.addItem(Clothing.none.fullDesc)
        TMBox.selectedItem = Clothing.none.fullDesc
        TMBox.addActionListener {
            if (!freeze) {
                loadImage()
            }
        }

        val lblTopInner = JLabel("Undershirts")
        lblTopInner.horizontalAlignment = SwingConstants.CENTER
        lblTopInner.font = Font("Georgia", Font.PLAIN, 20)
        menuPane.add(lblTopInner)

        TIBox = JComboBox()
        TIBox.font = Font("Georgia", Font.PLAIN, 22)
        menuPane.add(TIBox)
        TIBox.addItem(Clothing.none.fullDesc)
        TIBox.selectedItem = Clothing.none.fullDesc
        TIBox.addActionListener {
            if (!freeze) {
                loadImage()
            }
        }

        val separator = JSeparator()
        menuPane.add(separator)

        val lblBottom = JLabel("Pants")
        lblBottom.horizontalAlignment = SwingConstants.CENTER
        lblBottom.font = Font("Georgia", Font.PLAIN, 20)
        menuPane.add(lblBottom)

        BOBox = JComboBox()
        BOBox.font = Font("Georgia", Font.PLAIN, 22)
        menuPane.add(BOBox)
        BOBox.addItem(Clothing.none.fullDesc)
        BOBox.selectedItem = Clothing.none.fullDesc
        BOBox.addActionListener {
            if (!freeze) {
                loadImage()
            }
        }

        val lblBottomInner = JLabel("Underwear")
        lblBottomInner.horizontalAlignment = SwingConstants.CENTER
        lblBottomInner.font = Font("Georgia", Font.PLAIN, 20)
        menuPane.add(lblBottomInner)

        BIBox = JComboBox()
        BIBox.font = Font("Georgia", Font.PLAIN, 22)
        menuPane.add(BIBox)
        BIBox.addActionListener {
            if (!freeze) {
                loadImage()
            }
        }

        val separator_2 = JSeparator()

        //menuPane.add(separator_2);
        val horizontalBox_2 = Box.createHorizontalBox()
        menuPane.add(horizontalBox_2)

        val horizontalStrut = Box.createHorizontalStrut(200)
        horizontalBox_2.add(horizontalStrut)

        val btnOk = JButton("OK")
        btnOk.addActionListener {
            player.outfit[0].clear()
            player.outfit[1].clear()
            if (TIBox.selectedItem !== Clothing.none.fullDesc) {
                player.outfit[0].addLast(translator[TIBox.selectedItem as String]!!)
            }
            if (TMBox.selectedItem !== Clothing.none.fullDesc) {
                player.outfit[0].addLast(translator[TMBox.selectedItem as String]!!)
            }
            if (TOBox.selectedItem !== Clothing.none.fullDesc) {
                player.outfit[0].addLast(translator[TOBox.selectedItem as String]!!)
            }
            if (BIBox.selectedItem !== Clothing.none.fullDesc) {
                player.outfit[1].addLast(translator[BIBox.selectedItem as String]!!)
            }
            if (BOBox.selectedItem !== Clothing.none.fullDesc) {
                player.outfit[1].addLast(translator[BOBox.selectedItem as String]!!)
            }
            player.change(Modifier.normal)
            Global.gui.removeClosetGUI()
            resume!!.visit("Start")
        }
        btnOk.font = Font("Georgia", Font.PLAIN, 24)
        horizontalBox_2.add(btnOk)
        freeze = false
    }

    fun loadImage() {
        img.icon = null
        var imagepath = "assets/room/Room.jpg"
        var room: BufferedImage? = null

        try {
            val imgurl = AssetLoader.getResource(imagepath)
            if (imgurl != null) {
                room = ImageIO.read(imgurl)
            }
        } catch (_: IOException) {
        }
        val g = room!!.createGraphics()

        val TO = translator[TOBox.selectedItem]
        val TM = translator[TMBox.selectedItem]
        val TI = translator[TIBox.selectedItem]
        val BO = translator[BOBox.selectedItem]
        val BI = translator[BIBox.selectedItem]

        var TOIMG: BufferedImage? = null
        var TMIMG: BufferedImage? = null
        var TIIMG: BufferedImage? = null
        var BIIMG: BufferedImage? = null
        var BOIMG: BufferedImage? = null

        if (TO != Clothing.none) {
            imagepath = when (TO) {
                Clothing.cloak, Clothing.halfcloak -> "assets/room/TO_Cloak.png"
                Clothing.windbreaker -> "assets/room/TO_Windbreaker.png"
                Clothing.blazer -> "assets/room/TO_Blazer.png"
                Clothing.trenchcoat -> "assets/room/TO_Coat.png"
                Clothing.labcoat -> "assets/room/TO_LabCoat.png"
                Clothing.furcoat -> "assets/room/TO_FurCoat.png"
                else -> "assets/room/TO_Jacket.png"

            }
            try {
                val imgurl = AssetLoader.getResource(imagepath)
                if (imgurl != null) {
                    TOIMG = ImageIO.read(imgurl)
                }
            } catch (_: IOException) {
            }
            if (TOIMG != null) {
                g.drawImage(TOIMG, 0, 0, null)
            }
        }
        if (TI != Clothing.none) {
            when (TI) {
                Clothing.lacebra, Clothing.bra -> imagepath = "assets/room/TI_Bra.png"
                Clothing.undershirt -> imagepath = "assets/room/TI_Undershirt.png"
                else -> {}
            }
            try {
                val imgurl = AssetLoader.getResource(imagepath)
                if (imgurl != null) {
                    TIIMG = ImageIO.read(imgurl)
                }
            } catch (_: IOException) {
            }
            if (TIIMG != null) {
                g.drawImage(TIIMG, 0, 0, null)
            }
        }
        if (TM != Clothing.none) {
            imagepath = when (TM) {
                Clothing.silkShirt -> "assets/room/TM_SilkShirt.png"
                Clothing.sweater -> "assets/room/TM_Sweater.png"
                Clothing.shirt -> "assets/room/TM_Shirt.png"
                Clothing.sweatshirt -> "assets/room/TM_SweatShirt.png"
                Clothing.gi -> "assets/room/TM_KungfuShirt.png"
                Clothing.legendshirt -> "assets/room/TM_LegendTShirt.png"
                Clothing.shinobitop -> "assets/room/TM_Ninja.png"
                Clothing.blouse -> "assets/room/TM_Blouse.png"
                else -> "assets/room/TM_TShirt.png"
            }
            try {
                val imgurl = AssetLoader.getResource(imagepath)
                if (imgurl != null) {
                    TMIMG = ImageIO.read(imgurl)
                }
            } catch (_: IOException) {
            }
            if (TMIMG != null) {
                g.drawImage(TMIMG, 0, 0, null)
            }
        }
        if (BI != Clothing.none) {
            imagepath = when (BI) {
                Clothing.boxers -> "assets/room/BI_Boxers.png"
                Clothing.cup -> "assets/room/BI_Cup.png"
                Clothing.loincloth -> "assets/room/BI_Loincloth.png"
                Clothing.pouchlessbriefs -> "assets/room/BI_PouchlessBriefs.png"
                Clothing.speedo -> "assets/room/BI_Speedo.png"
                Clothing.lacepanties, Clothing.panties -> "assets/room/BI_Panties.png"
                else -> "assets/room/BI_Briefs.png"
            }
            try {
                val imgurl = AssetLoader.getResource(imagepath)
                if (imgurl != null) {
                    BIIMG = ImageIO.read(imgurl)
                }
            } catch (_: IOException) {
            }
            if (BIIMG != null) {
                g.drawImage(BIIMG, 0, 0, null)
            }
        }
        if (BO != Clothing.none) {
            imagepath = when (BO) {
                Clothing.shorts -> "assets/room/BO_Shorts.png"
                Clothing.cutoffs -> "assets/room/BO_JeanShorts.png"
                Clothing.jeans -> "assets/room/BO_Jeans.png"
                Clothing.gothpants -> "assets/room/BO_Goth.png"
                Clothing.kungfupants -> "assets/room/BO_KungfuPants.png"
                Clothing.kilt -> "assets/room/BO_Kilt.png"
                Clothing.skirt -> "assets/room/BO_Skirt.png"
                else -> "assets/room/BO_Pants.png"
            }
            try {
                val imgurl = AssetLoader.getResource(imagepath)
                if (imgurl != null) {
                    BOIMG = ImageIO.read(imgurl)
                }
            } catch (_: IOException) {
            }
            if (BOIMG != null) {
                g.drawImage(BOIMG, 0, 0, null)
            }
        }
        g.dispose()
        img.icon = ImageIcon(room)
    }

    fun update(event: Activity?) {
        freeze = true
        this.resume = event
        TopOut.clear()
        TopMid.clear()
        TopIn.clear()
        BotOut.clear()
        BotIn.clear()
        TOBox.removeAllItems()
        TOBox.addItem(Clothing.none.fullDesc)
        TOBox.selectedItem = Clothing.none.fullDesc
        TMBox.removeAllItems()
        TMBox.addItem(Clothing.none.fullDesc)
        TMBox.selectedItem = Clothing.none.fullDesc
        TIBox.removeAllItems()
        TIBox.addItem(Clothing.none.fullDesc)
        TIBox.selectedItem = Clothing.none.fullDesc
        BOBox.removeAllItems()
        BOBox.addItem(Clothing.none.fullDesc)
        BOBox.selectedItem = Clothing.none.fullDesc
        BIBox.removeAllItems()

        for (article in player.closet) {
            translator[article.fullDesc] = article
            when (article.type) {
                ClothingType.TOPOUTER -> {
                    TopOut.add(article)
                    TOBox.addItem(article.fullDesc)
                }

                ClothingType.TOP -> {
                    TopMid.add(article)
                    TMBox.addItem(article.fullDesc)
                }

                ClothingType.TOPUNDER -> {
                    TopIn.add(article)
                    TIBox.addItem(article.fullDesc)
                }

                ClothingType.BOTOUTER -> {
                    BotOut.add(article)
                    BOBox.addItem(article.fullDesc)
                }

                ClothingType.UNDERWEAR -> {
                    BotIn.add(article)
                    BIBox.addItem(article.fullDesc)
                }

                else -> {}
            }
        }

        for (article in player.outfit[0]) {
            if (TopOut.contains(article)) {
                TOBox.selectedItem = article.fullDesc
            }
        }
        for (article in player.outfit[0]) {
            if (TopMid.contains(article)) {
                TMBox.selectedItem = article.fullDesc
            }
        }
        for (article in player.outfit[0]) {
            if (TopIn.contains(article)) {
                TIBox.selectedItem = article.fullDesc
            }
        }
        for (article in player.outfit[1]) {
            if (BotOut.contains(article)) {
                BOBox.selectedItem = article.fullDesc
            }
        }
        for (article in player.outfit[1]) {
            if (BotIn.contains(article)) {
                BIBox.selectedItem = article.fullDesc
            }
        }
        loadImage()
        freeze = false
    }
}

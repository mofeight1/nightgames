package trap

import characters.Character

class ClothesSnareTrap : Trap("Clothes Snare", "", "", 3) {
    override fun trigger(target: Character) {
    }

    override fun requirements(owner: Character): Boolean {
        return false
    }

    override fun copy(): Trap {
        return ClothesSnareTrap()
    }
}

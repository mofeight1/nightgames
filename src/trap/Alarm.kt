package trap

import characters.Attribute
import characters.Character
import global.Global
import items.Component

class Alarm : Trap(
    "Alarm",
    "Will alert all participants in nearby areas when someone enters the room, even if they are being stealthy",
    "You rig up a disposable phone to a tripwire. When someone trips the wire, it should set of the phone's alarm.",
    2
) {
    init {
        recipe[Component.Tripwire] = 1
        recipe[Component.Phone] = 1
    }

    override fun trigger(target: Character) {
        if (target.human()) {
            Global.gui.message(
                "You're walking through the eerily quiet campus, when a loud beeping almost makes you jump out of your skin. You realize the beeping is " +
                        "coming from a cell phone on the floor. You shut it off as quickly as you can, but it's likely everyone nearby heard it already."
            )
        } else if (target.location.humanPresent()) {
            Global.gui.message(target.name + " Sets off your alarm, giving away her presence.")
        }
        target.location.alarm = true
        target.location.remove(this)
    }


    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Cunning) >= 6
    }

    override fun copy(): Trap {
        return Alarm()
    }
}

package trap

import characters.Attribute
import characters.Character
import global.Global
import items.Component
import status.Enthralled

class EnthrallingTrap : Trap(
    "Enthralling Trap",
    "Whoever triggers it will helplessly walk toward you in a trance",
    "You pop open a bottle of cum and use its contents to draw"
            + " a pentagram on the floor, all the while speaking"
            + " incantations to cause the first person to step into"
            + " it to be immediatly enthralled by you.",
    3
) {
    override var owner: Character? = null

    init {
        recipe[Component.Semen] = 1
    }

    override fun trigger(target: Character) {
        if (target.human()) {
            if (target.check(Attribute.Perception, 25 - (target[Attribute.Perception] + target.bonusDisarm()))) {
                Global.gui
                    .message(
                        "As you step across the "
                                + target.location.name
                                + ", you notice a pentagram drawn on the floor,"
                                + " appearing to have been drawn in cum. Wisely,"
                                + " you avoid stepping into it."
                    )
            } else {
                target.add(Enthralled(target, owner!!))
                target.location.opportunity(target, this)
                Global.gui
                    .message(
                        "As you step across the "
                                + target.location.name
                                + ", you are suddenly surrounded by purple flames. Your mind "
                                + "goes blank for a moment, leaving you staring into the distance."
                                + " When you come back to your senses, you shake your head a few"
                                + " times and hope whatever that thing was, it failed at"
                                + " whatever it was supposed to do. The lingering vision of two"
                                + " large red irises staring at you suggest differently, though."
                    )
            }
        } else if (!target.check(Attribute.Perception, 25 - (target[Attribute.Perception] + target.bonusDisarm()))) {
            if (target.location.humanPresent()) Global.gui
                .message(
                    "You catch a bout of purple fire in your peripheral vision,"
                            + "but once you have turned to look the flames are gone. All that is left"
                            + " to see is "
                            + target.name
                            + ", standing still and staring blankly ahead."
                            + " It would seem to be very easy to have your way with her now, but"
                            + " who or whatever left that thing there will probably be thinking"
                            + " the same."
                )
            target.add(Enthralled(target, owner!!))
            target.location.opportunity(target, this)
        }
        target.location.remove(this)
    }

    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Dark) >= 9
    }

    override fun copy(): Trap {
        return EnthrallingTrap()
    }
}

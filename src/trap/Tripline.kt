package trap

import characters.Anatomy
import characters.Attribute
import characters.Character
import global.Global
import items.Component

class Tripline : Trap(
    "Tripline", "Will trip the victim, letting you start the fight in an advantageous position",
    "You run a length of rope at ankle height. It should trip anyone who isn't paying much attention.", 3
) {
    init {
        recipe[Component.Rope] = 1
    }

    override fun trigger(target: Character) {
        if (target.human()) {
            if (!target.check(Attribute.Perception, 20 - (target[Attribute.Perception] + target.bonusDisarm()))) {
                Global.gui.message("You trip over a line of cord and fall on your face.")
                target.pain(5.0, Anatomy.head)
                target.location.opportunity(target, this)
            } else {
                Global.gui.message("You spot a line strung across the corridor and carefully step over it.")
                target.location.remove(this)
            }
        } else {
            if (!target.check(Attribute.Perception, 15)) {
                if (target.location.humanPresent()) {
                    Global.gui
                        .message(target.name + " carelessly stumbles over the tripwire and lands with an audible thud.")
                }
                target.pain(5.0, Anatomy.head)
                target.location.opportunity(target, this)
            }
        }
    }

    override fun requirements(owner: Character): Boolean {
        return true
    }

    override fun copy(): Trap {
        return Tripline()
    }
}

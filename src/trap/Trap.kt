package trap

import areas.Area
import areas.Deployable
import characters.Character
import characters.Pool
import combat.Encounter
import global.Global
import global.Scheduler
import items.Item
import status.Flatfooted
import utilities.EnumMap
import java.util.EnumMap

abstract class Trap(
    private val label: String,
    val description: String,
    val setup: String,
    override val priority: Int
) : Deployable {
    override var owner: Character? = null
    protected var location: Area? = null
    var decoy: Boolean = false
        protected set
    protected var recipe: HashMap<Item, Int> = HashMap()
    protected var cost: EnumMap<Pool, Int> = EnumMap()

    fun recipe(owner: Character): Boolean {
        if (recipe.keys.any { !owner.has(it, recipe[it]!!) }) return false
        if (cost.keys.any{ !owner.canSpend(it, cost[it]!!) }) return false
        return true
    }

    fun place(owner: Character, area: Area): Trap {
        val copy = copy()
        copy.owner = owner
        copy.location = area
        for (i in recipe.keys) {
            owner.consume(i, recipe[i]!!)
        }
        for (p in cost.keys) {
            owner.spend(p, cost[p]!!)
        }
        area.place(copy)
        return copy
    }

    abstract fun trigger(target: Character)

    abstract fun requirements(owner: Character): Boolean
    abstract fun copy(): Trap

    override fun resolve(active: Character) {
        if (active !== owner) {
            trigger(active)
        }
    }

    override fun toString(): String {
        return label
    }

    open fun capitalize(attacker: Character, victim: Character, enc: Encounter) {
        val c = if (attacker.human() || victim.human()) {
            Global.gui.beginCombat(attacker, victim)
        } else {
            Scheduler.match!!.buildCombat(attacker, victim)
        }
        victim.add(Flatfooted(victim, 1))
        enc.engage(c)
        attacker.location.remove(this)
    }
}

package trap

import characters.Attribute
import characters.Character
import characters.Trait
import global.Global
import items.Component

class Decoy : Trap(
    "Decoy",
    "Makes enough noise to lure nearby opponents",
    "You program a phone to play a prerecorded audio track five minutes from now. It should be noticable from a reasonable distance until someone switches it " +
            "off.",
    2
) {
    init {
        decoy = true
        recipe[Component.Phone] = 1
    }

    override fun trigger(target: Character) {
        if (target.human()) {
            Global.gui.message(
                "You follow the noise you've been hearing for a while, which turns out to be coming from a disposable cell phone. Seems like someone " +
                        "is playing a trick and you fell for it. You shut off the phone and toss it aside."
            )
        } else if (target.location.humanPresent()) {
            Global.gui.message(target.name + " finds the decoy phone and deactivates it.")
        }
        target.location.remove(this)
    }

    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Cunning) >= 7 && !owner.has(Trait.direct)
    }

    override fun copy(): Trap {
        return Decoy()
    }
}

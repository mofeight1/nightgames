package trap

import characters.Attribute
import characters.Character
import characters.Pool
import global.Global

class IllusionTrap : Trap(
    "Illusion Trap",
    "A seductive image that will arouse the victim",
    "You cast a simple illusion that will trigger when someone approaches and seduce them.",
    3
) {
    override var owner: Character? = null

    init {
        cost[Pool.MOJO] = 15
    }

    override fun trigger(target: Character) {
        if (target.human()) {
            Global.gui.message(
                "You run into a girl you don't recognize, but she's beautiful and completely naked. You don't have a chance to wonder where she came from, because " +
                        "she immediately presses her warm, soft body against you and kisses you passionately. She slips a hand between you to grope your crotch and suddenly vanishes. " +
                        "She was just an illusion, but your erection is very real."
            )
        } else if (target.location.humanPresent()) {
            Global.gui.message("There's a flash of pink light and " + target.name + " flushes with arousal")
        }
        target.tempt(25.0)
        target.location.opportunity(target, this)
        cost[Pool.MOJO] = 15
    }

    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Arcane) >= 6
    }

    override fun copy(): Trap {
        return IllusionTrap()
    }
}

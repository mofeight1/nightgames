package trap

import characters.Attribute
import characters.Character
import characters.Trait
import global.Global
import items.Component
import items.Flask

class DissolvingTrap : Trap(
    "Dissolving Trap",
    "Will completely strip the victim",
    "You rig up a trap to dissolve the clothes of whoever triggers it.",
    3
) {
    override var owner: Character? = null

    init {
        recipe[Component.Tripwire] = 1
        recipe[Flask.DisSol] = 1
        recipe[Component.Sprayer] = 1
    }

    override fun trigger(target: Character) {
        if (!target.check(Attribute.Perception, 20 - (target[Attribute.Perception] + target.bonusDisarm()))) {
            if (target.human()) {
                Global.gui
                    .message("You spot a liquid spray trap in time to avoid setting it off. You carefully manage to disarm the trap and pocket the potion.")
                target.gain(Flask.DisSol)
                target.location.remove(this)
            }
        } else {
            if (target.human()) {
                if (target.isNude) {
                    Global.gui.message(
                        "Your bare foot hits a tripwire and you brace yourself as liquid rains down on you. You hastily do your best to brush the liquid off, " +
                                "but after about a minute you realize nothing has happened. Maybe the trap was a dud."
                    )
                } else {
                    Global.gui.message(
                        "You are sprayed with a clear liquid. Everywhere it lands on clothing, it immediately dissolves it, but it does nothing to your skin. " +
                                "You try valiantly to save enough clothes to preserve your modesty, but you quickly end up naked."
                    )
                }
            } else if (target.location.humanPresent()) {
                if (target.isNude) {
                    Global.gui
                        .message(target.name + " is caught in your clothes dissolving trap, but she was already naked. Oh well.")
                } else {
                    Global.gui.message(
                        target.name + " is caught in your trap and is showered in dissolving solution. In seconds, her clothes vanish off her body, leaving her " +
                                "completely nude."
                    )
                }
            }
            target.nudify()
            target.location.opportunity(target, this)
        }
    }


    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Cunning) >= 17 && !owner.has(Trait.direct)
    }

    override fun copy(): Trap {
        return DissolvingTrap()
    }
}

package trap

import characters.Attribute
import characters.Character
import global.Global
import items.Component
import status.Bound

class Snare : Trap(
    "Snare",
    "Temporarily binds the victim",
    "You carefully rig up a complex and delicate system of ropes on a tripwire. In theory, it should be able to bind whoever triggers it.",
    3
) {
    init {
        recipe[Component.Tripwire] = 1
        recipe[Component.Rope] = 1
    }

    override fun trigger(target: Character) {
        if (target.check(Attribute.Perception, 20 - (target[Attribute.Perception] + target.bonusDisarm()))) {
            if (target.human()) {
                Global.gui.message("You notice a snare on the floor in front of you and manage to disarm it safely")
            }
            target.location.remove(this)
        } else {
            target.add(Bound(target, 30, "rope"))
            if (target.human()) {
                Global.gui
                    .message("You hear a sudden snap and you're suddenly overwhelmed by a blur of ropes. The tangle of ropes trip you up and firmly bind your arms.")
            } else if (target.location.humanPresent()) {
                Global.gui
                    .message(target.name + " enters the room, sets off your snare, and ends up thoroughly tangled in rope.")
            }
            target.location.opportunity(target, this)
        }
    }

    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Cunning) >= 11
    }

    override fun copy(): Trap {
        return Snare()
    }
}

package trap

import characters.Anatomy
import characters.Attribute
import characters.Character
import global.Global
import items.Component
import status.Protected
import status.Winded

class SpringTrap : Trap(
    "Spring Trap", "A spring-loaded trap aimed at the nether region",
    "You manage to rig up a makeshift booby trap, which should prove quite unpleasant to any who stumbles upon it.", 3
) {
    init {
        recipe[Component.Spring] = 1
        recipe[Component.Rope] = 1
    }

    override fun trigger(target: Character) {
        if (!target.check(Attribute.Perception, 24 - (target[Attribute.Perception] + target.bonusDisarm()))) {
            if (target.human()) {
                Global.gui.message(
                    "As you're walking, your foot hits something and there's a sudden debilitating pain in your groin. Someone has set up a spring-loaded rope designed " +
                            "to shoot up into your nuts, which is what just happened. You collapse into the fetal position and pray that there's no one nearby."
                )
            } else if (target.location.humanPresent()) {
                Global.gui.message(
                    "You hear a sudden yelp as your trap catches " + target.name + " right in the cooch. She eventually manages to extract the rope from between her legs " +
                            "and collapses to the floor in pain."
                )
            }
            target.pain(20.0, Anatomy.genitals)
            target.add(Winded(target, owner!!))
            target.add(Protected(target))
            target.location.opportunity(target, this)
        } else if (target.human()) {
            Global.gui.message(
                "You spot a suspicious mechanism on the floor and prod it from a safe distance. A spring loaded line shoots up to groin height, which would have been " +
                        "very unpleasant if you had kept walking."
            )
            target.location.remove(this)
        }
    }

    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Cunning) >= 10
    }

    override fun copy(): Trap {
        return SpringTrap()
    }
}

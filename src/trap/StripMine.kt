package trap

import characters.Attribute
import characters.Character
import characters.Pool
import global.Global
import items.Component

class StripMine : Trap(
    "Strip Mine", "A high-tech trap that will destroy a layer of clothing",
    "Using the techniques Jett showed you, you rig up a one-time-use clothing destruction device.", 3
) {
    init {
        recipe[Component.Tripwire] = 1

        cost[Pool.BATTERY] = 3
    }

    override fun trigger(target: Character) {
        if (target.human()) {
            if (target.isNude) {
                Global.gui
                    .message("You're momentarily blinded by a bright flash of light. A camera flash maybe? Is someone taking naked pictures of you?")
            } else {
                Global.gui.message(
                    "You're suddenly dazzled by a bright flash of light. As you recover from your disorientation, you notice that it feel a bit drafty. " +
                            "You find you're missing some clothes. You reflect that your clothing expenses have gone up significantly since you joined the Games."
                )
            }
        } else if (target.location.humanPresent()) {
            Global.gui
                .message("You're startled by a flash of light not far away. Standing there is a half-naked " + target.name + ", looking surprised.")
        }
        if (target.tops.isNotEmpty()) {
            target.shred(Character.OUTFITTOP)
        }
        if (target.bottoms.isNotEmpty()) {
            target.shred(Character.OUTFITBOTTOM)
        }
        target.location.opportunity(target, this)
    }

    override fun requirements(owner: Character): Boolean {
        return owner.getPure(Attribute.Science) >= 4
    }

    override fun copy(): Trap {
        return StripMine()
    }
}

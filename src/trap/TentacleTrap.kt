package trap

import characters.Anatomy
import characters.Character
import global.Global
import items.Component
import status.Hypersensitive
import status.Oiled

class TentacleTrap : Trap(
    "Tentacle Trap", "Extremely powerful, but only works on naked opponents",
    "You need to activate this phallic totem before it can be used as a trap. You stroke the small totem with your hand, which is... weird, but effective. You " +
            "quickly place the totem someplace out of sight and hurriedly get out of range. You're not sure whether this will actually discriminate before attacking.",
    3
) {
    init {
        recipe[Component.Totem] = 1
    }

    override fun trigger(target: Character) {
        if (target.isNude) {
            if (target.human()) {
                Global.gui.message(
                    "An unearthly glow appears from the floor surrounding you and at least a dozen tentacles burst from the floor. Before you can react, you're lifted helpless " +
                            "into the air. The tentacles assault you front and back, wriggling around you nipples and cock, while one persistent tentacle forces its way into your ass. The overwhelming " +
                            "sensations and violation keep you from thinking clearly and you can't even begin to mount a reasonable resistance. Just as suddenly as they attacked you, the tentacles " +
                            "are gone, dumping you unceremoniously to the floor. You're left coated in a slimy liquid that, based on your rock-hard erection, seems to be a powerful aphrodisiac. Holy " +
                            "fucking hell...."
                )
            } else if (target.location.humanPresent()) {
                Global.gui.message(
                    target.name + " gets caught in the tentacle trap and is immediately surrounded by penis-shaped tentacles. Before she can escape, they bind her " +
                            "limbs and start probing and caressing her naked body. The tentacles start to ooze out lubricant and two tentacles penetrate her vaginally and anally. A third " +
                            "tentacle slips into her mouth, while the rest frot against her body. They gang-bang her briefly, but thoroughly, before squirting liquid over her and disappearing " +
                            "back into the floor. She'll left shivering, sticky, and unsatisfied. In effect, she's already defeated."
                )
            }
            target.pleasure(target.arousal.max.toDouble(), Anatomy.genitals)
            target.calm(1.0)
            target.add(Oiled(target))
            target.add(Hypersensitive(target))
            target.location.opportunity(target, this)
        } else {
            if (target.human()) {
                Global.gui.message(
                    """
    Holy hell! A dozen large tentacles shoot out of the floor in front of you and thrash wildly. You freeze, hoping they won't notice you, but it seems futile. the tentacles approach you from all sides, poking at you tentatively. As suddenly as they appeared, the tentacles vanish back into the floor. 
    ...Is that it? You're safe... you guess?
    """.trimIndent()
                )
            } else if (target.location.humanPresent()) {
                Global.gui.message(
                    target.name + " stumbles into range of the fetish totem. A cage of phallic tentacles appear around her. They apparently aren't interested in her and " +
                            "they disappear, leaving her slightly bewildered."
                )
            }
        }
    }

    override fun requirements(owner: Character): Boolean {
        return owner.rank > 0
    }

    override fun copy(): Trap {
        return TentacleTrap()
    }
}

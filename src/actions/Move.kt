package actions

import areas.Area
import characters.Character

open class Move(val destination: Area) : Action("Move(" + destination.name + ")") {
    override fun usable(user: Character): Boolean {
        return true
    }

    override fun execute(user: Character): Movement {
        user.travel(destination)
        return destination.areaId
    }

    override fun consider(): Movement {
        return destination.areaId
    }
}

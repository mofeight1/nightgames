package actions

import areas.Area
import characters.Attribute
import characters.Character

class Shortcut(destination: Area) : Move(destination) {
    init {
        name = "Shortcut(" + destination.name + ")"
    }

    override fun usable(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 28
    }
}

package actions

import characters.Attribute
import characters.Character
import characters.Pool
import global.Global

class Recharge :
    Action("Recharge", "Jett has a power supply in here that can rapidly recharge the battery in your multitool") {
    override fun usable(user: Character): Boolean {
        return user.location.recharge && user.getPure(Attribute.Science) > 0 && !user[Pool.BATTERY].isFull
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            Global.gui.message("You find a power supply and restore your batteries to full.")
        }
        user.chargeBattery()
        return Movement.recharge
    }

    override fun consider(): Movement {
        return Movement.recharge
    }
}

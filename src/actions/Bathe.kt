package actions

import characters.Character
import characters.State
import global.Global

class Bathe : Action("Clean Up", "Recovers your stamina, but leaves you vulnerable to ambush.") {
    override fun usable(user: Character): Boolean {
        return user.location.bath
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            if (user.location.name === "Showers") {
                Global.gui.message("It's a bit dangerous, but a shower sounds especially inviting right now.")
            } else if (user.location.name === "Pool") {
                Global.gui.message("There's a jacuzzi in the pool area and you decide to risk a quick soak.")
            }
        }
        user.state = State.shower
        user.delay(1)
        return Movement.bathe
    }

    override fun consider(): Movement {
        return Movement.bathe
    }
}

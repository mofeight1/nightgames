package actions

import java.awt.Color

enum class Movement(val description: String, val traceDesc: String, val color: Color) {
    quad(" head outside, toward the quad.", " moved to the quad ", Color(250, 250, 250)),
    kitchen(" move into the kitchen.", " moved to the kitchen ", Color(250, 250, 250)),
    dorm(" move to the first floor of the dorm.", " moved to the dorm ", Color(250, 250, 250)),
    shower(" run into the showers.", " moved into the showers ", Color(250, 250, 250)),
    storage(" enter the storage room.", " moved to the storage room ", Color(250, 250, 250)),
    dining(" head to the dining hall.", " moved to the dining hall ", Color(250, 250, 250)),
    laundry(" move to the laundry room.", " moved to the laundry room ", Color(250, 250, 250)),
    tunnel(" move into the tunnel.", " moved to the tunnel ", Color(250, 250, 250)),
    bridge(" move to the bridge.", " moved to the bridge ", Color(250, 250, 250)),
    engineering(
        " head to the first floor of the engineering building.",
        " moved to the engineering building ",
        Color(250, 250, 250)
    ),
    workshop(" enter a workshop.", " moved to the workshop ", Color(250, 250, 250)),
    lab(" enter one of the chemistry labs.", " moved to the lab ", Color(250, 250, 250)),
    la(" move to the liberal arts building.", " moved to the liberal arts building ", Color(250, 250, 250)),
    library(" enter the library.", " moved to the library ", Color(250, 250, 250)),
    pool(" move to the indoor pool.", " went into the pool ", Color(250, 250, 250)),
    union(" head toward the student union.", " moved to the student union ", Color(250, 250, 250)),
    hide(" disappear into a hiding place.", " spent time hiding ", Color(200, 200, 200)),
    trap(" start rigging up something weird, probably a trap.", " rigged a trap here ", Color(120, 200, 120)),
    bathe(" start bathing in the nude, not bothered by your presence.", " got cleaned up ", Color(170, 200, 250)),
    scavenge(" begin scrounging through some boxes in the corner.", " rummaged around here ", Color(220, 220, 150)),
    craft(
        " start mixing various liquids. Whatever it is doesn't look healthy.",
        " brewed something ",
        Color(220, 220, 150)
    ),
    wait(" loitering nearby", " loitered here ", Color(200, 200, 200)),
    resupply(
        " heads for one of the safe rooms, probably to get a change of clothes.",
        " got dressed ",
        Color(170, 200, 250)
    ),
    oil(
        " rubbing body oil on her every inch of her skin. Wow, you wouldn't mind watching that again.",
        " spilled some body oil ",
        Color(100, 250, 250)
    ),
    energydrink(
        " opening an energy drink and downing the whole thing.",
        " left an energy drink can here ",
        Color(100, 250, 250)
    ),
    beer(" opening a beer and downing the whole thing.", " dropped a beer can here ", Color(100, 250, 250)),
    recharge(
        " plugging a battery pack into a nearby charging station.",
        " used the power supply ",
        Color(170, 200, 250)
    ),
    locating(
        " is holding someone's underwear in her hands and breathing deeply. Strange.",
        " burned something ",
        Color(170, 200, 250)
    ),
    masturbate(
        " starts to pleasure herself, while trying not to make much noise. It's quite a show.",
        " touched herself here, leaving some drops of love juice ",
        Color(250, 170, 170)
    ),
    mana(
        " doing something with a large book. When she's finished, you can see a sort of aura coming from her.",
        " looked through these thick texts ",
        Color(170, 200, 250)
    ),
    retire(" has left the match.", " exited the match area ", Color(250, 100, 100)),
    potion(" drink an unidentifiable potion.", " dropped an empty plastic container here ", Color(100, 250, 250)),
    fight(" has a fight.", " fought here ", Color.WHITE),
    none("","", Color(0,0,0));
}

package actions

import characters.Character

class Wait : Action("Wait", "Wait for 5 minutes") {
    override fun usable(user: Character): Boolean {
        return true
    }

    override fun execute(user: Character): Movement {
        return Movement.wait
    }

    override fun consider(): Movement {
        return Movement.wait
    }
}

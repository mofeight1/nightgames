package actions

import areas.NinjaStash
import characters.Character
import characters.State
import global.Global
import global.Modifier
import global.Scheduler

class Resupply : Action(
    "Resupply",
    "Retrieve replacement clothes and check in with the officials, making you eligible to fight opponents who recently defeated you"
) {
    override fun usable(user: Character): Boolean {
        return user.location.resupply || user.location.hasStash(user)
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            if (Scheduler.match!!.condition == Modifier.nudist) {
                Global.gui
                    .message("You check in so you're eligible to fight again, but you still don't get any clothes.")
            } else {
                Global.gui.message("You pick up a change of clothes and prepare to get back in the fray.")
            }
        }
        val stashes = user.location.getDeployables(NinjaStash(user))
        (stashes.firstOrNull { it.owner === user } as NinjaStash?)?.collect(user)
        user.state = State.resupplying
        return Movement.resupply
    }

    override fun consider(): Movement {
        return Movement.resupply
    }
}

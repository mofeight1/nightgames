package actions

import characters.Attribute
import characters.Character
import global.Global
import status.Energized
import status.Stsflag

class Energize : Action("Absorb Mana", "Use Aisha's spellbook to fill your Mojo") {
    override fun usable(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 1 &&
                user.location.mana && !user.has(Stsflag.energized)
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            Global.gui.message(
                "You duck into the creative writing room and find a spellbook sitting out in the open. Aisha must have left it for you. The spellbook builds mana " +
                        "continuously and the first lesson you learned was how to siphon off the excess. You absorb as much as you can hold, until you're overflowing with mana."
            )
        }
        user.mojo.fill()
        user.add(Energized(user, 20))
        return Movement.mana
    }

    override fun consider(): Movement {
        return Movement.mana
    }
}

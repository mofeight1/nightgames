package actions

import characters.Character
import global.Global
import global.Modifier
import global.Scheduler
import items.Flask
import items.Item
import status.Oiled

class Use(item: Item) : Action("Use " + item.getName(), item.desc) {
    private val item: Item

    init {
        if (item === Flask.Lubricant) {
            name = "Oil up"
        }
        this.item = item
    }

    override fun usable(user: Character): Boolean {
        return user.has(item) && (!user.human() || Scheduler.match!!.condition != Modifier.noitems)
    }

    override fun execute(user: Character): Movement {
        if (item === Flask.Lubricant) {
            if (user.human()) {
                Global.gui
                    .message("You cover yourself in slick oil. It's a weird feeling, but it should make it easier to escape from a hold.")
            }
            user.add(Oiled(user))
            user.consume(Flask.Lubricant, 1)
            return Movement.oil
        }

        return Movement.wait
    }

    override fun consider(): Movement {
        if (item === Flask.Lubricant) {
            return Movement.oil
        }
        return Movement.wait
    }
}

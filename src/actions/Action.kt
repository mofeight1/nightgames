package actions

import characters.Character


abstract class Action {
    protected var name: String
    var tooltip: String
        protected set

    constructor(name: String) {
        this.name = name
        this.tooltip = ""
    }

    constructor(name: String, tooltip: String) {
        this.name = name
        this.tooltip = tooltip
    }

    abstract fun usable(user: Character): Boolean
    abstract fun execute(user: Character): Movement
    override fun toString(): String {
        return name
    }

    abstract fun consider(): Movement
    open val freeAction = false
}

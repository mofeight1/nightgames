package actions

import characters.Character
import characters.State
import global.Global
import global.Modifier
import global.Scheduler

class JerkOff : Action("Masturbate", "Empties your Arousal, but leaves you extremely vulnerable to ambush") {
    override fun usable(user: Character): Boolean {
        return user.arousal.current >= 15 &&
                !(user.human() && Scheduler.match!!.condition == Modifier.norecovery)
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            Global.gui.message("You desperately need to deal with your erection before you run into an opponent. You find an isolated corner and quickly jerk off.")
        }
        user.state = State.masturbating
        user.delay(1)
        return Movement.masturbate
    }

    override fun consider(): Movement {
        return Movement.masturbate
    }
}

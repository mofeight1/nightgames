package actions

import characters.Character
import characters.Trait
import global.Global

class Locate(private val target: Character) :
    Action("Locate " + target.name, "Teleport yourself to " + target.name + "'s location.") {
    override fun usable(user: Character): Boolean {
        return user.has(Trait.locator) && user.has(target.underwear!!) && target !== user
    }

    override fun execute(user: Character): Movement {
        val area = target.location
        Global.gui.clearText()
        Global.gui.message(
            "Focusing on the essence contained in the "
                    + target.underwear
                    + ". In your mind, an image of the "
                    + area.name
                    + " appears. It falls apart as quickly as it came to be, but you know where "
                    + target.name + " currently is. Your hard-earned trophy is already burning up into a portal leading to your prey."
        )
        user.spendArousal(15)
        user.consume(target.underwear!!, 1)
        user.location.exit(user)
        area.enter(user)
        return Movement.locating
    }

    override fun consider(): Movement {
        return Movement.locating
    }

    override val freeAction = true
}

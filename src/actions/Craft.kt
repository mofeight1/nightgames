package actions

import characters.Attribute
import characters.Character
import characters.State

class Craft : Action("Craft Potion", "Use nearby ingredients and cunning to make usable flasks and potions") {
    override fun usable(user: Character): Boolean {
        return user.location.potions && user[Attribute.Cunning] > 5
    }

    override fun execute(user: Character): Movement {
        user.state = State.crafting
        return Movement.craft
    }

    override fun consider(): Movement {
        return Movement.craft
    }
}

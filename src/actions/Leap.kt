package actions

import areas.Area
import characters.Attribute
import characters.Character

class Leap(destination: Area) : Move(destination) {
    init {
        name = "Ninja Leap(" + destination.name + ")"
    }

    override fun usable(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 5
    }
}

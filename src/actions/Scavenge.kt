package actions

import characters.Character
import characters.State

class Scavenge : Action("Scavenge Items", "Search the nearby boxes and cabinets for anything useful") {
    override fun usable(user: Character): Boolean {
        return user.location.materials
    }

    override fun execute(user: Character): Movement {
        user.state = State.searching
        return Movement.scavenge
    }

    override fun consider(): Movement {
        return Movement.scavenge
    }
}

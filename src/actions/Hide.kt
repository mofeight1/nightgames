package actions

import characters.Character
import characters.State
import global.Global

class Hide :
    Action("Hide", "Gives an opportunity to ambush opponents who fail a Perception check against your Cunning") {
    override fun usable(user: Character): Boolean {
        return false
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            Global.gui.message("You find a decent hiding place and wait for unwary opponents.")
        }
        user.state = State.hidden
        return Movement.hide
    }

    override fun consider(): Movement {
        return Movement.hide
    }
}

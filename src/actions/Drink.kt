package actions

import characters.Character
import global.Global
import global.Modifier
import global.Scheduler
import items.Potion

class Drink(private val item: Potion) : Action(item.getName(), item.desc) {
    override fun usable(user: Character): Boolean {
        return user.has(item) && (!user.human() || Scheduler.match!!.condition != Modifier.noitems)
    }

    override fun execute(user: Character): Movement {
        if (user.human()) {
            Global.gui.message(item.getMessage(user))
        }
        user.add(item.getEffect(user))
        user.consume(item, 1)
        return item.action
    }

    override fun consider(): Movement {
        return item.action
    }
}

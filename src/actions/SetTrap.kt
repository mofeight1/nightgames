package actions

import characters.Character
import global.Global
import global.Modifier
import global.Scheduler
import trap.Trap

class SetTrap(private val trap: Trap) : Action("Set($trap)") {
    override fun usable(user: Character): Boolean {
        return trap.recipe(user) &&
                !user.location.open &&
                trap.requirements(user) &&
                user.location.env.size < 5 &&
                (!user.human() || Scheduler.match!!.condition != Modifier.noitems)
    }

    override fun execute(user: Character): Movement {
        val copy = trap.place(user, user.location)
        val message: String = copy.setup
        if (user.human()) {
            Global.gui.message(message)
        }

        return Movement.trap
    }

    override fun consider(): Movement {
        return Movement.trap
    }
}

package combat

enum class DamageType {
    Tempt,
    Pleasure,
    Weaken,
    Pain,
    Calm,
    Heal,
}
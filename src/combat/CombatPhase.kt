package combat

enum class CombatPhase {
    Start,
    Upkeep,
    Choose,
    Actions,
    End
}
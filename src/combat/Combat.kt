package combat

import areas.Area
import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.NPC
import characters.State
import characters.Trait
import daytime.Activity
import global.Flag
import global.Global
import global.Modifier
import global.Roster
import gui.GUI
import items.Clothing
import items.ClothingType
import items.Item
import items.OwnedItem
import items.Potion
import pet.Pet
import skills.Skill
import skills.SkillTag
import stance.Neutral
import stance.Position
import stance.Stance
import stance.StandingOver
import status.Protected
import status.Status
import status.Winded
import java.util.Observable
import java.util.Observer
import kotlin.math.min
import kotlin.math.roundToInt

class Combat(var p1: Character, var p2: Character, private val rules: List<Modifier>) : Observable(), Cloneable {
    var cPhase: CombatPhase = CombatPhase.Start
    var guiWatchers = ArrayList<Observer>()
    var phase: Int = 0
    var enc: Encounter? = null
    private var actions: HashMap<Character, Skill?> = HashMap()
    private var events: HashMap<Character, ArrayList<CombatEvent>>
    private val audience: ArrayList<Character> = ArrayList()
    var location: Area? = null
    var message: String = ""
        private set
    var stance: Position
    var clothespile: ArrayList<OwnedItem<Clothing>> = ArrayList()
    var used: HashMap<Character, ArrayList<Item>>
    private var timer: Int = 0
    var state: Result? = null
    private var images: HashMap<String, String> = HashMap()
    var isOver: Boolean = false
        private set
    private var parentActivity: Activity? = null

    init {
        stance = Neutral(p1, p2)
        used = hashMapOf(p1 to ArrayList(), p2 to ArrayList())
        events = hashMapOf(p1 to ArrayList(), p2 to ArrayList())
        p1.state = State.combat
        p2.state = State.combat
        p1.preCombat(p2, this)
        p2.preCombat(p1, this)
    }
    fun addWatcher(o: Observer) {
        if (!guiWatchers.contains(o))
            guiWatchers.add(o)
    }
    fun update() {
        //enc?.update()
        for (watcher in guiWatchers) {
            if (watcher is GUI) watcher.update()
        }
    }
    fun getOther(player: Character): Character {
        return if (p1 === player) {
            p2
        } else {
            p1
        }
    }

    fun go() {
        phase = 0
        cPhase = CombatPhase.Upkeep
        if (!(p1.human() || p2.human())) {
            automate()
        }
        //this.setChanged()
        //this.notifyObservers()
        update()
    }

    fun intervene(intruder: Character, assist: Character) {
        val target = if (p1 === assist) {
            p2
        } else {
            p1
        }
        //p1.undress(this)
        //p2.undress(this)
        if (intruder.human()) {
            Global.gui.watchCombat(this)
            Global.gui.loadTwoPortraits(p1, p2)
        } else if (p1.human()) {
            Global.gui.loadTwoPortraits(p2, intruder)
        } else if (p2.human()) {
            Global.gui.loadTwoPortraits(p1, intruder)
        }
        if (target.resist3p(this, intruder, assist)) {
            target.gainXP(20, intruder)
            intruder.gainXP(10, target)
            intruder.resetArousal()
            intruder.undress(this)
            if (this.clothespile.any { intruder.outfit[1].first() == it.item && it.owner == intruder }) {
                target.gain(intruder.underwear!!)
            }
            intruder.defeated(target)
            intruder.defeated(assist)
        } else {
            intruder.gainXP(10, target)
            assist.gainXP(15, target)
            target.gainXP(15 + target.lvlBonus(intruder) + target.lvlBonus(assist))
            target.resetArousal()
            assist.undress(this)
            target.undress(this)
            assist.dress(this)
            if (underwearIntact(target))
                assist.gain(target.underwear!!)
            target.defeated(intruder)
            target.defeated(assist)
            intruder.intervene3p(this, target, assist)
            assist.victory3p(this, target, intruder)
            assist.gainAttraction(intruder, 1)
            target.gainAttraction(assist, 1)
            phase = 2
            cPhase = CombatPhase.End
            if (!(p1.human() || p2.human() || intruder.human())) {
                end()
            } else if (intruder.human()) {
                Global.gui.watchCombat(this)
            }
        }
        //this.setChanged()
        //this.notifyObservers()
        update()
    }

    fun draw (p1: Character, p2: Character) {
        state = eval(null)
        p1.evalChallenges(this, null)
        p2.evalChallenges(this, null)

        p1.gainXP(15, p2)
        p2.gainXP(15, p1)
        p1.resetArousal()
        p2.resetArousal()
        p1.undress(this)
        p2.undress(this)
        if (underwearIntact(p1))
            p2.gain(p1.underwear!!)
        if (underwearIntact(p2))
            p1.gain(p2.underwear!!)
        p1.defeated(p2)
        p2.defeated(p1)
        p1.draw(this, state!!)
        p2.draw(this, state!!)
        Roster.gainAttraction(p1.id, p2.id, 4)
        if (Roster.getAffection(p1.id, p2.id) > 0) {
            Roster.gainAffection(p1.id, p2.id, 1)
            if (p1.has(Trait.affectionate) || p2.has(Trait.affectionate)) {
                Roster.gainAffection(p1.id, p2.id, 1)
            }
        }

        for (viewer in audience) {
            if (Global.random(2) == 0) {
                viewer.watcher(this, p1, p2)
            } else {
                viewer.watcher(this, p2, p1)
            }
        }
    }

    fun victory(winner: Character, loser: Character) {
        state = eval(winner)
        winner.evalChallenges(this, winner)
        loser.evalChallenges(this, winner)
        winner.victory(this, state!!)
        loser.defeat(this, state!!)
        winner.gainXP(15, loser)
        loser.gainXP(15, winner)
        if (stance.penetration(winner)) {
            winner.mojo.gainMax(2)
            if (winner.has(Trait.mojoMaster)) {
                winner.mojo.gainMax(2)
            }
        }
        loser.resetArousal()
        if (!winner.has(Trait.nosatisfaction))
            winner.resetArousal()

        if (hasModifier(Modifier.practice)) {
            return
        }
        winner.undress(this)
        loser.undress(this)
        if (underwearIntact(loser))
            winner.gain(loser.underwear!!)
        winner.dress(this)
        loser.defeated(winner)
        Roster.gainAttraction(winner.id, loser.id, 2)
        if (loser.has(Trait.gracefulloser)) {
            Roster.gainAttraction(winner.id, loser.id, 2)
        }

        for (viewer in audience) {
            viewer.watcher(this, winner, loser)
        }
    }
    fun eval(victor: Character?): Result {
        if (victor != null) {
            if ((stance.en == Stance.anal || stance.en == Stance.analm)) {
                return Result.anal
            }
            if ((stance.en == Stance.facesitting)) {
                return Result.facesitting
            }
        }
        return if (stance.penetration(p1) || stance.penetration(p2)) {
            Result.intercourse
        } else {
            Result.normal
        }
    }
    fun checkLoss(): Boolean {
        //val p1Lose = p1.arousal.isFull
        //val p2Lose = p2.arousal.isFull
        val p1Lose = willOrgasm(p1)
        val p2Lose = willOrgasm(p2)
        if (!p1Lose && !p2Lose) return false

        var winner: Character? = null
        var loser: Character? = null
        if (p1Lose && !p2Lose) {
            winner = p2
            loser = p1
        }
        if (p2Lose && !p1Lose) {
            winner = p1
            loser = p2
        }
        if (winner != null) victory(winner, loser!!)
        else draw(p1, p2)
        cPhase = CombatPhase.End
        phase = 2
        //this.setChanged()
        //this.notifyObservers()
        update()
        if (!(p1.human() || p2.human())) {
            end()
        }
        return true
    }

    fun upkeep() {
        p1.regen(true)
        p2.regen(true)
        events[p1]!!.clear()
        events[p2]!!.clear()
        phase = 1
        cPhase = CombatPhase.Choose
    }

    fun turn() {
        if (checkLoss()) return

        message = p2.describe(p1[Attribute.Perception]) + "<p>" + stance.describe() + "<p>" +
            p1.describe(p2[Attribute.Perception]) + "<p>"
        if (p1.human() || p2.human()) {
            val commenter = getOther(Global.player) as NPC
            val comment = commenter.getComment(this)
            write(commenter, comment)
        }
        upkeep()
        actions[p1] = null
        actions[p2] = null
        p1.act(this)
        p2.act(this)

        //this.setChanged()
        //this.notifyObservers()
        update()
    }
    fun evalAct() {
        cPhase = CombatPhase.Actions
        clear()
        if (Global.debug) {
            println("${p1.name} uses ${actions[p1]}")
            println("${p2.name} uses ${actions[p2]}")
        }
        if (p1.pet != null && p2.pet != null) {
            petbattle(p1.pet!!, p2.pet!!)
        } else if (p1.pet != null) {
            p1.pet!!.act(this, p2)
        } else if (p2.pet != null) {
            p2.pet!!.act(this, p1)
        }
        val skills = ArrayDeque(actions.filterValues { it != null }.toList())
        skills.sortByDescending { it.first.init(this) + it.second!!.speed }
        for ((user, skill) in skills) {
            val skill = skill!!
            val target = skill.target!!
            if (skill.usable(this, target)) {
                skill.resolve(this, target)
                addEvent(user, CombatEvent(Result.useskill, skill))
                addEvent(target, CombatEvent(Result.receiveskill, skill))
                checkStamina(p1)
                checkStamina(p2)
            }
        }
        /*if (p1.init(this) + actions[p1]!!.speed >= p2.init(this) + actions[p2]!!.speed
        ) {
            if (actions[p1]!!.usable(this, p2)) {
                actions[p1]!!.resolve(this, p2)
                addEvent(p1, CombatEvent(Result.useskill, actions[p1]))
                addEvent(p2, CombatEvent(Result.receiveskill, actions[p1]))
                checkStamina(p2)
            }
            if (actions[p2]!!.usable(this, p1)) {
                actions[p2]!!.resolve(this, p1)
                addEvent(p2, CombatEvent(Result.useskill, actions[p2]))
                addEvent(p1, CombatEvent(Result.receiveskill, actions[p2]))
                checkStamina(p1)
            }
        } else {
            if (actions[p2]!!.usable(this, p1)) {
                actions[p2]!!.resolve(this, p1)
                addEvent(p2, CombatEvent(Result.useskill, actions[p2]))
                addEvent(p1, CombatEvent(Result.receiveskill, actions[p2]))
                checkStamina(p1)
            }
            if (actions[p1]!!.usable(this, p2)) {
                actions[p1]!!.resolve(this, p2)
                addEvent(p1, CombatEvent(Result.useskill, actions[p1]))
                addEvent(p2, CombatEvent(Result.receiveskill, actions[p1]))
                checkStamina(p2)
            }
        }*/
        p1.eotFull(this, p2)
        p2.eotFull(this, p1)
        stance.decay()
        stance.checkOngoing(this)
    }

    fun checkStamina(p: Character) {
        if (!p.stamina.isEmpty) return
        if (p.has(Trait.planB) && (p.has(Potion.EnergyDrink) || p.has(Potion.SuperEnergyDrink))) {
            if (p.has(Potion.EnergyDrink)) {
                p.consume(Potion.EnergyDrink, 1)
            } else {
                p.consume(Potion.SuperEnergyDrink, 1)
            }
            if (p.human()) {
                write("Your legs feel weak, but you pop open an energy drink and down it to recover.")
            } else {
                write(
                    "${p.name} pulls a hidden energy drink out of her sleeve and chugs it. "
                        + "It seems to be enough to keep her on her feet."
                )
            }
            p.heal((p.stamina.max / 20).toDouble(), this)
            return
        }
        val other = if (p === p1) {
            p2
        } else {
            p1
        }
        p.add(Winded(p, other))
        p.add(Protected(p))
        if (stance.prone(p)) return
        if ((stance.penetration(p) || stance.penetration(other)) && stance.dom(other)) {
            if (p.human()) {
                write("Your legs give out, but ${other.name} holds you up.")
            } else {
                write("${p.name} slumps in your arms, but you support her to keep her from collapsing.")
            }
            return
        }
        stance = StandingOver(other, p)
        var lowblow = events[p]?.any { it.event == Result.receivepain && it.part == Anatomy.genitals } ?: false
        if (p.human()) {
            if (lowblow) {
                write("You crumple into the fetal position, cradling your sore testicles.")
            } else {
                write("You don't have the strength to stay on your feet. You slump to the floor.")
            }
        } else {
            if (lowblow) {
                if (p.hasBalls) {
                    write("${p.name} falls to the floor with tears in her eyes, clutching her plums.")
                } else {
                    write("${p.name} falls to the floor with tears in her eyes, holding her sensitive girl parts.")
                }
            } else {
                write("${p.name} drops to the floor, exhausted.")
            }
            if (!Global.checkFlag(Flag.exactimages) || p.id === ID.SOFIA) {
                offerImage("Sofia busted.jpg", "Art by AimlessArt")
            }
        }
    }

    fun act(c: Character, action: Skill) {
        if (c == p1) action.target = p2
        if (c == p2) action.target = p1
        actions[c] = action
        if (actions[p1] == null || actions[p2] == null) return
        evalAct()

        if (p1.human() || p2.human()) {
            val commenter = getOther(Global.player) as NPC
            val comment = commenter.getResponse(this)
            write(commenter, comment)
        }

        this.phase = 0
        this.cPhase = CombatPhase.Upkeep
        if (!(p1.human() || p2.human())) {
            timer++
            turn()
        }
        //this.setChanged()
        //this.notifyObservers()
        update()
    }

    fun automate() {
        while (!checkLoss()) {
            upkeep()
            actions[p1] = (p1 as NPC).actFast(this)
            actions[p1]!!.target = p2
            actions[p2] = (p2 as NPC).actFast(this)
            actions[p2]!!.target = p1
            evalAct()

            this.phase = 0
            cPhase = CombatPhase.Upkeep
        }
    }

    fun willOrgasm (character: Character): Boolean {
        if (!character.arousal.isFull) return false

        if (character.getPure(Attribute.Contender) < 5) return true

        if (character.has(Trait.protagonist)) {
            if (Global.random(100) < min(60, character.getPure(Attribute.Contender) * 5)) {
                if (character.human()) {
                    write(
                        character,
                        "You feel yourself about to ejaculate, but you muster all of your grit and determination and push the urge down."
                    )
                    character.calm(character.arousal.max * .3, this)
                } else {
                    write(
                        character,
                        "You are sure ${character.name} is on the verge of cumming, but she grits her teeth and keeps fighting. " +
                            "Judging by the fire in her eyes, she's far from giving up."
                    )
                    character.calm(character.arousal.max * .3, this)
                }
                return false
            }
        } else {
            if (character.human()) {
                write(
                    character,
                    "You feel yourself about to ejaculate, but you muster all of your grit and determination and push the urge down."
                )
                character.calm(character.arousal.max * .3, this)
            } else {
                write(
                    character,
                    "You are sure ${character.name} is on the verge of cumming, but she grits her teeth and keeps fighting. " +
                        "Judging by the fire in her eyes, she's far from giving up."
                )
                character.calm(character.arousal.max * .3, this)
            }
            if (Global.random(100) < min(40.0, (character.getPure(Attribute.Contender) * 3).toDouble())) {
                if (character.human()) {
                } else {
                }
                return false
            }
        }
        return true
    }

    fun clear() {
        message = ""
    }

    fun writeLine() {
        message += "<br>"
    }

    fun write(text: String?) {
        if (text.isNullOrEmpty()) return
        message += "<br>$text"
    }

    fun write(user: Character, text: String?) {
        if (text.isNullOrEmpty()) return
        message += if (user.human()) {
            "<br><font color='rgb(100,100,200)'>$text<font color='black'><br>"
        } else {
            "<br><font color='rgb(200,100,100)'>$text<font color='black'><br>"
        }
    }

    fun report(text: String) {
        if (text.isEmpty()) return
        if (!Global.checkFlag(Flag.noReports)) {
            message += "<font color='rgb(100,100,100)'>$text<font color='black'><br>"
        }
    }

    fun drop(player: Character, item: Item) {
        used[player]!!.add(item)
        player.consume(item, 1)
    }

    fun next() {
        if (cPhase == CombatPhase.Upkeep)
            turn()
        else if (cPhase == CombatPhase.End)
            end()
    }

    fun end(): Boolean {
        clear()
        for (p in used.keys) {
            for (i in used[p]!!) {
                p.gain(i, 1)
            }
        }
        p1.state = State.ready
        p2.state = State.ready
        p1.endofbattle(p2, this)
        p2.endofbattle(p1, this)
        isOver = true
        var ding = false
        if (hasModifier(Modifier.practice)) {
            parentActivity!!.visit("PostCombat")
        } else {
            if (p1.xp >= 95 + (5 * p1.level)) {
                p1.ding()
                if (p1.human()) {
                    ding = true
                }
            }
            if (p2.xp >= 95 + (5 * p2.level)) {
                p2.ding()
                if (p2.human()) {
                    ding = true
                }
            }
        }
        enc?.end()
        return ding
    }

    fun petbattle(one: Pet, two: Pet) {
        var roll1 = Global.random(20) + one.power()
        var roll2 = Global.random(20) + two.power()
        if (one.gender() == Trait.female && two.gender() == Trait.male) {
            roll1 += 3
        } else if (one.gender() == Trait.male && two.gender() == Trait.female) {
            roll2 += 3
        }
        if (roll1 > roll2) {
            one.vanquish(this, two)
        } else if (roll2 > roll1) {
            two.vanquish(this, one)
        } else {
            write("${one.own()}$one and ${two.own()}$two engage each other for awhile, but neither can gain the upper hand.")
        }
    }

    public override fun clone(): Combat {
        val c = super.clone() as Combat
        c.p1 = p1.clone()
        c.p2 = p2.clone()
        c.clothespile = ArrayList()
        c.stance = stance.clone()
        c.used = hashMapOf(c.p1 to ArrayList(), c.p2 to ArrayList())
        c.actions = HashMap()
        c.events = hashMapOf(c.p1 to ArrayList(), c.p2 to ArrayList())
        c.timer = timer
        c.images = HashMap()
        if (c.stance.top === p1) c.stance.top = c.p1
        if (c.stance.top === p2) c.stance.top = c.p2
        if (c.stance.bottom === p1) c.stance.bottom = c.p1
        if (c.stance.bottom === p2) c.stance.bottom = c.p2
        return c
    }

    fun lastact(user: Character): Skill? {
        return if (user === p1) {
            actions[p1]
        } else if (user === p2) {
            actions[p2]
        } else {
            null
        }
    }

    fun offerImage(path: String, artist: String) {
        images[path] = artist
    }

    fun showImage() {
        var imagePath = ""
        if ((p1.human() || p2.human()) && !Global.checkFlag(Flag.noimage)) {
            if (images.isNotEmpty()) {
                imagePath = images.keys.random()
            }
            if (imagePath !== "") {
                Global.gui.displayImage(imagePath, images[imagePath])
            }
        }
        images.clear()
    }

    fun clearImages() {
        images.clear()
    }

    fun forfeit(player: Character?) {
        end()
    }

    fun reportDamage(target: Character, damage: Damage, mod: Result = Result.normal) {
        val other = getOther(target)
        val isHuman = if (target.human()) true
        else if (other.human()) false
        else null
        if (isHuman == null) return

        val canReadAdvanced = if (isHuman) target.canReadAdvanced(other)
        else other.canReadAdvanced(target)

        val canReadBasic = if (isHuman) target.canReadBasic(other)
        else other.canReadBasic(target)

        if (!canReadAdvanced && !canReadBasic) return

        fun reportType(type: DamageType, amount: Int) {
            when(type) {
                DamageType.Tempt -> {
                    var type = "temptation"
                    if (mod == Result.foreplay) {
                        type = "foreplay temptation"
                    } else if (mod == Result.finisher) {
                        type = "finishing temptation"
                    }
                    val basicDamage = target.getBasicArousalDamage(type, amount)
                    if (isHuman) {
                        if (canReadAdvanced) {
                            report("You are aroused by $amount $type.")
                        } else {
                            report("You are aroused by $basicDamage $type.")
                        }
                    } else {
                        if (canReadAdvanced) {
                            report("${target.name} receives $amount $type.")
                        } else {
                            report("${target.name} receives $basicDamage $type.")
                        }
                    }
                }
                DamageType.Pleasure -> {
                    var type = "pleasure"
                    if (mod == Result.foreplay) {
                        type = "foreplay pleasure"
                    } else if (mod == Result.finisher) {
                        type = "finishing pleasure"
                    }
                    val basicDamage = target.getBasicArousalDamage(type, amount)
                    if (isHuman) {
                        if (canReadAdvanced) {
                            report("You receive $amount $type.")
                        } else {
                            report("You are aroused by $basicDamage $type.")
                        }
                    } else {
                        if (canReadAdvanced) {
                            report("${target.name} receives $amount $type.")
                        } else {
                            report("${target.name} receives $basicDamage $type.")
                        }
                    }
                }
                DamageType.Weaken -> {
                    val basicWeaken = target.getBasicWeaken(amount)
                    if (isHuman) {
                        if (canReadAdvanced) {
                            report("You are weakened by $amount damage.")
                        } else {
                            report("You are weakened $basicWeaken.")
                        }
                    } else {
                        if (canReadAdvanced) {
                            report("${target.name} is weakened by $amount damage.")
                        } else {
                            report("${target.name} is weakened $basicWeaken.")
                        }
                    }
                }
                DamageType.Pain -> {
                    val basicPain = target.getBasicPain(amount)
                    if (isHuman) {
                        if (canReadAdvanced) {
                            report("You take $amount pain damage.")
                        } else {
                            report("You take $basicPain pain damage.")
                        }
                    } else {
                        if (canReadAdvanced) {
                            report("${target.name} takes $amount pain damage.")
                        } else {
                            report("${target.name} takes $basicPain pain damage.")
                        }
                    }
                }
                DamageType.Calm -> {
                    val basicCalm = target.getBasicCalm(amount)
                    if (isHuman) {
                        if (canReadAdvanced) {
                            report("You calm down by $amount arousal.")
                        } else {
                            report("You calm down $basicCalm.")
                        }
                    } else {
                        if (canReadAdvanced) {
                            report("${target.name} calms down by $amount arousal.")
                        } else {
                            report("${target.name} calms down $basicCalm.")
                        }
                    }
                }
                DamageType.Heal -> {
                    val basicHeal = target.getBasicHeal(amount)
                    if (isHuman) {
                        if (canReadAdvanced) {
                            report("You heal $amount stamina damage.")
                        } else {
                            report("You heal $basicHeal.")
                        }
                    } else {
                        if (canReadAdvanced) {
                            report("${target.name} heals $amount stamina damage.")
                        } else {
                            report("${target.name} heals $basicHeal.")
                        }
                    }
                }
            }
        }

        for (type in DamageType.entries) {
            val amount = damage[type].roundToInt()
            if (amount != 0)
                when(type) {
                    DamageType.Tempt -> reportType(type, amount)
                    DamageType.Pleasure -> reportType(type, amount)
                    DamageType.Weaken -> reportType(type, amount)
                    DamageType.Pain -> reportType(type, amount)
                    DamageType.Calm -> reportType(type, amount)
                    DamageType.Heal -> reportType(type, amount)
                }
        }
    }

    fun reportStatus(target: Character, sts: Status) {
        if (target.human()) {
            if (target.canReadBasic(target)) {
                report("You gain the status $sts.")
            }
        } else if (getOther(target).human()) {
            if (getOther(target).canReadBasic(target)) {
                report("${target.name} gains the status $sts.")
            }
        }
    }

    fun reportStatusFizzle(target: Character, sts: Status) {
        if (target.human()) {
            if (target.canReadBasic(target)) {
                report("You are immune to $sts.")
            }
        } else if (getOther(target).human()) {
            if (getOther(target).canReadBasic(target)) {
                report("${target.name} is immune to $sts.")
            }
        }
    }

    fun reportStatusLoss(target: Character, sts: Status) {
        if (target.human()) {
            if (target.canReadBasic(target)) {
                report("You are no longer affected by the status $sts.")
            }
        } else if (getOther(target).human()) {
            if (getOther(target).canReadBasic(target)) {
                report("${target.name} loses the status $sts.")
            }
        }
    }

    fun addWatcher(voyeur: Character) {
        audience.add(voyeur)
    }

    fun getAudience(): List<Character> {
        return audience
    }

    fun isWatching(voyeur: ID): Boolean {
        return audience.any { it.id == voyeur }
    }

    fun addEvent(target: Character, event: CombatEvent) {
        events[target]?.add(event)
    }

    fun getEvents(target: Character): List<CombatEvent> {
        return events[target]!!
    }

    fun underwearIntact(target: Character): Boolean {
        val underwear = target.getOutfitItem(ClothingType.UNDERWEAR)
        return if (underwear == null) {
            true
        } else {
            this.clothespile.any { it.item == underwear && it.owner == target }
        }
    }

    fun attackRoll(skill: Skill, attacker: Character, target: Character): Boolean {
        val roll = Global.random(20) + 1
        var tohit = roll + attacker.tohit() + skill.accuracy
        if (stance.dom(attacker)) {
            tohit = (tohit * 1.3f).toInt()
        } else if (stance.sub(attacker)) {
            tohit = (tohit * .8).toInt()
        }
        val ac = target.ac()
        if (roll == 20) {
            return true
        } else if (tohit == 0 || tohit < ac) {
            if (tohit + 10 < ac + target.bonusCounter()) {
                target.counterattack(attacker, skill.type(), this)
            }
            return false
        }
        return true
    }

    fun effectRoll(skill: Skill, attacker: Character, target: Character, power: Int): Boolean {
        val roll = Global.random(20) + 1
        var tohit = roll.toDouble() + attacker.tohit() + skill.accuracy
        if (stance.dom(attacker)) {
            tohit *= 1.3
        } else if (stance.sub(attacker)) {
            tohit *= .8
        }
        val ac = target.ac()
        return roll == 20 || tohit > ac
    }

    fun isAllowed(skill: Skill, user: Character): Boolean {
        if (rules.contains(Modifier.pacifist)) {
            if (user.human() && skill.hasTag(SkillTag.PAINFUL)) {
                return false
            }
        }
        if (rules.contains(Modifier.notoys)) {
            if (user.human() && skill.hasTag(SkillTag.TOY)) {
                return false
            }
        }
        if (rules.contains(Modifier.noitems)) {
            if (user.human() && skill.hasTag(SkillTag.CONSUMABLE)) {
                return false
            }
        }
        if (rules.contains(Modifier.norecovery)) {
            if (user.human() && skill.hasTag(SkillTag.MASTURBATION)) {
                return false
            }
        }

        if (Global.checkFlag(Flag.nopain) && skill.hasTag(SkillTag.PAINFUL)) {
            return false
        }
        return true
    }

    fun isAllowed(tag: Tag, user: Character): Boolean {
        if (rules.contains(Modifier.pacifist)) {
            if (user.human() && tag === SkillTag.PAINFUL) {
                return false
            }
        }
        if (rules.contains(Modifier.notoys)) {
            if (user.human() && tag === SkillTag.TOY) {
                return false
            }
        }
        if (rules.contains(Modifier.noitems)) {
            if (user.human() && tag === SkillTag.CONSUMABLE) {
                return false
            }
        }
        if (rules.contains(Modifier.norecovery)) {
            if (user.human() && tag === SkillTag.MASTURBATION) {
                return false
            }
        }

        if (Global.checkFlag(Flag.nopain) && tag === SkillTag.PAINFUL) {
            return false
        }
        return true
    }

    fun hasModifier(mod: Tag?): Boolean {
        return rules.contains(mod)
    }

    fun setParent(parent: Activity?) {
        this.parentActivity = parent
        Global.gui.watchCombat(this)
    }
}

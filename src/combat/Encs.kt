package combat

enum class Encs {
    ambush,
    capitalize,
    showerattack,
    aphrodisiactrick,
    stealclothes,
    fight,
    flee,
    wait,
    smokebomb,
    watch,
}

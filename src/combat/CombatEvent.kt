package combat

import characters.Anatomy
import skills.Skill

class CombatEvent(val event: Result) {
    var skill: Skill? = null
        private set
    var part: Anatomy? = null
        private set

    constructor(event: Result, skill: Skill?) : this(event) {
        this.skill = skill
    }

    constructor(event: Result, part: Anatomy?) : this(event) {
        this.part = part
    }
}

package combat

import utilities.EnumMap
import java.util.EnumMap

class Damage {
    private val allDamage: EnumMap<DamageType, Double > = EnumMap()

    constructor() {
        for (type in DamageType.entries) {
            allDamage[type] = 0.0
        }
    }

    constructor(type: DamageType, amount: Double): this() {
        allDamage[type] = allDamage[type]!! + amount

    }
    operator fun plus(damage: Damage): Damage {
        val temp = Damage()
        for (type in DamageType.entries) {
            temp.allDamage[type] = this.allDamage[type]!! + damage[type]
        }
        return temp
    }

    operator fun plusAssign(damage: Damage) {
        for (type in DamageType.entries) {
            allDamage[type] = allDamage[type]!! + damage[type]
        }
    }
    operator fun minus(damage: Damage): Damage {
        val temp = Damage()
        for (type in DamageType.entries) {
            temp.allDamage[type] = allDamage[type]!! - damage[type]
        }
        return temp
    }
    operator fun minusAssign(damage: Damage) {
        for (type in DamageType.entries) {
            allDamage[type] = allDamage[type]!! - damage[type]
        }
    }
    operator fun times(damage: Damage): Damage {
        val temp = Damage()
        for (type in DamageType.entries) {
            temp.allDamage[type] = allDamage[type]!! * damage[type]
        }
        return temp
    }
    operator fun timesAssign(damage: Damage) {
        for (type in DamageType.entries) {
            allDamage[type] = allDamage[type]!! * damage[type]
        }
    }
    operator fun div(damage: Damage): Damage {
        val temp = Damage()
        for (type in DamageType.entries) {
            temp.allDamage[type] = allDamage[type]!! / damage[type]
        }
        return temp
    }
    operator fun divAssign(damage: Damage) {
        for (type in DamageType.entries) {
            allDamage[type] = allDamage[type]!! / damage[type]
        }
    }
    operator fun get(type: DamageType): Double {
        return allDamage[type]!!
    }
    operator fun set(type: DamageType, value: Double) {
        allDamage[type] = value
    }
}
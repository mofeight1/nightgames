package characters

import global.Constants

enum class Pool(val label: String, val baseCap: Int) {
    STAMINA("Stamina", Constants.STARTINGSTAMINA),
    AROUSAL("Arousal", Constants.STARTINGAROUSAL),
    MOJO("Mojo", Constants.STARTINGMOJO),
    BATTERY("Battery", 20),
    TIME("Time", 12),
    ENIGMA("Enigma", 7),
    FOCUS("Focus", 12)
}

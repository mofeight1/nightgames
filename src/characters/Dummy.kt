package characters

import com.google.gson.JsonObject
import combat.Combat
import combat.Encounter
import combat.Tag
import global.Global
import global.Match
import gui.AssetLoader
import skills.Tactics
import trap.Trap
import java.awt.image.BufferedImage
import java.io.IOException
import java.util.Scanner
import javax.imageio.ImageIO

class Dummy(name: String) : Character(name, 1) {
    var costume: Int = 1
    var wearingTopouter = false
    var wearingTop = false
    var wearingTopinner = false
    var wearingBotouter = false
    var wearingBotinner = false
    var blush: Int
    var strapped: Boolean

    init {
        wearingTopinner = false
        wearingBotouter = false
        wearingBotinner = false
        blush = 0
        strapped = false
        mood = Emotion.confident
    }

    constructor(name: String, costume: Int, dressed: Boolean) : this(name) {
        this.costume = costume
        if (dressed) {
            dress()
        }
    }

    override fun ding() {
        // TODO Auto-generated method stub
    }

    override fun detect() {
        // TODO Auto-generated method stub
    }

    override fun faceOff(opponent: Character, enc: Encounter) {
        // TODO Auto-generated method stub
    }

    override fun spy(opponent: Character, enc: Encounter) {
        // TODO Auto-generated method stub
    }

    override fun describe(per: Int): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun victory(c: Combat, flag: Tag) {
        // TODO Auto-generated method stub
    }

    override fun defeat(c: Combat, flag: Tag) {
        // TODO Auto-generated method stub
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character) {
        // TODO Auto-generated method stub
    }

    override fun victory3p(c: Combat, target: Character, assist: Character) {
        // TODO Auto-generated method stub
    }

    override fun act(c: Combat) {
        // TODO Auto-generated method stub
    }

    override fun move(match: Match) {
        // TODO Auto-generated method stub
    }

    override fun draw(c: Combat, flag: Tag) {
        // TODO Auto-generated method stub
    }

    override fun human(): Boolean {
        // TODO Auto-generated method stub
        return false
    }

    override fun bbLiner(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun nakedLiner(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun stunLiner(): String {
        // TODO Auto-generated method stub
        return ""
    }

    fun winningLiner(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun taunt(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun intervene(enc: Encounter, p1: Character, p2: Character) {
        // TODO Auto-generated method stub
    }

    override fun resist3p(combat: Combat, intruder: Character, assist: Character): Boolean {
        // TODO Auto-generated method stub
        return false
    }

    override fun showerScene(target: Character, encounter: Encounter) {
        // TODO Auto-generated method stub
    }

    override fun save(): JsonObject {
        // TODO Auto-generated method stub
        return JsonObject()
    }

    override fun load(loader: Scanner) {
        // TODO Auto-generated method stub
    }

    override fun afterParty() {
        // TODO Auto-generated method stub
    }

    override fun eot(c: Combat, opponent: Character) {
        // TODO Auto-generated method stub
    }

    override fun eotMoodSwing(c: Combat, opponent: Character) {
        // TODO Auto-generated method stub
    }

    override fun moodSwing(): Emotion {
        // TODO Auto-generated method stub
        return Emotion.confident
    }

    override fun resetOutfit() {
    }

    override fun challenge(opponent: Character): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun promptTrap(enc: Encounter, target: Character, trap: Trap) {
        // TODO Auto-generated method stub
    }

    override fun counterattack(target: Character, type: Tactics, c: Combat) {
        // TODO Auto-generated method stub
    }

    fun setCostumeLevel(set: Int) {
        costume = set
        clearSpriteImages()
    }

    fun dress() {
        wearingTopouter = true
        wearingTop = true
        wearingTopinner = true
        wearingBotouter = true
        wearingBotinner = true
    }

    fun undress() {
        wearingTopouter = false
        wearingTop = false
        wearingTopinner = false
        wearingBotouter = false
        wearingBotinner = false
    }

    fun setTopOuter(wearing: Boolean) {
        wearingTopouter = wearing
    }

    fun setTop(wearing: Boolean) {
        wearingTop = wearing
    }

    fun setTopInner(wearing: Boolean) {
        wearingTopinner = wearing
    }

    fun setBotOuter(wearing: Boolean) {
        wearingBotouter = wearing
    }

    fun setBotInner(wearing: Boolean) {
        wearingBotinner = wearing
    }

    override val portrait: String = "assets/${name}_${mood.name}.jpg"

    override val spriteImage: BufferedImage? get() {
        var res = ""
        if (Global.gui.lowRes()) {
            res = "_low"
        }
        if (spriteBody == null) {
            try {
                spriteBody = ImageIO.read(
                    AssetLoader.getResource(
                        "assets/$name/$name${costume}_Sprite$res.png"
                    )
                )
            } catch (_: IOException) {
            } catch (_: IllegalArgumentException) {
            }
        }
        if (spriteBody == null) {
            try {
                spriteBody = ImageIO.read(
                    AssetLoader.getResource(
                        "assets/$name/${name}_Sprite$res.png"
                    )
                )
            } catch (_: IOException) {
            } catch (_: IllegalArgumentException) {
            }
        }
        if (spriteBody == null) {
            return null
        }
        val sprite = BufferedImage(spriteBody!!.width, spriteBody!!.height, spriteBody!!.type)
        val g = sprite.createGraphics()
        g.drawImage(spriteBody, 0, 0, null)
        try {
            val face = ImageIO.read(
                AssetLoader.getResource(
                    "assets/$name/${name}_${mood.name}$res.png"
                )
            )
            g.drawImage(face, 0, 0, null)
        } catch (_: IOException) {
        } catch (_: IllegalArgumentException) {
        }
        if (blush >= 3) {
            if (blushHigh == null) {
                try {
                    blushHigh = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/${name}_blush3$res.png"
                        )
                    )
                    g.drawImage(blushHigh, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(blushHigh, 0, 0, null)
            }
        } else if (blush == 2) {
            if (blushMed == null) {
                try {
                    blushMed = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/${name}_blush2$res.png"
                        )
                    )
                    g.drawImage(blushLow, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(blushMed, 0, 0, null)
            }
        }
        if (blush == 1) {
            if (blushLow == null) {
                try {
                    blushLow = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/${name}_blush1$res.png"
                        )
                    )
                    g.drawImage(blushLow, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(blushLow, 0, 0, null)
            }
        }
        if (spriteAccessory == null) {
            try {
                spriteAccessory = ImageIO.read(
                    AssetLoader.getResource(
                        "assets/$name/$name${costume}_accessory$res.png"
                    )
                )
                g.drawImage(spriteAccessory, 0, 0, null)
            } catch (_: IOException) {
            } catch (_: IllegalArgumentException) {
            }
        } else {
            g.drawImage(spriteAccessory, 0, 0, null)
        }
        if (wearingTopouter) {
            if (spriteCoattail == null) {
                try {
                    spriteCoattail = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_outerback$res.png"
                        )
                    )
                    g.drawImage(spriteCoattail, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteCoattail, 0, 0, null)
            }
        }
        if (wearingBotinner) {
            if (strapped) {
                if (spriteStrapon == null) {
                    try {
                        spriteStrapon = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/${name}_strapon$res.png"
                            )
                        )
                        g.drawImage(spriteStrapon, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteStrapon, 0, 0, null)
                }
            } else if (spriteUnderwear == null) {
                try {
                    spriteUnderwear = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_underwear$res.png"
                        )
                    )
                    g.drawImage(spriteUnderwear, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteUnderwear, 0, 0, null)
            }
        }
        if (wearingTopinner) {
            if (spriteBra == null) {
                try {
                    spriteBra = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_bra$res.png"
                        )
                    )
                    g.drawImage(spriteBra, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteBra, 0, 0, null)
            }
        }
        if (wearingBotouter) {
            if (spriteBottom == null) {
                try {
                    spriteBottom = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_bottom$res.png"
                        )
                    )
                    g.drawImage(spriteBottom, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteBottom, 0, 0, null)
            }
        }
        if (wearingTop) {
            if (spriteTop == null) {
                try {
                    spriteTop = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_top$res.png"
                        )
                    )
                    g.drawImage(spriteTop, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteTop, 0, 0, null)
            }
        }
        if (wearingTopouter) {
            if (spriteOuter == null) {
                try {
                    spriteOuter = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_outer$res.png"
                        )
                    )
                    g.drawImage(spriteOuter, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteOuter, 0, 0, null)
            }
        }
        g.dispose()
        return sprite
    }

    override fun watcher(c: Combat, victor: Character, defeated: Character) {
        // TODO Auto-generated method stub
    }

    override fun watched(c: Combat, voyeur: Character, defeated: Character) {
        // TODO Auto-generated method stub
    }
}

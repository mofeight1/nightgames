package characters

enum class Trait {
    //Physical
    male("Male", "Male"),  //he,his,him pronouns, currently checked for hasDick and hasBalls
    female("Female", "Female"),  //she,hers, her pronouns, currently checked for hasBreasts and hasPussy
    herm(
        "Hermaphrodite",
        "Have both male and female genitalia"
    ),  //uses female pronouns, but also returns true for hasDick and hasBalls
    tailed("Tailed", "Have a prehensile tail"),

    //Perks
    //Cassie
    silvertongue("Silvertongue", "Terrific tongue talent"),  //Cassie Sex perk, increases pleasure from oral attacks
    judonovice("Judo Novice", "Basic understanding of judo"),  //Cassie Sparring perk, Hip Throw
    misdirection("Misdirection", "They look left, you go right"),  //Cassie Gaming perk, Diversion
    hiddenpotential("Hidden Potential", "Secret potential for rapid growth"),
    protagonist("Protagonist", "The hero of this story"),

    //Mara
    ticklemonster(
        "Tickle Monster",
        "Skilled at tickling in unconventional areas"
    ),  //Mara Sex perk, increases pleasure from tickling if target is nude
    heeldrop(
        "Heeldrop",
        "A wrestling move feared by men and women alike"
    ),  //Mara Sparring perk, increases damage from stomp
    spider("Spider", "Elaborate rope traps come naturally"),  //Mara Gaming perk, Spiderweb
    faefriend("Fae Friend", "Less effort to summon Faeries"),
    smallhands("Small Hands", "More pleasure from hands"),
    freeenergy("Free Energy", "Battery recharges over time"),

    //Angel
    greatkiss("Great Kisser", "Can charm with a single kiss"),  //Angel Sex perk, kiss has chance to inflict Charm
    disciplinarian(
        "Disciplinarian",
        "Frighteningly skilled at spanking"
    ),  //Angel Sparring perk, spank has a chance to inflict Shame
    pokerface("Poker Face", "Bluff like a champion"),  //Angel Gaming perk, Bluff
    scandalous("Scandalous Outfit", "Always seems more naked than she is"),
    lustconduit("Lust Conduit", "Reduce arousal backlash from Dark skills"),

    //Jewel
    dirtyfighter("Dirty Fighter", "Down, but not out"),  //Jewel Sparring perk, kick can be used from prone
    spiral("Spiral", "Who the hell do you think I am?"),  //Jewel Sex perk, Spiral Thrust
    fearless("Fearless", "Leeroy Jenkins"),  //Jewel Gaming perk, Bravado
    roughhandling("Rough Handling", "Does some stamina damage with handjobs or fingering"),
    reflexes("Lightning Reflexes", "Immune to Maneuver, Tackle, and Slight of Hand"),
    juggernaut("Juggernaut", "Recovers from winded faster"),

    //Yui
    assassin("Assassin", "Can ambush opponents even while moving"),

    //Kat
    affectionate("Affectionate", "Increased affection gain from draws"),  //Kat Sex perk
    aikidoNovice("Aikido Novice", "Improved counterattack rate"),  //Kat Sparring perk
    tailmastery("Tail Mastery", "Tail attacks independently"),
    furaffinity("Fur Affinity", "Channels animal spirit at lower arousal"),
    mostlyharmless("Mostly Harmless", "Less likely to be targeted by intervening opponents"),

    //Reyka
    clairvoyance("Clairvoyance", ""),  //Reyka Sparring perk evasion bonus
    locator("Locator", "Like a bloodhound"),  //Reyka Gaming perk out of combat action
    desensitized("Desensitized", "Sex is old hat now"),  //Reyka Sex perk slight pleasure reduction
    infernalexertion("Infernal Exertion", "Use stamina instead of Arousal to fuel Dark skills"),
    royalguard("Royal Guard", "Summon stronger Imps"),
    limitedpotential("Limited Potential", "Reaching the limit growth"),
    speeddemon("Speed Demon", "Wings improve mobility"),

    //Eve
    shameless("Shameless", "Impossible to embarrass"),  //Eve
    RawSexuality("Raw Sexuality", "constant lust boost for you and your opponent in battle"),  //Eve
    hardon("Walking Hard-on", "More genital pleasure dealt and taken"),

    //Samantha
    sexuallyflexible("Sexually Flexible", "Momentum bonuses are interchangeable"),
    careerseductress("Career Seductress", "Bonus to temptation damage"),

    //Valerie
    cropexpert("Crop Expert", "Deals pleasure when using Crops"),
    gracefulloser("Graceful Loser", "Increaased Attraction on loss"),
    slipperyfingers("Slippery Fingers", "Applies oiled when using Finger Ass"),
    secretkeeper("Secret Keeper", "Causer Nervous from Whisper"),

    //Sofia
    striker("Striker", "Good with feet."),
    foulqueen("Foul Queen", "Oppoenents stay stunned longer"),

    //Selene
    phallicappendage("Phallic Appendages", "Can initiate penetration without a strap-on"),

    bronzecock("Bronze Cock", "Started your journey on The Way"),
    silvercock("Silver Cock", "Advanced practitioner of The Way"),
    goldcock("Golden Cock", "Mastered the ultimate sexual technique"),

    //Passive Skills
    exhibitionist("Exhibitionist", "More effective without any clothes"),  //Passively builds mojo while nude
    improvedbattery("Improved Battery", "Increased Battery capacity"),
    pheromones("Pheromones", "Scent can drive people wild"),  //causes horny in opponents if aroused	
    lacedjuices("LacedJuices", "Intoxicating bodily fluids"),  //opponents take temptation when using oral skills
    darkpromises("Dark Promises", "Can enthrall with the right words"),  //whisper upgrade, can enthrall
    composure("Composure", "Gain Composed status at the start of match"),
    determinator("Determinaor", "Resist orgasm once per combat"),

    //Weaknesses
    ticklish("Ticklish", "Can be easily tickled into submission"),  //more weaken damage and arousal from tickle
    insatiable(
        "Insatiable",
        "Always left horny after winning a fight"
    ),  //arousal doesn't completely clear at end of match
    lickable("Lickable", "Weak against oral sex"),  //more arousal from oral attacks
    imagination(
        "Active Imagination",
        "More easily swayed by pillow talk"
    ),  //more temptation damage from indirect skills
    achilles("Achilles Jewels", "Delicate parts are somehow even more delicate"),  //more pain from groin attacks
    hairtrigger(
        "Hair Trigger",
        "Ready to burst at the slightest touch. Better keep those pants on if you want a chance. (Hard)"
    ),  //
    buttslut("Buttslut", "Very sensitive ass"),

    //Restrictions
    softheart("Soft Hearted", "Incapable of being mean"),  //restricts slap, stomp, flick
    petite("Petite", "Small body, small breasts"),  //restricts carry, tackle, paizuri
    undisciplined("Undisciplined", "Lover, not a fighter"),  //restricts manuever, focus, armbar
    direct(
        "Direct",
        "Patience is overrated"
    ),  //restricts whisper, dissolving trap, aphrodisiac trap, decoy, strip tease
    shy("Shy", "Not prone to showing off."),  //restricts striptease, flick, facesit, taunt, squeeze
    cursed(
        "Cursed",
        "Driven by something not quite human"
    ),  //restricts fondlebreasts, knee, leglock, licknipples, darktendrils, twist nipples, shove, slap, tear, tickle, striptease
    sportsmanship(
        "Sportsmanship",
        "Disapproves of (some) dirty tactics"
    ),  //restricts knee, kick, stomp, aphrodisiac trap

    //Class
    madscientist("Mad Scientist", "May have gone overboard with her projects"),
    witch("Witch", "Learned to wield traditional arcane magic"),
    succubus("Succubus", "Embraced the dark powers that feed on mortal lust"),
    fighter("Fighter", "A combination of martial arts and ki"),
    ninja("Ninja", "Trained in an old, exotic style of subterfuge"),

    //Strength
    dexterous("Dexterous", "Underwear is no obstacle for nimble fingers"),  //digital stimulation through underwear
    romantic("Romantic", "Every kiss is as good as the first"),  //bonus to tempt
    experienced("Experienced Lover", "Skilled at pacing yourself when thrusting"),  //reduced recoil from penetration
    wrassler("Wrassler", "A talent for fighting dirty"),  //squeeze, knee, kick reduce arousal less
    pimphand("Pimp Hand", "What did the five fingers say to the face?"),  //
    streaker("Streaker", "Get a rush from being naked in public"),
    brassballs("Brass Balls", "Can take a kick"),
    pantymaster("Panty Peeler", "Skilled at removing bottoms"),
    bramaster("Bra Master", "Skilled at removing tops"),
    toymaster("Toy Master", "20% more effective with all sex toys"),

    //Feats
    sprinter("Sprinter", "Better at escaping combat", isFeat = true),
    QuickRecovery("Quick Recovery", "Regain stamina rapidly out of combat", lvlreq = 8, isFeat = true),
    rapidrecovery("Rapid Recovery", "Recover from winded faster", QuickRecovery, isFeat = true),
    Sneaky("Sneaky", "Easier time hiding and ambushing competitors", isFeat = true),
    Clingy("Clingy", "Better at maintaining pins", isFeat = true),
    PersonalInertia("Personal Inertia", "Status effects (positive and negative) last 50% longer", isFeat = true),
    Confident("Confident", "Mojo decays slower out of combat", lvlreq = 12, isFeat = true),
    SexualGroove("Sexual Groove", "Passive mojo gain every turn in combat", Confident, lvlreq = 20, isFeat = true),
    BoundlessEnergy("Boundless Energy", "Increased passive stamina gain in battle", QuickRecovery, isFeat = true),
    Unflappable("Unflappable", "Not distracted by being fucked from behind", isFeat = true),
    freeSpirit("Free Spirit", "Better at escaping pins", isFeat = true),
    houdini("Houdini", "Bonus to escaping binds based on Cunning", freeSpirit, isFeat = true),
    resourceful("Resourceful", "Chance to not consume an item on use", isFeat = true),
    treasureSeeker("Treasure Seeker", "Improved chance of opening item caches", lvlreq = 8, isFeat = true),
    cautious("Cautious", "Better chance of avoiding traps", isFeat = true),
    sympathetic("Sympathetic", "Intervening opponents are more likely to side with you", lvlreq = 12, isFeat = true),
    fastLearner("Fast Learner", "+10% experience gain", isFeat = true),
    veryfastLearner("Very Fast Learner", "+20% experience gain", fastLearner, isFeat = true),
    leadership("Leadership", "Summoned pets are more powerful, based on Perception", lvlreq = 10, isFeat = true),
    tactician("Tactician", "Summoned pets have better evasion, based on Perception", leadership, isFeat = true),
    fitnessNut("Fitness Nut", "More efficient at exercising", lvlreq = 8, isFeat = true),
    expertGoogler("Expert Googler", "More efficient at finding porn", lvlreq = 8, isFeat = true),
    responsive("Responsive", "Returns more pleasure when being fucked", isFeat = true),
    assmaster("Ass Master", "Who needs lube?", isFeat = true),
    tieGuy("Tie Guy", "Use two Zipties at once for stronger binds.", isFeat = true),
    voyeurism("Voyeurism", "Fill Mojo by watching a fight.", isFeat = true),
    staydown("Stay Down", "Opponents are winded longer.", lvlreq = 10, isFeat = true),
    opportunist("Opportunist", "Deal increased Pleasure damage to winded opponents", lvlreq = 8, isFeat = true),

    coordinatedStrikes("Coordinated Strikes", "Summoned pets are even more powerful, based on Cunning", tactician, lvlreq = 20, isFeat = true),
    evasiveManuevers("Evasive Maneuvers", "Summoned pets have even better evasion, based on Cunning", tactician, lvlreq = 20, isFeat = true),
    handProficiency("Hand Proficiency", "+10% pleasure with hands.", lvlreq = 10, isFeat = true),
    handExpertise("Hand Expertise", "+20% pleasure with hands.", handProficiency, lvlreq = 20, isFeat = true),
    handMastery("Hand Mastery", "+30% pleasure with hands.", handExpertise, lvlreq = 30, isFeat = true),
    oralProficiency("Oral Proficiency", "+10% pleasure with oral.", lvlreq = 10, isFeat = true),
    oralExpertise("Oral Expertise", "+20% pleasure with oral.", oralProficiency, lvlreq = 20, isFeat = true),
    oralMastery("Oral Mastery", "+30% pleasure with oral.", oralExpertise, lvlreq = 30, isFeat = true),
    intercourseProficiency("Intercourse Proficiency", "+10% pleasure with fucking.", lvlreq = 10, isFeat = true),
    intercourseExpertise("Intercourse Expertise", "+20% pleasure with fucking.", intercourseProficiency, lvlreq = 20, isFeat = true),
    intercourseMastery("Intercourse Mastery", "+30% pleasure with fucking.", intercourseExpertise, lvlreq = 30, isFeat = true),
    footloose("Footloose", "Increased footjob and kick power", lvlreq = 12, isFeat = true),

    amateurMagician("Amateur Magician", "Can use Sleight of Hand to quickly strip an opponent using Mojo", isFeat = true),
    bountyHunter("Bounty Hunter", "Double rewards from gold envelope challenges", lvlreq = 10, isFeat = true),
    challengeSeeker("Challenge Seeker", "Gold Envelope appears at start of match", bountyHunter, isFeat = true),
    showoff("Showoff", "Masturbation builds a great deal of Mojo", isFeat = true),
    mojoMaster("Mojo Master", "Max Mojo increases faster", lvlreq = 8, isFeat = true),
    tracker("Tracker", "Better at tracking opponents on the map", lvlreq = 10, isFeat = true),
    advtracker("Advanced Tracker", "Even better at tracking opponents on the map", tracker, lvlreq = 20, isFeat = true),
    mastertracker("Master Tracker", "Master at tracking opponents on the map", advtracker, lvlreq = 30, isFeat = true),

    //Grudge
    //Cassie
    spirited("Spirited", "Builds Mojo each turn"),
    determined("Determined", "Recovers Stamina and Calms Arousal when standing up from prone"),
    modestlydressed("Modestly Dressed", "All clothing is harder to remove"),
    overflowingmana("Overflowing Mana", "Mojo costs of skills dramatically reduced"),

    //Mara
    inspired("Inspired", "Cunning bonus"),
    perfectplan("Perfect Plan", "Destroys opponent's clothing at start of combat"),
    planB("Plan B", "Automatically use an energy drink to recover when out of Stamina"),
    experimentalweaponry("Experimental Weaponry", "Sex Toys much more effective"),

    //Angel
    icequeen("Ice Queen", "Half Temptation damage"),
    seductress("Seductress", "Seduction bonus"),
    untouchable("Untouchable", "Major evasion bonus"),

    //Jewel
    powerup("Powered Up", "Power bonus"),
    healing("Healing Factor", "Stamina regen"),
    confidentdom("Confident Dom", "Seduction bonus and arousal recovery while dominant"),

    //Yui
    flash("Flash", "Speed bonus"),
    ishida3rdart(
        "Ishida 3rd Hidden Art",
        "A traditional kunoichi technique making sexual intercourse extremely responsive"
    ),
    ninjapreparation("Ninja Preparation", "Binds and inflicts horny and drowsy at start of combat"),

    //Kat
    feral("Feral", "Receives Animism bonus regardless of current arousal"),
    predatorinstincts("Predator's Instincts", "Pins are much harder to escape"),
    landsonfeet("Lands on Her Feet", "High resistance to trips or knockdowns"),

    //Reyka
    darkness("Overwhelming Darkness", "Dark bonus"),
    enthralling("Enthralling Presence", "Enthralls opponent at start of combat"),
    succubusvagina("Succubus Vagina", "Increases sexual proficiency"),

    //Samantha
    tantra("Tantric Breathing", "Reduces Arousal each turn"),
    sparehandcuffs("Spare Handcuffs", "Gains two sets of handcuffs at start of combat"),
    veteranprostitute("Veteran Prostitute", "Reduced pleasure damage to genitals"),

    //Eve
    sadisticmood("Sadistic Mood", "Give opponent Masochism at start of combat"),
    defensivemeasures("Defensive Measures", "Starts combat wearing a cup"),
    revvedup("Revved Up", "Bonus to Power, Seduction, and Fetish when aroused"),

    //Valerie
    highcomposure("High Composure", "Harder to break than usual"),
    potentcomposure("Potent Composure", "Composure reduces pleasure more"),
    enraged("Enraged", "Starts Broken, but with Speed and Power buff"),

    //Sofia
    cheapshot("Cheap Shot", "Delivers a stunning blow at the start of battle"),

    //Item
    strapped("Strapped", "Penis envy"),  //currently wearing a strapon
    ineffective("Ineffective", "Provides no protection"),
    armored("Armored", "Protects the delicate bits"),
    stylish("Stylish", "Better mojo gain"),
    lame("Lame", "Small mojo penalty"),
    skimpy("Skimpy", "Better temptation damage"),
    caste("Chaste", "Minor temptation resistance"),
    indestructible("Indestructible", "Cannot be destroyed"),
    bulky("Bulky", "Speed penalty"),
    geeky("Geeky", "Science bonus"),
    mystic("Mystic", "Arcane bonus"),
    martial("Martial", "Ki bonus"),
    broody("Broody", "Dark bonus"),
    kinky("Kinky", "Fetish bonus"),
    stealthy("Stealthy", "Ninjutsu bonus"),
    furry("Furry", "Animism bonus"),
    mighty("Mighty", "Power bonus"),
    antihorny("Anti-Horny", "Immune from Horny status"),
    accessible("Accessible", "Can initiate penetration while wearing"),
    legend("Legend", "Pleasure bonus and recovers Arousal every turn, but Mojo gain is negated"),

    //Situation
    painimmune("Pain Immune", "Takes no pain damage"),
    weakimmune("Weaken Immune", "Takes no weaken damage"),
    pleasureimmune("Pleasure Immune", "Takes no pleasure damage"),
    temptimmune("Temptation Immune", "Takes no temptation damage"),
    summonblock("Summoning Blocked", "Unable to summon pets"),

    //Match Modifiers
    lameglasses("Lame Glasses", "Negates Mojo generation"),
    mittens("Mittens", "-50% finger proficiency"),
    nosatisfaction("No Satisfaction", "Unable to orgasm by victory or masturbation"),

    event("Event Character", "Provides bonus victory points, XP bonus is capped"),
    none("", ""),
    ;

    private val prereq: Trait?
    private val lvlreq: Int
    val iName: String
    val desc: String
    val isFeat: Boolean
    override fun toString(): String {
        return iName
    }

    constructor(name: String, description: String, prereq: Trait? = null, lvlreq: Int = 0, isFeat: Boolean = false) {
        this.iName = name
        this.desc = description
        this.prereq = prereq
        this.lvlreq = lvlreq
        this.isFeat = isFeat
    }

    fun req(user: Character): Boolean {
        return if (user.level >= lvlreq) {
            if (prereq == null) {
                true
            } else if (user.has(prereq)) {
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    /*val isFeat: Boolean
        get() = this in sprinter..mojoMaster*/
}

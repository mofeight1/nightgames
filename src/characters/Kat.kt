package characters

import comments.CommentGroup
import comments.CommentSituation
import comments.SkillComment
import actions.Action
import actions.Movement
import combat.Combat
import combat.Result
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import items.Clothing
import items.Toy
import items.Trophy
import scenes.SceneFlag
import scenes.SceneManager
import skills.Carry
import skills.PheromoneOverdrive
import skills.Purr
import skills.Skill
import skills.SkillTag
import skills.Tackle
import skills.TailJob
import stance.Stance
import status.Stsflag


class Kat : Personality {
    override var character: NPC = NPC("Kat", ID.KAT, 10, this)

    init {
        character.outfit[0].add(Clothing.bra)
        character.outfit[0].add(Clothing.Tshirt)
        character.outfit[1].add(Clothing.panties)
        character.outfit[1].add(Clothing.skirt)
        character.closet.add(Clothing.bra)
        character.closet.add(Clothing.Tshirt)
        character.closet.add(Clothing.panties)
        character.closet.add(Clothing.skirt)
        character.change(Modifier.normal)
        character.underwear = Trophy.KatTrophy
        character.add(Trait.female)
        character.mod(Attribute.Power, 5)
        character.mod(Attribute.Animism, 14)
        character.mod(Attribute.Cunning, 3)
        character.mod(Attribute.Speed, 3)
        character.mod(Attribute.Seduction, 2)
        character.stamina.gainMax(45)
        character.arousal.gainMax(55)
        character.mojo.gainMax(25)
        character.add(Trait.dexterous)
        character.add(Trait.pheromones)
        character.add(Trait.tailed)
        character.add(Trait.shy)
        character.add(Trait.sympathetic)
        character.plan = Emotion.sneaking
        character.mood = Emotion.confident
        character.strategy[Emotion.hunting] = 1
        character.preferredSkills.add(Tackle(character))
        character.preferredSkills.add(Purr(character))
        character.preferredSkills.add(TailJob(character))
        character.preferredSkills.add(PheromoneOverdrive(character))
        character.preferredSkills.add(Carry(character))
    }

    override fun act(available: HashSet<Skill>, c: Combat): Skill {
        val mandatory = HashSet<Skill>()
        for (a in available) {
            if (a.toString() === "Command" || a.toString().equals("Ass Fuck", ignoreCase = true)) {
                mandatory.add(a)
            }
            if (character.has(Stsflag.orderedstrip)) {
                if (a.toString() === "Undress" || a.toString() === "Strip Tease") {
                    mandatory.add(a)
                }
            }
        }
        if (mandatory.isNotEmpty()) {
            return mandatory.random()
        }

        return chooseSkill(available, c)
    }

    /*override fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        val proposed = character.parseMoves(available, radar, match)
        return proposed
    }*/

    override fun rest(time: Int, day: Daytime) {
        if (!(character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && character.money >= 300) {
            character.gain(Toy.Onahole)
            character.money -= 300
        }
        if (!(character.has(Toy.Tickler) || character.has(Toy.Tickler2)) && character.money >= 300) {
            character.gain(Toy.Tickler)
            character.money -= 300
        }
        var loc: String
        val available = ArrayList<String>()
        available.add("Hardware Store")
        available.add("Black Market")
        available.add("XXX Store")
        available.add("Bookstore")
        available.add("Reference Room")
        available.add("Play Video Games")
        for (i in 0 until time - 3) {
            loc = available.random()
            day.visit(loc, character, Global.random(character.money))
            if (loc !== "Exercise" && loc !== "Browse Porn Sites") {
                available.remove(loc)
            }
        }
        if (character.getAffection(Global.player) > 0) {
            Global.modCounter(Flag.KatDWV, 1.0)
        }
        character.visit(3)
    }

    override fun bbLiner(): String {
        return when (Global.random(2)) {
            1 -> ("Kat's already large eyes widen even further as she looks at the pained expression on your face.  "
                    + "<i>\"Nya!  A swift shot to the nyutsack.  Works every time!\"</i>")

            else -> "Kat gives you a look of concern and sympathy. <i>\"Nya... Are you ok? I didn't mean to hit you that hard.\"</i>"
        }
    }

    override fun nakedLiner(): String {
        return if (character.arousal.percent() >= 50) {
            "Kat makes no effort to hide the moisture streaming down her thighs. <i>\"You want my pussy? I'm nyot going to myake it easy for you.\"</i>"
        } else {
            "Kat blushes deep red and bashfully tries to cover her girl parts with her tail. <i>\"Don't stare too much, ok?\"</i>"
        }
    }

    override fun stunLiner(): String {
        return "Kat mews pitifully on the floor. <i>\"Don't be so meaNya.\"</i>"
    }

    fun winningLiner(): String? {
        // TODO Auto-generated method stub
        return null
    }

    override fun taunt(): String {
        return "Kat smiles excitedly and bats at your cock. <i>\"Are you already close to cumming? Nya! I want to play with you more!\"</i>"
    }

    override fun victory(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        character.arousal.empty()
        if (opponent.human()) {
            if (flag === Result.anal) {
                Global.modCounter(Flag.PlayerAssLosses, 1.0)
                SceneManager.play(SceneFlag.KatPeggingVictory)
            } else if (flag === Result.intercourse) {
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.KatSexVictoryAlt)
                } else {
                    SceneManager.play(SceneFlag.KatSexVictory)
                }
            } else if (c.lastact(character)!!.hasTag(Result.feet)) {
                SceneManager.play(SceneFlag.KatFootjobVictory)
            } else if (c.lastact(character)!!.hasTag(SkillTag.TAIL)) {
                SceneManager.play(SceneFlag.KatTailjobVictory)
            } else if (!character.has(Stsflag.feral)) {
                SceneManager.play(SceneFlag.KatForeplayVictoryEasy)
            } else {
                opponent.arousal.set(opponent.arousal.max / 3)
                SceneManager.play(SceneFlag.KatForeplayVictory)
            }
        }
    }

    override fun defeat(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        declareGrudge(opponent, c)
        if (opponent.human()) {
            if (flag === Result.anal && c.stance.sub(character)) {
                SceneManager.play(SceneFlag.KatAnalDefeat)
            } else if (c.stance.en == Stance.standing) {
                SceneManager.play(SceneFlag.KatSexDefeatCarry)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.KatSexDefeat)
            } else if (character.has(Stsflag.bound)) {
                SceneManager.play(SceneFlag.KatBoundDefeat)
            } else if (opponent.has(Stsflag.feral)) {
                SceneManager.play(SceneFlag.KatFeralDefeat)
            } else {
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.KatForeplayDefeatAlt)
                } else {
                    SceneManager.play(SceneFlag.KatForeplayDefeat)
                }
                return
            }
        }
    }

    override fun describe(): String {
        return "It's easy to forget that Kat's older than you when she looks like she's about to start high school. She's a very short and slim, though you know she's " +
                "stronger than she looks. Adorable cat ears poke through her short, strawberry blonde hair and a soft tail dangles from the top of her cute butt. She " +
                "looks a bit timid, but there's a gleam of desire in her eyes."
    }

    override fun draw(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.KatSexDraw)
            } else {
                SceneManager.play(SceneFlag.KatForeplayDraw)
            }
        }
    }

    override fun fightFlight(opponent: Character): Boolean {
        return !character.isNude || opponent.isNude
    }

    override fun attack(opponent: Character): Boolean {
        return true
    }

    override fun ding() {
        var rand: Int
        for (i in 0 until (Global.random(3) / 2) + 2) {
            rand = Global.random(4)
            when (rand) {
                0 -> {
                    character.mod(Attribute.Power, 1)
                }
                1 -> {
                    character.mod(Attribute.Seduction, 1)
                }
                2 -> {
                    character.mod(Attribute.Cunning, 1)
                }
                else -> {
                    character.mod(Attribute.Animism, 1)
                }
            }
        }
        character.stamina.gainMax(4)
        character.arousal.gainMax(4)
    }

    override fun victory3p(c: Combat, target: Character, assist: Character): String {
        character.clearGrudge(target)
        character.clearGrudge(assist)
        return if (target.human()) {
            "Kat crouches between your legs, watching your erect penis twitch and gently bob. She playfully bats it back and forth, clearly enjoying herself. That is " +
                    "most definitely not a toy, but she seems to disagree. As your boner wags from side to side, she catches it with her mouth, fortunately displaying the " +
                    "presence of mind to keep her teeth clear. She covers the head of your dick with her mouth and starts to lick it intently, while she teases your shaft " +
                    "with both hands. Oh God. You're not sure whether or not this is still a game to her, but she's being very effective. With the combined pleasure from her " +
                    "mouth and hands, it's not long before your hips start bucking in anticipation of release. She jerks you off with both hands and sucks firmly on your glans. " +
                    "You groan as you ejaculate into her mouth and she eagerly swallows every drop."
        } else if (target.hasDick) {
            ("Kat gives " + target.name + " a distinctively feline smirk as she licks her lips, her hands wrapping around her opponent's throbbing "
                    + "member and pumping up and down as she leans in, swirling her tongue around the tip. " + target.name + " shudders, struggling in "
                    + "your grip, but you hold her hands firmly. Kat's tail curls behind her as she sucks the girl's cock. You can see the veiny shaft "
                    + "glistening with saliva. By the way " + target.name + "'s toes are clenching, you know she won't last much longer. She's already put "
                    + "up quite a fight. <i>\"S-Stop!\"</i> she cries, but her plea makes Kat's ears perk up. She pulls back, peering up at " + target.name
                    + " with light eyes, purring softly as her tongue massages the underside of the glans.<p>"
                    + "<i>\"You want me to stop, nyaa?\"</i> Kat murmurs, lapping at the shiny precum that's appearing as she continues to furiously "
                    + "masturbate her foe. " + target.name + " is at a loss for words, too overcome by the sensations to think coherently anymore. "
                    + "Kat plunges " + target.name + "'s cock back into her mouth, taking it in as far as she can as she feels the girl begin to orgasm. "
                    + "You hold her steady as she bucks wildly, moaning loudly. Kat slurps down her load, cum dripping from the corners of her mouth. "
                    + "When she's finished, she sits up, wiping her mouth with a satisfied expression. It looks like victory is yours.")
        } else {
            "Kat stalks over to " + target.name + " like a cat. Very much like a cat. She leans toward the other girl's breasts and starts to softly lick her nipples. " +
                    target.name + " stifles a moan of pleasure as Kat gradually covers her breasts in saliva, not missing an inch. Kat gradually works her way down, giving " +
                    "equal attention to " + target.name + "'s bellybutton. By the helpless girl's shudders and stifled giggles, it clearly tickles, but also seems to heighten " +
                    "her arousal. You feel her body tense up in anticipation when Kat licks her way down towards her waiting pussy. Instead Kat veers off and starts kissing " +
                    "and licking her inner thighs. " + target.name + " trembles in frustration and she must realize she has no chance to win, because she yells out: <i>\"Please! " +
                    "Stop teasing me and let me cum!\"</i> Kat blinks for a moment and looks surprised, like she had forgotten her original purpose, but she soon smiles and dives " +
                    "enthusiastically between " + target.name + "'s legs. It doesn't take long before you feel her shudder in orgasm from Kat's intense licking, but even then, " +
                    "Kat shows no sign of stopping. " + target.name + " stammers out a protest and you sympathetically release her hands, but she's too overwhelmed by pleasure " +
                    "to defend herself as Kat licks her to a second orgasm."
        }
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character): String {
        return if (target.human()) {
            "Your fight with " + assist.name + " goes back and forth for several minutes, leaving you both naked and aroused, but without a clear winner. You back off " +
                    "a couple steps to catch your breath, when suddenly you're blindsided and knocked to the floor. You look up to see Kat straddling your chest in full cat-mode. " +
                    "She's clearly happy to see you, kissing and nuzzling your neck while mewing happily. She's very cute, but unfortunately you're unable to dislodge her from " +
                    "her perch on your upper body. More unfortunately, your completely defenseless lower body is exposed to " + assist.name + ", who doesn't let the opportunity go " +
                    "to waste.<p>"
        } else {
            "Your fight with " + target.name + " goes back and forth for several minutes, leaving you both naked and aroused, but without a clear winner. You back off " +
                    "a couple steps to catch your breath, when you notice a blur of motion out of the corner of your eye. Distracted as you are, you aren't paying attention to " +
                    "your footing and stumble backwards. " + target.name + " advances to take advantage of your mistake, but she soon realizes you aren't looking at her, but " +
                    "behind her. She turns around just in time to get pounced on by an exuberant catgirl. Kat starts to playfully tickle her prey, incapacitating the poor, " +
                    "naked girl with uncontrollable fits of laughter. You stand up, brush off your butt, and leisurely walk towards your helpless opponent to finish her off.<p>"
        }
    }

    override fun watched(c: Combat, target: Character, viewer: Character) {
        if (viewer.human()) {
            if (target.hasDick) {
                SceneManager.play(SceneFlag.KatWatchPenis)
            } else {
                SceneManager.play(SceneFlag.KatWatch)
            }
        }
    }

    override fun startBattle(opponent: Character): String {
        if (character.grudge != null) {
            when (character.grudge) {
                Trait.feral -> return ("Kat approaches you with a confident swagger and her tail swaying slowly. She's clearly in cat mode already. "
                        + "It looks like someone already warmed her up for you.<p>"
                        + "<i>\"Nyot quite. I'm just myaking an effort to embrace my feline side. I'm gonnya go all out from the start.\"</i>")

                Trait.predatorinstincts -> return ("Kat stalks you with a dangerous expression. Despite her cute features and small build, you start to feel like a prey animal faced "
                        + "by a predator.<p>"
                        + "<i>\"I'll catch you this time, and I won't let you go until you cum!\"</i>")

                Trait.landsonfeet -> return ("Kat leaps toward you,  planting a light kiss on your lips. Before you can respond, she backflips "
                        + "out of reach, landing gracefully on one foot. She's even more nimble than you realized.<p>"
                        + "<i>\"Cats always land on their feet. I'll be the one on top this time.\"</i>")

                else -> {}
            }
        }
        if (character.isNude) {
            return if (character.arousal.percent() >= 50) {
                ("Kat approaches you, naked and unashamed. She parts her slick nether lips to show you her arousal.<p>"
                        + "<i>\"You showed up at just the right time, nya. Nyow you're gonnya satisfy me.\"</i>")
            } else {
                ("Kat blushes deep red as she tries to preserve her modesty. <i>\"I was hoping to get dressed before you caught me. "
                        + "Well, let's hurry up and start before this gets any more embarrassing.\"</i>")
            }
        }
        if (opponent.isPantsless) {
            return ("Kat stares at your dangling penis until you cough to get her attention. <i>\"Nya!? Oh, sorry.\"</i> She blushes a bit and "
                    + "regains her focus. <i>\"Cats are easily distracted by swinging things. Girls are also easily distracted by... certain things...\"</i>")
        }
        if (character.arousal.percent() >= 50) {
        }
        if (character.getAffection(opponent) >= 30) {
            return ("Kat smiles and lets out a quiet purr when she sees you. She fidgets shyly with her tail, looking more like a girl waiting for a date "
                    + "than someone preparing to fight.<p>"
                    + "<i>\"Sorry, I was just looking forward to this. I almost don't care who wins.\"</i>")
        }
        return "Kat looks a bit nervous, but her tail wags slowly in anticipation. <i>\"Let's have some fun-nya.\"</i>"
    }

    override fun fit(): Boolean {
        return (!character.isNude && character.stamina.percent() >= 50) || character.arousal.percent() > 50
    }

    override fun night(): Boolean {
        Global.gui.loadPortrait(Global.player, this.character)
        SceneManager.play(SceneFlag.KatAfterMatch)
        return true
    }

    override fun advance(rank: Int) {
        if (rank >= 3 && !character.has(Trait.tailmastery)) {
            character.add(Trait.tailmastery)
        }
        if (rank >= 2 && !character.has(Trait.furaffinity)) {
            character.add(Trait.furaffinity)
        }
    }

    override fun checkMood(mood: Emotion, value: Int): Boolean {
        if (character.has(Stsflag.feral)) {
            if (!character.has(Trait.shameless)) {
                character.add(Trait.shameless)
                character.remove(Trait.shy)
                character.strategy[Emotion.hunting] = 5
            }
            return when (mood) {
                Emotion.horny -> value >= 30
                Emotion.nervous -> value >= 80
                else -> value >= 50
            }
        } else {
            if (!character.has(Trait.shy)) {
                character.add(Trait.shy)
                character.remove(Trait.shameless)
                character.strategy[Emotion.hunting] = 1
            }
            return when (mood) {
                Emotion.nervous, Emotion.horny -> value >= 30
                Emotion.angry -> value >= 80
                else -> value >= 50
            }
        }
    }

    override fun moodWeight(mood: Emotion): Double {
        return if (character.has(Stsflag.feral)) {
            when (mood) {
                Emotion.horny -> 1.2
                Emotion.nervous -> .7
                else -> 1.0
            }
        } else {
            when (mood) {
                Emotion.nervous, Emotion.horny -> 1.2
                Emotion.angry -> .7
                else -> 1.2
            }
        }
    }

    override fun image(): String {
        return "assets/kat_" + character.mood.name + ".jpg"
    }

    /*override fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }*/

    override fun resist3p(c: Combat, target: Character, assist: Character): String {
        // TODO Auto-generated method stub
        return ""
    }

    override val comments: CommentGroup
        get() {
        val comments = CommentGroup()
            comments[CommentSituation.VAG_DOM_CATCH_WIN] = "<i>\"Nya! Cum in me now!\"</i>"
            comments[CommentSituation.VAG_DOM_CATCH_LOSE] = "<i>\"Ooohnya! But I'm on top! I should win!\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_WIN] = "<i>\"Yes! Fill me! Breed me~!\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_LOSE] = "<i>\"Nnnya! Oh! Fuck, breed, cum, NYAAA!\"</i>"
            comments[CommentSituation.ANAL_CATCH_WIN] = "<i>\"Ooh! Are you gonnya fill up my ass?\"</i>"
            comments[CommentSituation.ANAL_CATCH_LOSE] = "<i>\"Nya! And they ca-AAH-ll me an animalnya!\"</i>"
            comments[CommentSituation.ANAL_PITCH_WIN] = "<i>\"Yes! Come for me! There's nyot any shame in being the beta of a pack, nya~!\"</i>"
            comments[CommentSituation.ANAL_PITCH_LOSE] = "<i>\"Nnnya?! Th-this is what it's like to be the one on top?\"</i> Kat has two fingers jammed between her strap-on harness and her clitoris, and is clearly enjoying herself."
            comments[CommentSituation.MOUNT_DOM_WIN] = "<i>\"Nyanyanya! I'm going to win now!\"</i>"
            comments[CommentSituation.SELF_BOUND] = "<i>\"Please let me go! I'll do anyathing!\"</i>"
            comments[CommentSituation.MOUNT_SUB_LOSE] = "<i>\"Ah! Put it in! Put it in!\"</i>"
            comments[CommentSituation.PIN_SUB_LOSE] = "<i>\"Ah! Put it in! Put it in!\"</i>"
            comments[CommentSituation.OTHER_HORNY] = "<i>\"Are you in heat nyow too? I can help with that!\"</i>"
            comments[CommentSituation.SELF_HORNY] = "<i>\"Nyaa~... Please! I need it!\"</i>"
            comments[CommentSituation.SELF_SHAMED] = "<i>\"Don't look! Don't look! Don't look!\"</i>"
            comments[CommentSituation.OTHER_CHARMED] = "<i>\"Nya? Can I play with you a little? purretty please?\"</i>"
            comments[CommentSituation.SELF_BUSTED] = "Kat goes cross-eyed and squeezes her knees together protectively.  She lets out a long, low growl from the back of her throat that slowly rises in pitch, until it becomes a pitiful whimper."
            comments[SkillComment(Attribute.Animism, true)] = "<i>\"Nyice! Let's go at it like anyamals!\"</i>"

        return comments
    }

    override val responses: CommentGroup
        get() {
        val comments = CommentGroup()
        return comments
    }

    override val costumeSet = 1

    override fun declareGrudge(opponent: Character, c: Combat) {
        when (Global.random(3)) {
            2 -> character.addGrudge(opponent, Trait.feral)
            1 -> character.addGrudge(opponent, Trait.predatorinstincts)
            0 -> character.addGrudge(opponent, Trait.landsonfeet)
            else -> {}
        }
    }

    override fun resetOutfit() {
        character.outfit[0].clear()
        character.outfit[1].clear()
        character.outfit[0].add(Clothing.bra)
        character.outfit[0].add(Clothing.Tshirt)
        character.outfit[1].add(Clothing.panties)
        character.outfit[1].add(Clothing.skirt)
    }
}

package characters

enum class AttributeType {
    Basic,
    Passive,
    Advanced,
    Specialization
}

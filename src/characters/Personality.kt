package characters

import comments.CommentGroup
import actions.Action
import actions.Movement
import combat.Combat
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import skills.Skill

interface Personality {
    fun chooseSkill(available: HashSet<Skill>, c: Combat): Skill {
        val priority = character.parseSkills(available, c)
        val chosen = if (Global.checkFlag(Flag.hardmode) && !c.hasModifier(Modifier.quiet)) {
            character.prioritizeNew(priority, c)
        } else {
            character.prioritize(priority)
        }
        return chosen ?: available.random()
    }
    fun act(available: HashSet<Skill>, c: Combat): Skill
    fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        return character.parseMoves(available, radar, match)
    }
    val character: NPC
    fun rest(time: Int, day: Daytime)
    fun bbLiner(): String
    fun nakedLiner(): String
    fun stunLiner(): String
    fun taunt(): String
    fun victory(c: Combat, flag: Tag)
    fun defeat(c: Combat, flag: Tag)
    fun victory3p(c: Combat, target: Character, assist: Character): String
    fun intervene3p(c: Combat, target: Character, assist: Character): String
    fun resist3p(c: Combat, target: Character, assist: Character): String
    fun watched(c: Combat, target: Character, viewer: Character)
    fun describe(): String
    fun draw(c: Combat, flag: Tag)
    fun fightFlight(opponent: Character): Boolean
    fun attack(opponent: Character): Boolean
    fun ding()
    fun startBattle(opponent: Character): String
    fun fit(): Boolean
    fun night(): Boolean
    fun advance(rank: Int)
    fun checkMood(mood: Emotion, value: Int): Boolean
    fun moodWeight(mood: Emotion): Double
    fun image(): String
    fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }
    val comments: CommentGroup
    val responses: CommentGroup
    val costumeSet: Int
    fun declareGrudge(opponent: Character, c: Combat)
    fun resetOutfit()
}

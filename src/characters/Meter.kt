package characters

import kotlin.math.roundToInt

class Meter(max: Int) : Cloneable {
    var max: Int = max
        private set

    var current = 0
        private set

    fun reduce(i: Int): Int {
        val past = current
        if (i > 0) {
            current -= i
        }
        if (current < 0) {
            current = 0
        }
        if (current > max) {
            current = max
        }
        return past - current
    }

    fun minimize(i: Int): Int {
        if (current == 0) return 0
        val past = current
        if (i > 0) {
            current -= i
        }
        if (current <= 0) {
            current = 1
        }
        if (current > max) {
            current = max
        }
        return past - current
    }

    fun restore(i: Int): Int {
        val past = current
        current += i
        if (current > max) {
            current = max
        }
        if (current < 0) {
            current = 0
        }
        return current - past
    }

    fun edge(i: Int): Int {
        if (current == max) return 0
        val past = current
        current += i
        if (current >= max) {
            current = max - 1
        }
        if (current < 0) {
            current = 0
        }
        return current - past
    }

    fun addPercent(perc: Double): Int {
        val x = (perc * max) / 100
        restore(x.roundToInt())
        return x.roundToInt()
    }

    val isEmpty: Boolean
        get() = current <= 0
    val isNotEmpty: Boolean
        get() = !isEmpty
    val isFull: Boolean
        get() = current == max
    val isNotFull: Boolean
        get() = !isFull

    fun empty() {
        current = 0
    }

    fun fill() {
        current = max
    }

    fun set(i: Int) {
        current = i
        if (current > max) {
            current = max
        }
        if (current < 0)
            current = 0
    }

    fun gainMax(i: Int) {
        max += i
        if (current > max) {
            current = max
        }
    }

    fun setMax(i: Int) {
        max = i
        current = max
    }

    fun percent(): Double {
        return 100.0 * current / max
    }

    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Meter {
        return super.clone() as Meter
    }
}

package characters

import actions.Action
import actions.Move
import actions.Movement
import areas.Area
import areas.NinjaStash
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import combat.Combat
import combat.CombatEvent
import combat.Damage
import combat.DamageType
import combat.Encounter
import combat.Result
import combat.Tag
import global.Challenge
import global.Constants
import global.Flag
import global.Global
import global.Match
import global.Modifier
import global.Roster
import global.Scheduler
import global.SkillList
import gui.AssetLoader
import gui.GUI
import items.Clothing
import items.ClothingType
import items.Component
import items.Consumable
import items.Envelope
import items.Flask
import items.Item
import items.OwnedItem
import items.Potion
import pet.Pet
import skills.Distracted
import skills.Nothing
import skills.PullOut
import skills.Recover
import skills.ReverseStraddle
import skills.Skill
import skills.Straddle
import skills.Struggle
import skills.Stunned
import skills.Tactics
import skills.UseFlask
import skills.UsePotion
import stance.Position
import status.Beastform
import status.Bound
import status.Composed
import status.Disheveled
import status.Drowsy
import status.Enthralled
import status.Feral
import status.Horny
import status.Masochistic
import status.Oiled
import status.Status
import status.Stsflag
import trap.Trap
import utilities.EnumMap
import utilities.addNotNull
import java.awt.image.BufferedImage
import java.io.IOException
import java.util.Observable
import java.util.Scanner
import javax.imageio.ImageIO
import kotlin.collections.HashSet
import kotlin.math.exp
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.roundToInt

abstract class Character(): Observable(), Cloneable {
    var gui: GUI? = null
    lateinit var name: String
        protected set

    var level: Int = 0
        protected set

    constructor(name: String, level: Int) : this() {
        this.name = name
        this.level = level
    }
    lateinit var id: ID
        protected set

    var xp: Int = 0
        protected set
    var rank: Int = 0
    var money: Int = 0
    var att = EnumMap<Attribute, Int>()
    var stamina: Meter
        get() = get(Pool.STAMINA)
        protected set(value) {
            pools[Pool.STAMINA] = value
        }
    var arousal: Meter
        get() = get(Pool.AROUSAL)
        protected set(value) {
            pools[Pool.AROUSAL] = value
        }
    var mojo: Meter
        get() = get(Pool.MOJO)
        protected set(value) {
            pools[Pool.MOJO] = value
        }
    protected var pools = EnumMap<Pool, Meter>()
    var tops = ArrayDeque<OwnedItem<Clothing>>()
    var bottoms = ArrayDeque<OwnedItem<Clothing>>()
    var location: Area
        protected set
    var outfit: Array<ArrayDeque<Clothing>> = arrayOf(ArrayDeque(), ArrayDeque())
    protected var skills = HashSet<Skill>()
    protected var flaskskills = HashSet<Skill>()
    protected var potionskills = HashSet<Skill>()
    var status = HashSet<Status>()
        protected set
    var traits = HashSet<Trait>()

    //var removelist = HashSet<Status>() ;
    var addlist = HashSet<Status>()
    private var mercy = HashSet<Character>()
    var inventory = ArrayList<Item>()
        protected set
    var underwear: Item? = null
    var state: State = State.ready
    protected var busy: Int = 0
    var emotes = EnumMap<Emotion, Int>()
    var mood: Emotion = Emotion.confident
    var closet = HashSet<Clothing>()
    var pet: Pet? = null
    var challenges = ArrayList<Challenge>()
    protected var grudges = HashMap<Character, Trait>()
    var grudge: Trait? = null
        protected set
    protected var matchmod: Trait? = null
    var isBenched: Boolean = false

    var spriteBody: BufferedImage? = null
    var spriteAccessory: BufferedImage? = null
    var spriteAccessory2: BufferedImage? = null
    var spriteUnderwear: BufferedImage? = null
    var spriteStrapon: BufferedImage? = null
    var spriteBra: BufferedImage? = null
    var spriteBottom: BufferedImage? = null
    var spriteTop: BufferedImage? = null
    var spriteOuter: BufferedImage? = null
    var spriteCoattail: BufferedImage? = null
    var spriteShirttail: BufferedImage? = null
    var spritePenisSoft: BufferedImage? = null
    var spritePenisHard: BufferedImage? = null
    var spriteKemono: BufferedImage? = null
    var blushLow: BufferedImage? = null
    var blushMed: BufferedImage? = null
    var blushHigh: BufferedImage? = null


    init {
        location = Area("", "", Movement.none, null)
        for (e in Emotion.entries) {
            emotes[e] = 0
        }
        skills.add(Struggle(this))
        skills.add(Nothing(this))
        skills.add(Recover(this))
        skills.add(Straddle(this))
        skills.add(ReverseStraddle(this))
        skills.add(Stunned(this))
        skills.add(Distracted(this))
        skills.add(PullOut(this))
        flaskskills.addAll(Flask.entries.map { UseFlask(this, it) })
        potionskills.addAll(Potion.entries.map { UsePotion(this, it) })
    }

    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Character {
        val c = super.clone() as Character
        c.att = att.clone()
        c.pools = pools.clone()
        c.tops = ArrayDeque(tops)
        c.bottoms = ArrayDeque(bottoms)
        c.outfit = outfit.clone()
        c.skills = skills.clone() as HashSet<Skill>
        c.status = HashSet()
        for (s in status) {
            c.status.add(s.copy(c))
        }
        c.traits = traits.clone() as HashSet<Trait>
        //c.removelist = removelist.clone() as HashSet<Status>
        c.addlist = addlist.clone() as HashSet<Status>
        c.mercy = mercy.clone() as HashSet<Character>
        c.inventory = inventory.clone() as ArrayList<Item>
        c.skills = skills.clone() as HashSet<Skill>
        c.emotes = emotes.clone()
        c.grudge = grudge
        c.mood = mood
        return c
    }


    operator fun get(a: Attribute): Int {
        var total = att[a]?.toDouble() ?: 0.0
        total += status.sumOf { it.mod(a) }
        when (a) {
            Attribute.Power -> {
                if (has(Trait.powerup)) {
                    total += 10
                }
                if (has(Trait.mighty)) {
                    total += 2
                }
                if (has(Trait.revvedup)) {
                    if (arousal.percent() > 25) {
                        total += 5
                    }
                    if (arousal.percent() > 50) {
                        total += 5
                    }
                    if (arousal.percent() > 75) {
                        total += 5
                    }
                }
            }

            Attribute.Cunning -> if (has(Trait.inspired)) {
                total += 10
            }

            Attribute.Seduction -> {
                if (has(Trait.confidentdom) && (mood == Emotion.dominant || mood == Emotion.confident)) {
                    total *= 2
                }
                if (has(Trait.seductress)) {
                    total += 10
                }
                if (has(Trait.revvedup)) {
                    if (arousal.percent() > 25) {
                        total += 5
                    }
                    if (arousal.percent() > 50) {
                        total += 5
                    }
                    if (arousal.percent() > 75) {
                        total += 5
                    }
                }
            }

            Attribute.Arcane -> if (has(Trait.mystic)) {
                total += 2
            }

            Attribute.Dark -> {
                if (has(Trait.broody)) {
                    total += 2
                }
                if (has(Trait.darkness)) {
                    total += 10
                }
            }

            Attribute.Ki -> if (has(Trait.martial)) {
                total += 2
            }

            Attribute.Fetish -> {
                if (has(Trait.kinky)) {
                    total += 2
                }
                if (has(Trait.revvedup)) {
                    if (arousal.percent() > 25) {
                        total += 5
                    }
                    if (arousal.percent() > 50) {
                        total += 5
                    }
                    if (arousal.percent() > 75) {
                        total += 5
                    }
                }
            }

            Attribute.Science -> if (has(Trait.geeky)) {
                total += 2
            }

            Attribute.Ninjutsu -> if (has(Trait.stealthy)) {
                total += 2
            }

            Attribute.Animism -> if (has(Trait.furry)) {
                total += 2
            }

            Attribute.Speed -> {
                val bulky = tops.count { it.item.attribute == Trait.bulky } +
                        bottoms.count { it.item.attribute == Trait.bulky }
                total -= bulky
                if (has(Trait.streaker) && isNude) {
                    total += 4
                }
                if (has(Trait.flash)) {
                    total += 10
                }
            }

            else -> {}
        }
        return max(total.toInt(), 0)
    }

    fun getPure(a: Attribute): Int {
        return att[a] ?: 0
    }

    val advanced: Int
        get() = att.entries.sumOf {
            if(it.key.type == AttributeType.Advanced)
                it.value
            else 0
        }
    val advancedTrainingCost: Int
        get() = 400 + Constants.TRAININGSCALING * advanced

    fun hasSpecialization(): Boolean {
        return att.keys.any { it.isSpecialization }
    }

    fun mod(a: Attribute, i: Int) {
        if (att.containsKey(a)) {
            att[a] = att[a]!! + i
        } else {
            set(a, i)
        }
    }

    operator fun set(a: Attribute, i: Int) {
        att[a] = i
    }

    fun check(a: Attribute, dc: Int): Boolean {
        return if (get(a) == 0) {
            false
        } else {
            get(a) + Global.random(20) >= dc
        }
    }

    fun gainXP(i: Int, character: Character) {
        gainXP(i + lvlBonus(character))
    }

    fun gainXP(i: Int) {
        var scale = 1.0
        scale *= if (human()) {
            Global.getValue(Flag.PlayerScaling)
        } else {
            Global.getValue(Flag.NPCScaling)
        }
        if (has(Trait.fastLearner)) {
            scale += 0.1
        }
        if (has(Trait.veryfastLearner)) {
            scale += 0.1
        }
        if (Global.checkFlag(Flag.doublexp)) {
            scale += 1
        }
        if (has(Trait.hiddenpotential)) {
            scale += 0.3
        }
        if (has(Trait.limitedpotential)) {
            scale -= 0.2
        }
        xp += (i * scale).roundToInt()
    }

    open fun rankup() {
        rank++
    }

    abstract fun ding()

    //Damage
    fun scaleLevel(targetLevel: Int) {
        while (level < targetLevel) {
            ding()
        }
        xp = 0
    }
    fun addDamage(type: DamageType, amount: Double, area: Anatomy? = null, prof: Double = 0.0): Damage {
        val end = Damage(type, amount)
        if (area != Anatomy.soul ||
            (type != DamageType.Pleasure && type != DamageType.Pain)
            ) for (s in status) {
                end += s.damage(type, amount, area)
            }

        // Tempt
        end[DamageType.Tempt] *= prof
        if (has(Trait.imagination)) {
            end[DamageType.Tempt] *= 1.5
        }
        if (has(Trait.icequeen)) {
            end[DamageType.Tempt] *= 0.5
        }

        // Pleasure
        if (area != null && area != Anatomy.soul)
            for (s in status) {
                end[DamageType.Pleasure] *= s.sensitive(area)
            }
        if (has(Trait.desensitized)) {
            end[DamageType.Pleasure] -= 1.0
        }
        if (has(Trait.hairtrigger) && (area == Anatomy.genitals)) {
            end[DamageType.Pleasure] *= 2.0
        }
        if (has(Trait.veteranprostitute) && area == Anatomy.genitals) {
            end[DamageType.Pleasure] *= 0.75
        }
        if (has(Trait.silvercock) && area == Anatomy.genitals) {
            end[DamageType.Pleasure] *= 0.9
        }
        if (has(Trait.hardon) && (area == Anatomy.genitals)) {
            end[DamageType.Pleasure] *= 1.3
        }
        if (has(Trait.buttslut) && (area == Anatomy.ass)) {
            end[DamageType.Pleasure] *= 2.0
        }
        if (area == Anatomy.chest) {
            if (hasBreasts) {
                end[DamageType.Pleasure] *= 1.2
            }
        }

        // Pain
        if (area != null && area != Anatomy.soul)
            for (s in status) {
                end[DamageType.Pain] *= s.sore(area)
            }
        if (area == Anatomy.genitals) {
            if (has(Trait.achilles)) {
                end[DamageType.Pain] *= 1.5
            }
            if (has(Trait.brassballs)) {
                end[DamageType.Pain] *= 0.8
            }
            if (has(Trait.armored)) {
                end[DamageType.Pain] *= 0.3
            }
        }
        return end
    }

    fun applyDamage(damage: Damage) {
        arousal.edge(damage[DamageType.Tempt].roundToInt())
        arousal.restore(damage[DamageType.Pleasure].roundToInt())
        stamina.minimize(damage[DamageType.Weaken].roundToInt())
        stamina.reduce(damage[DamageType.Pain].roundToInt())
        arousal.reduce(damage[DamageType.Calm].roundToInt())
        stamina.restore(damage[DamageType.Heal].roundToInt())
    }

    fun pain(i: Double, area: Anatomy, combat: Combat? = null): Double {
        if (has(Trait.painimmune)) {
            return 0.0
        }
        val pain = max(1.0, i)
        val damage = addDamage(DamageType.Pain, pain, area)
        applyDamage(damage)
        val finalPain = damage[DamageType.Pain].roundToInt()
        emote(Emotion.angry, finalPain)
        if (combat != null) {
            combat.addEvent(this, CombatEvent(Result.receivepain, area))
            combat.addEvent(combat.getOther(this), CombatEvent(Result.dealpain, area))
            combat.reportDamage(this, damage)
        }
        return damage[DamageType.Pain]
    }

    fun weaken(i: Double, combat: Combat? = null): Double {
        if (has(Trait.weakimmune)) {
            return 0.0
        }
        val weak = max(1.0, i)
        val damage = addDamage(DamageType.Weaken, weak)
        applyDamage(damage)
        val finalWeak = damage[DamageType.Weaken].roundToInt()
        emote(Emotion.nervous, finalWeak)
        combat?.reportDamage(this, damage)
        return damage[DamageType.Weaken]
    }

    fun heal(i: Double, combat: Combat? =  null): Double {
        val damage = addDamage(DamageType.Heal, i)
        applyDamage(damage)
        val finalHeal = damage[DamageType.Heal].roundToInt()
        combat?.reportDamage(this, damage)
        emote(Emotion.confident, finalHeal)
        return damage[DamageType.Heal]
    }

    fun pleasure(
        i: Double,
        area: Anatomy,
        mod: Result = Result.normal,
        combat: Combat? = null
    ): Double {
        if (has(Trait.pleasureimmune)) {
            return 0.0
        }
        var pleasure = max(1.0, i)
        val value = max(1.0, i)

        if (mod == Result.foreplay) {
            pleasure = (value * .2) +
                    (value * .8 * (
                            min(1.0,
                                1.25 - (arousal.current / (.8 * arousal.max))
                            )))
        }
        if (mod == Result.finisher) {
            pleasure =
                (value * .2) + (value * .8 * (min(
                    1.0,
                    arousal.current / (.8 * arousal.max)
                )))
        }
        val damage = addDamage(DamageType.Pleasure, pleasure, area)
        applyDamage(damage)
        //pleasure = max(1.0, pleasure)
        if (combat != null) {
            combat.addEvent(this, CombatEvent(Result.receivepleasure, area))
            combat.addEvent(combat.getOther(this), CombatEvent(Result.dealpleasure, area))
            combat.reportDamage(this, damage, mod)
        }

        return damage[DamageType.Pleasure]
    }

    fun tempt(
        i: Double,
        prof: Double = 1.0,
        mod: Tag = Result.normal,
        combat: Combat? = null
    ): Double {
        if (arousal.isFull)
            return 0.0
        if (has(Trait.temptimmune)) {
            return 0.0
        }

        var temptation = max(1.0, i)
        if (mod === Result.foreplay) {
            temptation = temptation * .2 + (temptation * .8 * (min(
                1.0,
                1.25 - (arousal.current / (.8 * arousal.max))
            )))
        }
        if (mod === Result.finisher) {
            temptation = temptation * .2 + (temptation * .8 * (min(
                1.0,
                arousal.current / (.8 * arousal.max)
            )))
        }
        val damage = addDamage(DamageType.Tempt, temptation, prof = prof)
        applyDamage(damage)
        val finalTemptation = damage[DamageType.Tempt].roundToInt()
        emote(Emotion.horny, finalTemptation)

        if (combat != null) {
            combat.addEvent(this, CombatEvent(Result.receivetemptation))
            combat.addEvent(combat.getOther(this), CombatEvent(Result.dealtemptation))
            combat.reportDamage(this, damage)
        }
        return damage[DamageType.Tempt]
    }

    fun calm(i: Double, combat: Combat? = null): Double {
        val mag = i
        val damage = addDamage(DamageType.Calm, mag)
        applyDamage(damage)
        combat?.reportDamage(this, damage)
        return damage[DamageType.Calm]
    }

    operator fun get(res: Pool): Meter {
        if (!pools.containsKey(res)) {
            pools[res] = Meter(res.baseCap)
        }
        return pools[res]!!
    }

    fun buildMojoPercent(percent: Double) {
        if (has(Trait.lameglasses) || has(Trait.legend)) {
            return
        }
        if (has(Stsflag.mojodeny)) return
        var x = percent
        for (s in status) {
            x += s.gain(Pool.MOJO, x)
        }
        mojo.addPercent(x)
    }

    fun buildMojo(amount: Int) {
        if (has(Stsflag.mojodeny)) return
        if (has(Trait.lameglasses) || has(Trait.legend)) {
            return
        }
        var x = amount.toDouble()
        for (s in status) {
            x += s.gain(Pool.MOJO, x)
        }
        mojo.restore(x.roundToInt())
    }

    fun spendMojo(i: Int) {
        var cost = i.toDouble()
        for (s in status) {
            cost += s.spend(Pool.MOJO, i.toDouble())
        }
        if (has(Trait.overflowingmana)) {
            cost /= 20
        }
        mojo.reduce(cost.roundToInt())
    }

    fun spendArousal(i: Int) {
        var mag = i.toDouble()
        for (s in status) {
            mag += s.spend(Pool.AROUSAL, i.toDouble())
        }
        if (has(Trait.lustconduit)) {
            mag /= 2
        }
        if (has(Trait.infernalexertion)) {
            weaken(mag)
        } else {
            tempt(mag)
        }
    }

    fun spendStamina(i: Int) {
        weaken(i.toDouble())
    }

    fun spend(res: Pool, i: Int) {
        when (res) {
            Pool.MOJO -> spendMojo(i)
            Pool.AROUSAL -> spendArousal(i)
            Pool.STAMINA -> spendStamina(i)
            else -> {
                var cost = i.toDouble()
                for (s in status) {
                    cost += s.spend(res, i.toDouble())
                }
                get(res).reduce(cost.roundToInt())
            }
        }
    }

    val isBusy: Boolean get() = state != State.ready && state != State.quit

    val trackingScore: Int
        get() {
            var score = get(Attribute.Perception) * 3
            score += Global.random(level)
            if (has(Trait.tracker)) {
                score += 10
            }
            if (has(Trait.advtracker)) {
                score += 15
            }
            if (has(Trait.mastertracker)) {
                score += 20
            }
            return score
        }

    fun init(c: Combat): Int {
        var s = get(Attribute.Speed) + Global.random(10)
        if (c.stance.sub(this)) {
            s -= 2
        }
        if (c.stance.prone(this)) {
            s -= 2
        }
        return s
    }

    //Clothing
    val isNude: Boolean get() = isTopless && isPantsless

    val isTopless: Boolean get() {
        return tops.none { it.item.attribute != Trait.ineffective }
    }

    val isPantsless: Boolean get() {
        return bottoms.none { it.item.attribute != Trait.ineffective }
    }

    val canFuck: Boolean get() {
        return bottoms.none { it.item.attribute != Trait.ineffective && it.item.attribute != Trait.accessible }
    }

    val isErect: Boolean
        get() = arousal.current >= 10 || has(Trait.bronzecock) || has(Trait.strapped)

    fun dress(c: Combat) {
        for (article in outfit[0]) {
            if (tops.none { it.item == article }) tops.addNotNull(c.clothespile.firstOrNull { it.item == article && it.owner == this })
        }
        for (article in outfit[1]) {
            if (bottoms.none { it.item == article }) bottoms.addNotNull(c.clothespile.firstOrNull { it.item == article && it.owner == this })
        }
    }

    open fun change(rule: Modifier = Modifier.normal) {
        tops = ArrayDeque(outfit[0].map { OwnedItem(it, this) })
        bottoms = ArrayDeque(outfit[1].map { OwnedItem(it, this) })
    }

    fun undress(c: Combat) {
        c.clothespile.addAll(tops)
        c.clothespile.addAll(bottoms)
        c.clothespile.removeAll { it.item.isTemp }
        tops.clear()
        bottoms.clear()
    }

    fun nudify(liner: Boolean = false, combat: Combat? = null): Boolean {
        tops.removeAll { it.item.attribute != Trait.indestructible }
        bottoms.removeAll { it.item.attribute != Trait.indestructible }
        val nude = isNude
        if (nude && liner) combat!!.write(this, nakedLiner())
        return nude
    }

    fun stripAttempt(primaryAtt: Int, attacker: Character, c: Combat, target: Clothing): Boolean {
        if (!canAct()) {
            return true
        }
        var magnitude = primaryAtt * attacker.stamina.percent() / 100
        var dc = target.dc * (level * stamina.percent() / 100)
        if (has(Trait.modestlydressed)) {
            dc += 10.0
        }
        dc *= 1 - (arousal.percent() / 200)

        magnitude += status.sumOf { it.stripAttempt(target) }

        magnitude *= target.stripOffenseBonus(attacker)
        if (c.stance.dom(attacker)) {
            magnitude *= 1.2
        } else if (c.stance.sub(attacker)) {
            magnitude *= .8
        }
        magnitude += Global.random(20)
        if (magnitude <= dc && magnitude + 10 > dc) {
            add(Disheveled(this, target), c)
        }
        return magnitude > dc
    }

    fun strip(half: Int, c: Combat): Clothing? {
        return stripOwner(half, c)?.item
    }

    fun stripOwner(half: Int, c: Combat): OwnedItem<Clothing>? {
        if (half == 0) {
            if (isTopless) {
                return null
            } else {
                if (!tops.last().item.isTemp) {
                    c.clothespile.add(tops.last())
                }
                return tops.removeLast()
            }
        } else {
            if (isPantsless) {
                return null
            } else {
                if (!bottoms.last().item.isTemp) {
                    c.clothespile.add(bottoms.last())
                }
                return bottoms.removeLast()
            }
        }
    }

    fun stripRandom(c: Combat): Clothing? {
        return if (Global.random(2) == 0) {
            if (!tops.isEmpty()) {
                strip(0, c)
            } else {
                strip(1, c)
            }
        } else {
            if (!bottoms.isEmpty()) {
                strip(1, c)
            } else {
                strip(0, c)
            }
        }
    }

    fun shredOwner(half: Int): OwnedItem<Clothing>? {
        return if (half == 0) {
            if (!isTopless && tops.last().item.attribute != Trait.indestructible) {
                tops.removeLast()
            } else {
                null
            }
        } else {
            if (!isPantsless && bottoms.last().item.attribute != Trait.indestructible) {
                bottoms.removeLast()
            } else {
                null
            }
        }
    }

    fun shred(half: Int): Clothing? {
        return shredOwner(half)?.item
    }

    fun shredRandom(): Clothing? {
        return if (Global.random(2) == 0) {
            if (!tops.isEmpty()) {
                shred(OUTFITTOP)
            } else {
                shred(OUTFITBOTTOM)
            }
        } else {
            if (!bottoms.isEmpty()) {
                shred(OUTFITBOTTOM)
            } else {
                shred(OUTFITTOP)
            }
        }
    }

    fun canWear(article: Clothing): Boolean {
        return !isWearing(article.type)
    }

    fun isWearing(type: ClothingType?): Boolean {
        if (tops.any { it.item.type == type }) return true
        if (bottoms.any { it.item.type == type }) return true
        return false
    }

    fun removeLayer(type: ClothingType?) {
        tops.removeAll { it.item.type == type }
        bottoms.removeAll { it.item.type == type }
    }


    fun wear(article: OwnedItem<Clothing>) {
        when (article.item.type) {
            ClothingType.TOPUNDER -> tops.add(0, article)
            ClothingType.UNDERWEAR -> bottoms.add(0, article)
            ClothingType.TOP -> if (isWearing(ClothingType.TOPUNDER)) {
                tops.add(1, article)
            } else {
                tops.add(0, article)
            }

            ClothingType.TOPOUTER -> tops.addLast(article)
            else -> bottoms.addLast(article)
        }
    }

    fun wear(article: Clothing) {
        when (article.type) {
            ClothingType.TOPUNDER -> tops.add(0, OwnedItem(article, this))
            ClothingType.TOP -> if (isWearing(ClothingType.TOPUNDER)) {
                tops.add(1, OwnedItem(article, this))
            } else {
                tops.add(0, OwnedItem(article, this))
            }
            ClothingType.TOPOUTER -> tops.addLast(OwnedItem(article, this))

            ClothingType.UNDERWEAR -> bottoms.add(0, OwnedItem(article, this))
            else -> bottoms.addLast(OwnedItem(article, this))
        }
    }

    fun replaceArticle(article: Clothing) {
        if (canWear(article)) {
            wear(article)
        } else {
            removeLayer(article.type)
            wear(article)
        }
    }

    val skimpiness: Int
        get() {
            var result = 2
            if (tops.any { val item = it.item
                item.attribute != Trait.skimpy && item.attribute != Trait.ineffective
                }) result--
            if (bottoms.any { val item = it.item
                    item.attribute != Trait.skimpy && item.attribute != Trait.ineffective
                }) result--
            if (has(Trait.scandalous)) {
                result += 1
            }
            return result
        }

    fun getOutfitItem(slot: ClothingType): Clothing? {
        return outfit[0].firstOrNull { it.type == slot }
            ?: outfit[1].firstOrNull { it.type == slot }
    }

    val top: String get() {
        if (!isTopless) {
            return tops.last().item.getName()
        }
        return "shirt"
    }

    val bottom: String get() {
        if (!isPantsless) {
            if (bottoms.last().item.type == ClothingType.BOTOUTER) {
                return bottoms.last().item.getName()
            }
        }
        return "pants"
    }

    fun add(t: Trait) {
        traits.add(t)
    }

    fun remove(t: Trait) {
        traits.remove(t)
    }

    fun has(t: Trait): Boolean {
        if (tops.any { it.item.adds(t) }) return true
        if (bottoms.any { it.item.adds(t) }) return true
        if (grudge == t) return true
        if (matchmod == t) return true
        if (status.any { it.tempTraits(t) }) return true
        return traits.contains(t)
    }

    fun setModifier(t: Trait?) {
        matchmod = t
    }

    //Anatomy Checks
    val hasDick: Boolean get() =
        traits.contains(Trait.male) || traits.contains(Trait.herm)

    val hasBalls: Boolean get() =
        traits.contains(Trait.male) || traits.contains(Trait.herm)

    val hasPussy: Boolean get() =
        traits.contains(Trait.female) || traits.contains(Trait.herm)

    val hasBreasts: Boolean get() =
        traits.contains(Trait.female) || traits.contains(Trait.herm)

    fun countFeats(): Int {
        return traits.count { it.isFeat }
    }

    fun regen(combat: Boolean) {
        var regen = max(1.0, stamina.max / 50.0)
        regen += status.sumOf { it.regen() }
        if (has(Trait.BoundlessEnergy)) {
            regen += max(1.0, stamina.max / 20.0)
        }
        if (has(Trait.streaker) && isNude) {
            regen += max(1.0, stamina.max / 50.0)
        }
        if (has(Trait.healing)) {
            regen += max(1.0, stamina.max / 10.0)
        }
        heal(regen)
        var calming = 0.0
        if (has(Trait.confidentdom) && (mood == Emotion.dominant || mood == Emotion.confident)) {
            calming += arousal.max / 10.0
        }
        if (has(Trait.tantra)) {
            calming += arousal.max / 20.0
        }
        if (has(Trait.legend)) {
            calming += arousal.max / 10.0
        }
        if (has(Trait.freeenergy)) {
            get(Pool.BATTERY).restore(1)
        }
        calm(calming)
        if (combat) {
            var mojoGain = 0
            if (has(Trait.exhibitionist) && isNude) {
                mojoGain += 5
            }
            if (has(Trait.streaker) && isNude) {
                mojoGain += 10
            }
            if (has(Trait.stylish)) {
                mojoGain += 3
            }
            if (has(Trait.SexualGroove)) {
                mojoGain += 2
            }
            if (has(Trait.lame)) {
                mojoGain -= 3
            }
            buildMojo(mojoGain)
            if (has(Trait.spirited)) {
                buildMojoPercent(50.0)
            }
            if (getPure(Attribute.Spirituality) > 0
                && !has(Stsflag.mindaffecting) &&
                /*!has(Stsflag.cynical) &&*/ !has(Stsflag.stunned)
            ) {
                get(Pool.FOCUS).restore(1)
            }
            if (getPure(Attribute.Unknowable) > 0) {
                get(Pool.ENIGMA).restore(1)
            }
        }
    }

    fun add(status: Status, c: Combat? = null) {
        if (has(Trait.shameless) && status.flags.contains(Stsflag.shamed)) {
            c?.reportStatusFizzle(this, status)
            return
        }
        if (has(Trait.antihorny) && status.flags.contains(Stsflag.horny)) {
            c?.reportStatusFizzle(this, status)
            return
        }
        var cynical = false
        for (s in this.status.toList()) {
            if (s.flags.contains(Stsflag.cynical)) {
                cynical = true
            }
            if (s.toString() == status.toString()) {
                if (s.stacking) {
                    s.stack(status)
                }
                return
            }
        }
        if (cynical && status.flags.contains(Stsflag.mindaffecting)) {
            c?.reportStatusFizzle(this, status)
            return
        } else {
            c?.reportStatus(this, status)
            this.status.add(status)
        }
    }

    fun removeStatus(sts: Status, c: Combat? = null) {
        if (sts.removable()) {
            status.remove(sts)
            c?.reportStatusLoss(this, sts)
        }
    }

    fun removeStatus(flag: Stsflag, c: Combat? = null) {
        for (s in status.toList()) {
            if (s.flags.contains(flag) && s.removable()) {
                status.remove(s)
                c?.reportStatusLoss(this, s)
            }
        }
    }

    /*	public void dropStatus(){
		status.removeAll(removelist);
		for(Status s: addlist){
			add(s);
		}
		removelist.clear();
		addlist.clear();
	}*/
    fun has(sts: Stsflag): Boolean {
        return status.any { it.flags.contains(sts) }
    }

    fun stunned() = has(Stsflag.stunned)

    fun distracted(): Boolean {
        return status.any {
            it.flags.contains(Stsflag.distracted) ||
                    it.flags.contains(Stsflag.charmed)
        }
    }

    fun bound() = has(Stsflag.bound)

    fun free() {
        removeStatus(Stsflag.bound)
    }

    fun escape(): Int {
        var total = 0
        var maxbind = 0
        for (s in status) {
            if (s.flags.contains(Stsflag.bound)) {
                maxbind = min(maxbind, s.escape())
            } else {
                total += s.escape()
            }
        }
        total += maxbind
        if (has(Trait.houdini)) {
            total += get(Attribute.Cunning) / 5
        }
        if (has(Trait.freeSpirit)) {
            total += 2
        }
        return total
    }

    fun canAct(): Boolean {
        return !(stunned() || distracted() || bound() || has(Stsflag.enthralled))
    }

    fun canActNormally(c: Combat): Boolean {
        return canAct() && c.stance.mobile(this) && !c.stance.prone(this) && !c.stance.sub(this)
    }

    fun genitalsAvailable(c: Combat): Boolean {
        val available = isPantsless || (c.getOther(this).has(Trait.dexterous) && bottoms.size <= 1)
        if (!c.stance.reachBottom(c.getOther(this))) {
            return false
        }
        if (c.stance.penetration(this) && !(hasDick && hasPussy)) {
            return false
        }
        return available
    }

    abstract fun detect()
    abstract fun faceOff(opponent: Character, enc: Encounter)
    abstract fun spy(opponent: Character, enc: Encounter)
    abstract fun describe(per: Int): String?
    abstract fun victory(c: Combat, flag: Tag)
    abstract fun defeat(c: Combat, flag: Tag)
    abstract fun intervene3p(c: Combat, target: Character, assist: Character)
    abstract fun victory3p(c: Combat, target: Character, assist: Character)
    abstract fun watcher(c: Combat, victor: Character, defeated: Character)
    abstract fun watched(c: Combat, voyeur: Character, defeated: Character)
    abstract fun act(c: Combat)
    abstract fun move(match: Match)
    abstract fun draw(c: Combat, flag: Tag)
    abstract fun human(): Boolean
    abstract fun challenge(opponent: Character): String?
    abstract fun bbLiner(): String
    abstract fun nakedLiner(): String?
    abstract fun stunLiner(): String
    abstract fun taunt(): String
    abstract fun intervene(enc: Encounter, p1: Character, p2: Character)
    abstract fun resist3p(combat: Combat, intruder: Character, assist: Character): Boolean
    abstract fun showerScene(target: Character, encounter: Encounter)
    abstract fun load(loader: Scanner)
    abstract fun afterParty()
    fun preCombat(opponent: Character, c: Combat) {
        if (has(Trait.perfectplan)) {
            opponent.nudify()
        }
        if (has(Trait.ninjapreparation)) {
            opponent.add(Bound(opponent, 30, "net"))
            opponent.add(Horny(opponent, 3.0, 4), c)
            opponent.add(Drowsy(opponent, 4.0), c)
        }
        if (has(Trait.sadisticmood)) {
            opponent.add(Masochistic(opponent))
        }
        if (has(Trait.enthralling)) {
            opponent.add(Enthralled(opponent, this))
        }
        if (has(Trait.sparehandcuffs)) {
            gain(Consumable.Handcuffs)
            gain(Consumable.Handcuffs)
        }
        if (has(Trait.defensivemeasures)) {
            replaceArticle(Clothing.cup)
        }
        if (has(Trait.confidentdom)) {
            emote(Emotion.dominant, 50)
        }
        if (getPure(Attribute.Discipline) >= 1) {
            add(Composed(this, 5 + (get(Attribute.Discipline) / 2), get(Attribute.Discipline).toDouble()))
        }
        if (has(Trait.cheapshot)) {
            opponent.weaken(opponent.stamina.current.toDouble())
        }
    }

    fun endofbattle(opponent: Character?, c: Combat?) {
        for (s in status.toList()) {
            if (!s.lingering()) removeStatus(s)
        }
        if (pet != null) {
            pet!!.remove()
        }
        grudge = null
        //dropStatus();
    }
    abstract fun eot(c: Combat, opponent: Character)
    fun eotFull(c: Combat, opponent: Character) {
        for (s in status.toSet()) {
            s.turn(c)
        }
        //dropStatus();
        if (opponent.pet != null && canAct() && c.stance.mobile(this) && !c.stance.prone(this)) {
            if (get(Attribute.Speed) > opponent.pet!!.ac() * Global.random(20)) {
                opponent.pet!!.caught(c, this)
            }
        }
        // Includes pheromones and tailmastery check, which includes text, unfortunately
        eot(c, opponent)
        if (has(Trait.RawSexuality)) {
            tempt(1.0, combat = c)
            opponent.tempt(1.0, combat = c)
        }
        if ((getPure(Attribute.Animism) >= 4 && arousal.percent() >= 50) || has(Trait.feral) || (has(Trait.furaffinity) && arousal.percent() >= 25)) {
            if (!has(Stsflag.feral)) {
                add(Feral(this))
            }
        }
        eotMoodSwing(c, opponent)
    }
    abstract fun eotMoodSwing(c: Combat, opponent: Character)
    abstract fun moodSwing(): Emotion?
    abstract fun resetOutfit()
    open fun save(): JsonObject {
        val saver = JsonObject()
        saver.addProperty("Name", name)
        saver.addProperty("Level", level)
        saver.addProperty("Rank", rank)
        saver.addProperty("XP", xp)
        saver.addProperty("Money", money)
        saver.addProperty("Score", Scheduler.getScore(id))
        saver.addProperty("Benched", isBenched)

        val attributes = JsonObject()
        for (a in att.keys) {
            attributes.addProperty(a.toString(), att[a])
        }
        saver.add("Attributes", attributes)

        val pool = JsonObject()
        pools.keys.onEach { pool.addProperty(it.toString(), pools[it]!!.max) }
        saver.add("Pools", pool)

        val traits = JsonArray()
        for (t in this.traits) {
            traits.add(t.name)
        }
        saver.add("Traits", traits)

        val upper = JsonArray()
        for (c in outfit[OUTFITTOP]) {
            upper.add(c.name)
        }
        saver.add("Upper Clothing", upper)

        val lower = JsonArray()
        for (c in outfit[OUTFITBOTTOM]) {
            lower.add(c.name)
        }
        saver.add("Lower Clothing", lower)

        val allclothes = JsonArray()
        for (c in closet) {
            allclothes.add(c.name)
        }
        saver.add("Owned Clothes", allclothes)

        val items = JsonArray()
        for (i in inventory) {
            items.add(i.toString())
        }
        saver.add("Inventory", items)

        return saver
    }

    fun load(loader: JsonObject) {
        this.name = loader["Name"].asString
        this.level = loader["Level"].asInt
        this.rank = loader["Rank"].asInt
        this.xp = loader["XP"].asInt
        this.money = loader["Money"].asInt
        if (loader.has("Benched")) {
            isBenched = loader["Benched"].asBoolean
        }

        grudges.clear()
        mercy.clear()
        matchmod = null

        att.clear()
        val attributes = loader.getAsJsonObject("Attributes")
        for (a in Attribute.entries) {
            if (attributes.has(a.name)) {
                att[a] = attributes[a.name].asInt
            }
        }


        if (loader.has("Pools")) {
            val pool = loader.getAsJsonObject("Pools")
            pools.clear()
            for (res in Pool.entries) {
                if (pool.has(res.name)) {
                    pools[res] = Meter(pool[res.name].asInt)
                }
            }
        }
        if (loader.has("Stamina")) {
            stamina.setMax(loader["Stamina"].asInt)
        }
        if (loader.has("Arousal")) {
            arousal.setMax(loader["Arousal"].asInt)
        }
        if (loader.has("Mojo")) {
            mojo.setMax(loader["Mojo"].asInt)
        }

        traits.clear()
        for (t in loader.getAsJsonArray("Traits")) {
            traits.add(Trait.valueOf(t.asString))
        }
        if (human()) {
            outfit[OUTFITTOP].clear()
            for (t in loader.getAsJsonArray("Upper Clothing")) {
                outfit[OUTFITTOP].addLast(Clothing.valueOf(t.asString))
            }

            outfit[OUTFITBOTTOM].clear()
            for (t in loader.getAsJsonArray("Lower Clothing")) {
                outfit[OUTFITBOTTOM].addLast(Clothing.valueOf(t.asString))
            }
        } else {
            resetOutfit()
        }


        closet.clear()
        for (t in loader.getAsJsonArray("Owned Clothes")) {
            closet.add(Clothing.valueOf(t.asString))
        }
        inventory.clear()
        for (t in loader.getAsJsonArray("Inventory")) {
            inventory.add(Global.getItem(t.asString)!!)
        }
        Scheduler.setScore(id, loader["Score"].asInt)
        resetSkills()
        Global.gainSkills(this, false)
        stamina.fill()
        arousal.empty()
        mojo.empty()
    }

    fun resetSkills() {
        skills.clear()
        skills.add(Struggle(this))
        skills.add(Nothing(this))
        skills.add(Recover(this))
        skills.add(Straddle(this))
        skills.add(ReverseStraddle(this))
        skills.add(Stunned(this))
        skills.add(Distracted(this))
        skills.add(PullOut(this))
        flaskskills.clear()
        flaskskills.addAll(Flask.entries.map { UseFlask(this, it) })
        potionskills.clear()
        potionskills.addAll(Potion.entries.map { UsePotion(this, it) })
    }

    fun usableActions(): List<Action> {
        return SkillList.allActions.filter { it.usable(this) } +
            Global.actions.filter { it.usable(this) }
    }

    fun learn(copy: Skill) {
        skills.add(copy)
    }

    fun sneakValue(): Int {
        var sneak = 5
        if (state == State.ready) {
            sneak = (get(Attribute.Cunning).toDouble().pow(1.1) * 2).roundToInt()
        }
        if (has(Trait.Sneaky)) {
            sneak += 10
        }
        if (has(Trait.assassin) && Global.random(2) == 0) {
            sneak += 50
        }
        return sneak
    }

    fun listenCheck(sneak: Int): Boolean {
        return Global.random(get(Attribute.Perception) * (20 + level)) > sneak * 2
    }

    fun spotCheck(sneak: Int): Boolean {
        return Global.random(get(Attribute.Perception) * (20 + level)) > sneak
    }

    fun bonusDisarm(): Int {
        if (has(Trait.cautious)) {
            return 5
        }
        return 0
    }

    fun getSexPleasure(pace: Int, used: Attribute, area: Anatomy? = null): Double {
        var m = (3.0 * pace + get(used) / 2.0) * (.8 + (.1 * Global.random(4)))
        if (area != null) m = bonusProficiency(area, m)
        else if (has(Trait.strapped)) {
            m += get(Attribute.Science) / 2.0
            m = bonusProficiency(Anatomy.toy, m)
        } else {
            m = bonusProficiency(Anatomy.genitals, m)
        }
        return m
    }

    fun bonusRecoilPleasure(damage: Double): Double {
        var modifier: Double
        var cap = .1
        val shamedStatus = getStatus(Stsflag.shamed)
        if (shamedStatus != null) {
            cap -= shamedStatus.magnitude * .05
        }
        modifier = (get(Attribute.Seduction) * .01).coerceAtMost(cap)
        if (has(Trait.responsive)) {
            modifier += .2
        }
        if (has(Trait.ishida3rdart)) {
            modifier += get(Attribute.Ninjutsu) / 20.0
        }
        return damage * modifier
    }

    fun bonusPetPower(): Double {
        var total = 0.0
        if (has(Trait.leadership)) {
            total += get(Attribute.Perception) / 2.0
        }
        if (has(Trait.coordinatedStrikes)) {
            total += get(Attribute.Cunning) / 4.0
        }
        return total
    }

    fun bonusPetEvasion(): Int {
        var total = 0.0
        if (has(Trait.tactician)) {
            total += get(Attribute.Perception) / 2.0
        }
        if (has(Trait.evasiveManuevers)) {
            total += get(Attribute.Cunning) / 4.0
        }
        return total.roundToInt()
    }

    fun bonusEnthrallDuration(): Int {
        var total = 0.0
        total += get(Attribute.Dark) / 10.0
        total += get(Attribute.Hypnosis) / 3.0
        return total.roundToInt()
    }

    fun bonusCharmDuration(): Int {
        var total = 0.0
        if (skimpiness >= 2) {
            total += 1
        }
        total += get(Attribute.Hypnosis) / 3.0
        return total.roundToInt()
    }

    fun bonusProficiency(using: Anatomy, baseDamage: Double): Double {
        var prof = 1.0
        for (s in status) {
            prof *= s.proficiency(using)
        }
        if (has(Trait.legend)) {
            prof += 1
        }
        when (using) {
            Anatomy.fingers -> {
                if (has(Trait.handProficiency)) {
                    prof += .1
                }
                if (has(Trait.handExpertise)) {
                    prof += .1
                }
                if (has(Trait.handMastery)) {
                    prof += .1
                }
                if (has(Trait.mittens)) {
                    prof -= .75
                }
                if (has(Trait.smallhands)) {
                    prof += .3
                }
            }

            Anatomy.mouth -> {
                if (has(Trait.oralProficiency)) {
                    prof += .1
                }
                if (has(Trait.oralExpertise)) {
                    prof += .1
                }
                if (has(Trait.oralMastery)) {
                    prof += .1
                }
            }

            Anatomy.genitals -> {
                if (has(Trait.intercourseProficiency)) {
                    prof += .1
                }
                if (has(Trait.intercourseExpertise)) {
                    prof += .1
                }
                if (has(Trait.intercourseMastery)) {
                    prof += .1
                }
                if (has(Trait.succubusvagina)) {
                    prof += 1.5
                }
                if (has(Trait.hardon)) {
                    prof += 1
                }
            }

            Anatomy.feet -> {
                if (has(Trait.footloose)) {
                    prof += .5
                }
                if (has(Trait.striker)) {
                    prof += .3
                }
            }

            Anatomy.toy -> {
                if (has(Trait.toymaster)) {
                    prof += .2
                }
                if (has(Trait.experimentalweaponry)) {
                    prof += 1
                }
            }

            else -> {}
        }
        return prof * baseDamage
    }

    fun bonusTemptation(): Double {
        var prof = 1.0
        if (has(Trait.romantic)) {
            prof += .2
        }
        if (has(Trait.careerseductress)) {
            prof += .5
        }
        prof += skimpiness * .5
        return prof
    }

    fun bonusPin(baseDC: Double): Double {
        var scale = 1.0
        if (has(Trait.Clingy)) {
            scale += .1
        }
        if (has(Trait.predatorinstincts)) {
            scale += .5
        }
        return baseDC * scale
    }

    fun travel(dest: Area) {
        state = State.ready
        location.exit(this)
        location = dest
        dest.enter(this)
    }

    open fun flee(location2: Area) {
        val adjacent = location2.adjacent
        travel(adjacent.random())
        location2.endEncounter()
    }

    fun upkeep() {
        regen(false)
        if (has(Trait.Confident)) {
            mojo.reduce(5)
        } else {
            mojo.reduce(10)
        }
        if (bound()) {
            removeStatus(Stsflag.bound)
        }
        //dropStatus();
        if (has(Trait.QuickRecovery)) {
            heal(4.0)
        }
        if (has(Trait.exhibitionist) && isNude) {
            buildMojo(3)
        }
        for (s in status.toList()) {
            s.decay()
        }
        decayMood()
        moodSwing()
        update()
        //setChanged()
        //notifyObservers()
    }
    fun update() {
        gui?.update()
    }

    open fun income(amount: Int): Int {
        val total = amount
        money += total
        return total
    }

    open fun gain(item: Item) {
        inventory.add(item)
    }

    open fun gain(item: Clothing) {
        closet.add(item)
    }

    fun gain(item: Item, q: Int) {
        for (i in 0 until q) {
            gain(item)
        }
    }

    fun has(item: Item): Boolean {
        return inventory.contains(item)
    }

    fun hasAll(recipe: ArrayList<Item>): Boolean {
        val counts = HashMap<Item, Int>()
        for (i in recipe) {
            if (counts.keys.contains(i)) {
                counts[i] = counts[i]!! + 1
            } else {
                counts[i] = 1
            }
        }
        return counts.keys.none { !has(it, counts[it]!!) }
    }

    fun has(item: Item, quantity: Int): Boolean {
        var quantity = quantity
        for (i in inventory) {
            if (i === item) {
                quantity--
            }
            if (quantity <= 0) {
                return true
            }
        }
        return false
    }

    fun has(item: Clothing): Boolean {
        return closet.contains(item)
    }

    open fun canUse(item: Item, combat: Combat? = null): Boolean {
        return true
    }

    fun consume(item: Item, quantity: Int) {
        var quantity = quantity
        if (has(Trait.resourceful) && Global.random(5) == 0) {
            quantity--
        }
        for (i in 0 until quantity) {
            inventory.remove(item)
        }
    }

    fun consumeAll(items: ArrayList<Item>) {
        for (i in items) {
            consume(i, 1)
        }
    }

    fun remove(item: Item, quantity: Int) {
        for (i in 0 until quantity) {
            inventory.remove(item)
        }
    }

    fun count(item: Item): Int {
        return inventory.count { it === item }
    }

    //Special Currency
    fun chargeBattery() {
        pools[Pool.BATTERY]!!.fill()
    }

    fun addGrudge(target: Character, grudge: Trait) {
        grudges[target] = grudge
    }

    fun clearGrudge(target: Character) {
        grudges.remove(target)
    }

    fun defeated(victor: Character) {
        mercy.add(victor)
    }

    fun resetArousal() {
        if (!has(Trait.nosatisfaction)) {
            arousal.empty()
            if (has(Trait.insatiable)) {
                arousal.restore((arousal.max * .2).toInt())
            }
        }
    }

    fun resupply() {
        for (victor in mercy) {
            if (has(Trait.event)) {
                victor.bounty(5)
            } else {
                victor.bounty(1)
            }
        }
        mercy.clear()
        change(Scheduler.match!!.condition)
        state = State.ready
        if (location.present.size > 1) {
            if (location.areaId == Movement.dorm) {
                if (Scheduler.match!!.gps(Movement.quad)!!.present.isEmpty()) {
                    if (human()) {
                        Global.gui
                            .message("You hear your opponents searching around the dorm, so once you finish changing, you hop out the window and head to the quad.")
                    }
                    travel(Scheduler.match!!.gps(Movement.quad)!!)
                } else {
                    if (human()) {
                        Global.gui
                            .message("You hear your opponents searching around the dorm, so once you finish changing, you quietly move downstairs to the laundry room.")
                    }
                    travel(Scheduler.match!!.gps(Movement.laundry)!!)
                }
            }
            if (location.areaId == Movement.union) {
                if (Scheduler.match!!.gps(Movement.union)!!.present.isEmpty()) {
                    if (human()) {
                        Global.gui
                            .message("You don't want to be ambushed leaving the student union, so once you finish changing, you hop out the window and head to the quad.")
                    }
                    travel(Scheduler.match!!.gps(Movement.quad)!!)
                } else {
                    if (human()) {
                        Global.gui
                            .message("You don't want to be ambushed leaving the student union, so once you finish changing, you sneak out the back door and head to the pool.")
                    }
                    travel(Scheduler.match!!.gps(Movement.pool)!!)
                }
            }
        }
    }

    fun finishMatch() {
        for (victor in mercy) {
            if (has(Trait.event)) {
                victor.bounty(5)
            } else {
                victor.bounty(1)
            }
        }
        inventory.removeAll { it is Envelope }
        grudges.clear()
        mercy.clear()
        matchmod = null
        rest()
    }

    fun place(loc: Area) {
        location = loc
        loc.present.add(this)
    }

    fun rest() {
        change(Modifier.normal)
        clearStatus()
        for (res in pools.values) {
            res.empty()
        }
        stamina.fill()
    }

    fun bounty(points: Int) {
        Scheduler.match!!.score(this, points)
    }

    fun eligible(p2: Character): Boolean {
        return (!mercy.contains(p2)) && state != State.resupplying
    }

    open fun bathe() {
        status.clear()
        stamina.fill()
        state = State.ready
    }

    open fun masturbate() {
        arousal.empty()
        state = State.ready
    }

    fun craft() {
        var roll = Global.random(10)
        if (check(Attribute.Cunning, 40)) {
            if (roll >= 7 && Global.checkFlag(Flag.Kat)) {
                gain(Potion.Furlixir)
            } else {
                gain(Flask.PAphrodisiac)
            }
        }

        roll = Global.random(10)
        if (check(Attribute.Ninjutsu, 5)) {
            if (roll == 9) {
                gain(Consumable.needle)
                gain(Consumable.smoke)
                gain(Flask.Sedative)
            } else if (roll >= 7) {
                gain(Flask.Sedative)
            } else if (roll >= 5) {
                gain(Consumable.smoke)
            } else if (roll >= 3) {
                gain(Consumable.needle)
            }
        }

        roll = Global.random(10)
        if (check(Attribute.Science, 20)) {
            if (roll == 9) {
                gain(Potion.Fox)
                gain(Potion.Nymph)
                gain(Potion.Bull)
            } else if (roll >= 7) {
                gain(Potion.Cat)
            } else if (roll >= 5) {
                gain(Potion.Fox)
            } else if (roll >= 3) {
                gain(Potion.Nymph)
            } else if (roll >= 1) {
                gain(Potion.Bull)
            } else {
                gain(Potion.SuperEnergyDrink)
            }
        } else if (check(Attribute.Science, 5)) {
            if (roll == 9) {
                gain(Flask.Aphrodisiac, 2)
                gain(Flask.DisSol)
                gain(Potion.SuperEnergyDrink)
            } else if (roll >= 7) {
                gain(Flask.Aphrodisiac)
                gain(Flask.DisSol)
                gain(Potion.SuperEnergyDrink)
            } else if (roll >= 5) {
                gain(Flask.Lubricant, 3)
                gain(Potion.SuperEnergyDrink)
            } else if (roll >= 3) {
                gain(Flask.Lubricant)
                gain(Flask.Sedative)
                gain(Potion.SuperEnergyDrink)
            } else {
                gain(Potion.SuperEnergyDrink)
            }
        } else if (check(Attribute.Cunning, 25)) {
            if (roll == 9) {
                gain(Flask.Aphrodisiac)
                gain(Flask.DisSol)
            } else if (roll >= 5) {
                gain(Flask.Aphrodisiac)
            } else {
                gain(Flask.Lubricant)
                gain(Flask.Sedative)
            }
        } else if (check(Attribute.Cunning, 20)) {
            if (roll == 9) {
                gain(Flask.Aphrodisiac)
            } else if (roll >= 7) {
                gain(Flask.DisSol)
            } else if (roll >= 5) {
                gain(Flask.Lubricant)
            } else if (roll >= 3) {
                gain(Flask.Sedative)
            } else {
                gain(Potion.EnergyDrink)
            }
        } else if (check(Attribute.Cunning, 15)) {
            if (roll == 9) {
                gain(Flask.Aphrodisiac)
            } else if (roll >= 8) {
                gain(Flask.DisSol)
            } else if (roll >= 7) {
                gain(Flask.Lubricant)
            } else if (roll >= 6) {
                gain(Potion.EnergyDrink)
            } else if (human()) {
                Global.gui.message("Your concoction turns a sickly color and releases a foul smelling smoke. You trash it before you do any more damage.")
            }
        } else {
            if (roll >= 7) {
                gain(Flask.Lubricant)
            } else if (roll >= 5) {
                gain(Flask.Sedative)
            } else if (human()) {
                Global.gui.message("Your concoction turns a sickly color and releases a foul smelling smoke. You trash it before you do any more damage.")
            }
        }
        state = State.ready
    }

    fun search() {
        val roll = Global.random(10)
        when (roll) {
            9 -> {
                gain(Component.Tripwire)
                gain(Component.Tripwire)
            }

            8 -> {
                gain(Consumable.ZipTie)
                gain(Consumable.ZipTie)
                gain(Consumable.ZipTie)
            }

            7 -> gain(Component.Phone)
            6 -> gain(Component.Rope)
            5 -> gain(Component.Spring)
            else -> if (human()) {
                Global.gui.message("You don't find anything useful")
            }
        }
        state = State.ready
    }

    fun delay(i: Int) {
        busy += i
    }

    abstract fun promptTrap(enc: Encounter, target: Character, trap: Trap)
    fun lvlBonus(opponent: Character): Int {
        if (opponent.level > this.level) {
            if (opponent.has(Trait.event)) {
                return 50
            }
            return 5 * (opponent.level - this.level)
        } else {
            return 0
        }
    }

    fun getAttraction(other: Character): Int {
        return Roster.getAttraction(id, other.id)
    }

    fun gainAttraction(other: Character, x: Int) {
        Roster.gainAttraction(id, other.id, x)
    }

    fun getAffection(other: Character): Int {
        return Roster.getAffection(id, other.id)
    }

    fun gainAffection(other: Character, x: Int) {
        Roster.gainAffection(id, other.id, x)
    }

    val randomFriend: Character?
        get() {
            val known = ArrayList<Character>()
            for (person in Roster.existingPlayers) {
                if (!person.human() && Roster.getAttraction(person.id, id) > 0) {
                    known.add(person)
                }
            }
            return known.randomOrNull()
        }

    fun ac(): Int {
        var ac = (get(Attribute.Cunning) / 4) + 15
        for (s in status) {
            ac += s.evade().toInt()
        }
        if (has(Trait.clairvoyance)) {
            ac += 1
        }
        if (has(Trait.untouchable)) {
            ac += 5
        }
        if (has(Trait.speeddemon)) {
            ac += 5
        }
        if (!canAct()) {
            ac -= 999
        }
        return ac
    }

    fun resist(): Int {
        val ac = (get(Attribute.Cunning) / 4) + 15
        return ac
    }

    fun bonusCounter(): Int {
        var counter = 0
        for (s in status) {
            counter += s.counter()
        }
        if (has(Trait.clairvoyance)) {
            counter += 3
        }
        if (has(Trait.aikidoNovice)) {
            counter += 3
        }
        return counter
    }

    fun tohit(): Int {
        val hit = get(Attribute.Speed) / 2 + get(Attribute.Perception)
        return hit
    }

    fun attackRoll(attack: Skill, c: Combat, accuracy: Int): Boolean {
        val attackroll = Global.random(20) + 1
        val tohit = attackroll + tohit() + accuracy
        if (attackroll == 20) {
            return true
        } else if (attackroll == 0 || tohit < ac()) {
            if (tohit + 10 < ac() + bonusCounter()) {
                counterattack(attack.user, attack.type(), c)
            }
            return false
        }
        return true
    }

    fun knockdownDC(): Int {
        var dc = stamina.current / 4.0
        if (has(Stsflag.braced)) {
            dc += getStatus(Stsflag.braced)!!.value()
        }
        if (getPure(Attribute.Discipline) >= 9 && has(Stsflag.composed)) {
            dc += getStatus(Stsflag.composed)!!.magnitude
        }
        if (has(Trait.landsonfeet)) {
            dc += 50
        }
        return dc.roundToInt()
    }

    abstract fun counterattack(target: Character, type: Tactics, c: Combat)

    fun clearStatus() {
        status.clear()
    }

    fun getStatus(flag: Stsflag?): Status? {
        if (flag == null) return null
        return status.firstOrNull { it.flags.contains(flag) }
    }

    fun getStatusMagnitude(name: String): Double {
        return status.firstOrNull { it.toString() === name }?.magnitude
            ?: 0.0
    }

    fun canReadBasic(target: Character): Boolean {
        if (get(Attribute.Perception) > 3) {
            if (target === this || !target.has(Stsflag.unreadable) || get(Attribute.Perception) > 6) {
                return true
            }
        }
        return false
    }

    fun getBasicArousalDamage(type: String?, amount: Int): String {
        val p = amount * 1.0 / arousal.max
        return if (p > .25) {
            "extreme"
        } else if (p > .15) {
            "intense"
        } else if (p > .1) {
            "strong"
        } else if (p > .05) {
            "moderate"
        } else if (p > .02) {
            "a little"
        } else {
            "minimal"
        }
    }

    fun getBasicPain(amount: Int): String {
        val p = amount * 1.0 / stamina.max
        return if (p > .25) {
            "severe"
        } else if (p > .15) {
            "intense"
        } else if (p > .1) {
            "major"
        } else if (p > .05) {
            "manageable"
        } else if (p > .02) {
            "small"
        } else {
            "negligible"
        }
    }

    fun getBasicWeaken(amount: Int): String {
        val p = amount * 1.0 / stamina.max
        return if (p > .25) {
            "severely"
        } else if (p > .15) {
            "Heavily"
        } else if (p > .1) {
            "significantly"
        } else if (p > .05) {
            "moderately"
        } else if (p > .02) {
            "slightly"
        } else {
            "minimally"
        }
    }

    fun getBasicCalm(amount: Int): String {
        val p = amount * 1.0 / arousal.max
        return if (p > .7) {
            "completely"
        } else if (p > .4) {
            "a lot"
        } else if (p > .2) {
            "significantly"
        } else if (p > .1) {
            "moderately"
        } else if (p > .03) {
            "slightly"
        } else {
            "minimally"
        }
    }

    fun getBasicHeal(amount: Int): String {
        val p = amount * 1.0 / stamina.max
        return if (p > .7) {
            "completely"
        } else if (p > .4) {
            "a lot"
        } else if (p > .2) {
            "significantly"
        } else if (p > .1) {
            "moderately"
        } else if (p > .03) {
            "slightly"
        } else {
            "minimally"
        }
    }

    fun canReadAdvanced(target: Character): Boolean {
        return (target === this && get(Attribute.Perception) > 3) ||
                (!target.has(Stsflag.unreadable) && get(Attribute.Perception) > 6)
    }

    fun prize(): Int {
        return 50 + (100 * (rank.toDouble().pow(2.0).toInt()))
    }

    fun emote(emo: Emotion, amt: Int) {
        emotes[emo] = min((emotes[emo]!! + amt).toDouble(), 100.0).toInt()
        val inverse = emo.inverse()
        emotes[inverse] = max((emotes[inverse]!! - amt / 2).toDouble(), 0.0).toInt()
        if (emotes[emo]!! < 0) {
            emotes[emo] = 0
        }
        if (emotes[inverse]!! < 0) {
            emotes[inverse] = 0
        }
    }

    fun decayMood() {
        for (e in emotes.keys) {
            if (isNude && !has(Trait.exhibitionist) && !has(Trait.shameless) && e == Emotion.nervous) {
                emotes[e] = emotes[e]!! - ((emotes[e]!! - 50) / 2)
            } else if (isNude && has(Trait.exhibitionist) && e == Emotion.horny) {
                emotes[e] = emotes[e]!! - ((emotes[e]!! - 50) / 2)
            } else if (!isNude && e == Emotion.confident) {
                emotes[e] = emotes[e]!! - ((emotes[e]!! - 50) / 2)
            } else {
                if (emotes[e]!! >= 5) {
                    emotes[e] = emotes[e]!! - (emotes[e]!! / 2)
                }
            }
        }
    }

    fun findPath (target: Area?): Move? {
        if (target == null) {
            println("null target")
            return null
        }
        if (location == target) {
            println("move in place")
            return null
        }
        val qu = ArrayDeque<Area>()
        val vector = ArrayList<Area>()
        val parents = HashMap<Area, Area>()
        qu.addLast(location)
        vector.add(location)
        var last: Area? = null// = location!!findP
        while (qu.isNotEmpty()) {
            var set = false
            val t = qu.removeFirst()
            for (area in t.adjacent) {
                if (!vector.contains(area)){
                    parents[area] = t
                    if (area.name == target.name) {
                        last = area
                        set = true
                        break
                    }
                    qu.addLast(area)
                    vector.add(area)
                }
            }
            if (set) break
        }
        if (last == null) println("location not found in adjacents")
        if (last == null) return null
        while (parents[last] != location) last = parents[last]
        return Move(last!!)
    }

    fun knows(skill: Skill): Boolean {
        return skills.any { it == skill }
    }

    fun canSpend(res: Pool, amount: Int): Boolean {
        var cost = amount.toDouble()

        for (s in status) {
            cost += s.spend(res, amount.toDouble())
        }
        if (res == Pool.MOJO && has(Trait.overflowingmana)) {
            cost /= 20
        }
        return mojo.current >= cost
    }

    /*fun getStatus(): ArrayList<Status> {
        val result = ArrayList(status)
        return result
    }*/

    fun listStatus(): ArrayList<String> {
        val result = ArrayList<String>()
        for (s in status) {
            result.add(s.toString())
        }
        return result
    }

    fun dumpstats(): String {
        var s = "$name: Level $level; "
        for (a in att.keys) {
            s += "${a.name} ${att[a]}, "
        }
        s +=
            "<br>Max Stamina ${stamina.max}" +
                    ", Max Arousal ${arousal.max}" +
                    ", Max Mojo ${mojo.max}" +
                    ", Affection ${getAffection(Global.player)}.<p>"
        return s
    }

    fun accept(c: Challenge) {
        challenges.add(c)
    }

    fun evalChallenges(c: Combat, victor: Character?) {
        for (chal in challenges) {
            chal.check(c, victor)
        }
    }

    val urgency: Double
        get() {
            // How urgent do we want the fight to end? We are
            // essentially trying to approximate how many
            // moves we will still have until the end of the
            // fight.

            // Once arousal is too high, the fight ends.

            val remArousal = arousal.max - arousal.current

            // Arousal is worse the more clothes we lose, because
            // we will probably lose it quicker.
            var arousalPerRound = 5.0
            for (i in tops.indices) arousalPerRound /= 1.2
            for (i in bottoms.indices) arousalPerRound /= 1.2

            // Depending on our stamina, we should count on
            // spending a few of these rounds knocked down,
            // so effectively we have even less rounds to
            // work with - let's make them count!
            val fitness = max(
                0.5, min(1.0, stamina.current.toDouble() / 40)
            )

            return remArousal / arousalPerRound * fitness
        }

    fun rateUrgency(urgency: Double, urgency2: Double): Double {
        // How important is it to reach a goal with given
        // urgency, given that we are gunning roughly for
        // the second urgency? We use a normal distribution
        // (hah, fancy!) that gives roughly 50% weight at
        // a difference of 15.
        val x = (urgency - urgency2) / 15
        return exp(-x * x)
    }

    fun getFitness(urgency: Double, p: Position): Double {
        var fit = 0.0
        // Urgency marks
        val ushort = rateUrgency(urgency, 5.0)
        val umid = rateUrgency(urgency, 15.0)
        val ulong = rateUrgency(urgency, 25.0)
        val usum = ushort + umid + ulong
        // Always important: PositionK
        if (p.dom(this)) {
            fit += 2
        }
        if (p.sub(this)) {
            fit -= 2
        }
        if (p.reachTop(this)) {
            fit += 1
        }
        if (p.reachBottom(this)) {
            fit += 1
        }
        if (p.mobile(this)) {
            fit += 2
        }
        if (p.penetration(this) && p.dom(this)) {
            fit += 5
        }
        if (p.behind(this)) {
            fit += 1
        }
        // Also important: Clothing.
        // Especially when aroused (protects vs KO)
        fit += ushort * 3 * (tops.size + bottoms.size)
        // Also somewhat of a factor: Inventory (so we don't
        // just use it without thinking)
        for (item in inventory) fit += item.price / 10
        // Extreme situations
        if (arousal.isFull) fit -= 99
        if (stamina.isEmpty) fit -= umid * 3
        // Short-term: Arousal
        fit += ushort / usum * (arousal.max - arousal.current)
        // Mid-term: Stamina
        fit += umid / usum * stamina.current
        // Long term: Mojo
        fit += ulong / usum * mojo.current
        // TODO: Pets, status effects...
        return fit
    }

    abstract val portrait: String?
    fun pronounSubject(capital: Boolean): String {
        if (capital) {
            if (has(Trait.male)) {
                return "He"
            }
            return "She"
        }
        if (has(Trait.male)) {
            return "he"
        }
        return "she"
    }

    fun pronounTarget(capital: Boolean): String {
        if (capital) {
            if (has(Trait.male)) {
                return "Him"
            }
            return "Her"
        }
        if (has(Trait.male)) {
            return "him"
        }
        return "her"
    }

    fun possessive(capital: Boolean): String {
        if (capital) {
            if (has(Trait.male)) {
                return "His"
            }
            return "Her"
        }
        if (has(Trait.male)) {
            return "his"
        }
        return "her"
    }

    fun matchPrep(m: Match) {
        rest()
        change(m.condition)
        state = State.ready
        Global.gainSkills(this)
        if (getPure(Attribute.Ninjutsu) >= 9) {
            placeStash(m)
        }
        if (has(Trait.challengeSeeker)) {
            Challenge(m).resolve(this)
        }
        if (getPure(Attribute.Science) >= 1) {
            get(Pool.BATTERY).fill()
        }
        if (m.condition == Modifier.slippery && human()) {
            val s: Status = Oiled(this)
            s.fix()
            add(s)
        }
        if (m.condition == Modifier.furry && !human()) {
            val s: Status = Beastform(this)
            s.fix()
            add(s)
        }
    }

    private fun placeStash(m: Match) {
        val location = when (Global.random(6)) {
            0 -> Movement.library
            1 -> Movement.dining
            2 -> Movement.lab
            3 -> Movement.workshop
            4 -> Movement.storage
            else -> Movement.la
        }
        val loc = m.gps(location)
        loc!!.place(NinjaStash(this))
        if (human()) {
            Global.gui.message("<b>You've arranged for a hidden stash to be placed in the $loc.</b>")
        }
    }

    protected open val costumeSet: Int
        get() = 1
    open val spriteImage: BufferedImage?
        get() {
            val costume = costumeSet
            var res = ""
            if (Global.gui.lowRes()) {
                res = "_low"
            }
            if (spriteBody == null) {
                var path = AssetLoader.getResource(
                    "assets/$name/$name${costumeSet}_Sprite$res.png"
                )
                if (path == null) {
                    path = AssetLoader.getResource(
                        "assets/$name/${name}_Sprite$res.png"
                    )
                }
                try {
                    if (path != null) spriteBody = ImageIO.read(path)
                } catch (_: IOException) {
                }
            }
            if (spriteBody == null) {
                return null
            }
            val sprite = BufferedImage(spriteBody!!.width, spriteBody!!.height, spriteBody!!.type)
            val g = sprite.createGraphics()
            if (isWearing(ClothingType.TOPOUTER)) {
                if (spriteCoattail == null) {
                    try {
                        spriteCoattail = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_outerback$res.png"
                            )
                        )
                        g.drawImage(spriteCoattail, 0, 0, null)
                    } catch (_: IOException) {
                    }
                } else {
                    g.drawImage(spriteCoattail, 0, 0, null)
                }
            }
            if (isWearing(ClothingType.TOP)) {
                if (spriteCoattail == null) {
                    try {
                        spriteShirttail = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_topback$res.png"
                            )
                        )
                        g.drawImage(spriteShirttail, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteShirttail, 0, 0, null)
                }
            }
            g.drawImage(spriteBody, 0, 0, null)
            try {
                val face = ImageIO.read(
                    AssetLoader.getResource(
                        "assets/$name/${name}_${mood.name}$res.png"
                    )
                )
                g.drawImage(face, 0, 0, null)
            } catch (_: IOException) {
            } catch (_: IllegalArgumentException) {
            }
            if (arousal.percent() >= 75) {
                if (blushHigh == null) {
                    try {
                        blushHigh = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/${name}_blush3$res.png"
                            )
                        )
                        g.drawImage(blushHigh, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(blushHigh, 0, 0, null)
                }
            } else if (arousal.percent() >= 50) {
                if (blushMed == null) {
                    try {
                        blushMed = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/${name}_blush2$res.png"
                            )
                        )
                        g.drawImage(blushLow, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(blushMed, 0, 0, null)
                }
            }
            if (arousal.percent() >= 25) {
                if (blushLow == null) {
                    try {
                        blushLow = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/${name}_blush1$res.png"
                            )
                        )
                        g.drawImage(blushLow, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(blushLow, 0, 0, null)
                }
            }
            if (spriteAccessory == null) {
                try {
                    spriteAccessory = ImageIO.read(
                        AssetLoader.getResource(
                            "assets/$name/$name${costume}_accessory$res.png"
                        )
                    )
                    g.drawImage(spriteAccessory, 0, 0, null)
                } catch (_: IOException) {
                } catch (_: IllegalArgumentException) {
                }
            } else {
                g.drawImage(spriteAccessory, 0, 0, null)
            }
            if (has(Stsflag.beastform)) {
                if (spriteKemono == null) {
                    try {
                        spriteKemono = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costumeSet}_kemono$res.png"
                            )
                        )
                        g.drawImage(spriteKemono, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteKemono, 0, 0, null)
                }
                if (spriteKemono == null) {
                    try {
                        spriteKemono = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/${name}_kemono$res.png"
                            )
                        )
                        g.drawImage(spriteKemono, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                }
            } else {
                if (spriteAccessory2 == null) {
                    try {
                        spriteAccessory2 = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_accessory2$res.png"
                            )
                        )
                        g.drawImage(spriteAccessory2, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteAccessory2, 0, 0, null)
                }
            }
            if (isWearing(ClothingType.UNDERWEAR)) {
                if (has(Trait.strapped)) {
                    if (spriteStrapon == null) {
                        try {
                            spriteStrapon = ImageIO.read(
                                AssetLoader.getResource(
                                    "assets/$name/${name}_strapon$res.png"
                                )
                            )
                            g.drawImage(spriteStrapon, 0, 0, null)
                        } catch (_: IOException) {
                        } catch (_: IllegalArgumentException) {
                        }
                    } else {
                        g.drawImage(spriteStrapon, 0, 0, null)
                    }
                } else if (spriteUnderwear == null) {
                    try {
                        spriteUnderwear = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_underwear$res.png"
                            )
                        )
                        g.drawImage(spriteUnderwear, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteUnderwear, 0, 0, null)
                }
            }
            if (isWearing(ClothingType.TOPUNDER)) {
                if (spriteBra == null) {
                    try {
                        spriteBra = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_bra$res.png"
                            )
                        )
                        g.drawImage(spriteBra, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteBra, 0, 0, null)
                }
            }
            if (isWearing(ClothingType.BOTOUTER)) {
                if (spriteBottom == null) {
                    try {
                        spriteBottom = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_bottom$res.png"
                            )
                        )
                        g.drawImage(spriteBottom, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteBottom, 0, 0, null)
                }
            }
            if (isWearing(ClothingType.TOP)) {
                if (spriteTop == null) {
                    try {
                        spriteTop = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_top$res.png"
                            )
                        )
                        g.drawImage(spriteTop, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteTop, 0, 0, null)
                }
            }
            if (isWearing(ClothingType.TOPOUTER)) {
                if (spriteOuter == null) {
                    try {
                        spriteOuter = ImageIO.read(
                            AssetLoader.getResource(
                                "assets/$name/$name${costume}_outer$res.png"
                            )
                        )
                        g.drawImage(spriteOuter, 0, 0, null)
                    } catch (_: IOException) {
                    } catch (_: IllegalArgumentException) {
                    }
                } else {
                    g.drawImage(spriteOuter, 0, 0, null)
                }
            }
            if (hasDick && !isWearing(ClothingType.BOTOUTER)) {
                if (!isErect) {
                    if (spritePenisSoft == null) {
                        try {
                            spritePenisSoft = ImageIO.read(
                                AssetLoader.getResource(
                                    "assets/$name/${name}_penis_soft$res.png"
                                )
                            )
                            g.drawImage(spritePenisSoft, 0, 0, null)
                        } catch (_: IOException) {
                        } catch (_: IllegalArgumentException) {
                        }
                    } else {
                        g.drawImage(spritePenisSoft, 0, 0, null)
                    }
                } else {
                    if (spritePenisHard == null) {
                        try {
                            spritePenisHard = ImageIO.read(
                                AssetLoader.getResource(
                                    "assets/$name/${name}_penis_hard$res.png"
                                )
                            )
                            g.drawImage(spritePenisHard, 0, 0, null)
                        } catch (_: IOException) {
                        } catch (_: IllegalArgumentException) {
                        }
                    } else {
                        g.drawImage(spritePenisHard, 0, 0, null)
                    }
                }
            }
            if (Global.checkFlag(Flag.statussprites)) {
                val sts = Global.gui.loadStatusFilters(this)
                g.drawImage(sts, 0, 0, null)
            }
            g.dispose()
            return sprite
        }

    fun clearSpriteImages() {
        spriteBody = null
        spriteAccessory = null
        spriteUnderwear = null
        spriteStrapon = null
        spriteBra = null
        spriteBottom = null
        spriteTop = null
        spriteOuter = null
        spriteCoattail = null
        spritePenisHard = null
        spritePenisSoft = null
        blushLow = null
        blushMed = null
        blushHigh = null
    }

    companion object {
        const val OUTFITTOP: Int = 0
        const val OUTFITBOTTOM: Int = 1
    }
}

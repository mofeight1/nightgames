package characters

import comments.CommentGroup
import comments.CommentSituation
import comments.SkillComment
import actions.Action
import actions.Movement
import combat.Combat
import combat.Result
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import items.Clothing
import items.Consumable
import items.Toy
import items.Trophy
import scenes.SceneFlag
import scenes.SceneManager
import skills.BunshinService
import skills.KoushoukaBurst
import skills.Maneuver
import skills.Needle
import skills.Skill
import skills.SkillTag
import skills.StealClothes
import skills.Tackle
import skills.Tie
import status.Stsflag

class Yui : Personality {
    override var character: NPC = NPC("Yui", ID.YUI, 1, this)

    init {
        character.outfit[0].add(Clothing.kunoichitop)
        character.outfit[1].add(Clothing.panties)
        character.outfit[1].add(Clothing.ninjapants)
        character.closet.add(Clothing.bra)
        character.closet.add(Clothing.kunoichitop)
        character.closet.add(Clothing.halfcloak)
        character.closet.add(Clothing.panties)
        character.closet.add(Clothing.ninjapants)
        character.change(Modifier.normal)
        character.underwear = Trophy.YuiTrophy
        character.add(Trait.female)
        character.add(Trait.shy)
        character.plan = Emotion.sneaking
        character.mood = Emotion.confident
        character.strategy[Emotion.sneaking] = 5
        character.strategy[Emotion.bored] = 2
        character.mod(Attribute.Cunning, 2)
        character.mod(Attribute.Ninjutsu, 5)
        character.mod(Attribute.Speed, 2)
        character.gain(Consumable.needle, 20)
        character.gain(Consumable.smoke, 10)
        Global.gainSkills(character)
        character.preferredSkills.add(Tackle(character))
        character.preferredSkills.add(BunshinService(character))
        character.preferredSkills.add(Needle(character))
        character.preferredSkills.add(Tie(character))
        character.preferredSkills.add(KoushoukaBurst(character))
        character.preferredSkills.add(Maneuver(character))
        character.preferredSkills.add(StealClothes(character))
    }


    override fun act(available: HashSet<Skill>, c: Combat): Skill {
        val mandatory = HashSet<Skill>()
        for (a in available) {
            if (a.toString() === "Command" || a.toString().equals("Fertility Rite", ignoreCase = true)) {
                mandatory.add(a)
            }
            if (character.has(Stsflag.orderedstrip)) {
                if (a.toString() === "Undress" || a.toString() === "Strip Tease") {
                    mandatory.add(a)
                }
            }
        }
        if (mandatory.isNotEmpty()) {
           return mandatory.random()
        }
        return chooseSkill(available, c)
    }

    /*override fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        val proposed = character.parseMoves(available, radar, match)
        return proposed
    }*/

    override fun rest(time: Int, day: Daytime) {
        var loc: String
        val available = ArrayList<String>()
        available.add("Hardware Store")
        available.add("Black Market")
        available.add("XXX Store")
        available.add("Bookstore")
        available.add("Dojo")
        available.add("Play Video Games")
        available.add("Workshop")
        if (!(character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && character.money >= 300) {
            character.gain(Toy.Onahole)
            character.money -= 300
        }
        if (!(character.has(Toy.Tickler) || character.has(Toy.Tickler2)) && character.money >= 300) {
            character.gain(Toy.Tickler)
            character.money -= 300
        }
        if (!(character.has(Toy.Dildo) || character.has(Toy.Dildo2)) && character.money >= 250) {
            character.gain(Toy.Dildo)
            character.money -= 250
        }
        if (!(character.has(Toy.Crop) || character.has(Toy.Crop2)) && character.money >= 200) {
            character.gain(Toy.Crop)
            character.money -= 200
        }
        for (i in 0 until time - 1) {
            loc = available.random()
            day.visit(loc, character, Global.random(character.money))
        }
        character.visit(1)
    }

    override fun bbLiner(): String {
        return when (Global.random(2)) {
            1 -> "<i>\"Master, the groin makes a better target than the eyes, throat, and solar plexus combined.  You should improve your ability to protect your considerably vulnerable testicles.  I will do everything I can to help you practice,\"</i> she says with a quick bow."
            else -> "Yui gives a quick bow of apology. <i>\"Sorry Master. I was trained to always target my opponent's weakest point.\"</i>"
        }
    }

    override fun nakedLiner(): String {
        return ("A deep blush colors Yui's cheeks, but she doesn't cover herself. <i>\"It's fine,\"</i> she whispers to herself unconvincingly. "
                + "<i>\"A kunoichi doesn't get embarrassed, especially not in front of her master.\"</i>")
    }

    override fun stunLiner(): String {
        return "Yui lets out a quiet grunt of exertion as she tries to get back to her feet. <i>\"That was a good hit. I wasn't quite fast enough to dodge it.\"</i>"
    }

    fun winningLiner(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun taunt(): String {
        return "<i>\"Master, I thought you were better than this. Are you letting me win? Ah! You must be pent up. I'll help you cum immediately.\"</i>"
    }

    override fun victory(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.anal) {
                Global.modCounter(Flag.PlayerAssLosses, 1.0)
                SceneManager.play(SceneFlag.YuiPeggingVictory)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.YuiSexVictory)
            } else if (c.lastact(character) != null && c.lastact(character)!!.hasTag(SkillTag.CLONE)) {
                SceneManager.play(SceneFlag.YuiBunshinVictory)
            } else if (opponent.has(Stsflag.bound)) {
                SceneManager.play(SceneFlag.YuiBoundVictory)
            } else {
                SceneManager.play(SceneFlag.YuiForeplayVictory)
            }
        }
    }

    override fun defeat(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        declareGrudge(opponent, c)
        if (opponent.human()) {
            if (flag === Result.anal && c.stance.sub(character)) {
                SceneManager.play(SceneFlag.YuiAnalDefeat)
            }
            if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.YuiSexDefeat)
            } else if (character.has(Stsflag.shamed)) {
                SceneManager.play(SceneFlag.YuiShamedDefeat)
            } else if (!character.isTopless && !character.isPantsless) {
                SceneManager.play(SceneFlag.YuiClothedDefeat)
            } else if (Global.random(2) == 0) {
                SceneManager.play(SceneFlag.YuiForeplayDefeatAlt)
            } else {
                SceneManager.play(SceneFlag.YuiForeplayDefeat)
            }
        }
    }

    override fun victory3p(c: Combat, target: Character, assist: Character): String {
        character.clearGrudge(target)
        character.clearGrudge(assist)
        return if (target.human()) {
            ("Yui stands over you and looks over your naked, restrained body excitedly. <i>\"I'm sorry Master, but this is how the game works, right?\"</i> "
                    + "She tries to sound calm, but you can tell she's practically drooling with anticipation. She straddles your hips and positions her slick "
                    + "entrance above your cock. <i>\"I'll finish you with the utmost care and pleasure.\"</i><p>"
                    + "Yui slowly lowers her hips to take your length inside her. You both let out a moan in unison as she starts to move her hips rhythmically. This "
                    + "seems like a risky strategy when she's as turned on as you are, but you know she has some tricks up her sleeve. Her vaginal walls suddenly contract "
                    + "around your shaft and your hips buck involuntarily. <i>\"How do you like the Ishida Kunoichi's third hidden art?\"</i> Her voice is slightly strained, "
                    + "but hot with desire. <i>\"It takes a lot of concentration, but if you can't resist, I can make you cum quickly.\"</i><p>"
                    + "Her insides continue to contract and relax in time with her hip movements, rapidly milking you to orgasm. You shoot your load inside her and feel "
                    + "her orgasm about 20 seconds later.")
        } else {
            if (target.hasDick) {
                String.format(
                    "Yui stares at %s's exposed penis with intense curiosity. <i>\"It's very different than master's. I guess dicks really "
                            + "do come in all shapes and sizes...\"</i> She grabs the shaft with both hands and begins a practiced handjob. <i>\"But they all cum, just the same.\"</i><br>"
                            + "She flashes you an expectant grin. Her pun only earns a shrug, but you give her a smile for the effort. She seems sufficiently "
                            + "encouraged anyway. <i>\"Of course, your penis is still my favorite, Master. If you see any techniques you want me to use on you after "
                            + "the match, just let me know. Oh! Like this one.\"</i> She pulls a black cloth from her outfit and wraps it around %s's cock. <i>\"It's pure silk, "
                            + "very pleasant on sensitive skin.\"</i> She rapidly moves the cloth back and forth, like she's shining a shoe. %s lets out a passionate "
                            + "moan at the sensation. In no time at all, %s covers the cloth in hot cum.<br>"
                            + "Yui gives you another expectant look. This time she gets a thumbs up for execution.",
                    target.name, target.name, target.pronounSubject(false)
                )
            } else {
                String.format(
                    "Yui looks down at %s with a dominance you aren't used to seeing from her. It's slightly comical since her fight left her nude. "
                            + "<i>\"You were a worthy opponent for me alone, but you were no match for the unbreakable trust between me and my Master.\"</i> "
                            + "She kneels confidently between the helpless girl's legs. <i>\"Don't worry, I'll give you a nice pleasant orgasm.\"</i><br> "
                            + "Yui begins to finger %s, who lets out a gasp of surprise. Yui notices her victim's confusion and holds up a compact, fingertip-size "
                            + "vibrator. <i>\"Are you enjoying the Ishida Kunoichi Hidden Weapon technique? You make some really cute sounds. "
                            + "Let Master and I hear some more of them.\"</i><br>"
                            + "%s lets out quite a few moans before, during, and even slightly after she climaxes at Yui's relentless hands. The cute ninja seems "
                            + "to have a bit of a dominant streak when it comes to other girls.",
                    target.name, target.name, target.name
                )
            }
        }
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character): String {
        return if (target.human()) {
            ("Your fight with " + assist.name + " has barely started when you hear a familiar voice call out to you. <i>\"Master! I was hoping you would be here.\"</i> " +
                    "Before you can react, Yui grabs you and eagerly kisses you on the lips. Your surprise quickly gives way to extreme lightheadedness and drowsiness. " +
                    "Your legs give out and you collapse into her arms. Did Yui drug you? <i>\"Please forgive this betrayal, Master. You work so hard fighting and training " +
                    "every night. For the sake of your health, I thought it was necessary to make you take a break.\"</i> She sounds genuinely apologetic, but also a little " +
                    "excited. <br>"
                    + "<i>\"Don't worry. We'll take good care of you until you can move again.\"</i> She carefully lowers your limp upper body onto her lap as " +
                    assist.name + " fondles your dick to full hardness. <i>\"I'm sure we can relieve some of your built up stress too.\"</i><br>")
        } else {
            ("This fight could certainly have gone better than this. You're completely naked and have your hands bound behind your back. " + target.name + " is just taking " +
                    "her time to finish you off. A familiar voice calls out to her. <i>\"I see you've caught my master. I've always wanted to get him in this position.\"</i> You " +
                    "both surprised to see Yui standing nearby. She hadn't made a sound when she approached. <i>\"Do you mind if I play with him for a moment? I promise I " +
                    "won't make him cum.\"</i><p>"
                    + "She kneels in front of you, fondling your balls playfully, but you suddenly feel her freeing your hands. She leans close to your ear and whispers quietly. "
                    + "<i>\"I'll create an opening so you can finish her off, Master.\"</i><br>"
                    + "She stands up casually as if to walk away, but suddenly grabs " + target.name + "'s arms. "
                    + "You scramble to your feet and join the fight. In no time at all, you and your ninja companion have her completely immobilized.")
        }
    }

    override fun watched(c: Combat, target: Character, viewer: Character) {
        if (viewer.human()) {
            SceneManager.play(SceneFlag.YuiWatch, target)
        }
    }

    override fun describe(): String {
        return ("Yui is a much more convincing kunoichi in her combat outfit. Her stylish ninja top gives her freedom of movement and is skimpy enough "
                + "to show off her toned midriff and a fair bit of side-boob. It's a little distracting, which is probably the intention. "
                + "In contrast to her exposed body, her face is hidden behind her long blonde bangs. A real shame, since you know she's quite beautiful.")
    }

    override fun draw(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (c.state == Result.intercourse) {
            SceneManager.play(SceneFlag.YuiSexDraw)
        } else {
            SceneManager.play(SceneFlag.YuiForeplayDraw)
        }
    }

    override fun fightFlight(opponent: Character): Boolean {
        return !character.isNude || opponent.isNude
    }

    override fun attack(opponent: Character): Boolean {
        return true
    }

    override fun ding() {
        character.mod(Attribute.Ninjutsu, 1)
        var rand: Int
        for (i in 0 until (Global.random(3) / 2) + 1) {
            rand = Global.random(4)
            when (rand) {
                0 -> {
                    character.mod(Attribute.Power, 1)
                }
                1 -> {
                    character.mod(Attribute.Seduction, 1)
                }
                2 -> {
                    character.mod(Attribute.Cunning, 1)
                }
                else -> {
                    character.mod(Attribute.Ki, 1)
                }
            }
        }
        character.stamina.gainMax(5)
        character.arousal.gainMax(5)
        character.mojo.gainMax(3)
    }

    override fun startBattle(opponent: Character): String {
        if (character.grudge != null) {
            when (character.grudge) {
                Trait.ninjapreparation -> return ("As you approach Yui, she suddenly sprints away. You quickly give chase, but are blindsided by a spring-propelled net "
                        + "after a few steps. As you struggle to untangle yourself from the net, your head starts to swim, and you realize Yui "
                        + "got you with a couple of her drugged needles while you were distracted.<p>"
                        + "The kunoichi girl approaches to finish you off. <i>\"Sorry Master, but you should know it's dangerous to pursue a ninja.\"</i>")

                Trait.flash -> return ("After your last fight, you confidently stride toward Yui, but she dashes backward to keep a safe distance. She seems pretty cautious. "
                        + "More importantly, she's a lot quicker than you realized.<p>"
                        + "<i>\"Of course, Master. I am a ninja, after all. I can be extremely nimble when I need to be.\"</i>")

                Trait.ishida3rdart -> return ("Yui looks a little more nervous than usual, but smiles at you excitedly. <i>\"Master, I know I sometimes have "
                        + "trouble concentrating when we're doing... intense things,\"</i> her hands moves unconsciously to her groin. "
                        + "<i>\"But I've been doing image training to prepare. If you put it inside me again, I'm sure I'll do my techniques "
                        + "properly this time.\"</i>")

                else -> {}
            }
        }
        if (character.isNude) {
            return ("Yui gives you a sheepish grin as she tries to cover her naked body. <i>\"I guess I got a little careless, Master. Normally I wouldn't "
                    + "let anyone but you undress me.\"</i>")
        }
        if (opponent.isPantsless) {
            return ("<i>\"Ah! Master!\"</i> Yui starts to bow to you, but freezes partway through. She seems to be staring at your naked crotch with intensity. "
                    + "You cough to get her attention.<p>"
                    + "<i>\"Oh, right! The match! I was just planning my strategy. I definitely wasn't daydreaming about how I lost my virginity.\"</i>")
        }
        if (character.getAffection(opponent) >= 30) {
            return "Yui smiles brightly as she sees you. "
        }
        return ("Yui gives you a polite, eager bow. <i>\"Master, let's have a good sparring match. Just like our training, but "
                + "with more pleasure!\"</i>")
    }

    override fun fit(): Boolean {
        return character.stamina.percent() >= 60 && character.arousal.percent() <= 20 && !character.isNude
    }

    override fun night(): Boolean {
        SceneManager.play(SceneFlag.YuiAfterMatch)
        return true
    }

    override fun advance(rank: Int) {
        if (rank >= 2 && !character.has(Trait.assassin)) {
            character.add(Trait.assassin)
        }
    }

    override fun checkMood(mood: Emotion, value: Int): Boolean {
        return when (mood) {
            Emotion.confident -> value >= 30
            else -> value >= 70
        }
    }

    override fun moodWeight(mood: Emotion): Double {
        return when (mood) {
            Emotion.confident -> 1.2
            else -> .8
        }
    }

    override fun image(): String {
        return "assets/yui_" + character.mood.name + ".jpg"
    }

    /*override fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }*/

    override fun resist3p(c: Combat, target: Character, assist: Character): String {
        // TODO Auto-generated method stub
        return ""
    }

    override val comments: CommentGroup
        get() {
            val comments = CommentGroup()
            comments[CommentSituation.VAG_DOM_CATCH_WIN] = "<i>\"Master, don't hold back. I'll happily accept your seed.\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_WIN] = "<i>\"Master, don't hold back. I'll happily accept your seed.\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_LOSE] = "<i>\"It feels so good! I can't hold back!\"</i>"
            comments[CommentSituation.VAG_DOM_CATCH_LOSE] = "<i>\"Master! Ah! Even on top, your dick is irresistible!\"</i>"
            comments[CommentSituation.ANAL_PITCH_WIN] = "<i>\"It appears I've found your weak spot, Master. Give in, and let your student practice dominating you with her techniques!\"</i>"
            comments[CommentSituation.ANAL_PITCH_LOSE] = "<i>\"Master! Incredible! Even stimulating you with a toy, I'm left breathless with pleasure!\"</i>"
            comments[CommentSituation.BEHIND_DOM_WIN] = "<i>\"Master, you shouldn't leave your back open when a ninja is nearby.\"</i>"
            comments[CommentSituation.BEHIND_SUB_LOSE] = "<i>\"From behind!? How!?\"</i>"
            comments[CommentSituation.SIXTYNINE_WIN] = "<i>\"Your dick is really throbbing. Are you enjoying my technique?\"</i>"
            comments[CommentSituation.SIXTYNINE_LOSE] = "<i>\"Master, you're too good at this! I can't keep up!\"</i>"
            comments[CommentSituation.OTHER_BOUND] = "<i>\"Master, you look a little tied up. Don't worry, I'll handle your needy penis.\"</i>"
            comments[CommentSituation.SELF_BOUND] = "<i>\"Well done, Master. You caught your very own ninja girl. I'll be out of this in a snap!\"</i>"
            comments[CommentSituation.SELF_HORNY] = "<i>\"Please, Master... I don't care if I lose, just give me your love!\"</i>"
            comments[CommentSituation.SELF_CHARMED] = "<i>\"Yes, Master. Whatever you need.\"</i>"
            comments[CommentSituation.ANAL_CATCH_LOSE] = "<i>\"In my butt!? That's a really advanced technique!\"</i>"
            comments[CommentSituation.OTHER_STUNNED] = "<i>\"I didn't expect you to go down so quickly. I'll give you lots of pleasure if you don't resist.\"</i>"
            comments[CommentSituation.OTHER_OILED] = "<i>\"Preparations are complete. Time for a well-lubricated handjob!\"</i>"
            comments[CommentSituation.SELF_SHAMED] = "<i>\"M-Master, don't look! It's embarrassing...\"</i>"
            comments[CommentSituation.PIN_DOM_WIN] = "<i>\"I've practiced this hold, you won't get away so easily.\"</i>"
            comments[CommentSituation.PIN_SUB_LOSE] = "<i>\"I know how to break this hold...ah...um... This might be trickier than I thought.\"</i>"
            comments[CommentSituation.SELF_BUSTED] = "In a high, weak voice, Yui says, <i>\"Thank you for the lesson, master, I still have much to learn.\"</i>"
            comments[SkillComment(SkillTag.PET, true)] = "<i>\"You summoned backup? Oh! I can still outnumber you with my bunshin techniques.\"</i>"
            comments[SkillComment(Attribute.Ninjutsu, true)] = "<i>\"Nice technique, Master! You're learning quickly!\"</i>"
            comments[SkillComment(Attribute.Science, true)] = "<i>\"Ninjutsu uses a lot of tools, but I've never used a gadget that elaborate.\"</i>"
            comments[SkillComment(SkillTag.TOY, false)] = "<i>\"How do you like my hidden weapon technique, Master?\"</i>"

            return comments
        }

    override val responses: CommentGroup
        get() {
            val comments = CommentGroup()
            return comments
        }

    override val costumeSet: Int
        get() = 1

    override fun declareGrudge(opponent: Character, c: Combat) {
        if (c.eval(character) == Result.intercourse) {
            character.addGrudge(opponent, Trait.ishida3rdart)
        } else {
            when (Global.random(2)) {
                0 -> character.addGrudge(opponent, Trait.flash)
                1 -> character.addGrudge(opponent, Trait.ninjapreparation)
                else -> {}
            }
        }
    }

    override fun resetOutfit() {
        character.outfit[0].clear()
        character.outfit[1].clear()
        character.outfit[0].add(Clothing.kunoichitop)
        character.outfit[1].add(Clothing.panties)
        character.outfit[1].add(Clothing.ninjapants)
    }
}

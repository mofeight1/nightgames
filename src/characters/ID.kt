package characters

enum class ID {
    PLAYER,
    CASSIE,
    MARA,
    ANGEL,
    JEWEL,
    YUI,
    KAT,
    REYKA,
    EVE,
    SAMANTHA,
    MAYA,
    VALERIE,
    SELENE,
    JUSTINE,
    AESOP,
    LILLY,
    AISHA,
    SUZUME,
    JETT,
    RIN,
    ALICE,
    GINETTE,
    CAROLINE,
    SARAH,
    MEI,
    SOFIA,
    CUSTOM1,
    CUSTOM2,
    CUSTOM3,
    CUSTOM4,
    CUSTOM5;


    companion object {
        fun fromString(name: String): ID {
            return if (name.equals("Cassie", ignoreCase = true)) {
                CASSIE
            } else if (name.equals("Mara", ignoreCase = true)) {
                MARA
            } else if (name.equals("Angel", ignoreCase = true)) {
                ANGEL
            } else if (name.equals("Jewel", ignoreCase = true)) {
                JEWEL
            } else if (name.equals("Yui", ignoreCase = true)) {
                YUI
            } else if (name.equals("Kat", ignoreCase = true)) {
                KAT
            } else if (name.equals("Reyka", ignoreCase = true)) {
                REYKA
            } else if (name.equals("Eve", ignoreCase = true)) {
                EVE
            } else if (name.equals("Samantha", ignoreCase = true)) {
                SAMANTHA
            } else if (name.equals("Maya", ignoreCase = true)) {
                MAYA
            } else if (name.equals("Valerie", ignoreCase = true)) {
                VALERIE
            } else if (name.equals("Sofia", ignoreCase = true)) {
                SOFIA
            } else if (name.equals("Selene", ignoreCase = true)) {
                SELENE
            } else {
                PLAYER
            }
        }
    }
}

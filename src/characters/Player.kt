package characters

import actions.Leap
import actions.Move
import actions.Shortcut
import areas.Area
import combat.Combat
import combat.Encounter
import combat.Tag
import global.Constants
import global.Flag
import global.Global
import global.Match
import global.Modifier
import global.Roster
import global.Scheduler
import items.Clothing
import items.ClothingType
import items.Component
import items.Consumable
import items.Flask
import items.Item
import items.OwnedItem
import items.Potion
import items.Toy
import items.Trophy
import skills.Skill
import skills.Tactics
import stance.Behind
import status.Enthralled
import status.Horny
import status.Stsflag
import trap.Trap
import java.util.Scanner
import kotlin.math.roundToInt

class Player(name: String) : Character(name, 1) {
    var attpoints: Int = 0
    var pings: HashSet<Area>

    init {
        id = ID.PLAYER
        att[Attribute.Power] =
            (Constants.STARTINGPOWER * Global.getValue(Flag.PlayerBaseStrength)).roundToInt()
        att[Attribute.Cunning] =
            (Constants.STARTINGCUNNING * Global.getValue(Flag.PlayerBaseStrength)).roundToInt()
        att[Attribute.Seduction] =
            (Constants.STARTINGSEDUCTION * Global.getValue(Flag.PlayerBaseStrength)).roundToInt()
        att[Attribute.Perception] = Constants.STARTINGPERCEPTION
        att[Attribute.Speed] =
            (Constants.STARTINGSPEED * Global.getValue(Flag.PlayerBaseStrength)).roundToInt()
        money = 0
        stamina = Meter((Constants.STARTINGSTAMINA * Global.getValue(Flag.PlayerBaseStrength)).roundToInt())
        stamina.fill()
        arousal = Meter((Constants.STARTINGAROUSAL * Global.getValue(Flag.PlayerBaseStrength)).roundToInt())
        mojo = Meter((Constants.STARTINGMOJO * Global.getValue(Flag.PlayerBaseStrength)).roundToInt())
        add(Trait.male)
        outfit[0].add(Clothing.Tshirt)
        outfit[1].add(Clothing.boxers)
        outfit[1].add(Clothing.jeans)
        closet.add(Clothing.Tshirt)
        closet.add(Clothing.boxers)
        closet.add(Clothing.jeans)
        change(Modifier.normal)
        underwear = Trophy.PlayerTrophy
        pings = HashSet()
    }

    override fun describe(per: Int): String {
        val description = StringBuilder()
        description.append("<i>")
        for (s in status) {
            if (s.describe() != null && s.describe() !== "") {
                description.append(s.describe() + "<br>")
            }
        }
        description.append("</i>")
        if (tops.isEmpty() && bottoms.isEmpty()) {
            description.append("You are completely naked.")
        } else {
            if (tops.isEmpty()) {
                description.append("You are shirtless and ")
                if (bottoms.isNotEmpty()) {
                    description.append("wearing ")
                }
            } else {
                description.append("You are wearing a " + tops.last() + " and ")
            }
            if (bottoms.isEmpty()) {
                description.append("are naked from the waist down.")
            } else {
                description.append("" + bottoms.last() + ".")
            }
        }
        for (p in pools.keys) {
            if (p != Pool.STAMINA && p != Pool.AROUSAL && p != Pool.MOJO) {
                if (pools[p]!!.current > 0) {
                    description.append("<br>")
                    description.append(p.label + ": " + pools[p]!!.current)
                }
            }
        }
        return description.toString()
    }

    override fun victory(c: Combat, flag: Tag) {
        if (c.stance.penetration(this)) {
            Global.modCounter(Flag.IntercourseWins, 1.0)
        }
        /*if (!c.hasModifier(Modifier.practice)) {
            if (has(Trait.insatiable)) {
                arousal.set((arousal.max * .5).roundToInt())
            }
        }*/
    }

    override fun defeat(c: Combat, flag: Tag) {
        //c.write("Bad thing")
    }

    override fun act(c: Combat) {
        gui!!.clearCommand()
        val target = if (c.p1 === this) {
            c.p2
        } else {
            c.p1
        }

        val skillArray = ArrayList<Skill>()
        val demandsArray = ArrayList<Skill>()
        val flaskArray = ArrayList<Skill>()
        val potionArray = ArrayList<Skill>()

        for (a in skills) {
            if (a.usable(c, target) && c.isAllowed(a, this)) {
                if (a.type() == Tactics.demand) {
                    demandsArray.add(a)
                } else {
                    skillArray.add(a)
                }
            }
        }

        for (s in flaskskills) if (s.usable(c, target) && c.isAllowed(s, this)) {
            flaskArray.add(s)
        }
        for (s in potionskills) if (s.usable(c, target) && c.isAllowed(s, this)) {
            potionArray.add(s)
        }


        // sort them.
        skillArray.sort()
        demandsArray.sort()
        flaskArray.sort()
        potionArray.sort()


        // add them to the gui.
        for (s in skillArray) gui!!.addSkill(c, s)
        for (s in demandsArray) gui!!.addDemands(s, c)
        for (s in flaskArray) gui!!.addFlasks(s, c)
        for (s in potionArray) gui!!.addPotions(s, c)

        // change end -GD
        gui!!.showSkills()
    }

    override fun human(): Boolean {
        return true
    }

    override fun draw(c: Combat, flag: Tag) {
    }

    override fun change(rule: Modifier) {
        when(rule) {
            Modifier.nudist -> {
                tops.clear()
                bottoms.clear()
            }
            Modifier.pantsman -> {
                tops.clear()
                bottoms.clear()
                for (article in outfit[OUTFITBOTTOM]) {
                    if (article.type == ClothingType.UNDERWEAR) {
                        wear(article)
                    }
                }
                for (article in outfit[OUTFITTOP]) {
                    if (article.type == ClothingType.TOPUNDER) {
                        wear(article)
                    }
                }
            }
            else -> {
                tops = ArrayDeque(outfit[OUTFITTOP].map { OwnedItem(it, this) })
                bottoms = ArrayDeque(outfit[OUTFITBOTTOM].map { OwnedItem(it, this) })
            }
        }
    }

    override fun bbLiner(): String {
        return ""
    }

    override fun nakedLiner(): String? {
        return null
    }

    override fun stunLiner(): String {
        return ""
    }

    fun winningLiner(): String? {
        return null
    }

    override fun taunt(): String {
        return ""
    }

    override fun detect() {
        pings.clear()
        for (distant in Scheduler.match!!.areas) {
            if (distant !== location && !location.adjacent.contains(distant) && distant.alarm) {
                Global.gui.message("You can faintly hear a far-away noise from the <b>" + distant.name + "</b>.")
                pings.add(distant)
            }
        }
        for (adjacent in location.adjacent) {
            if (adjacent.ping(this)) {
                Global.gui.message("You hear something in the <b>" + adjacent.name + "</b>.")
                pings.add(adjacent)
            }
        }
    }

    fun track() {
        for (trace in location.traces) {
            if (trace.person !== this) {
                trace.track(trackingScore)
            }
        }
    }

    fun opponentDetected(location: Area): Boolean {
        return pings.contains(location)
    }

    override fun faceOff(opponent: Character, enc: Encounter) {
        gui!!.message("You run into <b>" + opponent.name + "</b> and you both hesitate for a moment, deciding whether to attack or retreat.")
        assessOpponent(opponent)
        for (nearby in location.present) {
            if (nearby !== this && nearby !== opponent) {
                gui!!.message("<b>" + nearby.name + "</b> is watching nearby and may get involved.")
            }
        }
        gui!!.promptFF(enc, opponent)
    }

    private fun assessOpponent(opponent: Character) {
        val arousal: String
        val stamina: String
        if (opponent.state == State.webbed) {
            gui!!.message("She is naked and helpless<br>")
            return
        }
        if (get(Attribute.Perception) >= 6) {
            gui!!.message("She is level " + opponent.level)
        }
        if (get(Attribute.Perception) >= 8) {
            gui!!.message("Her Power is " + opponent[Attribute.Power] + ", her Cunning is " + opponent[Attribute.Cunning] + ", and her Seduction is " + opponent[Attribute.Seduction])
        }
        if (opponent.isNude || opponent.state == State.shower) {
            gui!!.message("She is completely naked.")
        } else {
            gui!!.message("She is dressed and ready to fight.")
        }
        if (get(Attribute.Perception) >= 4) {
            arousal = if (opponent.arousal.percent() > 70) {
                "horny"
            } else if (opponent.arousal.percent() > 30) {
                "slightly aroused"
            } else {
                "composed"
            }
            stamina = if (opponent.stamina.percent() < 50) {
                "tired"
            } else {
                "eager"
            }
            gui!!.message("She looks $stamina and $arousal.")
        }
    }

    override fun spy(opponent: Character, enc: Encounter) {
        gui!!.message("You spot <b>" + opponent.name + "</b> but she hasn't seen you yet. You could probably catch her off guard, or you could slip away before she notices you.")
        assessOpponent(opponent)
        for (nearby in location.present) {
            if (nearby !== this && nearby !== opponent) {
                gui!!.message("<b>" + nearby.name + "</b> is watching nearby and may get involved.")
            }
        }
        gui!!.promptAmbush(enc, opponent)
        for (path in location.adjacent) {
            gui!!.addAction(Move(path), this)
        }
        if (getPure(Attribute.Cunning) >= 28) {
            for (path in location.shortcut) {
                gui!!.addAction(Shortcut(path), this)
            }
        }
        if (getPure(Attribute.Ninjutsu) >= 5) {
            for (path in location.jump) {
                gui!!.addAction(Leap(path), this)
            }
        }
    }

    override fun move(match: Match) {
        gui!!.clearCommand()
        if (state == State.combat) {
            if (location.encounter == null || location.encounter?.battle() == false) {
                Scheduler.unpause()
            }
        }
        else if (busy > 0) {
            busy--
        }
        else if (has(Stsflag.enthralled)) {
            val master = (getStatus(Stsflag.enthralled) as Enthralled).master
            val compelled = findPath(master.location)
            gui!!.message(
                "You feel an irresistible compulsion to head to the <b>" + master.location.name + "</b>"
            )
            if (compelled != null) gui!!.addAction(compelled, this)
        }
        else if (state == State.shower || state == State.lostclothes) {
            bathe()
        }
        else if (state == State.crafting) {
            craft()
        }
        else if (state == State.searching) {
            search()
        }
        else if (state == State.resupplying) {
            resupply()
        }
        else if (state == State.webbed) {
            gui!!.message("You eventually manage to get an arm free, which you then use to extract yourself from the trap.")
            state = State.ready
        }
        else if (state == State.masturbating) {
            masturbate()
        }
        else {
            gui!!.showMap()
            gui!!.messageHead(location.description + "<p>")
            for (trap in location.env) {
                if (trap.owner === this) {
                    gui!!.message("You've set a $trap here.")
                }
            }
            detect()
            if (has(Toy.bloodhound)) {
                val targets = Scheduler.match!!.combatants.toMutableList()
                targets.remove(this)
                val target = targets.random()
                gui!!.message("You Bloodhound app tells you that ${target.name} is currently in ${target.location.name}")
            }
            track()
            if (!location.encounter(this)) {
                for (path in location.adjacent) {
                    gui!!.addAction(Move(path), this)
                }
                if (getPure(Attribute.Cunning) >= 28) {
                    for (path in location.shortcut) {
                        gui!!.addAction(Shortcut(path), this)
                    }
                }
                if (getPure(Attribute.Ninjutsu) >= 5) {
                    for (path in location.jump) {
                        gui!!.addAction(Leap(path), this)
                    }
                }
                for (act in usableActions()) {
                    gui!!.addAction(act, this)
                }
            }
        }
    }

    override fun ding() {
        xp -= 95 + (level * 5)
        level++
        attpoints += 2
        gui!!.clearText()
        gui!!.message("You've gained a Level!")
        for (att in att.keys) {
            if (att.isSpecialization) {
                mod(att, 1)
                gui!!.message("You've gained a point in $att")
            }
        }
        gui!!.message("Select which attributes to increase.")
        gui!!.message("Upcoming Skills:")
        for (att in att.keys) {
            if (att != Attribute.Perception && att != Attribute.Speed && !att.isSpecialization) {
                gui!!.message(Global.getUpcomingSkills(att, getPure(att)))
            }
        }
        gui!!.ding()
    }

    override fun income(amount: Int): Int {
        val total = (amount * Global.getValue(Flag.PlayerScaling)).roundToInt()
        return super.income(total)
    }

    override fun flee(location2: Area) {
        val adjacent = location2.adjacent
        val destination = adjacent.random()
        gui!!.message("You dash away and escape into the <b>" + destination.name + "</b>")
        travel(destination)
        location2.endEncounter()
    }

    override fun bathe() {
        status.removeAll { it.removable() }
        status.clear()
        stamina.fill()
        if (location.name === "Showers") {
            gui!!.message("You let the hot water wash away your exhaustion and soon you're back to peak condition")
        }
        if (location.name === "Pool") {
            gui!!.message("The hot water soothes and relaxes your muscles. You feel a bit exposed, skinny-dipping in such an open area. You decide it's time to get moving.")
        }
        if (state == State.lostclothes) {
            gui!!.message("Your clothes aren't where you left them. Someone must have come by and taken them.")
        }
        state = State.ready
    }

    override fun masturbate() {
        if (getPure(Attribute.Dark) >= 9) {
            gui!!.message(
                "You hurriedly stroke yourself off, eager to finish before someone catches you. Ever pragmatic, you carefully aim your ejaculation into a small container, "
                        + "which you store in your pocket. You feel much more refreshed and now you have some prime fuel for your dark powers."
            )
            gain(Component.Semen)
        } else {
            gui!!.message(
                "You hurriedly stroke yourself off, eager to finish before someone catches you. After what seems like an eternity, you ejaculate into a tissue and " +
                        "throw it in the trash. Looks like you got away with it."
            )
        }

        arousal.empty()
        state = State.ready
    }

    override fun showerScene(target: Character, encounter: Encounter) {
        if (target.location.name === "Showers") {
            gui!!.message(
                "You hear running water coming from the first floor showers. There shouldn't be any residents on this floor right now, so it's likely one " +
                        "of your opponents. You peek inside and sure enough, <b>" + target.name + "</b> is taking a shower and looking quite vulnerable. Do you take advantage " +
                        "of her carelessness?"
            )
        } else if (target.location.name === "Pool") {
            gui!!.message("You stumble upon <b>" + target.name + "</b> skinny dipping in the pool. She hasn't noticed you yet. It would be pretty easy to catch her off-guard.")
        }
        assessOpponent(target)
        gui!!.promptShower(encounter, target)
    }

    override fun intervene(enc: Encounter, p1: Character, p2: Character) {
        gui!!.message("You find <b>" + p1.name + "</b> and <b>" + p2.name + "</b> fighting too intensely to notice your arrival. If you intervene now, it'll essentially decide the winner.")
        gui!!.promptIntervene(enc, p1, p2)
    }

    override fun resist3p(combat: Combat, intruder: Character, assist: Character): Boolean {
        return has(Trait.cursed)
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character) {
        /*gainXP(20 + lvlBonus(target))
        target.defeated(this)
        assist.gainAttraction(this, 1)*/
        c.write(
            "You take your time, approaching " + target.name + " and " + assist.name + " stealthily. " + assist.name + " notices you first and before her reaction " +
                    "gives you away, you quickly lunge and grab " + target.name + " from behind. She freezes in surprise for just a second, but that's all you need to " +
                    "restrain her arms and leave her completely helpless. Both your hands are occupied holding her, so you focus on kissing and licking the " +
                    "sensitive nape of her neck.<p>"
        )
    }

    override fun victory3p(c: Combat, target: Character, assist: Character) {
//        gainXP(20 + lvlBonus(target))
//        target.gainXP(10 + target.lvlBonus(this) + target.lvlBonus(assist))
//        target.arousal.empty()
//        if (target.has(Trait.insatiable)) {
//            target.arousal.restore((arousal.max * .2).toInt())
//        }
//        dress(c)
//        target.undress(c)
//        if (c.underwearIntact(target)) {
//            gain(target.underwear!!)
//        }
//        target.defeated(this)
//        gainAttraction(target, 1)
//        target.gainAttraction(this, 1)
        if (target.hasDick) {
            c.write("You position yourself between ${target.name}'s legs, gently forcing them open with your knees. ${target.possessive(true)} dick stands erect, fully exposed and " +
                "ready for attention. You grip the needy member and start jerking it with a practiced hand. ${target.name} moans softly, but seems to be able to handle " +
                "this level of stimulation. You need to turn up the heat some more. Well, if you weren't prepared to suck a cock or two, you may have joined " +
                "the wrong competition. You take just the glans into your mouth, attacking the most sensitive area with your tongue. ${target.pronounSubject(true)} lets out a gasp and " +
                "shudders. That's a more promising reaction.<p>" +
                "You continue your oral assault until you hear a breathy moan, <i>\"I'm gonna cum!\"</i> You hastily remove ${target.possessive(false)} dick out of your mouth and " +
                "pump it rapidly. ${target.name} shoots ${target.possessive(false)} load into the air, barely missing you."
            )
        } else {
            c.write(
                target.name + "'s arms are firmly pinned, so she tries to kick you ineffectually. You catch her ankles and slowly begin kissing and licking your way " +
                        "up her legs while gently, but firmly, forcing them apart. By the time you reach her inner thighs, she's given up trying to resist. Since you no " +
                        "longer need to hold her legs, you can focus on her flooded pussy. You pump two fingers in and out of her while licking and sucking her clit. In no " +
                        "time at all, she's trembling and moaning in orgasm."
            )
        }
    }

    override fun watcher(c: Combat, victor: Character, defeated: Character) {
        if (has(Trait.voyeurism)) {
            buildMojoPercent(50.0)
        }
        Global.gui.clearText()
        victor.watched(c, this, defeated)
    }

    override fun watched(c: Combat, voyeur: Character, defeated: Character) {
    }

    override fun gain(item: Item) {
        gui!!.message("<b>You've gained ${item.prefix}${item.getName()}</b>")
        inventory.add(item)
    }

    override fun gain(item: Clothing) {
        gui!!.message("<b>You've gained ${item.prefix}${item.getName()}</b>")
        closet.add(item)
    }

    override fun load(loader: Scanner) {
        name = loader.next()
        level = loader.next().toInt()
        rank = loader.next().toInt()
        xp = loader.next().toInt()
        money = loader.next().toInt()
        var e = loader.next()
        while (e != "@") {
            set(Attribute.valueOf(e), loader.next().toInt())
            e = loader.next()
        }
        stamina.setMax(loader.next().toInt())
        arousal.setMax(loader.next().toInt())
        mojo.setMax(loader.next().toInt())
        e = loader.next()
        while (e != "*") {
            gainAffection(Roster[e], loader.next().toInt())
            e = loader.next()
        }
        e = loader.next()
        while (e != "*") {
            gainAttraction(Roster[e], loader.next().toInt())
            e = loader.next()
        }
        e = loader.next()
        outfit[0].clear()
        while (e != "!") {
            outfit[0].add(Clothing.valueOf(e))
            e = loader.next()
        }
        e = loader.next()
        outfit[1].clear()
        while (e != "!" && e != "!!") {
            outfit[1].add(Clothing.valueOf(e))
            e = loader.next()
        }
        if (e == "!!") {
            e = loader.next()
            closet.clear()
            while (e != "!!!") {
                closet.add(Clothing.valueOf(e))
                e = loader.next()
            }
        }
        e = loader.next()
        while (e != "@") {
            traits.add(Trait.valueOf(e))
            e = loader.next()
        }
        e = loader.next()
        while (e != "$") {
            inventory.add(Global.getItem(e)!!)
            e = loader.next()
        }
        change(Modifier.normal)
        resetSkills()
        Global.gainSkills(this)
        stamina.fill()
        arousal.empty()
        mojo.empty()
    }

    override fun challenge(opponent: Character): String? {
        return null
    }

    override fun promptTrap(enc: Encounter, target: Character, trap: Trap) {
        Global.gui.message("Do you want to take the opportunity to ambush <b>" + target.name + "</b>?")
        assessOpponent(target)
        gui!!.promptOpportunity(enc, target, trap)
    }

    fun listinventory(): HashMap<Item, Int> {
        val inv = ""
        val tally = HashMap<Item, Int>()
        for (i in inventory) {
            if (tally.containsKey(i)) {
                tally[i] = tally[i]!! + 1
            } else {
                tally[i] = 1
            }
        }
        return tally
    }

    override fun afterParty() {
        // TODO Auto-generated method stub
    }

    override fun counterattack(target: Character, type: Tactics, c: Combat) {
        when (type) {
            Tactics.damage -> {
                c.write("You dodge " + target.name + "'s slow attack and hit her sensitive tit to stagger her.")
                target.pain(4.0 + Global.random(get(Attribute.Cunning)), Anatomy.chest, c)
            }

            Tactics.pleasure -> {
                if (!target.isNude) {
                    c.write("You pull " + target.name + " off balance and lick her sensitive ear. She trembles as you nibble on her earlobe.")
                } else {
                    c.write("You pull " + target.name + " to you and rub your thigh against her girl parts.")
                }
                target.pleasure(4.0 + Global.random(get(Attribute.Cunning)), Anatomy.genitals, combat = c)
            }

            Tactics.positioning -> {
                c.write(target.name + " loses her balance while grappling with you. Before she can fall to the floor, you catch her from behind and hold her up.")
                c.stance = Behind(this, target)
            }

            else -> {}
        }
    }

    override fun eot(c: Combat, opponent: Character) {
        if (opponent.has(Trait.pheromones) && opponent.arousal.percent() >= 50 &&
            !has(Stsflag.horny) && Global.random(5) == 0
        ) {
            c.write("Whenever you're near ${opponent.name}, you feel your body heat up. Something in her scent is making you extremely horny.")
            add(Horny(this, 2.0 + 2 * opponent.skimpiness, 3), c)
        }
        if (opponent.has(Trait.tailmastery) && opponent.has(Trait.tailed) && !opponent.distracted() && !opponent.stunned() && isPantsless) {
            c.write("${opponent.name} opportunistically teases you with her soft tail.")
            pleasure(Global.random(5).toDouble(), Anatomy.genitals, combat = c)
        }
    }

    override fun eotMoodSwing(c: Combat, opponent: Character) {
        moodSwing()
    }

    override val portrait: String? = null

    override fun moodSwing(): Emotion {
        val highest = emotes.maxBy { it.value }
        mood = highest.key
        return mood
    }

    override fun resetOutfit() {
        outfit[0].clear()
        outfit[1].clear()
        outfit[0].add(Clothing.Tshirt)
        outfit[1].add(Clothing.boxers)
        outfit[1].add(Clothing.jeans)
    }

    override fun canUse(item: Item, combat: Combat?): Boolean {
        if (combat != null && combat.hasModifier(Modifier.noitems)) {
            if (item is Flask || item is Potion || item is Consumable)
                return false
        }
        if (combat == null && Scheduler.match?.condition == Modifier.noitems) {
            if (item is Flask || item is Potion || item is Consumable)
                return false
        }
        return super.canUse(item, combat)
    }
}

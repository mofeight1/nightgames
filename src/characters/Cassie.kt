package characters

import comments.CommentGroup
import comments.CommentSituation
import comments.SkillComment
import actions.Action
import actions.Movement
import combat.Combat
import combat.Result
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import items.Clothing
import items.Toy
import items.Trophy
import scenes.SceneFlag
import scenes.SceneManager
import skills.Blowjob
import skills.Cunnilingus
import skills.Kiss
import skills.MageArmor
import skills.ManaFortification
import skills.Skill
import skills.SkillTag
import skills.Whisper
import status.Energized
import status.Stsflag

class Cassie : Personality {
    override var character: NPC = NPC("Cassie", ID.CASSIE, 1, this)

    init {
        character.outfit[0].add(Clothing.bra)
        character.outfit[0].add(Clothing.blouse)
        character.outfit[1].add(Clothing.panties)
        character.outfit[1].add(Clothing.skirt)
        character.closet.add(Clothing.bra)
        character.closet.add(Clothing.blouse)
        character.closet.add(Clothing.panties)
        character.closet.add(Clothing.skirt)
        character.change(Modifier.normal)
        character.mod(Attribute.Power, 1)
        character.mod(Attribute.Seduction, 1)
        character.mod(Attribute.Cunning, 1)
        character.mod(Attribute.Perception, 1)
        character.mojo.gainMax(20)
        character.add(Trait.female)
        character.add(Trait.softheart)
        character.add(Trait.romantic)
        character.add(Trait.imagination)
        character.underwear = Trophy.CassieTrophy
        character.plan = Emotion.bored
        character.mood = Emotion.confident
        Global.gainSkills(character)
        character.preferredSkills.add(Kiss(character))
        character.preferredSkills.add(Blowjob(character))
        character.preferredSkills.add(Cunnilingus(character))
        character.preferredSkills.add(Whisper(character))
        character.preferredSkills.add(MageArmor(character))
        character.preferredSkills.add(ManaFortification(character))
    }

    override fun act(available: HashSet<Skill>, c: Combat): Skill {
        val mandatory = HashSet<Skill>()
        for (a in available) {
            if (character.has(Stsflag.orderedstrip)) {
                if (a.toString() === "Undress" || a.toString() === "Strip Tease") {
                    mandatory.add(a)
                }
            }
        }
        if (mandatory.isNotEmpty()) {
            return mandatory.random()
        }

        return chooseSkill(available, c)
    }

    /*override fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        val proposed = character.parseMoves(available, radar, match)
        return proposed
    }*/

    override fun rest(time: Int, day: Daytime) {
        if (character.rank >= 1) {
            if (character.money > 0) {
                day.visit("Magic Training", character, Global.random(character.money))
            }
        }
        if (!(character.has(Toy.Tickler) || character.has(Toy.Tickler2)) && character.money >= 300) {
            character.gain(Toy.Tickler)
            character.money -= 300
        }
        if (!(character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && character.money >= 300) {
            character.gain(Toy.Onahole)
            character.money -= 300
        }
        if (!(character.has(Toy.Dildo) || character.has(Toy.Dildo2)) && character.money >= 250) {
            character.gain(Toy.Dildo)
            character.money -= 250
        }
        var loc: String
        val available = ArrayList<String>()
        available.add("Hardware Store")
        available.add("Black Market")
        available.add("XXX Store")
        available.add("Bookstore")
        if (character.rank > 0) {
            available.add("Reference Room")
            available.add("Workshop")
        }
        available.add("Play Video Games")
        for (i in 0 until time - 4) {
            loc = available.random()
            day.visit(loc, character, Global.random(character.money))
        }
        if (character.getAffection(Global.player) > 0) {
            Global.modCounter(Flag.CassieDWV, 1.0)
        }
        character.visit(4)
    }

    override fun describe(): String {
        return if (character.has(Trait.witch)) {
            "Cassie has changed a lot since you started the Game. Maybe she isn't that different physically. She has the same bright blue eyes and the same sweet smile. " +
                    "The magic spellbook and cloak are both new. She's been dabbling in the arcane, and it may be your imagination, but you feel like you can perceive the power " +
                    "radiating from her. Her magic seems to have given her more confidence and she seems even more eager than usual."
        } else {
            character.name + " is a cute girl with shoulder-length auburn hair, clear blue eyes, and glasses. She doesn't look at all like the typical sex-fighter. " +
                    "She's short with modest breasts. She's not chubby, but you would describe her body as soft rather than athletic. Her gentle tone and occasional " +
                    "flickers of shyness give the impression of sexual innocence, but she seems determined to win."
        }
    }

    override fun victory(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.anal) {
                character.arousal.empty()
                Global.gui.displayImage("Cassie_pegging.jpg", "Art by AimlessArt")
                Global.modCounter(Flag.PlayerAssLosses, 1.0)
                SceneManager.play(SceneFlag.CassiePeggingVictory)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.CassieSexVictory)
            } else if (character.pet != null) {
                SceneManager.play(SceneFlag.CassieFaerieVictory)
            } else if (opponent.has(Stsflag.horny)) {
                character.arousal.empty()
                SceneManager.play(SceneFlag.CassieHornyVictory)
            } else if (character.has(Trait.witch) && character.has(Trait.silvertongue) && Global.random(2) == 0) {
                character.arousal.empty()
                SceneManager.play(SceneFlag.CassieMagicVictory)
            } else if (character.arousal.percent() > 50) {
                character.arousal.empty()
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.CassieForeplayVictoryAlt)
                } else {
                    SceneManager.play(SceneFlag.CassieForeplayVictoryBasic)
                }
            } else {
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.CassieForeplayVictoryEasyAlt)
                } else {
                    SceneManager.play(SceneFlag.CassieForeplayVictoryEasy)
                }
            }
        }
    }

    override fun defeat(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        declareGrudge(opponent, c)
        if (opponent.human()) {
            if (flag === Result.anal && c.stance.sub(character)) {
                SceneManager.play(SceneFlag.CassieAnalDefeat)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.CassieSexDefeat)
            } else if (character.has(Stsflag.bound)) {
                SceneManager.play(SceneFlag.CassieForeplayDefeatRoleplay)
            } else if (!character.isPantsless && !character.isTopless) {
                SceneManager.play(SceneFlag.CassieForeplayDefeatClothed)
            } else if (opponent.arousal.percent() <= 20) {
                SceneManager.play(SceneFlag.CassieForeplayDefeatEasy)
            } else if (character.has(Trait.witch) && Global.random(2) == 0) {
                opponent.add(Energized(opponent, 10))
                SceneManager.play(SceneFlag.CassieForeplayDefeatManaDrain)
            } else {
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.CassieForeplayDefeatAlt)
                } else {
                    SceneManager.play(SceneFlag.CassieForeplayDefeatBasic)
                }
            }
        }
    }

    override fun draw(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.intercourse) {
                if (character.has(Trait.witch) && character.getAffection(opponent) >= 12 && opponent.getPure(Attribute.Arcane) >= 4 && Global.random(
                        2
                    ) == 0
                ) {
                    SceneManager.play(SceneFlag.CassieMagicDraw)
                } else {
                    SceneManager.play(SceneFlag.CassieSexDraw)
                }
            } else {
                SceneManager.play(SceneFlag.CassieForeplayDraw)
            }
        }
    }

    override fun bbLiner(): String {
        if (character.getAffection(Global.player) >= 25) {
            return "Cassie looks apologetic and a bit flustered. <i>\"Sorry! Sorry! I love your boy parts, but they're also a really good target.\"</i>"
        }
        return when (Global.random(3)) {
            1 -> ("Cassie seems a little embarrassed for having to resort to such attacks.  "
                    + "<i>\"T-They said low blows were fair game!\"</i> she stammers.")

            2 -> "Cassie looks away, sheepishly. <i>\"I-I swear I wasn't aiming for your balls!\"</i>  Somehow you doubt that."
            else -> "Cassie winces apologetically. <i>\"That looks really painful. Sorry, but I can't afford to go easy on you.\"</i>"
        }
    }

    override fun nakedLiner(): String {
        return "Cassie blushes noticeably and covers herself. <i>\"No matter how much time I spend naked, it doesn't get any less embarrassing.\"</i>"
    }

    override fun stunLiner(): String {
        return "Cassie groans softly as she tends her bruises, <i>\"Come on, you don't have to be so rough.\"</i> she complains."
    }

    fun winningLiner(): String? {
        // TODO Auto-generated method stub
        return null
    }

    override fun taunt(): String {
        return "Cassie giggles and taps the head of your dick. <i>\"Your penis is so eager and cooperative,\"</i> she jokes. <i>\"Are you sure you're not just letting me win?\"</i>"
    }

    override fun fightFlight(opponent: Character): Boolean {
        return !character.isNude
    }

    override fun attack(opponent: Character): Boolean {
        return !character.isNude
    }

    override fun ding() {
        if (character.rank >= 4) {
            character.mod(Attribute.Arcane, 1)
            character.mod(Attribute.Contender, 1)
            var rand: Int
            for (i in 0 until (Global.random(3) / 2) + 1) {
                rand = Global.random(4)
                when (rand) {
                    0 -> {
                        character.mod(Attribute.Power, 1)
                    }
                    1 -> {
                        character.mod(Attribute.Seduction, 1)
                    }
                    2 -> {
                        character.mod(Attribute.Cunning, 1)
                    }
                    3 -> {
                        character.mod(Attribute.Arcane, 1)
                    }
                }
            }
        } else if (character.getPure(Attribute.Arcane) >= 1) {
            character.mod(Attribute.Arcane, 1)
            var rand: Int
            for (i in 0 until (Global.random(3) / 2) + 1) {
                rand = Global.random(4)
                when (rand) {
                    0 -> {
                        character.mod(Attribute.Power, 1)
                    }
                    1 -> {
                        character.mod(Attribute.Seduction, 1)
                    }
                    2 -> {
                        character.mod(Attribute.Cunning, 1)
                    }
                    3 -> {
                        character.mod(Attribute.Arcane, 1)
                    }
                }
            }
        } else {
            var rand: Int
            for (i in 0 until (Global.random(3) / 2) + 2) {
                rand = Global.random(3)
                when (rand) {
                    0 -> {
                        character.mod(Attribute.Power, 1)
                    }
                    1 -> {
                        character.mod(Attribute.Seduction, 1)
                    }
                    2 -> {
                        character.mod(Attribute.Cunning, 1)
                    }
                }
            }
        }
        character.stamina.gainMax(4)
        character.arousal.gainMax(4)
        character.mojo.gainMax(3)
    }

    override fun victory3p(c: Combat, target: Character, assist: Character): String {
        character.clearGrudge(target)
        character.clearGrudge(assist)
        return if (target.human()) {
            "Cassie positions herself between your legs, enjoying her unrestricted access to your naked body. She lightly runs her fingers along the length of your " +
                    "erection and places a kiss on the tip. <i>\"Don't worry,\"</i> she whispers happily. <i>\"I'm going to make sure you enjoy this.\"</i> She slowly begins licking and " +
                    "sucking you penis like a popsicle. You tremble helplessly as she gradually brings you closer and closer to your defeat. A low grunt is the only warning " +
                    "you can give of your approaching climax, but Cassie picks up on it. She backs off your dick just far enough to circle her tongue around the sensitive head, " +
                    "pushing you over the edge. You shoot your load over her face and glasses as she pumps your shaft with her hand."
        } else {
            if (target.hasDick) {
                String.format(
                    "Cassie kneels between %s's legs and takes hold of %s cock. She gives you a hesitant look. <i>\"This is a bit awkward.\"</i> Is she suddenly "
                            + "reluctant to pleasure a penis? You can attest that she's quite good at it.<p>"
                            + "Cassie's cheeks turn noticeably red. <i>\"Just don't get jealous.\"</i> She starts to stroke the cock, while slowly licking the glans. %s moans in "
                            + "pleasure and bucks %s hips. Cassie's technique has obviously gotten quite good. It only takes a few minutes for her to milk out a mouthful of semen. "
                            + "You can't help feeling a bit envious, maybe you should go a round with her before the match ends.",
                    target.name, target.possessive(false), target.name, target.possessive(false)
                )
            } else {
                "Cassie settles herself in front of " + target.name + " and tenderly kisses her on the lips. <i>\"I don't really swing this way, but setting the mood is " +
                        "important.\"</i> She leans in to lick and suck " + target.name + " neck, before moving down to her breasts. She gives each nipple attention until " + target.name +
                        " is panting with desire. She continues downward to " + target.name + "'s pussy and starts eating her out. " + target.name + " moans loudly and arches her back against " +
                        "you. You gently lower her to the floor as she recovers from her climax, while Cassie wipes the juices from her mouth and looks satisfied at her work."
            }
        }
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character): String {
        return if (target.human()) {
            ("You grapple with " + assist.name + ", but neither of you can find an opening. She loses her balance while trying to grab you and you manage to trip her. " +
                    "Before you can follow up, a warm body presses against your back and a soft hand gently grasps your erection. Cassie whispers playfully in your ear. <i>\"Hello "
                    + target.name + ". How about a threesome?\"</i> You start to break away from Cassie, but " + assist.name + " is already back on her feet. You struggle valiantly, " +
                    "but you're quickly overwhelmed by the two groping and grappling girls. Cassie manages to force both your arms under her, leaving you helpless.<br>")
        } else {
            "You wrestle " + target.name + " to the floor, but she slips away and gets to her feet before you. You roll away to a safe distance before you notice that " +
                    "she's not coming after you. She seems more occupied by the hands that have suddenly grabbed her breasts from behind. You cautiously approach and realize " +
                    "it's Cassie who is holding onto the flailing " + target.name + ". Releasing her boobs, Cassie starts tickling " + target.name + " into submission and pins her " +
                    "arms while she catches her breath.<br>"
        }
    }

    override fun watched(c: Combat, target: Character, viewer: Character) {
        if (viewer.human()) {
            if (character.has(Trait.witch)) {
                if (target.hasDick) {
                    SceneManager.play(SceneFlag.CassieWatchPenis, target)
                } else {
                    SceneManager.play(SceneFlag.CassieWatchMagic, target)
                }
            } else {
                SceneManager.play(SceneFlag.CassieWatch, target)
            }
        }
    }

    override fun startBattle(opponent: Character): String {
        if (character.grudge != null) {
            when (character.grudge) {
                Trait.overflowingmana -> return ("As Cassie approaches you, her magical energy is visible to the naked eye. She seems to glow and crackle with "
                        + "power. She twists the aura around her hands as she grins at you.<p>"
                        + "<i>\"Do you see this? When you made me orgasm last fight, it was like you opened the flood gates. I'm full "
                        + "of Arcane power right now, and I'm going to use all of it to thank you.\"</i>")

                Trait.spirited -> return ("Cassie gives you a bright smile, practically bouncing with eagerness. Did she just finish an energy drink?<p>"
                        + "<i>\"I don't know why, but I've been feeling really good since our last fight. I don't just mean the normal "
                        + "way you make me feel good. I feel like I could keep going all night.\"</i>")

                Trait.determined -> return ("Cassie gives you a pleasant smile, but her eyes show firm resolve. <i>\"I may have lost last time, but don't think "
                        + "I'm going to just give up. I'll keep going until I give you an orgasm.\"</i><p>"
                        + "Her determination is strong. You probably won't be able to keep her down easily.")

                Trait.modestlydressed -> return if (character.isNude) {
                    ("Cassie pouts as she tries to cover her nakedness. <i>\"Shit. After last fight, I had a plan to try to keep "
                            + "my clothes on. Instead, I'm naked before the fight even starts. It's not fair!\"</i>")
                } else {
                    ("Cassie grins and does a little spin to show off. <i>\"What do you think of my clothes?\"</i> Her outfit is cute, "
                            + "but not much different than usual. If anything, it appears to fit more snugly.<br>"
                            + "<i>\"Exactly. I paid special attention to making sure all my clothes fit perfectly, so they'll be harder to "
                            + "take off. If I can stay dressed, I should have a much better chance of winning.\"</i>")
                }

                else -> {}
            }
        }
        if (character.isNude) {
            return ("Cassie does her best to cover her naked body as her cheeks turn pink. <i>\"We haven't even started yet and I'm already naked. Go easy "
                    + "on me, OK?\"</i>")
        }
        if (opponent.isPantsless) {
            return ("Cassie giggles cutely as her eyes drift over your naked body. <i>\"Is it my birthday? I don't usually run into hot naked boys when I'm "
                    + "walking around the campus.\"</i>")
        }
        if (character.getAffection(opponent) >= 30) {
            return ("Cassie gives you a warm smile as she prepares to do sexy battle. <i>\"Fighting you is always the highlight of my night, win or lose. "
                    + "I'm still going to do my best to win though.\"</i>")
        }
        if (character.has(Trait.witch)) {
            return ("Cassie extends a hand and a soft light radiates from her palm. <i>\"Do you like my magic? I'm still learning, but I bet it can do "
                    + "some cool things.\"</i>")
        }
        return "Cassie looks hesitant for just a moment, but can't contain a curious little smile as she prepares to face you."
    }

    override fun fit(): Boolean {
        return !character.isNude && character.stamina.percent() >= 50 && character.arousal.percent() <= 50
    }

    override fun night(): Boolean {
        Global.gui.loadPortrait(Global.player, this.character)
        SceneManager.play(SceneFlag.CassieAfterMatch)
        return true
    }

    override fun advance(rank: Int) {
        if (rank >= 4 && character.getPure(Attribute.Contender) == 0) {
            character.add(Trait.protagonist)
            character.mod(Attribute.Contender, 1)
            character.outfit[Character.OUTFITTOP].clear()
            character.outfit[Character.OUTFITBOTTOM].clear()
            character.outfit[0].add(Clothing.bra)
            character.outfit[0].add(Clothing.magetop)
            character.outfit[0].add(Clothing.herorobe)
            character.outfit[1].add(Clothing.panties)
            character.outfit[1].add(Clothing.miniskirt)
            character.closet.add(Clothing.herorobe)
            character.closet.add(Clothing.magetop)
            character.closet.add(Clothing.miniskirt)
            character.clearSpriteImages()
        }
        if (rank >= 2 && !character.has(Trait.hiddenpotential)) {
            character.add(Trait.hiddenpotential)
        }
        if (rank >= 1 && !character.has(Trait.witch)) {
            character.add(Trait.witch)
            character.outfit[0].clear()
            character.outfit[1].clear()
            character.outfit[0].add(Clothing.bra)
            character.outfit[0].add(Clothing.Tshirt)
            character.outfit[0].add(Clothing.cloak)
            character.outfit[1].add(Clothing.panties)
            character.outfit[1].add(Clothing.skirt)
            character.closet.add(Clothing.cloak)
            character.clearSpriteImages()
            character.mod(Attribute.Arcane, 1)
        }
    }

    override fun checkMood(mood: Emotion, value: Int): Boolean {
        return when (mood) {
            Emotion.nervous -> value >= 30
            Emotion.angry -> value >= 80
            else -> value >= 50
        }
    }

    override fun image(): String {
        return "assets/cassie_" + character.mood.name + ".jpg"
    }

    /*override fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }*/

    override fun moodWeight(mood: Emotion): Double {
        return when (mood) {
            Emotion.nervous -> 1.2
            Emotion.angry -> .7
            else -> 1.0
        }
    }

    override fun resist3p(c: Combat, target: Character, assist: Character): String {
        // TODO Auto-generated method stub
        return ""
    }

    override val comments: CommentGroup
        get() {
        val comments = CommentGroup()
            comments[CommentSituation.VAG_DOM_CATCH_WIN] = "<i>\"Oh, yes! Give it all to me! Please!\"</i>"
            comments[CommentSituation.VAG_DOM_CATCH_LOSE] = "<i>\"Ah! I... Can't... Lose... Yet!\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_WIN] = "<i>\"Ah! Do you feel good? You don't have to hold back, just let it out!\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_LOSE] = "<i>\"Ah, ah ah! Please...\"</i>"
            comments[CommentSituation.ANAL_CATCH_WIN] = "<i>\"You are a dirty boy, aren't you? I might be a bit dirty as well for doing this...\"</i>"
            comments[CommentSituation.ANAL_CATCH_LOSE] = "<i>\"Not my butt! Don't make me cum from my butt!\"</i>"
            comments[CommentSituation.SIXTYNINE_WIN] = "<i>\"I think I really like this position. How about you?\"</i>"
            comments[CommentSituation.OTHER_ENTHRALLED] = "<i>\"Oh, I really shouldn't do this... but it's really exciting!\"</i>"
            comments[CommentSituation.OTHER_BOUND] = "<i>\"If you just sit still like that, I'll make sure you enjoy it...\"</i>"
            comments[CommentSituation.SELF_BOUND] = "<i>\"This isn't fair! I want to make you feel good too!\"</i>"
            comments[CommentSituation.OTHER_HORNY] = "<i>\"Am I turning you on? Hehe. I'm glad.\"</i>"
            comments[CommentSituation.SELF_HORNY] = "<i>\"I'm turning into such a horny girl... I want you so much!\"</i>"
            comments[CommentSituation.SELF_CHARMED] = "<i>\"You want to do something nice to me? Ok, just for a bit.\"</i>"
            comments[CommentSituation.OTHER_OILED] = "<i>\"With your dick so slippery, I bet I can make you feel great!\"</i>"
            comments[CommentSituation.MOUNT_DOM_WIN] = "<i>\"Do you like a girl on top? I know I'm having fun.\"</i>"
            comments[CommentSituation.SELF_SHAMED] = "<i>\"Don't tease me so much! I'm so embarrassed I could die!\"</i>"
            comments[CommentSituation.SELF_BUSTED] = "Cassie covers her groin and turns her face away from you, flushing with equal parts pain and embarrassment."
            comments[SkillComment(Attribute.Arcane, true)] = "<i>\"Nice spell! It's like we're in magic duel.\"</i>"
            comments[SkillComment(Attribute.Animism, true)] = "<i>\"Aww, You're like a puppy! So cute.\"</i>"
            comments[SkillComment(SkillTag.PET, true)] = "<i>\"Aww, how cute!\"</i>"
            comments[SkillComment(Result.kiss, true)] = "<i>\"Mmm, kissing isn't fair... I didn't say you should stop...\"</i>"
            comments[SkillComment(Result.oral, false)] = "<i>\"You make a cute expression when I'm sucking on your thing.\"</i>"
            comments[SkillComment(SkillTag.PET, false)] = "<i>\"Thank you for coming to help, my fae friend.\"</i>"


        return comments
    }

    override val responses: CommentGroup
        get() {
        val comments = CommentGroup()
        return comments
    }

    override val costumeSet: Int
        get() {
            return if (character.getPure(Attribute.Contender) > 0) {
                3
            }
            else if (character.has(Trait.witch)) {
                2
            } else {
                1
            }
        }

    override fun declareGrudge(opponent: Character, c: Combat) {
        if (character.grudge == Trait.spirited && character.has(Trait.witch)) {
            character.addGrudge(opponent, Trait.overflowingmana)
        } else if (c.eval(character) == Result.intercourse) {
            character.addGrudge(opponent, Trait.spirited)
        } else {
            when (Global.random(2)) {
                0 -> character.addGrudge(opponent, Trait.determined)
                1 -> character.addGrudge(opponent, Trait.modestlydressed)
                else -> {}
            }
        }
    }

    override fun resetOutfit() {
        character.outfit[0].clear()
        character.outfit[1].clear()
        if (character.getPure(Attribute.Contender) > 0) {
            character.outfit[0].add(Clothing.bra)
            character.outfit[0].add(Clothing.magetop)
            character.outfit[0].add(Clothing.herorobe)
            character.outfit[1].add(Clothing.panties)
            character.outfit[1].add(Clothing.miniskirt)
        } else if (character.has(Trait.witch)) {
            character.outfit[0].add(Clothing.bra)
            character.outfit[0].add(Clothing.Tshirt)
            character.outfit[0].add(Clothing.cloak)
            character.outfit[1].add(Clothing.panties)
            character.outfit[1].add(Clothing.skirt)
        } else {
            character.outfit[0].add(Clothing.bra)
            character.outfit[0].add(Clothing.blouse)
            character.outfit[1].add(Clothing.panties)
            character.outfit[1].add(Clothing.skirt)
        }
    }
}

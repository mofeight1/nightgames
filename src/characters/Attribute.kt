package characters

import combat.Tag

enum class Attribute(val description: String, val type: AttributeType) : Tag {
    Power(
        "Power is primarily used to control and subdue opponents. Power skills mostly reduce the target's Stamina which can stun the opponent.",
        AttributeType.Basic
    ),
    Seduction(
        "Seduction skills are mostly aimed at raising an opponent's arousal. Many of them are unusable or less effective if an opponent has clothes on.",
        AttributeType.Basic
    ),
    Cunning(
        "Cunning skills are mostly aimed at gaining an advantage over opponents, from positioning during combat, to traps outside of combat and more. Cunning contributes to strip attempts, evasion, counter attacks, and hiding.",
        AttributeType.Basic
    ),
    Perception(
        "The higher your Perception the more accurately you can view the Mood, Lust, and Stamina level of an opponent. Perception also improves your ability to evade traps and attacks. Conversely it also raises your sensitivity to touch.",
        AttributeType.Passive
    ),
    Speed(
        "Speed increases the chance you will act before your opponent. It also increases your evasion.",
        AttributeType.Passive
    ),
    Arcane(
        "Arcane represents your ability to cast traditional magic spells. Arcane spells are very versatile, but consume significant Mojo.",
        AttributeType.Advanced
    ),
    Science(
        "Science skills provide a lot of utility and consume little to no Mojo. Instead they consume Battery power, which can be recharged at the Workshop.",
        AttributeType.Advanced
    ),
    Dark(
        "Dark skills allow potent control of sexual corruption. Most Dark skills raise the user's Arousal.",
        AttributeType.Advanced
    ),
    Fetish(
        "Fetish skills involving magic manipulating sexual kinks. Most Fetish skills affect both the opponent and the user, and require the user to be somewhat aroused.",
        AttributeType.Advanced
    ),
    Animism(
        "Animism derives from a powerful feral spirit. Most Animism skills are upgrades to normal Power attacks, but it also provides a massive boost to your abilities when heavily aroused.",
        AttributeType.Advanced
    ),
    Ki(
        "Ki skills are martial arts abilities that consume your Stamina. Ki forms provide powerful buffs, but only one can be active at a time.",
        AttributeType.Advanced
    ),
    Ninjutsu(
        "Ninjutsu is an old Japanese art focused on hidden tools and trickery. Ninjutsu skills provide a lot of utility and the Bunshin abilities can do extreme damage with enough Mojo.",
        AttributeType.Advanced
    ),
    Submissive(
        "Submissive skills involve putting yourself in a disadvantageous position. Submission allows you to deal more pleasure while being fucked and turn Shame to your advantage.",
        AttributeType.Advanced
    ),
    Discipline(
        "Discipline focuses on controlling your own composure while demanding the obedience of your opponent.",
        AttributeType.Advanced
    ),
    Footballer(
        "Footballer skills focus on dealing pleasure and pain with feet and inflicting debilitating status effects on opponents.",
        AttributeType.Advanced
    ),
    Eldritch("Eldritch skills focus on grappling, and can be used even when pinned.", AttributeType.Advanced),
    Unknowable(
        "Unknowable skills are too powerful for humans, but can be countered by skilled specialists in various other attributes",
        AttributeType.Advanced
    ),
    Hypnosis(
        "Hypnosis is a potent specialization that involves mind control. Most Hypnotic skills require the target to be Charmed or Enthralled.",
        AttributeType.Specialization
    ),
    Professional(
        "The oldest profession. Most Professional skills become more effective after consecutive use.",
        AttributeType.Specialization
    ),
    Temporal(
        "Temporal is a specialization revolving around time travel. Winding up the Temporal Manipulator gives charges that are consumed by powerful skills.",
        AttributeType.Specialization
    ),
    Contender(
        "Contender is a specialization about refusing to lose. Contenders can be very hard to stun or even make orgasm.",
        AttributeType.Specialization
    ),
    Vigilantism("", AttributeType.Specialization),
    Spirituality(
        "Spirituality provides excellent defense. Spiritual focus builds up each turn, but requires a clear head.",
        AttributeType.Specialization
    ),
    ;

    val isTrainable: Boolean
        get() = type == AttributeType.Basic || type == AttributeType.Advanced
    val isSpecialization: Boolean
        get() = type == AttributeType.Specialization
}

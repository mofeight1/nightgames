package characters

import actions.Action
import actions.Leap
import actions.Move
import actions.Movement
import actions.Shortcut
import comments.CommentGroup
import comments.CommentSituation
import areas.Area
import combat.Combat
import combat.Encounter
import combat.Tag
import daytime.Daytime
import global.Constants
import global.Flag
import global.Global
import global.Match
import global.Modifier
import global.Roster
import global.Scheduler
import items.Clothing
import items.Consumable
import items.Flask
import skills.Skill
import skills.Tactics
import stance.Behind
import status.Enthralled
import status.Horny
import status.Stsflag
import trap.Trap
import utilities.EnumMap
import java.time.LocalTime
import java.util.EnumMap
import java.util.NavigableMap
import java.util.Scanner
import java.util.TreeMap
import kotlin.math.exp
import kotlin.math.min

class NPC(name: String, identity: ID, level: Int, ai: Personality) : Character(name, level) {
    private val ai: Personality
    var plan: Emotion? = null
    var goal: Area? = null
    var strategy: EnumMap<Emotion, Int>
    private val comments: CommentGroup? = null
    private val responses: CommentGroup? = null
    var preferredSkills: ArrayList<Skill>

    init {
        this.id = identity
        this.ai = ai
        att[Attribute.Power] =
            Math.round(Constants.STARTINGPOWER * Global.getValue(Flag.NPCBaseStrength)).toInt()
        att[Attribute.Cunning] =
            Math.round(Constants.STARTINGCUNNING * Global.getValue(Flag.NPCBaseStrength)).toInt()
        att[Attribute.Seduction] =
            Math.round(Constants.STARTINGSEDUCTION * Global.getValue(Flag.NPCBaseStrength)).toInt()
        att[Attribute.Perception] = Constants.STARTINGPERCEPTION
        att[Attribute.Speed] =
            Math.round(Constants.STARTINGSPEED * Global.getValue(Flag.NPCBaseStrength)).toInt()
        this.money = 0
        stamina = Meter(Math.round(Constants.STARTINGSTAMINA * Global.getValue(Flag.NPCBaseStrength)).toInt())
        stamina.fill()
        arousal = Meter(Math.round(Constants.STARTINGAROUSAL * Global.getValue(Flag.NPCBaseStrength)).toInt())
        mojo = Meter(Math.round(Constants.STARTINGMOJO * Global.getValue(Flag.NPCBaseStrength)).toInt())
        strategy = EnumMap()
        strategy[Emotion.hunting] = 3
        strategy[Emotion.bored] = 3
        strategy[Emotion.sneaking] = 3
        preferredSkills = ArrayList()
    }

    override fun describe(per: Int): String {
        var description = ai.describe()
        for (s in status) {
            if (s.describe() != null && s.describe() !== "") {
                description += "<br>" + s.describe()
            }
        }
        description = "$description<p>"
        if (tops.isEmpty() && bottoms.isEmpty()) {
            description += "She is completely naked.<br>"
        } else {
            if (tops.isEmpty()) {
                description += "She is topless and wearing"
            } else {
                description = description + "She is wearing " + tops.last().item.prefix + tops.last() + " and "
            }
            description = if (bottoms.isEmpty()) {
                description + "is naked from the waist down.<br>"
            } else {
                description + bottoms.last().item.prefix + bottoms.last() + ".<br>"
            }
        }
        description += observe(per)
        return description
    }

    private fun observe(per: Int): String {
        var visible = ""
        if (status.any { it.flags.contains(Stsflag.unreadable) }) return visible
        if (per >= 9) {
            visible = visible + "Her arousal is at " + arousal.percent() + "%.<br>"
        }
        if (per >= 8) {
            visible = visible + "Her stamina is at " + stamina.percent() + "%.<br>"
        }
        if (per in 7..8) {
            if (arousal.percent() >= 75) {
                visible += "She's dripping with arousal and breathing heavily. She's at least 3/4 of the way to orgasm.<br>"
            } else if (arousal.percent() >= 50) {
                visible += "She's showing signs of arousal. She's at least halfway to orgasm.<br>"
            } else if (arousal.percent() >= 25) {
                visible += "She's starting to look noticeably aroused, maybe a quarter of her limit.<br>"
            }
        }
        if (per in 6..7) {
            if (stamina.percent() <= 33) {
                visible += "She looks a bit unsteady on her feet.<br>"
            } else if (stamina.percent() <= 66) {
                visible += "She's starting to look tired.<br>"
            }
        }
        if (per in 3..6) {
            if (arousal.percent() >= 50) {
                visible += "She's showing clear sign of arousal. You're definitely getting to her.<br>"
            }
        }
        if (per in 4..5) {
            if (stamina.percent() <= 50) {
                visible += "She looks pretty tired.<br>"
            }
        }
        if (per >= 5) {
            visible = visible + mood.description + "<br>"
        }
        return visible
    }

    override fun victory(c: Combat, flag: Tag) {
        ai.victory(c, flag)
        if (!c.hasModifier(Modifier.practice)) {
            plan = rethink()
            /*if (has(Trait.insatiable)) {
                arousal.addPercent(20.0)
            }*/
        }
    }

    override fun defeat(c: Combat, flag: Tag) {
        ai.defeat(c, flag)
        if (!c.hasModifier(Modifier.practice)) {
            plan = rethink()
        }
    }

    override fun resist3p(combat: Combat, intruder: Character, assist: Character): Boolean {
        if (has(Trait.cursed)) {
            if (intruder.human() || assist.human()) {
                Global.gui.message(ai.resist3p(combat, intruder, assist))
            }
            return true
        } else {
            return false
        }
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character) {
        /*this.gainXP(10 + lvlBonus(target))
        target.defeated(this)
        assist.gainAttraction(this, 1)*/
        c.write(ai.intervene3p(c, target, assist))
    }

    override fun victory3p(c: Combat, target: Character, assist: Character) {
        /*this.gainXP(15 + lvlBonus(target))
        target.gainXP(15 + target.lvlBonus(this) + target.lvlBonus(assist))
        target.arousal.empty()
        dress(c)
        target.undress(c)

        if (c.underwearIntact(target)) {
            this.gain(target.underwear!!)
        }
        target.defeated(this)
        gainAttraction(target, 1)*/
        c.write(ai.victory3p(c, target, assist))
    }

    override fun watcher(c: Combat, victor: Character, defeated: Character) {
        if (has(Trait.voyeurism)) {
            buildMojoPercent(50.0)
        }
        gainAttraction(victor, 3)
    }

    override fun watched(c: Combat, voyeur: Character, defeated: Character) {
        ai.watched(c, defeated, voyeur)
    }

    override fun act(c: Combat) {
        val available = HashSet<Skill>()
        val target = if (c.p1 === this) {
            c.p2
        } else {
            c.p1
        }
        for (act in skills) {
            if (act.usable(c, target) && c.isAllowed(act, this)) {
                available.add(act)
            }
        }
        for (act in flaskskills) {
            if (act.usable(c, target) && c.isAllowed(act, this)) {
                available.add(act)
            }
        }
        c.act(this, ai.act(available, c))
    }

    fun actFast(c: Combat): Skill {
        val available = HashSet<Skill>()
        val target = if (c.p1 === this) {
            c.p2
        } else {
            c.p1
        }
        for (act in skills) {
            if (act.usable(c, target) && c.isAllowed(act, this)) {
                available.add(act)
            }
        }
        for (act in flaskskills) {
            if (act.usable(c, target) && c.isAllowed(act, this)) {
                available.add(act)
            }
        }
        return ai.act(available, c)
    }

    override fun human(): Boolean {
        return false
    }

    override fun draw(c: Combat, flag: Tag) {
        ai.draw(c, flag)
        plan = rethink()
    }

    override fun bbLiner(): String {
        return ai.bbLiner()
    }

    override fun nakedLiner(): String {
        return ai.nakedLiner()
    }

    override fun stunLiner(): String {
        return ai.stunLiner()
    }

    override fun taunt(): String {
        return ai.taunt()
    }

    override fun detect() {
        // TODO Auto-generated method stub
    }

    override fun move(match: Match) {
        if (state == State.combat) {
            if (location.encounter == null) {
                state = State.ready
                move(match)
            } else {
                location.encounter!!.battle()
            }
        } else if (busy > 0) {
            busy--
        } else if (this.has(Stsflag.enthralled)) {
            val master = (getStatus(Stsflag.enthralled) as Enthralled).master
            val compelled = findPath(master.location)
            if (compelled != null) {
                location.leaveTrace(this, compelled.execute(this))
                return
            }
        } else if (state == State.shower || state == State.lostclothes) {
            bathe()
        } else if (state == State.crafting) {
            craft()
        } else if (state == State.searching) {
            search()
        } else if (state == State.resupplying) {
            resupply()
        } else if (state == State.webbed) {
            state = State.ready
        } else if (state == State.masturbating) {
            masturbate()
        } else {
            if (!location.encounter(this)) {
                val available = HashSet<Action>()
                val radar = HashSet<Movement>()
                for (path in location.adjacent) {
                    available.add(Move(path))
                    if (path.ping(this)) {
                        radar.add(path.areaId)
                    }
                }
                if (getPure(Attribute.Cunning) >= 28) {
                    for (path in location.shortcut) {
                        available.add(Shortcut(path))
                    }
                }
                if (getPure(Attribute.Ninjutsu) >= 5) {
                    for (path in location.jump) {
                        available.add(Leap(path))
                    }
                }
                available.addAll(usableActions())
                if (location.humanPresent()) {
                    Global.gui.message(
                        "You notice $name" +
                                parseMoves(available, radar, match).execute(this).description
                    )
                } else {
                    val act = parseMoves(available, radar, match)
                    location.leaveTrace(this, act.execute(this))
                }
            }
        }
    }

    override fun faceOff(opponent: Character, enc: Encounter) {
        if (has(Consumable.smoke) && !ai.fightFlight(opponent)) {
            consume(Consumable.smoke, 1)
            enc.fightOrFlight(this, ai.fightFlight(opponent), true)
        } else {
            enc.fightOrFlight(this, ai.fightFlight(opponent), false)
        }
    }

    override fun spy(opponent: Character, enc: Encounter) {
        if (ai.attack(opponent)) {
            enc.ambush(this, opponent)
        } else {
            location.endEncounter()
        }
    }

    override fun ding() {
        xp -= 95 + (level * 5)
        if (xp < 0) {
            xp = 0
        }
        level++
        ai.ding()
        if (countFeats() < level / 4) {
            ai.pickFeat()
        }
        if (has(Trait.expertGoogler)) {
            arousal.gainMax(1)
        }
        if (has(Trait.fitnessNut)) {
            stamina.gainMax(1)
        }
        Global.gainSkills(this)
    }

    override fun income(amount: Int): Int {
        val total = Math.round(amount * Global.getValue(Flag.NPCScaling))
        return super.income(total.toInt())
    }

    override fun rankup() {
        rank++
        ai.advance(rank)
    }

    override fun showerScene(target: Character, encounter: Encounter) {
        if (this.has(Flask.Aphrodisiac)) {
            encounter.aphrodisiactrick(this, target)
        } else if (!target.isNude && Global.random(3) >= 2) {
            encounter.steal(this, target)
        } else {
            encounter.showerambush(this, target)
        }
    }

    override fun intervene(enc: Encounter, p1: Character, p2: Character) {
        if (Global.random(5) == 0) {
            enc.watch(this)
        }
        if (Global.random(20) + getAffection(p1) + (if (p1.has(Trait.sympathetic)) 10 else 0) + (if (p1.has(Trait.mostlyharmless)) 10 else 0) >= Global.random(
                20
            ) + getAffection(p2) + (if (p2.has(Trait.sympathetic)) 10 else 0) + (if (p2.has(Trait.mostlyharmless)) 10 else 0)
        ) {
            enc.intrude(this, p1)
        } else {
            enc.intrude(this, p2)
        }
    }

    override fun load(loader: Scanner) {
        level = loader.next().toInt()
        rank = loader.next().toInt()
        xp = loader.next().toInt()
        money = loader.next().toInt()
        var e = loader.next()
        while (e != "@") {
            set(Attribute.valueOf(e), loader.next().toInt())
            e = loader.next()
        }
        stamina.setMax(loader.next().toInt())
        arousal.setMax(loader.next().toInt())
        mojo.setMax(loader.next().toInt())
        e = loader.next()
        while (e != "*") {
            Roster.gainAffection(id, ID.fromString(e), loader.next().toInt())
            e = loader.next()
        }
        e = loader.next()
        while (e != "*") {
            Roster.gainAttraction(id, ID.fromString(e), loader.next().toInt())
            e = loader.next()
        }
        e = loader.next()
        outfit[0].clear()
        while (e != "!") {
            outfit[0].add(Clothing.valueOf(e))
            e = loader.next()
        }
        e = loader.next()
        outfit[1].clear()
        while (e != "!" && e != "!!") {
            outfit[1].add(Clothing.valueOf(e))
            e = loader.next()
        }
        if (e == "!!") {
            e = loader.next()
            closet.clear()
            while (e != "!!!") {
                closet.add(Clothing.valueOf(e))
                e = loader.next()
            }
        }
        e = loader.next()
        while (e != "@") {
            traits.add(Trait.valueOf(e))
            e = loader.next()
        }
        e = loader.next()
        while (e != "$") {
            inventory.add(Global.getItem(e)!!)
            e = loader.next()
        }
        change(Modifier.normal)
        resetSkills()
        Global.gainSkills(this)
    }

    override fun challenge(opponent: Character): String {
        if (grudges.containsKey(opponent)) {
            grudge = grudges[opponent]
        }
        return ai.startBattle(opponent)
    }

    override fun promptTrap(enc: Encounter, target: Character, trap: Trap) {
        if (ai.attack(target)) {
            enc.trap(this, target, trap)
        } else {
            location.endEncounter()
        }
    }

    override fun afterParty() {
        ai.night()
    }

    fun daytime(time: Int, day: Daytime) {
        ai.rest(time, day)
    }

    override fun counterattack(target: Character, type: Tactics, c: Combat) {
        when (type) {
            Tactics.damage -> {
                c.write("$name avoids your clumsy attack and swings her fist into your nuts.")
                target.pain(4.0 + Global.random(get(Attribute.Cunning)), Anatomy.genitals, c)
            }

            Tactics.pleasure -> {
                if (target.isPantsless) {
                    c.write("$name catches you by the penis and rubs your sensitive glans.")
                } else {
                    c.write(name + " catches you as you approach and grinds her knee into the tent in your " + target.bottoms.last())
                }
                target.pleasure(4.0 + Global.random(get(Attribute.Cunning)), Anatomy.genitals, combat = c)
            }

            Tactics.positioning -> {
                c.write("$name outmaneuvers you and catches you from behind when you stumble.")
                c.stance = Behind(this, target)
            }

            else -> {}
        }
    }

    fun prioritize(plist: ArrayList<HashSet<Skill>>): Skill? {
        if (plist.isEmpty()) {
            return null
        }
        val wlist = ArrayList<HashSet<Skill>>()
        for (i in plist.indices) {
            if (plist[i].isNotEmpty()) {
                for (j in 0 until plist.size - i) {
                    wlist.add(plist[i])
                }
            }
        }
        val cat = wlist.randomOrNull()
        return cat?.randomOrNull()
    }

    fun parseSkills(available: HashSet<Skill>, c: Combat): ArrayList<HashSet<Skill>> {
        val target = c.getOther(this)
        val damage = HashSet<Skill>()
        val pleasure = HashSet<Skill>()
        val fucking = HashSet<Skill>()
        val position = HashSet<Skill>()
        val debuff = HashSet<Skill>()
        val recovery = HashSet<Skill>()
        val calming = HashSet<Skill>()
        val summoning = HashSet<Skill>()
        val stripping = HashSet<Skill>()
        val preferred = HashSet<Skill>()
        val priority = ArrayList<HashSet<Skill>>()
        for (a in available) {
            if (preferredSkills.contains(a)) {
                preferred.add(a)
            }
            if (a.type() == Tactics.damage) {
                damage.add(a)
            } else if (a.type() == Tactics.pleasure) {
                pleasure.add(a)
            } else if (a.type() == Tactics.fucking) {
                fucking.add(a)
            } else if (a.type() == Tactics.positioning) {
                position.add(a)
            } else if (a.type() == Tactics.status) {
                debuff.add(a)
            } else if (a.type() == Tactics.recovery) {
                recovery.add(a)
            } else if (a.type() == Tactics.calming) {
                calming.add(a)
            } else if (a.type() == Tactics.summoning || a.type() == Tactics.preparation) {
                summoning.add(a)
            } else if (a.type() == Tactics.stripping) {
                stripping.add(a)
            }
        }
        when (this.mood) {
            Emotion.confident -> {
                priority.add(pleasure)
                priority.add(fucking)
                priority.add(preferred)
                priority.add(position)
                priority.add(stripping)
                priority.add(summoning)
                if (!target.stunned()) {
                    priority.add(damage)
                }
                priority.add(debuff)
            }

            Emotion.angry -> {
                if (!target.has(Stsflag.protection)) {
                    priority.add(damage)
                }
                priority.add(preferred)
                priority.add(debuff)
                priority.add(position)
                priority.add(stripping)
                priority.add(fucking)
            }

            Emotion.nervous -> {
                priority.add(position)
                priority.add(calming)
                priority.add(summoning)
                priority.add(recovery)
                priority.add(pleasure)
                if (!target.has(Stsflag.protection)) {
                    priority.add(damage)
                }
            }

            Emotion.desperate -> {
                priority.add(calming)
                priority.add(recovery)
                priority.add(position)
                if (!target.has(Stsflag.protection)) {
                    priority.add(damage)
                }
                priority.add(pleasure)
            }

            Emotion.horny -> {
                priority.add(fucking)
                priority.add(stripping)
                priority.add(pleasure)
                priority.add(preferred)
                priority.add(position)
            }

            Emotion.dominant -> {
                priority.add(fucking)
                priority.add(stripping)
                priority.add(pleasure)
                priority.add(preferred)
                priority.add(debuff)
                priority.add(summoning)
                priority.add(position)
                if (!target.has(Stsflag.protection)) {
                    priority.add(damage)
                }
            }

            else -> {}
        }
        return priority
    }

    fun parseMoves(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        val enemy = HashSet<Action>()
        val safe = HashSet<Action>()
        val highpri = HashSet<Action>()
        val lowpri = HashSet<Action>()
        if (location === goal) {
            plan = rethink()
        }
        if (isNude) {
            for (act in available) {
                if (act.consider() == Movement.resupply) {
                    return act
                }
                if (goal == null) {
                    if (act is Move) {
                        if (act.consider() == Movement.union && !radar.contains(Movement.union)
                            || act.consider() == Movement.dorm && !radar.contains(Movement.dorm)
                        ) {
                            goal = act.destination
                        }
                    }
                }
            }
            if (goal == null) {
                goal = if (Global.random(2) == 1) {
                    match.gps(Movement.dorm)
                } else {
                    match.gps(Movement.union)
                }
            }
        }
        if (arousal.percent() >= 40 && !location.humanPresent() && radar.isEmpty()) {
            for (act in available) {
                if (act.consider() == Movement.masturbate) {
                    return act
                }
            }
        }
        if (stamina.percent() <= 60 || arousal.percent() >= 30) {
            for (act in available) {
                if (act.consider() == Movement.bathe) {
                    return act
                }
                if (goal == null) {
                    if (act is Move) {
                        if (act.consider() == Movement.pool || act.consider() == Movement.shower) {
                            goal = act.destination
                        }
                    }
                }
            }
            if (goal == null) {
                goal = if (Global.random(2) == 1) {
                    match.gps(Movement.shower)
                } else {
                    match.gps(Movement.pool)
                }
            }
        }
        if (get(Attribute.Science) >= 1 && get(Pool.BATTERY).current < 10) {
            for (act in available) {
                if (act.consider() == Movement.recharge) {
                    return act
                }
            }
            if (goal == null) {
                goal = match.gps(Movement.workshop)
            }
        }
        if (goal == null) {
            when(plan) {
                Emotion.hunting -> {
                    goal = when (Global.random(5)) {
                        4 -> match.gps(Movement.shower)
                        3 -> match.gps(Movement.union)
                        2 -> match.gps(Movement.la)
                        1 -> match.gps(Movement.dining)
                        else -> match.gps(Movement.workshop)
                    }
                }
                Emotion.sneaking -> {
                    goal = when (Global.random(5)) {
                        4 -> match.gps(Movement.storage)
                        3 -> match.gps(Movement.bridge)
                        2 -> match.gps(Movement.tunnel)
                        1 -> match.gps(Movement.workshop)
                        else -> match.gps(Movement.lab)
                    }
                }
                Emotion.bored -> {
                    goal = when (Global.random(5)) {
                        4 -> match.gps(Movement.storage)
                        3 -> match.gps(Movement.kitchen)
                        2 -> match.gps(Movement.shower)
                        1 -> match.gps(Movement.workshop)
                        else -> match.gps(Movement.lab)
                    }
                }
                Emotion.retreating -> {
                    goal = if (Global.random(2) == 1) {
                        match.gps(Movement.dorm)
                    } else {
                        match.gps(Movement.union)
                    }
                }
                else -> {}
            }
        }
        val priorityMove = findPath(goal)
        if (priorityMove != null)
            highpri.add(priorityMove)
        for (act in available) {
            if (radar.contains(act.consider())) {
                enemy.add(act)
            } else if (act.consider() == Movement.quad || act.consider() == Movement.kitchen || act.consider() == Movement.dorm || act.consider() == Movement.shower || act.consider() == Movement.storage || act.consider() == Movement.dining || act.consider() == Movement.laundry || act.consider() == Movement.tunnel || act.consider() == Movement.bridge || act.consider() == Movement.engineering || act.consider() == Movement.workshop || act.consider() == Movement.lab || act.consider() == Movement.la || act.consider() == Movement.library || act.consider() == Movement.pool || act.consider() == Movement.union) {
                safe.add(act)
            } else if (plan == Emotion.hunting) {
                if (act.consider() == Movement.mana || act.consider() == Movement.recharge || act.consider() == Movement.locating || act.consider() == Movement.potion || act.consider() == Movement.trap || act.consider() == Movement.energydrink) {
                    lowpri.add(act)
                }
            } else if (plan == Emotion.sneaking) {
                if (location.hasTrap(this)) {
                    highpri.clear()
                    if (act.consider() == Movement.hide || act.consider() == Movement.wait) {
                        highpri.add(act)
                    }
                } else {
                    if (act.consider() == Movement.hide || act.consider() == Movement.scavenge || act.consider() == Movement.trap || act.consider() == Movement.recharge) {
                        highpri.add(act)
                    } else if (act.consider() == Movement.mana || act.consider() == Movement.potion || act.consider() == Movement.bathe || act.consider() == Movement.energydrink) {
                        lowpri.add(act)
                    }
                }
            } else if (plan == Emotion.bored) {
                if (act.consider() == Movement.mana || act.consider() == Movement.recharge || act.consider() == Movement.scavenge || act.consider() == Movement.craft) {
                    highpri.add(act)
                } else if (act.consider() == Movement.trap || act.consider() == Movement.potion || act.consider() == Movement.bathe || act.consider() == Movement.energydrink || act.consider() == Movement.beer || act.consider() == Movement.oil) {
                    lowpri.add(act)
                }
            } else if (plan == Emotion.retreating) {
                if (act.consider() == Movement.energydrink) {
                    highpri.add(act)
                }
            }
        }
        if ((plan == Emotion.hunting || plan == Emotion.bored) && enemy.isNotEmpty()) {
            highpri.addAll(enemy)
        } else if (plan == Emotion.retreating) {
            highpri.removeAll(enemy)
            lowpri.addAll(safe)
        } else if (plan == Emotion.sneaking && enemy.isNotEmpty()) {
            lowpri.addAll(enemy)
        }

        if (highpri.isEmpty() || Global.random(4) == 0) {
            highpri.addAll(lowpri)
        }
        if (highpri.isEmpty()) {
            highpri.addAll(available)
        }
        return highpri.random()
    }

    override fun moodSwing(): Emotion {
        val highest = emotes.maxBy { it.value * ai.moodWeight(it.key) }
        mood = highest.key
        return mood
    }

    override fun resetOutfit() {
        ai.resetOutfit()
    }

    fun rethink(): Emotion? {
        goal = null
        var current = plan
        if (!ai.fit()) {
            return Emotion.retreating
        }
        var total = 5
        total += strategy[Emotion.hunting]!!
        total += strategy[Emotion.sneaking]!!
        total += strategy[Emotion.bored]!!
        val result = Global.random(total)
        if (result >= 5 + strategy[Emotion.hunting]!! + strategy[Emotion.bored]!!) {
            current = Emotion.bored
        } else if (result >= 5 + strategy[Emotion.hunting]!!) {
            current = Emotion.sneaking
        } else if (result >= 5) {
            current = Emotion.hunting
        }
        return current
    }

    override fun eot(c: Combat, opponent: Character) {
        if (opponent.has(Trait.pheromones) && opponent.arousal.percent() >= 50 &&
            !has(Stsflag.horny) && Global.random(5) == 0
        ) {
            c.write("You see $name swoon slightly as she gets close to you. Seems like she's starting to feel the effects of your musk.")
            add(Horny(this, 2.0 + 2 * opponent.skimpiness, 3), c)
        }
        if (opponent.has(Trait.tailmastery) && opponent.has(Trait.tailed) && !opponent.distracted() && !opponent.stunned() && isPantsless) {
            c.write("You tease $name's sensitive ares with your fluffy tail.")
            pleasure(Global.random(5).toDouble(), Anatomy.genitals, combat = c)
        }
    }

    override fun eotMoodSwing(c: Combat, opponent: Character) {
        if (c.stance.dom(this)) {
            emote(Emotion.dominant, 30)
            emote(Emotion.confident, 10)
        } else if (c.stance.sub(this)) {
            emote(Emotion.nervous, 30)
            emote(Emotion.desperate, 20)
        }
        if (c.stance.penetration(this) || c.stance.penetration(opponent)) {
            emote(Emotion.horny, 20)
        }
        if (opponent.isNude) {
            emote(Emotion.horny, 5)
            emote(Emotion.confident, 10)
            emote(Emotion.dominant, 10)
        }
        if (isNude) {
            emote(Emotion.nervous, 20)
            emote(Emotion.desperate, 10)
            if (has(Trait.exhibitionist)) {
                emote(Emotion.horny, 20)
            }
        }
        if (opponent.arousal.percent() >= 75) {
            emote(Emotion.dominant, 15)
            emote(Emotion.confident, 5)
        }
        if (arousal.percent() >= 75) {
            emote(Emotion.desperate, 15)
            emote(Emotion.nervous, 10)
        }
        if (arousal.percent() >= 90) {
            emote(Emotion.desperate, 25)
        }
        if (!canAct()) {
            emote(Emotion.desperate, 20)
        }
        if (!opponent.canAct()) {
            emote(Emotion.dominant, 30)
        }
        moodSwing()
    }

    fun visit(time: Int) {
        var visits = time
        var bff = Scheduler.getAvailable(LocalTime.of(3, 0))
            .filterNot { it.human() }
            .maxByOrNull { getAttraction(it) }
        if (bff != null) {
            bff.gainAffection(this, 1)
            when (Global.random(3)) {
                0 -> {
                    Daytime.train(this, bff, Attribute.Power)
                    Daytime.train(this, bff, Attribute.Cunning)
                    Daytime.train(this, bff, Attribute.Seduction)
                }

                1 -> {
                    Daytime.train(this, bff, Attribute.Cunning)
                    Daytime.train(this, bff, Attribute.Seduction)
                }

                else -> Daytime.train(this, bff, Attribute.Seduction)
            }
            visits -= 1
        }
        while (visits > 0) {
            bff = randomFriend
            if (bff != null) {
                bff.gainAffection(this, 1)
                when (Global.random(3)) {
                    0 -> {
                        Daytime.train(this, bff, Attribute.Power)
                        Daytime.train(this, bff, Attribute.Cunning)
                        Daytime.train(this, bff, Attribute.Seduction)
                    }

                    1 -> {
                        Daytime.train(this, bff, Attribute.Cunning)
                        Daytime.train(this, bff, Attribute.Seduction)
                    }

                    else -> Daytime.train(this, bff, Attribute.Seduction)
                }
            }
            visits -= 1
        }
    }

    @Throws(CloneNotSupportedException::class)
    override fun clone(): NPC {
        return super.clone() as NPC
    }

    private fun rateMove(skill: Skill, c: Combat, urg: Double, fit1: Double, fit2: Double): Double {
        // Clone ourselves a new combat... This should clone our characters, too
        val c2 = c.clone()
        // Now do it!
        if (c.p1 === this) {
            skill.user = c2.p1
            skill.resolve(c2, c2.p2)
            skill.user = c.p1
        } else {
            skill.user = c2.p2
            skill.resolve(c2, c2.p1)
            skill.user = c.p2
        }
        // How close is the fight to finishing?
        val urgency = min(c2.p1.urgency, c2.p2.urgency)
        val dfit1 = c2.p1.getFitness(urgency, c2.stance) - fit1
        val dfit2 = c2.p2.getFitness(urgency, c2.stance) - fit2
        return (if (c.p1 === this) dfit1 - dfit2 else dfit2 - dfit1)
    }

    fun prioritizeNew(plist: ArrayList<HashSet<Skill>>, c: Combat): Skill? {
        if (plist.isEmpty()) {
            return null
        }

        // Starting fitness
        val urgency0 = min(c.p1.urgency, c.p2.urgency)
        val fit1 = c.p1.getFitness(urgency0, c.stance)
        val fit2 = c.p2.getFitness(urgency0, c.stance)
        // Now simulate the result of all actions
        val ratingMap: NavigableMap<Double, Skill> = TreeMap()
        var sum = 0.0
        var weight = 0.0
        for (skills in plist) {
            for (skill in skills) {
                // Run it a couple of times
                var rating = 0.0
                for (j in 0 until RUN_COUNT) {
                    rating += rateMove(skill, c, urgency0, fit1, fit2)
                }
                // Sum up rating, add to map
                rating = exp(RATING_FACTOR * rating - weight)
                sum += rating
                ratingMap[sum] = skill
            }
            if (skills.isNotEmpty()) weight += INPUT_WEIGHT
        }
        c.clearImages()
        if (sum == 0.0) return null
        // Debug
        if (Global.debug) {
            var s = "AI choices: "
            var last = 0.0
            for ((key, value) in ratingMap) {
                val d = key - last
                last = key
                s += value.toString() +
                        "(" + String.format("%.1f", d / sum * 100) + "%) "
            }
            println(s)
        }
        // Select
        val s = (Global.random((sum * 1024).toInt()).toDouble()) / 1024
        return ratingMap.ceilingEntry(s).value
    }

    override val portrait: String get() = ai.image()

    override val costumeSet: Int get() = ai.costumeSet

    fun getComment(c: Combat): String? {
        return CommentSituation.getBestComment(ai.comments, c, this, c.getOther(this))
    }

    fun getResponse(c: Combat): String? {
        return CommentSituation.getBestComment(ai.responses, c, this, c.getOther(this))
    }

    companion object {
        // The higher, the better the AI will plan for "rare" events better
        const val RUN_COUNT = 5
        // Decrease to get an "easier" AI. Make negative to get a suicidal AI.
        const val RATING_FACTOR = 0.1
        // Increase to take input weights more into consideration
        const val INPUT_WEIGHT = 1.0
    }
}

package characters

class Relationship {
    private var person1: ID
    private var person2: ID
    var value: Int
    var affection = 0
    var attraction = 0


    constructor(p1: ID, p2: ID) {
        person1 = p1
        person2 = p2
        value = 0
    }

    constructor(p1: ID, p2: ID, value: Int) {
        person1 = p1
        person2 = p2
        this.value = value
    }

    fun get(): Int {
        return value
    }

    fun set(value: Int) {
        this.value = value
    }

    fun add(value: Int) {
        this.value += value
    }

    fun contains(person: ID): Boolean {
        return person1 == person || person2 == person
    }

    fun contains(p1: ID, p2: ID): Boolean {
        return contains(p1) && contains(p2)
    }

    fun getPartner(character: ID): ID? {
        if (person1 == character) {
            return person2
        }
        if (person2 == character) {
            return person1
        }
        return null
    }

    override fun equals(other: Any?): Boolean {
        if (other is Relationship) {
            return other.contains(person1, person2)
        }
        return false
    }
}

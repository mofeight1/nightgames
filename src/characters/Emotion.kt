package characters

enum class Emotion(val description: String) {
    confident("She looks fairly confident and eager to fight."),
    angry("She looks pissed."),
    nervous("She seems a little unsure of herself."),
    desperate("She knows she's losing and has started to become desperate."),
    horny("She seems much more interested in fucking than fighting."),
    dominant("She has you right where she wants you. She's just playing with you now."),
    hunting(""),
    retreating(""),
    sneaking(""),
    bored(""),
    ;

    fun inverse(): Emotion {
        return when (this) {
            angry -> horny
            nervous -> confident
            desperate -> dominant
            horny -> angry
            dominant -> desperate
            else -> nervous
        }
    }
}

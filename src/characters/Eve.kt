package characters

import comments.CommentGroup
import comments.CommentSituation
import comments.SkillComment
import actions.Action
import actions.Movement
import combat.Combat
import combat.Result
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import items.Clothing
import items.Toy
import items.Trophy
import scenes.SceneFlag
import scenes.SceneManager
import skills.AssFuck
import skills.FaceFuck
import skills.Frottage
import skills.Fuck
import skills.Masochism
import skills.Skill
import skills.SkillTag
import skills.Squeeze
import skills.Turnover
import status.Stsflag

class Eve : Personality {
    override var character: NPC = NPC("Eve", ID.EVE, 12, this)

    init {
        character.outfit[0].add(Clothing.tanktop)
        character.outfit[1].add(Clothing.crotchlesspanties)
        character.outfit[1].add(Clothing.cutoffs)
        character.closet.add(Clothing.tanktop)
        character.closet.add(Clothing.crotchlesspanties)
        character.closet.add(Clothing.cutoffs)
        character.change(Modifier.normal)
        character.mod(Attribute.Power, 8)
        character.mod(Attribute.Fetish, 14)
        character.mod(Attribute.Cunning, 7)
        character.mod(Attribute.Speed, 1)
        character.mod(Attribute.Seduction, 8)
        character.stamina.gainMax(50)
        character.arousal.gainMax(80)
        character.mojo.gainMax(30)
        Global.gainSkills(character)
        character.add(Trait.herm)
        character.add(Trait.exhibitionist)
        character.add(Trait.insatiable)
        character.add(Trait.assmaster)
        character.underwear = Trophy.EveTrophy
        character.plan = Emotion.bored
        character.mood = Emotion.confident
        character.strategy[Emotion.sneaking] = 2
        character.strategy[Emotion.bored] = 4
        character.preferredSkills.add(Fuck(character))
        character.preferredSkills.add(AssFuck(character))
        character.preferredSkills.add(Turnover(character))
        character.preferredSkills.add(Frottage(character))
        character.preferredSkills.add(FaceFuck(character))
        character.preferredSkills.add(Masochism(character))
        character.preferredSkills.add(Squeeze(character))
    }

    override fun act(available: HashSet<Skill>, c: Combat): Skill {
        val mandatory = HashSet<Skill>()
        for (a in available) {
            if (a.toString() === "Command" || a.toString().equals("Ass Fuck", ignoreCase = true)) {
                mandatory.add(a)
            }
            if (character.has(Stsflag.orderedstrip)) {
                if (a.toString() === "Undress" || a.toString() === "Strip Tease") {
                    mandatory.add(a)
                }
            }
        }
        if (mandatory.isNotEmpty()) {
            return mandatory.random()
        }

        return chooseSkill(available, c)
    }

    /*override fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        val proposed = character.parseMoves(available, radar, match)
        return proposed
    }*/

    override fun rest(time: Int, day: Daytime) {
        if (!(character.has(Toy.Dildo) || character.has(Toy.Dildo2)) && character.money >= 250) {
            character.gain(Toy.Dildo)
            character.money -= 250
        }
        if (!(character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && character.money >= 300) {
            character.gain(Toy.Onahole)
            character.money -= 300
        }
        var loc: String
        val available = ArrayList<String>()
        available.add("Hardware Store")
        available.add("Black Market")
        available.add("XXX Store")
        available.add("Bookstore")
        available.add("Workshop")
        available.add("Play Video Games")
        for (i in 0 until time - 3) {
            loc = available.random()
            day.visit(loc, character, Global.random(character.money))
        }
        character.visit(3)
    }

    override fun bbLiner(): String {
        return when (Global.random(2)) {
            1 -> "<i>\"I never really appreciated how painful nut-shots were, until I got a pair of my own.\"</i> Eve says, carefully adjusting her balls. <i>\"It almost gives me sympathy pains.  Almost.\"</i>"
            else -> "Eve grins at you and pats her own groin. <i>\"Better you than me, boy.\"</i>"
        }
    }

    override fun nakedLiner(): String {
        return "Eve seems more comfortable with her cock and balls hanging out than she was with her clothes on. <i>\"Like what you see? We're just getting started.\"</i>"
    }

    override fun stunLiner(): String {
        return "Eve lets out a soft growl as she lays flat on the floor. <i>\"Enjoy it while you can, boy. As soon as I catch my breath, your ass is mine.\"</i>"
    }

    override fun taunt(): String {
        return "Eve grins sadistically. <i>\"If you're intimidated by my cock, don't worry. Size isn't everything.\"</i>"
    }

    override fun victory(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.arousal.empty()
        if (opponent.human()) {
            if (flag === Result.anal) {
                Global.modCounter(Flag.PlayerAssLosses, 1.0)
                SceneManager.play(SceneFlag.EvePeggingVictory)
            } else if (Global.random(2) == 0) {
                SceneManager.play(SceneFlag.EveForeplayVictoryAlt)
            } else {
                SceneManager.play(SceneFlag.EveForeplayVictory)
            }
        }
    }

    override fun defeat(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        declareGrudge(opponent, c)
        if (opponent.human()) {
            if (flag === Result.anal && c.stance.sub(character)) {
                SceneManager.play(SceneFlag.EveAnalDefeat)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.EveSexDefeat)
            } else {
                SceneManager.play(SceneFlag.EveForeplayDefeat)
            }
        }
    }

    override fun describe(): String {
        return "If there's one word to describe Eve's appearance, it would have to be 'wild'. Her face is quite pretty, though her eyes are an unnerving silver color. " +
                "She has bright purple hair gathered in a messy ponytail, a variety of tattoos decorating her extremely shapely body, and of course it's " +
                "impossible to miss the larger than average cock and balls hanging between between her legs."
    }

    override fun draw(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.EveSexDraw)
            } else {
                SceneManager.play(SceneFlag.EveForeplayDraw)
            }
        }
    }

    override fun fightFlight(opponent: Character): Boolean {
        return !character.isNude || opponent.isNude
    }

    override fun attack(opponent: Character): Boolean {
        return true
    }

    override fun ding() {
        character.mod(Attribute.Fetish, 1)
        var rand: Int
        for (i in 0 until (Global.random(3) / 2) + 1) {
            rand = Global.random(4)
            when (rand) {
                0 -> {
                    character.mod(Attribute.Power, 1)
                }
                1 -> {
                    character.mod(Attribute.Seduction, 1)
                }
                2 -> {
                    character.mod(Attribute.Cunning, 1)
                }
                else -> {
                    character.mod(Attribute.Fetish, 1)
                }
            }
        }
        character.stamina.gainMax(3)
        character.arousal.gainMax(6)
        character.mojo.gainMax(1)
    }

    override fun victory3p(c: Combat, target: Character, assist: Character): String {
        character.clearGrudge(target)
        character.clearGrudge(assist)
        if (target.human()) {
            return "Eve lifts up your legs, putting you in an extremely undignified position. She pulls open your ass cheeks and starts to probe at your puckered hole. Oh Fuck. " +
                    "You'd pretty much given up winning this one, but there are many better ways to lose than getting pegged in the ass. You struggle as best you can, but you're " +
                    "well aware it's completely futile. Pretty quickly, she gives you a slap on the ass and leaves your anus alone. <i>\"Bad angle for insertion, lucky for you " +
                    "boy. You're going to have to find another way to entertain me.\"</i> It seems a little unfair that she's putting the responsibility of entertainment on you, " +
                    "when you're almost completely incapacitated. She doesn't actually seem to be waiting for you though. She straddles your hips and grinds her cock against yours. " +
                    "<i>\"You were just freaking out that I was going to plug you in the ass, but you got hard again already. Such an eager little penis.\"</i> She continues to " +
                    "hump your rod as you squirm helplessly. If your cocks are dueling, hers definitely has the advantage. She also uses her hands to tease your balls and shaft, " +
                    "eroding your willpower. When you cum, she immediately uses your jizz as lubricant to frot against you more aggressively. Your overstimulated dick becomes " +
                    "uncomfortably sensitive, but she doesn't let up until she ejaculates onto you."
        } else {
            if (assist.eligible(this.character)) {
                assist.defeated(this.character)
                assist.nudify()
            }
            return "Eve kneels between " + target.name + "'s legs and plays with the helpless girl's slit. <i>\"Nice and wet. Good.\"</i> She flicks a finger across " + target.name + "'s " +
                    "clit and you feel her body jerk at the sensation. <i>\"Sensitive too, and all mine? I don't mind if I do.\"</i> She lines up her impressive " +
                    "member with the other girl's dripping entrance and penetrates her with one firm thrust. Both girls moan in pleasure and you feel your boner stand at attention in " +
                    "response to the enticing scene. Eve grins at you confidently. <i>\"Jealous? I'm sure you wish your cock was buried deep in this tight, wet cunt, but I got " +
                    "here first.\"</i> She thrusts her hips several times for emphasis. <i>\"You could always go for the back door.\"</i> " + target.name + " frantically shakes her " +
                    "head in protest, though she's moaning too much to speak. <i>\"Aww... She doesn't want to be double penetrated. You'll just have to settle for this.</i>\" <br>" +
                    "Without slowing down her thrusts, she pushes the other girl into your lap. Suddenly " + target.name + "'s soft butt is rubbing firmly and pleasurably against your erection. " +
                    "Shit. She's going to make you cum too, even when you're helping her. <i>\"Don't let go of her, or you won't get credit for her orgasm.\"</i> Unable to struggle " +
                    "free, the sensation of " + target.name + "'s ass grinding against your dick makes you cum right after she does."
        }
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character): String {
        return if (target.human()) {
            "Your fight with " + assist.name + " is interrupted when you're suddenly grabbed from behind. The big breasts pressed against your back don't narrow down the " +
                    "suspects too much, but the hard bulge hitting your ass does. The last thing you want to do is expose your ass to Eve, but fortunately she doesn't have a " +
                    "firm grip on you yet. You jerk forward to try to escape her grasp. <i>\"Stop squirming!\"</i> She slams her knee up between your legs, hitting you squarely " +
                    "in the balls. The intense pain takes all the fight out of you and you go limp in her arms. <i>\"Are you done struggling or do you need another kick?\"</i> " +
                    "You meekly shake your head and let her secure her grip.<br>"
        } else {
            "Your fight with " + target.name + " quickly renders you both naked and aroused. She manages to trip you, dropping you solidly to the floor. She wastes no time " +
                    "and bends down to suck on your defenseless dick. You groan in pleasure as her tongue plays with your glans. Suddenly, she yelps in surprise and loses her balance. " +
                    "You spot Eve standing behind her, fondling her exposed girl parts. " + target.name + " tries to get back to her feet, but Eve easily forces her onto her back. " +
                    "<i>\"When I saw your sexy ass waving in front of me, I thought about giving you a good fucking, but then I noticed how enthusiastically you were blowing that boy.\"</i> " +
                    "She straddles the other girl's face and presses her girl-cock against her lips. <i>\"Let's see you put those skills to better use.\"</i><br>" +
                    "Apparently you've been forgotten. Oh well. " + target.name + "'s pussy looks pretty lonely. Looks like this will be your win.<br>"
        }
    }

    override fun watched(c: Combat, target: Character, viewer: Character) {
        if (viewer.human()) {
            SceneManager.play(SceneFlag.EveWatch)
        }
    }

    override fun startBattle(opponent: Character): String {
        if (character.grudge != null) {
            when (character.grudge) {
                Trait.sadisticmood -> return ("Eve gives you a wicked grin. <i>\"I had some fun on the bottom last time, but now I'm in the mood for payback. "
                        + "I'm gonna hurt you so good, boy. I'll break your balls until you cum like a bitch, and you'll love me for it.\"</i><p>"
                        + "Her voice is dripping with sensuality. Despite her threatening words, you find yourself getting excited. Her "
                        + "ability to manipulate fetishes must already be taking effect.")

                Trait.defensivemeasures -> return ("Eve fidgets with her underwear as you approach. <i>\"Just this once I'm gonna try taking everyone's advice.\"</i><p>"
                        + "She raps her knuckles again her groin, making a hollow plastic noise. She's clearly wearing a cup.<p>"
                        + "<i>\"This stupid thing is protecting my big weaknesses, but Jesus fuck it's cramped! If this doesn't work, "
                        + "I'm throwing it out.\"</i>")

                Trait.revvedup -> return ("Eve approaches you, already flushed and breathing hard. <i>\"Hey boy, you really know how to get my motor going! "
                        + "Those last couple fights felt fucking great! I can't wait to fuck your brains out this time!\"</i><p>"
                        + "She seems way more into this than she usually is. You better be careful, especially when she gets more excited.")

                else -> {}
            }
        }
        if (character.isNude) {
            return ("Eve approaches casually, showing off her huge assets. <i>\"Which do you like more? These?\"</i> She uses her hands to "
                    + "push her heavy breasts together. <i>\"Or these?\"</i> She waves her dangling genitals in your direction.<p>"
                    + "<i>\"Personally, I like them all.\"</i>")
        }
        if (opponent.isPantsless) {
            return "Eve smirks as her gaze drops to your groin. <i>\"Aww, look at that. So cute.\"</i>"
        }
        if (character.getAffection(opponent) >= 30) {
            return ("Eve gives you a dangerous smile as you approach. <i>\"Don't think I'll go easy on you because you're cute, boy. It just "
                    + "makes me want to toy with you more.\"</i>")
        }

        return "Eve gives you a dominant grin and cracks her knuckles. <i>\"Come on boy, let's play.\"</i>"
    }

    override fun fit(): Boolean {
        return !character.isNude && character.stamina.percent() >= 50
    }

    override fun night(): Boolean {
        return false
    }

    override fun advance(rank: Int) {
        if (rank >= 3 && !character.has(Trait.hardon)) {
            character.add(Trait.hardon)
        }
    }

    override fun checkMood(mood: Emotion, value: Int): Boolean {
        return when (mood) {
            Emotion.horny, Emotion.dominant -> value >= 30
            Emotion.nervous, Emotion.desperate -> value >= 80
            else -> value >= 50
        }
    }

    override fun moodWeight(mood: Emotion): Double {
        return when (mood) {
            Emotion.horny, Emotion.dominant -> 1.2
            Emotion.nervous, Emotion.desperate -> .7
            else -> 1.0
        }
    }

    override fun image(): String {
        // TODO Auto-generated method stub
        return ""
    }

    /*override fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }*/

    override fun resist3p(c: Combat, target: Character, assist: Character): String {
        // TODO Auto-generated method stub
        return ""
    }

    override val comments: CommentGroup
        get() {
        val comments = CommentGroup()
            comments[CommentSituation.VAG_DOM_CATCH_WIN] = "<i>\"What? You thought just because I have a cock, I couldn't take yours? Let me show you!\"</i>"
            comments[CommentSituation.VAG_SUB_CATCH_LOSE] = "<i>\"Fuck! You're going to have to let me repay you in kind next time!\"</i>"
            comments[CommentSituation.ANAL_CATCH_WIN] = "<i>\"I can take as well as I can give! You see?\"</i>"
            comments[CommentSituation.ANAL_PITCH_WIN] = "<i>\"Ah fuck! I knew the moment I saw you I was going to make you my bitch!\"</i>"
            comments[CommentSituation.ANAL_PITCH_LOSE] = "<i>\"Oh, shit! I'm gonna paint your insides white!\"</i>"
            comments[CommentSituation.BEHIND_DOM_WIN] = "<i>\"Do you want to cum, now? I could always take this pretty ass of yours...\"</i>"
            comments[CommentSituation.OTHER_OILED] = "<i>\"All lubed up and ready, I see. Just how I like you!\"</i>"
            comments[CommentSituation.OTHER_STUNNED] = "<i>\"Are you going to let me do what I want now? Good!\"</i>"
            comments[CommentSituation.SELF_HORNY] = "<i>\"Fuck! Wrap your ass around my cock NOW!\"</i>"
            comments[CommentSituation.OTHER_HORNY] = "<i>\"Don't cum before I'm satisfied or I'll make you pay!\"</i>"
            comments[CommentSituation.OTHER_SHAMED] = "<i>\"How pathetic. Are you going to shrivel like a shy little prick?\"</i>"
            comments[CommentSituation.SELF_BUSTED] = "<i>\"Fuck, I'll never get used to nut-shots.\"</i>  Eve grumbles, as her body hunches forward in pain."
            comments[SkillComment(Attribute.Fetish, true)] = "<i>\"You think you can out-kink me, dirty boy?\"</i>"
            comments[SkillComment(SkillTag.PET, false)] = "<i>\"Now it's going to get really kinky.\"</i>"


        return comments
    }

    override val responses: CommentGroup
        get() {
        val comments = CommentGroup()
        return comments
    }

    override val costumeSet = 1

    override fun declareGrudge(opponent: Character, c: Combat) {
        if ((character.grudge == Trait.defensivemeasures || character.grudge == Trait.sadisticmood)) {
            character.addGrudge(opponent, Trait.revvedup)
        } else {
            when (Global.random(2)) {
                0 -> character.addGrudge(opponent, Trait.defensivemeasures)
                1 -> character.addGrudge(opponent, Trait.sadisticmood)
                else -> {}
            }
        }
    }

    override fun resetOutfit() {
        character.outfit[0].clear()
        character.outfit[1].clear()
        character.outfit[0].add(Clothing.tanktop)
        character.outfit[1].add(Clothing.crotchlesspanties)
        character.outfit[1].add(Clothing.cutoffs)
    }
}

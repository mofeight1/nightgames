package characters

enum class State {
    ready,
    shower,
    combat,
    searching,
    crafting,
    hidden,
    resupplying,
    lostclothes,
    webbed,
    masturbating,
    quit,
}

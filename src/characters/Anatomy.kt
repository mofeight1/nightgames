package characters

import combat.Tag

enum class Anatomy : Tag {
    mouth,
    fingers,
    chest,
    genitals,
    ass,
    feet,
    arm,
    leg,
    head,
    neck,
    toy,
    soul,
    back,
}

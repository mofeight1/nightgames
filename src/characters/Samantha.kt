package characters

import comments.CommentGroup
import comments.CommentSituation
import actions.Action
import actions.Movement
import combat.Combat
import combat.Result
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import items.Clothing
import items.Toy
import items.Trophy
import scenes.SceneFlag
import scenes.SceneManager
import skills.Handjob
import skills.HandjobEX
import skills.Paizuri
import skills.Skill
import skills.ThrustEX
import skills.Whisper
import status.Stsflag

class Samantha : Personality {
    override var character: NPC = NPC("Samantha", ID.SAMANTHA, 20, this)

    init {
        character.outfit[0].add(Clothing.lacebra)
        character.outfit[0].add(Clothing.blackdress)
        character.outfit[1].add(Clothing.lacythong)
        character.outfit[1].add(Clothing.garters)
        character.closet.add(Clothing.lacebra)
        character.closet.add(Clothing.blackdress)
        character.closet.add(Clothing.lacythong)
        character.closet.add(Clothing.garters)
        character.change(Modifier.normal)
        character.mod(Attribute.Seduction, 24)
        character.mod(Attribute.Cunning, 9)
        character.mod(Attribute.Power, 11)
        character.mod(Attribute.Professional, 10)
        character.stamina.gainMax(20)
        character.arousal.gainMax(70)
        character.mojo.gainMax(25)
        character.money += 500
        Global.gainSkills(character)
        character.add(Trait.female)
        character.add(Trait.experienced)
        character.underwear = Trophy.SamanthaTrophy
        character.plan = Emotion.hunting
        character.mood = Emotion.confident
        character.strategy[Emotion.sneaking] = 1
        character.preferredSkills.add(Handjob(character))
        character.preferredSkills.add(HandjobEX(character))
        character.preferredSkills.add(ThrustEX(character))
        character.preferredSkills.add(Paizuri(character))
        character.preferredSkills.add(Whisper(character))
    }

    override fun act(available: HashSet<Skill>, c: Combat): Skill {
        val mandatory = HashSet<Skill>()
        for (a in available) {
            if (a.toString() === "Command" || a.toString().equals("Ass Fuck", ignoreCase = true)) {
                mandatory.add(a)
            }

            if (character.has(Stsflag.orderedstrip)) {
                if (a.toString() === "Undress" || a.toString() === "Strip Tease") {
                    mandatory.add(a)
                }
            }
        }
        if (mandatory.isNotEmpty()) {
            return mandatory.random()
        }

        return chooseSkill(available, c)
    }

    /*override fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        val proposed = character.parseMoves(available, radar, match)
        return proposed
    }*/

    override fun rest(time: Int, day: Daytime) {
        if (!(character.has(Toy.Dildo) || character.has(Toy.Dildo2)) && character.money >= 250) {
            character.gain(Toy.Dildo)
            character.money -= 250
        }
        if (!(character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && character.money >= 300) {
            character.gain(Toy.Onahole)
            character.money -= 300
        }
        if (!(character.has(Toy.Strapon) || character.has(Toy.Strapon2)) && character.money >= 600 && character.getPure(
                Attribute.Seduction
            ) >= 20
        ) {
            character.gain(Toy.Strapon)
            character.money -= 600
        }
        var loc: String
        val available = ArrayList<String>()
        available.add("Hardware Store")
        available.add("Black Market")
        available.add("XXX Store")
        available.add("Bookstore")
        if (character.rank > 0) {
            available.add("Workshop")
        }
        available.add("Play Video Games")
        for (i in 0 until time - 4) {
            loc = available.random()
            day.visit(loc, character, Global.random(character.money))
            if (loc !== "Exercise" && loc !== "Browse Porn Sites") {
                available.remove(loc)
            }
        }
        character.visit(4)
    }

    override fun bbLiner(): String {
        return when (Global.random(2)) {
            1 -> ("Samantha looks frustrated.   "
                    + "<i>\"Seems like common sense to wear a cup to a competition like this, but for some reason you boys never do.  Frankly, it makes the games a little too easy.\"</i>")

            else -> "<i>\"Oh, a bit of a masochist, are we? That's alright, I cater to all tastes!\"</i>"
        }
    }

    override fun nakedLiner(): String {
        return "Samantha strikes a stunning pose as the last of her body is uncovered. <i>\"All this could be yours, you know. You just need to cum a few times for me first.\"</i>"
    }

    override fun stunLiner(): String {
        return "<i>\"I much prefer the gentler kind of love, but if this is what you want, it's what you'll get!\"</i>"
    }
    
    fun winningLiner(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun taunt(): String {
        return "<i>\"I think I might try and see how effective pain is for you. Of course, I might have to really crank it up to get a good reaction...\"</i>"
    }

    override fun victory(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.anal) {
                Global.modCounter(Flag.PlayerAssLosses, 1.0)
                SceneManager.play(SceneFlag.SamanthaPeggingVictory)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.SamanthaSexVictory)
            } else if (opponent.has(Stsflag.horny)) {
                SceneManager.play(SceneFlag.SamanthaHornyVictory)
            } else {
                SceneManager.play(SceneFlag.SamanthaForeplayVictory)
            }
        }
    }

    override fun defeat(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        declareGrudge(opponent, c)
        if (opponent.human()) {
            if (c.stance.sub(character) && c.stance.anal()) {
                SceneManager.play(SceneFlag.SamanthaAnalDefeat)
            } else if (flag === Result.intercourse) {
                SceneManager.play(SceneFlag.SamanthaSexDefeat)
            } else {
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.SamanthaForeplayDefeatAlt)
                } else {
                    SceneManager.play(SceneFlag.SamanthaForeplayDefeat)
                }
            }
        }
    }

    override fun victory3p(c: Combat, target: Character, assist: Character): String {
        character.clearGrudge(target)
        character.clearGrudge(assist)
        return if (target.human()) {
            "Samantha squats down in front of you, and you immediately cease your struggling at the sight of her shapely bosom. <i>\"Good boy. Why don't we see if I can't have a " +
                    "little fun with you?\"</i> She gets on her knees and leans in, kissing you softly. Meanwhile, her hands roam over your chest, her fingers working magic on your nipples. " +
                    "You had grown flaccid after " + assist.name + " arrived, but just these simple touches are getting you rock hard again. Of course, her soft tongue dexterously exploring " +
                    "your mouth also helps. She keeps her body at some distance, careful never to make contact with you your turgid shaft. She moves her head down to one of your nipples, " +
                    "bringing the hand that was there down to your stomach. Her hand deftly scurries across your lower body, pressing down on your lower belly, tickling your sides and " +
                    "lightly drawing patterns over your abs, but still not laying a finger on the painfully swollen head of your cock. Every touch down below is accentuated by a caress, " +
                    "a suckle, a pull or even a light nibble on your nipples, and they seem to have a direct connection to your balls. You start to think you might actually cum just from " +
                    "this, and you start thinking about other things, trying to distract yourself and at least have her jerk you off to get her win. A couple of minutes pass, during which " +
                    "she never repeats the same combination of attentions twice, always bombarding you with new and wonderful sensations. Just when you think you are no longer able to hold on, " +
                    "she suddenly stops. <i>\"Really impressive, " + target.name + ". Most guys cum after two minutes at most. Guess I'll just have to give your friend down there some direct " +
                    "attention. Unless...\"</i> She lays down in front of you, resting her chin on her wrists, right in front of your dick. Then she purses her lips and softly blows. The feeling " +
                    "is so light, so immaterial, that you normally probably wouldn't have felt it. Now, however, it's just enough to send you into a screaming climax. You throw your head back " +
                    "and squeeze your eyes shut as your cock erupts. The final proof of your defeat soars through the air, nearly coating your own face. When your orgasm finally dies down, you " +
                    "are barely aware of being lain down on the ground and Samantha and " + assist.name + " walking of, and you honestly don't care."
        } else {
            "Samantha takes a moment to consider the situation, with you offering up " + target.name + " as you are. <i>\"Hmm, I guess I could just finger you and be done with it, but where's the " +
                    "fun in that? Besides, I should get something out of this too. But how to best... Ah! Perfect!\"</i> She moves in a blur, and somehow ends up with her legs wrapped around both " +
                    "your and " + target.name + "'s heads, pressing her pussy into her face while she dives in between " + target.name + "'s spread legs. You can't see what's going on down there, " +
                    "but being pressed up against " + target.name + " like this you can certainly feel the effects it has. She is twitching and squirming in your arms, and you have to work to hold on to her. " +
                    "Her moans grow to muffled screams as Samantha works her magic with her talented tongue. You count three orgasms before Samantha comes up for air, though it might have been a single " +
                    "long one. <i>\"Well go on, honey. From now on you don't get to cum unless I do, I'll just get you really close until you get me off.\"</i> " + target.name + " dives in with a vengeance as " +
                    "Samantha goes back down. Just the smell of Samantha's pussy so close to you has you hard as a rock against " + target.name + "'s back, and her swirling tongue occasionally splatters " +
                    "some of those sweet juices your way. You hear Samantha let out a soft moan, and immediately every muscle in " + target.name + "'s body tenses up as she screams her ecstasy into Samantha's " +
                    "squirting cunt. Both girls collapse into a heap as you stand, still painfully hard from the smell and " + target.name + "'s squirming. Samantha, of course is the first to rise. <i>\"Well " +
                    "that was fun,\"</i> she says looking down at " + target.name + ", <i>\"I could have you curled up and panting like that too, you know. But let's make it a fair match this time, huh?\"</i>"
        }
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character): String {
        return if (target.human()) {
            "Your fight against " + assist.name + " is going rather well, and you've got her on her stomach with you on top of her. Suddenly, a weight settles on your back, and when you look to the side, " +
                    "you are greeted by Samantha's face. She's lain down on top of you, pressing her thighs against yours and caressing your sides with her hands. <i>\"You don't mind if I join in, do you? " +
                    "I always enjoy threesomes.\"</i> You do mind, so you roll over, ending up atop her with both of you on your backs. She wraps her limbs around you holding you firmly in place. <i>\"Go " +
                    "ahead,\"</i> she says to " + assist.name + ", who has gotten up by now, <i>\"Have at him! I'll just wait for my turn.\"</i>"
        } else {
            target.name + " has gotten you in a bit of a pinch here. You're on the verge of orgasm and she doesn't show any signs of being remotely close yet. You lunge at her, " +
                    "but even as you do so you know she's going to counter it and bring you down. Then she freezes, looking at something behind you, and your attack connects. You are now sitting on her thighs " +
                    "as she is sprawled out beneath you, and then Samantha leisurely strolls by, ostensibly the cause of " + target.name + "'s distraction. She plants herself on " + target.name + "'s chest, " +
                    "pinning her arms above her head. Samantha looks over her shoulder at you. <i>\"Well go on, get her off and I will be able to do you next.\"</i>"
        }
    }

    override fun watched(c: Combat, target: Character, viewer: Character) {
        if (viewer.human()) {
            SceneManager.play(SceneFlag.SamanthaWatch)
        }
    }

    override fun resist3p(c: Combat, target: Character, assist: Character): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun describe(): String {
        return "Looking at Samantha, it's immediately obvious why she's so popular among the filthy rich. Her every curve is perfectly proportioned, " +
                "her skin completely unblemished, her long hair a red so dark it's almost purple and with a satin shine. Her every action oozes sensuality, " +
                "and the mischievous glint in her eyes tells you that she has no doubt she'll have you begging for her to finish you off in record time."
    }

    override fun draw(c: Combat, flag: Tag) {
        val opponent = c.getOther(character)
        character.clearGrudge(opponent)
        if (opponent.human()) {
            if (flag === Result.intercourse) {
                if (Global.random(2) == 0) {
                    SceneManager.play(SceneFlag.SamanthaSexDraw2)
                } else {
                    SceneManager.play(SceneFlag.SamanthaSexDraw)
                }
            } else {
                SceneManager.play(SceneFlag.SamanthaForeplayDraw)
            }
        }
    }

    override fun fightFlight(opponent: Character): Boolean {
        return !character.isNude || opponent.isNude
    }

    override fun attack(opponent: Character): Boolean {
        return true
    }

    override fun ding() {
        character.mod(Attribute.Seduction, 1)
        var rand: Int
        for (i in 0 until (Global.random(3) / 2) + 1) {
            rand = Global.random(3)
            when (rand) {
                0 -> {
                    character.mod(Attribute.Power, 1)
                }
                1 -> {
                    character.mod(Attribute.Seduction, 1)
                }
                2 -> {
                    character.mod(Attribute.Cunning, 1)
                }
            }
        }
        character.stamina.gainMax(3)
        character.arousal.gainMax(6)
        character.money += character.prize() * 5
    }

    override fun startBattle(opponent: Character): String {
        if (character.grudge != null) {
            when (character.grudge) {
                Trait.tantra -> return ("Samantha takes several deep breaths before addressing you. <i>\"Are you familiar with tantric sex, " + opponent.name + "? "
                        + "It's a set of yoga techniques that can keep sex going for ages without orgasming. It's not my preference, but in a "
                        + "competition like this, it's practically cheating.\"</i>")

                Trait.sparehandcuffs -> return ("Samantha approaches excitedly. <i>\"Hey " + opponent.name + ", I have a surprise for you.\"</i><p>"
                        + "She pulls out a set of handcuffs with a flourish. <i>\"I almost forgot I had these in my old 'Cops and Robbers' costume. "
                        + "So, have you been a bad boy?\"</i>")

                Trait.veteranprostitute -> return ("Samantha smiles gently, but looks a little bored. <i>\"It's pretty impressive that you made me cum first during sex, "
                        + "but love, can we try something more exotic? I love fucking, but after having so many cocks inside me, even I start "
                        + "to crave variety.\"</i>")

                else -> {}
            }
        }
        if (character.isNude) {
            return ("Samantha approaches with a sexy swagger that makes her tits bob pleasantly. If she's at all bothered by her nudity, she "
                    + "hides it well.<p>"
                    + "<i>\"Do you need a moment to pick up your jaw, love? It's OK. I'll wait until you're ready.\"</i>")
        }
        if (opponent.isPantsless) {
            return "Samantha grins at you. <i>\"Aren't you a little overeager, love? You didn't even give me a chance to undress you.\"</i>"
        }
        if (character.getAffection(opponent) >= 30) {
        }
        return "<i>\"Ah, there you are. I wanted to show my appreciation for your patronage. Oh yes, only the finest will do for you...\"</i>"
    }

    override fun fit(): Boolean {
        return !character.isNude && character.stamina.percent() >= 50
    }

    override fun night(): Boolean {
        SceneManager.play(SceneFlag.SamanthaAfterMatch)
        return true
    }

    override fun advance(rank: Int) {
        if (rank >= 4 && !character.has(Trait.careerseductress)) {
            character.add(Trait.careerseductress)
        }
        if (rank >= 3 && !character.has(Trait.sexuallyflexible)) {
            character.add(Trait.sexuallyflexible)
        }
    }

    override fun checkMood(mood: Emotion, value: Int): Boolean {
        return when (mood) {
            Emotion.horny -> value >= 50
            Emotion.nervous -> value >= 150
            else -> value >= 100
        }
    }

    override fun moodWeight(mood: Emotion): Double {
        return when (mood) {
            Emotion.horny -> 1.2
            Emotion.nervous -> .7
            else -> 1.0
        }
    }

    override fun image(): String {
        return "assets/samantha_" + character.mood.name + ".jpg"
    }

    /*override fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }*/

    override val comments: CommentGroup
        get() {
            val comments = CommentGroup()
            comments[CommentSituation.SELF_BUSTED] = "<i>\"That last hit was below the belt.\"</i>  Samantha says through her clenched teeth.  <i>\"I'll make sure to return the favor.\"<i>"
            return comments
        }

    override val responses: CommentGroup
        get() {
            val comments = CommentGroup()
            return comments
        }

    override val costumeSet = 1

    override fun declareGrudge(opponent: Character, c: Combat) {
        if (c.eval(character) == Result.intercourse) {
            character.addGrudge(opponent, Trait.veteranprostitute)
        } else {
            when (Global.random(2)) {
                0 -> character.addGrudge(opponent, Trait.tantra)
                1 -> character.addGrudge(opponent, Trait.sparehandcuffs)
                else -> {}
            }
        }
    }

    override fun resetOutfit() {
        character.outfit[0].clear()
        character.outfit[1].clear()
        character.outfit[0].add(Clothing.lacebra)
        character.outfit[0].add(Clothing.blackdress)
        character.outfit[1].add(Clothing.lacythong)
        character.outfit[1].add(Clothing.garters)
    }
}

package characters

import comments.CommentGroup
import actions.Action
import actions.Movement
import combat.Combat
import combat.Tag
import daytime.Daytime
import global.Flag
import global.Global
import global.Match
import global.Modifier
import items.Clothing
import skills.Skill
import status.Stsflag

class Selene : Personality {
    override val character: NPC = NPC("Selene", ID.SELENE, 1, this)

    init {
        character.outfit[Character.OUTFITTOP].add(Clothing.lacebra)
        character.outfit[Character.OUTFITTOP].add(Clothing.ruffledress)
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.lacepanties)
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.skirt2)
        character.closet.add(Clothing.lacebra)
        character.closet.add(Clothing.lacepanties)
        character.closet.add(Clothing.ruffledress)
        character.closet.add(Clothing.skirt2)
        character.change(Modifier.normal)
        character.add(Trait.female)
        character.add(Trait.phallicappendage)
        Global.gainSkills(this.character)
        character.plan = Emotion.sneaking
        character.mood = Emotion.confident
        character.strategy[Emotion.hunting] = 2
        character.strategy[Emotion.sneaking] = 4
    }

    override fun act(available: HashSet<Skill>, c: Combat): Skill {
        val mandatory = HashSet<Skill>()
        for (a in available) {
            if (character.has(Stsflag.orderedstrip)) {
                if (a.toString() === "Undress" || a.toString() === "Strip Tease") {
                    mandatory.add(a)
                }
            }
        }
        if (mandatory.isNotEmpty()) {
            return mandatory.random()
        }

        return chooseSkill(available, c)
    }

    /*override fun move(available: HashSet<Action>, radar: HashSet<Movement>, match: Match): Action {
        return character.parseMoves(available, radar, match)
    }*/

    override fun rest(time: Int, day: Daytime) {
    }

    override fun bbLiner(): String {
        return ""
    }

    override fun nakedLiner(): String {
        return ""
    }

    override fun stunLiner(): String {
        return ""
    }

    override fun taunt(): String {
        return ""
    }

    override fun victory(c: Combat, flag: Tag) {
        Global.gui.displayImage("premium/Selene Tease.jpg", "Art by AimlessArt")
    }

    override fun defeat(c: Combat, flag: Tag) {
        Global.gui.displayImage("premium/Selene Licked.jpg", "Art by AimlessArt")
    }

    override fun victory3p(c: Combat, target: Character, assist: Character): String {
        return ""
    }

    override fun intervene3p(c: Combat, target: Character, assist: Character): String {
        return ""
    }

    override fun resist3p(c: Combat, target: Character, assist: Character): String {
        return ""
    }

    override fun watched(c: Combat, target: Character, viewer: Character) {
    }

    override fun describe(): String {
        return "Selene"
    }

    override fun draw(c: Combat, flag: Tag) {
    }

    override fun fightFlight(opponent: Character): Boolean {
        return !character.isNude || opponent.isNude
    }

    override fun attack(opponent: Character): Boolean {
        return true
    }

    override fun ding() {
        for (i in 0 until (Global.random(3) / 2) + 2) {
            val rand = Global.random(3)
            when (rand) {
                0 -> {
                    character.mod(Attribute.Power, 1)
                }
                1 -> {
                    character.mod(Attribute.Seduction, 1)
                }
                2 -> {
                    character.mod(Attribute.Cunning, 1)
                }
            }
        }
        character.stamina.gainMax(4)
        character.arousal.gainMax(4)
        character.mojo.gainMax(2)
        character.money += character.prize() * 5
    }

    override fun startBattle(opponent: Character): String {
        return ""
    }

    override fun fit(): Boolean {
        return !character.isNude && character.stamina.percent() >= 50
    }

    override fun night(): Boolean {
        return false
    }

    override fun advance(rank: Int) {
    }

    override fun checkMood(mood: Emotion, value: Int): Boolean {
        return false
    }

    override fun moodWeight(mood: Emotion): Double {
        return 1.0
    }

    override fun image(): String {
        return ""
    }

    /*override fun pickFeat() {
        val available = Global.availableFeats(character)
        character.add(available.random())
    }*/

    override val comments: CommentGroup
        get() = CommentGroup()

    override val responses: CommentGroup
        get() = CommentGroup()

    override val costumeSet = 0

    override fun declareGrudge(opponent: Character, c: Combat) {
    }

    override fun resetOutfit() {
        character.outfit[0].clear()
        character.outfit[1].clear()
        character.outfit[Character.OUTFITTOP].add(Clothing.lacebra)
        character.outfit[Character.OUTFITTOP].add(Clothing.ruffledress)
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.lacepanties)
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.skirt2)
    }
}

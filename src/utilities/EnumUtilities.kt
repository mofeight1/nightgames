package utilities

import java.util.EnumMap
import java.util.EnumSet

inline fun <reified E: Enum<E>> enumValueOfOrNull(name: String): E? {
    return try {
        enumValueOf<E>(name)
    } catch (_: IllegalArgumentException) {
        null
    }
}


inline fun <reified K : Enum<K>, V> EnumMap(): EnumMap<K, V> {
    return EnumMap<K, V>(K::class.java)
}

inline fun <reified K : Enum<K>, V> Map<K, V>.toEnumMap(): EnumMap<K, V> {
    if (this.isEmpty()) return EnumMap()
    return this.toMap(EnumMap())
}

inline fun <reified E : Enum<E>> EnumSet(): EnumSet<E> {
    return EnumSet.noneOf(E::class.java)
}

inline fun <reified E : Enum<E>> EnumSet(c: Iterable<E>): EnumSet<E> {
    if (c is Collection) {
        if (c.isEmpty()) return EnumSet()
    }
    return c.toCollection(EnumSet())
}

inline fun <reified E : Enum<E>> Iterable<E>.toEnumSet(): EnumSet<E> {
    if (this is Collection) {
        if (isEmpty()) return EnumSet()
    }
    return this.toCollection(EnumSet())
}

inline fun <reified E : Enum<E>> enumSetOf(vararg elements: E): EnumSet<E> {
    return elements.toCollection(EnumSet())
}
inline fun <reified E : Enum<E>> enumSetOf(element: E): EnumSet<E> {
    return EnumSet.of(element)
}

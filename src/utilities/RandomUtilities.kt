package utilities

import kotlin.random.Random

fun Int.random(): Int {
    if (this <= 0) return 0
    return Random.nextInt(this)
}

fun <T> List<T>.weightedRandom(weights: Collection<Int>): T {
    return this.weightedRandom(weights.toIntArray())
}

fun <T> Array<T>.weightedRandom(weights: Collection<Int>): T {
    return this.weightedRandom(weights.toIntArray())
}

fun <T> List<T>.weightedRandom(weights: Array<Int>): T {
    return this.weightedRandom(weights.toIntArray())
}

fun <T> Array<T>.weightedRandom(weights: Array<Int>): T {
    return this.weightedRandom(weights.toIntArray())
}

fun <T> List<T>.weightedRandom(weights: IntArray): T {
    val total = weights.sum()
    var count = 0
    val choice = total.random()
    for (i in this.indices) {
        count += weights[i]
        if (choice <= count)
            return this[i]
    }
    return this[0]
}

fun <T> Array<T>.weightedRandom(weights: IntArray): T {
    val total = weights.sum()
    var count = 0
    val choice = total.random()
    for (i in this.indices) {
        count += weights[i]
        if (choice <= count)
            return this[i]
    }
    return this[0]
}
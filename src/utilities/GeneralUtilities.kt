package utilities

inline infix fun <reified A, reified B> A.isOf(other: B): Boolean {
    return this is B
}

inline infix fun <reified A, reified B> A.notOf(other: B): Boolean {
    return this !is B
}

inline fun <K, V, T: Pair<K, V>, R : Comparable<R>> Map<K, V>.sortedBy(crossinline selector: (T) -> R?): Map<K, V> {
    val list = this.toList() as List<T>
    return list.sortedBy(selector).toMap()
}

inline fun <K, V, T: Pair<K, V>, R : Comparable<R>> Map<K, V>.sortedByDescending(crossinline selector: (T) -> R?): Map<K, V> {
    val list = this.toList() as List<T>
    return list.sortedByDescending(selector).toMap()
}

inline fun <K, V> MutableMap<K, V>.removeAll(predicate: (K, V) -> Boolean): Boolean {
    var removed = false
    for ((key, value) in this) {
        if (predicate(key, value)) {
            removed = remove(key) != null || removed
        }
    }
    return removed
}

inline fun <K, V> MutableMap<K, V>.removeKeys(predicate: (K) -> Boolean): Boolean {
    var removed = false
    for (pair in this) {
        if (predicate(pair.key)) {
            removed = remove(pair.key) != null || removed
        }
    }
    return removed
}

inline fun <K, V> MutableMap<K, V>.removeValues(predicate: (V) -> Boolean): Boolean {
    var removed = false
    for ((key, value) in this) {
        if (predicate(value)) {
            removed = remove(key) != null || removed
        }
    }
    return removed
}

/**
 * Adds the specified element to the end of this list if it is not `null`.
 * @return `true` if the element is non-null because the list is always modified as the result of this operation
 */
fun <E> MutableList<E>.addNotNull(element: E?): Boolean {
    return if (element == null) false
    else add(element)
}

/**
 * Inserts an element into the list at the specified [index] if it is not `null`.
 * @return `true` if the element is non-null because the list is always modified as the result of this operation
 */
fun <E> MutableList<E>.addNotNull(index: Int, element: E?): Boolean {
    if (element == null) return false
    else {
        add(index, element)
        return true
    }
}

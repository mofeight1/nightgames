package global

import characters.Character

object LineUp {
    private val sixPa = arrayOf(
        intArrayOf(0, 1, 5, 2, 3),
        intArrayOf(2, 0, 3, 5, 4),
        intArrayOf(1, 3, 2, 4, 0),
        intArrayOf(0, 2, 4, 1, 3),
        intArrayOf(5, 4, 3, 1, 2),
        intArrayOf(5, 2, 1, 3, 0),
        intArrayOf(4, 5, 1, 3, 0),
        intArrayOf(0, 3, 4, 5, 2),
        intArrayOf(1, 2, 4, 0, 3),
        intArrayOf(1, 5, 4, 3, 0),
        intArrayOf(1, 3, 2, 4, 5),
        intArrayOf(1, 5, 4, 2, 0),
        intArrayOf(1, 5, 3, 4, 0),
        intArrayOf(1, 5, 0, 4, 2),
        intArrayOf(1, 2, 5, 0, 4),
        intArrayOf(3, 2, 4, 5, 0)
    )

    private val sixPb = arrayOf(
        intArrayOf(3, 2, 1, 0, 5),
        intArrayOf(3, 5, 4, 2, 0),
        intArrayOf(3, 4, 1, 2, 5),
        intArrayOf(4, 1, 5, 0, 2),
        intArrayOf(4, 0, 2, 1, 5),
        intArrayOf(3, 1, 4, 0, 2),
        intArrayOf(3, 5, 4, 1, 0),
        intArrayOf(2, 3, 1, 4, 5),
        intArrayOf(0, 3, 4, 5, 2),
        intArrayOf(3, 1, 5, 2, 0),
        intArrayOf(5, 0, 1, 4, 3),
        intArrayOf(3, 4, 1, 2, 0),
        intArrayOf(5, 3, 1, 0, 2),
        intArrayOf(0, 2, 4, 1, 3),
        intArrayOf(5, 4, 1, 0, 3),
        intArrayOf(5, 4, 3, 2, 0)
    )

    private val sevenPa = arrayOf(
        intArrayOf(1, 3, 4, 2, 0),
        intArrayOf(0, 6, 5, 3, 4),
        intArrayOf(2, 4, 0, 5, 6),
        intArrayOf(3, 5, 1, 6, 0),
        intArrayOf(1, 3, 5, 2, 6),
        intArrayOf(1, 4, 2, 5, 0),
        intArrayOf(3, 4, 5, 0, 6),
        intArrayOf(2, 6, 3, 1, 4),
        intArrayOf(3, 5, 2, 1, 0),
        intArrayOf(6, 5, 1, 0, 2),
        intArrayOf(4, 5, 1, 3, 2),
        intArrayOf(6, 3, 0, 1, 5),
        intArrayOf(4, 1, 2, 6, 3),
        intArrayOf(0, 4, 6, 3, 5),
        intArrayOf(0, 2, 4, 5, 1),
        intArrayOf(4, 3, 2, 0, 6)
    )

    private val sevenPb = arrayOf(
        intArrayOf(6, 2, 4, 5, 0),
        intArrayOf(3, 2, 4, 6, 1),
        intArrayOf(0, 2, 4, 3, 6),
        intArrayOf(1, 5, 3, 2, 6),
        intArrayOf(2, 4, 0, 3, 6),
        intArrayOf(1, 5, 2, 3, 4),
        intArrayOf(0, 1, 5, 2, 4),
        intArrayOf(0, 1, 5, 3, 2),
        intArrayOf(3, 5, 4, 0, 1),
        intArrayOf(0, 4, 2, 1, 6),
        intArrayOf(3, 5, 0, 6, 2),
        intArrayOf(2, 0, 1, 6, 5),
        intArrayOf(3, 5, 6, 4, 1),
        intArrayOf(3, 2, 0, 5, 6),
        intArrayOf(1, 3, 4, 5, 6),
        intArrayOf(0, 6, 4, 5, 1)
    )

    private val eightPa = arrayOf(
        intArrayOf(6, 5, 3, 7, 0),
        intArrayOf(4, 1, 2, 3, 6),
        intArrayOf(0, 4, 5, 3, 7),
        intArrayOf(4, 1, 6, 3, 5),
        intArrayOf(1, 4, 7, 2, 3),
        intArrayOf(0, 6, 7, 5, 2),
        intArrayOf(0, 2, 3, 4, 1),
        intArrayOf(2, 4, 5, 3, 0),
        intArrayOf(5, 2, 1, 7, 6),
        intArrayOf(4, 6, 2, 0, 7),
        intArrayOf(4, 6, 0, 1, 5),
        intArrayOf(0, 3, 1, 6, 4),
        intArrayOf(2, 5, 7, 1, 3),
        intArrayOf(0, 2, 7, 4, 5),
        intArrayOf(3, 5, 7, 6, 1),
        intArrayOf(1, 7, 2, 6, 0)
    )

    private val eightPb = arrayOf(
        intArrayOf(0, 3, 5, 2, 1),
        intArrayOf(3, 4, 5, 6, 7),
        intArrayOf(0, 1, 3, 6, 2),
        intArrayOf(7, 4, 1, 2, 6),
        intArrayOf(2, 0, 7, 6, 5),
        intArrayOf(4, 1, 2, 3, 5),
        intArrayOf(0, 7, 3, 5, 6),
        intArrayOf(6, 4, 7, 2, 3),
        intArrayOf(0, 1, 2, 7, 4),
        intArrayOf(0, 1, 5, 4, 2),
        intArrayOf(5, 6, 3, 0, 1),
        intArrayOf(4, 6, 7, 5, 3),
        intArrayOf(0, 4, 3, 1, 7),
        intArrayOf(7, 4, 5, 2, 1),
        intArrayOf(0, 6, 5, 4, 7),
        intArrayOf(3, 2, 6, 1, 0)
    )

    private val ninePa = arrayOf(
        intArrayOf(4, 3, 8, 6, 0),
        intArrayOf(4, 7, 2, 4, 1),
        intArrayOf(8, 6, 3, 7, 4),
        intArrayOf(0, 3, 2, 6, 8),
        intArrayOf(1, 5, 3, 6, 0),
        intArrayOf(0, 8, 1, 5, 6),
        intArrayOf(7, 4, 5, 6, 1),
        intArrayOf(2, 4, 7, 8, 3),
        intArrayOf(0, 5, 2, 1, 7),
        intArrayOf(7, 5, 3, 2, 6),
        intArrayOf(4, 8, 1, 0, 3),
        intArrayOf(2, 3, 8, 5, 0),
        intArrayOf(4, 7, 6, 2, 5),
        intArrayOf(3, 2, 0, 1, 8),
        intArrayOf(4, 7, 1, 5, 2),
        intArrayOf(8, 5, 1, 0, 7)
    )

    private val ninePb = arrayOf(
        intArrayOf(7, 0, 6, 1, 4),
        intArrayOf(1, 5, 7, 4, 8),
        intArrayOf(3, 1, 2, 6, 5),
        intArrayOf(0, 4, 3, 5, 6),
        intArrayOf(6, 4, 7, 3, 8),
        intArrayOf(0, 3, 8, 5, 1),
        intArrayOf(2, 0, 1, 4, 8),
        intArrayOf(1, 2, 5, 4, 3),
        intArrayOf(6, 7, 2, 4, 0),
        intArrayOf(6, 7, 8, 5, 0),
        intArrayOf(5, 6, 1, 2, 3),
        intArrayOf(7, 8, 2, 5, 6),
        intArrayOf(8, 0, 7, 3, 2),
        intArrayOf(2, 1, 3, 7, 0),
        intArrayOf(4, 6, 1, 3, 8),
        intArrayOf(2, 8, 5, 0, 7)
    )

    private val tenPa = arrayOf(
        intArrayOf(0, 8, 5, 2, 3),
        intArrayOf(8, 4, 5, 9, 3),
        intArrayOf(1, 3, 7, 5, 8),
        intArrayOf(0, 8, 1, 7, 4),
        intArrayOf(2, 6, 9, 4, 3),
        intArrayOf(4, 5, 1, 9, 0),
        intArrayOf(2, 6, 9, 4, 7),
        intArrayOf(6, 4, 7, 5, 9),
        intArrayOf(0, 1, 2, 3, 6),
        intArrayOf(0, 7, 6, 4, 3),
        intArrayOf(1, 2, 8, 9, 0),
        intArrayOf(1, 5, 8, 9, 7),
        intArrayOf(2, 3, 8, 5, 9),
        intArrayOf(1, 6, 8, 7, 4),
        intArrayOf(0, 2, 3, 6, 5),
        intArrayOf(6, 7, 0, 2, 1)
    )

    private val tenPb = arrayOf(
        intArrayOf(4, 0, 5, 8, 2),
        intArrayOf(2, 7, 8, 9, 4),
        intArrayOf(3, 6, 2, 5, 9),
        intArrayOf(1, 6, 9, 0, 3),
        intArrayOf(4, 7, 3, 5, 0),
        intArrayOf(7, 2, 9, 8, 6),
        intArrayOf(1, 4, 2, 7, 3),
        intArrayOf(4, 9, 7, 6, 0),
        intArrayOf(1, 7, 8, 2, 9),
        intArrayOf(1, 4, 0, 7, 8),
        intArrayOf(3, 5, 6, 1, 8),
        intArrayOf(8, 5, 6, 3, 1),
        intArrayOf(2, 4, 1, 0, 5),
        intArrayOf(2, 4, 6, 0, 5),
        intArrayOf(1, 3, 8, 6, 9),
        intArrayOf(3, 5, 9, 7, 0)
    )

    private val elevenPa = arrayOf(
        intArrayOf(0, 10, 1, 4, 9),
        intArrayOf(7, 8, 1, 9, 6),
        intArrayOf(0, 6, 8, 3, 5),
        intArrayOf(2, 0, 6, 3, 1),
        intArrayOf(3, 10, 2, 6, 4),
        intArrayOf(5, 7, 10, 3, 0),
        intArrayOf(9, 1, 5, 0, 4),
        intArrayOf(2, 7, 8, 6, 4),
        intArrayOf(8, 4, 3, 7, 5),
        intArrayOf(2, 9, 7, 5, 3),
        intArrayOf(0, 3, 2, 8, 4),
        intArrayOf(1, 6, 9, 10, 4),
        intArrayOf(7, 10, 5, 6, 9),
        intArrayOf(0, 2, 7, 1, 3),
        intArrayOf(10, 5, 9, 8, 1),
        intArrayOf(2, 8, 10, 1, 0)
    )

    private val elevenPb = arrayOf(
        intArrayOf(6, 10, 5, 1, 0),
        intArrayOf(1, 6, 4, 0, 5),
        intArrayOf(2, 8, 9, 6, 7),
        intArrayOf(2, 3, 9, 1, 0),
        intArrayOf(1, 0, 7, 8, 10),
        intArrayOf(2, 9, 3, 4, 8),
        intArrayOf(5, 3, 1, 6, 7),
        intArrayOf(3, 7, 1, 10, 0),
        intArrayOf(2, 4, 5, 8, 9),
        intArrayOf(0, 10, 5, 4, 8),
        intArrayOf(2, 6, 9, 5, 4),
        intArrayOf(6, 8, 1, 3, 2),
        intArrayOf(4, 0, 7, 8, 5),
        intArrayOf(9, 7, 5, 10, 3),
        intArrayOf(9, 2, 6, 10, 4),
        intArrayOf(7, 3, 0, 10, 4)
    )

    private val twelvePa = arrayOf(
        intArrayOf(1, 10, 6, 8, 0),
        intArrayOf(1, 7, 10, 6, 3),
        intArrayOf(7, 9, 8, 4, 0),
        intArrayOf(0, 4, 7, 2, 3),
        intArrayOf(3, 4, 1, 6, 10),
        intArrayOf(11, 5, 9, 3, 2),
        intArrayOf(8, 0, 4, 6, 9),
        intArrayOf(1, 7, 11, 3, 10),
        intArrayOf(2, 5, 8, 3, 9),
        intArrayOf(6, 10, 4, 1, 7),
        intArrayOf(11, 5, 9, 0, 3),
        intArrayOf(11, 2, 5, 8, 1),
        intArrayOf(9, 1, 10, 6, 2),
        intArrayOf(5, 8, 4, 0, 2),
        intArrayOf(11, 6, 8, 4, 5),
        intArrayOf(11, 2, 5, 7, 0)
    )

    private val twelvePb = arrayOf(
        intArrayOf(0, 6, 8, 9, 11),
        intArrayOf(6, 7, 2, 3, 4),
        intArrayOf(2, 5, 0, 6, 7),
        intArrayOf(3, 1, 7, 2, 9),
        intArrayOf(11, 8, 10, 4, 7),
        intArrayOf(10, 1, 5, 3, 0),
        intArrayOf(11, 8, 9, 2, 5),
        intArrayOf(3, 4, 6, 9, 2),
        intArrayOf(0, 9, 6, 1, 5),
        intArrayOf(11, 8, 10, 4, 5),
        intArrayOf(3, 10, 0, 7, 5),
        intArrayOf(11, 1, 8, 10, 3),
        intArrayOf(10, 11, 4, 5, 1),
        intArrayOf(7, 4, 6, 0, 1),
        intArrayOf(9, 4, 3, 7, 2),
        intArrayOf(0, 8, 1, 9, 2)
    )


    fun getLineup(combatants: ArrayList<Character>, matchnNum: Int, seasonNum: Int): ArrayList<Character> {
        val lineup = ArrayList<Character>()
        var bracket: IntArray? = null
        when (combatants.size) {
            5 -> return combatants
            6 -> bracket = if (seasonNum % 2 == 0) {
                sixPb[matchnNum]
            } else {
                sixPa[matchnNum]
            }

            7 -> bracket = if (seasonNum % 2 == 0) {
                sevenPb[matchnNum]
            } else {
                sevenPa[matchnNum]
            }

            8 -> bracket = if (seasonNum % 2 == 0) {
                eightPb[matchnNum]
            } else {
                eightPa[matchnNum]
            }

            9 -> bracket = if (seasonNum % 2 == 0) {
                ninePb[matchnNum]
            } else {
                ninePa[matchnNum]
            }

            10 -> bracket = if (seasonNum % 2 == 0) {
                tenPb[matchnNum]
            } else {
                tenPa[matchnNum]
            }

            11 -> bracket = if (seasonNum % 2 == 0) {
                elevenPb[matchnNum]
            } else {
                elevenPa[matchnNum]
            }

            12 -> bracket = if (seasonNum % 2 == 0) {
                twelvePb[matchnNum]
            } else {
                twelvePa[matchnNum]
            }
        }
        for (i in bracket!!) {
            lineup.add(combatants[i])
        }
        return lineup
    }
}

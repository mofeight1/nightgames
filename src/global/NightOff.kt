package global

import characters.Attribute
import characters.Dummy
import characters.Emotion
import characters.ID
import characters.Player
import characters.Trait
import scenes.Event
import java.time.LocalTime

class NightOff(private val player: Player) : Event {
    private val cassieSprite: Dummy
    private val maraSprite: Dummy
    private val jewelSprite: Dummy
    private val angelSprite: Dummy
    private val lillySprite: Dummy
    private val yuiSprite: Dummy
    private val valerieSprite: Dummy
    private val katSprite: Dummy
    private val samSprite: Dummy
    private val aishaSprite: Dummy
    private val reykaSprite: Dummy
    private var doing: plans

    init {
        this.doing = plans.NONE
        Global.current = this
        Global.gui.clearCommand()
        cassieSprite = Dummy("Cassie")
        maraSprite = Dummy("Mara")
        jewelSprite = Dummy("Jewel")
        angelSprite = Dummy("Angel")
        lillySprite = Dummy("Lilly")
        yuiSprite = Dummy("Yui")
        valerieSprite = Dummy("Valerie")
        katSprite = Dummy("Kat")
        samSprite = Dummy("Samantha")
        aishaSprite = Dummy("Aisha")
        reykaSprite = Dummy("Reyka")
        play("Choices")
    }

    fun offerChoices() {
        Global.gui
            .message("You could take tonight to relax and recover. Sometimes it's not bad to have a quiet night to yourself.")
        Global.gui.choose("Stay in")
        if (Roster.getAffection(ID.PLAYER, ID.CASSIE) >= 10 && Scheduler.isAvailable(ID.CASSIE)) {
            Global.gui.message(
                "You don't think Cassie has any plans for tonight. If you want to be a good boyfriend, this would be a good opportunity "
                        + "for a proper date night."
            )
            Global.gui.choose("Invite Cassie on a date")
        }
        if (Roster.getAffection(ID.PLAYER, ID.CASSIE) >= 30 &&
            Roster[ID.CASSIE].rank >= 3 &&
            Scheduler.isAvailable(ID.CASSIE)
        ) {
            Global.gui
                .message("Cassie recently mentioned having a surprise for you. Maybe you should see what she's cooked up.")
            Global.gui.choose("Check out Cassie's surprise")
        }
        if (Roster.getAffection(ID.PLAYER, ID.MARA) >= 12 &&
            player.getPure(Attribute.Science) >= 1 &&
            Scheduler.isAvailable(ID.MARA)
        ) {
            Global.gui
                .message("Mara sends you a text that she'll be working on something in Jett's workshop, and could use a hand.")
            Global.gui.choose("Go help Mara")
        }
        if (Roster.getAffection(ID.PLAYER, ID.JEWEL) >= 16 &&
            player.getPure(Attribute.Ki) >= 1 &&
            Scheduler.isAvailable(ID.JEWEL)
        ) {
            Global.gui.message(
                "Jewel texts you with an invite to a nice restaurant. She's polite, but insistent that you should only "
                        + "decline if you already have plans. Is she assuming you'd turn her down to just sit in your room doing nothing?"
            )
            Global.gui.choose("Dinner with Jewel")
        }
        if (Roster.getAffection(ID.PLAYER, ID.ANGEL) >= 10 &&
            Scheduler.isAvailable(ID.ANGEL)) {
            Global.gui.message(
                "Angel texts you to let you know she and the girls are planning to watch a bad movie. Her phrasing "
                        + "makes it clear that you're welcome to join and hints at possible sex after the movie."
            )
            Global.gui.choose("Movie night with Angel")
        }
        if (Global.checkFlag(Flag.Yui) &&
            Roster.getAffection(ID.PLAYER, ID.YUI) >= 12 &&
            Scheduler.isAvailable(ID.YUI)
        ) {
            Global.gui.message(
                "Yui would probably be up for a date tonight, but you should probably decide quick. Since she doesn't have a phone, " +
                        "you'll have to go track her down to ask her out."
            )
            Global.gui.choose("Go find Yui")
        }
        if (Roster.getAffection(ID.PLAYER, ID.AISHA) >= 12) {
            Global.gui.message(
                "You heard from Aisha that there's a big festival happening in the fae realm tonight, though that's probably true most nights. " +
                        "She could help arrange a visit if you think you can handle it."
            )
            Global.gui.choose("Fae party")
        }
        if (Roster.getAffection(ID.PLAYER, ID.VALERIE) >= 16 &&
            Roster.getAffection(ID.PLAYER, ID.JUSTINE) >= 8 &&
            Scheduler.isAvailable(ID.VALERIE)
        ) {
            Global.gui.message(
                "You get a text from Valerie (composed as if she's writing a letter - still no progress on that particular quirk of hers), asking if you might be in the mood for a group date with her and Justine. " +
                        "She has a nice restaurant in mind, and absolutely insists on paying for the meal."
            )
            Global.gui.choose("Group date with Valerie and Justine")
        }
        if (Roster.getAffection(ID.PLAYER, ID.KAT) >= 14 && Scheduler.isAvailable(ID.KAT)) {
            Global.gui.message("You get a text from Kat inviting you to meet up outside your dorm.")
            Global.gui.choose("Meet up with Kat")
        }
        if (Roster.getAffection(ID.PLAYER, ID.SAMANTHA) >= 10 && Scheduler.isAvailable(ID.SAMANTHA)) {
            Global.gui
                .message("Maybe you can show Samantha that you have mature taste by inviting her to a nice restaurant.")
            Global.gui.choose("Dinner with Samantha")
        }
        if (Roster.getAffection(ID.PLAYER, ID.REYKA) >= 12 && Scheduler.isAvailable(ID.REYKA)) {
            Global.gui
                .message("You receive a suggestive voicemail from Reyka that she could use some male assistance tonight.")
            Global.gui.choose("Reyka booty call")
        }
        if (Global.checkFlag(Flag.Lillyphone) && !Scheduler.isMatchNight) {
            Global.gui.message(
                "There's no match tonight, so Lilly is probably free. She's pretty good company, even if there's no "
                        + "romantic chemistry between you. Maybe you should give her a call."
            )
            Global.gui.choose("Hang out with Lilly")
        }
    }

    override fun respond(response: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (this.doing == plans.STAYIN) {
            if (response.contains("Power")) {
                Global.gui.message(
                    "You watch over several of the fights that you weren't present for, focusing on direct combat techniques. "
                            + "There are no amateurish catfights here, "
                            + "these girls can be brutal with each other. You had assumed your testicles were being unfairly persecuted, but you "
                            + "see a surprising number of kicks and knees aimed at the female anatomy. Each painful hit is usually followed by a "
                            + "significant amount of licking and fingering. That probably helps prevent any hard feelings between them.<p>"
                            + "By careful study, you manage to pick up some new grappling techniques that should be useful in future fights. In order "
                            + "to stay focused, you end up jerking off several times, which keeps you from getting as much rest as you planned.<p>"
                            + "<b>Your Power has improved noticeably.</b>"
                )
                player.mod(Attribute.Power, 2)
            } else if (response.contains("Seduction")) {
                Global.gui.message(
                    "You skim through and watch some of the most intense intimate encounters of the last couple matches. You can "
                            + "tell that despite your experience in the games, these girls still know their way around the female body better than "
                            + "you do. You're pretty sure most of them are bisexual, but even the supposedly straight Cassie looks pretty enthusiastic "
                            + "about eating out her opponents. These videos are effectively the most convincing lesbian porn you've ever seen. The fact that "
                            + "it stars girls you intimately know is just a bonus.<p>"
                            + "Needless to say, you spend most of time watching and rewatching these videos with your cock in your hand, but you also "
                            + "learn a few new tricks for getting a girl off.<p>"
                            + "<b>Your Seduction has improved noticeably.</b>"
                )
                player.mod(Attribute.Seduction, 2)
            } else if (response.contains("Cunning")) {
                Global.gui.message(
                    "You watch the highlights of the most recent matches, looking for the most cunning tricks you can "
                            + "adopt. Unsurprisingly, Mara is the most prolific trickster, but just about everyone seems to have a few tricks up their "
                            + "sleeves. To some extent, each girl has subtly different tactics for stripping and outmaneuvering their opponents.<p>"
                            + "You're theoretically observing and analyzing strategies, but the abundance of nudity and sex makes it difficult to "
                            + "concentrate. You end up needing to take a couple breaks to masturbate, but you eventually have a short list of tricks "
                            + "you're eager to try out in your next match."
                            + "<b>Your Cunning has improved noticeably.</b>"
                )
                player.mod(Attribute.Cunning, 2)
            } else {
                Global.gui.message(
                    "You spend a quiet night relaxing and watching videos on your computer. You end up falling asleep pretty early. "
                            + "It's not the most exciting night, but your body thanks you for the extra rest. When you wake up, you feel more refreshed and "
                            + "focused.<p>"
                            + "<b>Your maximum Stamina and Arousal have increased.</b>"
                )
                player.stamina.gainMax(5)
                player.arousal.gainMax(5)
            }
            end()
        }
        if (this.doing == plans.CASSIEMOVIE) {
            if (response.startsWith("Head back")) {
                cassieSprite.undress()
                cassieSprite.blush = 3
                cassieSprite.mood = Emotion.horny
                Global.gui.loadPortrait(player, cassieSprite)
                Global.gui.message(
                    " You need no further invitation and stand up so quickly that you almost forget to put your dick back in your pants.<p>"
                            + "When you are back inside your room, Cassie almost immediately begins undressing. She is much more fired up than usual after the "
                            + "daring act she pulled in the theater. Her enthusiasm is infectious, and, before you know it, you're both naked on the bed "
                            + "kissing passionately.<p>"
                            + "In a matter of moments, Cassie is straddling you, rubbing her moist pussy against your hardening dick. Your focus is purely on "
                            + "pleasuring her, so you bring one hand up to her breast and move one to your now throbbing erection. You knead her breast while "
                            + "you line up your cock and slowly slide into her burning pussy.<p>"
                            + "Cassie stays still for a moment to adjust to the full warmth, then starts moving her hips, slowly at first. Soon though, she "
                            + "finds the right rhythm and begins speeding up. You bring your second hand up to play both of her cute nipples, spurring her "
                            + "on even further.<p>"
                            + "After several minutes, you both are nearing climax. Cassie picks up her bouncing to a feverish pace, and you begin thrusting "
                            + "to match, taking hold of her hips to time it. Each time your pelvises pound together, a shockwave of pleasure rolls over both "
                            + "of you. Cassie is quickly overwhelmed by these quakes, and cums hard on your dick. You keep pounding away at the beautiful "
                            + "woman on top of you, and quickly reach climax yourself. With one last mighty thrust, you blast your load inside her, giving her "
                            + "a second powerful orgasm.<p>"
                            + "As the two of you begin catching your breath, Cassie leans down to plant a gentle kiss on your lips. You wrap her in your arms "
                            + "and bring her close while basking in the afterglow of your love. The two of you stay together like that for some time. "
                            + "Eventually, she breaks the embrace. She dresses and leaves the room, but only long enough to clean up a little. After a minute, "
                            + "she returns and lies down next to you. You take the opportunity to ask her if she enjoyed the date, even though it didn't end up "
                            + "being very romantic.<p>"
                            + "She quietly replies, <i>\"Well, I hadn't really thought about it until recently, but I guess sex is pretty "
                            + "romantic itself. And the movie was boring anyway, so no loss.\"</i> After all that's happened, you can't help but agree. By now "
                            + "it's late enough that Cassie sees no reason to get up, so the two of you lie together until you both fall asleep."
                )
            } else {
                cassieSprite.undress()
                cassieSprite.blush = 3
                cassieSprite.mood = Emotion.horny
                Global.gui.loadPortrait(player, cassieSprite)
                Global.gui.displayImage("premium/Cassie Cinema.jpg", "Art by AimlessArt")
                Global.gui.message(
                    "Cassie's eyes widen in surprise for a moment before she slips down to the floor, between your knees. She quickly "
                            + "removes her shirt and her bra, giving you free access to her boobs as she starts to lick the tip of your cock.<p>"
                            + "Between the skillful ministrations of the beautiful woman in front of you and the idea of being noticed by the people sitting "
                            + "merely 20 feet away, you don't last very long. You quietly tell Cassie that you're about to cum, and she responds by taking "
                            + "the length of your shaft into her mouth until the head is poking at the back of her throat. The rush of warmth and pressure "
                            + "sends you over the edge, and you blow your load down Cassie's eager throat.<p>"
                            + "Before you have a chance to catch your breath, Cassie stands up and brings her face close to yours. She swallows deeply, making "
                            + "sure that you can hear the last bit of your cum make its way down her throat.<p>"
                            + "Cassie is clearly not satisfied yet. She quickly throws on her shirt and stuffs her bra into her purse as she leads you away from "
                            + "your seats. In the light of the lobby, you can see that Cassie is much more fired up than usual, so much so that she's turning "
                            + "red in the face. She keeps pulling you along, but not toward the exit. She looks around quickly to make sure no one is looking "
                            + "and drags you into the women's restroom. The restroom is empty and likely will be until the movie finishes, so she begins "
                            + "undressing on the spot.<p>"
                            + "While you remove your pants, Cassie hops onto the restroom counter and spreads her legs. She begins rubbing her moist pussy "
                            + "in anticipation as you move over to her. She has herself so worked up that she nearly cums as soon as you slide your cock into "
                            + "her. You don't give her a chance to breathe, though, as you find the rhythm of your thrusts and begin to pick up the pace. In "
                            + "a matter of moments, Cassie is teetering on the edge of orgasm, so you decide to push her over. You begin to rub her clit, and "
                            + "she reacts without thinking by frantically playing with her nipples. She cums hard, and the increased tightness of her folds "
                            + "sends you over the edge with her. You unload your cum in her hot pussy for what seems like an eternity.<p>"
                            + "After you finish, she wraps her legs around you to keep you inside her. By now the movie is probably almost finished, but you "
                            + "are both more worried about holding each other close than the people who might catch you. Now that she has calmed down again, "
                            + "Cassie gives you a gentle kiss on the lips.<p>"
                            + "After a few minutes of cuddling, you reluctantly break your embrace and pull out of her. The two of you quickly clean up and "
                            + "dress, and make your way out of the restroom before anyone else comes in. In the lobby, you see people leaving the movie, "
                            + "some of them making their way toward the restrooms. Cassie can't help but giggle at how close you came to being found out. "
                            + "Now that you have a chance, you ask Cassie if that date was really her idea of romantic.<p>"
                            + "She says, <i>\"Well, I hadn't really thought about it until recently, but I guess sex is pretty romantic itself. And the "
                            + "movie was boring anyway, so no loss.\"</i> After all that's happened, you can't help but agree. You plant another kiss on "
                            + "Cassie's lips, thinking it will be a goodbye kiss, but it lingers longer than you intended. Before you know it, you are inviting "
                            + "her back to your room to further 'discuss' the intricacies of romance."
                )
            }
            Roster.gainAffection(ID.PLAYER, ID.CASSIE, 3)
            Global.gui.message("<b>You've gained Affection with Cassie</b>")
            end()
        }
        if (this.doing == plans.MARATESTING) {
            if (response.contains("down")) {
                maraSprite.blush = 3
                maraSprite.mood = Emotion.confident
                Global.gui.loadPortrait(player, maraSprite)
                Global.gui.message(
                    "You turn the vibration down to about 20%. Mara starts giving a more detailed explanation of the design of her "
                            + "multitool, directing Jett's attention back to it. She glances your direction and gives an almost imperceptible nod. You "
                            + "take that to mean this level is manageable for now.<p>"
                            + "She pulls a stool up next to Jett's workbench and sits down. You shouldn't have to worry about her ability to stay "
                            + "standing now. You wait until you're sure Jett is completely focused on testing the device, then turn up the vibration a "
                            + "little bit. Mara doesn't visibly react this time. She's getting better at this.<p>"
                            + "You gradually continue to increase the vibration intensity as Mara and Jett continue working on her multitool. You "
                            + "notice her squirming on the stool, but it's probably only because you're looking for it. Jett still doesn't seem to "
                            + "have caught on.<p>"
                            + "<i>\"Even at a consistent wattage, is this even safe in concept?\"</i> Jett gives Mara a stern look. <i>\"The last "
                            + "thing you want to do is sterilize your targets.\"</i> He has to be at least a little suspicious by now. You can tell "
                            + "how flushed Mara is from here, and he's sitting right next to her. You consider turning the vibrator down again, "
                            + "but if you don't let her cum, the test will never finish.<p>"
                            + "<i>\"I-I wasn't planning to use it on anyone until I finished researching the safety considerations.\"</i> Mara "
                            + "suddenly breaks into a minor coughing fit. You notice her hand under the workbench, trying to signal you outside "
                            + "of Jett's line of sight. You're not 100% sure what she's trying to indicate, but it looks urgent.<p>"
                            + "Did she finish already? Is she coughing to conceal her orgasm? You recall that Mara gets very sensitive after "
                            + "orgasming. If she just came, that would explain the urgency of her gestures. You shut off the remote to be safe.<p>"
                            + "Mara looks relieved and reassures Jett that she is ok. When he isn't paying attention, she throws a quick wink "
                            + "in your direction. Looks like you made the right call.<p>"
                            + "Jett removes the new attachment from Mara's multitool and hands it back to her. <i>\"Sorry Mara, but I think "
                            + "this 'pleasure beam' of yours isn't a viable design. I can tell the idea has you pretty worked up, but it doesn't "
                            + "seem safe or practical.\"</i><p>"
                            + "Mara cheerfully excuses herself and practically skips back to you. Jett looks a little confused at her attitude, "
                            + "given his rejection of her prototype, but he shrugs and resumes his work. Mara cozies up next to you and gives "
                            + "you a quick kiss on the cheek.<p>"
                            + "<i>\"Mission accomplished.\"</i> She whispers seductively in your ear. <i>\"That felt so good.\"</i> You were "
                            + "focused on your task, but it's actually pretty exciting to think you just secretly made Mara orgasm in front of "
                            + "someone. It would have been more satisfying to do it with a more 'hands on' approach, but knowing you can pleasure "
                            + "her with a button press is pretty hot in its own way.<p>"
                            + "You congratulate Mara on a successful field test and try to pass her back the remote. However, she grins and shakes "
                            + "her head. She snuggles up closer to you and nibbles gently on your ear.<p>"
                            + "<i>\"You hold onto it.\"</i> Her whisper is hot with anticipation. <i>\"Surprise me.\"</i>"
                )
            } else {
                maraSprite.blush = 3
                maraSprite.mood = Emotion.desperate
                Global.gui.loadPortrait(player, maraSprite)
                Global.gui.message(
                    "When you turn the remote up to maximum, you see a shudder run through Mara's body. She bites her lip to "
                            + "suppress a moan and her knees buckle. You quickly shut off the remote, but that was suspicious as hell. There's no "
                            + "way Jett missed that.<p>"
                            + "Jett lets out a patient sigh and pushes the multitool to the side. <i>\"Ok Mara, show me your <u>other</u> new toy.\"</i>"
                            + " He gestures sternly to an open space on the workbench. Well, shit. You're busted now.<p>"
                            + "Mara sheepishly reaches into her pants to retrieve the vibrator. Unfortunately you're holding half the toy, so you "
                            + "have to come forward too. You walk up to Jett's workbench and hand over the remote. He raises an eyebrow at you, "
                            + "but says nothing.<p>"
                            + "Mara elbows you in the ribs for messing up and places the egg on the workbench. It's still visibly wet with her "
                            + "juices, but Jett picks it up without hesitation and begins disassembling the casing. Mara turns bright red as she "
                            + "watches her mentor casually handle something that was just inside her.<p>"
                            + "Long awkward seconds pass as Jett examines the device. <i>\"This is a very expensive motor for such a basic sex toy. "
                            + "What's the point?\"</i> He pins Mara with his stare, while she fidgets embarrassedly.<p>"
                            + "<i>\"It's very quiet, so you can use it in public without getting caught.\"</i> Mara is much more quiet and subdued "
                            + "than usual.<p>Jett's stern expression breaks as he lets out a snort of amusement. <i>\"It's quiet, but you're not. "
                            + "As soon as you start moaning, you're going to get caught.\"</i> He passes the egg back to Mara and she pockets it "
                            + "with another blush. <i>\"I don't see any practical use for this in the Games, but you two are free to "
                            + "have your fun.\"</i><p>"
                            + "You and Mara quietly retreat after Jett dismisses you. Outside the workshop, Mara slumps against you, thoroughly "
                            + "defeated.<p>"
                            + "<i>\"I should have picked a different test subject. I didn't expect getting caught by Jett to be so embarrassing. "
                            + "I'm going to remember that every time I see him now.\"</i> You pat her on the head reassuringly. At least Jett wasn't "
                            + "particularly angry. Besides, Mara usually goes out of her way to try to appear sexier and more seductive. This could "
                            + "potentially help.<p>"
                            + "She slaps you lightly on the arm. <i>\"I only do that for you, dumbass! You're my boy, so I want you to think I'm sexy. "
                            + "Jett is my teacher, so I want him to respect me for my brain.\"</i> She moves from leaning on your arm to leaning on "
                            + "your chest; a significantly more huggable position, which you take full advantage of.<p>"
                            + "<i>\"It would be nice if you respect me for my brain too, but I don't want Jett thinking sexy thoughts about me. "
                            + "That'll just make things complicated.\"</i> You give her a tender kiss on the lips, which seems to cheer her up.<p>"
                            + "<i>\"Since we got caught, I didn't get to cum.\"</i> She smiles up at you seductively. <i>\"Can we return to your room "
                            + "and fix that?\"</i>"
                )
            }
            Roster.gainAffection(ID.PLAYER, ID.MARA, 3)
            Global.gui.message("<b>You've gained Affection with Mara</b>")
            end()
        }
        if (this.doing == plans.JEWELDINNER) {
            jewelSprite.undress()
            jewelSprite.blush = 3
            jewelSprite.mood = Emotion.horny
            Global.gui.loadPortrait(player, jewelSprite)
            Global.gui.message(
                "Soon, you and Jewel have changed into workout clothes and you face her in the middle of Suzume's dojo. "
                        + "It's not surprising that Jewel has a key to the place, although it's weird you've never seen her there.<p>"
                        + "You're wondering if Suzume's ever taught Jewel, and if she teaches Jewel the same weird way she teaches you, when "
                        + "Jewel throws a kick at your head. You barely get out of the way, and block a follow-up without thinking. Jewel's "
                        + "putting a lot of force behind her strikes, and it takes most of your concentration and recent experience to stay "
                        + "on your feet.<p>"
                        + "She's been at this longer than you have, though, and eventually one gets through. Thankfully, you manage to roll "
                        + "with it and her knuckles skip off your cheekbone. You take a few steps backward, and Jewel halts in the middle "
                        + "of a follow-up strike.<p>"
                        + "<i>\"Too far?\"</i><p>"
                        + "You rub your face. It was close, but you're fine. Before the Games and all the associated training, a punch "
                        + "like that would have laid you out, guaranteed.<p>"
                        + "<i>\"Good.\"</i><p>"
                        + "She attacks you again, and if she's trying to take it easier on you, you can't tell at first. You manage to "
                        + "block or avoid the worst of it, though, and eventually, Jewel goes for a high kick that she ordinarily wouldn't "
                        + "have tried at all. You catch her shin on your forearm, pull her off-balance, and pin her to the floor with a "
                        + "forearm across her collarbone.<p>"
                        + "<i>\"Okay. That wasn't bad,\"</i> Jewel says.<p>"
                        + "You thank her for what is, for her, a fawning compliment.<p>"
                        + "<i>\"Are you going to let me up?\"</i><p>"
                        + "You were, but you get a better idea. Jewel smiles as you strip off her clothes, and once you've gotten her naked, "
                        + "tears at your workout gear with the same peculiar intensity that she fought with.<p>"
                        + "Jewel rolls you over and gets on top, kissing you fiercely. She's different right now, faster and shakier, even when "
                        + "she sits gently down on your shaft. For what could be a long time, she seems content to leave you there sheathed "
                        + "inside her, busy with her tongue against yours.<p>"
                        + "Eventually, she starts moving. There's an urgency to her movement that isn't usually there, and you realize that "
                        + "she's using up the last of that burst of nervous adrenaline on this. As has become the theme of the night, you "
                        + "just do your best to hold on, letting Jewel dictate the pace. She breaks the kiss so she can brace herself against "
                        + "your naked chest with both hands, bearing down on your cock with short snaps of her hips.<p>"
                        + "At that pace, it doesn't take her long before both of you are at the edge. You close your eyes and dig your fingers "
                        + "into Jewel's thighs, trying to keep yourself from coming, but then Jewel makes a sound you recognize by heart by now. "
                        + "She's let go, shuddering to a climax, and you stop holding back. Quietly, with a sigh, you fill Jewel with your seed.<p>"
                        + "You gather her into a hug as she loses her balance. Jewel lies on top of you, resting her cheek against your shoulder, "
                        + "and you listen as her breathing slowly returns to normal.<p>"
                        + "<i>\"I'm okay,\"</i> Jewel says before you ask out loud. <i>\"Just... I needed that. All of that.\"</i> "
                        + "She kisses your chest, just above your heart. <i>\"I think I might be an adrenaline junkie.\"</i><p>"
                        + "You find some paper towels and clean up the dojo, then let yourselves out. It is, after all, a school night<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.JEWEL, 3)
            Global.gui.message("<b>You've gained Affection with Jewel</b>")
            end()
        }
        if (response.startsWith("Stay in")) {
            Global.gui.message(
                "You settle in for a quiet night alone. You've worked up a real sleep deficit this week. When you check your email, you notice that "
                        + "you've been sent recordings of the recent matches. If you wanted to be particularly diligent, you could probably learn quite a bit by studying "
                        + "your opponents' techniques. Of course, just glancing over one of the videos has you pretty excited. You probably won't be able to calm down "
                        + "very well while watching these.<p>"
                        + "You could focus on studying combat techniques, sexual techniques, or clever tricks. However, it would probably be beneficial to both body "
                        + "and mind if you spend all night just resting."
            )
            this.doing = plans.STAYIN
            Global.gui.choose("Study Power techniques")
            Global.gui.choose("Study Seduction techniques")
            Global.gui.choose("Study Cunning techniques")
            Global.gui.choose("Rest")
        }
        if (response.startsWith("Invite Cassie")) {
            cassieSprite.dress()
            cassieSprite.blush = 1
            cassieSprite.mood = Emotion.horny
            Global.gui.loadPortrait(player, cassieSprite)
            this.doing = plans.CASSIEMOVIE
            Global.gui.message(
                "Dinner and a movie isn't exactly Cassie's ideal romantic date, but it is a close second, and a weekend in Paris "
                        + "wasn't really reasonable. The two of you have reservations at a nearby Italian restaurant. As you arrive, the atmosphere "
                        + "is perfect. Cassie practically glows with excitement at the taste of amour in the air. You glow with excitement at the "
                        + "taste of spaghetti in your mouth.<p>"
                        + "The conversation turns to topics that seem 'right' to discuss on a romantic date, "
                        + "such as hopes and dreams for the future. Neither of you have given much thought to such things, though, since you've "
                        + "both been focused on the Games, so the conversation dries up quickly. Cassie suggests finishing up and heading to an "
                        + "earlier showing of the movie you were planning on seeing.<p>"
                        + "The movie itself is nothing special. It's standard romcom fare, and the theater is mostly empty. However, the lonely "
                        + "dark is still the height of romance for Cassie, as if being so close together is a treat enough. She snuggles close "
                        + "to you as soon as you get into your seats.<p>"
                        + "As the movie gets underway, though, you notice Cassie fidgeting and fussing more and more. She grows more impatient "
                        + "throughout the movie, and, around the halfway mark, sits upright. By now, Cassie has realized why she is restless, and "
                        + "you realize it too when she moves her hand onto your crotch. The two of you are sitting in the back of the theater with "
                        + "few people around, so you don't try move her hand away.<p>"
                        + "Before she really thinks about what she's doing, she pulls your dick out of your pants and begins gently rubbing. You "
                        + "follow her lead and slide your hand into her shirt, copping a feel of her breast. After a minute or two of gentle "
                        + "fondling, Cassie starts getting restless again. She's torn between thrill and terror at the idea of being caught giving "
                        + "a handjob in public, but her shyness shortly wins out. She whispers in your ear, "
                        + "<i>\"Let's get back to your room, so we can really go all out.\"</i>"
            )
            Global.gui.choose("Head back", "Some good times surely await you back at your dorm room.")
            if (Roster[ID.CASSIE].rank >= 2) {
                Global.gui.choose(
                    "Stay here",
                    "Why not stay here? The theatre is almost empty, and Cassie can't be that shy after all you've done together."
                )
            }
        }
        if (response.startsWith("Check out Cassie's surprise")) {
            cassieSprite.undress()
            cassieSprite.blush = 2
            Global.gui.loadPortrait(player, cassieSprite)
            Global.gui.message(
                "<i>\"No peeking,\"</i> Cassie says.<p>" +
                        "You close your eyes and make a point of putting your hand over them, too. She's adorably nervous right now, for some reason, so you're happy to play along.<p>" +
                        "The way she's acting, you expect her to be putting on lingerie. Instead, not even a full minute later, a tingling rush of raw mana passes over you. There's a familiar feel to it, one you can't quite place, but before you can put too much thought into that, you're distracted.<p>" +
                        "For one thing, that rush took your clothes with it.<p>" +
                        "For another, you're somewhere else. You uncover your eyes, and instantly have to shade them again, flash-blinded by sudden sunlight.<p>" +
                        "You're sweating at once. You can hear the gentle rush of waves against sand; you smell the wind off the ocean, with underlying currents of coconut and citrus. There's sand between your toes. Somewhere nearby, a bird calls out, and is answered.<p>" +
                        "By the time you can see again, Cassie's in front of you, as naked as you are, beet-red and making concerned sounds. <i>\"Are you okay?\"</i> she asks.<p>" +
                        "On the whole, you are. It's just a jarring transition. Wherever you are now, it's an equatorial climate, at least 30F warmer than Cassie's dorm room. As you straighten up, you look around.<p>" +
                        "You're on a postage stamp of an island somewhere in the middle of a placid blue ocean. You and Cassie stand next to a sturdy round wooden hut, built in the shade of a couple of palm trees. Two big glasses made from coconut shells sit on the closest table with straws sticking out of them, next to clay plates of grilled fish and fruit.<p>"
            )
            Global.gui.displayImage("premium/Cassie Resort.jpg", "Art by AimlessArt")
            Global.gui.message(
                "<i>\"This was supposed to be perfect,\"</i> Cassie says. <i>\"I think your mana still has weird knock-on effects when it, you know, intersects with mine. Plus, uh, there's the sexual component, which might be why we're both naked...\"</i><p>" +
                        "You'd like to know where you are.<p>" +
                        "<i>\"Oh. Uh... I *think* it's an astral bubble,\"</i> Cassie says. <i>\"Somewhere in the higher realms, anyway.\"</i><p>" +
                        "You stare at her.<p>" +
                        "<i>\"I... kind of made this,\"</i> she says, <i>\"or maybe I just discovered it? I mean, this is pretty much what I wanted it to be, so I assume I had something to do with it, but it's possible that it just shaped itself in line with my intent. But... yeah.\"</i><p>" +
                        "Cassie puts her hands behind her back awkwardly, then realizes she's still naked and half-covers herself. A second later, she thinks it through and lets her hands drop to her sides, which isn't quite comfortable either, but Cassie forces herself to act casual about it.<p>" +
                        "<i>\"Pocket dimension,\"</i> she says. <i>\"So... surprise?\"</i><p>" +
                        "You've got nothing but questions now. If you built a raft out of the hut and paddled in any direction, would you hit a wall or just circle back around to the island? Who caught and grilled these fish? Where's the sunlight coming from?<p>" +
                        "The real questions, the ones you don't articulate, are less comfortable. If Cassie is capable of casually teleporting the two of you to another plane of existence, what else is she capable of? Why have you ever been able to beat her in the Games?<p>" +
                        "How powerful *is* she now?<p>" +
                        "Cassie quietly picks up the grilled fish and eats some of it with her hands. She doesn't have anywhere near as many answers for you as you'd like.<p>" +
                        "<i>\"I just wanted to have a nice tropical mini-vacation,\"</i> she says quietly, like she thinks you're upset with her, and it interrupts your train of thought. You have to apologize then, and belatedly realize you're starving.<p>" +
                        "The fish--salmon?--is excellent, freshly-caught and perfectly seasoned, and the coconuts turn out to contain some kind of tropical juice blend. As you eat, you look over the counter into the hut.<p>" +
                        "It has shelves built into its walls, and those shelves contain a couple of spellbooks, a few fantasy novels, and a *lot* of manga. Two folded deck chairs sit by the hut's entrance, next to a plastic cooler, an oil lamp, and a paper grocery sack. You walk inside and open the latter, to find a couple of bags of your favorite chips; the cooler has a pitcher of iced tea and some soda you like.<p>" +
                        "<i>\"I found this place,\"</i> Cassie says behind you, <i>\"or created it, maybe, and thought it'd be nice to...\"</i> She throws her hands up in the air. <i>\"I don't know. Get away for a while.\"</i><p>" +
                        "She really is acting like you're angry with her. You have to give her a hug then, and she relaxes against you, resting her forehead against your chest.<p>" +
                        "<i>\"I mean, not a long while,\"</i> she says, muffled by her mouth against your skin, <i>\"because time pretty much passes at the same rate here as it does on Earth--\"</i><p>" +
                        "--just that casual, like \"we're not on Earth right now\" is a common everyday thing--<p>" +
                        "--you might be panicking a little, somewhere in the back of your mind--<p>" +
                        "<i>\"--but long enough.\"</i><p>" +
                        "You kiss Cassie on the forehead. She maybe could've not surprised you with this, since it raises a lot of questions, but this isn't a terrible idea.<p>" +
                        "When you tell her that, she brightens, and some of her nervousness goes away. <i>\"Okay,\"</i> she says. <i>\"But... there are a couple of other places like this, and if you ever wanted to visit...?\"</i><p>" +
                        "Let's cross those bridges when we come to them, you say. Maybe make sure we can visit them clothed.<p>" +
                        "<i>\"Uh... yeah.\"</i> Cassie realizes she's naked again, and goes through the same cycle as before: embarrassment, then wary acceptance. She's been naked with you a hundred times by now, under much less private circumstances than these, but she'll never be a nudist.<p>" +
                        "You spend the rest of the afternoon and early evening with her, sitting together in companionable silence reading manga.<p>" +
                        "<i>\"We should get going,\"</i> Cassie murmurs eventually. <i>\"It's about six.\"</i><p>" +
                        "How do you know? you ask. The sun hasn't moved.<p>" +
                        "<i>\"I just do.\"</i> She stands up and puts her book back in order on the shelf. <i>\"...do you want to go for a swim before we leave?\"</i><p>" +
                        "There's really no reason not to.<p>" +
                        "The water is bathtub-warm, of course, and cleaner than any Earthly ocean has been for... centuries, really. You swim out a few yards, looking at the horizon line, lost in thought about it.<p>" +
                        "You can see straight down to the ocean floor. It's just sand. No plants, no coral. No fish. Whatever you just ate wasn't something that came with the ocean. It just raises more questions.<p>" +
                        "Then Cassie comes up behind you and wraps her arms around your waist. You turn in her grasp and decide that your questions, in the long run, either don't matter or don't have answers yet.<p>" +
                        "Cassie kisses you, and that helps cement that decision. You guide her and yourself closer to the sand, and as the kiss deepens, the two of you end up lying down on the waterline with faint waves crashing against you. It's a perfect postcard of a scene, an unhurried private moment you'll remember for the rest of your life.<p>" +
                        "<i>\"I really want you inside me right now,\"</i> Cassie breathes into your ear.<p>" +
                        "You move to comply, rolling over on top of her and between her thighs, but she stops you with a hand against your chest.<p>" +
                        "<i>\"...but I don't know what happens if we have sex here,\"</i> Cassie says.<p>" +
                        "Right. The mana thing. You nod.<p>" +
                        "<i>\"So...\"</i><p>" +
                        "Her aura suddenly flares into the visible range. For a second, she's frightening, her eyes glowing brightly enough to overcome the permanent sunlight of her pocket realm, and you almost scramble backward away from her.<p>" +
                        "Then you're both back in her dorm room, in her bed, shivering with the sudden temperature drop. You glance over, and both your clothes and hers are in piles where you were standing when she teleported you out. Her clock says it's 8:45. You're soaked in salt water and half-covered in white sand.<p>" +
                        "<i>\"...I did not think this through at all, did I?\"</i> Cassie asks sheepishly.<p>" +
                        "You kiss her, then roll off the bed in search of towels. A few minutes later, dry and a little cleaner, you wish her a good night.<p>" +
                        "It's not an entirely comfortable walk back to your room.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.CASSIE, 3)
            Global.gui.message("<b>You've gained Affection with Cassie</b>")
            end()
        }
        if (response.startsWith("Go help Mara")) {
            maraSprite.dress()
            maraSprite.blush = 2
            maraSprite.mood = Emotion.confident
            Global.gui.loadPortrait(player, maraSprite)
            this.doing = plans.MARATESTING
            Global.gui.message(
                "You head over to Jett's workshop to tune up your equipment, and more importantly, spend some time with Mara. "
                        + "She seems hard at work assembling some new device, but immediately greets you excitedly when you arrive.<p>"
                        + "Jett is also working on something in the back of the lab, but he just gives you a cordial nod before returning to whatever "
                        + "he's doing.<p>"
                        + "Mara is the one you're here to see, so you sit down next to her at the workbench and offer to lend to hand. However, she "
                        + "immediately covers up the parts she's working on until you back off dejectedly.<p>"
                        + "<i>\"Not yet!\"</i> Mara stands up some scrap material as in improvised wall, so she can continue to work in secret. "
                        + "<i>\"It's a surprise.\"</i><p>"
                        + "Jett glances up from his own workbench. <i>\"She won't tell me what she's up to either. Do me a favor and yell if you see "
                        + "her pick up anything explosive.\"</i><p>"
                        + "With your offer to help firmly rejected, you decide to make the best of your time and tune up your own equipment. It's weird "
                        + "though. Mara invited you to come collaborate, but you're each working separately.<p>"
                        + "...<p>"
                        + "An hour passes in relative silence, before Mara sidles up to you, excitedly. She whispers to you quietly so that Jett won't "
                        + "notice, but mischief is still clear in her voice and expression.<i>\"It's done. Take a look.\"</i> She opens her hands "
                        + "secretively to show you what she's holding. It's a small plastic ovaloid and an adjustable remote. It looks exactly like a "
                        + "vibrating egg you'd see in a sex shop.<p>"
                        + "<i>\"It basically is, but those eggs don't work like they do in book or movies.\"</i> She's presumably referring to porn. "
                        + "There can't be that many mainstream books and movies with sex toys. <i>\"The motor buzzes really loudly when they're on, "
                        + "so you'd never be able to use them in public without someone noticing. This one is basically silent though.\"</i><p>"
                        + "She hits the switch to demonstrate. The egg vibrates in her palm, but you can barely hear it. Ok, that's kinda cool. She's "
                        + "built a better vibrator. You'd be happy to indulge her with some public naughtiness later, but it still doesn't explain why "
                        + "she's being so secretive.<p>"
                        + "<i>\"Well, it wouldn't be an effective test if the subject knew what to look for.\"</i> She meaningfully nods in Jett's "
                        + "direction while covertly pushing the egg and remote into your hand. <p>"
                        + "<i>\"Put it inside me while you pick this up.\"</i> She casually nudges a screwdriver off the workbench, causing it to fall "
                        + "underneath. You crouch down to retrieve the screwdriver. While your actions are shielded from Jett's line of sight, you "
                        + "quickly slip a hand inside Mara's panties and place the toy at her hot entrance. If you weren't so used to handling her "
                        + "nethers, you probably couldn't do it as smoothly, but you're pretty sure Jett didn't notice.<p>"
                        + "You flick on the remote on the lowest setting to test it out. Mara jumps slightly at the initial sensation, but there's "
                        + "no other indication that she has a vibrator between her legs. She takes a deep breath to compose herself and picks up "
                        + "her multitool off a nearby workbench.<p>"
                        + "<i>\"Let's see if I can hold a conversation and 'finish' without Jett noticing anything.\"</i> She walks over to the "
                        + "workbench where her mentor is sitting. Since you know what to look for, you notice she's walking slightly awkwardly, "
                        + "but she's doing a pretty good job hiding it. You pretend to continue working, so it's not obvious you're watching.<p>"
                        + "<i>\"Hey Jett, I've been seeing unstable power draw from my multitool since I added this new attachment. Can you take a look "
                        + "at it?\"</i> She sets down the device on Jett's workbench and he sets aside his own work to take a look at it. He doesn't "
                        + "show any sign of suspicion. So far so good.<p>"
                        + "<i>\"I assume this is what you've been working so secretly on?\"</i> He leans in to take a closer look. <i>\"This looks "
                        + "like an electron gun. What is it supposed to do?\"</i> He's focused on the hardware now, so you take this opportunity to "
                        + "turn up intensity to about 50%. Mara's breathing catches, but Jett doesn't appear to notice.<p>"
                        + "<i>\"I-It generates a "
                        + "localized stream of charged particles that causes a tingling sensation on skin.\"</i> Mara looks a little flushed now, "
                        + "and she's fidgeting in place. <i>\"I think it could be effective on exposed g-genitals.\"</i> She starts to lean against "
                        + "the workbench. She may be having a bit of trouble standing.<p>"
                        + "Jett glances up from the device <i>\"I sure as hell wouldn't want an electron gun pointed at my junk, especially if you're "
                        + "having power regulation issues.\"</i> He gives her a suspicious look. You can't tell whether it's because he's concerned "
                        + "about the new attachment on Mara's multitool or he noticed her unusual behavior. Her knees have started shaking a little "
                        + "and she's more visibly flushed.<p>"
                        + "This prolonged stimulation seems to be taking its toll on Mara's composure. You should probably do something. You could "
                        + "either turn the vibration down to let her recover, or turn it up to finish her (and the test) faster."
            )
            Global.gui.choose("Turn it down")
            Global.gui.choose("Turn it up")
        }
        if (response.startsWith("Dinner with Jewel")) {
            jewelSprite.dress()
            jewelSprite.blush = 1
            jewelSprite.mood = Emotion.angry
            Global.gui.loadPortrait(player, jewelSprite)
            this.doing = plans.JEWELDINNER
            Global.gui.message(
                "You're still getting dressed to go out when Jewel comes to pick you up. For someone who claims "
                        + "to dislike girly and romantic stuff, she seems pretty gung-ho for this date. You don't have any complaints "
                        + "about a night out with such a beautiful girl, but is there a special occasion you aren't aware of?<p>"
                        + "<i>\"Nothing in particular. I just think you spend so much time cooped up, staring at screens,\"</i> Jewel says. "
                        + "<i>\"It's just not healthy.\"</i><p>"
                        + "She ignores your protests as the two of you walk across town. You work out almost every day, after all, "
                        + "and even when you don't, the Games have got to count as some kind of sustained, extreme cardio.<p>"
                        + "Eventually, you give up. It's just Jewel wanting to boss you around about something, and this is what she "
                        + "picked. It's pointless to argue about it.<p>"
                        + "Plus, maybe you do play a lot of video games, all things considered.<p>"
                        + "The two of you have a nice meal at a place that, before the Games, you might have considered too expensive. "
                        + "It still seems pricey, especially for a college town, but when you take your first bite you can tell where "
                        + "your money's going.<p>"
                        + "Jewel wolfs down her own food--steamed vegetables and grilled chicken--and picks up the check before you can.<p>"
                        + "<i>\"I asked you out,\"</i> Jewel says, <i>\"so I'm paying. You should ask me out sometime, and when you do, "
                        + "you can pay.\"</i><p>"
                        + "You hold the door for her as the two of you leave the restaurant.<p>"
                        + "<i>\"I do need to come back here on my next cheat day. Everything on the dessert menu looks amazing.\"</i><p>"
                        + "Was that a subtle hint?<p>"
                        + "<i>\"It could have been.\"</i><p>"
                        + "As you step onto the sidewalk outside the restaurant, you both hear sudden quick footsteps on the pavement "
                        + "at the same time. You half-turn to look, right as a guy with a snow hat pulled down over his eyes puts his "
                        + "hand on the strap of Jewel's purse.<p>"
                        + "He's hoping that speed and momentum will yank the purse away from her or break the strap, but the moment "
                        + "Jewel feels the tug, she plants one foot on the ground for stability and holds firm. The would-be purse "
                        + "snatcher almost falls over, but then uses both hands to try to yank Jewel's bag away from her.<p>"
                        + "You almost do something, but then you look at Jewel and see how her eyes are narrowed. It stops you in your "
                        + "tracks, because you've seen that look directed at you before, in the Games, right before she did something "
                        + "vicious.<p>"
                        + "She puts most of her upper body behind a short hook that plows into the purse snatcher's diaphragm. You hear "
                        + "most of the wind leave his lungs at once, and he lets go of Jewel's purse. Before he can collapse, Jewel finishes him "
                        + "off with a brutal kick to the groin. You and Jewel take a step backward to let him fall, clutching his injuries, "
                        + "onto the sidewalk.<p>"
                        + "The sparse crowd around you erupts into impressed laughter. You put an arm around Jewel's shoulders and guide "
                        + "her away from the purse snatcher. It's tempting to put a good kick or two into his ribs while he's down, "
                        + "which is part of why you leave him alone.<p>"
                        + "A couple of blocks away, when you're most of the way back to campus, Jewel bursts into giggles, which surprises "
                        + "both you and her. She puts both hands over her mouth and stops, her shoulders shaking.<p>"
                        + "That guy did look really surprised right before he fell over.<p>"
                        + "<i>\"He did, but it's not that. It's the adrenaline rush. I mean, the Games are one thing, but that was almost a "
                        + "real fight. It's not like the schoolyard stuff I did as a kid. For all we know, that guy could have had a knife on him, "
                        + "but I still took him down in a couple seconds.\"</i><p>"
                        + "She pulls her hair out of its typical ponytail and runs both hands through it. Her eyes are bright as she turns to you.<p>"
                        + "<i>\"Would you mind if we made a stop at Suzume's?\"</i> Jewel asks. <i>\"I need to work some of this off.\"</i><p>"
                        + "You go along with it, although not without an inward groan. You think you know where this is going.<p>"
            )
            Global.gui.choose("Next")
        }
        if (response.startsWith("Movie night with Angel")) {
            angelSprite.blush = 2
            Global.gui.message(
                "You open Angel's door to a surprising scene. Angel and her friends are sitting around her room with popcorn "
                        + "and drinks waiting beside them. When Angel invited you to watch a movie with her friends, you assumed it was simply a "
                        + "cover for sex. You didn't think that she meant it. Regardless, relaxing with a movie does sound nice, so you take a "
                        + "position next to Angel and join the girls as they turn off the lights for their private screening of Simian Apocalypse 2: Going Ape.<p>"
                        + "As the movie starts, the mood of the room quickly deflates. The first Simian Apocalypse was a masterstroke of terrible. "
                        + "However, the money they made off of it allowed for better effects and acting in the sequel. The writing wasn't any better, "
                        + "so the movie came out really bland. By the twenty-minute mark of this two-hour movie, no one is still paying attention to "
                        + "the screen. All of the girls except for Angel quickly retreat into their phones. Angel has a different distraction in mind, "
                        + "which is made apparent by her hand running up and down your thigh. She teases closer and closer to your hardening dick. "
                        + "You notice that the other girls aren't paying the two of you any attention. Angel catches your eye and puts a finger to her "
                        + "lips with a soft <i>\"Shh.\"</i> She clearly wants to make a game out of it, trying to see how far you two can go without "
                        + "anyone noticing.<p>"
                        + "Playing along with her, you slide your hand onto her leg and start massaging toward her crotch as well. The petting gets "
                        + "heavier and faster as you two both near your marks. In one swift motion, she undoes the button on your pants and quickly "
                        + "dives in to retrieve your cock from inside. It's a tight squeeze without unzipping, but you know she doesn't want to risk "
                        + "the noise. While she starts rubbing your dick directly, you retaliate by slipping into her pants. She's wearing yoga pants "
                        + "today, so you have much more room to pleasure her than she does with you. Sensing that she's at a disadvantage, Angel takes "
                        + "the risk of unzipping your pants, staying very quiet while doing it. It wasn't enough, though. Out of the corner of your eye, "
                        + "you notice Sarah look over at you. Angel has her back toward her, so she doesn't notice, but Sarah makes eye contact with you "
                        + "for a brief moment. She looks back down at her phone, but you notice her shift her body more to face you two while her hand "
                        + "slowly slides down into her pants. You're already busted, but Angel doesn't seem to notice, so you continue playing with her "
                        + "as she starts jerking you off.<p>"
                        + "Not wanting to fall behind, you move your free hand to Angel's chest, slipping it under her shirt. She's not wearing a bra, "
                        + "so you deftly tease her exposed nipples, eliciting a soft moan. No one seems to have heard it, but Angel starts to panic "
                        + "nonetheless. In an act of desperation, she leans over and takes your dick in her mouth, knocking your hands away from her in "
                        + "the process. She starts slowly to try and keep the noise down, only giving your head some gentle licks at first. In a few minutes, "
                        + "though she's picked up the pace enough to make some sloppy noises. The movie is still playing and largely drowns them out, but "
                        + "you can see Caroline look up from her phone. She gives you a gentle smile, and you try to return it, though you don't do a very "
                        + "good job since you are also trying to hold down your moans. She soon looks away from the two of you again, though not at her phone. "
                        + "Instead, she goes back to looking at the TV screen since it's easier to steal glances at you that way. You can just see her arm moving, "
                        + "but you can't really tell where her hand is.<p>"
                        + "It's hard enough not to cum right away from one of Angel's blowjobs, but now, with two of her friends watching, it feels impossible. "
                        + "You've all but given up on trying to pleasure her back, focusing your effort on staying quiet. Luckily, Angel switches her tactics and "
                        + "pulls back. She glances around the room and completely misses that Sarah and Caroline are watching. Thinking that you two are still "
                        + "unnoticed, she starts to slowly slide her pants down. She only needs to move them enough to expose her pussy. After she does, "
                        + "she quietly shifts herself onto your lap, impaling herself on your dick in the process. Both of you try and fail to stifle your moans. "
                        + "After a second, the light comes back on, with Mei standing in front of you.<p>"
                        + "<i>\"Well, don't stop on my account,\"</i> she says, <i>\"I just wanted to know what those sounds were.\"</i> She sits back down, "
                        + "but doesn't return to her phone, instead sliding her pants down for easy access to her pussy. Caroline is also openly masturbating while "
                        + "watching you, and Sarah has picked up the pace of her rubbing. Angel needs no further incentive to start bucking her hips in your lap. "
                        + "You're both on the edge from your play. You cum first, pumping your load deep into Angel's pussy. Angel continues working her hips, "
                        + "desperately trying to get off. You help her with some gentle nibbles on her ear and some quick twists of her nipples. In no time, "
                        + "she has an intense orgasm, tightening down on your cock hard enough to wring another small orgasm out of you. You're not the only ones "
                        + "who cum, though, as the other girls each cry out with orgasms of their own.<p>"
                        + "After you both settle down for a second, you whisper in Angel's ear that you're sorry the two of you lost your little game. "
                        + "<i>\"What game?\"</i> Angel asks. <i>\"I was just trying to share a nice quiet moment with you without disturbing the others.\"</i> "
                        + "Mei pipes up, <i>\"Don't worry girl, this movie's trash anyway.\"</i> Now that everyone is satisfied, you all decide to finish the movie "
                        + "together. The ending is surprisingly heartfelt, with the two main characters sharing a beautiful moment together."
            )
            Roster.gainAffection(ID.PLAYER, ID.ANGEL, 3)
            Global.gui.message("<b>You've gained Affection with Angel</b>")
            end()
        }
        if (response.startsWith("Go find Yui")) {
            Global.gui.message(
                "You know that Yui has tonight off like you do, so you find her and let her know that you want take her out tonight.  She sounds happy that you have asked her out and you can't help but smile as she enthusiastically agrees.  You tell her to meet you at the bus stop just off campus and then go to make some plans.  At the appointed time she walks up and meets you at the bus stop.  She is dressed in her typical clothing from the games.  <p>" +
                        "<i>\"What are we training in today master?\"</i> she asks.  You realize that you probably should have been a bit clearer on the purpose of tonight.  You explain to her that this is a date night, that you wanted her to take some time off from games and training and enjoy herself.  She smiles at you as you explain this and you think that she must get it.  You finish up by asking her if she would like some time to go back and change.<p>" +
                        "<i>\"Why would I master?\"</i> she asks inquisitively.  You shrug and say that most girls would want to wear something other than their regular clothes on a date.  She shrugs, <i>\"I like these clothes.\"</i>  Just then the bus pulls up and you decide not to push any harder.  You walk on to the bus and have to walk almost to the back before you find a seat for the two of you.  She follows you the whole way and sits down next to you.<p>" +
                        "You ride a little ways in silence, Yui is looking out the window and you are thinking about your plans for tonight.  You realize that she is moving about a bit on the seat, fidgeting.  You turn to her and ask what's wrong.<p>" +
                        "<i>\"I am waiting for your orders master,\"</i> she says, a bit more loudly than you would like.  You quickly look around, worried someone might have interpreted that the wrong way.  When you don't see anyone staring you quietly explain again that this is a date night, not training.  You aren't planning to give her orders and she doesn't need to call you master.  She nods again like she understands but you aren't so sure this time.<p>" +
                        "Finally your stop comes up and the two of you get off the bus.  You have decided to take her down to the local burger joint that is popular with the kids at school first.  The two of you go in and order then grab a table.   You sit there and try to learn more about her as you wait for your food to be ready.  You don't know much about her and have always been curious.  Yui is very guarded though and doesn't really answer any of your questions, looking a bit uncomfortable the whole time.  Finally the food is ready and you go pick it up.  You bring it back to the table and sit and eat, talking about school life which goes a bit better but doesn't last long.  Pretty soon you are sitting in silence, not sure what to say next.  <p>" +
                        "<i>\"I'm sorry master,\"</i> Yui finally says, <i>\"I don't have a lot of experience dating.  My family was very traditional back home.\"</i> You sit there and nod, finally learning something about this mysterious ninja you have been spending time with.  <i>\"My father really didn't like the idea of me dating, and most of the boys that trained with us knew it.  I had a crush on a couple that I wished would ask me out, but they were too scared to ask me.\"</i>  You don't know what to say, but it puts Yui in perspective for you and you feel like you understand her a bit better.<p>" +
                        "After a little while you both finish up your meals and ask Yui if she would clear the table.  She leaps up with a <i>\"Hai Master\"</i> and does it, looking happier than she has since you got here.  You shake your head, she seems thrilled that you finally gave her an order, even though you didn't intend it to be one.  You get up and head to the door where she meets you and you head out into the night.  You take her down the street to a local club, thinking that the two of you might enjoy some dancing.  <p>" +
                        "You both walk inside and are hit with the loud music.  There are already a lot of people here both filling the dance floor and the tables around it. You start toward the dance floor but Yui doesn't follow you, so you grab her hand and drag her out.  She comes with you, but you can feel her reluctance.  You eventually get her out to the edge of the floor and start dancing, but she just stands there like she has no clue what to do.  You try to show her what to do but she seems confused and doesn't really get into it like you do.  Finally you decide that this isn't working and head outside with her in tow.  Once you are outside you walk slowly away from the club toward the park.  You decide that maybe the club was too much and a quiet walk in the park would be better.  You walk through the park in silence, for a bit.<p>" +
                        "<i>\"What's wrong master?\"</i> Yui asks, concerned. You explain that you aren't sure what else they could do, and apologize for ruining her first date.  <i>\"But you haven't.  I am having a great time.\"</i>  She grabs your hand and holds it as you continue to walk.  You smile down at her and tell her that you are glad that you are getting a chance to get away from training and the games to have some fun.  You ask if there is anything specific that she would enjoy doing.<p>" +
                        "<i>\"But master,\"</i> Yui says, <i>\"I think that you giving me orders and helping me train to be better in the games is fun.\"</i>  You look at her as you walk through the entrance to the park.  <i>\"Training at my father's dojo always felt like work, but it is less structured and more fun.\"</i>  She is smiling as she says this and you believe it is true.  You tell her you still have concerns about all of it being too much and eventually causing too much stress.  She looks at you thoughtfully for a minute and nods.<p>" +
                        "She shakes her head, <i>\"I don't think that will happen, but I will always agree to another date if it makes you feel better.\"</i> You smile and nod as you lead her over to a bench and sit down.  She sits down next to you and puts her head on your shoulder.  <i>\"You asked what I want to do, maybe we can spend some time just like this?\"</i> she suggests.  <i>\"Just master and kunoichi, enjoying being together\"</i>  You smile at her and agree that it sounds like a good way to spend an evening.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.YUI, 3)
            Global.gui.message("<b>You've gained Affection with Yui</b>")
            end()
        }
        if (response.startsWith("Fae party")) {
            aishaSprite.dress()
            aishaSprite.mood = Emotion.confident
            aishaSprite.blush = 1
            Global.gui.message(
                "<i>\"Don't accept any gifts,\"</i> Aisha says. <i>\"Seriously. Not even a glass of water. Pay for everything, one way or the other.\"</i><p> " +
                        "You nod.<p>" +
                        "<i>\"Don't go anywhere but where they expressly tell you it's safe to go. There's going to be a very small part of the glade that's set up for us. Anywhere else, and who knows what can happen.\"</i><p> " +
                        "You nod.<p>" +
                        "<i>\"Don't eat or drink anything that you don't recognize,\"</i> Aisha says, <i>\"or can't identify, or get a weird vague answer if you ask what it is. In fact, try not to eat or drink anything. There *might* be food for us, and the water's *usually* okay, but best not to risk it.\"</i><p> " +
                        "You nod, again. It feels like a winning strategy.<p> " +
                        "Aisha looks you over, then pats you down for the fourth time, just to make sure you aren't carrying any metal. You aren't. At her instruction, your keys and phone are in her car, you didn't bring any coins, and you don't have any zippers or snaps on your clothes. If she wasn't so self-assured, it'd strike you as paranoid.<p> " +
                        "Not that she's exactly wrong, of course. You've heard this story once or twice before, in cartoons and movies and children's books. The moment you think you know better than the expert, you end up turned to stone or stuck in another dimension or punted out 300 years later. Warnings exist for a reason.<p> " +
                        "You'd always assumed, on some level, that the point of those stories was that moral. Now, the moral seems to be the story's silver lining. Somewhere along the line, someone just like you fucked up and became a cautionary tale.<p> " +
                        "This isn't what you thought you'd get when you decided to see what Aisha was up to. Then again, you weren't real sure what you'd get at all.<p> " +
                        "Tonight's a faeryland holiday, although that doesn't mean much. According to Aisha, any half-literate fae with fifteen minutes' warning can figure out a valid reason why any point on the calendar is a fae holiday, suitable for orgiastic celebration. That's why she's dragged you halfway out of town to the hardest-to-reach part of the nearest nature preserve, to a quiet clearing marked by a ring of odd mushrooms.<p>" +
                        "<i>\"Repeat what I warned you about,\"</i> Aisha says.<p> " +
                        "You do, dutifully, but you've got to ask: if there's so much danger here, why go at all? What's to gain, if one wrong move means disaster?<p>" +
                        "<i>\"A couple of reasons,\"</i> Aisha says. <i>\"If we're going to drag a couple of them across the barrier every so often to help us out, it's considered rude if we don't show up for a party now and again. I should've brought you along a while ago.\"</i><p> " +
                        "You nod.<p>" +
                        "<i>\"It's also possible, if unlikely, that we'll run into someone else who's useful to know. Xaldin's just one contact in the fae realm. You could have a rich life just by keeping him sweet, but there are hundreds more fae who'd come running for a taste of human mana, all of 'em with useful powers of their own.\"</i><p> " +
                        "Makes sense.<p>" +
                        "<i>\"And,\"</i> Aisha says, <i>\"gotta admit, it's usually a good time for its own sake. You ain't been to a party 'til you been to a party thrown by somebody who's been honing the art thereof for a couple thousand years.\"</i><p> " +
                        "As you think that over, dusk continues to deepen into night. At first, it was easy to brush off the luminescence above the mushroom ring as an optical illusion, but the darker it gets, the more obvious it becomes. You can feel a faint pull from it if you focus, as the ring draws on ambient mana to activate itself.<p>" +
                        "<i>\"Showtime,\"</i> Aisha says. <i>\"Last chance to back out.\"</i><p> " +
                        "Nah. You shrug. You came this far, and now you're curious.<p>" +
                        "<i>\"Hell of an epitaph, " + player.name + ".\"</i><p> " +
                        "Her smile makes you think she's kidding. It might be wishful thinking.<p> " +
                        "Side by side with Aisha, you walk hesitantly forward towards the light in the clearing. As you approach, motes of it dance up to meet you, whirling around you with something you could easily mistake for enthusiasm, and you become so fascinated with their motion, so intent upon trying to figure out if the motes are somehow alive, that you almost miss the smooth gliding sensation that accompanies your transition into somewhere else.<p> " +
                        "It feels like being pulled through a wall made of warm honey. Every inch of exposed skin is suddenly an erogenous zone, which leaves you breathing hard and barely standing. When you regain control of yourself, you're drenched in sweat, on the verge of orgasm, and taking deep breaths of air like wine.<p> " +
                        "Described strictly with sight, it's an old-growth forest, one that no man has ever dared to touch. The fae realm is as much a feeling as a place, though. Everything is right and correct here, just as it was always meant to be; pain does not exist, unhappiness seems impossible, and euphoria is the law. You giggle like a crazy person without meaning to.<p> " +
                        "Aisha glances at you with a soft smile. <i>\"Yeah. I know.\"</i><p> " +
                        "She puts a hand on your back, gentle and firm, which serves as an anchor. You end up pulling her close, breathing in the smell of her hair, because she's something real that you can focus on. Left by yourself, if you'd just stumbled through that faery ring one night, you might have stood here just taking it all in, staring, existing in the now, until starvation or interference forced you to stop.<p>" +
                        "<i>\"You good?\"</i> Aisha asks.<p> " +
                        "You close your eyes, take one last breath of her, and move a step away. You're as close to coping as you're going to get.<p>" +
                        "<i>\"Keep that dream alive,\"</i> Aisha says. <i>\"It gets crazier.\"</i><p> " +
                        "What?<p> " +
                        "Everyone is beautiful and perfect.<p> " +
                        "None of them need clothes. What they do wear is ornamentation, or the occasional belt or pouch. They decorate themselves with dust, leaves, water, or smoke. It's impossible to know where to look.<p> " +
                        "From the moment you walk into the celebration, it feels like you're close to blackout drunk. It's difficult to perceive everything that's going on, or remember it after it's happened. Aisha stays with you, a protective stabilizing influence, and at least half a dozen times, steps in to quietly turn something down on your behalf.<p> " +
                        "It's a real concern. You get offered a lot tonight, by a lot of different things and people.<p> " +
                        "A flickering torch speaks at one point, asking to trade your first memory of your mother for a ring that would grant you absolute success in a single endeavor. A pint of your blood, or a pound of your hair, would get you ten times as much in solid gold, or so a mouse tries to claim. A year of servitude at the feet of a woman dressed in a handful of diamonds, whose skin is too gray and limbs too long to be at all mistakable for human, and you'd go back to Earth with the potential to become a king.<p> " +
                        "Aisha intercedes and denies them all, when you're too drunk on being here to make a proper decision for yourself.<p> " +
                        "There are easier exchanges, and 'exchange' is definitely the right word. You walked into a party and ended up at a business meeting. Nothing here is free. Everything costs.<p> " +
                        "Fortunately, you're also the single most valuable commodity here. Between you and Aisha, you collect any number of small favors and possible connections, just for as little as a kiss, or an amount of mana so small that you don't really feel it leave. Some just want to 'taste' you, which turns out to be literal, and you spend time with fae you can't identify, blurs of wind or green girls with hummingbird wings, as they gently lick along the back of your ear or the side of your neck. A woman who looks as if she might have been carved from wood gives you a mug full of sweet water in exchange for the chance to run her hands through your hair.<p> " +
                        "You don't give anything serious away, or anything you recognize as serious at the time. Just the same, you stumble back out into the real world eventually, feeling drained and exhausted. Aisha's a few steps behind you.<p> " +
                        "Both of you lost your clothes at some point without noticing. Aisha looks as if she's spent a few hours rolling around under a bush, with an intricate lattice of cobwebs and twigs spun with a purpose through her hair. Now that you're outside the fae realm, it immediately begins to fall prey to actual physics, falling apart with every stray motion she takes.<p> " +
                        "Back in her car, you pick up your phone and discover you've been gone for under five minutes.<p>" +
                        "<i>\"I think what I learned tonight,\" Aisha says, as she gets dressed, <i>\"is that you don't get to do this without me.\"</i><p> " +
                        "You pause with one hand on your backup clothes. It's hard to remember the night clearly--it's like you spent the entire time in a strobe light, with no gaps between individual experiences--but you don't feel like you did that poorly.<p>" +
                        "<i>\"You didn't,\"</i> Aisha says. <i>\"But you're valuable. The next time you go in there, they're going to try harder to get you.\"</i><p> " +
                        "Oh.<p>" +
                        "<i>\"Might be worth trying to figure a few things out,\"</i> she says, and pulls on a pair of yoga pants. <i>\"We knew you were gifted. That's not too unusual. But how they reacted to you indicates there's something up.\"</i><p> " +
                        "Well, that's... a little alarming.<p>" +
                        "<i>\"Doesn't have to be. Could just be the Games, somehow. Or you've got a few drops of fae blood in you, from a long time back. Just food for thought, huh?\"</i><p> " +
                        "You nod. Again. It's starting to feel like a habit.<p>" +
                        "<i>\"Anyway,\"</i> Aisha says. <i>\"You hungry?\"</i><p> " +
                        "Starved, actually. You spend the rest of the night with Aisha, eating like starving wolves at a nearby diner, talking about nothing in particular.<p> " +
                        "The real world seems unusually drab, right now, but at the same time, unusually safe. It's an ugly place, but at least nothing in it is actively trying to trick you into some form of slavery."
            )
            Roster.gainAffection(ID.PLAYER, ID.AISHA, 3)
            Global.gui.message("<b>You've gained Affection with Aisha</b>")
            Global.gui.loadPortrait(player, aishaSprite)
            end()
        }
        if (response.startsWith("Meet up with Kat")) {
            katSprite.undress()
            katSprite.blush = 1
            katSprite.mood = Emotion.confident
            Global.gui.message(
                "You go outside and wait for Kat, rubbing your hands and breathing into them to warm them up. It crosses your mind that you don't know what Kat does when she's not in the Games. You wonder what she has in mind as she doesn't seem the type to want to have a romp outside. <p>" +
                        "<i>\"Sorry about the wait!\"</i> You turn to your side and notice Kat mid pounce as she glomps you. She is wearing a skirt and no hoodie, meaning her tail and cat ears are out in the open. You panic at the sight and you swerves back and forth for witnesses but you let out a tense breath and relax when you hear Kat giggling at you. Right, if Kat isn't worried then there is no reason for you to be. Why isn't she worried? <p>" +
                        "<i>\"Silly " + player.name + ", I do this all the time.\"</i> <p>" +
                        "Really? <p>" +
                        "<i>\"Yup, come on!\"</i> Kat jumps off and grabs you by the arm. You can't help but smile at Kat's exuberance and play along as her arm candy as you walk around campus. <p>" +
                        "Maybe it's due to the fact that you've been juggling schoolwork, the girls, and the Games, but you've never really had a look around campus. Sure you know how they look inside due to coursework, visiting the girls, or fighting in the Games but you've never paid attention to the outside. Kat acts as a guide as she hugs your arm, keeping her head on your shoulder. Every now and then people stare, more so Kat and sometimes you, but for the most part everyone seems content to ignore the two of you. <p>" +
                        "<i>\"See? Nyo one is going to actually think a real cat girl is just wandering around,\"</i> She giggles once again. A few months ago you'd also assume it was some enthusiastic cosplayer or some kinky roleplaying. It makes you wonder how many other supernatural things are just walking out in plain sight. <p>" +
                        "You look at Kat to see her smiling happily. She seems unusually calm and relaxed right now. <p>" +
                        "<i>\"Something wrong?\"</i> She asks as she catches you staring. <p>" +
                        "You shake your head and ask if she has any plans right now. <p>" +
                        "<i>\"Nope. Do you?\"</i> <p>" +
                        "No, hence the asking. Is this actually just a walk? <p>" +
                        "<i>\"Is that a problem?\"</i> She suddenly looks nervous. You stop for a second and give Kat a small kiss on the lips, letting her know this doesn't bother you. A simple walk through the night with a beautiful girl is doing wonders for your stress and ego. <p>" +
                        "<i>\"Really? That's good. I love walking around at night,\"</i> You glance at Kat as two of you continue walking, <i>\"I was super shy when I first came here. Could barely look at people without feeling anxious. I used to take walks to just clear my head. Now that I've fused with my spirit, I can't really walk outside during the day or else people will ask questions. I love Mel and Emma but every time they cover for me I feel restrained. They fuss over everything.\"</i> <p>" +
                        "That sounds exhausting. Is that the reason she goes out on walks at night now? <p>" +
                        "<i>\"Yup,\"</i> Kat shivers as a breeze passes and nuzzles in a bit tighter, <i>\"Ever since the fusion, I've preferred the night. Cat ears might be weird in the day time but at night no one cares. In a match I can work myself to sleep but when I'm not scheduled I just take a walk around like this.\"</i> <p>" +
                        "That sounds kind of reckless. Kat pouts, <i>\"Hey, I'm your senior. I know how to take care of myself. I'm fast, strong, nyand I can hear from really far away. Anyone try to corner me and they'll go flying.\"</i> <p>" +
                        "Kat punches the air in front of her, letting out a nya as a punching noise. You laugh at her antics as you hug her. She hums in annoyance as you pull her closer to you but she purrs in pleasure as you rub her tummy. <p>" +
                        "<i>\"Come on,\"</i> Kat says as she suddenly breaks out of your grip to grab your hand, <i>\"let me show you my secret place.\"</i> <p>" +
                        "You waggle your eyebrows but Kat misses it as she enthusiastically pulls you along. She leads you to the campus garden towards an area with some trees. To your surprise, Kat scratches at some tree bark and it pops right out to reveal a hole with a blanket inside it. Your eyebrows raise up as she places the blanket on the ground and readily falls down on top of it. <p>" +
                        "<i>\"Oof!\"</i> She lands on the blanket, the leaves and soft dirt acting as padding so she didn't hurt herself. She turns on her stomach to lay down on her back. Her tail pats at the ground next to <p>" +
                        "her. You take the open invitation for what it is and join her. Kat settles into the crook of your shoulder and you wrap your arm on her hip. <p>" +
                        "<i>\"This tree has a natural open spot in it,\"</i> Kat quietly explains as if to make sure her secret can't be revealed, <i>\"Campus security usually don't come here. Sometimes I like to look at the stars and take a nap.\"</i> <p>" +
                        "This is a nice spot. It's in an open area of the gardens yet mostly hidden from view if you weren't looking for it actively. Even if someone was to come here during the day, the trees pretty much look alike so no one could tell which one held her stuff. The chances of someone accidently finding this place is slim. <p>" +
                        "<i>\"Isn't it pretty?\"</i> Kat looks up and you follow suit. Without a single cloud in the sky, the stars are out in full view and a breath escapes you. The bright stars and the moon lighting up the night, the lull of the crickets, the gentle caress of the wind, and Kat's warm body on yours, you feel completely at ease. You nearly fall asleep before Kat speaks up, <i>\"When I don't have to fight in the Games, I just like sitting here and letting my worries go away. This was supposed to be my secret place that no one should know. Now there's two.\"</i> <p>" +
                        "Touched at the sentiment, you lean over and kiss Kat softly on the lips. Kat takes a moment before deciding she wants none of that and leaps at you. Somewhat shocked and still tired, you reciprocate the best you can as Kat sits on top of you now. <p>" +
                        "<i>\"You said you were tired " + player.name + ",\"</i> Kat says as her hands quickly work on taking your both your clothes off, <i>\"I'll take care of this.\"</i> <p>" +
                        "Soon enough Kat's on top of you completely naked and you sit stunned at the sight. The moonlight bathes her in white light, her naturally light skin practically glowing silver as the shadows on her face intensify. Her hands push you down, gently caressing your body, and when she purrs with a wicked smile you swear her eyes turn gold for a second. You wonder if this is what the last thing prey look at before their predator eats them whole. <p>" +
                        "You snap out of it when you feel her tail wrap around your dick and lead it to her pussy. Kat meows in pleasure, slamming her hips on your dick. You can't help but reciprocate, thrusting up into her to match her frantic pace. Her hands move from your chest to your shoulder, refusing to let go as she peppers your body with kisses. In the dead of night, your lovemaking echoes like a siren, constant slaps, bangs, and rattled breaths echoing into the night. <p>" +
                        "The small rational part of your brain knows you should calm down but the bigger part of you doesn't care. You're sure if your loud enough someone is going to think there's a cat out there that needs help, but this cat is anything but helpless. <p>" +
                        "Kat increases her pace, her insides doing their best to squeeze the life out of you as she kisses you. You hold as best as you can, trying to make sure Kat cums first before you let out your own. In the end, the two of you cum at the same time, embracing each other with your lips locked. The energy leaves her and she collapses on top of you with heavy breaths once you disconnect the kiss. <p>" +
                        "<i>\"I owe a lot to the Games,\"</i> Kat murmurs into your chest as you absentmindedly played with Kat's hair and ears, <i>\"I was always the weak one even before the Games. When I joined the Games I was destroyed my first match.\"</i> <p>" +
                        "You vaguely recall Aesop telling you that when you had first heard of Kat, <i>\"I didn't want to keep feeling weak. So I kept trying and trying. And when I got the spirit, I became good, better. And now I'm here, under the stars with the boy I like trapped under me. Old me would have had a heart attack.\"</i> <p>" +
                        "You chuckle at Kat's words, closing your eyes to Kat's gentle breathing and her heat. Tonight is a good night. <p>" +
                        "--- You wake up with the sunlight stinging your eyes. You grumble and try to ignore it before your eyes practically burst out of your skull as you recall the last thing you did. You see the same old ceiling of your room and calm down immediately. A dream? <p>" +
                        "You hear a mumble and realize someone is on your arm. You look to find Kat in her underwear next to you, her clothes neatly piled on your desk. The sunlight is hitting her eyes and she rolls into your side to block it. You see your clothes from last night also neatly packed next to hers and you realize Kat must have brought you back to the dorms after last night. You glance at the time and see it's still early so you turn to hug Kat and to help block the sunlight away from her. <p>" +
                        "You quietly thank her and drift back to sleep as she hums at your words. <p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.KAT, 3)
            Global.gui.message("<b>You've gained Affection with Kat</b>")
            Global.gui.loadPortrait(player, katSprite)
            end()
        } else if (response.startsWith("Reyka booty call")) {
            reykaSprite.undress()
            reykaSprite.mood = Emotion.horny
            reykaSprite.blush = 2
            Global.gui.message(
                "It's only a matter of minutes before you're standing outside the Succubus' door. You knock, and receive a muffled <i>\"Come in!\"</i> from within.<p>" +
                        "Pushing the door open and stepping inside, you find a strange sight before you. Reyka is standing up in the middle of the room, wearing nothing but a revealing black bra and panties. Her succubi features seem to be hidden as well. You can see her rocking her hips from side to side, and hear her speak in a sultry tone to some unseen listener.<p>" +
                        "After a moment, she turns towards you and smiles. <i>\"" + player.name + ", I'm so glad you could make it. The show was starting to get dull without any company…\"</i><p>" +
                        "Confused, you ask her what she's talking about. She raises an eyebrow, and gestures to her computer. Glancing over at it, you realize the camera light is active, and that the two of you are being displayed.<p>" +
                        "<i>\"I have a cam show, and invited you over to be my special guest.\"</i> She suddenly pulls you into a hug, and her voice drops to a whisper. <i>\"I can't use any of my powers on the show for obvious reasons, but I'll make sure to make it worth your while if you help out.\"</i> <p>" +
                        "Even if you wanted to say no, Reyka's sudden closeness causes your erection to stir and stiffen; a response the succubus interprets as <i>\"yes\"</i>. <p>" +
                        "She pushes you back onto the bed, a predatory smile on her face. Positioning herself off to the side, she makes quick work of your clothes, and tosses them off-camera.<p>" +
                        "Looking away from Reyka for a moment, you see your own naked body on the screen, and the chat's commentary. Even with all your experience from the Games, it feels a bit strange knowing hundreds of people are watching the two of you.<p>" +
                        "The succubus doesn't let you think about it for too long though, and she begins to deftly stroke your hardened member with her soft palms. Flashing you a brief smile, she slowly slides her remaining clothing off, revealing her alluring body in full.<p>" +
                        "<i>\"Alright " + player.name + ", it's time to begin. You can take the lead, but if you fall behind I'll spend the rest of the stream milking you dry~\"</i> <p>" +
                        "You swallow, calming your racing heartbeat before starting. You have Reyka get on her hands and knees facing the camera, and position yourself behind her. Hoping to catch her off guard, you plunge your member into her entrance in a single thrust.<p>" +
                        "This seems to have the intended effect, making her audibly moan. She's already soaked, no doubt from the earlier part of the show. Grabbing her hips, you begin to hammer away at her slit.<p>" +
                        "Each movement draws another moan from the succubus, though you suspect some of it is just for the show. Either way, you continue to pierce deeper into her folds with reckless abandon.<p>" +
                        "You're still going strong two minutes later, and strangely don't feel any closer to orgasm. Reyka's moans have decreased, and she seems to be reading the chat. You take a look as well, and are surprised to find the entire chat filled with messages with the same text— <i>\"Do it!\"</i> <p>" +
                        "You ask what the chat is talking about, only to receive a sinister chuckle from Reyka. <i>\"Well, they probably want me to do… this~\"</i> The sensation is sudden, and irresistible. Electric pleasure arcs through your body, just as her walls clamp down around you. You cum near instantly, flooding her with thick seed.<p>" +
                        "In your moment of weakness, Reyka pushes backwards and sends you toppling backwards onto the bed. She wastes no time in mounting you properly, still looking at the camera from the reverse cowgirl position.<p>" +
                        "She looks back at you for a moment, a slightly kinder smile dawning on her face. <i>\"You're doing great " + player.name + ", sorry about that. A lot of the viewers lean towards the submissive side, so I'll be taking over from here.\"</i><p>" +
                        "As she says this, her hips begin to move once more. Starting out slowly, the main stimulation comes from her swaying from side to side. Now you're the one moaning for the camera, and you weakly grasp at her waist.<p>" +
                        "Her pace quickens, and the swaying shifts to a more aggressive bouncing. She grabs onto your legs for extra leverage, and the sounds of your bodies slapping against each other echo through the room.<p>" +
                        "Despite having cum just moments before, Reyka's rough motions quickly build you towards another height. You feebly try to hold back the impending orgasm, but your resistance is easily shattered by her greedy hole.<p>" +
                        "You cum once again, filling her with a second helping of seed. <i>\"Oh, that's another good load. I wonder how much more you have stored in these balls?\"</i> She doesn't stop moving for even a moment, and your vision begins to darken as she continues to bounce…<p>" +
                        "<i>\"..." + player.name + ". Hey " + player.name + ", wake up~\"</i> Your eyes slowly flutter open, to see Reyka's face positioned above you. You're fully clothed, and your head rests on the Succubus's lap. She smiles as your eyes open, a hint of smugness clearly visible.<p>" +
                        "<i>\"You must have really enjoyed that, to have passed out halfway through an orgasm. And even then, you just kept on giving… so thanks a lot for helping.\"</i> <p>" +
                        "You slowly rise up, and tell her that it was no trouble. Getting back to your room is going to be a bit rough, but you have no doubt you'll be sleeping like a baby tonight.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 3)
            Global.gui.message("<b>You've gained Affection with Reyka</b>")
            Global.gui.loadPortrait(player, reykaSprite)
            end()
        } else if (response.contains("Valerie and Justine")) {
            valerieSprite.dress()
            valerieSprite.mood = Emotion.confident

            Global.gui.message(
                "---<p>" +
                        "You immediately feel a bit out-of-place as you step into the restaurant. It's a good thing Valerie told you to dress up, as you probably wouldn't have been let through the door if you hadn't put on your best clothes tonight. Thankfully you are indeed let through the door, and you're guided to a part of the restaurant sectioned off by a curtain. On the other side of it, you find Valerie and Justine already seated at a small table, and you're left alone for a moment of privacy with them.<p>" +
                        "You share an awkward smile with Justine, silently confirming that she feels equally out-of-place here. You lean in to give her a quick kiss, then do the same for Valerie before taking your seat.<p>" +
                        "<i>\"I hope you don't mind,\"</i> Valerie says, <i>\"but I paid a bit extra so that we could have some privacy tonight.\"</i><p>" +
                        "You certainly don't mind the thought, but it does make you feel a bit awkward to be treated like this. Valerie's probably making about as much money from the Games as you are though, so affording this isn't likely much of an issue for her. You smile at Valerie and thank her for this treat, making a mental note that you'll have to thank her properly after you return to the campus this evening.<p>" +
                        "<i>\"You know, Val,\"</i> Justine says, glancing over at you for a moment, then back to Valerie, <i>\"In trying to figure you out, I came to the conclusion that you must be loaded, whatever your exact deal is, though you were trying not to make a big thing of it. I guess what I'm saying is... This feels like a bit of a big thing, and I'm not quite sure what that means.\"</i><p>" +
                        "<i>\"Oh,\"</i> Valerie says. She blinks a couple times, then turns her gaze away from you and Justine. <i>\"Um, my apologies. I suppose I'm still having trouble calibrating my expenditures to what might be appropriate here. I was hoping that this would simply come across as a nice treat, but it would seem that I might have overshot the mark here.\"</i><p>" +
                        "Justine's eyes widen. <i>\"Oh, no!\"</i> She says. She reaches a hand across the table, cupping Valerie's hand. <i>\"I-I'm sorry, Val. I didn't mean it like that. You didn't do anything wrong. I was just... kind of wondering if this meant you were ready to open up to us a bit more about who you are. I mean...\"</i> Justine glances over at you, then back at Valerie. After a moment, she shakes her head. <i>\"Never mind. I promised I wouldn't press you, so I'll shut up now.\"</i><p>" +
                        "Valerie quickly shakes her head. She turns her hand around and intertwines her fingers with Justine's, then glances at you for a moment. Taking her cue, you reach over to take Justine's other hand in yours. <i>\"While I wasn't planning on anything of the sort tonight, I suppose that it would be a good idea to open up to you a bit more. I don't know if I'm ready to get into a discussion about my past, but there is one other thing that I think " + player.name + " and I should fill you in on, which does help with being able to afford this meal.\"</i> Valerie looks from Justine to you, and she says, <i>\"Do you think it will be alright if we tell Justine about what we do most other nights?\"<p>" +
                        "You raise an eyebrow at this. This certainly wasn't how you were expecting this evening to go. But since it's come up, it probably is well past the time that Justine deserves to be filled in on the Games, as long as she promises to keep it a secret. It's not the best-kept secret at the university, but it is still supposed to be a secret, so you'd prefer she not talk to anyone else about it.<p>" +
                        "Justine furrows her brow, looking between the two of you. <i>\"...Okay,\"</i> she says. <i>\"You have my word. So dish: What have you two been up to?\"<p>" +
                        "You and Valerie spend most of dinner explaining the Games to Justine, being careful not to say anything too explicit while a waiter comes by. Thankfully, Justine isn't at all mad that the two of you have been having sex with other people as part of this. She just seems to be frustrated that there are too many hurdles to entry for her to join in, but some promises of playing some unofficial games with her are enough to satisfy her.<p>" +
                        "<i>\"Oh!\"</i> Justine says as another question occurs to her, looking away from her dinner and pointing her fork at you. <i>\"You're not getting any of these other girls pregnant, are you?\"<p>" +
                        "You laugh and shake your head, explaining to Justine that all the female competitors are required to be on birth control during the Games to make sure that isn't an issue.<p>" +
                        "<i>\"Mm, well...\"</i> Valerie says, looking between you and Justine for a moment, but she turns her gaze back to her dinner without saying another word.<p>" +
                        "You and Justine share a glance at this, and you look back at Valerie, trying to parse why she might have said this. Valerie is on birth control, right? You can't recall her ever explicitly confirming that, but she wouldn't be allowed to participate in the Games if she weren't.<p>" +
                        "Valerie remains silent, picking at her food but not actually eating any of it. Finally, she lets out a sigh. <i>\"As far as I know,\"</i> she says, still not looking up at you, <i>\"that's the case for everyone else. I have a waiver though.\"</i><p>" +
                        "You blink at Valerie, your eyes widening. Have you really been risking pregnancy all this time? How was that even allowed?<p>" +
                        "Valerie shakes her head. She looks at you and holds your gaze, silently pleading for you not to get mad. <i>\"No, it's not like that. I'm...\"</i> Valerie glances to Justine for a moment, then closes her eyes. <i>\"Might I ask the two of you to keep a secret for me? This is one that even my family isn't aware of, but...\"</i> Valerie takes a deep breath. <i>\"I want to tell you this.\"</i><p>" +
                        "You reach out to Valerie, taking her hand in yours and giving it a squeeze in support. You see Justine moving her chair closer to Valerie, reaching out under the table, probably to place a hand on Valerie's knee. <i>\"Of course, Val. I promise not to tell anyone,\"</i> she says. You nod, agreeing as well.<p>" +
                        "<i>\"Very well,\"</i> Valerie says. She takes another deep breath, then takes a sip of her drink. Finally, she looks up again, her gaze switching between you and Justine. <i>\"I had to have surgery a year or two ago, and a side-effect of it was rendering me infertile. I've never told anyone about this. It's very important for my father to one day have grandchildren... and I very much wanted children someday as well, so this isn't a conversation I've been able to bring myself to have with him yet. You two are... really the first people I've told about this.\"</i><p>" +
                        "This is the first time Valerie's ever mentioned her father, but now isn't the time to ask for details about him. You can see her trembling a bit as she says this, so you move around the table and wrap your arms around her. This is obviously still a bit of a raw wound, and you don't really have much that you can say to help. All you can do is be here for Valerie right now.<p>" +
                        "<i>\"I can...\"</i> Justine says. <i>\"I mean...\"</i> You look over at Justine, seeing her squeeze her eyes shut. She bites her lips for a moment, and then finally manages to say. <i>\"Um, do you want to go back to campus? I don't know what I can say, but...\"</i> Justine finally lets out a sigh, smiling apologetically at Valerie.<p>" +
                        "Valerie shakes her head. She places a hand on your shoulder, gently letting you know that you don't need to keep hugging her right now. <i>\"It's alright,\"</i> Valerie says as you pull away and return to your chair. <i>\"This is something I've already had a lot of time to process, and I don't want to deprive the two of you of dessert.\"</i><p>" +
                        "You ask Valerie if she's sure, but she's quite insistent. You change the subject, and the three of you manage to enjoy the rest of your date well enough, though a certain cloud seems to hang over the conversation.<p>" +
                        "When you return to campus after a very tasty dessert, you follow Valerie and Justine to their dorm room. Valerie pulls off to use the bathroom as soon as you arrive, leaving you alone with Justine for a bit.<p>" +
                        "Justine glances at the bathroom door, then pulls you away from it, to the far corner of the living room. <i>\"Um, " + player.name + ",\"</i> she says, keeping her voice low. <i>\"I wanted to say this to Val earlier, but I don't know if it's the right time. When Val mentioned about being infertile but still wanting kids... for some reason the first thing that popped into my head was to... well, offer myself for that. I mean, if the three of us stay together...\"</i> Justine trails off, looking toward the door to the bathroom, then lets out a chuckle. <i>\"I guess just saying that out loud makes me realize how silly it is. We're just three kids playing around here. Who the fuck am I to offer something like that?\"<p>" +
                        "You reach out and give Justine a hug, pulling her in toward you. You tell her that if she's thinking of something like that, then this obviously something more than just kids playing around. It probably isn't the best time to say that to Valerie, as it's still to early to know if you can make this work in the long run, but it does at least say something about how deep Justine's feelings go.<p>" +
                        "<i>\"Heh... Yeah, I guess you're right,\"</i> Justine says. She seems as if she's about to say something further, but the bathroom door opens and she pulls away from you. She dashes to Valerie and leans in, surprising her roommate with a kiss to her lips. Valerie is caught off-guard by this, but she soon melts into it, and you can spot her placing her hands on Justine's sides as they share a moment together.<p>" +
                        "<i>\"Mm...\"</i> Valerie hums a bit as she pulls back from the kiss. <i>\"Can I expect such treatment after every time I use the wash?\"<p>" +
                        "<i>\"If you want,\"</i> Justine says. She leans in and plants a quick kiss on Valerie's cheek. <i>\"I love ya, Val. You know that, right?\"<p>" +
                        "Valerie smiles at this. You spot the telltale signs of her legs melting beneath her, and you rush in to embrace her and keep her from falling. Before she can reply, you make sure to repeat these words, making sure she knows that you feel the same way.<p>" +
                        "Valerie doesn't seem to have it in her to reply right now. <i>\"Thank you... both of you,\"</i> she says. She removes a hand from Justine's side and wraps it around your back.<p>" +
                        "You lean in to place a soft kiss on Valerie's lips, lingering just as long as you think you can before you risk making her melt even further. When you pull back, you ask Valerie what she thinks of cuddling up on the couch together for a bit.<p>" +
                        "<i>\"I think I would be truly blessed,\"</i> Valerie says, beaming at both you and Justine.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.VALERIE, 3)
            Global.gui.message("<b>You've gained Affection with Valerie</b>")
            Roster.gainAffection(ID.PLAYER, ID.JUSTINE, 3)
            Global.gui.message("<b>You've gained Affection with Justine</b>")
            Global.gui.loadPortrait(player, valerieSprite)
            end()
        } else if (response.contains("Dinner with Samantha")) {
            samSprite.dress()
            samSprite.mood = Emotion.confident
            Global.gui.message(
                "Wanting to put your night off to good use, and knowing that Samantha is off tonight as well, you decide to treat her to a night out. To that end, you find yourself looking up nearby restaurants on your computer. Suspecting that Samantha wouldn't be the type to enjoy just any random fare, you specifically search for some of the higher end options in the vicinity. After a bit of deliberation, you settle on an Italian restaurant that isn't too far from campus.<p>" +
                        "You give the restaurant a call to ask if they have room for walk-ins. They assure you that they do, and then you call up Samantha. She answers after only a few rings. <i>\"Good evening, Love. Are you calling on your night off from sex to ask for more sex?\"</i><p>" +
                        "You can hear the smirk in her voice and can't stop one from forming on your own face. With some pride, you correct her misconception and inform her that you are calling to invite her out to dinner. There's a moment's pause as she processes the revelation. <i>\"Really? Well, what did you have in mind to eat?\"</i> You tell her about the Italian Restaurant you checked with, and hear a small <i>\"hmm\"</i> from Samantha. <p>" +
                        "<i>\"Tell me, are you aware of their prices?\"</i> You admit that you don't, but you're aware that they're one of the fancier restaurants in the area, so it's safe to assume that they're pricey. <i>\"'Pricey' is right. That particular restaurant can charge upwards of $90 per person.\"</i> Your first instinct is to balk at that number, but you quickly regain your composure. You remind Samantha that you're making good money in the games. It's not like you can't afford to throw a little around. <i>\"Hmm. Alright, another question. It's true that you're making money. It's also true that you could afford to spend some without worry. But tell me, are you aware of how lottery winners often end up losing their money and why?\"</i><p>" +
                        "That's an easy one. It's because they go ahead and just start spending without...oh. <i>\"Now you understand. I think I understand as well; you were trying to impress me, weren't you?\"</i><p>" +
                        "Sheepishly, you admit that she got it in one. She laughs lightly on the other end. <i>\"I appreciate the thought, " + player.name + ". Really, it's sweet of you. But just because you have money to burn, it doesn't mean you actually should. This is a good time to think about your future. You won't be participating in the Games forever, so you should consider saving now so that you can have a safe cushion to fall back on afterwards.\"</i><p>" +
                        "Glad that she can't see how red your cheeks are, you thank her for her wisdom. Then you ask if she has any ideas to spend the night. \"I do, in fact. Instead of going out to get an expensive meal, why don't you come over to my place and we'll cook up something ourselves? I can promise it'll be just as good as anything in a restaurant.\"</i><p>" +
                        "That sounds appealing. You agree, and a few minutes later you're heading over. It doesn't take long to arrive and you knock on the door. It opens only a moment later, revealing Samantha, dressed comfortably for cooking. \"Hello, Love. Welcome to my humble abode.\" She invites you in with a wave of her arm, and you join her inside. True to her frugal advice, the interior is nice, but she clearly hasn't spent much more than she really needed to.<p>" +
                        "The two of you head into the kitchen quickly. Together you look through the cabinets and the fridge as you discuss what to make. \"What about this, Love?\" Looking away from the cabinet you were searching, you see Samantha holding a package of frozen chicken and a bottle of marinara sauce. <i>\"Since you were considering an Italian restaurant earlier, why not chicken parmesan?\"</i> <p>" +
                        "That's an option that's both simple to make, and delicious. You agree, and the two of you get to work. Samantha puts the chicken in the microwave, setting it to defrost mode. You grab a few eggs, breadcrumbs, and cheese. With the two of you working together, it's not long before you're sliding the freshly breaded and seasoned chicken into the oven and begin setting up two plates at the small, but cozy table. Soon enough, everything is ready, and the two of you settle in at the table. Both of your plates have a generous serving of chicken parmesan, complete with marinara sauce, mozzarella cheese, and a side of assorted veggies that you boiled up at the last minute.<p>" +
                        "Samantha runs her eyes over the food, looking hungry. <i>\"Now this is how you impress me. A nice, home cooked meal.\"</i> You blush lightly, but remind her that she did half of the work. <i>\"Oh, I know. That just means that sometimes I even impress myself.\"</i> The two of you share a laugh at the joke before digging in.<p>" +
                        "You eat in companionable silence for a while, enjoying the meal that you didn't pay restaurant prices for. As you make a chunk in the food, Samantha speaks up. <i>\"So, you really were calling me just to treat me to dinner?\"</i> You nod, explaining that you really were just trying to impress her a bit, just as she deduced. You hadn't really planned for anything beyond a nice dinner.<p>" +
                        "<i>\"Hmm...well, that's incredibly thoughtful of you. I should thank you.\"</i> You shake your head. There's no need for her to thank you. <i>\"Oh no. I want to. And I have an idea on how to do so…\"</i> You suddenly feel a weight in your lap. Looking under the table, you find Samantha's feet there, her toes tapping at your crotch. <i>\"Come on. You can even keep eating while I thank you.\"</i> You pause for a moment, considering the offer. Then you undo your pants, sliding them and your underwear down enough to free your dick. Samantha immediately begins rubbing her feet against it, showing her skill and experience as she quickly works you towards hardness. You continue eating as she said to, but can't help the moans escaping your mouth.<p>" +
                        "It takes only moments to find yourself at full mast, and Samantha catches your shaft between her feet. She wastes no time in sliding up and down your length, applying just the right amount of pressure and sending shocks of pleasure up your spine. You groan loudly, struggling to continue eating before giving up entirely. Leaning back in the chair, you focus on the feeling of her soles pleasuring you. You try to hold back, wanting to enjoy the sensations for as long as possible. However, there's no denying Samantha's skill when she has the opportunity to apply it without resistance, and it's only a matter of time until you groan in orgasm, basting her feet in your seed, along with a small area of the bottom of the table. Once you come down from your high, Samantha pulls her feet away with a smile. <i>\"I'm glad you enjoyed that. Would you consider that adequate thanks for this evening?\"</i><p>"
            )
            Global.gui.displayImage("premium/Samantha Footjob.jpg", "Art by AimlessArt")
            Global.gui.message(
                "You take some time to recover before responding, getting your breathing under control. Finally, you give her a smile. While that certainly did feel great, you couldn't possibly consider this finished without dessert. Samantha raises a brow, giving you a knowing smirk. <i>\"Oh? Well, what do you have in mind?\"</i> Fixing your pants, you stand and walk around the table to join her. You kneel between her legs, and she invitingly lifts her skirt to give you easy access to your target. You shift aside her lace panties, and begin prodding with your tongue. She doesn't respond at first, but as you continue your ministrations, she begins to get wet and vocal.<p>" +
                        "Samantha's soft moans spur you on, and your tongue finds its way deeper into her welcoming folds. You rub against every sensitive spot you can find, drawing out more lurid sounds of pleasure. Soon enough, she's squirming in her seat. Occasionally, her thighs squeeze your skull rather tightly before she regains control of herself. Once you're certain that she's at her limit, you draw your tongue out and look up at her face. Her face is flush, so close to release, and her eyes demand that you finish the job. With a smile, you lean back in and give her clit the final nibble she needs. She cries out in pleasure, her love juices squirting all over your face in orgasm. You resume licking her through her orgasm, making sure it feels as good as possible on her way back down.<p>" +
                        "A few minutes later, the two of you are cleaning the mess you made, from both dinner and the sex. You thank Samantha for inviting you over. This really had been better than any trip to a restaurant could have been. Once all the cleanup is finished, you check the time and nearly choke. It was far later than you had realized. Apparently, time really does fly when you're having fun. You let Samantha know that it's about time for you to head back to campus, but she grabs you by the arm. <i>\"There's no need to rush home, Love. You're welcome to spend the night here. My bed is big enough for two, and I assure you it's quite comfortable.\"</i> As you consider the offer, you notice the lust in her eyes. Deciding you'd be up for some more fun, you agree. Samantha leads you to her bedroom, and you know you're not going to get much rest tonight.<p>" +
                        "Worth it."
            )
            Roster.gainAffection(ID.PLAYER, ID.SAMANTHA, 3)
            Global.gui.message("<b>You've gained Affection with Samantha</b>")
            Global.gui.loadPortrait(player, samSprite)
            end()
        }
        if (response.startsWith("Hang out with Lilly")) {
            lillySprite.dress()
            lillySprite.mood = Emotion.confident
            if (Roster.getAffection(ID.PLAYER, ID.LILLY) >= 8) {
                if (Global.random(2) == 0) {
                    Global.gui.message(
                        "Danielle's bailed on sushi night. Lilly complains about it good-naturedly as she drives the two of you back to the restaurant; despite the fact there are a good dozen things on the menu that Danielle probably would eat, like udon, she isn't really willing to set foot in the building.<p>" +
                                "<i>\"She's a small-town girl,\"</i> Lilly says. <i>\"She keeps saying sushi is bait, not food. I'm working on it.\"</i><p>" +
                                "As you arrive at the restaurant, you mentally steel yourself for what's to come. Sure enough, you get seated at the sushi counter again, and Lilly goes on a rampage through the nigiri menu. You almost don't order anything of your own, because she keeps insisting on sharing her food with you, as well as a couple of sips from every little clay bottle of the house sake.<p>" +
                                "<i>\"I work out like a demon all week,\"</i> Lilly says, like she's telling you a much bigger secret than this, <i>\"so I can eat whatever I want on Sundays.\"</i><p>" +
                                "Clearly. You aren't sure where she's putting all of this sushi. Even the guy behind the counter, a middle-aged man in a chef's uniform with a sketchy command of conversational English, looks at her with something like awe.<p>"
                    )
                    Global.gui.displayImage("premium/Lilly Drinking.jpg", "Art by AimlessArt")
                    Global.gui.message(
                        "As you leave, Lilly is a little wobbly, and hands you her car keys without further comment. You start the drive back to campus with her half-asleep in the passenger seat.<p>" +
                                "Naturally, the conversation turns to the week ahead, and what you're dreading and anticipating. That leads to the Games, of course, and your recent bouts; who you've beaten, who beat you, who's probably going to be out for payback tomorrow.<p>" +
                                "Lilly tries to stay neutral in these things. You haven't asked for anything that would give you a competitive advantage, and she's been clear that she wouldn't say anything if you did.<p>" +
                                "She does surprise you, however, right before you get to her place, by telling you something you didn't know.<p>" +
                                lillySecret + "<p>" +
                                "Lilly's quiet after that. When you check, it's because she's dozing in her seat. You can't seem to wake her, so you carry her to her apartment and leave her on her couch with her keys on her coffee table.<p>" +
                                "The walk back to the dorm helps, but even so, you fall into a food coma the moment you reach your bed."
                    )
                } else {
                    Global.gui.message(
                        "You and Lilly meet up at your dorm room. She brings a bottle of wine for herself and a six-pack for you, and you order a couple of pizzas. After you eat, you and Lilly spend a few hours thrashing each other in fighting games.<p>" +
                                "She's clearly been spending her free time 'in the lab,' which is both a hindrance and a help. Lilly really wants to win by doing complicated strategies and highly technical combos, which means she sometimes throws away a game because she's being too methodical for her own good. There's something to be said in any game--even the Games, now that you think about it--for the direct approach, and Lilly doesn't want to take it.<p>" +
                                "On the other hand, this is turning into a crash course for you on what's actually possible in this game with a little practice, and she keeps surprising you.<p>" +
                                "Nobody's keeping track, but it feels like you're about neck and neck by the end of the night. Lilly eventually calls it; she's polished off her wine and is tipsy enough that her motor skills aren't up to the task of playing like she wants to be.<p>" +
                                "There's a good hour left before you both have to be in bed, though, so you throw in another game. You chill out hitting each other with giant hammers and rocket launchers for a while, and the sheer idiocy of the game gives Lilly a fit of the giggles.<p>" +
                                "This really isn't that much less dumb than the Games, you say without thinking.<p>" +
                                "Lilly brightens, and leans over to murmur in your ear, despite the fact that you're the only two people in the room.<p>" +
                                lillySecret + "<p>" +
                                "You nod, and use the distraction to blow the floor out from under Lilly's character. She jumps, smacks you on the arm, and makes a point of trying harder for the next few rounds.<p>" +
                                "Eventually, you've both got to head out. You walk Lilly back to her place, then meander back to your dorm and collapse into your bed."
                    )
                }
            } else if (Roster.getAffection(ID.PLAYER, ID.LILLY) >= 6) {
                Global.gui.message(
                    "You call up Lilly and chat for a few minutes. Abruptly, she asks, <i>\"Do you like sushi?\"</i><p>" +
                            "Sometimes.<p>" +
                            "<i>\"Danielle hates it, and there's a new place about thirty minutes away I really want to try. You up for going?\"</i><p>" +
                            "Well, why not. You didn't have a better idea, besides another fighting-game marathon.<p>" +
                            "<i>\"Great. I'll drive.\"</i><p>" +
                            "She meets you outside the dorm ten minutes later, behind the wheel of a two-year-old sedan. Getting in feels like you're climbing inside an armored personnel carrier; it's a sturdy metal tank of a car, where everything about its design was a distant secondary consideration behind the safety of its passengers. Lilly drives it like there's a cop directly behind her.<p>" +
                            "You chat, mostly about video games and movies, until she pulls into a space at a restaurant that's tucked into an isolated lot near the highway. You're far enough away from the ocean out here that you immediately have misgivings about eating any kind of sushi they serve. When you look up the reviews on your phone, though, the reviews are mostly good, and the handful of one-star write-ups mention bad service, not food poisoning. So to hell with it.<p>" +
                            "Lilly has a reservation. The woman working the front, middle-aged and dressed all in black with a nearly-impenetrable Japanese accent, brings you to two seats at the end of the sushi counter.<p>" +
                            "<i>\"Okay,\"</i> Lilly says, rubbing her hands together. <i>\"I am going to make a pig of myself here.\"</i><p>" +
                            "You look at her strangely.<p>" +
                            "<i>\"I love sushi and the places near school are all kind of lame. The second best tuna roll in town is from the supermarket. I've had a craving for the real thing for <b>months</b>.\"</i><p>" +
                            "More to the point, she seems to feel like your own palate is uncultured. Lilly orders a lot of nigiri and rolls and keeps offering you some, squid, eel, scallops, and more elaborate creations, way past the point where you're full and when you'd think she'd be full. After an hour of this, you feel like lying down on the floor and passing out, but she's still ordering little things off the menu.<p>" +
                            "<i>\"The best part of working at the Games,\"</i> she whispers to you at one point, <i>\"is that sometimes we can go nuts like this and spend half my rent on dinner.\"</i><p>" +
                            "You're not sure that's the best part--it definitely isn't for you--but it's not worth arguing over, particularly since Lilly is about three bottles deep on the house sake. It's mild-tasting stuff that hits harder than you'd think it would, and you decided to slow down on it after one cup. Somebody has to be sober enough to drive back.<p>" +
                            "You're about to say something along those lines to Lilly, but then she gets uncharacteristically chatty. At first, it's just about that fighting game you were playing. She's been watching a lot of online videos about her character since that night, and nearly bought a console just to practice.<p>" +
                            "Then, out of nowhere, she starts talking about Danielle.<p>" +
                            "<i>\"She's so cute,\"</i> Lilly says to you, whispering like it's a secret, <i>\"and I like her so much, but she's so shy and she's so far in the closet. I want her to just jump on me, you know? To finally cut loose.\"</i><p>" +
                            "Well, that was more than you needed to hear.<p>" +
                            "Lilly giggles. <i>\"Sorry.\"</i><p>"
                )
                Global.gui.displayImage("premium/Lilly Drinking.jpg", "Art by AimlessArt")
                Global.gui.message(
                    "Clearly, Lilly has no filter when she's drunk. She drinks the last of the sake on the counter, and you manage to convince her that it's time to switch to water.<p>" +
                            "<i>\"You know, funny story,\"</i> Lilly says, stage-whispering. <i>\"There's a superhero in Seattle who used to compete in the Games.\"</i><p>" +
                            "You raise your eyebrows.<p>" +
                            "<i>\"Everybody thinks she's just one of those goofballs who dresses up to go out and help people in their neighborhood, but she's actually really, really strong. One of these days, she's going to pick up a car on TV and we are going to be in <b>so much trouble</b>.\"</i> Lilly giggles hysterically. <i>\"Don't tell anybody I told you that.\"</i><p>" +
                            "After that, she pulls the conversation back towards nerd culture.<p>" +
                            "About half an hour later, she's sagging on her stool, but still talks you into splitting a scoop of green tea ice cream. Afterward, you pay for the meal, then talk Lilly out of her keys and drive her car back to campus. She's still a little tipsy when you wish her good night."
                )
            } else if (Roster.getAffection(ID.PLAYER, ID.LILLY) >= 4) {
                Global.gui.message(
                    "You fire up your PC to kill some time before you're supposed to meet up with Lilly for drinks. A few simulated card games later, someone knocks on your door.<p>" +
                            "You back out of the match and answer it. Lilly's on the other side with her arms folded. <i>\"You're late.\"</i><p>" +
                            "...you are. You lost half an hour to the card game, and sheepishly explain as much, then invite Lilly in.<p>" +
                            "<i>\"I guess I get that,\"</i> she says. Lilly immediately goes to your messy stack of console games and picks them up to read the backs of their cases. <i>\"I used to be really bad about appointments and classes because of video games.\"</i><p>" +
                            "Used to be?<p>" +
                            "<i>\"Oh, I decided to stick with phone games and things that'll run on my laptop,\"</i> Lilly says. <i>\"At least then I can go to where I'm supposed to be first, then play.\"</i><p>" +
                            "You nod.<p>" +
                            "Lilly puts down your games. They're now in a neat stack, organized alphabetically, except for the one she's kept to show to you. <i>\"I didn't know they made another one of these,\"</i> she says.<p>" +
                            "They didn't. It's an HD port of the last installment in a 3D head-to-head fighting game. You play it occasionally, but the netcode's crap and you've never run into anyone else on campus who likes this particular game. Caroline played it with you once for thirty minutes and immediately changed it out for her favorite.<p>" +
                            "<i>\"Do you mind if I throw this in?\"</i> she asks.<p>" +
                            "You need to grab a shower anyway, so you don't mind. Lilly fires up your TV and console, then immediately goes into training mode.<p>" +
                            "By the time you're back, dry, and dressed, she's gravitated to the character that you'd dismissed as being way too complicated and technical to ever bother with. Lilly is about halfway through the combo tutorial.<p>" +
                            "You ask her if she wants to grab dinner.<p>" +
                            "Lilly digs a twenty out of her pocket and hands it to you without looking. <i>\"Get pizzas,\"</i> she says. <i>\"Mine's double cheese, thick crust, garlic. Two-liter of diet cola.\"</i><p>" +
                            "Well, since she insists. You pick up your phone and place an order for delivery.<p>" +
                            "By the time it's arrived, you're playing against Lilly. The first few matches go your way, but she learns from each one, and asks a lot of questions about what you did to beat her. After a short break to eat a slice each, you go back into it and Lilly's started to pick up a round here and there.<p>" +
                            "A few hours later, you aren't quite neck and neck, and she's gotten a feel for the game's particular rhythm. More to the point, Lilly's focused on mechanics and the exploitation thereof in a way that you never quite mastered yourself. There are intricacies here that pro players don't bother with, but she's pulling them out like they're obvious.<p>" +
                            "Eventually, you both should've been in bed half an hour ago. A pitched battle between you comes right down to the wire, and ends in a double knockout. Lilly looks at you and breaks down into exhausted giggling.<p>" +
                            "<i>\"That's a good note to go out on,\"</i> she says, and picks up her leftover pizza. <i>\"See you tomorrow?\"</i><p>" +
                            "Of course.<p>" +
                            "<i>\"This was fun. Let's do this again soon,\"</i> Lilly says, and lets herself out.<p>"
                )
            } else {
                Global.gui.message(
                    "Lilly picks up on the third ring. It sounds like you might have woke her up, but she rallies quickly, and suggests you meet her at a bar you've never heard of before.<p>"
                            + "About twenty minutes later, you walk into an upscale tavern on the other side of town, well away from campus. It's all oak, brass, and old, maintained leather, full of people who look like they're having important conversations. Nobody objects to your presence, but you do feel like you should've dressed up for this.<p>"
                            + "It takes you a second to recognize Lilly at the bar, in low light. You slide in next to her, and she looks up from her phone with a slight smile.<p>"
                            + "<i>\"Hey,\"</i> she says. <i>\"What do you think of this place?\"</i><p>"
                            + "You offer the first thought that comes to mind: old money drinks here.<p>"
                            + "<i>\"Yeah,\"</i> Lilly says. <i>\"I like that. A lot of the kids at school don't even dare come through the door. I started coming here after my first couple of Games paychecks.\"</i><p>"
                            + "You double-check the crowd when she mentions the Games, just to make sure nobody's listening in. If anybody's going to randomly know about them, it feels to you like it'd be a crowd like this. You wouldn't be surprised to find out people are conducting corporate espionage, or making backroom political deals. It's just that sort of atmosphere.<p>"
                            + "Lilly's mentioned that she was a competitor, usually while she's offering you a handicap. It's surprisingly difficult to imagine her on the field, though, and you say as much.<p>"
                            + "<i>\"Why's that?\"</i><p>"
                            + "Now's a good enough example. She's immaculate, in tailored casual clothes. Every incidental gesture she makes is measured and precise: exactly one sip of wine, one bite of cheese, every time. It's like she was designed by a watchmaker. You can't imagine her when she's not in control, or less than composed.<p>"
                            + "<i>\"Yeah. I had a hard time with that, too. It was never the nudity, or even the sex. It was how <b>messy</b> the whole thing is.\"</i> Lilly swirls the last swallow of wine around in her glass. <i>\"But... I couldn't not do it. A lot of my old high school classmates are looking at five, maybe six digits of student loan debt. A year in the Games paid for all the education I could ever want to have.\"</i><p>"
                            + "She half-smirks.<p>"
                            + "<i>\"Of course, I'm not sure you ever quite get out of them.\"</i><p>"
                            + "You'd wondered about that. You've encountered enough people at this point who used to compete, but are still affiliated with them one way or the other. It's not the first time you've wondered exactly what it is you signed up for.<p>"
                            + "<i>\"I probably should've mentioned that up front,\"</i> she says. <i>\"A lot of Games veterans seem to mount one-night-only comeback tours, sooner or later. Maya's famous for it.\"</i><p>"
                            + "You can't help but ask: famous with whom?<p>"
                            + "<i>\"The right people,\"</i> Lilly says.<p>"
                            + "She almost continues that thought, but instead, she drinks the last of her wine. The bartender's over in a minute to decant a fresh glass for her, and sets down a plate of cheese to go with it.<p>"
                            + "<i>\"It's our night off,\"</i> she says, <i>\"and I'm sitting here talking about work.\"</i> Lilly shakes her head.<p>"
                            + "You're technically co-workers, you say. Sort of. It's probably to be expected.<p>"
                            + "<i>\"Still,\"</i> Lilly says. <i>\"Have you eaten?\"</i><p>"
                            + "Neither of you actually say as much, but you decide on the spot not to mention the games for the rest of the night. Lilly finds a nearby taco truck on her phone, you grab a bite to eat, and end up wandering into a nearby movie theater.<p>"
                            + "She wants to look up reviews until she finds a movie that's pleasantly challenging; you want to randomly pick a comedy based on the poster. You argue about it good-naturedly for a few minutes before ending up in an empty showing of some suspense movie about preventing an imminent apocalypse.<p>"
                            + "Lilly calls for a rideshare as you walk out of the theater, and the two of you spend the ride back to campus discussing the film with the driver.<p>"
                            + "When you get back to your dorm room, your gaze falls across the drawer in which you keep your gear for the Games, and just for a second, it all feels as weird again as it did when you started.<p>"
                            + "You don't know what being an average college student would feel like, and now you never will. You started in on the Games too quickly. For a while tonight, though, you feel like you got a glimpse of it, and it's oddly refreshing, like you took a six-hour vacation.<p>"
                            + "You sleep well that night."
                )
            }
            Roster.gainAffection(ID.PLAYER, ID.LILLY, 2)
            Global.gui.message("<b>You've gained Affection with Lilly</b>")
            Global.gui.loadPortrait(player, lillySprite)
            end()
        }
    }

    override fun play(response: String): Boolean {
        if (response.startsWith("Choices")) {
            if (Scheduler.isMatchNight) {
                Global.gui.message(
                    "There is a match tonight, but not for you. To keep matches from getting out of control, participation is rotated " +
                            "each night. As a consequence, some of the girls you would typically like to spend your night off with may be busy tonight."
                )
            } else if (Scheduler.matchNumber > Constants.SEASONLENGTH) {
                Global.gui.message(
                    "The season has ended and there is a very brief respite until the new one begins. You can do whatever you " +
                            "want tonight."
                )
            } else {
                Global.gui.message(
                    "There are no matches on Sunday nights. While the Games have guaranteed you an eventful sex life, they've "
                            + "really dominated your schedule. This is the only night each week you have to yourself."
                )
            }

            offerChoices()
        }
        return false
    }

    override fun morning(): String {
        return ""
    }

    override fun mandatory(): String {
        return ""
    }

    override fun addAvailable(available: HashMap<String, Int>) {
        // TODO Auto-generated method stub
    }

    private fun end() {
        Scheduler.advanceTime(LocalTime.of(3, 0))
        Scheduler.match?.end()
        Global.gui.endMatch()
    }

    val lillySecret: String
        get() {
            val bucket = ArrayList<String>()
            bucket.add("<i>\"As far as I can tell, Cassie is legitimately straight. That's more uncommon in the Games than you might think. It's a good thing you're competing, or we'd have trouble keeping her motivated.\"</i>")
            bucket.add("<i>\"Cassie was top of the list for competitors this year. I wasn't sure why at first, but damn she's a really quick learner.\"</i>")
            bucket.add("<i>\"Have you ever noticed that Angel's win rate against Cassie is nowhere near what it should be? I think she might be throwing fights on purpose. She doesn't really need the money anyway.\"</i>")
            if (Roster[ID.ANGEL].has(Trait.succubus)) {
                bucket.add("<i>\"Angel is <b>really</b> bad at keeping her powers a secret. I think I'm going to have to have a talk with her. I had to convince the school newspaper there's a breed of giant bat that's native to this region, all because she likes to fly to class.\"</i>")
            }
            if (Roster[ID.MARA].has(Trait.madscientist)) {
                bucket.add("<i>\"Mara's tripped the circuit breaker to her entire dormitory twice. Sooner or later, somebody's going to notice what she's doing. I wish she's just keep it to her lab.\"</i>")
            }
            bucket.add("<i>\"I keep arranging it so Jewel gets free coupons for spa weekends or massages, but she's never taken advantage of them. She really needs to learn the value of rest or she's going to hurt herself.\"</i>")
            if (Global.checkFlag(Flag.Yui)) {
                bucket.add("<i>\"Yui watches you sleep on occasion. It's right on the border between cute and creepy.\"</i>")
            }
            if (Global.checkFlag(Flag.Kat)) {
                bucket.add("<i>\"I used to think I'd like to do something like what Kat did, but then she told me all about the downsides of having a cat's sense of smell. Ugh. She goes through a <b>lot</b> of vapor rub.\"</i>")
                bucket.add("<i>\"I think Kat's superpower is being irresistibly adorable. Even Cassie seems to be crushing on her.\"</i>")
            }
            if (Global.checkFlag(Flag.Eve)) {
                bucket.add("<i>\"Eve is banned from most of the piercing parlors in the state. She wants things that most people can't do legally, if at all.\"</i>")
                bucket.add("<i>\"As much as Eve shit talks competing in the Games, she never misses a match that she's scheduled for. I guess getting her dick wet is really the most important thing for her.\"</i>")
                bucket.add("<i>\"We've basically had to give up on trying to stop Eve from talking about the Games. She just doesn't care about the idea of secrecy. Fortunately, she doesn't care enough to actively try to expose us either.\"</i>")
            }
            if (Global.checkFlag(Flag.Reyka)) {
                bucket.add("<i>\"Reyka made a couple of porn movies under the name 'Lilly Ardat.' I wonder if she's the only demon in that business. Probably not.\"</i>")
                bucket.add("<i>\"Reyka is totally addicted to online games. Apparently there's no internet in her home dimension. I bet that's why she's so reluctant to go home.\"</i>")
            }
            bucket.add("<i>\"You should try to take one of Maya's classes, if you can. She's actually a really good teacher, but a lot of the older professors resent her for being attractive.\"</i>")
            if (Global.checkFlag(Flag.Samantha)) {
                bucket.add("<i>\"Samantha has had a lot of close calls with Campus Safety. After all, she is a prostitute who spends most of her nights on campus. They really frown on that sort of thing.\"</i>")
            }
            bucket.add("<i>\"I tried to convince my little sister to participate in the Games. I figured it would help her come out of her shell, but apparently the whole 'sexfighting other girls' was a dealbreaker for her.\"</i>")
            bucket.add("<i>\"I met a woman once who claimed she'd competed in the Games in 1982, in France. It didn't sound much like what we do, though.\"</i>")
            bucket.add("<i>\"There's an alternative rules set or something where the Games last a whole week. Competitors get dumped into the venue like it's a survival game and whoever gives out the most orgasms wins. It sounded like a real pain in the neck to organize.\"</i>")
            bucket.add("<i>\"The last week I competed, we found this weird device set up in the tunnel near the laundry room. We never did figure out who put it there, or what it was supposed to do. It was almost like it was supposed to collect something from the environment.\"</i>")
            bucket.add("<i>\"There are definitely versions of the Games going on all over the country, but for some reason, Maya is working at our school. Are we special or do you think there are Maya-equivalents at the other campuses?\"</i>")
            bucket.add("<i>\"Back when I competed, there was this one girl I hated fighting. She was a creepy blonde loli, who was weak, but really hard to beat. No matter how much I tied her up and dominated her, it always felt like she was in control.\"</i>")
            bucket.add("<i>\"Maya would almost certainly burn down the school if she thought it'd make the Benefactor's life even a little easier. I don't know what all she's gotten from the Games, but it's got to have been more than I can even guess.\"</i>")
            bucket.add("<i>\"I'll never understand why Aesop is allowed to sell so many secrets about the Games. He's actually lucky he retired before meeting the Benefactor, or I'm sure Maya would shut him down in a second.\"</i>")
            bucket.add("<i>\"Before Maya became the Benefactor's right hand woman, he apparently used to supervise the Games in person. Now Maya has turned meeting him into some huge rite of passage.\"</i><p>")
            return bucket.random()
        }

    private enum class plans {
        NONE,
        STAYIN,
        LILLYDRINKS,
        CASSIEMOVIE,
        MARATESTING,
        ANGELMOVIE,
        JEWELDINNER,
        YUIDATE,
        GROUPSHOPPING,
        LILLYGAMES,
        LILLYSUSHI
    }
}

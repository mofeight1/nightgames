package global

import characters.Angel
import characters.Cassie
import characters.Character
import characters.Eve
import characters.ID
import characters.Jewel
import characters.Kat
import characters.Mara
import characters.Maya
import characters.NPC
import characters.Player
import characters.Relationship
import characters.Reyka
import characters.Samantha
import characters.Sofia
import characters.Valerie
import characters.Yui
import com.google.gson.JsonObject

object Roster {
    private var players = ArrayList<Character>()
    private val relationships = HashSet<Relationship>()
    init {
        init()
    }
    fun init() {
        players = ArrayList<Character>()
        players.add(Global.player)
        players.add(Cassie().character)
        players.add(Mara().character)
        players.add(Angel().character)
        players.add(Jewel().character)
        players.add(Yui().character)
        players.add(Kat().character)
        players.add(Reyka().character)
        players.add(Eve().character)
        players.add(Samantha().character)
        players.add(Valerie().character)
        players.add(Sofia().character)
        players.add(Maya().character)
    }

    fun reset(p: Player) {
        init()
        players.remove(get(ID.PLAYER))
        players.add(p)
    }

    fun getNPC(name: String): NPC? {
        return players.firstOrNull { it.name.equals(name, ignoreCase = true) } as NPC?
    }

    operator fun get(identity: ID): Character {
        return players.first { it.id == identity }
    }

    operator fun get(name: String): Character {
        return players.first { it.name.equals(name, ignoreCase = true) }
    }

    fun exists(npc: ID?): Boolean {
        return when (npc) {
            ID.EVE -> Global.checkFlag(Flag.Eve)
            ID.KAT -> Global.checkFlag(Flag.Kat)
            ID.REYKA -> Global.checkFlag(Flag.Reyka)
            ID.YUI -> Global.checkFlag(Flag.metYui)
            ID.SAMANTHA -> Global.checkFlag(Flag.Samantha)
            ID.VALERIE -> Global.checkFlag(Flag.Valerie)
            ID.SOFIA -> Global.checkFlag(Flag.Sofia)
            else -> true
        }
    }

    fun canParticipate(npc: ID): Boolean {
        if (!exists(npc)) {
            return false
        }
        if (get(npc).isBenched) {
            return false
        }
        when (npc) {
            ID.MAYA -> return false
            ID.YUI -> return Global.checkFlag(Flag.Yui)
            else -> {}
        }
        return true
    }

    fun areIntimate(p1: ID, p2: ID): Boolean {
        val pair = hashSetOf(p1, p2)
        if (pair.contains(ID.PLAYER)) {
            if (pair.contains(ID.CASSIE)) {
                return Global.checkFlag(Flag.CassieDate)
            }
            if (pair.contains(ID.MARA)) {
                return Global.checkFlag(Flag.MaraDate)
            }
            if (pair.contains(ID.JEWEL)) {
                return Global.checkFlag(Flag.JewelDate)
            }
            if (pair.contains(ID.ANGEL)) {
                return Global.checkFlag(Flag.AngelDate)
            }
            if (pair.contains(ID.YUI)) {
                return Global.checkFlag(Flag.Yui)
            }
            if (pair.contains(ID.KAT)) {
                return Global.checkFlag(Flag.KatDate)
            }
            if (pair.contains(ID.REYKA)) {
                return Global.checkFlag(Flag.ReykaDate)
            }
            if (pair.contains(ID.VALERIE)) {
                return Global.checkFlag(Flag.Valerie)
            }
            if (pair.contains(ID.SAMANTHA)) {
                return Global.checkFlag(Flag.SamanthaDate)
            }
            if (pair.contains(ID.EVE)) {
                return Global.getValue(Flag.GangRank) >= 1
            }
            if (pair.contains(ID.SOFIA)) {
                return Global.checkFlag(Flag.SofiaDate)
            }
            return false
        }
        return exists(p1) && get(p1).rank >= 1 &&
                exists(p2) && get(p2).rank >= 1
    }

    fun scaleOpponent(npc: ID) {
        val level = get(ID.PLAYER).level
        get(npc).scaleLevel(level)
    }

    fun getRelationship(p1: ID, p2: ID): Relationship {
        var relationship = relationships.firstOrNull { it.contains(p1, p2) }
        if (relationship == null) {
            relationship = Relationship(p1, p2)
            relationships.add(relationship)
        }
        return relationship
    }

    fun getAttraction(p1: ID, p2: ID): Int {
        if (!exists(p1) || !exists(p2)) {
            return 0
        }
        return getRelationship(p1, p2).attraction
    }

    fun gainAttraction(p1: ID, p2: ID, magnitude: Int) {
        val pairing = getRelationship(p1, p2)
        pairing.attraction += magnitude

        if (areIntimate(p1, p2) && pairing.attraction >= 20) {
            pairing.affection += 2
            pairing.attraction -= 20
        }
    }

    fun setAttraction(p1: ID, p2: ID, magnitude: Int) {
        val relationship = getRelationship(p1, p2)
        relationship.attraction = magnitude
    }

    fun getAffection(p1: ID, p2: ID): Int {
        if (!exists(p1) || !exists(p2)) {
            return 0
        }
        return getRelationship(p1, p2).affection
    }

    fun gainAffection(p1: ID, p2: ID, magnitude: Int) {
        val relationship = getRelationship(p1, p2)
        relationship.affection += magnitude
    }

    fun setAffection(p1: ID, p2: ID, magnitude: Int) {
        val relationship = getRelationship(p1, p2)
        relationship.affection = magnitude
    }

    val existingPlayers: ArrayList<Character>
        get() {
            val group = ArrayList<Character>()
            for (c in players) {
                if (exists(c.id)) {
                    group.add(c)
                }
            }
            return group
        }
    val combatants: ArrayList<Character>
        get() {
            val group = ArrayList<Character>()
            for (c in players) {
                if (canParticipate(c.id)) {
                    group.add(c)
                }
            }
            return group
        }

    val benched: ArrayList<Character>
        get() {
            val group = ArrayList<Character>()
            for (c in players) {
                if (exists(c.id) && c.isBenched) {
                    group.add(c)
                }
            }
            return group
        }

    fun save(): JsonObject {
        val saver = JsonObject()
        for (c in players) {
            saver.add(c.id.name, c.save())
        }
        val saved = ArrayList<ID>()
        var currentAttraction: JsonObject
        var currentAffection: JsonObject
        var needed: Boolean
        val affections = JsonObject()
        val attractions = JsonObject()
        for (c in ID.entries) {
            needed = false
            currentAffection = JsonObject()
            currentAttraction = JsonObject()
            for (r in relationships) {
                if (r.contains(c)) {
                    val partner = r.getPartner(c)!!
                    if (!saved.contains(partner)) {
                        needed = true
                        currentAffection.addProperty(partner.name, r.affection)
                        currentAttraction.addProperty(partner.name, r.attraction)
                    }
                }
            }
            if (needed) {
                affections.add(c.name, currentAffection)
                attractions.add(c.name, currentAttraction)
            }
            saved.add(c)
        }
        saver.add("Affection", affections)
        saved.clear()
        saver.add("Attraction", attractions)
        return saver
    }

    fun load(loader: JsonObject) {
        init()
        for (c in players) {
            if (loader.has(c.id.name)) {
                c.load(loader[c.id.name].asJsonObject)
            }
        }
        relationships.clear()
        var p1: ID
        var p2: ID
        val affections = loader.getAsJsonObject("Affection")
        for (name in affections.keySet()) {
            p1 = ID.valueOf(name!!)
            val rel = affections.getAsJsonObject(name)
            for (name2 in rel.keySet()) {
                p2 = ID.valueOf(name2!!)
                setAffection(p1, p2, rel[name2].asInt)
            }
        }
        val attractions = loader.getAsJsonObject("Attraction")
        for (name in attractions.keySet()) {
            p1 = ID.valueOf(name!!)
            val rel = attractions.getAsJsonObject(name)
            for (name2 in rel.keySet()) {
                p2 = ID.valueOf(name2!!)
                setAttraction(p1, p2, rel[name2].asInt)
            }
        }
    }
}

package global

import characters.Character
import characters.Dummy
import characters.Emotion
import gui.KeyableButton
import gui.SceneButton
import scenes.Event
import scenes.SceneFlag
import scenes.SceneManager
import scenes.YuiEvent

class Postmatch(private val player: Character, private val combatants: ArrayList<Character>) : Event {
    private var normal = true

    init {
        Global.current = this
        val message = ""
        val choice = ArrayList<KeyableButton>()
        choice.add(SceneButton("Collect Payment"))
        Global.gui.prompt(message, choice)
    }

    override fun respond(response: String) {
        Global.gui.clearCommand()
        if (response.startsWith("Collect")) {
            Global.gui.clearText()
            events()
            if (normal) {
                normal()
            }
        } else if (response.startsWith("Next")) {
            normal()
        } else if (response.startsWith("Take Mara")) {
            Global.gui.clearText()
            SceneManager.play(SceneFlag.MaraSickDayNight)
            if (Global.checkFlag(Flag.autosave)) {
                SaveManager.save(true)
            }
            Global.gui.endMatch()
        } else if (response.startsWith("Take Yui")) {
            Global.current = YuiEvent(player)
            (Global.current as YuiEvent).play("VirginityUndressing")
        }
    }

    private fun events() {
        if (Global.checkFlag(Flag.MaraDayOff) && !Global.checkFlag(Flag.MaraTemporal)) {
            SceneManager.play(SceneFlag.MaraSickDayAfterMatch)
            normal = false
            Global.gui.choose("Take Mara back to your dorm")
        } else if (Global.checkFlag(Flag.YuiUnlocking) && !Global.checkFlag(Flag.Yui)) {
            val yui = Dummy("Yui", 1, true)
            yui.mood = Emotion.nervous
            Global.gui.loadPortrait(player, yui)
            SceneManager.play(SceneFlag.YuiFirstAfterMatch)
            normal = false
            Global.gui.choose("Take Yui to your room")
        } else if (Global.checkFlag(Flag.metLilly) && !Global.checkFlag(Flag.challengeAccepted) && Global.random(10) >= 7) {
            val jewel = Dummy("Jewel", 1, true)
            jewel.mood = Emotion.angry
            Global.gui.loadPortrait(player, jewel)
            Global.gui.message(
                "When you gather after the match to collect your reward money, you notice Jewel is holding a crumpled up piece of paper and ask about it. " +
                        "<i>\"This? I found it lying on the ground during the match. It seems to be a worthless piece of trash, but I didn't want to litter.\"</i> Jewel's face is expressionless, " +
                        "but there's a bitter edge to her words that makes you curious. You uncrumple the note and read it.<p>"
                        + "'Jewel always acts like the dominant, always-on-top tomboy, " +
                        "but I bet she loves to be held down and fucked hard.'<p>"
                        + "<i>\"I was considering finding whoever wrote the note and tying his penis in a knot,\"</i> Jewel says, still " +
                        "impassive. <i>\"But I decided to just throw it out instead.\"</i> It's nice that she's learning to control her temper, but you're a little more concerned with the note. " +
                        "It mentions Jewel by name and seems to be alluding to the Games. You doubt one of the other girls wrote it. You should probably show it to Lilly.<p>"
                        + "...<p>"
                        + "<i>\"Oh for fuck's " +
                        "sake..\"</i> Lilly sighs, exasperated. <i>\"I thought we'd seen the last of these. I don't know who writes them, but they showed up last year too. I'll have to do a second " +
                        "sweep of the grounds each night to make sure they're all picked up by morning. They have competitors' names on them, so we absolutely cannot let a normal student find " +
                        "one.\"</i> She toys with a pigtail idly while looking annoyed. <i>\"For what it's worth, <b>they do seem to pay well if you do what the note says that night.</b> Do with them what " +
                        "you will.\"</i><br>"
            )
            Global.flag(Flag.challengeAccepted)
            Global.gui.choose("Next")
        }
    }

    private fun normal() {
        var maxaffection = 0
        var closest: Character? = null
        for (rival in combatants) {
            if (rival !== player && rival.getAffection(player) > maxaffection) {
                closest = rival
                maxaffection = rival.getAffection(player)
            }
        }

        if (maxaffection >= 15 && closest != null) {
            closest.afterParty()
        } else {
            Global.gui.message("You walk back to your dorm and get yourself cleaned up.")
        }
        if (Global.checkFlag(Flag.autosave)) {
            SaveManager.save(true)
        }
        Global.gui.endMatch()
    }

    override fun play(response: String): Boolean {
        return true
    }

    override fun morning(): String {
        return ""
    }

    override fun mandatory(): String {
        return ""
    }

    override fun addAvailable(available: HashMap<String, Int>) {
        return
    }
}

package global

import java.awt.Color

object Constants {
    var STARTINGPOWER: Int = 5
    var STARTINGSEDUCTION: Int = 5
    var STARTINGCUNNING: Int = 5
    var STARTINGPERCEPTION: Int = 5
    var STARTINGSPEED: Int = 5
    var STARTINGSTAMINA: Int = 25
    var STARTINGAROUSAL: Int = 50
    var STARTINGMOJO: Int = 35
    var STARTINGSTATPOINTS: Int = 2
    var STARTINGCASH: Int = 50
    var EASYSPEED: Int = 10
    var EASYSTAMINA: Int = 75
    var EASYAROUSAL: Int = 100
    var EASYMOJO: Int = 50
    var EASYSTATPOINTS: Int = 10
    var EASYCASH: Int = 5000
    var TRAININGSCALING: Int = 100
    var BASETRACKDIFF: Int = 20

    var SEASONLENGTH: Int = 15

    var PRIMARYFRAMECOLOR: Color = Color(240, 238, 235)
    var PRIMARYBGCOLOR: Color = Color(255, 250, 240)
    var PRIMARYTEXTCOLOR: Color = Color(20, 20, 40)

    var ALTFRAMECOLOR: Color = Color(40, 62, 76)
    var ALTBGCOLOR: Color = Color(20, 42, 56)
    var ALTTEXTCOLOR: Color = Color(220, 220, 224)
}

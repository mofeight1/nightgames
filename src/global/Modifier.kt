package global

import combat.Tag

enum class Modifier(val bonus: Double) : Tag {
    normal(0.0),  //No challenge
    pantsman(.3),  //Only get boxers when resupplying
    nudist(.5),  //No clothes
    norecovery(.5),  //Arousal only empties on loss or draw, no masturbation
    vibration(.3),  //Small arousal gain in upkeep
    vulnerable(.25),  //Permanent Hypersensitive effect
    pacifist(.25),  //Unable to use pain attacks
    notoys(.15),  //Unable to use toys
    noitems(.15),  //Unable to use consumables
    mittens(.3),  //Hand proficiency 25%
    lameglasses(.5),  //Negates Mojo gain
    hairtie(.2),  //Increased pain damage to groin
    ticklish(.2),  //Increased temptation and weakness damage from Tickle
    furry(.3),  //All opponents in BeastForm
    slippery(.2),  //Permanently oiled
    ftc(0.0),  //Fuck the Carrier
    maya(0.0),  //Maya Exhibition match
    quiet(0.0),  //AI only match
    practice(0.0),
    ; //Practice fight outside of match


    fun percentage(): String {
        return String.format("%d", Math.round(bonus * 100))
    }
}

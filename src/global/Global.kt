package global

import actions.Action
import actions.Locate
import characters.*
import daytime.Daytime
import gui.GUI
import items.*
import scenes.Event
import scenes.SceneFlag
import utilities.EnumMap
import utilities.sortedBy
import java.util.EnumMap
import java.util.Random

object Global {
    private var rng: Random = Random()
    lateinit var gui: GUI
        private set
    var actions: ArrayList<Action> = ArrayList()
        private set
    var flags: HashSet<Flag> = HashSet()
        private set
    private var watched: HashSet<SceneFlag> = HashSet()
    var counters: EnumMap<Flag, Double> = EnumMap()
        private set
    lateinit var player: Player
    private val match: Match? = null
    private val day: Daytime? = null
    var current: Event? = null
    var debug: Boolean = false

    fun init(gui: GUI) {
        rng = Random()
        flags = HashSet()
        counters = EnumMap()
        watched = HashSet()
        current = null
        setDefaultOptions()
        this.gui = gui
        this.gui.BuildGUI()
    }

    @JvmStatic
    fun main(args: Array<String>) {
        GUI()
    }

    fun newGame(one: Player, tutorial: Boolean) {
        player = one
        gui.populatePlayer(player)
        gainSkills(player, false)
        Roster.reset(one)
        Scheduler.reset()
        flag(Flag.exactimages)
        if (tutorial) {
            Tutorial(one).play("")
        } else {
            Scheduler.dusk()
        }
    }

    fun populateTargetedActions(combatants: Collection<Character>) {
        actions = ArrayList()
        for (active in combatants) {
            actions.add(Locate(active))
        }
    }

    fun getUpcomingSkills(att: Attribute, level: Int): String {
        val skillList = SkillList.skillsByAttribute
        val skills = skillList[att]?.sortedBy { it.second }
        if (skills.isNullOrEmpty()) {
            return ""
        }
        var list = "${att.name}($level): "
        var found = 0
        for (skill in skills) {
            if (level < skill.value) {
                list += "${skill.key.name} (${skill.value}), "
                found++
                if (found >= 5) {
                    break
                }
            }
        }
        return if (found == 0) "" else list
    }

    fun gainSkills(c: Character, display: Boolean = true) {
        for (skill in SkillList.allSkills) {
            val charCopy = skill.copy(c)
            if (!c.knows(charCopy) && charCopy.requirements(c)) {
                c.learn(charCopy)
                if (display && c.human()) {
                    gui.message("You've learned $skill.")
                }
            }
        }
        if (c.getPure(Attribute.Dark) >= 6 && !c.has(Trait.darkpromises)) {
            c.add(Trait.darkpromises)
        }
        if (c.getPure(Attribute.Animism) >= 2 && !c.has(Trait.pheromones)) {
            if (c.human()) {
                gui.message("You've gained the passive skill Pheromones.")
            }
            c.add(Trait.pheromones)
        }
        if (c.getPure(Attribute.Science) >= 24 && !c.has(Trait.improvedbattery)) {
            if (c.human()) {
                gui.message("You've gained the passive skill Improved Battery.")
            }
            c[Pool.BATTERY].gainMax(10)
            c.add(Trait.improvedbattery)
        }
        if (c.getPure(Attribute.Fetish) >= 1 && !c.has(Trait.exhibitionist)) {
            if (c.human()) {
                gui.message("You've gained the passive skill Exhibitionist.")
            }
            c.add(Trait.exhibitionist)
        }
        if (c.getPure(Attribute.Contender) >= 5 && !c.has(Trait.determinator)) {
            if (c.human()) {
                gui.message("You've gained the passive skill Determinator.")
            }
            c.add(Trait.determinator)
        }
    }
    fun availableFeats(c: Character): List<Trait> {
        return SkillList.allFeats.filter { !c.has(it) && it.req(c) }
    }

    fun flag(f: Flag) {
        flags.add(f)
    }

    fun unflag(f: Flag) {
        flags.remove(f)
    }

    fun checkFlag(f: Flag): Boolean {
        return flags.contains(f)
    }

    fun getValue(f: Flag): Double {
        return counters[f] ?: 0.0
    }

    fun modCounter(f: Flag, inc: Double) {
        if (!counters.containsKey(f)) {
            setCounter(f, inc)
        } else {
            counters[f] = getValue(f) + inc
        }
    }

    fun setCounter(f: Flag, value: Double) {
        counters[f] = value
    }

    fun watched(f: SceneFlag) {
        watched.add(f)
    }

    fun hasWatched(f: SceneFlag): Boolean {
        return watched.contains(f)
    }

    val allWatched: List<SceneFlag>
        get() {
            return watched.toList()
        }

    fun getItem(name: String?): Item? {
        return Flask.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
            ?: Potion.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
            ?: Component.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
            ?: Consumable.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
            ?: Trophy.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
            ?: Toy.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
            ?: Attachment.entries.firstOrNull { it.toString().equals(name, ignoreCase = true) }
    }

    const val intro =
        "Warning This game contains depictions of explicit sexual content. Do not proceed unless you are at least 18 and are comfortable with such content. All characters depicted are over 18 years of age.\n\nYou don't really know why you're going to the Student Union in the middle of the night. You'd have to be insane to accept the invitation you received this afternoon. Seriously, someone is offering you money to sexfight a bunch of girls? You're more likely to get mugged (though you're not carrying any money) or murdered if you show up. Best case scenario, it's probably a prank for gullible freshmen. You have no good reason to believe the invitation is on the level, but here you are, walking into the empty Student Union.\n\nNot quite empty, it turns out. The same woman who approached you this afternoon greets you and brings you to a room near the back of the building. Inside, you're surprised to find three quite attractive girls. After comparing notes, you confirm they're all freshmen like you and received the same invitation today. You're surprised, both that these girls would agree to such an invitation, and that you're the only guy here. For the first time, you start to believe that this might actually happen.\n\nAfter a few minutes of awkward small talk (though none of these girls seem self-conscious about being here), the woman walks in again leading another girl. Embarrassingly you recognize the girl, named Cassie, who is a classmate of yours, and who you've become friends with over the past couple weeks. She blushes when she sees you and the two of you consciously avoid eye contact while the woman explains the rules of the competition.\n\nThere are a lot of specific points, but the rules basically boil down to this:\n*Competitors move around the empty areas of the campus and engage each other in sexfights.\n*When one competitor orgasms, the other gets a point and can claim their clothes.\n*Additional orgasms between those two players are not worth any points until the loser gets a replacement set of clothes at either the Student Union or the first floor of the dorm building.\n *It seems to be customary, but not required, for the loser to get the winner off after a fight, when it doesn't count.\n*After three hours, the match ends and each player is paid for each opponent they defeat, each set of clothes taken, and a bonus for whoever scores the most points.\n\nAfter the explanation, she confirms with each participant whether they are still interested in participating.\n\nEveryone agrees. The first match starts at exactly 10:00."

    fun reset() {
        flags.clear()
        counters.clear()
        player = Player("Dummy")
        gui.purgePlayer()
        setDefaultOptions()
        gui.clearText()
    }

    private fun setDefaultOptions() {
        setCounter(Flag.NPCBaseStrength, 1.0)
        setCounter(Flag.NPCScaling, 1.0)
        setCounter(Flag.PlayerBaseStrength, 1.0)
        setCounter(Flag.PlayerScaling, 1.0)
        setCounter(Flag.fontsize, 6.0)
    }

    fun random(d: Int): Int {
        if (d <= 0) {
            return 0
        }
        return rng.nextInt(d)
    }
}

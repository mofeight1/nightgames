package global

import characters.ID
import characters.Player
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import scenes.SceneFlag
import utilities.enumValueOfOrNull
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.time.LocalTime
import java.util.Scanner
import javax.swing.JFileChooser
import javax.swing.filechooser.FileNameExtensionFilter

object SaveManager {
    fun save(auto: Boolean) {
        val file: FileWriter
        try {
            if (auto) {
                file = FileWriter("./auto.sav")
            } else {
                val dialog = JFileChooser("./")
                dialog.isMultiSelectionEnabled = false
                dialog.fileFilter = FileNameExtensionFilter("Save File (.sav)", "sav")
                dialog.selectedFile = File("Save.sav")
                val rv = dialog.showSaveDialog(Global.gui)

                if (rv != JFileChooser.APPROVE_OPTION) {
                    return
                }
                file = FileWriter(dialog.selectedFile)
            }

            val saver = JsonObject()
            saver.add("Roster", Roster.save())

            saver.addProperty("Date", Scheduler.date)
            saver.addProperty("Time", Scheduler.time.hour)
            saver.addProperty("Match", Scheduler.matchNumber)

            val flags = JsonArray()
            for (f in Global.flags) {
                flags.add(f.name)
            }
            saver.add("Flags", flags)

            val counters = JsonObject()
            for (f in Global.counters.keys) {
                counters.addProperty(f.name, Global.getValue(f))
            }
            saver.add("Counters", counters)

            val scenesflags = JsonArray()
            for (f in Global.allWatched) {
                scenesflags.add(f.name)
            }
            saver.add("SceneFlags", scenesflags)

            file.write(saver.toString())
            file.flush()
            file.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun load() {
        val dialog = JFileChooser("./")
        dialog.isMultiSelectionEnabled = false
        dialog.fileFilter = FileNameExtensionFilter("Save File (.sav)", "sav")
        val rv = dialog.showOpenDialog(Global.gui)
        if (rv != JFileChooser.APPROVE_OPTION) {
            return
        }

        Global.reset()

        //Global.buildSkillPool(Global.player)
        val file: FileReader
        try {
            file = FileReader(dialog.selectedFile)
            val loader = JsonParser()
            val data = loader.parse(file).asJsonObject

            Roster.load(data.getAsJsonObject("Roster"))

            Scheduler.date = data["Date"].asInt
            Scheduler.time = LocalTime.of(data["Time"].asInt, 0)
            Scheduler.matchNumber = data["Match"].asInt

            val flags = data.getAsJsonArray("Flags")
            for (f in flags) {
                val flag = enumValueOfOrNull<Flag>(f.asString)
                if (flag != null) Global.flag(flag)
            }

            val counters = data.getAsJsonObject("Counters")
            for (key in counters.keySet()) {
                val flag = enumValueOfOrNull<Flag>(key)
                if (flag != null) Global.setCounter(flag, counters[key].asDouble)
            }

            if (data.has("SceneFlags")) {
                val sceneflags = data.getAsJsonArray("SceneFlags")
                for (f in sceneflags) {
                    val flag = enumValueOfOrNull<SceneFlag>(f.asString)
                    if (flag != null) Global.watched(flag)
                }
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            try {
                load_legacy(FileInputStream(dialog.selectedFile))
            } catch (e1: FileNotFoundException) {
                e1.printStackTrace()
            }
        }
        Global.player = Roster[ID.PLAYER] as Player
        Global.gui.populatePlayer(Roster[ID.PLAYER] as Player)
        handleLegacyScenes()
        if (Scheduler.time.hour >= 22) {
            Scheduler.dusk()
        } else {
            Scheduler.dawn()
        }
    }

    fun handleLegacyScenes() {
        if (Global.checkFlag(Flag.MaraTemporal)) {
            Global.watched(SceneFlag.MaraSickDayNight)
            Global.watched(SceneFlag.MaraSickDayMorning)
            Global.watched(SceneFlag.MaraSickDayAfterMatch)
            Global.watched(SceneFlag.MaraSickDayInterrogation)
            Global.watched(SceneFlag.MaraSickDayLunch)
            Global.watched(SceneFlag.MaraSickDayShower)
            Global.watched(SceneFlag.MaraSickDayFinale)
        }
        if (Global.checkFlag(Flag.Yui)) {
            Global.watched(SceneFlag.YuiFirstAfterMatch)
            Global.watched(SceneFlag.YuiFirstUndress)
            Global.watched(SceneFlag.YuiFirstLick)
            Global.watched(SceneFlag.YuiFirstService)
            Global.watched(SceneFlag.YuiFirstSex)
        }
        if (Global.getValue(Flag.GangRank) > 0) {
            Global.watched(SceneFlag.EveInititation)
        }
    }

    fun load_legacy(file: FileInputStream) {
        var header: String
        var dawn = false

        val loader = Scanner(file)
        while (loader.hasNext()) {
            header = loader.next()
            if (header.equals("P", ignoreCase = true)) {
                Global.player.load(loader)
            } else if (header.equals("NPC", ignoreCase = true)) {
                val name = loader.next()
                Roster[name].load(loader)
            } else if (header.equals("Flags", ignoreCase = true)) {
                var next = loader.next()
                while (next != "#") {
                    Global.flag(Flag.valueOf(next))
                    next = loader.next()
                }
                dawn = loader.next().equals("Dawn", ignoreCase = true)
                if (loader.hasNext()) {
                    Scheduler.date = loader.nextInt()
                } else {
                    Scheduler.date = 1
                }
                while (loader.hasNext()) {
                    Global.setCounter(Flag.valueOf(loader.next()), loader.next().toDouble())
                }
            }
        }
        loader.close()

        if (dawn) {
            Scheduler.time = LocalTime.of(15, 0)
        } else {
            Scheduler.time = LocalTime.of(22, 0)
        }
    }
}

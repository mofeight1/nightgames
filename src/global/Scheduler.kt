package global

import characters.Character
import characters.ID
import characters.Player
import characters.Trait
import daytime.Daytime
import utilities.EnumMap
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.EnumMap

object Scheduler {
    var date = 1
    var time: LocalTime = LocalTime.of(22, 0)
    var day: Daytime? = null
        private set
    var match: Match? = null
        private set
    private var score: EnumMap<ID, Int> = EnumMap()
    var matchNumber = 0
    var isPaused: Boolean = false
        private set

    fun reset() {
        date = 1
        time = LocalTime.of(22, 0)
        matchNumber = 0
        score = EnumMap()
        isPaused = false
    }

    fun dawn() {
        time = LocalTime.of(9, 0)
        match = null
        if (matchNumber > Constants.SEASONLENGTH && date % 7 == 0) {
            matchNumber = 0
            Global.modCounter(Flag.SeasonNumber, 1.0)
            Global.unflag(Flag.OffSeason)
        }
        for (player in Roster.existingPlayers) {
            player.rest()
        }
        date++
        day = Daytime(Global.player)
        Global.gui.refresh()
    }

    fun dusk() {
        day = null
        time = LocalTime.of(22, 0)
        val player = Roster[ID.PLAYER] as Player
        if (hasMatch(player)) {
            Prematch(player)
        } else if (isMatchNight) {
            createMatch(Modifier.quiet)
            NightOff(player)
            match!!.automate(LocalTime.of(0, 0))
        } else {
            NightOff(player)
        }
    }

    fun createMatch(matchmod: Modifier) {
        match = null
        val lineup = ArrayList<Character>()
        val available = Roster.combatants
        if (matchmod == Modifier.maya) {
            lineup.add(Roster[ID.PLAYER])
            available.shuffle()
            for (player in available) {
                if (!lineup.contains(player) && !player.human() && lineup.size < 4 && !player.has(Trait.event)) {
                    lineup.add(player)
                }
            }
            if (!Global.checkFlag(Flag.Maya)) {
                Roster.scaleOpponent(ID.MAYA)
                Global.flag(Flag.Maya)
            }
            val maya = Roster[ID.MAYA]
            maya.scaleLevel(Roster[ID.PLAYER].level + 20)
            lineup.add(maya)
            match = Match(lineup, matchmod)
        } else if (available.size > 5) {
            match = Match(calculateLineup(matchNumber), matchmod)
        } else {
            match = Match(available, matchmod)
        }
        //Global.buildActionPool()
        Global.populateTargetedActions(lineup)
        if (matchmod != Modifier.quiet) {
            match!!.round()
        }
    }

    fun isAvailable(npc: ID): Boolean {
        var available = Roster.exists(npc)
        if (isMatchNight && time.hour >= 10) {
            if (match!!.combatants.contains(Roster[npc])) {
                available = false
            }
        }
        return available
    }

    fun isAvailable(npc: ID, time: LocalTime): Boolean {
        var available = Roster.exists(npc)
        if (isMatchNight && time.hour >= 10) {
            if (match!!.combatants.contains(Roster[npc])) {
                available = false
            }
        }
        return available
    }

    val available: ArrayList<Character>
        get() {
            val available = ArrayList<Character>()
            for (person in Roster.existingPlayers) {
                if (isAvailable(person.id)) {
                    available.add(person)
                }
            }
            return available
        }

    fun getAvailable(time: LocalTime): ArrayList<Character> {
        val available = ArrayList<Character>()
        for (person in Roster.existingPlayers) {
            if (isAvailable(person.id, time)) {
                available.add(person)
            }
        }
        return available
    }

    val isMatchNight: Boolean
        get() = date % 7 != 0 && matchNumber <= Constants.SEASONLENGTH + 1



    fun advanceTime(diff: LocalTime) {
        time = time.plusHours(diff.hour.toLong())
        time = time.plusMinutes(diff.minute.toLong())
    }

    fun displayScores(): String {
        val display = StringBuilder()
        val ordered = rankParticipants()
        for (c in ordered) {
            display.append(c.name + ": " + getScore(c.id) + " points.")
            display.append("<br>")
        }
        return display.toString()
    }

    fun rankParticipants(): ArrayList<Character> {
        val result = ArrayList<Character>()
        var inserted: Boolean
        for (combatant in Roster.combatants) {
            inserted = false
            for (i in result.indices) {
                if (getScore(combatant.id) > getScore(result[i].id)) {
                    result.add(i, combatant)
                    inserted = true
                    break
                }
            }
            if (!inserted) {
                result.add(combatant)
            }
        }
        return result
    }

    fun calculateLineup(round: Int): List<Character> {
        val available = Roster.combatants

        return LineUp.getLineup(available, matchNumber, Math.round(Global.getValue(Flag.SeasonNumber)).toInt())
    }

    fun hasMatch(player: Character): Boolean {
        if (!isMatchNight) {
            return false
        }
        return if (matchNumber > Constants.SEASONLENGTH) {
            player.human()
        } else {
            calculateLineup(matchNumber).contains(player)
        }
    }

    fun getScore(player: ID): Int {
        return score[player] ?: 0
    }

    fun addScore(player: ID, points: Int) {
        setScore(player, getScore(player) + points)
    }

    fun setScore(player: ID, points: Int) {
        score[player] = points
    }

    val timeString: String
        get() = LocalTime.parse(
            time.toString(),
            DateTimeFormatter.ofPattern("HH:mm"))
            .format(DateTimeFormatter.ofPattern("hh:mm a")
            )

    fun getDayString(day: Int): String {
        return when (day % 7) {
            1 -> "Monday"
            2 -> "Tuesday"
            3 -> "Wednesday"
            4 -> "Thursday"
            5 -> "Friday"
            6 -> "Saturday"
            else -> "Sunday"
        }
    }

    fun clearScores() {
        score.clear()
    }

    fun pause() {
        isPaused = true
    }

    fun unpause() {
        isPaused = false
        match?.round()
    }
}


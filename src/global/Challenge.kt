package global

import areas.Deployable
import characters.Attribute
import characters.Character
import characters.State
import combat.Combat
import combat.Result
import items.Envelope
import items.Toy

class Challenge(private val match: Match) : Deployable {
    override var owner: Character? = null
    private lateinit var target: Character
    private var goal: GOAL? = null
    var done: Boolean = false
    fun pick(): GOAL {
        val available = ArrayList<GOAL>()
        if (!target.isTopless && !target.isPantsless) {
            available.add(GOAL.clothedwin)
        }
        if (owner!!.getPure(Attribute.Seduction) >= 20) {
            available.add(GOAL.analwin)
        }
        if (owner!!.getAffection(target) >= 10) {
            available.add(GOAL.kisswin)
            available.add(GOAL.pendraw)
        }
        if (target.has(Toy.Strapon) || target.has(Toy.Strapon2) || target.hasDick) {
            available.add(GOAL.peggedloss)
        }
        available.add(GOAL.pendomwin)
        available.add(GOAL.subwin)
        return available.random()
    }

    fun message(): String {
        return when (goal) {
            GOAL.kisswin -> target.name + " seems pretty head over heels for you, at least to my eyes. <b>I bet she'll climax if you give her a good kiss.</b> Give it a try."
            GOAL.clothedwin -> "Not everyone relies on brute force to get their opponents off. The masters of seduction often don't bother to even undress their opponents. <b>See " +
                    "if you can make " + target.name + " cum while she's still got her clothes on.</b>"

            GOAL.bathambush -> ""
            GOAL.peggedloss -> "<b>Getting pegged in the ass is a hell of a thing, isn't it. I sympathize... especially since " + target.name + " seems to have it in for you tonight.</b> If it " +
                    "happens, I'll see that you're compensated."

            GOAL.analwin -> target.name + " has been acting pretty cocky lately. <b>If you can make her cum while fucking her in the ass, she should learn some humility.</b>"
            GOAL.pendomwin -> "How good are you exactly? <b>If you want to show " + target.name + " that you're the best, make her cum while giving her a good fucking.</b>"
            GOAL.pendraw -> "Some things are better than winning, like cumming together with your sweetheart. <b>You and " + target.name + " seem pretty sweet.</b>"
            GOAL.subwin -> "Everyone loves an underdog. <b>If you can win a fight with " + target.name + " when she thinks you're at her mercy, you'll get a sizeable bonus.</b>"
            else -> ""
        }
    }

    fun summary(): String {
        return when (goal) {
            GOAL.kisswin -> "Kiss " + target.name
            GOAL.clothedwin -> target.name + " with clothes on"
            GOAL.bathambush -> ""
            GOAL.peggedloss -> "Pegged by " + target.name
            GOAL.analwin -> target.name + " anal"
            GOAL.pendomwin -> "Fuck " + target.name
            GOAL.pendraw -> target.name + " draw"
            GOAL.subwin -> target.name + " from bottom"
            else -> ""

        }
    }

    enum class GOAL {
        kisswin,
        clothedwin,
        bathambush,
        peggedloss,
        analwin,
        pendomwin,
        pendraw,
        subwin
    }

    fun check(state: Combat, victor: Character?) {
        if (!done && (state.p1 === target || state.p2 === target)) {
            when (goal) {
                GOAL.kisswin -> if (victor === owner && state.lastact(owner!!).toString().contains("Kiss")) {
                    done = true
                }

                GOAL.clothedwin -> if (victor === owner && !target.isTopless && !target.isPantsless) {
                    done = true
                }

                GOAL.bathambush -> {}
                GOAL.peggedloss -> if (target === victor && state.state == Result.anal) {
                    done = true
                }

                GOAL.analwin -> if (owner === victor && state.state == Result.anal) {
                    done = true
                }

                GOAL.pendomwin -> if (owner === victor && state.state == Result.intercourse) {
                    done = true
                }

                GOAL.pendraw -> if (state.state == Result.intercourse) {
                    done = true
                }

                GOAL.subwin -> if (victor === owner && state.stance.sub(owner!!)) {
                    done = true
                }

                else -> {}
            }
        }
    }

    override fun resolve(active: Character) {
        if (active.state == State.ready) {
            this.owner = active
            target = match.combatants.filter { it !== active }.random()
            goal = pick()
            if (active.human()) {
                Global.gui.message(
                    "You find a gold envelope sitting conspicuously in the middle of the room. You open it up and read the note inside.\n'${message()}'\n"
                )
            }
            active.location.remove(this)
            active.accept(this)
            active.gain(Envelope(this))
        }
    }


    fun reward(): Int {
        return when (goal) {
            GOAL.kisswin, GOAL.clothedwin -> 250
            GOAL.peggedloss -> 1000
            GOAL.pendomwin -> 300
            else -> 500
        }
    }

    override val priority = 1
}

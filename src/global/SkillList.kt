package global

import actions.*
import characters.Attribute
import characters.Dummy
import characters.Trait
import items.Flask
import items.Potion
import pet.Ptype
import skills.*
import trap.*
import utilities.EnumMap

object SkillList {
    val dummy = Dummy("dummy")
    val allSkills: Set<Skill> by lazy {
        val skillList = HashSet<Skill>()

        //Seduction
        skillList.add(Kiss(dummy))
        skillList.add(FondleBreasts(dummy))
        skillList.add(Fuck(dummy))
        skillList.add(Tickle(dummy))
        skillList.add(Blowjob(dummy))
        skillList.add(Cunnilingus(dummy))
        skillList.add(LickNipples(dummy))
        skillList.add(Paizuri(dummy))
        skillList.add(SuckNeck(dummy))
        skillList.add(Whisper(dummy))
        skillList.add(Footjob(dummy))
        skillList.add(Handjob(dummy))
        skillList.add(Finger(dummy))
        skillList.add(Piston(dummy))
        skillList.add(Grind(dummy))
        skillList.add(Thrust(dummy))
        skillList.add(Carry(dummy))
        skillList.add(Tighten(dummy))
        skillList.add(AssFuck(dummy))
        skillList.add(Frottage(dummy))
        skillList.add(FingerAss(dummy))

        //Cunning
        skillList.add(StripTop(dummy))
        skillList.add(StripBottom(dummy))
        skillList.add(Escape(dummy))
        skillList.add(Maneuver(dummy))
        skillList.add(Reversal(dummy))
        skillList.add(Taunt(dummy))
        skillList.add(Trip(dummy))
        skillList.add(Turnover(dummy))

        //Power
        skillList.add(Shove(dummy))
        skillList.add(Slap(dummy))
        skillList.add(ArmBar(dummy))
        skillList.add(Flick(dummy))
        skillList.add(Knee(dummy))
        skillList.add(LegLock(dummy))
        skillList.add(Restrain(dummy))
        skillList.add(Spank(dummy))
        skillList.add(Stomp(dummy))
        skillList.add(Tackle(dummy))
        skillList.add(Kick(dummy))
        skillList.add(Squeeze(dummy))
        skillList.add(Nurple(dummy))
        skillList.add(Tear(dummy))

        //Arcane
        skillList.add(MagicMissile(dummy))
        skillList.add(Binding(dummy))
        skillList.add(Illusions(dummy))
        skillList.add(NakedBloom(dummy))
        skillList.add(Barrier(dummy))
        skillList.add(MageArmor(dummy))
        skillList.add(ManaFortification(dummy))

        //Dark
        skillList.add(LustAura(dummy))
        skillList.add(TailPeg(dummy))
        skillList.add(Dominate(dummy))
        skillList.add(DarkTendrils(dummy))
        skillList.add(Sacrifice(dummy))
        skillList.add(Fly(dummy))
        skillList.add(Drain(dummy))
        skillList.add(LustOverflow(dummy))

        //Ki
        skillList.add(WaterForm(dummy))
        skillList.add(FlashStep(dummy))
        skillList.add(FlyCatcher(dummy))
        skillList.add(StoneForm(dummy))
        skillList.add(FireForm(dummy))
        skillList.add(IceForm(dummy))
        skillList.add(Burst(dummy))
        skillList.add(PleasureBomb(dummy))


        //Science
        skillList.add(StunBlast(dummy))
        skillList.add(ShrinkRay(dummy))
        skillList.add(ShortCircuit(dummy))
        skillList.add(Defabricator(dummy))
        skillList.add(Fabricator(dummy))
        skillList.add(MatterConverter(dummy))


        //Fetish
        skillList.add(Masochism(dummy))
        skillList.add(Bondage(dummy))
        skillList.add(TentaclePorn(dummy))
        skillList.add(FaceFuck(dummy))
        skillList.add(BondageStraps(dummy))
        skillList.add(TortoiseWrap(dummy))
        skillList.add(Nymphomania(dummy))


        //Animism
        skillList.add(CatsGrace(dummy))
        skillList.add(TailJob(dummy))
        skillList.add(Purr(dummy))
        skillList.add(PheromoneOverdrive(dummy))
        skillList.add(BeastTF(dummy))


        //Ninjutsu
        skillList.add(StealClothes(dummy))
        skillList.add(BunshinAssault(dummy))
        skillList.add(BunshinService(dummy))
        skillList.add(Substitute(dummy))
        skillList.add(GoodnightKiss(dummy))
        skillList.add(KoushoukaBurst(dummy))


        //Submissive
        skillList.add(Dive(dummy))
        skillList.add(Cowardice(dummy))
        skillList.add(Stumble(dummy))
        skillList.add(Invite(dummy))
        skillList.add(Beg(dummy))
        skillList.add(ShamefulDisplay(dummy))
        skillList.add(Buck(dummy))
        skillList.add(HoneyTrap(dummy))
        skillList.add(PleasureSlave(dummy))

        //Discipline
        skillList.add(WarningCrop(dummy))
        skillList.add(MastersOrder(dummy))
        skillList.add(DominatingGaze(dummy))
        skillList.add(BerserkerBarrage(dummy))
        skillList.add(RegainComposure(dummy))
        skillList.add(Punishment(dummy))

        //Footballer
        skillList.add(ChargeFeint(dummy))
        skillList.add(PenaltyShot(dummy))
        skillList.add(KissBetter(dummy))
        skillList.add(PenaltyKick(dummy))
        skillList.add(SpreadThighs(dummy))
        skillList.add(OpenGoal(dummy))
        skillList.add(HandBall(dummy))


        //Professional
        skillList.add(Blindside(dummy))


        //Hypnosis
        skillList.add(Suggestion(dummy))
        skillList.add(HeightenSenses(dummy))
        skillList.add(Humiliate(dummy))
        skillList.add(EnflameLust(dummy))
        skillList.add(CrushPride(dummy))


        //Temporal
        skillList.add(WindUp(dummy))
        skillList.add(Haste(dummy))
        skillList.add(CheapShot(dummy))
        skillList.add(AttireShift(dummy))
        skillList.add(EmergencyJump(dummy))
        skillList.add(Unstrip(dummy))
        skillList.add(Rewind(dummy))

        //Contender
        skillList.add(DramaticPose(dummy))
        skillList.add(SecondWind(dummy))
        skillList.add(Soulmate(dummy))

        //Spiritual
        skillList.add(Tranquility(dummy))
        skillList.add(CleanseMind(dummy))
        skillList.add(Banish(dummy))
        skillList.add(Purge(dummy))
        skillList.add(InnerPeace(dummy))

        //Unknowble
        skillList.add(DecayClothes(dummy))
        skillList.add(Vertigo(dummy))
        skillList.add(LustRelease(dummy))
        skillList.add(EcstasyWave(dummy))
        skillList.add(Blink(dummy))


        //Special
        skillList.add(PerfectTouch(dummy))
        skillList.add(HipThrow(dummy))
        skillList.add(SpiralThrust(dummy))
        skillList.add(Bravado(dummy))
        skillList.add(Diversion(dummy))


        //Item
        skillList.add(Tie(dummy))
        skillList.add(UseDildo(dummy))
        skillList.add(UseOnahole(dummy))
        skillList.add(UseCrop(dummy))
        skillList.add(Undress(dummy))
        skillList.add(Strapon(dummy))
        skillList.add(VibroTease(dummy))
        skillList.add(Scroll(dummy))
        skillList.add(DarkTalisman(dummy))
        skillList.add(Needle(dummy))
        skillList.add(UsePowerband(dummy))
        skillList.add(UseExcalibur(dummy))
        skillList.add(UseClamps(dummy))
        skillList.add(InsertBead(dummy))
        skillList.add(PullBeads(dummy))

        //Misc
        skillList.add(Release(dummy))
        skillList.add(Masturbate(dummy))
        skillList.add(FaceSit(dummy))
        skillList.add(GoldCock(dummy))
        skillList.add(LowBlow(dummy))
        skillList.add(KittyKick(dummy))
        skillList.add(Rotate(dummy))


        //Pets
        skillList.add(SpawnFaerie(dummy, Ptype.fairyfem))
        skillList.add(SpawnImp(dummy, Ptype.impfem))
        skillList.add(SpawnFaerie(dummy, Ptype.fairymale))
        skillList.add(SpawnImp(dummy, Ptype.impmale))
        skillList.add(SpawnSlime(dummy))
        skillList.add(SpawnFGoblin(dummy))


        //Commands
        skillList.add(Command(dummy))
        skillList.add(Obey(dummy))
        skillList.add(CommandDismiss(dummy))
        skillList.add(CommandDown(dummy))
        skillList.add(CommandGive(dummy))
        skillList.add(CommandHurt(dummy))
        skillList.add(CommandInsult(dummy))
        skillList.add(CommandMasturbate(dummy))
        skillList.add(CommandOral(dummy))
        skillList.add(CommandStrip(dummy))
        skillList.add(CommandStripPlayer(dummy))
        skillList.add(CommandUse(dummy))


        //EX
        skillList.add(KissEX(dummy))
        skillList.add(KneeEX(dummy))
        skillList.add(HandjobEX(dummy))
        skillList.add(StripTopEX(dummy))
        skillList.add(StripBottomEX(dummy))
        skillList.add(FingerEX(dummy))
        skillList.add(ThrustEX(dummy))

        skillList
    }

    val skillRequirements: Map<Skill, Map<Attribute, Int>> by lazy {
        val skillRequirements = HashMap<Skill, Map<Attribute, Int>>()
        for (skill in allSkills) {
            skillRequirements[skill] = skill.attRequired()
        }
        skillRequirements
    }

    val skillsByAttribute: Map<Attribute, Map<Skill, Int>> by lazy {
        val attMap = EnumMap<Attribute, HashMap<Skill, Int>>()
        for(att in Attribute.entries) {
            attMap[att] = HashMap()
        }
        for ((skill, attInfo) in skillRequirements) {
            for ((att, level) in attInfo) {
                attMap[att]!![skill] = level
            }
        }
        attMap
    }

    val allFeats: Set<Trait> by lazy {
        val feats = HashSet<Trait>()
        feats.add(Trait.sprinter)
        feats.add(Trait.QuickRecovery)
        feats.add(Trait.rapidrecovery)
        feats.add(Trait.Sneaky)
        feats.add(Trait.Confident)
        feats.add(Trait.SexualGroove)
        feats.add(Trait.BoundlessEnergy)
        feats.add(Trait.Unflappable)
        feats.add(Trait.resourceful)
        feats.add(Trait.treasureSeeker)
        feats.add(Trait.sympathetic)
        feats.add(Trait.leadership)
        feats.add(Trait.tactician)
        feats.add(Trait.PersonalInertia)
        feats.add(Trait.fitnessNut)
        feats.add(Trait.expertGoogler)
        feats.add(Trait.mojoMaster)
        feats.add(Trait.fastLearner)
        feats.add(Trait.veryfastLearner)
        feats.add(Trait.cautious)
        feats.add(Trait.responsive)
        feats.add(Trait.assmaster)
        feats.add(Trait.Clingy)
        feats.add(Trait.freeSpirit)
        feats.add(Trait.houdini)
        feats.add(Trait.coordinatedStrikes)
        feats.add(Trait.evasiveManuevers)
        feats.add(Trait.handProficiency)
        feats.add(Trait.handExpertise)
        feats.add(Trait.handMastery)
        feats.add(Trait.oralProficiency)
        feats.add(Trait.oralExpertise)
        feats.add(Trait.oralMastery)
        feats.add(Trait.intercourseProficiency)
        feats.add(Trait.intercourseExpertise)
        feats.add(Trait.intercourseMastery)
        feats.add(Trait.footloose)
        feats.add(Trait.amateurMagician)
        feats.add(Trait.bountyHunter)
        feats.add(Trait.challengeSeeker)
        feats.add(Trait.showoff)
        feats.add(Trait.tieGuy)
        feats.add(Trait.staydown)
        feats.add(Trait.opportunist)
        feats.add(Trait.tracker)
        feats.add(Trait.advtracker)
        feats.add(Trait.mastertracker)
        feats
    }

    val allActions: List<Action> by lazy {
        val actions = ArrayList<Action>()
        actions.add(Resupply())
        actions.add(Bathe())
        actions.add(Scavenge())
        actions.add(Craft())
        actions.add(Use(Flask.Lubricant))
        actions.add(Recharge())
        actions.add(JerkOff())
        actions.add(Energize())
        actions.add(Hide())
        for (p in Potion.entries) {
            actions.add(Drink(p))
        }
        for (t in allTraps) {
            actions.add(SetTrap(t))
        }
        actions.add(Wait())
        actions
    }

    val allTraps: List<Trap> by lazy {
        val trapPool = ArrayList<Trap>()
        trapPool.add(Alarm())
        trapPool.add(Tripline())
        trapPool.add(Snare())
        trapPool.add(SpringTrap())
        trapPool.add(AphrodisiacTrap())
        trapPool.add(DissolvingTrap())
        trapPool.add(Decoy())
        trapPool.add(Spiderweb())
        trapPool.add(EnthrallingTrap())
        trapPool.add(IllusionTrap())
        trapPool.add(StripMine())
        trapPool.add(TentacleTrap())
        trapPool
    }
}
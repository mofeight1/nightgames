package global

import actions.Movement
import areas.Area
import areas.Cache
import areas.MapDrawHint
import characters.Character
import characters.ID
import characters.State
import characters.Trait
import combat.Combat
import items.Trophy
import java.awt.Rectangle
import java.time.LocalTime
import kotlin.math.roundToInt

open class Match(combatants: List<Character>, condition: Modifier) {
    private var dropOffTime: Int
    private var cacheDelay = 0
    private var matchend: LocalTime
    private val map: HashMap<Movement, Area>
    var combatants: ArrayList<Character>
    private val score = HashMap<Character, Int>()
    private var clothingscore: HashMap<Character, Int> = HashMap()
    private var index = 0
    var condition: Modifier
    private var quit: Boolean

    init {
        this.condition = condition
        this.combatants = ArrayList(combatants)
        matchend = LocalTime.of(1, 0)
        if (Global.checkFlag(Flag.shortmatches)) {
            matchend = LocalTime.of(0, 0)
        }
        cacheDelay = if (Global.checkFlag(Flag.MoreCache1)) {
            4
        } else {
            6
        }
        map = buildMap()
        Global.gui.startMatch()
        dropOffTime = 0
        Scheduler.unpause()
        quit = false
        this.combatants[0].place(map[Movement.dorm]!!)
        this.combatants[1].place(map[Movement.engineering]!!)
        if (this.combatants.size >= 3) {
            this.combatants[2].place(map[Movement.la]!!)
        }
        if (this.combatants.size >= 4) {
            this.combatants[3].place(map[Movement.dining]!!)
        }
        if (this.combatants.size >= 5) {
            this.combatants[4].place(map[Movement.union]!!)
        }
        for (combatant in combatants) {
            score[combatant] = 0
            combatant.matchPrep(this)
        }
    }

    fun automate(until: LocalTime) {
        while (Scheduler.time != until) {
            if (index >= combatants.size) {
                index = 0
                if (meanLvl() > 5 && dropOffTime >= cacheDelay) {
                    dropPackage()
                    dropOffTime = 0
                }
                if (Global.checkFlag(Flag.challengeAccepted) && (Scheduler.time.minute % 15 == 0)) {
                    dropChallenge()
                }
                Scheduler.advanceTime(LocalTime.of(0, 5))
                dropOffTime++
            }
            while (index < combatants.size) {
                if (combatants[index].state != State.quit) {
                    combatants[index].upkeep()
                    if (combatants[index].human()) {
                        manageConditions(combatants[index])
                    }
                    combatants[index].move(this)
                    if (Global.debug) {
                        println(combatants[index].name + " is in " + combatants[index].location.name)
                    }
                }
                index++
            }
        }
        //end();
    }

    fun round() {
        while (Scheduler.time.isBefore(matchend) || Scheduler.time
                .isAfter(LocalTime.of(20, 0)) || Global.player.isBusy
        ) {
            if (index >= combatants.size) {
                index = 0
                if (meanLvl() > 5 && dropOffTime >= cacheDelay) {
                    dropPackage()
                    dropOffTime = 0
                }
                if (Global.checkFlag(Flag.challengeAccepted) && (Scheduler.time.minute % 15 == 0)) {
                    dropChallenge()
                }
                Scheduler.advanceTime(LocalTime.of(0, 5))
                dropOffTime++
            }
            while (index < combatants.size) {
                Global.gui.refresh()
                if (combatants[index].state != State.quit) {
                    combatants[index].upkeep()
                    if (combatants[index].human()) {
                        manageConditions(combatants[index])
                    }
                    combatants[index].move(this)
                    if (Global.debug) {
                        println(combatants[index].name + " is in " + combatants[index].location.name)
                    }
                }
                index++
                if (Scheduler.isPaused) {
                    return
                }
            }
            for (location in map.values) {
                location.age()
            }
        }
        end()
    }

    fun end() {
        for (next in combatants) {
            next.finishMatch()
        }
        if (condition != Modifier.quiet || quit) {
            Global.gui.message("Tonight's match is over.")
        }
        var cloth: Int
        var creward: Int
        var trophies: Int
        clothingscore = HashMap()
        val clothingmoney = HashMap<Character, Int>()
        //val player: Character? = combatants.firstOrNull { it.human() }
        val player = Global.player
        for (combatant in combatants) {
            trophies = 0
            cloth = 0
            for (t in Trophy.entries) {
                while (combatant.has(t)) {
                    combatant.remove(t, 1)
                    cloth += combatant.income(combatant.prize())
                    trophies++
                }
            }
            clothingscore[combatant] = trophies
            clothingmoney[combatant] = cloth
        }
        val order = rankParticipants(score)
        val winsmoney = HashMap<Character, Int>()
        val challengemoney = HashMap<Character, Int>()
        for (combatant in order) {
            if (condition != Modifier.quiet || quit) {
                Global.gui.message(combatant.name + " scored " + score[combatant] + " victories.")
            }
            winsmoney[combatant] = combatant.income(score[combatant]!! * combatant.prize())
            creward = 0
            for (c in combatant.challenges) {
                if (c.done) {
                    creward += c.reward() + (c.reward() * 3 * combatant.rank)
                    if (combatant.has(Trait.bountyHunter)) {
                        creward *= 2
                    }
                    if (combatant.human()) {
                        Global.modCounter(Flag.ChallengesCompleted, 1.0)
                    }
                }
            }
            creward = combatant.income(creward)
            challengemoney[combatant] = creward
            combatant.challenges.clear()
            combatant.state = State.ready
            combatant.change(Modifier.normal)
        }
        if (condition != Modifier.quiet || quit) {
            Global.gui.message("You made $" + winsmoney[player] + " for defeating opponents.")
            if (!Global.checkFlag(Flag.challengemode)) {
                val bonus = player.income((score[player]!! * player.prize() * condition.bonus).roundToInt())
                if (bonus > 0) {
                    Global.gui.message("You earned an additional $$bonus for accepting the handicap.")
                }
            }
            if (order[0] === player) {
                Global.gui.message("You also earned a bonus of $" + 5 * player.prize() + " for placing first.")
                Global.flag(Flag.victory)
                Global.modCounter(Flag.MatchWins, 1.0)
            }
            Global.gui
                .message("You traded in " + clothingscore[player] + " sets of clothes for a total of $" + clothingmoney[player] + ".")
            if (challengemoney[player]!! > 0) {
                Global.gui
                    .message("You also discover an envelope with $" + challengemoney[player] + " slipped under the door to your room. Presumably it's payment for completed challenges.")
            }
            Global.gui.message("<p>")
        }
        if (Scheduler.matchNumber <= Constants.SEASONLENGTH) {
            order[0].income(5 * order[0].prize())
            Scheduler.addScore(order[0].id, 5)
            Scheduler.addScore(order[1].id, 3)
            Scheduler.addScore(order[2].id, 2)
            Scheduler.addScore(order[3].id, 1)
            if (Scheduler.matchNumber == Constants.SEASONLENGTH) {
                Global.gui
                    .message("The season has concluded. There will be a small award ceremony for the champion tomorrow morning.")
            } else {
                Global.gui.message(
                    "There are still " + (Constants.SEASONLENGTH - Scheduler.matchNumber) + " matches left in the season.<br>" +
                            "The new rankings are:<br>" + Scheduler.displayScores()
                )
            }
        } else {
            Global.gui
                .message("This match wasn't part of a season, so the results do not matter beyond tonight's prize. The new season will start with a clean slate next week.")
        }
        Scheduler.matchNumber++
        Postmatch(player, combatants)
    }

    private fun rankParticipants(score: HashMap<Character, Int>): ArrayList<Character> {
        val result = ArrayList<Character>()
        var inserted: Boolean
        for (combatant in score.keys) {
            inserted = false
            for (i in result.indices) {
                if (score[combatant]!! > score[result[i]]!! ||
                    (score[combatant] === score[result[i]] && clothingscore[combatant]!! > clothingscore[result[i]]!!)
                ) {
                    result.add(i, combatant)
                    inserted = true
                    break
                }
            }
            if (!inserted) {
                result.add(combatant)
            }
        }
        return result
    }

    fun gps(dest: Movement): Area? {
        return map[dest]
    }

    fun score(character: Character, points: Int) {
        score[character] = score[character]!! + points
    }

    fun manageConditions(player: Character) {
        if (condition == Modifier.vibration) {
            player.tempt(5.0)
        }
    }

    fun meanLvl(): Int {
        val total = combatants.sumOf { it.level }.toDouble()
        return (total / combatants.size).roundToInt()
    }

    fun dropPackage() {
        val areas = map.values.filter { !it.corridor && !it.open && it.env.size < 5 }
        val target = areas.random()
        target.place(Cache(Global.player.rank))
        if (Global.checkFlag(Flag.CacheFinder) && condition != Modifier.quiet) {
            Global.gui.message("You receive a text that a new cache has been placed in " + target.name)
        }
    }

    fun dropChallenge() {
        val areas = map.values.filter { !it.open && it.env.size < 5 }
        val target = areas.random()
        target.place(Challenge(this))
    }

    fun buildCombat(p1: Character, p2: Character): Combat {
        return Combat(p1, p2, listOf(condition))
    }

    fun quit() {
        Global.gui.clearText()
        val human = Roster[ID.PLAYER]
        if (human.state == State.combat) {
            human.location.encounter!!.combat
            if (human.location.encounter!!.combat != null) {
                human.location.encounter!!.combat!!.forfeit(human)
            }
            human.location.endEncounter()
        }
        human.travel(Area("", "", Movement.retire, this))
        human.state = State.quit
        condition = Modifier.quiet
        quit = true
        Scheduler.unpause()
    }

    val areas: Collection<Area>
        get() = map.values

    fun buildMap(): HashMap<Movement, Area> {
        val quad = Area(
            "Quad",
            "You are in the <b>Quad</b> that sits in the center of the Dorm, the Dining Hall, the Engineering Building, and the Liberal Arts Building. There's "
                    + "no one around at this time of night, but the Quad is well-lit and has no real cover. You can probably be spotted from any of the surrounding buildings, so it may "
                    + "not be a good idea to hang out here for long.",
            Movement.quad, MapDrawHint(Rectangle(10, 3, 7, 9), "Quad", false), this
        )
        val dorm = Area(
            "Dorm",
            "You are in the <b>Dorm</b>. Everything is quieter than it would be in any other dorm this time of night. You've been told the entire first floor "
                    + "is empty during match hours, but you wouldn't be surprised if a few of the residents are hiding in their rooms, peeking at the fights. You've stashed some clothes "
                    + "in one of the rooms you're sure is empty, which is common practice for most of the competitors.",
            Movement.dorm, MapDrawHint(Rectangle(14, 12, 3, 5), "Dorm", false), this
        )
        val shower = Area(
            "Showers",
            "You are in the first floor <b>Showers</b>. There are a half-dozen stalls shared by the residents on this floor. They aren't very big, but there's "
                    + "room to hide if need be. A hot shower would help you recover after a tough fight, but you'd be vulnerable if someone finds you.",
            Movement.shower, MapDrawHint(Rectangle(13, 17, 4, 2), "Showers", false), this
        )
        val laundry = Area(
            "Laundry Room",
            "You are in the <b>Laundry Room</b> in the basement of the Dorm. Late night is prime laundry time in your dorm, but none of these machines "
                    + "are running. You're a bit jealous when you notice that the machines here are free, while yours are coin-op. There's a tunnel here that connects to the basement of the "
                    + "Dining Hall.",
            Movement.laundry, MapDrawHint(Rectangle(17, 15, 8, 2), "Laundry", false), this
        )
        val engineering = Area(
            "Engineering Building",
            "You are in the Science and <b>Engineering Building</b>. Most of the lecture rooms are in other buildings; this one is mostly "
                    + "for specialized rooms and labs. The first floor contains workshops mostly used by the Mechanical and Electrical Engineering classes. The second floor has "
                    + "the Biology and Chemistry Labs. There's a third floor, but that's considered out of bounds.",
            Movement.engineering, MapDrawHint(Rectangle(10, 0, 7, 3), "Eng", false), this
        )
        val lab = Area(
            "Chemistry Lab",
            "You are in the <b>Chemistry Lab</b>. The shelves and cabinets are full of all manner of dangerous and/or interesting chemicals. A clever enough "
                    + "person could combine some of the safer ones into something useful. Just outside the lab is a bridge connecting to the library.",
            Movement.lab, MapDrawHint(Rectangle(0, 0, 10, 3), "Lab", false), this
        )
        val workshop = Area(
            "Workshop",
            "You are in the Mechanical Engineering <b>Workshop</b>. There are shelves of various mechanical components and the back table is covered "
                    + "with half-finished projects. A few dozen Mechanical Engineering students use this workshop each week, but it's well stocked enough that no one would miss "
                    + "some materials that might be of use to you.",
            Movement.workshop, MapDrawHint(Rectangle(17, 0, 8, 3), "Workshop", false), this
        )
        val libarts = Area(
            "Liberal Arts Building",
            "You are in the <b>Liberal Arts Building</b>. There are three floors of lecture halls and traditional classrooms, but only "
                    + "the first floor is in bounds. The Library is located directly out back, and the side door is just a short walk from the pool.",
            Movement.la, MapDrawHint(Rectangle(5, 5, 5, 7), "Lib Arts", false), this
        )
        val pool = Area(
            "Pool",
            "You are by the indoor <b>Pool</b>, which is connected to the Student Union for reasons that no one has ever really explained. The pool is quite "
                    + "large and there is even a jacuzzi. A quick soak would feel good, but the lack of privacy is a concern. The side doors are locked at this time of night, but the "
                    + "door to the Student Union is open and there's a back door that exits near the Liberal Arts building. Across the water in the other direction is the Courtyard.",
            Movement.pool, MapDrawHint(Rectangle(6, 12, 4, 2), "Pool", false), this
        )
        val library = Area(
            "Library",
            "You are in the <b>Library</b>. It's a two floor building with an open staircase connecting the first and second floors. The front entrance leads to "
                    + "the Liberal Arts building. The second floor has a Bridge connecting to the Chemistry Lab in the Science and Engineering building.",
            Movement.library, MapDrawHint(Rectangle(0, 8, 5, 12), "Library", false), this
        )
        val dining = Area(
            "Dining Hall",
            "You are in the <b>Dining Hall</b>. Most students get their meals here, though some feel it's worth the extra money to eat out. The "
                    + "dining hall is quite large and your steps echo on the linoleum, but you could probably find someplace to hide if you need to.",
            Movement.dining, MapDrawHint(Rectangle(17, 6, 4, 6), "Dining", false), this
        )
        val kitchen = Area(
            "Kitchen",
            "You are in the <b>Kitchen</b> where student meals are prepared each day. The industrial fridge and surrounding cabinets are full of the "
                    + "ingredients for any sort of bland cafeteria food you can imagine. Fortunately, you aren't very hungry. There's a chance you might be able to cook up some "
                    + "of the more obscure items into something useful.",
            Movement.kitchen, MapDrawHint(Rectangle(18, 12, 4, 2), "Kitchen", false), this
        )
        val storage = Area(
            "Storage Room",
            "You are in a <b>Storage Room</b> under the Dining Hall. It's always unlocked and receives a fair bit of foot traffic from students "
                    + "using the tunnel to and from the Dorm, so no one keeps anything important in here. There's enough junk down here to provide some hiding places and there's a chance "
                    + "you could find something useable in one of these boxes.",
            Movement.storage, MapDrawHint(Rectangle(21, 6, 4, 5), "Storage", false), this
        )
        val tunnel = Area(
            "Tunnel",
            "You are in the <b>Tunnel</b> connecting the dorm to the dining hall. It doesn't get a lot of use during the day and most of the freshmen "
                    + "aren't even aware of its existence, but many upperclassmen have been thankful for it on cold winter days and it's proven to be a major tactical asset. The "
                    + "tunnel is well-lit and doesn't offer any hiding places.",
            Movement.tunnel, MapDrawHint(Rectangle(23, 11, 2, 4), "Tunnel", true), this
        )
        val bridge = Area(
            "Bridge",
            "You are on the <b>Bridge</b> connecting the second floors of the Science and Engineering Building and the Library. It's essentially just a "
                    + "corridor, so there's no place for anyone to hide.",
            Movement.bridge, MapDrawHint(Rectangle(0, 3, 2, 5), "Bridge", true), this
        )
        val sau = Area(
            "Student Union",
            "You are in the <b>Student Union</b>, which doubles as base of operations during match hours. You and the other competitors can pick up "
                    + "a change of clothing here.",
            Movement.union, MapDrawHint(Rectangle(10, 12, 3, 5), "S.Union", true), this
        )
        quad.link(dorm)
        quad.link(engineering)
        quad.link(libarts)
        quad.link(dining)
        quad.link(sau)
        dorm.link(shower)
        dorm.link(laundry)
        dorm.link(quad)
        shower.link(dorm)
        laundry.link(dorm)
        laundry.link(tunnel)
        engineering.link(quad)
        engineering.link(lab)
        engineering.link(workshop)
        workshop.link(engineering)
        workshop.shortcut(pool)
        lab.link(engineering)
        lab.link(bridge)
        lab.jump(dining)
        libarts.link(quad)
        libarts.link(library)
        libarts.link(pool)
        pool.link(libarts)
        pool.link(sau)
        pool.shortcut(workshop)
        library.link(libarts)
        library.link(bridge)
        library.shortcut(tunnel)
        dining.link(quad)
        dining.link(storage)
        dining.link(kitchen)
        kitchen.link(dining)
        storage.link(dining)
        storage.link(tunnel)
        tunnel.link(storage)
        tunnel.link(laundry)
        tunnel.shortcut(library)
        bridge.link(lab)
        bridge.link(library)
        bridge.jump(quad)
        sau.link(pool)
        sau.link(quad)
        val map = HashMap<Movement, Area>()
        map[Movement.quad] = quad
        map[Movement.dorm] = dorm
        map[Movement.shower] = shower
        map[Movement.laundry] = laundry
        map[Movement.engineering] = engineering
        map[Movement.workshop] = workshop
        map[Movement.lab] = lab
        map[Movement.la] = libarts
        map[Movement.pool] = pool
        map[Movement.library] = library
        map[Movement.dining] = dining
        map[Movement.kitchen] = kitchen
        map[Movement.storage] = storage
        map[Movement.tunnel] = tunnel
        map[Movement.bridge] = bridge
        map[Movement.union] = sau
        return map
    }
}

package status

import characters.Anatomy
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType

class Charmed(affected: Character?, duration: Int) : Status("Charmed", affected) {
    init {
        flag(Stsflag.charmed)
        flag(Stsflag.mindaffecting)
        tooltip = "Unable to act and susceptible to suggestion"
        this.duration = duration
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = Math.round(this.duration * 1.5f)
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You know that you should be fighting back, but it's so much easier to just surrender."
        } else {
            affected!!.name + " is flush with desire and doesn't seem interested in fighting back."
        }
    }

    override fun tempted(x: Double): Double {
        return 3.0
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Tempt) temp[DamageType.Tempt] = 3.0
        return temp
    }

    override fun escape(): Int {
        return -10
    }

    override fun counter(): Int {
        return -10
    }

    override fun copy(target: Character): Status {
        return Charmed(target, duration)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.horny, 15)
        decay(c)
    }
}

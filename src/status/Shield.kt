package status

import characters.Anatomy
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType

class Shield(affected: Character?, magnitude: Double) : Status("Shield", affected) {
    init {
        flag(Stsflag.shielded)
        this.duration = 4
        this.magnitude = magnitude
        stacking = true
        tooltip = "Pain resistance"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 6
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You are protected by an Arcane shield."
        } else {
            affected!!.name + " is protected by an Arcane shield."
        }
    }

    override fun damage(x: Double, area: Anatomy): Double {
        return -magnitude
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pain) temp[DamageType.Pain] = -magnitude
        return temp
    }

    override fun counter(): Int {
        return 2
    }

    override fun copy(target: Character): Status {
        return Shield(target, magnitude)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 5)
        decay(c)
    }
}

package status

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import kotlin.math.roundToInt

class Drowsy(affected: Character?, magnitude: Double) : Status("Drowsy", affected) {
    init {
        this.magnitude = magnitude
        lingering = true
        stacking = true
        this.duration = 4
        flag(Stsflag.drowsy)
        tooltip = "Stamina loss each turn, take reduced pleasure damage"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 6
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You feel lethargic and sluggish. You're struggling to remain standing"
        } else {
            affected!!.name + " looks extremely sleepy."
        }
    }

    override fun regen(): Double {
        return -magnitude
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return -(x * magnitude) / 12
    }

    override fun weakened(x: Double): Double {
        return (x * magnitude) / 12
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure)
            temp[DamageType.Pleasure] = -(amount * magnitude) / 12
        if (type == DamageType.Weaken)
            temp[DamageType.Weaken] = amount * magnitude / 12
        return temp
    }

    override fun copy(target: Character): Status {
        return Drowsy(target, magnitude)
    }

    override fun turn(c: Combat) {
        affected!!.buildMojo((-5 * magnitude).roundToInt())
        decay(c)
    }
}

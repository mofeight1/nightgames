package status

import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat

class Energized(affected: Character?, duration: Int) : Status("Energized", affected) {
    init {
        this.duration = duration
        lingering = true
        flag(Stsflag.energized)
        tooltip = "Gain 10% mojo each turn"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You're brimming with energy."
        } else {
            affected!!.name + "'s eyes seem to be faintly glowing."
        }
    }

    override fun copy(target: Character): Status {
        return Energized(target, duration)
    }

    override fun turn(c: Combat) {
        affected!!.buildMojo(10)
        affected!!.emote(Emotion.confident, 5)
        affected!!.emote(Emotion.dominant, 10)
        decay(c)
    }
}

package status

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import kotlin.math.min
import kotlin.math.roundToInt

class Composed(affected: Character, durability: Int, magnitude: Double) : Status("Composed", affected) {
    private var durability: Int

    init {
        this.magnitude = magnitude
        this.durability = durability
        flags.add(Stsflag.composed)
        tooltip = "Resist Pleasure until composure breaks. Endurance: ${durability}durability, Pleasure resistance: $magnitude%"
        if (!affected.human()) {
            traits.add(Trait.sportsmanship)
        }
        lingering = true
        if (affected.has(Trait.highcomposure)) {
            this.durability *= 2
        }
        if (affected.has(Trait.potentcomposure)) {
            this.magnitude = min(90.0, 2 * magnitude)
        }
        duration = 999
        decaying = false
    }

    override fun describe(): String {
        return ""
    }

    override fun turn(c: Combat) {
        if (affected!!.has(Stsflag.shamed)) {
            durability -= affected!!.getStatus(Stsflag.shamed)!!.magnitude.roundToInt()
        }
        if (affected!!.has(Stsflag.masochism)) {
            durability -= 1
        }
        if (affected!!.has(Stsflag.bondage)) {
            durability -= 1
        }
        if (affected!!.has(Stsflag.buzzed)) {
            durability -= 1
        }
        if (durability <= 0) {
            if (affected!!.human()) {
                c.write(
                    affected!!,
                    "Something snaps in your mind. Despite your best efforts, you can't keep your emotions in check any longer, and you let out a curse before you can stop yourself. Fuck it then. You've still got a match to win right now."
                )
            } else {
                c.write(
                    affected!!,
                    affected!!.name + "'s eyebrow twitches. Then twitches again. Her face scrunches up and she closes her eyes, finally letting out a fierce scream. <i>\"Fuck!\"</i> It seems she can't hold her emotions in check any longer."
                )
            }
            affected!!.removeStatus(this, c)
            affected!!.add(Broken(affected!!, magnitude), c)
        }
    }

    override fun damage(x: Double, area: Anatomy): Double {
        durability -= 1
        if (area == Anatomy.genitals) {
            durability -= 1
        }
        return 0.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        if (area == Anatomy.ass) {
            durability -= 2
        }
        return -x * magnitude / 100
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) {
            if (area == Anatomy.ass) {
                durability -= 2
            }
            temp[DamageType.Pleasure] = -amount * magnitude / 100
        }
        if (type == DamageType.Pain) {
            durability -= 1
            if (area == Anatomy.genitals) {
                durability -= 1
            }
        }
        return temp
    }

    override fun copy(target: Character): Status {
        return Composed(target, durability, magnitude)
    }
}

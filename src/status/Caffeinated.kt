package status

import characters.Character
import characters.Trait
import combat.Combat
import kotlin.math.roundToInt

class Caffeinated(affected: Character?, duration: Int, magnitude: Double) : Status("Caffeinated", affected) {
    init {
        this.duration = duration
        this.magnitude = magnitude
        stacking = true
        lingering = true
        tooltip = "Regenerate Stamina each turn"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        } else {
            this.duration = duration
        }
        this.affected = affected
    }


    override fun describe(): String {
        return if (affected!!.human()) {
            "You feel the effects of the energy drink filling you with energy."
        } else {
            affected!!.name + " looks a little hyper."
        }
    }

    override fun regen(): Double {
        return magnitude
    }

    override fun copy(target: Character): Status {
        return Caffeinated(target, duration, magnitude)
    }

    override fun turn(c: Combat) {
        affected!!.buildMojo(magnitude.roundToInt())
        decay(c)
    }
}

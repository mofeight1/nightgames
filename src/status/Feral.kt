package status

import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat

class Feral(affected: Character?) : Status("Feral", affected) {
    init {
        flag(Stsflag.feral)
        tooltip = "Bonus to all basic Attributes based on Animism"
        this.affected = affected
        decaying = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You feel your animal instincts start to take over, increasing your strength and speed."
        } else {
            ""
        }
    }

    override fun mod(a: Attribute): Double {
        when (a) {
            Attribute.Power -> return affected!![Attribute.Animism] / 5.0
            Attribute.Cunning -> return affected!![Attribute.Animism] / 4.0
            Attribute.Seduction -> return affected!![Attribute.Animism] / 4.0
            Attribute.Animism -> return 2.0
            Attribute.Speed -> return affected!![Attribute.Animism] / 6.0
            else -> {}
        }
        return 0.0
    }

    override fun copy(target: Character): Status {
        return Feral(target)
    }

    override fun turn(c: Combat) {
        if (!affected!!.has(Trait.feral)) {
            if (affected!!.has(Trait.furaffinity)) {
                if (affected!!.arousal.percent() < 25) {
                    affected!!.removeStatus(this, c)
                }
            } else {
                if (affected!!.arousal.percent() < 50) {
                    affected!!.removeStatus(this, c)
                }
            }
        }
    }
}

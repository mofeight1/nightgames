package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType

class Buzzed(affected: Character?) : Status("Buzzed", affected) {
    init {
        magnitude = 1.0
        stacking = true
        lingering = true
        tooltip = "Penalty to Power, Cunning, and Perception. Take reduced pleasure damage"
        duration = if (affected != null && affected.has(Trait.PersonalInertia)) {
            30
        } else {
            20
        }

        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You feel a pleasant buzz, which makes you a bit sluggish, but also takes the edge off your sense of touch."
        } else {
            affected!!.name + " looks mildly buzzed, probably trying to dull her senses."
        }
    }

    override fun mod(a: Attribute): Double {
        return when (a) {
            Attribute.Perception -> {
                -3 * magnitude
            }

            Attribute.Power -> {
                -1 * magnitude
            }

            Attribute.Cunning -> {
                -2 * magnitude
            }

            else -> 0.0
        }
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return -(x * magnitude) / 10
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) temp[DamageType.Pleasure] = -(amount * magnitude) / 10
        return temp
    }


    override fun evade(): Double {
        return -5.0
    }

    override fun copy(target: Character): Status {
        return Buzzed(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 15)
        decay(c)
    }
}

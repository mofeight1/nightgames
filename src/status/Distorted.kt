package status

import characters.Character
import characters.Trait

class Distorted(affected: Character, magnitude: Double) : Status("Distorted", affected) {
    init {
        this.magnitude = magnitude
        flag(Stsflag.distorted)
        tooltip = "Evasion bonus"
        duration = 6
        if (affected.has(Trait.PersonalInertia)) {
            duration = 9
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your image is distorted, making you hard to hit."
        } else {
            "Multiple " + affected!!.name + "s appear in front of you. When you focus, you can tell which one is real, but it's still screwing up your accuracy."
        }
    }

    override fun evade(): Double {
        return magnitude
    }

    override fun copy(target: Character): Status {
        return Distorted(target, magnitude)
    }
}

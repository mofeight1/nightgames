package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import kotlin.math.max
import kotlin.math.roundToInt

class Shamed(affected: Character?) : Status("Shamed", affected) {
    init {
        magnitude = 2.0
        stacking = true
        flag(Stsflag.shamed)
        flag(Stsflag.mindaffecting)
        this.duration = 4
        tooltip = "Cunning and Seduction penalty, deal less pleasure, bonus to Submissive"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 6
        }
        this.affected = affected
    }

    constructor(affected: Character?, magnitude: Double) : this(affected) {
        this.magnitude = magnitude
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You're a little distracted by self-consciousness, and it's throwing you off your game."
        } else {
            affected!!.name + " is red faced from embarrassment as much as arousal."
        }
    }

    override fun mod(a: Attribute): Double {
        return if (a == Attribute.Seduction) {
            -magnitude
        } else if (a == Attribute.Cunning) {
            -magnitude
        } else if (a == Attribute.Submissive && affected!!.getPure(Attribute.Submissive) > 0) {
            magnitude
        } else {
            0.0
        }
    }

    override fun weakened(x: Double): Double {
        return magnitude / 2.0
    }

    override fun tempted(x: Double): Double {
        return magnitude
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Weaken) temp[DamageType.Weaken] = magnitude / 2
        if (type == DamageType.Tempt) temp[DamageType.Tempt] = magnitude
        return temp
    }

    override fun escape(): Int {
        return -magnitude.roundToInt()
    }

    override fun proficiency(using: Anatomy): Double {
        return max(1.0 - (.05 * magnitude), 0.0)
    }

    override fun copy(target: Character): Status {
        return Shamed(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.nervous, 30)
        affected!!.emote(Emotion.desperate, 30)
        decay(c)
    }
}

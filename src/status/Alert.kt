package status

import characters.Character
import characters.Trait
import kotlin.math.roundToInt

class Alert(affected: Character?) : Status("Alert", affected) {
    init {
        this.magnitude = 5.0
        duration = if (affected != null && affected.has(Trait.PersonalInertia)) {
            5
        } else {
            3
        }
        flag(Stsflag.alert)
        tooltip = "Evasion and Counter bonus"
    }

    override fun describe(): String {
        return ""
    }

    override fun evade(): Double {
        return magnitude
    }

    override fun counter(): Int {
        return (magnitude * 3).roundToInt()
    }

    override fun copy(target: Character): Status {
        return Alert(target)
    }
}

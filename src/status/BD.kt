package status

import characters.Character
import characters.Trait
import combat.Combat

class BD(affected: Character?) : Status("Bondage", affected) {
    init {
        magnitude = 7.0
        flag(Stsflag.bondage)
        duration = if (affected != null && affected.has(Trait.PersonalInertia)) {
            15
        } else {
            10
        }
        this.affected = affected
        tooltip = "Gain arousal each turn if bound"
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Fantasies of being tied up continue to dance through your head."
        } else {
            affected!!.name + " is affected by a brief bondage fetish."
        }
    }

    override fun copy(target: Character): Status {
        return BD(target)
    }

    override fun turn(c: Combat) {
        if (affected!!.bound() || affected!!.has(Stsflag.tied)) {
            affected!!.tempt(magnitude, combat = c)
        }
        decay(c)
    }
}

package status

import characters.Character

class Primed(affected: Character?, charges: Int) : Status("Primed", affected) {
    var amount = charges
    init {
        stacking = true
        tooltip = "Time charges available"
        decaying = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You have $amount time charges primed."
        } else {
            ""
        }
    }

    override fun copy(target: Character): Status {
        return Primed(target, amount)
    }

    override fun stack(other: Status) {
        if (other !is Primed) {
            super.stack(other)
            return
        }
        this.amount += other.amount
        if (other.duration > this.duration) {
            this.duration = other.duration
        }
        if (this.amount == 0) {
            affected!!.removeStatus(this)
        }
    }
}

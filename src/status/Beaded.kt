package status

import characters.Character

class Beaded(affected: Character?, beads: Int) : Status("Beads Inside", affected) {
    var amount: Int

    init {
        flag(Stsflag.beads)
        amount = beads
        stacking = true
        duration = 999
        decaying = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            if (amount == 1) {
                "There is a bead in your ass"
            } else {
                "There are $amount beads in your ass"
            }
        } else {
            if (amount == 1) {
                "There is a single anal bead inside " + affected!!.name + "."
            } else {
                "There are " + amount + " anal beads inside " + affected!!.name + "."
            }
        }
    }

    override fun copy(target: Character): Status {
        return Beaded(affected, amount)
    }

    override fun stack(other: Status) {
        if (other !is Beaded){
            super.stack(other)
            return
        }
        this.amount += other.amount
        if (other.duration > this.duration) {
            this.duration = other.duration
        }
        if (this.amount == 0) {
            affected!!.removeStatus(this)
        }
    }
}

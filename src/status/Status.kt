package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import items.Clothing
import kotlin.math.roundToInt

abstract class Status {
    protected lateinit var name: String
    var tooltip: String? = null
        protected set
        get() {
            var tip = field
            if (magnitude != 0.0) {
                tip += " (${magnitude.roundToInt()})"
            }
            if (duration > 0) {
                tip += "; $duration turns"
            }
            return tip
        }
    protected var affected: Character? = null
    var flags: HashSet<Stsflag> = HashSet()
        protected set
    protected var traits: ArrayList<Trait> = ArrayList()
    var magnitude: Double = 0.0
        protected set
    var duration: Int = 0
        protected set
    var stacking: Boolean = false
        protected set
    protected var lingering: Boolean = false
    fun lingering(): Boolean {
        return lingering || fixed
    }
    protected var decaying: Boolean = false
    protected var fixed: Boolean = false

    constructor()

    constructor(name: String, affected: Character?) {
        this.name = name
        flags = HashSet()
        traits = ArrayList()
        this.magnitude = 0.0
        this.duration = 0
        stacking = false
        lingering = false
        decaying = true
        this.affected = affected
        fixed = false
    }

    override fun toString(): String {
        return name
    }

    abstract fun describe(): String?

    open fun mod(a: Attribute): Double {
        return 0.0
    }

    open fun regen(): Double {
        return 0.0
    }

    open fun damage(x: Double, area: Anatomy): Double {
        return 0.0
    }

    open fun pleasure(x: Double, area: Anatomy): Double {
        return 0.0
    }

    open fun weakened(x: Double): Double {
        return 0.0
    }

    open fun tempted(x: Double): Double {
        return 0.0
    }

    open fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        return Damage()
    }

    open fun evade(): Double {
        return 0.0
    }

    open fun escape(): Int {
        return 0
    }

    open fun gain(pool: Pool, x: Double): Double {
        return 0.0
    }

    open fun spend(pool: Pool, x: Double): Double {
        return 0.0
    }

    open fun counter(): Int {
        return 0
    }

    open fun value(): Double {
        return 0.0
    }

    open fun stripAttempt(article: Clothing): Double {
        return 0.0
    }

    fun tempTraits(bestows: Trait): Boolean {
        return traits.contains(bestows)
    }

    open fun turn(c: Combat) {
        if (decaying) {
            decay(c)
        }
    }

    abstract fun copy(target: Character): Status

    fun flag(status: Stsflag) {
        flags.add(status)
    }


    open fun proficiency(using: Anatomy): Double {
        return 1.0
    }

    open fun sensitive(targeted: Anatomy): Double {
        return 1.0
    }

    open fun sore(targeted: Anatomy): Double {
        return 1.0
    }

    open fun stack(other: Status) {
        this.magnitude += other.magnitude
        if (other.duration > this.duration) {
            this.duration = other.duration
        }
        if (this.magnitude == 0.0) {
            affected!!.removeStatus(this)
            if (flags.contains(Stsflag.mindaffecting)) {
                affected!!.add(Cynical(affected!!))
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Status) return false
        return this.name === other.name
    }

    fun decay(c: Combat? = null) {
        if (!fixed) {
            if (duration <= 0) {
                affected!!.removeStatus(this, c)
                if (flags.contains(Stsflag.mindaffecting)) {
                    affected!!.add(Cynical(affected!!), c)
                }
            }
            duration--
        }
    }

    fun removable(): Boolean {
        return !fixed
    }

    fun fix() {
        fixed = true
    }
}

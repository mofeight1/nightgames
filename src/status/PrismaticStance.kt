package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import kotlin.math.min

class PrismaticStance(affected: Character?) : Status("Prismatic Form", affected) {
    init {
        duration = 10
        flag(Stsflag.form)
        tooltip = "Pain and Pleasure resistance, x2 Mojo gain, Half Mojo costs, Evasion and Counter bonus"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 15
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "combining all the elemental forms, your Ki pushes your body past its limit"
        } else {
            affected!!.name + "'s powerful Ki seems to be limitless."
        }
    }

    override fun mod(a: Attribute): Double {
        if (Attribute.Power == a) {
            return -affected!![Attribute.Ki] / 2.0
        }
        if (a == Attribute.Speed) {
            return -affected!![Attribute.Ki].toDouble()
        }
        return 0.0
    }

    override fun regen(): Double {
        return -min(affected!![Attribute.Ki], 30) / 5.0
    }

    override fun damage(x: Double, area: Anatomy): Double {
        return -x * min(affected!![Attribute.Ki] * 3, 90) / 100.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return -x * min(affected!![Attribute.Ki] * 2, 80) / 100.0
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) {
            temp[DamageType.Pleasure] = -amount * min(affected!![Attribute.Ki] * 2, 80) / 100.0
        }
        if(type == DamageType.Pain) {
            temp[DamageType.Pain] = -amount * min(affected!![Attribute.Ki] * 3, 90) / 100.0
        }
        return temp
    }

    override fun evade(): Double {
        return 2.0 * affected!![Attribute.Ki]
    }

    override fun gain(pool: Pool, x: Double): Double {
        return if (pool == Pool.MOJO) {
            x * min(affected!![Attribute.Ki], 30) / 6.0
        } else {
            0.0
        }
    }

    override fun spend(pool: Pool, x: Double): Double {
        return if (pool == Pool.MOJO) {
            -x / 2
        } else {
            0.0
        }
    }

    override fun counter(): Int {
        return affected!![Attribute.Ki]
    }

    override fun copy(target: Character): Status {
        return PrismaticStance(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.dominant, 20)
        decay(c)
    }
}

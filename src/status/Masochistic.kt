package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import kotlin.math.roundToInt

class Masochistic(affected: Character?) : Status("Masochism", affected) {
    init {
        flag(Stsflag.masochism)
        flag(Stsflag.mindaffecting)
        duration = 10
        tooltip = "Gain Arousal whenever you take Pain damage"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 15
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Arousing fantasies of being hurt continue to tempt you."
        } else {
            affected!!.name + " is still flushed with arousal at the idea of being struck."
        }
    }

    override fun mod(a: Attribute): Double {
        return 0.0
    }

    override fun regen(): Double {
        return 0.0
    }

    override fun damage(x: Double, area: Anatomy): Double {
        affected!!.arousal.restore((3 * x / 2).roundToInt())
        return 0.0
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pain)
            temp[DamageType.Pleasure] = 3 * amount / 2
        return temp
    }


    override fun copy(target: Character): Status {
        return Masochistic(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.nervous, 15)
        decay(c)
    }
}

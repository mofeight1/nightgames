package status

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Damage
import combat.DamageType
import kotlin.math.roundToInt

class Rewired(affected: Character?, duration: Int) : Status("Rewired", affected) {
    init {
        this.duration = duration
        lingering = true
        flag(Stsflag.rewired)
        tooltip = "Pleasure reduces Stamina and Pain raises Arousal"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your senses feel... wrong. It's like your sense of pleasure and pain are jumbled."
        } else {
            affected!!.name + " fidgets uncertainly at the alien sensation of her rewired nerves."
        }
    }

    override fun damage(x: Double, area: Anatomy): Double {
        if (area != Anatomy.soul) {
            affected!!.arousal.restore(x.roundToInt())
        }
        return -x
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        if (area != Anatomy.soul) {
            affected!!.stamina.reduce(x.roundToInt())
        }
        return -x
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (area == Anatomy.soul) return temp

        if (type == DamageType.Pleasure) {
            temp[DamageType.Pain] = amount
            temp[DamageType.Pleasure] = -amount
        }
        if (type == DamageType.Pain) {
            temp[DamageType.Pleasure] = amount
            temp[DamageType.Pain] = -amount
        }
        return temp
    }

    override fun copy(target: Character): Status {
        return Rewired(target, duration)
    }
}

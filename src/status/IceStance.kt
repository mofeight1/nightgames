package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import kotlin.math.min

class IceStance(affected: Character?) : Status("Ice Form", affected) {
    init {
        duration = 10
        flag(Stsflag.form)
        flag(Stsflag.mojodeny)
        tooltip = "2% pleasure resistance per point of Ki, no Mojo gain"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 15
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You're as frigid as a glacier"
        } else {
            affected!!.name + " is cool as ice."
        }
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return -x * (min(affected!![Attribute.Ki] * 2, 80) / 100.0)
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure)
            temp[DamageType.Pleasure] = -amount * (min(affected!![Attribute.Ki] * 2, 80) / 100.0)
        return temp
    }

    override fun gain(pool: Pool, x: Double): Double {
        // TODO Auto-generated method stub
        if (pool == Pool.MOJO) {
            return -x
        }
        return 0.0
    }

    override fun copy(target: Character): Status {
        return IceStance(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 5)
        affected!!.emote(Emotion.dominant, 5)
        decay(c)
    }
}

package status

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import kotlin.math.min

class FireStance(affected: Character?) : Status("Fire Form", affected) {
    init {
        duration = 10
        flag(Stsflag.form)
        tooltip = "Mojo skills cost half, increased Mojo gain, lose Stamina each turn"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 15
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your spirit burns in you, feeding your power"
        } else {
            affected!!.name + " is still all fired up."
        }
    }

    override fun regen(): Double {
        return -min(affected!![Attribute.Ki].toDouble(), 30.0) / 5
    }

    override fun gain(pool: Pool, x: Double): Double {
        return if (pool == Pool.MOJO) {
            x * min(affected!![Attribute.Ki].toDouble(), 30.0) / 6
        } else {
            0.0
        }
    }

    override fun spend(pool: Pool, x: Double): Double {
        return if (pool == Pool.MOJO) {
            -x / 2
        } else {
            0.0
        }
    }

    override fun copy(target: Character): Status {
        return FireStance(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 5)
        affected!!.emote(Emotion.dominant, 5)
        decay(c)
    }
}

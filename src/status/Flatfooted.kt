package status

import characters.Character
import characters.Emotion
import combat.Combat

class Flatfooted(affected: Character, duration: Int) : Status("Flat-Footed", affected) {
    init {
        flag(Stsflag.distracted)
        flag(Stsflag.mindaffecting)
        this.duration = duration
        tooltip = "Unable to act"
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You are caught off-guard."
        } else {
            affected!!.name + " is flat-footed and not ready to fight."
        }
    }

    override fun evade(): Double {
        return -10.0
    }

    override fun escape(): Int {
        return -20
    }

    override fun counter(): Int {
        return -3
    }

    override fun copy(target: Character): Status {
        return Flatfooted(target, duration)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.nervous, 5)
        decay(c)
    }
}

package status

import characters.Character
import characters.Trait

class Protected(affected: Character?) : Status("Mysterious Protection", affected) {
    init {
        this.duration = 6
        this.flag(Stsflag.protection)
        traits.add(Trait.painimmune)
        traits.add(Trait.weakimmune)
        tooltip = "Protection from Pain and Weaken damage"
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You are protected from attacks by a mysterious force."
        } else {
            affected!!.name + " is protected by a mysterious force."
        }
    }

    override fun copy(target: Character): Status {
        val copy = Protected(affected)
        copy.duration = duration
        return copy
    }
}

package status

import characters.Anatomy
import characters.Character
import characters.Trait

class ProfMod(name: String, affected: Character?, part: Anatomy, percent: Double) : Status(name, affected) {
    private val part: Anatomy

    init {
        this.magnitude = percent
        this.part = part
        stacking = true
        tooltip = "Bonus " + part.name + " proficiency"
        duration = 3
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
    }

    override fun describe(): String {
        return ""
    }

    override fun proficiency(using: Anatomy): Double {
        if (using == part) {
            return 1.0 + (magnitude / 100)
        }
        return 1.0
    }

    override fun copy(target: Character): Status {
        return ProfMod(name, affected, part, magnitude)
    }
}

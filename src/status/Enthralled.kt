package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.State
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType
import global.Global

class Enthralled(user: Character, var master: Character) : Status("Enthralled", user) {
    init {
        flag(Stsflag.enthralled)
        flag(Stsflag.mojodeny)
        flag(Stsflag.mindaffecting)
        tooltip = "Unable to act, must follow commands from caster"
        duration = 4 + master.bonusEnthrallDuration()
        if (user.has(Trait.PersonalInertia)) {
            duration = Math.round(duration * 1.5).toInt()
        }
    }

    override fun describe(): String {
        return if (affected!!.human()) ("You feel a constant pull on your mind, forcing you to obey"
                + " your master's every command.")
        else {
            affected!!.name + " looks dazed and compliant, ready to follow your orders."
        }
    }

    override fun mod(a: Attribute): Double {
        if (a == Attribute.Perception) return -5.0
        return -2.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return x / 4
    }

    override fun tempted(x: Double): Double {
        return x / 4
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Tempt) temp[DamageType.Tempt] = amount / 4
        if (type == DamageType.Pleasure) temp[DamageType.Pleasure] = amount / 4
        return temp
    }

    override fun evade(): Double {
        return -20.0
    }

    override fun escape(): Int {
        return -20
    }

    override fun gain(pool: Pool, x: Double): Double {
        if (pool == Pool.MOJO) {
            return -x
        }
        return 0.0
    }

    override fun copy(target: Character): Status {
        return Enthralled(target, master)
    }

    override fun turn(c: Combat) {
        affected!!.spendMojo(5)
        if (duration <= 0 || affected!!.check(Attribute.Cunning, 20 + 10 * duration)) {
            affected!!.removeStatus(this, c)
            if (affected!!.human() && affected!!.state != State.combat) Global.gui.message(
                "Everything around you suddenly seems much clearer,"
                        + " like a lens snapped into focus. You don't really remember why"
                        + " you were heading in the direction you where..."
            )
        }
        affected!!.emote(Emotion.horny, 15)
        decay(c)
    }
}

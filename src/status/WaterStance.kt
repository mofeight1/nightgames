package status

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import kotlin.math.min

class WaterStance(affected: Character?) : Status("Water Form", affected) {
    init {
        this.duration = 10
        flag(Stsflag.form)
        tooltip = "Evasion and Counter bonus, Power penalty"
        this.affected = affected
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 15
        }
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You're as smooth and responsive as flowing water."
        } else {
            affected!!.name + " continues her flowing movements."
        }
    }

    override fun mod(a: Attribute): Double {
        if (Attribute.Power == a) {
            return -min(affected!![Attribute.Ki], 30) / 2.0
        }
        return 0.0
    }

    override fun evade(): Double {
        // TODO Auto-generated method stub
        return min(affected!![Attribute.Ki], 30).toDouble()
    }

    override fun counter(): Int {
        return min(affected!![Attribute.Ki], 30) / 2
    }

    override fun copy(target: Character): Status {
        return WaterStance(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 5)
        decay(c)
    }
}

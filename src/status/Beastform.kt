package status

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat

class Beastform(affected: Character?) : Status("Beastform", affected) {
    init {
        traits.add(Trait.tailed)
        flags.add(Stsflag.beastform)
        tooltip = "25% Power and Speed bonus. Doubled if also Feral"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 15
        } else {
            this.duration = 10
        }
        this.affected = affected
    }

    override fun mod(a: Attribute): Double {
        if (affected!!.has(Stsflag.feral)) {
            when (a) {
                Attribute.Power -> return affected!!.getPure(Attribute.Power) / 2.0
                Attribute.Speed -> return affected!!.getPure(Attribute.Speed) / 2.0
                Attribute.Animism -> return 10.0
                else -> {}
            }
        } else {
            when (a) {
                Attribute.Power -> return affected!!.getPure(Attribute.Power) / 4.0
                Attribute.Speed -> return affected!!.getPure(Attribute.Speed) / 4.0
                Attribute.Animism -> return 10.0
                else -> {}
            }
        }
        return 0.0
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your animal transformation is still stable, giving you enhanced physical capability."
        } else {
            affected!!.name + " is partially transformed into a feral animal."
        }
    }

    override fun copy(target: Character): Status {
        return Beastform(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.dominant, 5)
        decay(c)
    }
}

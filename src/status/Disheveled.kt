package status

import characters.Character
import characters.Trait
import combat.Combat
import items.Clothing

class Disheveled(afflicted: Character?, article: Clothing) : Status("Disheveled: " + article.properName, afflicted) {
    private val article: Clothing

    init {
        tooltip = "Increased chance to strip"
        this.duration = 4
        if (affected != null && affected!!.has(Trait.PersonalInertia)) {
            this.duration = 6
        }
        this.article = article
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your $article almost fell off with that last attempt."
        } else {
            affected!!.name + " is struggling to keep her " + article + " on."
        }
    }

    override fun stripAttempt(article: Clothing): Double {
        if (this.article == article) {
            return 20.0
        }
        return 0.0
    }

    override fun copy(target: Character): Status {
        return Disheveled(target, article)
    }

    override fun turn(c: Combat) {
        if (affected!!.isWearing(article.type!!)) {
            decay()
        } else {
            affected!!.removeStatus(this, c)
        }
    }
}

package status

import characters.Anatomy
import characters.Character
import characters.Trait

class ShadowFingers(affected: Character?) : Status("Shadow Fingers", affected) {
    init {
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 6
        } else {
            this.duration = 4
        }
        flags.add(Stsflag.shadowfingers)
        tooltip = "+50 finger proficiency"
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "The fingers of your right hand have fused with shadow tendrils, giving you supernatural dexterity."
        } else {
            "The dark tentacles extending from " + affected!!.name + "'s wriggle ominously."
        }
    }

    override fun proficiency(using: Anatomy): Double {
        if (using == Anatomy.fingers) {
            return 1.5
        }
        return 1.0
    }

    override fun copy(target: Character): Status {
        return ShadowFingers(affected)
    }
}

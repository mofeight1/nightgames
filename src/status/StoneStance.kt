package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import kotlin.math.min

class StoneStance(affected: Character?) : Status("Stone Form", affected) {
    init {
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 15
        } else {
            this.duration = 10
        }
        flag(Stsflag.form)
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your body still feels as impenetrable as rock."
        } else {
            affected!!.name + "'s movements are slow, but rock-solid."
        }
    }

    override fun mod(a: Attribute): Double {
        if (a == Attribute.Speed) {
            return -affected!![Attribute.Ki].toDouble()
        }
        return 0.0
    }

    override fun regen(): Double {
        return 0.0
    }

    override fun damage(x: Double, area: Anatomy): Double {
        return -x * (min(affected!![Attribute.Ki] * 3, 90) / 100.0)
    }

    override fun copy(target: Character): Status {
        return StoneStance(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 5)
        decay(c)
    }
}

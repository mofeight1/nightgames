package status

import characters.Character
import characters.Trait

class Unwavering(affected: Character?) : Status("Unwavering", affected) {
    init {
        flag(Stsflag.unwavering)
        traits.add(Trait.temptimmune)
        tooltip = "Immune to temptation"
        this.duration = 4
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 6
        }
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your unwavering heart ignores all temptation."
        } else {
            affected!!.name + " is completely serene."
        }
    }

    override fun copy(target: Character): Status {
        return Unwavering(target)
    }
}

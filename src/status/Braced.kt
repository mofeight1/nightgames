package status

import characters.Character

class Braced(affected: Character?) : Status("Braced", affected) {
    init {
        duration = 4
        flag(Stsflag.braced)
        tooltip = "Resist knockdowns"
        this.affected = affected
    }

    override fun describe(): String {
        return ""
    }

    override fun value(): Double {
        // TODO Auto-generated method stub
        return 20.0 + (25 * duration)
    }

    override fun copy(target: Character): Status {
        return Braced(target)
    }
}

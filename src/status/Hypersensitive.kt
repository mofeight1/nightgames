package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType

class Hypersensitive(affected: Character?) : Status("Hypersensitive", affected) {
    init {
        lingering = true
        duration = 20
        flag(Stsflag.hypersensitive)
        tooltip = "+4 Perception, +30% pleasure"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 30
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your skin tingles and feels extremely sensitive to touch."
        } else {
            "She shivers from the breeze hitting her skin and has goosebumps"
        }
    }

    override fun mod(a: Attribute): Double {
        if (a == Attribute.Perception) {
            return 4.0
        }
        return 0.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return x / 3
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) temp[DamageType.Pleasure] = amount / 3
        return temp
    }

    override fun copy(target: Character): Status {
        return Hypersensitive(target)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.nervous, 5)
        decay(c)
    }
}

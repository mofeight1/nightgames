package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Damage
import combat.DamageType
import kotlin.math.min

class Tranquil(affected: Character?) : Status("Tranquil", affected) {
    init {
        flag(Stsflag.tranquil)
        this.duration = 4
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 6
        }
        magnitude = min(70.0, affected!![Attribute.Spirituality] * 5.0)
        tooltip = "$magnitude% Pleasure resistance"
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your aura of tranquility resists pleasure"
        } else {
            affected!!.name + " resists pleasure with impressive tranquility."
        }
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return x * -.01 * magnitude
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) temp[DamageType.Pleasure] = amount * -.01 * magnitude
        return temp
    }

    override fun copy(target: Character): Status {
        return Tranquil(target)
    }
}

package status

import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat

class Horny(affected: Character?, magnitude: Double, duration: Int) : Status("Horny", affected) {
    init {
        this.duration = duration
        this.magnitude = magnitude
        lingering = true
        stacking = true
        flag(Stsflag.horny)
        tooltip = "Temptation damage each turn"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your heart pounds in your chest as you try to suppress your arousal."
        } else {
            affected!!.name + " is flushed and her nipples are noticeably hard."
        }
    }


    override fun copy(target: Character): Status {
        return Horny(target, magnitude, duration)
    }

    override fun turn(c: Combat) {
        affected!!.tempt(magnitude, combat = c)
        affected!!.emote(Emotion.horny, 20)
        decay(c)
    }
}

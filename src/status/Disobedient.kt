package status

import characters.Anatomy
import characters.Character
import characters.Pool
import characters.Trait
import combat.Damage
import combat.DamageType

class Disobedient(affected: Character) : Status("Disobedient", affected) {
    init {
        flag(Stsflag.disobedient)
        flag(Stsflag.mindaffecting)
        flag(Stsflag.mojodeny)
        if (affected.has(Trait.PersonalInertia)) {
            this.duration = 9
        } else {
            this.duration = 6
        }
        tooltip = "Receive double damage and unable to evade"
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You disobeyed your master's command and await punishment"
        } else {
            affected!!.name + " has disobeyed your order and deserves punishment"
        }
    }

    override fun damage(x: Double, area: Anatomy): Double {
        return x
    }

    override fun weakened(x: Double): Double {
        return x
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pain) temp[DamageType.Pain] = amount
        if (type == DamageType.Weaken) temp[DamageType.Weaken] = amount
        return temp
    }

    override fun evade(): Double {
        return -99.0
    }

    override fun gain(pool: Pool, x: Double): Double {
        if (pool == Pool.MOJO) {
            return -x
        }
        return 0.0
    }

    override fun copy(target: Character): Status {
        return Disobedient(affected!!)
    }
}

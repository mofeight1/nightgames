package status

import characters.Anatomy
import characters.Character
import characters.Trait

class ProfessionalMomentum(affected: Character?, percent: Double) : Status("Professional Momentum", affected) {
    private val part: Anatomy? = null

    init {
        this.magnitude = percent
        stacking = true
        lingering = false
        decaying = false
        tooltip = "Bonus finger, mouth, and intercourse proficiency"
        duration = 3
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
    }

    override fun describe(): String {
        return ""
    }

    override fun proficiency(using: Anatomy): Double {
        if (using == Anatomy.fingers || using == Anatomy.mouth || using == Anatomy.genitals) {
            return 1.0 + (magnitude / 100)
        }
        return 1.0
    }

    override fun copy(target: Character): Status {
        return ProfessionalMomentum(affected, magnitude)
    }
}

package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Damage
import combat.DamageType

class Broken(affected: Character?, magnitude: Double) : Status("Broken", affected) {
    init {
        this.magnitude = magnitude
        this.flag(Stsflag.broken)
        duration = 999
        decaying = false
    }

    override fun describe(): String {
        return ""
    }

    override fun mod(a: Attribute): Double {
        if (affected!!.has(Trait.enraged)) {
            if (a == Attribute.Power) {
                return magnitude / 5
            }
            if (a == Attribute.Speed) {
                return magnitude / 5
            }
        } else {
            if (a == Attribute.Power) {
                return magnitude / 10
            }
            if (a == Attribute.Speed) {
                return magnitude / 10
            }
        }
        return 0.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return x * magnitude / 200
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) temp[DamageType.Pleasure] = amount * magnitude / 200
        return temp
    }

    override fun copy(target: Character): Status {
        return Broken(target, magnitude)
    }
}

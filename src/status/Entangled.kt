package status

import characters.Attribute
import characters.Character
import combat.Combat
import kotlin.math.roundToInt

class Entangled(affected: Character?, owner: Character, magnitude: Double) : Status("Entangled", affected) {
    private val owner: Character

    init {
        flag(Stsflag.entangled)
        this.owner = owner
        this.magnitude = magnitude
        tooltip = "$magnitude% Entangled. Negative effects will apply as this increases."
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You are partially entangled in " + owner.name + "'s tentacles."
        } else {
            affected!!.name + " is partially wrapped in tentacles."
        }
    }

    override fun escape(): Int {
        return (-magnitude / 10).roundToInt()
    }

    override fun copy(target: Character): Status {
        return Entangled(affected, owner, magnitude)
    }

    override fun turn(c: Combat) {
        if (c.stance.contact) {
            magnitude += 5
        } else {
            magnitude -= 30
        }
        if (magnitude >= 80 && owner.getPure(Attribute.Eldritch) >= 9) {
            affected!!.add(Bound(affected!!, owner[Attribute.Eldritch] / 3, "tentacles"))
        }
        if (magnitude >= 60 && owner.getPure(Attribute.Eldritch) >= 21) {
            affected!!.add(Hypersensitive(affected), c)
        }
        if (magnitude >= 40 && owner.getPure(Attribute.Eldritch) >= 18) {
            affected!!.add(Horny(affected, 3.0, 2), c)
        }
        if (magnitude >= 20 && owner.getPure(Attribute.Eldritch) >= 12) {
            affected!!.add(Oiled(affected), c)
        }
        if (magnitude <= 0) {
            affected!!.removeStatus(this)
        }
    }
}

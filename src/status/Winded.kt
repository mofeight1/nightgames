package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Damage
import combat.DamageType

class Winded(affected: Character, private val opponent: Character, duration: Int = 3) :
    Status("Winded", affected) {
    init {
        flag(Stsflag.stunned)
        var d = duration
        if (affected.has(Trait.rapidrecovery)) {
            d--
        }
        if (affected.has(Trait.juggernaut)) {
            d--
        }
        if (opponent.has(Trait.staydown)) {
            d++
        }
        if (opponent.has(Trait.foulqueen)) {
            d++
        }
        this.duration = d
        tooltip = "Unable to act or evade, Stamina recovers quickly, take reduced Temptation damage"
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You need a moment to catch your breath"
        } else {
            affected!!.name + " is panting and trying to recover"
        }
    }

    override fun mod(a: Attribute): Double {
        return if (a == Attribute.Power || a == Attribute.Speed) {
            -2.0
        } else {
            0.0
        }
    }

    override fun regen(): Double {
        return affected!!.stamina.max / 4.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return if (opponent.has(Trait.opportunist)) {
            x / 5
        } else {
            0.0
        }
    }

    override fun tempted(x: Double): Double {
        return -x / 2
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure && opponent.has(Trait.opportunist))
            temp[DamageType.Pleasure] = amount / 5
        if (type == DamageType.Tempt) temp[DamageType.Tempt] = -amount / 2
        return temp
    }

    override fun evade(): Double {
        return -99.0
    }

    override fun counter(): Int {
        return -99
    }

    override fun copy(target: Character): Status {
        val copy = Winded(target, opponent)
        copy.duration = duration
        return copy
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.nervous, 15)
        affected!!.emote(Emotion.angry, 10)
        decay(c)
    }
}

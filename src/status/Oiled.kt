package status

import characters.Anatomy
import characters.Character
import combat.Damage
import combat.DamageType

class Oiled(affected: Character?) : Status("Oiled", affected) {
    init {
        lingering = true
        flag(Stsflag.oiled)
        tooltip = "-25% pleasure resistance, easier to escape pins"
        this.affected = affected
        decaying = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your skin is slick with oil and kinda feels weird."
        } else {
            affected!!.name + " is shiny with lubricant, making you more tempted to touch and rub her skin."
        }
    }

    override fun escape(): Int {
        return 10
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        return x / 4
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Pleasure) temp[DamageType.Pleasure] = amount / 4
        return temp
    }

    override fun copy(target: Character): Status {
        return Oiled(target)
    }
}

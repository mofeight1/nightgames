package status

import characters.Anatomy
import characters.Character
import characters.Emotion
import combat.Combat

class GoldenCock(affected: Character?) : Status("Golden Cock", affected) {
    init {
        this.flag(Stsflag.goldencock)
        this.duration = 20
        this.affected = affected
        tooltip = "Deal double pleasure with penis, receive half pleasure on penis."
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your cock glows with incredible power"
        } else {
            affected!!.name + "'s penis glows intimidatingly."
        }
    }

    override fun proficiency(using: Anatomy): Double {
        if (using == Anatomy.genitals) {
            return 2.0
        }
        return 1.0
    }

    override fun sensitive(targeted: Anatomy): Double {
        if (targeted == Anatomy.genitals) {
            return .5
        }
        return 1.0
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.dominant, 10)
        decay(c)
    }

    override fun copy(target: Character): Status {
        return GoldenCock(target)
    }
}

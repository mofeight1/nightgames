package status

import characters.Character
import characters.Trait

class Isolated(affected: Character?) : Status("Isolated", affected) {
    init {
        flag(Stsflag.isolated)
        duration = 20
        tooltip = "Unable to summon pets"
        traits.add(Trait.summonblock)
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You feel cut off from anything you would try to summon."
        } else {
            affected!!.name + " is cut off from any pets."
        }
    }

    override fun copy(target: Character): Status {
        return Isolated(target)
    }
}

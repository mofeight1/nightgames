package status

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat

class Erebia(affected: Character, duration: Int) : Status("Mana Fortification", affected) {
    init {
        this.duration = duration
        flag(Stsflag.erebia)
        tooltip = "Bonus to Power, Seduction, Speed, and Stamina regen"
        this.magnitude = affected[Attribute.Arcane] / 3.0
        if (affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Magic is still flowing through your body, giving you tremendous speed and power"
        } else {
            affected!!.name + " is glowing with powerful magic."
        }
    }

    override fun mod(a: Attribute): Double {
        if (a == Attribute.Power || a == Attribute.Seduction) {
            return magnitude
        } else if (a == Attribute.Speed) {
            return magnitude / 2
        }
        return 0.0
    }

    override fun regen(): Double {
        return 3.0
    }

    override fun evade(): Double {
        // TODO Auto-generated method stub
        return 5.0
    }

    override fun escape(): Int {
        // TODO Auto-generated method stub
        return 5
    }

    override fun value(): Double {
        // TODO Auto-generated method stub
        return magnitude
    }

    override fun copy(target: Character): Status {
        return Erebia(target, duration)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 25)
        affected!!.emote(Emotion.dominant, 25)
        decay(c)
    }
}

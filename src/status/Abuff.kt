package status

import characters.Attribute
import characters.Character
import characters.Trait

class Abuff(affected: Character?, private val modded: Attribute, value: Double, duration: Int) : Status(
    "$modded buff", affected
) {
    init {
        this.magnitude = value
        if (value < 0) {
            this.name = "$modded debuff"
        }
        stacking = true
        lingering = true
        tooltip = modded.toString() + "buff"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        } else {
            this.duration = duration
        }
        this.affected = affected
    }

    override fun describe(): String {
        return ""
    }

    override fun mod(a: Attribute): Double {
        if (a == modded) {
            return magnitude
        }
        return 0.0
    }

    override fun copy(target: Character): Status {
        return Abuff(target, modded, magnitude, duration)
    }
}

package status

import characters.Anatomy
import characters.Character
import characters.Pool
import characters.Trait
import combat.Damage
import combat.DamageType

class Cynical(affected: Character) : Status("Cynical", affected) {
    init {
        flag(Stsflag.cynical)
        //flag(Stsflag.mojodeny)
        tooltip = "Immune to mind-affecting statuses"
        this.affected = affected
        duration = if (affected.has(Trait.PersonalInertia)) {
            5
        } else {
            3
        }
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You're feeling more cynical than usual and won't fall for any mind games."
        } else {
            affected!!.name + " has a cynical edge in her eyes."
        }
    }

    override fun tempted(x: Double): Double {
        return -x / 4
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        val temp = Damage()
        if (type == DamageType.Tempt) temp[DamageType.Tempt] = -amount / 4
        return temp
    }

    override fun gain(pool: Pool, x: Double): Double {
        return super.gain(pool, x)
        return if (pool == Pool.MOJO) {
            -x
        } else {
            0.0
        }
    }

    override fun copy(target: Character): Status {
        return Cynical(target)
    }
}

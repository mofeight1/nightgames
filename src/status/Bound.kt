package status

import characters.Character
import characters.Emotion
import combat.Combat

class Bound(affected: Character, private var toughness: Int, private val binding: String) :
    Status("Bound", affected) {
    init {
        flag(Stsflag.bound)
        tooltip = "Unable to move. Escape difficulty: $toughness"
        this.affected = affected
        decaying = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "Your hands are bound by $binding."
        } else {
            "Her hands are restrained by $binding."
        }
    }

    override fun evade(): Double {
        return -15.0
    }

    override fun escape(): Int {
        val dc = toughness
        toughness -= 5
        return -dc
    }

    override fun counter(): Int {
        return -10
    }

    override fun toString(): String {
        return binding
    }

    override fun copy(target: Character): Status {
        return Bound(target, toughness, binding)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.desperate, 10)
        affected!!.emote(Emotion.nervous, 10)
    }
}

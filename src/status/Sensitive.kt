package status

import characters.Anatomy
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat

class Sensitive(affected: Character?, duration: Int, part: Anatomy, value: Double) :
    Status("Sensitive: $part", affected) {
    private val part: Anatomy
    private val value: Double

    init {
        this.duration = duration
        this.part = part
        this.value = value
        this.flag(Stsflag.sensitive)
        lingering = true
        tooltip = "Increased pleasure to $part"
        if (value < 1.0) {
            this.name = "Numb: $part"
            tooltip = "Decreased pleasure to $part"
        }
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
        this.affected = affected
    }

    override fun describe(): String {
        return ""
    }

    override fun value(): Double {
        return value
    }

    override fun sensitive(targeted: Anatomy): Double {
        if (part == targeted) {
            return value
        }
        return 1.0
    }

    override fun copy(target: Character): Status {
        return Sensitive(target, duration, part, value)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.horny, 10)
        decay(c)
    }
}

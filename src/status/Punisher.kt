package status

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Damage
import combat.DamageType

class Punisher(affected: Character, target: Character) : Status("Punisher", affected) {
    var opponent: Character
    var triggered: Boolean

    init {
        flags.add(Stsflag.punisher)
        duration = affected[Attribute.Discipline] / 5
        opponent = target
        triggered = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You keep your riding crop at ready to counter attack."
        } else {
            affected!!.name + " is still holding her crop threateningly"
        }
    }

    override fun damage(x: Double, area: Anatomy): Double {
        if (affected!!.canAct() && area != Anatomy.soul) {
            opponent.pain(x, Anatomy.soul)
            triggered = true
            opponent.emote(Emotion.nervous, 30)
        }

        return 0.0
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        if (type == DamageType.Pain) {
            if (affected!!.canAct() && area != Anatomy.soul) {
                opponent.pain(amount, Anatomy.soul)
                triggered = true
                opponent.emote(Emotion.nervous, 30)
            }
        }
        return Damage()
    }

    override fun turn(c: Combat) {
        decay(c)
        if (triggered) {
            if (affected!!.human()) {
                c.write(
                    affected!!,
                    "You react in an instant to " + opponent.name + "'s attack, lashing out with your riding crop to strike back at her and make sure she knows better than to act up again."
                )
            } else {
                c.write(
                    affected!!,
                    "A sharp slash of " + affected!!.name + "'s riding crop against your flesh makes it clear to you how much of a mistake you just made by trying to attack her. You wince in pain as you pull back from her, and almost find yourself apologizing before you manage to stop yourself."
                )
            }
        }
        triggered = false
    }

    override fun copy(target: Character): Status {
        return Punisher(target, opponent)
    }
}

package status

import characters.Attribute
import characters.Character

class Tied(affected: Character?) : Status("Tied Up", affected) {
    init {
        flag(Stsflag.tied)
        tooltip = "Penalty to evasion, trigger arousal effect from Bondage"
        this.affected = affected
        decaying = false
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "The rope wrapped around you digs into your body, but only slows you down a bit."
        } else {
            affected!!.name + " squirms against the rope, but you know you tied it well."
        }
    }

    override fun mod(a: Attribute): Double {
        if (a == Attribute.Speed) {
            return -1.0
        }
        return 0.0
    }

    override fun evade(): Double {
        return -10.0
    }

    override fun copy(target: Character): Status {
        return Tied(target)
    }
}

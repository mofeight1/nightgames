package status

import characters.Character
import characters.Trait
import combat.Combat

class Dissolving(affected: Character?) : Status("Dissolving", affected) {
    init {
        duration = 2
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            duration = 3
        }
        flag(Stsflag.dissolving)
        tooltip = "An article of clothing dissolves each turn"
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "The corrosive solution continues to eat away at your clothes."
        } else {
            affected!!.name + "'s clothes are rapidly deteriorating."
        }
    }

    override fun copy(target: Character): Status {
        return Dissolving(target)
    }

    override fun turn(c: Combat) {
        if (affected!!.isNude) {
            affected!!.removeStatus(this, c)
            return
        }
        val lost = affected!!.shredRandom()
        if (lost != null) {
            if (affected!!.human()) {
                c.write("Your $lost dissolves away.")
            } else {
                c.write("Her $lost dissolves away.")
            }
        }
        if (affected!!.isNude) {
            affected!!.removeStatus(this, c)
            return
        }
        decay(c)
    }
}

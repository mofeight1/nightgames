package status

import characters.Character
import characters.Trait

class Unreadable(affected: Character?) : Status("Unreadable", affected) {
    init {
        this.duration = 3
        flag(Stsflag.unreadable)
        tooltip = "Hides arousal and stamina from opponent"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 5
        }
        this.affected = affected
    }

    override fun describe(): String {
        return ""
    }

    override fun copy(target: Character): Status {
        return Unreadable(target)
    }
}

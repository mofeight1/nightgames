package status

import characters.Character
import combat.Combat

class OrderedStrip(affected: Character?) : Status("Ordered to Strip", affected) {
    init {
        flag(Stsflag.orderedstrip)
        duration = 3
        tooltip = "Will receive Disobedient debuff if not naked in $duration turns."
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You've been ordered to undress"
        } else {
            "You've ordered " + affected!!.name + " to strip"
        }
    }

    override fun turn(c: Combat) {
        decay()
        if (affected!!.isNude) {
            affected!!.removeStatus(this, c)
        }
        if (duration <= 0) {
            affected!!.removeStatus(this, c)
            if (!affected!!.isNude) {
                affected!!.add(Disobedient(affected!!), c)
            }
        }
    }

    override fun copy(target: Character): Status {
        return OrderedStrip(target)
    }
}

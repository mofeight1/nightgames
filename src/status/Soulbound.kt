package status

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Damage
import combat.DamageType
import kotlin.math.roundToInt

class Soulbound(afflicted: Character, target: Character, duration: Int) : Status("Soulbound", afflicted) {
    var target: Character

    init {
        flag(Stsflag.soulbound)
        this.target = target
        this.duration = duration
        if (afflicted.has(Trait.PersonalInertia)) {
            this.duration = (3 * duration / 2.0).roundToInt()
        }
    }

    override fun damage(x: Double, area: Anatomy): Double {
        if (area != Anatomy.soul) {
            target.pain(x / 2, Anatomy.soul)
        }
        return 0.0
    }

    override fun pleasure(x: Double, area: Anatomy): Double {
        if (area != Anatomy.soul) {
            target.pleasure(x / 2, Anatomy.soul)
        }
        return 0.0
    }

    override fun damage(type: DamageType, amount: Double, area: Anatomy?): Damage {
        if (type == DamageType.Pleasure) target.pleasure(amount / 2, Anatomy.soul)
        if (type == DamageType.Pain) target.pain(amount / 2, Anatomy.soul)
        return Damage()
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "$target is still bound to you, sharing your sensations."
        } else {
            "You are still feeling everything $affected does."
        }
    }

    override fun copy(target: Character): Status {
        return Soulbound(affected!!, target, duration)
    }
}

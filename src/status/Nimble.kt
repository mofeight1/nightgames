package status

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat

class Nimble(affected: Character?, duration: Int) : Status("Nimble", affected) {
    init {
        this.duration = duration
        flag(Stsflag.nimble)
        tooltip = "Bonus to evasion and escaping based on Animism and current Arousal"
        if (affected != null && affected.has(Trait.PersonalInertia)) {
            this.duration = 3 * duration / 2
        }
        this.affected = affected
    }

    override fun describe(): String {
        return if (affected!!.human()) {
            "You're as quick and nimble as a cat."
        } else {
            affected!!.name + " darts around gracefully."
        }
    }

    override fun evade(): Double {
        return affected!![Attribute.Animism] * affected!!.arousal.percent() / 100
    }

    override fun escape(): Int {
        return (affected!![Attribute.Animism] * affected!!.arousal.percent() / 100).toInt()
    }

    override fun counter(): Int {
        return (affected!![Attribute.Animism] * affected!!.arousal.percent() / 100).toInt()
    }

    override fun copy(target: Character): Status {
        return Nimble(target, duration)
    }

    override fun turn(c: Combat) {
        affected!!.emote(Emotion.confident, 5)
        decay(c)
    }
}

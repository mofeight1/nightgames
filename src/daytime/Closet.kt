package daytime

import characters.Character
import global.Global

class Closet(player: Character) : Activity("Change Clothes", player) {
    override fun known(): Boolean {
        return true
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (choice === "Start") {
            Global.gui.changeClothes(player, this)
        } else {
            done(false)
        }
    }

    override fun shop(npc: Character, budget: Int) {
        // TODO Auto-generated method stub
    }
}

package daytime

import characters.Character
import characters.ID
import characters.Trait
import global.Flag
import global.Global
import global.Roster

class Porn(player: Character) : Activity("Browse Porn Sites", player) {
    override fun known(): Boolean {
        return Global.checkFlag(Flag.metBroker)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        if (page == 0) {
            Global.gui.next(this)
            var gain = Global.random(3) + 1
            if (player.has(Trait.expertGoogler)) {
                gain += 1
            }
            showScene(pickScene(gain))
            player.arousal.gainMax(gain)
            Global.gui.message("<b>Your maximum arousal has increased by $gain.</b>")
        } else {
            done(true)
        }
    }

    override fun shop(npc: Character, budget: Int) {
        var gain = Global.random(3) + 1
        if (npc.has(Trait.expertGoogler)) {
            gain += 1
        }
        npc.arousal.gainMax(gain)
    }

    private fun showScene(chosen: Scene) {
        when (chosen) {
            Scene.basic1 -> Global.gui
                .message("You watch a nude 'audition' by a self-proclaimed aspiring actress. If she can't fake a better orgasm than that, you can see why her career isn't going anywhere.")

            Scene.basic2 -> Global.gui
                .message("You spend about an hour browsing fetish porn websites. Some things do not need to be inserted into the human body.")

            Scene.basic3 -> Global.gui
                .message("You spend about an hour browsing fetish porn websites. You feel a bit more desensitized to normal sex and a little bit dead inside.")

            Scene.fail1 -> Global.gui
                .message("It feels like the internet has run out of sexy. There's nothing new worth fapping to. Maybe there's something decent behind this paywall? No, don't do it. It's a trap.")

            Scene.mara1 -> {
                Global.gui.message(
                    "You were planning to browse some porn and probably rub one out, but why is Mara in your room? <i>\"Don't sweat the details. I brought you " +
                            "this new porn game so we could play it together. I even saved you some time by making a custom girl who looks like me.\"</i>"
                )
                Roster.gainAffection(ID.PLAYER, ID.MARA, 1)
                Global.gui.message("<b>You've gained Affection with Mara</b>")
            }

            Scene.angel1 -> {
                Global.gui.message(
                    "When Angel invited you to watch a movie with her friends, you did not expect it to be porn. In retrospect, you probably should have. Caroline " +
                            "and Sarah have claimed comfortable looking arm chairs, while you, Angel and Mei are packed together on a small sofa. Mei grins at you suggestively. <i>\"If " +
                            "you need to whip it out and jerk off, we'll pretend not to notice.\"</i> That kinda considerate of her, but it's probably not going to be an option. Angel already " +
                            "has her hand down your pants."
                )
                Roster.gainAffection(ID.PLAYER, ID.ANGEL, 1)
                Global.gui.message("<b>You've gained Affection with Angel</b>")
            }

            Scene.reyka1 -> Global.gui.message(
                "You stumble onto a webcam of a girl who specializes in fantasy roleplay. Wait... is that Reyka? That's definitely Reyka. Can she absorb " +
                        "libido over the internet?"
            )
        }
    }

    private fun pickScene(gain: Int): Scene {
        val available = ArrayList<Scene>()
        if (gain == 1) {
            available.add(Scene.fail1)
        } else {
            available.add(Scene.basic1)
            available.add(Scene.basic2)
            available.add(Scene.basic3)
            if (Roster.getAffection(ID.PLAYER, ID.MARA) >= 5) {
                available.add(Scene.mara1)
            }
            if (Roster.getAffection(ID.PLAYER, ID.ANGEL) >= 10) {
                available.add(Scene.angel1)
            }
            if (Global.checkFlag(Flag.Reyka) && Roster.getAffection(ID.PLAYER, ID.REYKA) >= 1) {
                available.add(Scene.reyka1)
            }
        }
        return available.random()
    }

    private enum class Scene {
        basic1,
        basic2,
        basic3,
        fail1,
        mara1,
        angel1,
        reyka1
    }
}

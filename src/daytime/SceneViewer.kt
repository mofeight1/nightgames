package daytime

import characters.Character
import characters.ID
import global.Global
import global.Roster
import scenes.SceneFlag
import scenes.SceneManager
import scenes.SceneType

class SceneViewer(player: Character, private val parent: Activity) : Activity("Scene Viewer", player) {
    private var selected: ID? = null
    private var target: ID? = null
    private var category: SceneType? = null
    private val watched: List<SceneFlag> = Global.allWatched
    private val selectable = HashSet<ID>()
    private val categories = HashSet<SceneType>()
    private val availableScenes = ArrayList<SceneFlag>()

    override fun known(): Boolean {
        return false
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (choice.equals("Start", ignoreCase = true) || choice.equals("Back", ignoreCase = true)) {
            Global.gui
                .message("You review the match footage you were sent. You can revist some of the highlights of your sexual escapades here.")
            selected = null
            category = null
            target = null
            selectable.clear()
            availableScenes.clear()
            categories.clear()
            for (f in watched) {
                selectable.add(f.star)
            }
            for (actor in selectable) {
                Global.gui.choose(this, Roster[actor].name)
            }
            Global.gui.choose(parent, "Back")
        } else if (selected == null) {
            selected = ID.fromString(choice)
            categories.clear()
            var count = 0
            for (f in watched) {
                if (f.star == selected) {
                    count++
                    categories.add(f.type)
                }
            }
            for (type in categories) {
                Global.gui.choose(this, type.toString())
            }
            Global.gui.choose(this, "Back")
            Global.gui.message("Scenes featuring: $choice")
            Global.gui.message("$count total scenes available.")
        } else if (category == null) {
            category = SceneType.valueOf(choice)
            availableScenes.clear()
            for (f in watched) {
                if (f.star == selected && f.type == category) {
                    availableScenes.add(f)
                }
            }
            Global.gui.message(category.toString() + " scenes featuring " + Roster[selected!!].name + ".")
            Global.gui
                .message(availableScenes.size.toString() + " of " + SceneManager.getTotalCount(selected!!, category!!) + " seen.")
            if (category == SceneType.INTERVENTION) {
                Global.gui.message("Choose the other person to appear in the scene.")
                for (npc in Roster.combatants) {
                    if (npc.id !== selected && npc.id !== ID.PLAYER) {
                        Global.gui.choose(this, npc.name)
                    }
                }
            } else {
                for (f in availableScenes) {
                    Global.gui.choose(this, f.label)
                }
            }
            Global.gui.choose(this, "Back")
        } else if (category == SceneType.INTERVENTION && target == null) {
            target = ID.fromString(choice)
            availableScenes.clear()
            for (f in watched) {
                if (f.star == selected && f.type == category) {
                    availableScenes.add(f)
                }
            }
            Global.gui.message(category.toString() + " scenes featuring " + Roster[selected!!].name + ".")
            Global.gui
                .message(availableScenes.size.toString() + " of " + SceneManager.getTotalCount(selected!!, category!!) + " seen.")
            Global.gui.message("Additional appearance by: " + Roster[target!!].name + ".")
            for (f in availableScenes) {
                Global.gui.choose(this, f.label)
            }
            Global.gui.choose(this, "Back")
        } else {
            for (f in availableScenes) {
                if (choice.equals(f.label, ignoreCase = true)) {
                    SceneManager.play(f, Roster[target!!])
                }
            }
            Global.gui.choose(this, "Back")
        }
    }

    override fun shop(npc: Character, budget: Int) {
    }
}

package daytime

import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import combat.Combat
import global.Flag
import global.Global
import global.Modifier
import global.Roster
import scenes.SceneFlag
import scenes.SceneManager

class EveTime(player: Character) : Activity("Eve", player) {
    private var acted = false
    private var match: Opponent? = null
    private val sprite = Dummy("Eve")

    override fun known(): Boolean {
        return Global.checkFlag(Flag.Eve)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        sprite.dress()
        sprite.blush = 1
        sprite.mood = Emotion.confident
        if (choice.startsWith("Start")) {
            acted = false
            Global.gui.displayImage("chibi/eve pool.png", "Art by AimlessArt")
            if (Global.getValue(Flag.GangRank) >= 1) {
                Global.gui.message(
                    "You hang out with Eve's gang. Their presence in this shitty bar seems pretty constant, but they don't seem to care how often you come and go.<br>" +
                            "<br>" +
                            "As the lowest seniority member of the gang, you're expected to make the drinks and handle minor errands. No one demands anything unreasonable. Zoe, despite her initial hostility, seems pretty pleased to have a subordinate. She still maintains her prickly demeanor, but you can tell she's starting to warm up to you.<br>" +
                            "<br>" +
                            "Strangely, the girls seem to have free reign of the bar. At one point, you asked Eve why you never see anyone in the bar. She just shrugged. <i>\"We're here.\"</i> That seems to be all the explanation you're likely to receive from her.<br>" +
                            "<br>" +
                            "You're probably not going to get any significant answers out of Eve unless you bet her for it. Unfortunately, in your current position, you can't challenge Eve or Faye. You'll have to beat Zoe a few times to work your way up.<br>"
                )
                Global.gui.choose(this, "Challenge Zoe")
                if (Global.getValue(Flag.GangRank) >= 4) {
                    Global.gui.choose(this, "Challenge Zoe High Stakes")
                }
                Global.gui.choose(this, "Practice Fight", "Have a sex-fight off the record.")
                Roster.gainAffection(ID.PLAYER, ID.EVE, 1)
                Global.gui.choose(this, "Leave")
            } else {
                Global.gui.message(
                    "It's a bit of a walk off campus to find the bar where Eve apparently hangs out. It's a much shittier part of town than you were expecting. The bar/pool hall is only accessible via a seedy looking alleyway, with a sign you wouldn't have spotted if you didn't know what to look for. This is not a place you'd want to come to after dark.<br>" +
                            "You enter cautiously. You don't have a fake ID, but hopefully you'll have a chance to talk to her before you get kicked out. You should try to act as inconspicuous as possible to avoid getting carded.<br>" +
                            "<br>" +
                            "It turns out the bar's only occupants are three girls, including Eve. You don't recognize the other two. One is a short girl with dyed hair, torn clothing, and multiple piercings. She'd look right at home in this seedy bar except for being pretty clearly your age. The other girls seems entirely out of place here; Tall and beautiful with refined features and clothing. They immediately turn their attention toward you.<br>" +
                            "<br>" +
                            "<i>\"Well, look who it is.\"</i> Eve smirks at you. <i>\"Did Aesop send you?\"</i> You shake your head. Aesop told you where to find her, but you're here on your own business. You choose your words carefully in front of the other two girls, but make it clear that you'd like to speak to Eve in private.<br>" +
                            "<br>" +
                            "Eve picks up a pool cue and returns to a game that was apparently in progress. <i>\"Say what you want. My girls here know what's what. This is Faye,\"</i> she points her cue toward the taller girl. \"</i>And Zoe.\"</i> She points to the short girl. <br>" +
                            "<br>" +
                            "That makes sense. If this eclectic pair is hanging out with Eve, you aren't surprised they're former participants in the Games.<br>" +
                            "<i>\"No, I'm afraid not.\"</i> The tall girl, Faye, speaks in a soothing tone. <i>\"We heard about these Games from Eve here. However, we were not invited to join, despite expressing interest.\"</i><br>" +
                            "<br>" +
                            "Other other girl, Zoe approaches you with barely disguised hostility. <i>\"I take it this guy's in the Games?\"</i> She sizes you up in a way that makes you uncomfortable. <i>\"He doesn't look like much. Why'd he make the cut instead of me?\"</i><br>" +
                            "<br>" +
                            "It sounds like Eve has been pretty loose lipped. <i>\"Maya is trying to keep this shit quiet, but why the fuck should I care? I guess they can kick me out, but it's not like they can steal my powers.\"</i><br>" +
                            "Her contempt for secrecy actually suits your interests. There's a lot about the Games you don't know yet, and Eve's probably been around long enough to have heard some things.<br>" +
                            "<br>" +
                            "Eve considers this while sinking her next shot. <i>\"Yeah, I probably know some stuff. I'm not going to just tell you for free though. You want this info, which makes it valuable. It's at least worth betting on.\"</i> She takes another shot and sinks another ball. <i>\"I don't really need your money either. You'll need to wager something I want.\"</i><br>" +
                            "<br>" +
                            "Eve's desires are pretty predictable. If she wants to wager sexual favors, that's something you're prepared for.<br>" +
                            "<i>\"Well… I don't just make bets with anyone.\"</i> She aims carefully and sinks the 8 ball. Zoe curses under her breath and reluctantly gets down on her knees. Eve unzips her jeans and pulls out her impressive cock. Neither of the other girls shows any surprise at her unusual genitalia.<br>" +
                            "<br>" +
                            "Zoe gives you a resentful glare before she takes Eve's member into her mouth and starts to service it. When she wagered a blowjob on their pool game, she probably didn't expect to do it in front of a stranger.<br>" +
                            "<br>" +
                            "<i>\"You see, we have our own version of the Games.\"</i> Eve affectionately runs her hand through Zoe's short hair. <i>\"My girls here have earned the right to challenge me. If you want to join in, you'll have to become a member of my little gang and work your way up.\"</i> She closes her eyes and groans lustily. Apparently Zoe is pretty good with her mouth.<br>" +
                            "<br>" +
                            "You do your best to ignore the sex act going on in front of you. OK, let's say you wanted to join her games. What would you have to do?<br>" +
                            "<br>" +
                            "<i>\"We have a pretty strict hierarchy here. Zoe here is currently at the bottom of the ladder.\"</i> She pats the head currently blowing her. <i>\"If you join up, you'll be the new bottom, under her.\"</i> Eve thrusts her hips suddenly as she ejaculates into the girl's mouth. You wait patiently for her to finish and resume talking.<br>" +
                            "<br>" +
                            "<i>\"Good girl. Since Zoe will be your direct superior, I figure she should decide your initiation.\"</i><br>" +
                            "The short girl takes a few seconds to swallow the load Eve gave her. When she recovers, she stands up and gives you a wicked smile.<br>" +
                            "<br>" +
                            "<i>\"OK pretty boy, you want to join us? You took my spot in the Games, so you'll have to help me vent my resentment. Specifically, I want your balls.\"</i> She raises her open hand threateningly. <i>\"Five slaps. If you can handle five slaps on your balls without tapping out, I'll accept you.\"</i><br>"
                )
                Global.gui
                    .choose(this, "Initiation", "It won't be fun, but you can take it. Information is worth some pain.")
                Global.gui.choose(this, "Refuse", "No way! You're not letting this crazy bitch near your balls!")
            }
        } else if (choice.startsWith("Open the Game")) {
            if (Global.random(player.mojo.max) > match!!.dc) {
                //Win
                when (match) {
                    Opponent.ZOELOW -> {
                        SceneManager.play(SceneFlag.EveZoeLowWin)
                        if (Global.getValue(Flag.GangRank) < 4) {
                            Global.modCounter(Flag.GangRank, 1.0)
                        }
                    }

                    Opponent.ZOEHIGH -> {
                        SceneManager.play(SceneFlag.EveZoeHighWin)
                        if (Global.getValue(Flag.GangRank) < 6) {
                            Global.modCounter(Flag.GangRank, 1.0)
                        }
                    }

                    else -> {}
                }
            } else {
                //Loss
                when (match) {
                    Opponent.ZOELOW -> SceneManager.play(SceneFlag.EveZoeLowLoss)
                    Opponent.ZOEHIGH -> SceneManager.play(SceneFlag.EveZoeHighLoss)
                    else -> {}
                }
            }
            Global.gui.choose(this, "Leave")
        } else if (choice.equals("Challenge Zoe", ignoreCase = true)) {
            SceneManager.play(SceneFlag.EveZoeLowWager)
            match = Opponent.ZOELOW
            Global.gui.choose(this, "Open the Game")
        } else if (choice.equals("Challenge Zoe High Stakes", ignoreCase = true)) {
            SceneManager.play(SceneFlag.EveZoeHighWager)
            match = Opponent.ZOEHIGH
            Global.gui.choose(this, "Open the Game")
        } else if (choice.startsWith("Leave")) {
            Global.gui.showNone()
            done(acted)
        } else if (choice.startsWith("Refuse")) {
            Global.gui.message(
                "No way. You aren't going to degrade yourself just for the privilege of gambling with Eve's crew.<br>" +
                        "<br>" +
                        "You walk to the door, half-hoping Eve will relent and offer a better deal, but the girls seem to have lost interest in you. They don't say a word as you leave.<br>"
            )
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Initiation")) {
            acted = true
            SceneManager.play(SceneFlag.EveInititation)
            Roster.gainAffection(ID.PLAYER, ID.EVE, 4)
            Global.gui.choose(this, "Leave")
            Global.setCounter(Flag.GangRank, 1.0)
        } else if (choice.startsWith("Practice Fight")) {
            val fight = Combat(player, Roster[ID.EVE], listOf(Modifier.practice))
            fight.setParent(this)
            fight.go()
        } else if (choice.startsWith("PostCombat")) {
            Global.gui
                .message("With flushed faces and lingering pleasure, you clean up after your practice fight and get dressed.")
            player.rest()
            Roster[ID.EVE].rest()
            Global.gui.choose(this, "Leave")
        }
    }

    override fun shop(npc: Character, budget: Int) {
    }

    private enum class Opponent(val opponent: String, val dc: Int) {
        ZOELOW("Zoe", 50),
        ZOEHIGH("Zoe", 60),
        FAYELOW("Faye", 80),
        FAYEHIGH("Faye", 100),
        EVELOW("Eve", 125),
        EVEHIGH("Eve", 150);
    }
}

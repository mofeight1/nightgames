package daytime

import characters.Attribute
import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import characters.Trait
import global.Flag
import global.Global
import global.Roster
import items.Attachment
import items.Toy

class PlayerRoom // TODO Auto-generated constructor stub
    (player: Character) : Activity("Your Room", player) {
    private var excaliburMenu = false
    private var whiteRabbitMenu = false
    private var acted = false

    override fun known(): Boolean {
        return true
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (choice === "Start" || choice === "Back") {
            Global.gui.message(
                "You have a pretty standard single dorm room. It's a little cramped, but at least you don't have to worry about "
                        + "a roommate wondering where you go every night. Your TV is mounted on the wall, leaving you enough room for the bookshelf of "
                        + "board and video games. You try to keep it reasonably tidy in case you have company over, which is "
                        + "a much more likely prospect since you joined the Games."
            )
            if (player.has(Toy.Excalibur)) {
                Global.gui
                    .message("You've moved your computer to one side of your desk so that you have room to tinker with Sexcalibur.")
            }
            if (player.has(Toy.Aersolizer)) {
                Global.gui.message(
                    "Your multitool is tucked under your couch between matches. Jett would probably not approve, but you don't "
                            + "have a better place to store it."
                )
            }
            if (player.getPure(Attribute.Dark) >= 1) {
                Global.gui.message(
                    "A small statue sits on your bookshelf, a souvenir of your first ritual with Rin. The curse that once filled "
                            + "the statue is a part of you now."
                )
            }
            if (Roster.combatants.size > 5 && !Global.checkFlag(Flag.mysteryletter)) {
                Global.gui.message(
                    "<p>Something is off when you return to your room. The door is still locked like it was when you left it, but there's a gold envelope sitting conspicuously on your desk. It looks identical to the challenge envelopes scattered around the grounds during matches. Whoever left it could have just slid it under your door, but they clearly want you to know they can get into your room.<p>" +
                            "You carefully open the envelope. Inside is a brief printed note: \"If you want more control over the Games and don't mind getting your hands dirty, wire $5000 to the account below.\" The note is signed, \"White Rabbit.\"<p>" +
                            "According to Lilly, no one involved in the Games knows where these gold envelopes come from. Even without that information, the contents of the letter make it clear this offer isn't on the up and up. Still, if this note is from whoever is responsible for the challenges, you know they are resourceful.<p>" +
                            "You should probably keep this note to yourself while you consider the offer. You have a hunch that if you ask Aesop about this, White Rabbit will disappear down their hole."
                )
                Global.flag(Flag.mysteryletter)
            }
            excaliburMenu = false
            acted = false
            if (Global.checkFlag(Flag.excalibur)) {
                Global.gui.choose(this, "Sexcalibur")
            }
            if (Global.checkFlag(Flag.goldcockstart)) {
                Global.gui.choose(this, "Golden Cock")
            }
            if (Global.checkFlag(Flag.metWhiteRabbit)) {
                Global.gui.choose(this, "Contact White Rabbit")
            } else if (Global.checkFlag(Flag.mysteryletter) && player.money >= 5000) {
                Global.gui.choose(this, "Send $5000 to White Rabbit")
            }
            Global.gui.choose(this, "Change Clothes")
            Global.gui.choose(this, "Plan Skills")
            Global.gui.choose(this, "Replay Scenes")
            Global.gui.choose(this, "Leave")
        } else if (choice === "Leave") {
            done(acted)
        } else if (excaliburMenu) {
            if (choice.startsWith("Order Sexcalibur")) {
                if (player.money >= 1000) {
                    Global.gui.message(
                        "This is quite a pricy sex toy. Fortunately, you get free same-day shipping on orders over $900. It'll arrive in "
                                + "time for your match tonight."
                    )
                    player.money -= 1000
                    player.gain(Toy.Excalibur)
                } else {
                    Global.gui.message("You can't afford this overpriced toy.")
                }
            }
            if (choice.startsWith("Upgrade")) {
                if (!player.has(Attachment.Excalibur2)) {
                    Global.gui.message(
                        "Despite the fine build quality, it's pretty easy to disassemble Sexcalibur. You remove the existing motor and "
                                + "replace it with the high-grade model. This new motor is so powerful, you need to limit its maximum output or risk the toy "
                                + "shaking itself to pieces."
                    )
                    player.consumeAll(Attachment.Excalibur2.recipe)
                    player.money -= Attachment.Excalibur2.price
                    player.gain(Attachment.Excalibur2)
                } else if (!player.has(Attachment.Excalibur3)) {
                    Global.gui.message(
                        "Machining this quantity of titanium is no easy feat. Fortunately, the Mechanical Engineering department has some "
                                + "state of the art CNC machining equipment. You get permission to use the equipment with surprising ease. After considerable time "
                                + "and effort, you craft a masterwork metal casing."
                    )
                    player.consumeAll(Attachment.Excalibur3.recipe)
                    player.money -= Attachment.Excalibur3.price
                    player.gain(Attachment.Excalibur3)
                } else if (!player.has(Attachment.Excalibur4)) {
                    Global.gui.message(
                        "This Dragon Bone seems like the ultimate crafting material, but there isn't exactly information on the internet about "
                                + "how to use it. Forging a new Sexcalibur from it requires a combination of lore, science, and divine inspiration, but eventually "
                                + "you succeed. Sexcalibur is now a sex toy of legend, unmatched by anything that has previously been used in the Games."
                    )
                    player.consumeAll(Attachment.Excalibur4.recipe)
                    player.money -= Attachment.Excalibur4.price
                    player.gain(Attachment.Excalibur4)
                } else if (!player.has(Attachment.Excalibur5)) {
                    Global.gui.message(
                        "You've done a lot of advanced customization on Sexcalibur, but the blueprints and components are beyond even your "
                                + "understanding. The technology involved is completely alien. However, even if you can't understand the blueprints, you can "
                                + "still follow them. The resulting sex toy surpasses your wildest expectations."
                    )
                    player.consumeAll(Attachment.Excalibur5.recipe)
                    player.money -= Attachment.Excalibur5.price
                    player.gain(Attachment.Excalibur5)
                }
            }
            if (choice.startsWith("Add")) {
                Global.gui.message("With some time and effort, you craft the attachment and add it to Sexcalibur.")
                for (addon in Attachment.entries) {
                    if (choice.contains(addon.getFullName(player))) {
                        player.consumeAll(addon.recipe)
                        player.money -= addon.price
                        player.gain(addon)
                    }
                }
            }
            this.acted = true
            Global.gui.choose(this, "Leave")
        } else if (whiteRabbitMenu) {
            if (choice.startsWith("Close")) {
                Global.gui.message("You leave the conversation with White Rabbit.")
                whiteRabbitMenu = false
                this.visit("Back")
            } else if (choice.startsWith("Bring someone back")) {
                Global.gui.message(
                    "White Rabbit: Having 2nd thoughts? My goal is to get people out of the Games, not back in. " +
                            "You can always talk to Aesop, I'm sure the organizers have asked him to look into recruiting the prodigal combatants again."
                )
                Global.gui.choose(this, "Next")
            } else if (choice.startsWith("Next")) {
                val participating = ArrayList<Character>()
                val benched = ArrayList<Character>()
                for (c in Roster.existingPlayers) {
                    if (!c.human() && c.id !== ID.MAYA) {
                        if (c.isBenched) {
                            benched.add(c)
                        } else if (Roster.canParticipate(c.id)) {
                            participating.add(c)
                        }
                    }
                }
                if (benched.isEmpty()) {
                    Global.gui.message("White Rabbit: Are you ready to get started?")
                } else {
                    Global.gui
                        .message("White Rabbit: The following combatants will no longer participate in the Games, thanks to us.")
                    for (c in benched) {
                        Global.gui.message(c.name)
                    }
                    Global.gui.choose(this, "Bring someone back")
                    Global.gui.message("<p>")
                }
                Global.gui.message(
                    "White Rabbit: The following competitors are currently participating in the Games with you.<br>" +
                            "White Rabbit: For $3000, I can ensure one of them declines to participate in future matches."
                )
                for (c in participating) {
                    Global.gui.message(c.name)
                }
                if (player.money < 3000) {
                    Global.gui.message("<p>White Rabbit: If you want to remove someone, save up some money.")
                } else if (participating.size < 5) {
                    Global.gui.message(
                        "<p>White Rabbit: If anyone else quits, there won't be enough participants to fill a match. If that happens, " +
                                "you can be sure Maya and the Benefactor will come down on us. Let's lay low for now."
                    )
                } else {
                    Global.gui.message("<p>White Rabbit: Who do you want to be rid of?")
                    for (c in participating) {
                        Global.gui.choose(this, c.name)
                    }
                }

                Global.gui.choose(this, "Close chat")
            } else {
                when (choice) {
                    "Cassie" -> {
                        Global.gui
                            .message("White Rabbit: Cassie will soon be the recipient of a generous scholarship with a strict morals clause. She won't risk getting caught naked in public.")
                        Roster["Cassie"].isBenched = true
                    }

                    "Mara" -> {
                        Global.gui
                            .message("White Rabbit: Mara is about to be too busy with her new research grant to participate in the games anytime soon.")
                        Roster["Mara"].isBenched = true
                    }

                    "Angel" -> {
                        Global.gui
                            .message("White Rabbit: A prominent porn producer is going to ask Angel and her friends to star in his new amateur video. She'll jump at the chance, even if filming conflicts with the match hours.")
                        Roster["Angel"].isBenched = true
                    }

                    "Jewel" -> {
                        Global.gui
                            .message("White Rabbit: Two words: fight club. I'll need to break the first two rules to tell Jewel about it, but she'll be happy to spend her nights there.")
                        Roster["Jewel"].isBenched = true
                    }

                    "Samantha" -> {
                        Global.gui
                            .message("White Rabbit: Samantha's little paparazzi mess is going to go away very suddenly. I'm sure she'll be back to her usual business in no time.")
                        Roster["Samantha"].isBenched = true
                    }

                    "Reyka" -> {
                        Global.gui
                            .message("White Rabbit: I'm sending Reyka an ad for a new adult online RPG. Knowing her personality, she'll be addicted for the foreseeable future.")
                        Roster["Reyka"].isBenched = true
                    }

                    "Valerie" -> {
                        Global.gui
                            .message("White Rabbit: There's another version of the Games that sounds right up Valerie's alley. I'll send her some anonymous hints and let her curiosity do the rest.")
                        Roster["Valerie"].isBenched = true
                    }

                    "Yui" -> {
                        Global.gui
                            .message("White Rabbit: Yui is going to receive some nightly training from a senior member of her clan. Unfortunately, this will keep her too busy at night to participate in matches.")
                        Roster["Yui"].isBenched = true
                    }

                    "Kat" -> {
                        Global.gui.message("White Rabbit: I'll make sure Kat's feral side keeps her busy at night.")
                        Roster["Kat"].isBenched = true
                    }

                    "Sofia" -> {
                        Global.gui
                            .message("White Rabbit: There are a bunch of masochists who frequent the XXX Store and would love to meet Sofia. That should scratch her itch even better than the Games.")
                        Roster["Sofia"].isBenched = true
                    }

                    "Eve" -> {
                        Global.gui
                            .message("White Rabbit: Eve was on the fence about participating in the Games in the first place. It should be easy to focus her on something else.")
                        Roster["Eve"].isBenched = true
                    }
                }
                player.money -= 3000
                Global.gui.choose(this, "Next")
            }
        } else if (choice === "Sexcalibur") {
            excaliburMenu = true
            if (player.has(Toy.Excalibur)) {
                if (!player.has(Attachment.Excalibur2)) {
                    Global.gui.message(
                        "Your Sexcalibur is factory standard. It's fine for a sex toy, but the components "
                                + "aren't good enough to justify the price. The best place to start would be replacing the motor with a "
                                + "high end model."
                    )
                    if (player.hasAll(Attachment.Excalibur2.recipe) && player.money >= Attachment.Excalibur2.price) {
                        Global.gui
                            .choose(this, "Upgrade", "Required: High Grade Motor, $" + Attachment.Excalibur2.price)
                    } else {
                        Global.gui
                            .unchoose(this, "Upgrade", "Required: High Grade Motor, $" + Attachment.Excalibur2.price)
                    }
                } else if (!player.has(Attachment.Excalibur3)) {
                    Global.gui.message(
                        "This Grand Sexcalibur contains the best small motor on the market. Most of the rest of it "
                                + "is still just plastic. The casing isn't really durable enough to support the maximum vibration the "
                                + "motor is capable of. Improving it further would require rebuilding the whole body out of a stronger "
                                + "material."
                    )
                    if (player.hasAll(Attachment.Excalibur3.recipe) && player.money >= Attachment.Excalibur3.price) {
                        Global.gui.choose(this, "Upgrade", "Required: Titanium, $" + Attachment.Excalibur3.price)
                    } else {
                        Global.gui.unchoose(this, "Upgrade", "Required: Titanium, $" + Attachment.Excalibur3.price)
                    }
                } else if (!player.has(Attachment.Excalibur4)) {
                    Global.gui.message(
                        "Sexcalibur is now a genuine masterwork sex toy. Improving it further would require something truly "
                                + "mythical. Before entering the games, that would seem impossible, but your perception of reality and fantasy "
                                + "has been challenged a lot recently."
                    )
                    if (player.hasAll(Attachment.Excalibur4.recipe) && player.money >= Attachment.Excalibur4.price) {
                        Global.gui.choose(this, "Upgrade", "Required: Dragon Bone, $" + Attachment.Excalibur4.price)
                    } else {
                        Global.gui.unchoose(this, "Upgrade", "Required: Dragon Bone, $" + Attachment.Excalibur4.price)
                    }
                } else if (!player.has(Attachment.Excalibur5)) {
                    Global.gui.message(
                        "Sexcalibur is now worthy of legend. This toy, crafted out of actual dragon bone, inspires awe "
                                + "in any sexfighter who sees it, let alone feels its potent vibrations. Logic tells you that you've improve "
                                + "this toy to the maximum limit, but your heart whispers that there is still more."
                    )
                    if (player.hasAll(Attachment.Excalibur5.recipe) && player.money >= Attachment.Excalibur5.price) {
                        Global.gui.choose(
                            this,
                            "Upgrade",
                            "Required: Prototype Blueprints, Prototype Vibrator, Prototype Handle, $" + Attachment.Excalibur5.price
                        )
                    } else {
                        Global.gui.unchoose(
                            this,
                            "Upgrade",
                            "Required: Prototype Blueprints, Prototype Vibrator, Prototype Handle, $" + Attachment.Excalibur5.price
                        )
                    }
                } else {
                    Global.gui.message(
                        "Sexcalibur is now the perfect sex toy in every way, surpassing human understanding. If there is "
                                + "any way to improve it further, you dare not even think about it."
                    )
                }
                if (player.has(Attachment.ExcaliburAnimism)) {
                    Global.gui.message(
                        "The head of the vibrator is covered with soft fox fur and embued with the spirit of a mystical "
                                + "fox creature."
                    )
                } else if (player.getPure(Attribute.Animism) >= 1) {
                    Global.gui.message("You could probably power up the Sexcalibur using an animal spirit.")
                    if (player.getPure(Attribute.Animism) < 12) {
                        Global.gui.message("You would need to improve your understanding of Animism to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburAnimism.recipe)) {
                        Global.gui.message("You would need something with a lot of residual spiritual energy.")
                    } else {
                        Global.gui.message(
                            "The Fox Tail you found could summon a spirit. You'd also need $" +
                                    Attachment.ExcaliburAnimism.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburAnimism.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburAnimism.getFullName(player))
                        }
                    }
                }
                if (player.has(Attachment.ExcaliburArcane)) {
                    Global.gui
                        .message("The magical enchantment carved into the shaft should draw mana out of the target and into you.")
                } else if (player.getPure(Attribute.Arcane) >= 1) {
                    Global.gui.message("You could enchant Sexcalibur using Arcane magic.")
                    if (player.getPure(Attribute.Arcane) < 12) {
                        Global.gui.message("You would need to learn more Arcane magic to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburArcane.recipe)) {
                        Global.gui.message("You would need help from the faeries. A lot of them")
                    } else {
                        Global.gui.message(
                            "You would need help from the faeries. The summoning scroll could call "
                                    + "enough of them to complete the process. You'd also need $" +
                                    Attachment.ExcaliburArcane.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburArcane.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburArcane.getFullName(player))
                        }
                    }
                    Global.gui.message("<br>")
                }
                if (player.has(Attachment.ExcaliburDark)) {
                    Global.gui
                        .message("The toys radiates a cursed aura. It should feed off the target's impure thoughts to gain power.")
                } else if (player.getPure(Attribute.Dark) >= 1) {
                    Global.gui.message("A carefully applied curse could make Sexcalibur more dangerous.")
                    if (player.getPure(Attribute.Dark) < 12) {
                        Global.gui.message("You would need more Dark power to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburDark.recipe)) {
                        Global.gui.message("You would need some sort of cursed token.")
                    } else {
                        Global.gui.message(
                            "You could transfer the curse from a Dark Talisman. You'd also need $" +
                                    Attachment.ExcaliburDark.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburDark.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburDark.getFullName(player))
                        }
                    }
                    Global.gui.message("<br>")
                }
                if (player.has(Attachment.ExcaliburFetish)) {
                    Global.gui.message(
                        "A braid of small tentacles are wrapped around the shaft. They should come alive and go wild "
                                + "in response to your arousal."
                    )
                } else if (player.getPure(Attribute.Fetish) >= 1) {
                    Global.gui.message("This could do with some tentacles. Pretty much everything could.")
                    if (player.getPure(Attribute.Fetish) < 12) {
                        Global.gui.message("You would need to develop your Fetishes more to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburFetish.recipe)) {
                        Global.gui.message("You would need something capable of sustaining tentacles.")
                    } else {
                        Global.gui.message(
                            "Your Fetish Totem would work. You'd also need $" +
                                    Attachment.ExcaliburFetish.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburFetish.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburFetish.getFullName(player))
                        }
                    }
                    Global.gui.message("<br>")
                }
                if (player.has(Attachment.ExcaliburKi)) {
                    Global.gui.message(
                        "The handle is specially designed to stimulate your chakras as you hold it. Just using the toy "
                                + "will recover some of your stamina."
                    )
                } else if (player.getPure(Attribute.Ki) >= 1) {
                    Global.gui
                        .message("With some modification, the handle of Sexcalibur could help open your chakras to improve Ki flow.")
                    if (player.getPure(Attribute.Ki) < 12) {
                        Global.gui.message("You would need to train your Ki more to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburKi.recipe)) {
                        Global.gui.message("You would need to embed a powerful item.")
                    } else {
                        Global.gui.message(
                            "You could remake the handle out of this Power Band. You'd also need $" +
                                    Attachment.ExcaliburKi.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburKi.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburKi.getFullName(player))
                        }
                    }
                    Global.gui.message("<br>")
                }
                if (player.has(Attachment.ExcaliburNinjutsu)) {
                    Global.gui.message(
                        "The shaft has been shortened slightly to fit inside a concealed holster. Opponents won't see your "
                                + "attack coming until it's too late to block."
                    )
                } else if (player.getPure(Attribute.Ninjutsu) >= 1) {
                    Global.gui.message(
                        "It would be easier to catch opponents by surprise if they didn't see Sexcalibur attached to your belt. "
                                + "You should craft a hidden holster for it."
                    )
                    if (player.getPure(Attribute.Ninjutsu) < 12) {
                        Global.gui.message("You would need more knowledge of Ninjutsu to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburNinjutsu.recipe)) {
                        Global.gui.message("You would need to a very light, but durable material to craft it.")
                    } else {
                        Global.gui.message(
                            "The Serpent Skin you found would be perfect for a holster. You'd also need $" +
                                    Attachment.ExcaliburNinjutsu.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburNinjutsu.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburNinjutsu.getFullName(player))
                        }
                    }
                    Global.gui.message("<br>")
                }
                if (player.has(Attachment.ExcaliburScience)) {
                    Global.gui.message(
                        "A small tube runs up the shaft to the head. It will carry dissolving solution from a tank in the handle "
                                + "to a tiny sprayer."
                    )
                } else if (player.getPure(Attribute.Science) >= 1) {
                    Global.gui.message(
                        "If Sexcalibur could deliver some dissolving solution on contact, you could use it against "
                                + "opponents who still have their clothes on."
                    )
                    if (player.getPure(Attribute.Science) < 12) {
                        Global.gui.message("You would need more Science knowledge to attempt it.")
                    } else if (!player.hasAll(Attachment.ExcaliburScience.recipe)) {
                        Global.gui
                            .message("There isn't a lot of space in the head. You'd need a very compact delivery system.")
                    } else {
                        Global.gui.message(
                            "You could fit a Micro Sprayer in the head of the toy and store the solution in the handle. "
                                    + "You'd also need $" + Attachment.ExcaliburScience.price + " for additional materials."
                        )
                        if (player.money >= Attachment.ExcaliburScience.price) {
                            Global.gui.choose(this, "Add " + Attachment.ExcaliburScience.getFullName(player))
                        }
                    }
                    Global.gui.message("<br>")
                }
            } else {
                Global.gui.message(
                    "You eventually locate an online retailer selling the Sexcalibur sex toy. Unfortunately, it's as "
                            + "expensive as Aesop warned you. There aren't a lot of product reviews available."
                )
                Global.gui.choose(this, "Order Sexcalibur: $1000")
            }
            Global.gui.choose(this, "Back")
        } else if (choice === "Golden Cock") {
            var checks = 0
            if (player.has(Trait.goldcock)) {
                Global.gui.message(
                    "You have completely mastered the Way of the Golden Cock and achieved sexual enlightenment. There "
                            + "is nothing more to learn from this guide. You feel like you're on the verge of something incredible. You just "
                            + "need to perservere.<p>"
                )
            } else if (player.has(Trait.silvercock)) {
                Global.gui
                    .message("You've almost completed the Way. This last group of goals is the hardest as expected. ")
                if (Global.checkFlag(Flag.Mayabeaten)) {
                    Global.gui.message("Defeat an overpowering opponent (Complete)")
                    checks++
                } else {
                    Global.gui.message("Defeat an overpowering opponent")
                }
                if (Global.getValue(Flag.ThreesomeCnt) >= 30) {
                    Global.gui.message("Have 30 threesomes: 30/30 (Complete)")
                    checks++
                } else {
                    Global.gui.message(
                        String.format(
                            "Have 30 threesomes: %d/30",
                            Math.round(Global.getValue(Flag.ThreesomeCnt))
                        )
                    )
                }
                val lovers = countLovers()
                if (lovers >= 4) {
                    Global.gui.message("Have four girls in love with you: 4/4 (Complete)")
                    checks++
                } else {
                    Global.gui.message(String.format("Have four girls in love with you: %d/4", lovers))
                }
                if (player.stamina.max >= 150) {
                    Global.gui.message("Raise max Stamina to 150: 150/150 (Complete)")
                    checks++
                } else {
                    Global.gui
                        .message(String.format("Raise max Stamina to 150: %d/150", player.stamina.max))
                }
                if (player.arousal.max >= 250) {
                    Global.gui.message("Raise max Arousal to 250: 250/250 (Complete)")
                    checks++
                } else {
                    Global.gui
                        .message(String.format("Raise max Arousal to 250: %d/250", player.arousal.max))
                }
                if (checks >= 5) {
                    Global.gui.choose(
                        this,
                        "Meditate",
                        "You've completed the requirements. It's time to look within yourself to see what you've learned"
                    )
                }
            } else if (player.has(Trait.bronzecock)) {
                Global.gui.message(
                    "This new list of goals is primarily focused on your performance in the matches, confirming this "
                            + "guide is targeted at participants in the Games. You're less skeptical of the value of this guide now, but some "
                            + "of these goals are clearly going to take some time.<p>"
                )
                if (Global.getValue(Flag.MatchWins) >= 25) {
                    Global.gui.message("Finish 25 matches in first place: 25/25 (Complete)")
                    checks++
                } else {
                    Global.gui.message(
                        String.format(
                            "Finish 25 matches in first place: %d/25", Math.round(
                                Global.getValue(
                                    Flag.MatchWins
                                )
                            )
                        )
                    )
                }
                if (Global.getValue(Flag.HandicapMatches) >= 20) {
                    Global.gui.message("Accept 20 match handicaps: 20/20 (Complete)")
                    checks++
                } else {
                    Global.gui.message(
                        String.format(
                            "Accept 20 match handicaps: %d/20", Math.round(
                                Global.getValue(
                                    Flag.HandicapMatches
                                )
                            )
                        )
                    )
                }
                if (Global.getValue(Flag.IntercourseWins) >= 50) {
                    Global.gui.message("Defeat 50 opponents using sexual penetration: 50/50 (Complete)")
                    checks++
                } else {
                    Global.gui.message(
                        String.format(
                            "Defeat 50 opponents using sexual penetration: %d/50", Math.round(
                                Global.getValue(Flag.IntercourseWins)
                            )
                        )
                    )
                }
                if (player.getPure(Attribute.Speed) >= 12) {
                    Global.gui.message("Raise Speed to 12: 12/12 (Complete)")
                    checks++
                } else {
                    Global.gui.message(String.format("Raise Speed to 12: %d/12", player.getPure(Attribute.Speed)))
                }
                val max = MaxAdvanced()
                if (max >= 30) {
                    Global.gui.message("Have 30 points in an advanced Attribute: 30/30 (Complete)")
                    checks++
                } else {
                    Global.gui.message(String.format("Have 30 points in an advanced Attribute: %d/30", max))
                }
                if (checks >= 5) {
                    Global.gui.choose(
                        this,
                        "Meditate",
                        "You've completed the requirements. It's time to look within yourself to see what you've learned"
                    )
                }
            } else {
                Global.gui.message(
                    "So far this guide has been a bit short on guidance. There is mostly just a list of goals and "
                            + "a checklist to keep track of your progress. After completing the list, you're suppose to meditate on everything "
                            + "you've learned. It's implied that more goals will become available after finishing these.<p>"
                            + "So far you're having trouble imagining you'll get your money's worth out of this app.<p>"
                )
                val lovers = countLovers()
                if (lovers >= 2) {
                    Global.gui.message("Have two girls in love with you: 2/2 (Complete)")
                    checks++
                } else {
                    Global.gui.message(String.format("Have two girls in love with you: %d/2", lovers))
                }
                if (Global.getValue(Flag.ThreesomeCnt) > 0) {
                    Global.gui.message("Have a threesome (Complete)")
                    checks++
                } else {
                    Global.gui.message("Have a threesome")
                }

                if (player.stamina.max >= 80) {
                    Global.gui.message("Raise max Stamina to 80: 80/80 (Complete)")
                    checks++
                } else {
                    Global.gui
                        .message(String.format("Raise max Stamina to 80: %d/80", player.stamina.max))
                }
                if (player.arousal.max >= 125) {
                    Global.gui.message("Raise max Arousal to 125: 125/125 (Complete)")
                    checks++
                } else {
                    Global.gui
                        .message(String.format("Raise max Arousal to 125: %d/125", player.arousal.max))
                }
                if (checks >= 4) {
                    Global.gui.choose(
                        this,
                        "Meditate",
                        "You've completed the requirements. It's time to look within yourself to see what you've learned"
                    )
                }
            }
            Global.gui.choose(this, "Back")
        } else if (choice === "Meditate") {
            if (player.has(Trait.silvercock)) {
                Global.gui.message(
                    "You've done it. You've come so far and overcome every obstacle in your path. It's time to complete the Way of "
                            + "the Golden Cock. You've loved many women, sometimes at the same time. You've refined your body to be the ideal sex-fighter. "
                            + "As you center yourself, you can feel spiritual energy flow into your abdomen. You realize your cock has become the perfect "
                            + "vessel for spiritual power. As the energy grows, light begins to leak out of your pants. When you undress, you discover that "
                            + "your cock is radiating a golden light. The is the Golden Cock that the way is named for."
                )
                player.add(Trait.goldcock)
            } else if (player.has(Trait.bronzecock)) {
                Global.gui.message(
                    "You sit and reflect on everything that has brought you this far. You've gained quite a lot of experience in the "
                            + "Games. You've fucked your way to victory time and time "
                            + "again and become strong. At this point, your penis may be your single most exercised organ. After overcoming so much pleasure, "
                            + "you realize you've become more resistant to it. "
                )
                player.add(Trait.silvercock)
            } else {
                Global.gui.message(
                    "You're suppose to reflect on what you've learned so far. So far you've mostly just learned how to have a lot of sex. "
                            + "You suppose that is basically the point of the guide though. You think back to all the sex you've had recently, which doesn't "
                            + "seem to give you anything except a boner. You relax to calm down your arousal, but you discover you can regain your erection "
                            + "with minimal effort. If you can get hard at will, you'll never have to worry about not being aroused enough to initiate sex."
                )
                player.add(Trait.bronzecock)
            }
            Global.gui.choose(this, "Leave")
        } else if (choice === "Change Clothes") {
            Global.gui.changeClothes(player, this)
        } else if (choice === "Plan Skills") {
            Global.gui.message("Upcoming Skills:")
            for (att in player.att.keys) {
                Global.gui.message(Global.getUpcomingSkills(att, player.getPure(att!!)))
            }
            Global.gui.choose(this, "Back")
        } else if (choice === "Contact White Rabbit") {
            whiteRabbitMenu = true
            Global.gui
                .message("You contact White Rabbit on the address she gave you. She responds almost immediately.")
            Global.gui.choose(this, "Next")
        } else if (choice === "Send $5000 to White Rabbit") {
            player.money -= 5000
            val sprite = Dummy("Alice")
            sprite.dress()
            sprite.mood = Emotion.confident
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "You wire the money to the account White Rabbit listed. $5000 is not trivial, but the Games pay well. Hopefully this will be worth the investment.<p>" +
                        "A short while later, you receive a call from a restricted number. \"If you want to meet, follow my directions. If you are too slow, I hang up. If you are followed, I hang up.\" The caller is using a voice modulator, so you can't even be sure of their gender, but you're confident it's not the Benefactor who called you before.<p>" +
                        "You follow the directions from the anonymous caller, which lead you to an old house. Judging by the lack of furniture and For Sale sign on the lawn, it's currently unoccupied, but the door is unlocked.<p>" +
                        "Inside, you are led to a room that is empty, save for a webcam. \"Now, undress so I can see you aren't wearing a wire.\" The voice on the phone is all business.<p>" +
                        "You hesitate, this is reaching an extreme level of paranoia. Still, you're getting pretty used to public nudity. You strip down to your underwear and turn around in front of the webcam. <p>" +
                        "\"Take everything off. Don't be shy.\" Well, you've come this far. You shed your underwear, and probably whatever is left of your dignity. \"Good. Leave the phone with your clothes and meet me upstairs, in the room on the left.\"<p>" +
                        "The room turns out to be empty, save for two chairs. You take a seat in one, still naked, and wait.<p>" +
                        "You are only mildly surprised when Alice, the girl from the XXX Store, walks in and takes the seat across from you. When you saw White Rabbit on the note, her name did pop into your head, but it was only a tenuous connection at best.<p>" +
                        "\"Thank you for accepting the extreme conditions for this meeting.\" Her voice is hollow as usual, you don't sense any sincerity in her words. \"Unfortunately, this level of caution is necessary, given the nature of the proposal I have for you. Thank you also for your monetary donation. I'm sorry to say that if the subject of this conversation reaches Maya's ears, that money will be used to implicate you as an accomplice. For both our sakes, please do everything in your power to prevent that from happening.\"<p>" +
                        "Blackmailing you right off the bat doesn't leave a great first impression, but you stifle your anger until she gets to the point.<p>" +
                        "\"You are here because you wanted to be able to influence the Games. That is also something I want, and together we can make it possible. Is there anyone you would like to stop seeing in the Games? Maybe they're giving you trouble? Maybe they just don't appeal to you? Would you be willing to pay a few thousand dollars to arrange for them to bow out of the Games?\"<p>" +
                        "This is sounding pretty sinister. Is Alice talking about disappearing your opponents? There certainly isn't anyone you'd wish harm or coercion on.<p>" +
                        "\"Don't worry. I'm not planning anything malicious. I'm simply going to fulfil their needs, so they no longer have any reason to participate in the Games. It's win-win for everyone, except for the Benefactor.\" She folds her hands on her lap, her face still not betraying any emotion. \"Everyone has something they want. That's why they are participating in the Games. I've investigated all of the current participants, and identified their raison d'etre.\"<p>" +
                        "\"Everyone except you.\" Alice leans forward slightly. \"I can't identify any deeper reason for you to participate in the Games, besides the obvious. I certainly couldn't find anything to use as leverage. That's why I decided it would be more prudent to make you my ally. You can decide whose needs we will satisfy as long as you are willing to help bankroll it.\"<p>" +
                        "Alice leans back in her chair and waits for your response, but there isn't much for you to say. You're already $5000 deep in this scheme and she's not asking for more of a commitment yet. Rather, it seems like she isn't planning to move ahead unless you take the initiative.<p>" +
                        "She seems satisfied by your lack of protest and hands you a small piece of paper. \"This is the only time we'll speak in person about this. Here is the address of a private IRC server where you can contact me. Refer to me as White Rabbit online, and Alice in person. Never mix them up.\"<p>" +
                        "With that, she leaves you to get dressed and stew on her proposal."
            )
            Global.flag(Flag.metWhiteRabbit)
            Global.gui.choose(this, "Leave")
        } else if (choice.equals("Replay Scenes", ignoreCase = true)) {
            SceneViewer(player, this).visit("Start")
        }
    }

    private fun countLovers(): Int {
        return Roster.existingPlayers.count { it.getAffection(player) >= 25 }
    }

    private fun MaxAdvanced(): Int {
        var sum = 0
        for (att in Attribute.entries) {
            when (att) {
                Attribute.Arcane,
                Attribute.Animism,
                Attribute.Ki,
                Attribute.Dark,
                Attribute.Fetish,
                Attribute.Submissive,
                Attribute.Ninjutsu,
                Attribute.Science -> sum += player.getPure(att)

                else -> {}
            }
        }
        return sum
    }

    override fun shop(npc: Character, budget: Int) {
        // TODO Auto-generated method stub
    }
}

package daytime

import characters.Attribute
import characters.Character
import characters.NPC
import characters.Player
import global.Flag
import global.Global
import global.Roster
import global.SaveManager
import global.Scheduler
import java.time.LocalTime

class Daytime {
    private var activities: ArrayList<Activity> = ArrayList()
    private val player: Player
    private val time: Int
    private val auto: Events
    private val NPCDAYTIME = 8
    
    constructor(player: Player) {
        Global.gui.clearText()
        this.player = player
        manageFlags()
        buildActivities()
        this.auto = Events(player, this)
        time = 9
        if (auto.checkMorning()) {
            return
        }
        plan()
    }

    fun manageFlags() {
        if (Global.checkFlag(Flag.metAlice)) {
            if (Global.checkFlag(Flag.victory)) {
                Global.unflag(Flag.AliceAvailable)
            } else {
                Global.flag(Flag.AliceAvailable)
            }
        }
        if (Global.checkFlag(Flag.metYui)) {
            Global.flag(Flag.YuiAvailable)
        }
        if (Global.checkFlag(Flag.threesome)) {
            Global.unflag(Flag.threesome)
        }
    }

    fun plan() {
        var scene: String
        if (Scheduler.time.hour < 22) {
            if (Scheduler.getDayString(Scheduler.date).startsWith("Sunday")) {
                Global.gui.message("It is currently ${Scheduler.timeString}. There is no match tonight.")
            } else if (!Scheduler.hasMatch(player)) {
                Global.gui.message("It is currently ${Scheduler.timeString}. You have tonight off.")
            } else {
                Global.gui.message("It is currently ${Scheduler.timeString}. Your next match starts at 10:00pm.")
            }
            Global.gui.refresh()
            Global.gui.clearCommand()
            Global.gui.showNone()
            if (auto.checkScenes()) {
                return
            }
            for (act in activities) {
                if (act.known() && Scheduler.time.hour <= 22) {
                    Global.gui.addActivity(act)
                }
            }
        } else {
            for (npc in Roster.existingPlayers) {
                if (!npc.human()) {
                    if (npc.level >= 10 * (npc.rank + 1)) {
                        npc.rankup()
                    }
                    (npc as NPC).daytime(NPCDAYTIME, this)
                }
            }
            //Global.gui.nextMatch();
            if (Global.checkFlag(Flag.autosave)) {
                SaveManager.save(true)
            }
            Scheduler.dusk()
        }
    }

    fun buildActivities() {
        activities = ArrayList()
        activities.add(Exercise(player))
        activities.add(Porn(player))
        activities.add(VideoGames(player))
        activities.add(Informant(player))
        activities.add(BlackMarket(player))
        activities.add(XxxStore(player))
        activities.add(HWStore(player))
        activities.add(Bookstore(player))
        activities.add(Dojo(player))
        activities.add(AngelTime(player))
        activities.add(CassieTime(player))
        activities.add(JewelTime(player))
        activities.add(MaraTime(player))
        if (Global.checkFlag(Flag.Kat)) {
            activities.add(KatTime(player))
        }
        activities.add(PlayerRoom(player))
        activities.add(ClothingStore(player))
        if (Global.checkFlag(Flag.Reyka)) {
            activities.add(ReykaTime(player))
        }
        if (Global.checkFlag(Flag.Samantha)) {
            activities.add(SamanthaTime(player))
        }
        activities.add(MagicTraining(player))
        activities.add(Workshop(player))
        activities.add(YuiTime(player))
        activities.add(GinetteTime(player))
        activities.add(ValerieTime(player))
        activities.add(EveTime(player))
        activities.add(Specialize(player))
    }

    /*fun getTime(): Int {
        return Scheduler.time.hour
    }*/

    fun advance(t: Int) {
        Scheduler.advanceTime(LocalTime.of(1, 0))
    }

    fun visit(name: String, npc: Character, budget: Int) {
        for (a in activities) {
            if (a.toString().equals(name, ignoreCase = true)) {
                a.shop(npc, budget)
                break
            }
        }
    }

    companion object {
        fun train(one: Character, two: Character, att: Attribute) {
            val a: Int
            val b: Int
            if (one.getPure(att) > two.getPure(att)) {
                a = 100 - (2 * one[Attribute.Perception])
                b = 90 - (2 * two[Attribute.Perception])
            } else if (one.getPure(att) < two.getPure(att)) {
                a = 90 - (2 * one[Attribute.Perception])
                b = 100 - (2 * two[Attribute.Perception])
            } else {
                a = 100 - (2 * one[Attribute.Perception])
                b = 100 - (2 * two[Attribute.Perception])
            }
            if (Global.random(100) >= a) {
                one.mod(att, 1)
                if (one.human()) {
                    Global.gui.message("<b>Your $att has improved.</b>")
                }
            }
        }
    }
}

package daytime

import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import combat.Combat
import global.Flag
import global.Global
import global.Modifier
import global.Roster

class SofiaTime(player: Character) : Activity("Sofia", player) {
    private val sofia = Roster[ID.SOFIA]
    private val sprite = Dummy("Sofia", 1, true)

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        sprite.dress()
        sprite.setCostumeLevel(1)
        sprite.blush = 0
        sprite.mood = Emotion.confident
        if (choice === "Start") {
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Practice Fight")) {
            val fight = Combat(player, Roster[ID.SOFIA], listOf(Modifier.practice))
            fight.setParent(this)
            fight.go()
        } else if (choice.startsWith("PostCombat")) {
            Global.gui
                .message("With flushed faces and lingering pleasure, you clean up after your practice fight and get dressed.")
            player.rest()
            Roster[ID.SOFIA].rest()
            Global.gui.choose(this, "Leave")
        } else if (choice === "Leave") {
            Global.gui.showNone()
            Global.setCounter(Flag.SofiaDWV, 0.0)
            done(true)
        }
    }

    override fun known(): Boolean {
        return Global.checkFlag(Flag.Sofia)
    }

    private fun promptScenes() {
        Global.gui.choose(this, "Practice Fight", "Have a sex-fight off the record.")
    }

    override fun shop(npc: Character, budget: Int) {
        Roster.gainAffection(npc.id, ID.SOFIA, 1)
    }
}

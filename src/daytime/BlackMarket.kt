package daytime

import characters.Attribute
import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import global.Flag
import global.Global
import global.Roster
import items.Flask
import items.Potion
import kotlin.math.min

class BlackMarket(player: Character) : Store("Black Market", player) {
    private var trained = false
    private val sprite: Dummy

    init {
        add(Flask.Aphrodisiac)
        add(Flask.SPotion)
        add(Flask.DisSol)
        add(Potion.Beer)
        sprite = Dummy("Rin", 1, true)
    }

    override fun known(): Boolean {
        return Global.checkFlag(Flag.blackMarket)
    }

    override fun visit(choice: String) {
        if (choice === "Start") {
            acted = false
            trained = false
        }
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (choice === "Leave") {
            done(acted)
            return
        }
        checkSale(choice)
        if (player.human()) {
            if (Global.checkFlag(Flag.blackMarketPlus)) {
                sprite.dress()
                sprite.blush = 0
                sprite.mood = Emotion.confident
                Global.gui.loadPortrait(player, sprite)
                when {
                    !Global.checkFlag(Flag.metRin) -> {
                        Global.gui.message(
                            "You knock on the door to the black market. When Ridley answers, you tell him that you're here to see his premium goods on behalf of " +
                                    "Callisto. Ridley glances back into the room for a moment and then walks past you without saying anything. You stand there confused, until you see the " +
                                    "girl on the couch stand up and approach you with a smile.<p>" +
                                    "<i>\"Hello " + player.name + ",\"</i> she says while extending her hand. <i>\"I'm Rin Callisto. You shouldn't " +
                                    "be surprised I know who you are, you've been putting on a good show lately.\"</i> You've seen her here before, but you've never taken a good look at her. She " +
                                    "has elegant features, shoulder length black hair, and looks a couple years older than you. She's very pretty, but you overlooked her because you assumed she " +
                                    "was Ridley's girlfriend.<p>" +
                                    "<i>\"Mike isn't the most pleasant company, but he's a good middleman. He keeps his mouth shut and he doesn't ask questions.\"</i> He must " +
                                    "not be reliable enough to earn her trust, otherwise she wouldn't feel the need to keep an eye on him all the time.<p>" +
                                    "<i>\"Aesop sold you my name, right? I'll have " +
                                    "to collect my share from him later. I have many items you won't find anywhere else; items that will give you an edge in the Games. Not all of them are completely " +
                                    "safe, but I think you knew that when you came looking for the black market.\"</i>"
                        )
                        Global.flag(Flag.metRin)
                        Roster.gainAffection(ID.PLAYER, ID.RIN, 1)
                        Global.gui.message("<b>You've gained Affection with Rin</b>")
                    }
                    choice.startsWith("Cursed Artifacts") -> {
                        Global.gui.message(
                            "You ask Rin about the assorted tomes and unpleasant looking idols she's laid out.<p>"
                                    + "<i>\"You should be careful with those, they're all " +
                                    "cursed. None of them will kill you unless you're unusually susceptible to such things, but the effects would not be pleasant.\"</i> Ok, it sounds " +
                                    "like these aren't what you're looking for. You aren't looking to get cursed, no matter how useful the artifacts are.<p>"
                                    + "<i>\"The items aren't very " +
                                    "valuable themselves, otherwise I'd have sold them to collectors. A skilled spiritualist could refine the curse to bestow an unholy boon. Fortunately, " +
                                    "I have the training to do so.\"</i> An unholy boon? That sounds more than a bit worrying.<p>"
                                    + "<i>\"Are you religious? In my experience, demons are just another " +
                                    "flavor of spirit. It's still your choice, do what you like.\"</i>"
                        )
                        Global.flag(Flag.darkness)
                        acted = true
                    }
                    choice.startsWith("Dark Power") -> {
                        if (player.money >= player.advancedTrainingCost) {
                            player.money -= player.advancedTrainingCost
                            var scene = ""
                            clearRandomScenes()
                            addRandomScene("Generic", 1)
                            if (player.getPure(Attribute.Dark) >= 6) {
                                addRandomScene("Imp", 1)
                                if (player.getPure(Attribute.Dark) == 6) {
                                    scene = "Imp"
                                }
                                if (player.getPure(Attribute.Dark) == 9) {
                                    scene = "Dominate"
                                } else if (player.getPure(Attribute.Dark) > 9) {
                                    addRandomScene("Dominate", 1)
                                }
                            }


                            /*if(player.getPure(Attribute.Dark)>=8){
                                     * Learning Domination
                                     * }
                                     */
                            if (scene === "") {
                                scene = randomScene
                            }
                            when {
                                scene.startsWith("Dominate") -> {
                                    sprite.undress()
                                    sprite.blush = 1
                                    sprite.mood = Emotion.confident
                                    Global.gui.loadPortrait(player, sprite)
                                    Global.gui.message(
                                        "<i>\"You ever try Dominating someone?\"</i> <p>" +
                                                "And with those words you wonder if it would be poor form to just walk out of the room. Still, you suppress the urge to bolt at Rin's question and ask what exactly she means. <p>" +
                                                "<i>\"I don't mean you taking control of the pacing when you fuck,\"</i> She says, stopping what thoughts you had in your head, <i>\"I mean to actually take control. To make someone submit to your will and make them follow your commands.\"</i> <p>" +
                                                "That sounds like suspiciously like mind control. You must have shown something on your face because Rin shrugged. <p>" +
                                                "<i>\"It's not exactly what you think. I guess you could call it mind control with an extremely weak willed target but overall it's more like ‘suggesting' loudly that they should follow your orders. Demonic spells tend to focus more on tempting the target instead of changing their minds entirely. More importantly you're not actually controlling them, they're still aware. The more submissive they are, the more likely they'll respond to you.\"</i> <p>" +
                                                "Submissive? You recall some of the girls in the Games and you don't think that'll apply to some of them. Rin raises an eyebrow, <i>\"Everyone has a bit of submissive in them. They can hide it all they want but you can drag it out if you try hard or if they trust you enough. If you can ask them for a piece of gum you could get something out of them.\"</i> <p>" +
                                                "You think some of them might be real protective of their gum. Ignoring that, you tell Rin that sounds easily abusable, <i>\"That's because it is. The Dark Arts can give you power but you have to be careful not to let it overwhelm you. It's a very slippery slope and there's a reason people have to be vetted before I show them any of these things.\"</i> <p>" +
                                                "You understand, letting her know that your grateful for the opportunity. Rin looks surprised at your thanks but quickly waves it off, <i>\"Yeah well you were recommended and paid in full already. Here's what you do. You can control some of the dark energy independently now right?\"</i> <p>" +
                                                "With the lessons you've done with Rin you can say that you have some control but not a lot. <p>" +
                                                "<i>\"As long as you have some control then we can get started on this lesson. You should have enough energy to do several attempts,\"</i> She says, making you recall her words whenever she channels a artifacts energy into you, <i>\"It's simple. You channel the energy into your voice and focus your mind into making them do what you want. Considering how often you guys fuck in the Games and the cordial relationships you have out of them, I bet you could have your opponents hold whatever they were thinking about to follow your commands.\"</i> <p>" +
                                                "You can get behind that. Still, that does bring up the question of how you were going to practice this. It's not like Rin's going to volunteer herself. <p>" +
                                                "<i>\"Of course I am.\"</i> <p>" +
                                                "Say what? <p>" +
                                                "<i>\"You paid for your training and I'm not going to let you out of here with some vague notion of what to do and with no supervision. Ignoring how that would ruin my reputation as a trainer, Dark magic can spiral way out of control. No, you're just going to have to practice on me till you get it right.\"</i> <p>" +
                                                "Well if she gave her permission no reason not to start. You begin concentrating on the Dark energy in your body. You can feel it wiggling in your chest, wanting to get out and be used for some nefarious purpose. When you push it out to your throat, you shape the energy into words and command loudly for Rin to submit. Rin's legs give out from under her, forcing her to land hard on her knees. You can't help but wince but before you can help she stands right back up with her hands on her hips, expression blank. <p>" +
                                                "<i>\"Too much energy, not enough control. Try to be more focused when you Dominate me. Remember, it's pushing someone's willingness to obey versus actually forcing them. Don't force it out like a yell. Yelling isn't really that attractive to most girls. You're going to have to be a bit more suave.\"</i> <p>" +
                                                "You nod. You keep working on summoning the power, letting it through your voice as you focus on dominating Rin's mind with your sheer thoughts. At first her legs twitched, then she opened her legs slightly before resetting back, but with each attempt you slowly are able to get the right amount of dark energy and the proper control to get Rin to submit. It's not long before slowly a blush emerges as she slowly unzipped her pants and leans back onto a wall. <p>" +
                                                "You're exhausted by the time you get her to present herself but once you see her clothing off you can't help but feel a rush of power. It's suddenly a lot easier to grab onto the Dark. You pull out more and more, some from inside you and some from around you, trying to push the limits of what you can control. At first you were cautious but over time it started to feel...comforting? Relaxing? Something soothing. The Dark energy turned out to be a lot easier to manage than you thought. <p>" +
                                                "<i>\"Not bad,\"</i> She says, blush evaporating almost instantly as she suddenly seems completely unaffected by her nudity. She leans back more relaxed on the wall despite the fact her bottom being openly presentable to you. It's only when she looks at you she scowls, <i>\"Your control is slipping. Are you alright?\"</i> <p>" +
                                                "Alright? More than alright. This power is easy. You could Dominate Rin right here on the wall if you wanted to. In fact, she's practically presenting herself right now. You can't help but lick your lips as the power forms in your mouth quickly. <p>" +
                                                "At first Rin looks surprised that you order her to spread her legs as you take off your pants. Rin blushes once more and a low growl escapes her but her fingers trace down to her lower mouth with her back to the wall. She begins fingering herself, staring at you with fire in her eyes but clearly doing what you want. Still, you can't stop grinning and when her body is wet enough, you slip your cock in without any issue. <p>" +
                                                "This is amazing! The rush of power, the feeling of superiority, no wonder Rin always plays at being in control. This thrill of domination as you pump your hips deeper and deeper into her fills you with a bigger sense of euphoria. More than you ever felt in your life. Rin's quiet grunts and huffs of pleasure only fuel your desire to dominate her further. As you cum deep into Rin, you grin with just how far you could push this. How no one could ever get in your way with this dominating power. No one in the Games could possibly beat yo- <p>" +
                                                "<i>\"Okay enough of that.\"</i> <p>" +
                                                "A sudden void emerges inside you. It felt like the entire world just went flat. The colors bleed away to reveal grays and noises suddenly become muted. You find it hard to breath only for everything to morph back to normal as if nothing happened. What just happened? <p>" +
                                                "Your eyes go back to Rin, who is staring back at you with a tiny bit of concern. Your whole body suddenly feels tired as you realize you don't feel any of the Dark inside of you. You fall forward onto Rin, who despite her position is able to hold you up with minimal effort. Your eyes blink heavily as you Rin strokes your head gently. What were you rambling on about before? <p>" +
                                                "<i>\"This is why we make sure not to let ourselves be consumed by the Dark,\"</i> Rin warns, <i>\"It can take you, feed the part of you that wants more, and then spit out something that's unrecognizable. You have to be careful or else you'll lose both yourself and those you affect.\"</i> <p>" +
                                                "You nod slowly, the haze slowly lifting from your mind and body. You feel guilty at your urges going wild like that. Rin chuckles as she can sense your discomfort and pats you on the back. <p>" +
                                                "<i>\"This is why you need to train here with me,\"</i> Rin says, <i>\"You practice this on someone else and it can get bad. You need someone to keep you afloat or else you'll drown in the depths.\"</i> <p>" +
                                                "You imagine doing this to Angel, to Mara, to Cassie. Your mind shivers at how badly it could have gotten considering how you didn't notice you were going overboard. You tell Rin you can't promise that you won't lose control again but you'll try your best. Rin chuckles. <p>" +
                                                "<i>\"Good. If you said you would never lose control again I would have taken whatever power you had left and thrown you out,\"</i> You blink at her admission but she continues, <i>\"As long as you know it will tempt you constantly then you can eventually control it. Overconfidence leads nowhere.\"</i> <p>" +
                                                "You thank her, feeling way too tired to continue this conversation now. Rin points to the couch as she disengages from you, seemingly unperturbed at the fact your cum is sliding down her leg. <p>" +
                                                "<i>\"Go take a siesta. We can train another day. Besides, I need to clean up before another customer comes in.\"</i> <p>" +
                                                "You nod and crash head first onto the couch. Sleep comes easily.<p>"
                                    )
                                }
                                scene === "Imp" -> {
                                    Global.gui.message(
                                        "Rin takes you to a dimly-lit back room which is entirely empty, except for a strange, ornate circle drawn on the ground.  The circle is about 10 feet in diameter, and takes up most of the room.  Rin closes the door behind you, and faces the circle.  <i>\"Imps are easy to manipulate,\"</i> she says, <i>\"because they are so predictable.  They value sex and domination above all else, and form intensely hierarchical relationships.  Every imp, upon meeting another imp, has to determine who the dominant one is, and who the submissive one is.  Once an imp is submissive, that imp becomes the dominant one's thrall, and will forever do the bidding of their master without question.  Observe.\"</i><br>" +
                                                "Rin begins chanting slowly, the circle begins to glow, and two shadowy shapes begin to form within.  Rin finishes her chant, and you see a male and a female imp inside the circle, facing each other.  The imps are both particularly attractive specimens: the female with her perky, shapely breasts and round, firm ass; the male with hefty, engorged balls and thick, pendulous cock.  They posture aggressively at one another for a moment, before lunging at each other in a wild battle for dominance.  Biting, punching, kicking, kissing, and sucking each other senseless, the two seem evenly-matched as they hotly vie for control of each other's orifices.<br>" +
                                                "<i>\"These battles can sometimes last for hours, or even days,\"</i> Rin says, sounding a little bored, <i>\"so we are going to speed up the process.\"</i>  Rin casts a quick spell, and you see shadowy tendrils appear all around the male imp.  They grab at his hands and legs, pulling them in all directions and immobilizing him spread-eagle, standing up.  The female imp smiles sadistically and takes a few steps backwards, only to get a running start and kick the helpless male squarely in his massive, dangling testicles.  You hear a loud smacking sound as the female imp's foot collides with the male imp's balls, and the male lets out a low, pained groan.  More out of efficiency than empathy, Rin promptly ends her spell and frees the male imp, who crumples to the ground, clutching his testicles. <br>" +
                                                "The female imp doesn't hesitate to press her advantage, and she quickly moves to smear her slippery pussy over the stunned male's nose and mouth.  Her fragrant aphrodisiac takes its effect almost immediately, and you see the male imp's dick engorge in a matter of seconds.  While the male is still paralyzed with pain, the female easily takes the male's throbbing dick into her mouth, and the battle is settled within moments as he cannot resist shooting a massive load down her coaxing throat.<br>" +
                                                "<i>\"This is where it gets interesting,\"</i> Rin says, as the female imp wipes her mouth.  <i>\"If someone were to now dominate the female imp, they would gain the loyalty of both the female and the male, since he has become her thrall.  He will be incapacitated for a while, and she should still be weakened from her previous battle.\"</i>  It suddenly dawns on you what Rin expects you to do.  <i>\"It's a real two-for-one deal, good luck!\"</i>  Rin says, and gestures towards the circle. <br>" +
                                                "You know what needs to be done, so you step into the circle, ready to challenge the small female imp.  The imp licks her lips as you approach, and you each size each other up, before she pounces at you.  You are considerably bigger than she is, so she doesn't provide much of a challenge - you easily swat her away when she tries to fondle you, and your superior strength lets you hold her down with one hand while pleasuring her with the other.  You soon find yourself behind her, with one hand gently squeezing her tiny, plump breasts and the other lightly teasing her clit. <br>" +
                                                "The imp is shuddering on the brink of orgasm, and you momentarily let your guard down, assured of your victory.  You open your legs into a wider stance to gain leverage and finish her off, but the imp seizes her opportunity by swiftly kicking her foot backwards, striking you cleanly between the balls with the back of her small, knobby heel.  You let go of the imp as your body lurches forward, and you cup your acing scrotum with your hands and bring your knees together.  Bent over in pain, you are easy to knock off-balance, so the imp further presses her advantage by hooking your knee and tripping you flat on your back, though you keep your legs crossed and your knees to your chest.<br>" +
                                                " <i>\"You have to win for the ritual to work.\"</i>  Rin reminds you, matter-of-factly. <br>" +
                                                "Your hands are too busy clutching at your bruised balls to stop the imp from straddling your head and wiping a thick bead of seductive lubricant onto your face; the aphrodisiac begins working immediately and you feel your penis begin to stiffen involuntarily – you realize you had better finish this match quickly if you want to win.  The imp stands up with her legs on either side of your head, and you see your chance; you punch your fist up between her legs, repaying her low blow with one of your own.  Your knuckles collide solidly with her clit, and you hear the imp let out a low groan, crossing her eyes as she collapses forwards, bending over and holding her pussy.  You capitalize upon her position by quickly getting behind her and dominantly inserting yourself into her tight slit, doggy-style.<br>" +
                                                "You completely knocked the fight out of her with that crotch shot, and she doesn't seem able to resist as you grab her small body by the hips from behind and fuck her senseless.  You feel her spasm and tremble as she climaxes, and you quickly follow with an orgasm of your own, finalizing your victory.  You pull yourself out of the exhausted imp, and both the male and female imps disappear in black wisps of smoke. <br>" +
                                                "<i>\"Both imps are now under your control, and from now on they will come to your aid when you call them.\"</i> Rin says.  <i>\"Use them wisely, an imp's loyalty is not to be abused.  This concludes today's lesson.\"</i>  You make your way back outside, eager to try your new power.<br>"
                                    )
                                }
                                scene === "Generic" -> {
                                    Global.gui.message(
                                        "Rin lights some incense and has you lie down on the couch with one of the cursed artifacts on your chest. As she performs a lengthy " +
                                                "ritual, you feel your body heat up and an overwhelming sense of danger flood through you. You're certain something powerful is trying to take control " +
                                                "of your soul.<p>"
                                                + "Rin chants softly and wraps a talisman around a wooden rod. The presence inside you looms and seems ready to devour you, when suddenly she " +
                                                "strikes the artifact with the rod. A shock runs through your consciousness, and the sense of danger disappears. The dark power is still present, but it " +
                                                "seems tame now, willing to obey your command should you call for it.<p>"
                                                + "<i>\"The ritual is complete. You can keep the artifact as a souvenir, but all its " +
                                                "power is in you now.\"</i><p>"
                                    )
                                }
                            }
                            player.mod(Attribute.Dark, 1)
                            Roster.gainAffection(ID.PLAYER, ID.RIN, 1)
                            Global.gui.message("<b>You've gained Affection with Rin</b>")
                            acted = true
                            trained = true
                        } else {
                            Global.gui.message("You can't afford the artifacts.")
                        }
                    }
                    choice === "S&M Gear" -> {
                        Global.gui.message(
                            "Rin opens a case containing a variety of S&M toys and bondage gear.<p>"
                                    + "<i>\"You probably shouldn't touch those,\"</i> she says as you reach for them. " +
                                    "<i>\"I didn't pick these up at a sex shop, they have potent enchantments on them.\"</i><p>"
                                    + "Enchanted sex toys? At this point, you'll believe just about anything. How " +
                                    "useful are they? <i>\"They won't do you much good to use them during the match, but if you use them now, their power will transfer to you. Mostly they'll give " +
                                    "you fetishes that you'll be able to share with your opponents. It's a high risk, high reward style of sex-fighting.\"</i>"
                                    + "<p>Rin smiles and reclines on the couch. " +
                                    "<i>\"If you buy something, I don't mind helping you try it out. No extra charge of course, I'm not a prostitute.\"</i>"
                        )
                        Global.flag(Flag.fetishism)
                        acted = true
                    }
                    choice.startsWith("Fetishism") -> {
                        if (player.money >= player.advancedTrainingCost) {
                            player.money -= player.advancedTrainingCost
                            player.mod(Attribute.Fetish, 1)
                            acted = true
                            trained = true
                            acted = true
                            clearRandomScenes()
                            addRandomScene("Exhibitionism", 1)
                            var scene = ""

                            if (player.getPure(Attribute.Fetish) == 1) {
                                scene = "Exhibitionism"
                            }
                            if (player.getPure(Attribute.Fetish) > 3) {
                                addRandomScene("Bondage", 2)
                            }
                            if (player.getPure(Attribute.Fetish) == 3) {
                                scene = "Bondage"
                            }
                            if (player.getPure(Attribute.Fetish) > 9) {
                                addRandomScene("Masochism", 2)
                            }
                            if (player.getPure(Attribute.Fetish) == 9) {
                                scene = "Masochism"
                            }
                            if (player.getPure(Attribute.Fetish) > 12) {
                                addRandomScene("Tentacles", 2)
                            }
                            if (player.getPure(Attribute.Fetish) == 12) {
                                scene = "Tentacles"
                            }
                            if (scene === "") {
                                scene = randomScene
                            }
                            when {
                                scene.startsWith("Exhibitionism") -> {
                                    Global.gui.message(
                                        "You look through the box and your eye is caught by a collar lying toward the bottom of the pile.  You pick it up and Rin's eyes light up.  <br>" +
                                                "<i>\"Excellent,\"</i> she says, <i>\"I'm . . . I mean we are going to have a lot of fun with that.  It will grant you the exhibitionism fetish.\"</i><br>" +
                                                "You look at her in shock, which she easily recognizes on your face.  <i>\"What?  You don't like people seeing you naked?\"</i> she asks.  <i>\"Seems like you should get used to it since you participate in the games every night.\"</i>  You hadn't thought about it that way and start to relax.  <i>\"There you go, time to strip.\"</i><br>" +
                                                "You slowly strip out of your clothes, unsure about doing this in front of Rin.  She watches in mild amusement though as you do, showing no sign of turning away to give you privacy.  Finally you remove your underwear and stand there in front of her completely naked, not sure what to do next.  Once you have your arms at your sides she walks over and slips the collar around your neck.  She gets very close to you, bumping against your flaccid penis with her hip as she fastens it against your skin.  You instinctively pull away and a slight smile crosses her face as you do.  Her fingers linger against your neck for a minute after she finishes working on the buckle and then she finally steps back. <br>" +
                                                "<i>\"It will take a minute to begin to work,\"</i> she says and stands there watching you.  She just stares at you and you stare back.  You initially feel like you should cover yourself with your hands but you force yourself to continue to stand there with your arms at your side.  As the minutes tick by, your skin starts to tingle, starting slowly at your neck and then spreading down your body.  It travels down your chest, through your stomach and into your crotch.  As it spreads, you begin to notice you are more comfortable with what is happening and don't feel the desire to move your hands in front of yourself.  You begin to actually enjoy the fact that Rin is watching you and you relax.<br>" +
                                                "<i>\"Good,\"</i> Rin finally says, <i>\"It looks like the enchantment is starting to take effect.\"</i>  She takes out her phone and holds it up in front of herself.  <i>\"Now for the real thing.\"</i>  Your eyes widen as you hear her phone make the familiar sound that comes with it taking a picture.  She takes a couple more as your hands move to cover your crotch.<br>" +
                                                "<i>\"Nope, move those hands,\"</i> she says.  <i>\"You are my little bitch now and this is part of the process. I'm taking these pictures to prove that you now like being exposed to other people.\"</i> She adds as if reading your mind.  You drop your hands and she takes a couple more pictures.  Each subsequent picture causes the tingling that has continued across your skin to begin to subside.  It starts receding from the furthest parts of your body and centers on your groin. <br>" +
                                                "<i>\"Hands behind your back,\"</i> Rin suddenly demands.  <i>\"Let me see your little dick.\"</i>  You hesitate for a minute but then do as she says.  As she takes a few more pictures the sensation fully focuses around your cock and balls then begins to strengthen.  <i>\"Huh, not enjoying this?  I thought you were more of a show off, since you are in the games.  But you haven't even started to harden yet.\"</i><br>" +
                                                "<i>\"Now put your hands behind your head,\"</i> she demands.  This time you don't hesitate and stand there without a hint of modesty as she clicks away.  You feel your cock finally begin to harden as you realize you are actually enjoying this session.  The question of what she is planning to do with the pictures doesn't even cross your mind at this point, you just want her to keep going.  <br>" +
                                                "<i>\"Grab your dick and start stroking,\"</i> she says after seeing that you had become comfortable with just posing naked.  This escalation does cause you to hesitate slightly, but after a moment's thought you begin throbbing at the thought and can't resist following her direction.  You begin to slowly stoke your length, enjoying the feeling.  You hear her phone make a different sound from before and you realize that she is now taking a video of you instead of just pictures.<br>" +
                                                "<i>\"Stroke faster, I know you want to you little slut,\"</i> she says.  You pick up the pace. <i>\"Good, my friends are going to love this little video.\"</i>  Your jaw drops when she says that, you didn't realize that she was planning to share these with anyone.  However, despite your surprise at this revelation you keep stroking.  <br>" +
                                                "<i>\"You're enjoying this,\"</i> she states, <i>\"you can't resist being turned on by the idea of my friends watching my little toy play with himself.\"</i>  You realize that she is right as your excitement is quickly rising inside you.  The idea of people you don't know, and not knowing how many there might be, watching you masturbate is definitely turning you on.<br>" +
                                                "<i>\"Come on boy, let them see you cum,\"</i> Rin encourages you, <i>\"Let them know how little control you have over yourself.\"</i>  Your breathing has increased and you feel your orgasm rising inside you as you reach the point of no return.  A small part of your brain wants you to stop, but the larger part wants to show everyone that might watch this video your release.  The majority wins out and you explode only a few minutes later, spraying your sperm out and onto the floor.<br>" +
                                                "<i>\"There you go,\"</i> she says as you empty your balls onto her floor.  You hear her phone chime as she pushes something on the screen and she lowers it.  She tosses you a box of Kleenex.  <i>\"The transfer has been successful, you now have the exhibitionism fetish.  Now clean up and get dressed.\"</i><br>"
                                    )
                                }
                                scene.startsWith("Masochism") -> {
                                    Global.gui.message(
                                        "You select one of the S&M toys and pay Rin for it. She picks up the toy - a leather riding crop - and "
                                                + "gives you a small wicked smile.<p>"
                                                + "<i>\"Good choice. This one will grant you the masochism fetish if used properly. I'm quite good with these. "
                                                + "Get undressed and we'll get started.\"</i><p>"
                                                + "Your trepidation must show on your face, because her smiles becomes "
                                                + "slightly more reassuring. <i>\"Sometimes the path to power can be painful. The gifts in these items may bring you "
                                                + "victory, but there's no easy way to unlock them. Besides, you'll enjoy it before we're through. That's the whole "
                                                + "point after all.\"</i><p>"
                                                + "You strip and stand awkwardly in front of Rin who grins unabashedly as she stares at your flaccid member, twirling "
                                                + "the leather riding crop between her fingers.<p>"
                                                + "<i>\"I must say, " + player.name + ", you're quickly becoming one of my favorite customers.\"</i><p>"
                                                + "Rin expertly twirls the riding crop one last time before sending a hard swat to your nipple!<p>"
                                                + "You gasp at the stinging sensation and are surprised by the animalistic urge that flares in the back of your mind.<p>"
                                                + "She reels back and smacks your other nipple even harder than before! Another primal, beastly feeling flared in the "
                                                + "back of your mind! Your chest is heaving with breaths that are coming faster and faster. Your mind rushes with images "
                                                + "of slapping the riding crop from Rin's hands and pushing her up against the wall!<p>"
                                                + "Rin traces the tip of the riding crop down your abs and teases your stiffening cock for a moment before sliding it "
                                                + "toward your butt. She reels back and delivers full strength blow with a loud SMACK!<p>"
                                                + "Much to your own surprise, you moan in pleasure!<p>"
                                                + "As the pain from the welting blow on your ass grows your mind races with images of ripping Rin's clothes off and "
                                                + "making her yours. You're starting to understand that the pain gets you in touch with your inner beast; the part of "
                                                + "you that was designed to hunt sabretooth tigers and to mount girls like Rin.<p>"
                                                + "And Rin? The fact that such a petite, slender girl was causing you this pain was incredibly arousing! You, a strong "
                                                + "man who could stop this at any moment, wait in excitement for where she'll hit you next. Wherever it is, you know you "
                                                + "can take it!<p>"
                                                + "<i>\"Wow,\"</i> Rin is staring in wide-eyed disbelief at your throbbing erection. <i>\"I've never seen the enchanted "
                                                + "riding crop work so quickly! You must've been a fairly kinky man before this!\"</i><p>"
                                                + "She delivers another intoxicating smack to your butt!<p>"
                                                + "<i>\"Let's make a fun little wager! If you stay standing for three more hits than I'll let you fuck me however "
                                                + "you please.\"</i> Rin doesn't even wait for you to acknowledge her bet before she flicks her wrist between your legs!<p>"
                                                + "The tip of the riding crop hits your right ball, causing an intense flash of pain that almost drops you to your knees.<p>"
                                                + "<i>\"Hmm, not bad, but I've had men stay standing after the first blow before.\"</i> Rin gently rubs your balls with the "
                                                + "riding crop as she mocks you. She slowly reels the riding crop back as she prepares her next strike.<p>"
                                                + "You fight the instinctive urge to protect yourself and grit your teeth.<p>"
                                                + "The riding crop smacks hard against your left testicle! Your knees wobble as the intense pain takes over all your senses. "
                                                + "You almost to drop to the floor but stay standing so your struggle thus far wasn't in vain.<p>"
                                                + "<i>\"Wow! I am impressed!\"</i> Rin reaches over and cups your burning testicles. <i>\"Almost everyone is done by the second swat! I wonder if you'll be the first to stay standing after all three! I doubt it though, seeing as you were just as dumb as the others.\"</i><p>" +
                                                "Before you can fully comprehend the implication of that statement, Rin grabs your shoulder and launches a devastating knee to your testicles!<p>" +
                                                "You drop to the floor and moan, clutching at your bruised balls.<p>" +
                                                "<i>\"I said three hits. Not three hits with the riding crop. Anyhow, your erection is proof that the fetish has been transferred. You may take as long as you need to recover before you show yourself out.\"</i>"
                                    )
                                }
                                scene.startsWith("Bondage") -> {
                                    Global.gui.message(
                                        "You pull a long, golden rope out of the box and pay Rin for it. She gently picks up the rope and smiles seductively.<p>"
                                                + "<i>\"A man after my own tastes. This rope will bestow unto you a bondage fetish. Please hold out your wrists and we will "
                                                + "begin the process.\"</i><p>"
                                                + "You anxiously hold out your wrists and present them to Rin.<p> "
                                                + "Your self-doubt at the choice you made grows as you watch her expertly tie a comfortable, yet inescapable, knot around "
                                                + "your wrists. Just as you're about to voice your concerns she gives the rope one final tug before taking a step back "
                                                + "and admiring her work.<p>"
                                                + "For a moment it seems as if nothing happened. You're about to call the entire thing off before you feel a familiar "
                                                + "twitch of arousal in your cock. Your cock continues to grow as your heart and breath begin to race. You look up at "
                                                + "Rin in bewilderment who, you just now notice, has been hungrily staring at you this entire time."
                                                + "<i>\"You know,\"</i> she gives your bulge a soft squeeze before she begins unbuttoning your pants, <i>\"this fetish "
                                                + "isn't about immobility. Rather, it's a fetish for being completely vulnerable to your captor.\"</i>"
                                                + "Your pants fall to the floor and, to your bewilderment, your cock is harder than you can ever recall seeing it. It "
                                                + "throbs between you two before she places a delicate hand on your cock and begins to slowly stroke your shaft.<p>"
                                                + "<i>\"Tell me " + player.name + ",\"</i> her hand slides down your shaft and cups your balls, <i>\"how does it feel to be "
                                                + "at my complete mercy?\"</i> Her hand gently, yet firmly, squeezes your delicate testicles.<p>"
                                                + "It dawns on you that you hardly know this woman. You have no clue whether she'll continue to treat you gently or if "
                                                + "she has a sadistic streak. In a quick moment of panic you try to escape, but find the ropes just as unforgiving as when "
                                                + "she tied them.<p>"
                                                + "Realizing you're unable to escape sends an unexpected feeling of arousal through your body. Your mind races with lust, "
                                                + "fear, arousal, and just as she predicted, a vulnerability that feels more delicious the longer it lasts.<p>"
                                                + "Rin goes back to stroking your cock and smiles as she watches the wide range of emotions playing out on your face.<p>"
                                                + "<i>\"Enjoying it more, yes? These ropes have had even the strongest of men on their knees. And don't worry. You're "
                                                + "in absolutely no danger here.\"</i><p>"
                                                + "You breathe a sigh of relief.<p>"
                                                + "<i>\"I wouldn't relax too quickly. I'll remind you that I said I would help you learn the fetish of the item you purchased. "
                                                + "Never once did I say I would help you cum.\"</i><p>"
                                                + "Rin teases your cock for almost an hour, constantly stroking or sucking you to the edge of release but never letting "
                                                + "you reach climax. By the end of your session you're on your knees, begging her to let you cum while at the same time "
                                                + "secretly hoping she doesn't just so this pleasurable torture can continue.<p>"
                                    )
                                    Global.gui.displayImage("premium/Rin Bondage.jpg", "Art by AimlessArt")
                                    Global.gui.message(
                                        "Eventually, she rewards you for holding out as long as you did, and she swallows every drop of cum from your screaming "
                                                + "erection down her throat.<p>"
                                                + "You leave the shop in a haze, unsure of what happened, but with the rope clutched tightly in your hand."
                                    )
                                }
                                scene.startsWith("Tentacles") -> {
                                    Global.gui.message(
                                        "You shift around some of the other toys to find a magazine on the bottom. A magazine with the cover being a young woman in a school uniform being molested by slimy tentacles. Since you've been getting more used to seeing magical energies, you narrow your eyes as you see something dark oozing out of it's pages. <p>" +
                                                "<i>\"Ah, right. Might want to be careful around that one. It's cursed.\"</i> <p>" +
                                                "A snort escapes you. Isn't everything in this chest cursed? You thought that was the whole point. But still, why a porn magazine? <p>" +
                                                "<i>\"Doujin technically,\"</i> You look back towards Rin, <i>\"Fanworks. It's from some recent anime. The doujin there is a result of a botched summoning of some tentacle god. Came into my possession but none of my usual clients wants to buy it. While unique, it's a bit awkward for them to be placing that on display somewhere. Besides, it doesn't want to be left alone unused.\"</i> <p>" +
                                                "You nearly ask her what that means until you notice the dark energies wrapping themselves around your arm. At first you panic but then calm down as you realize it's just holding onto your arm. You don't feel any different, it's just sitting there. Huh. <p>" +
                                                "<i>\"All of its energy is trapped in it's pages. Once you establish a gateway, you should be able to use the doujin's power to create tentacles until you're used to it and then form your own with your own libido. Since you haven't thrown it away from you in terror, we'll be learning how to use it today.\"</i> <p>" +
                                                "Tentacles. It takes a few seconds for the word to fully register into your brain. You could make your own tentacles? The idea of watching the girls struggle valiantly only to lose and be forcibly pleasured by tentacles was a turn on when you consider it'd be yours. Limbs tied up, bodies at your mercy, it sounded better the more you thought about it. <p>" +
                                                "<i>\"Glad to see you're enjoying yourself.\"</i> Your eyes snap up to see Rin grinning. You notice the tightness in your pants and shift them to push down your rising chub, <i>\"Some people might not get the appeal but you seem to be all right with it.\"</i> <p>" +
                                                "You nod and prepare yourself. If the other lessons were an example to follow, Rin would no doubt subject you to the tentacles as well. You get ready to take your clothes on only for Rin to stop you. <p>" +
                                                "<i>\"None of that,\"</i> You blink as she takes off her own clothes, <i>\"Making and controlling tentacles requires your full concentration. If it goes out of control you could end up tearing a hole in someone's insides. You need more practice with moving the tentacles then you do getting your holes stuffed.\"</i> <p>" +
                                                "You glance at the doujin and sweat a bit. You didn't quite consider that. <p>" +
                                                "<i>\"Summon a tentacle and see how it looks.\"</i> <p>" +
                                                "You nod and without thinking will a tentacle forward. The energy of the doujin spikes and you feel a bit drained but to your surprise a thick purple tentacle just appears out of the ground and stands erect right in front of you. <p>" +
                                                "You slowly walk forward and place a hand on the tentacle. The second you make contact you feel a weird tingling sensation all over your body. As you move your hand on the service you realize it seems that you could slightly feel what the tentacle does. <p>" +
                                                "It was about as thick as your arm while height wise it reached up to your shoulders. It was smooth in an unnatural way. Normal skin has pores, marks, bumps, but as you touch it it feels like smooth silicone. Probably for the best. You don't think anyone is going to want something that causes friction burns inside of them. It takes you a moment to realize it looks exactly like the tentacles of the doujin. <p>" +
                                                "<i>\"Not bad,\"</i> A fully naked Rin stands in front of the tentacle, patting at it and nodding in approval, <i>\"See how it's just standing here? That's because you didn't give it any orders. Tell it to grab a hold of me.\"</i> <p>" +
                                                "You attempt to do as she says and watch as the tentacle roughly slaps against Rin's skin. She hisses and sends a glare your way. You wince before calming down and concentrating properly. The tentacle slowly makes its way over to Rin to gently wrap itself around her waist. <p>" +
                                                "<i>\"Good,\"</i> She pets the tentacle, causing a pleasant shiver down your spine, <i>\"now make three more tentacles and get them inside me.\"</i> <p>" +
                                                "You nod and expend more energy to summon three more tentacles. Rin's eyes widen and her skin pales as they approach her. <p>" +
                                                "<i>\"Whoa, hold up " + player.name + "! If you think these,\"</i> She points at the arm wide tentacles that froze right before entry, <i>\"can fit in me, we really need to reevaluate how you see my sex life.\"</i> <p>" +
                                                "Slapping yourself on the head, you quickly envision making the approaching tentacles around her only as thick as your dick, transferring some of your power instead of the doujin's inside it. A voice pops up somewhere in the back of your head telling you that they're fine as is but you ignore it as Rin lets out a sigh of relief. <p>" +
                                                "<i>\"Good. Now, let's see how well you can keep it up,\"</i> Catching the innuendo, you roll your eyes and will the tentacles forward. The original thick tentacle stays around her and keeps her in place as the other three go into her. While you know the tentacles aren't coarse, you stop just <p>" +
                                                "before penetrating her. Rin gives you a nod to let you know it's okay before you fully push inside. <p>" +
                                                "You watch as the tentacles begin fucking Rin's holes, slowly but surely pushing further and further in with every stroke. You have to make sure to carefully moderate how far they go by looking at Rin's face, who thankfully seems more lost in pleasure and pain. You feel some of the feedback from the tentacles and the experience of fucking past the length of your dick times three, even muted, brings you unexperience pleasure that you can barely contain. <p>" +
                                                "Needing release, you spawn another tentacle. First her right leg, then her left, then her arms, and then her breasts. Rin's moans of pleasure and accommodating holes almost drive you crazy. It all comes to an end, however, as her back arches and she bites the tentacle forcing its way down her throat. The shock of it breaks you out of your lust fueled haze and the tentacles poof out of existence. Rin drops to the floor. <p>" +
                                                "<i>\"Wow,\"</i> Rin says from the ground. You apologize for that but she holds her hand up. <p>" +
                                                "<i>\"I think I can call it even after biting your phantom dick there,\"</i> You wince at that. While it was muted it did feel like you were fucking Rin's mouth, pussy, and ass all at the same time. You shiver at the lingering phantom pain of being bitten. <i>\"When you overwrote the initial tentacle design your energy was used there. You'll feel what it does. Keep that in mind when you use it against the other girls. Take your pants off.\"</i> <p>" +
                                                "The sudden command confuses you until you look down and realize you creamed your pants. Grumbling in both annoyance and embarrassment, you take off your pants and underwear. Rin takes them and walks into another room, where the sounds of a laundry machine chime through. When she comes back she somehow has a phone with her despite still being naked. <p>" +
                                                "<i>\"We're not done. This isn't going to do you any good in the Games if you cum when you use it. I'll keep an eye on your dick while you keep working out how to control the powers of the doujin,\"</i> She throws her phone over to the side, <i>\"I cleared an hour for this so we better make good use of it.\"</i> <p>" +
                                                "The doujin flares as if excited. You grin and get to work. By the time the hour is up, Rin has trouble walking, your balls feel extremely light, and the energy in the doujin seems content. Just before you leave, where you have a plastic bag with the doujin inside of it, Rin gives you one more piece of advice. <p>" +
                                                "<i>\"Try to read the doujin every now and then. It'll help solidify any thoughts on how to make your tentacles and it's pretty lonely. Play with it every now and then.\"</i> <p>" +
                                                "Play with it? Did she sell you a pet? You look down to notice some remnant of the doujin's energy crawling up to latch onto you again. Huh, perhaps she did.<p>"
                                    )
                                }
                            }
                            Roster.gainAffection(ID.PLAYER, ID.RIN, 1)
                            Global.gui.message("<b>You've gained Affection with Rin</b>")
                        } else {
                            Global.gui.message("You can't afford that.")
                        }
                    }
                    else -> {
                        Global.gui.message(
                            "Ridley leaves the black market as soon as he sees you. It seems you're a preferred client now, and you can deal with Rin directly. Rin gives " +
                                    "you a polite smile and stands up. <i>\"Welcome back. What can I do for you?\"</i><p>"
                                    + "Upcoming Skills:<br>" +
                                    Global.getUpcomingSkills(Attribute.Dark, player.getPure(Attribute.Dark)) + "<br>" +
                                    Global.getUpcomingSkills(Attribute.Fetish, player.getPure(Attribute.Fetish))
                        )
                    }
                }
                if (!trained) {
                    if (!Global.checkFlag(Flag.darkness)) {
                        Global.gui.choose(this, "Cursed Artifacts")
                    } else {
                        Global.gui.choose(this, "Dark Power: $" + (player.advancedTrainingCost))
                    }
                    if (!Global.checkFlag(Flag.fetishism)) {
                        Global.gui.choose(this, "S&M Gear")
                    } else {
                        Global.gui.choose(this, "Fetishism: $" + (player.advancedTrainingCost))
                    }
                }
            } else {
                Global.gui.message(
                    "You knock on the door to the room Aesop pointed you to. A somewhat overweight man, a few years older than you, looks you over for a moment " +
                            "before letting you in. The dorm room is tidier than its occupant, whose clothes are noticeably stained and smell faintly of old beer and weed. There's a bong on " +
                            "the nearby table and an attractive girl on the couch who makes no indication that she noticed you enter. <i>\"Don't mind the bitch,\"</i> says Ridley, noticing your " +
                            "attention. <i>\"She doesn't care who you are and neither do I. What are you looking for?\"</i>"
                )
            }
            for (i in stock.keys) {
                Global.gui.message(i.getName() + ": $" + i.price)
            }
            Global.gui.message("You have :$" + player.money + " to spend.")
            displayGoods()
            Global.gui.choose(this, "Leave")
        }
    }

    override fun shop(npc: Character, budget: Int) {
        var remaining = budget
        if (npc.getPure(Attribute.Dark) > 0 && remaining >= npc.advancedTrainingCost) {
            if (remaining >= npc.advancedTrainingCost * 2.5) {
                npc.money -= npc.advancedTrainingCost
                remaining -= npc.advancedTrainingCost
                npc.mod(Attribute.Dark, 1)
            }
            npc.money -= npc.advancedTrainingCost
            remaining -= npc.advancedTrainingCost
            npc.mod(Attribute.Dark, 1)
        }
        if (npc.getPure(Attribute.Fetish) > 0 && remaining >= npc.advancedTrainingCost) {
            if (remaining >= npc.advancedTrainingCost * 2.5) {
                npc.money -= npc.advancedTrainingCost
                remaining -= npc.advancedTrainingCost
                npc.mod(Attribute.Fetish, 1)
            }
            npc.money -= npc.advancedTrainingCost
            remaining -= npc.advancedTrainingCost
            npc.mod(Attribute.Fetish, 1)
        }
        var bored = 0
        remaining = min(remaining, 120)
        while (remaining > 25 && bored < 5) {
            for (i in stock.keys.shuffled()) {
                if (remaining > i.price && !npc.has(i, 10)) {
                    npc.gain(i)
                    npc.money -= i.price
                    remaining -= i.price
                } else {
                    bored++
                }
            }
        }
    }
}

package daytime

import characters.Character
import global.Global
import items.Clothing
import items.Item
import utilities.EnumMap
import java.util.EnumMap

abstract class Store(name: String, player: Character) : Activity(name, player) {
    var stock: HashMap<Item, Int> = HashMap()
        protected set
    var clothing: EnumMap<Clothing, Int> = EnumMap()
        protected set
    protected var acted: Boolean

    init {
        acted = false
    }

    fun add(item: Item) {
        stock[item] = item.price
    }

    fun add(item: Clothing) {
        clothing[item] = item.price
    }


    protected fun displayGoods() {
        for (i in stock.keys) {
            Global.gui.sale(this, i)
        }
        for (i in clothing.keys) {
            if (!player.has(i)) {
                Global.gui.sale(this, i)
            }
        }
    }

    protected fun checkSale(name: String): Boolean {
        val item = stock.keys.firstOrNull { it.getName() == name } ?:
        clothing.keys.firstOrNull { it.properName == name } ?:
        return false
        if (item is Item) buy(item)
        if (item is Clothing) buy(item)
        return true
    }

    fun buy(item: Item) {
        if (player.money >= stock[item]!!) {
            player.money -= stock[item]!!
            player.gain(item)
            acted = true
            Global.gui.refreshLite()
        } else {
            Global.gui.message("You don't have enough money to purchase that.")
        }
    }

    fun buy(item: Clothing) {
        if (player.money >= clothing[item]!!) {
            player.money -= clothing[item]!!
            player.gain(item)
            acted = true
            Global.gui.refreshLite()
        } else {
            Global.gui.message("You don't have enough money to purchase that.")
        }
    }
}

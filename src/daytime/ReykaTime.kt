package daytime

import characters.Attribute
import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import characters.Trait
import combat.Combat
import global.Flag
import global.Global
import global.Modifier
import global.Roster


class ReykaTime(player: Character) : Activity("Reyka", player) {
    private val reyka = Roster[ID.REYKA]
    private val sprite = Dummy("Reyka")

    override fun known(): Boolean {
        return Global.checkFlag(Flag.Reyka)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        sprite.dress()
        sprite.blush = 0
        sprite.mood = Emotion.confident
        if (choice == "Start") {
            Global.gui.loadPortrait(player, sprite)
            if (Roster.areIntimate(ID.PLAYER, ID.REYKA)) {
                if (reyka.getAffection(player) > 25 && reyka.has(Trait.clairvoyance) && reyka.has(Trait.desensitized) /*&& false*/) {
                    Global.gui.message("[Love Intro]")
                    Global.gui.choose(this, "Games")
                    Global.gui.choose(this, "Sparring")
                    Global.gui.choose(this, "Sex")
                } else if (Global.checkFlag(Flag.ReykaDate)) {
                    Global.gui.displayImage("chibi/reyka doorway.png", "Art by AimlessArt")
                    Global.gui.message(
                        "You go over to the chapel, wondering if Her Demonic Highness would deign to " +
                                "see you. As you enter, the priest notices you and quickly shuffles away, apparently " +
                                "a little skittish around anyone who would want to visit a demon. You walk towards " +
                                "the back and descend the basement stairs. Strangely, Reyka isn't there, it's just " +
                                "the creepy decor greeting you. You look around for a moment, but decide not to pry, " +
                                "there are probably a few dozen things in here that could kill you. If not, Reyka probably " +
                                "will. As you turn to go back up, Reyka appears in the doorway. <i>\"Oh hello " + player.name +
                                ", how nice of you to come visit me! I was just out getting some... supplies.\"</i> She " +
                                "gives the room a once-over and looks at you darkly, seeming to bore straight into your " +
                                "soul with her menacing eyes: <i>\"You didn't touch anything, did you?\"</i> " +
                                "Quickly shaking your head, you emphatically declare your innocence. You're only here to spend some time " +
                                "with your favorite demoness. <i>\"Is that so, and what might you be planning then?\"</i>"
                    )
                    promptScenes()
                }
            } else if (reyka.getAttraction(player) < 10) {
                Global.gui.message(
                    "You were going to ask Aesop where to find Reyka, but while on your way " +
                            "there you noticed a dim pink haze protruding from a window leading into " +
                            "the campus chapel's basement. The irony of a demon living beneath a church " +
                            "is definitely Reyka's style, so you go over to investigate.<p>" +
                            "You go to the back of the chapel, looking for the priest. You find him " +
                            "setting a pot of tea and gently tap him on the shoulder. <i>\"Oh! You scared me, my son! " +
                            "I'm sorry but I'm a bit jumpy these days... Anyway, what can I do for you?\"</i> You " +
                            "inquire with as much subtlety as you can muster whether the church has any long term housing, perhaps in the basement. " +
                            "The priest's eyes go wide in shock as you say this. Trembling, he slowly " +
                            "raises his arm, finger outstretched, pointing towards a narrow door. Soon " +
                            "after, he briskly walks away, leaving you alone facing the basement door.<p>" +
                            "Taking care to be as quiet as possible, you walk down the stairs. There " +
                            "is more of that haze here, it smells mildly like strawberries. " +
                            "When you reach the bottom, you take a look around. There is a large pile " +
                            "of junk in one corner, probably the original contents of the basement. " +
                            "The rest of the space is mostly empty; except for some intricate drawings " +
                            "on the floor, there is only a bed on which Reyka lies. Lay would be a better " +
                            "word, though, as she sat up faster than you could blink and she looks rather " +
                            "displeased. <i>\"Would you like it if I barged into your room like that? No, wait, " +
                            "don't answer that, of course you would. Still, a lady deserves her privacy, yes?\"</i> " +
                            "She bats a hand at you dismissively as if swatting away a fly and suddenly you are flying back " +
                            "up the stairs, through the open door which promptly slams shut. She probably " +
                            "doesn't want to see you..."
                )
                Roster.gainAttraction(ID.PLAYER, ID.REYKA, 2)
            } else {
                Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
                Global.gui.message(
                    "Deciding you'd rather have lunch instead, you head for the cafeteria. Halfway there, " +
                            "you are pushed to the wall by a surprisingly wing-less but still strikingly beautiful Reyka. " +
                            "\"So listen, I love playing around at night and all, but the days do tend to get a bit dull. " +
                            "Usually, I'd go and have a little fun with some random coeds, nothing serious of course. " +
                            "I just go around discreetly getting people all hot and bothered in a variety of ways and " +
                            "observe the results. It breaks a few relationships here and there and even creates a few " +
                            "new ones, I'm a regular Cupid. Then I thought, 'well, " + player.name + " is pretty nice, perhaps " +
                            "I could have some fun with him.', so here I am. Are you interested?\"<p>" +
                            "You are frozen for a moment, staring into those deep eyes of hers. 'Pretty nice'? " +
                            "What's that supposed to mean? As you ponder this, she apparently gets slightly annoyed by " +
                            "your inaction and presses herself into you even harder. She leans over and whispers in you ear: " +
                            "<i>\"We really could have some very... good... fun... you know....\"</i> Those last words are punctuated " +
                            "by her gently squeezing your crotch. You maintain enough composure to agree. She immediately jumps back, smiling. " +
                            "<i>\"Excellent. Now you never really had a choice of course, but as a reward for coming willingly, I'll let you choose what " +
                            "we are going to do! Who knows, I might even let you into my home.\"</i> " +
                            "You are not sure whether to be relieved or worried about this, but you have a choice to make."
                )
                Global.gui.message("<b>You gained affection with Reyka.</b>")
                Global.flag(Flag.ReykaDate)
                promptScenes()
            }
            Global.gui.choose(this, "Leave")
        } else if (choice == "Sex") {
            sprite.undress()
            sprite.blush = 2
            sprite.mood = Emotion.dominant
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "Feeling a tad squeamish, you are about to suggest going out to get something to eat. As you turn " +
                        "to Reyka to say this, however,  you see a quick flash of red in her eyes. Your mind fogs over for " +
                        "a moment. When you try to recall what you were saying, Reyka instead whispers in your ear: <i>\"How about " +
                        "we go and have a little private fun somewhere?\"</i> This sounds like an excellent idea to you, so you accept " +
                        "and follow her as she leads you over to that wonderfully cozy basement room of hers.<p>" +
                        "You reach the room without incident, save for a sad look the priest gave you on your way in. " +
                        "As soon as you reach the bottom of the stairs, Reyka pulls your arm, hard. The force of the pull " +
                        "flings you onto the bed, which stands at the center of the intricate pattern of symbols drawn on the " +
                        "floor. Reyka looks you over possessively, grinning impishly. She speaks some words your mind doesn't " +
                        "seem to be able to process and a bright  ball of swirling purple light appears in her hands. She spreads " +
                        "her arms wide and the ball seems to explode, blinding you.<p>"
                        + "After only a second your vision returns, " +
                        "revealing both of you to be naked. Reyka settles in between your legs and takes your surprisingly hard " +
                        "member into her mouth. The feeling is amazing; she doesn't waste any time and starts sucking hard while " +
                        "her tongue swirls around your head. Wherever she is not touching, her saliva feels tingly on your flesh.<p>"
            )
            Global.gui.displayImage("premium/Reyka Blowjob.jpg", "Art by AimlessArt")
            Global.gui.message(
                "It doesn't take long for you to get close to orgasm, but Reyka has other plans. With a long-nailed finger " +
                        "she draws an arcane symbol just above your cock and you immediately feel the oncoming waves draw back into " +
                        "your balls. She stops sucking and with a flick of her wrist, she sends you sprawling further onto the bed.<p>" +
                        "She immediately proceeds to mount you, taking you fully inside her fully lubricated entrance. It feels as if " +
                        "her insides are an entirely separate organism, squirming and squeezing and pulling and tightening and " +
                        "SUCKING, man the suction is powerful. If you were at all able, you would have cum already, probably twice, " +
                        "but whatever spell she cast on you is keeping a tight lid on things. The sensory overload is driving you " +
                        "mad and you start bucking back fervently, desperate for that small push over the edge.<p>"
                        + "This continues " +
                        "for minutes, every second feeling like an eternity. At long last, she decides you've had enough. She " +
                        "settles down, impaling herself fully, brushes a hand over the symbol she previously drew and with " +
                        "a final smile and squeeze, sends you blasting over the edge. It is beyond anything you have ever felt. " +
                        "It feels as if all your energy is draining out of you, replaced by pure ecstasy. When after what " +
                        "feels like hours the feeling dissipates, leaving only weariness in its wake.<p>" +
                        "Mustering all your strength, you look up to see Reyka already dressed. While brushing her hair, " +
                        "she asks: <i>\"You remember that moment right before I let you cum? That's when I'd usually ask you " +
                        "to give up your soul. As you'll understand, they never refuse at that point. But given all of our " +
                        "nighttime fun, it would have been a terrible shame to do that to you. Now go and get cleaned up; " +
                        "you won't be any fun if you're crawling across the Quad all night. Actually, now that I think about it, " +
                        "if I could get my hands on one of those strap-ons, you might be...\"</i><p>"
                        + "Taking this worrying prospect as " +
                        "your cue, you quickly stumble up the stairs. After taking a quick breather and cup of coffee in the " +
                        "chapel's kitchen, you walk back home, still drowsy. You probably shouldn't do that too often, " +
                        "lest she changes her mind about your value to her.<br>" +
                        "Then again, you realize you didn't have any choice from the start. "
            )
            Global.gui.choose(this, "Leave")
            Daytime.train(player, reyka, Attribute.Seduction)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice == "Clearheaded Sex") {
            sprite.undress()
            sprite.blush = 3
            sprite.mood = Emotion.confident
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "You hesitated for a bit too long, and again Reyka's eyes flash red. Again, your mind fogs over, " +
                        "ready to do whatever your mistress wants. Again, Reyka, hauls you off. You don't seem to " +
                        "be able to focus on anything; everything around you is distorted and blurry, except for Reyka. " +
                        "She is casually walking in front of you, not even bothering to look back and see if you " +
                        "are still with her. You wouldn't dream of leaving, of course: She is your one buoy in a sea " +
                        "of vagueness. Eventually, you arrive at her place, ready for another round of her surreptitiously " +
                        "feeding on you.<p>"
                        + "No. Not this time. You muster up all your willpower, all your strength, and " +
                        "you break free from the shroud around your mind. Instantly the fog clears, the demonic hidey-hole " +
                        "that is Reyka's room snapping into focus. Reyka knows it too. She spins around, a hint of surprise " +
                        "in her expression. Not wanting to give her a chance to put you under again, you tackle her onto the " +
                        "bed. You know you'll never make it to the door. Then again, you did want to have sex with her. You " +
                        "just wanted it without being mentally restrained and helpless the entire time. Seeing little other options " +
                        "in your current position, you lean in and kiss her. You've obviously kissed her plenty of times already " +
                        "during the fights, some of them even voluntarily, but this one is different. This is a kiss with " +
                        "emotion behind it and Reyka senses this too. She seems quite startled for a moment, but soon returns " +
                        "your kiss passionately. Her tongue forces its way into your mouth and twists and turns around your own.<p>" +
                        "The aphrodisiacs in her saliva are already starting to get to you, so you decide it's probably best " +
                        "to act quickly, while your mind is still your own. You hurry downward and push up the miniskirt she " +
                        "always seems to be wearing. Also as per usual, she has forgone any further means of covering up, not " +
                        "wanting anything to get in the way should she have a sudden need for a snack. Feeling quite in the mood " +
                        "for a snack yourself, you move in and start licking her gently. Reyka is clearly not used to this. Usually " +
                        "when she compels her 'partners' she makes them work hard; the slow pace and meticulous placement of each " +
                        "lick and caress means she feels every detail. She locks her thighs around your head and grabs your hair, " +
                        "trying to hump your face. You manage to keep your pace though, fully aware of the dangerous position " +
                        "you are in given how her juices have had somewhat disquieting effects on you in the past. Reyka by now " +
                        "is desperate for more stimulation, and has no problem screaming out profanities to that effect. It's quite " +
                        "possible any people upstairs may hear her; that'd be something to see. She's getting close; you can tell " +
                        "by her wings and tail popping into existence as she can no longer summon the will to hide them.<p>" +
                        "While her juices have not taken control of you, they've certainly had other effects as you are rock " +
                        "hard. You move up her body and deeply kiss her once more. Simultaneously, you position your dick at " +
                        "her entrance and thrust all the way in in one go. The sudden invasion sends her over the edge. Her " +
                        "inner muscles are always hard at work when they have a guest over, but right now it's simply insane. " +
                        "They are twisting and pulling more dexterously than you have ever felt anyone with their hands, and " +
                        "you've felt Mara at work. The sensations would have been enough to set you off instantly, but you " +
                        "are in some kind of trance-like state where you feel all the incredible pleasure, yet feel no need " +
                        "to cum.<p>"
                        + "You continue pistoning into her furiously for several minutes, Reyka seemingly in a state of " +
                        "perpetual orgasm. It's a good thing she doesn't actually need to breathe or she would have passed " +
                        "out by now. As she continues to moan and squirm, the more demonic features of her anatomy once again " +
                        "disappear. You close your eyes and just focus on keeping the thrusting going.<p>"
                        + "Not a minute later, she " +
                        "grabs your face and pulls it off of her own. When you open your eyes, you see a pair of deep blue " +
                        "eyes staring back at you. Her skin becomes less pale and her hair shifts from black to blonde. " +
                        "As you stare at her in amazement, ceasing your thrusts, she pulls you back in and gives you a " +
                        "loving kiss. After all the internal milking you've endured it seems strange to you that a kiss " +
                        "should send you over the edge, but send you over it does.<p>"
                        + "You thrust in one final time and pour " +
                        "a deluge of cum into her. Strangely, though, you don't feel any of the tiredness you've come to expect " +
                        "from Reyka; it just feels like an incredible orgasm. When you finally finish you roll off to the side, " +
                        "catching your breath. Reyka rolls onto her side, putting her arms around your neck and a leg across your " +
                        "waist.<p>" +
                        "After a few minutes you ask her what the transformation thing is all about. <i>\"Normally when succubi have " +
                        "sex, it feels good, but in the same way you feel good having a really nice meal. What we just did " +
                        "had true emotion in it, from both of us. Demons have souls, did you know that? They're just a bit... " +
                        "emptier than mortals'. That kind of... making love, it fills them up for a while, turning us into what " +
                        "we would look like if we were mortal. It feels different too, needing to breathe and the constant " +
                        "thumping of a heart in your chest seemed like chores to me before, but now... I don't know.\"</i><p>"
                        + "She's quiet " +
                        "after that, so you are too. You just hold her as her hair is beginning to darken at the roots and " +
                        "her irises are taking on a pinkish hue. Eventually, you get up and get dressed. Reyka, her old self " +
                        "again, does the same and you share a final kiss before you leave."
            )
            if (!player.has(Trait.desensitized)) {
                Global.gui.message(
                    "<p><b>Having been in the maelstrom that is Reyka's pussy while she was orgasming, you are sure " +
                            "nothing else will ever come close to those feelings. Your sexual endurance has permanently increased.</b>"
                )
                player.add(Trait.desensitized)
                reyka.add(Trait.desensitized)
            }
            Global.gui.choose(this, "Leave")
            Daytime.train(player, reyka, Attribute.Seduction)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice == "Sparring") {
            sprite.mood = Emotion.dominant
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "<i>\"So basically, you want to practice sexfighting, just more the fighting than " +
                        "the sex, yes?\"</i> Reyka asks you. <i>\"How dull, but I'm sure " +
                        "we can make it fun!\"</i> Soon you are in a relatively small practice room in the far " +
                        "corner of the gym with nothing but a mat in it. Reyka spins around to face you, her " +
                        "wings suddenly materializing behind her. <i>\"Here's how this is going to work: You are going " +
                        "to try to work me to the ground, you'll fail, and then you'll be my pet until nightfall. " +
                        "How does that sound?\"</i><p>"
                        + "She's quite cocky given how well you've kept up with her at night. " +
                        "<i>\"That's because there are some additional rules during the Games for me, " +
                        "courtesy of some creepy arrogant guy, which make things slightly trickier. " +
                        "Here, I can do what I want, and I want you.\"</i> You're not convinced. After all, demons are known for " +
                        "their incessant lying. But you will need some terms of your own: If you do bring her down, she has to " +
                        "follow your commands instead. Reyka seems to have to struggle not to burst out laughing at the " +
                        "suggestion, but agrees to your proposal.<p>" +
                        "After a quick warming-up, you stand at one edge of the mat, looking straight at Reyka on the opposite side. " +
                        "One brief countdown later, you start cautiously approaching Reyka. She has taken up a defensive " +
                        "posture, but her wry smile tells you she is not worried in the least. Eager to take advantage of " +
                        "her overconfidence, you suddenly swing your arm at her, hoping to disorient her. " +
                        "She certainly has good reflexes, though, as she starts moving out of the way the same instant " +
                        "you start your swing. The miss has left you horribly imbalanced, but she doesn't press her " +
                        "advantage; instead she just keeps smiling.<p>"
                        + "The next ten minutes are filled with repetitions of this " +
                        "first attack: you attempt to make a move and she dodges impossibly quickly. Finally, she decides " +
                        "she's had enough fun for now and latches onto your outstretched arm, efficiently throwing you down. " +
                        "The back of your head slams down on the mat hard, and the world begins to fade. Just before you pass out, " +
                        "you see Reyka standing over you with a hungry look in her eyes and a menacing smile.<p>" +
                        "You wake up in your own bed. Your alarm clock tells you it has been a surprisingly short time since " +
                        "you got beaten by Reyka. Given her nature, though, a short time is probably all she needs to do some damage. " +
                        "As you groggily get up, you become aware of the fact your entire body feels sore, particularly " +
                        "your groin. You decide a quick, hot shower is in order before you get into any more fights.<p>" +
                        "While giving yourself a quick rinse, you notice marks all over your body. Upon closer inspection, " +
                        "they're bite marks. You don't remember anything since falling to the ground in the gym, but between " +
                        "the marks and the distinct soreness in your ass, it's probably better that way."
            )
            Global.gui.choose(this, "Leave")
            Daytime.train(player, reyka, Attribute.Power)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice == "Sex Sparring") {
            sprite.blush = 2
            sprite.mood = Emotion.dominant
            Global.gui.loadPortrait(player, sprite)
            if (!player.has(Trait.clairvoyance)) {
                Global.gui.message(
                    "<i>\"Are you that eager to get your ass... kicked, shall we say... again? I must " +
                            "say I admire your courage. Tell you what, if you do a little something for me, " +
                            "I'll teach you a little trick, give you a fighting chance. Come with me.\"</i> " +
                            "You are certainly intrigued and the prospect of learning something is more appealing " +
                            "than waking up sore again in places you really shouldn't be sore, so you follow her.<p>" +
                            "You end up at the library, of all places, and Reyka leads you to the back. In front of " +
                            "an old limestone archway leading to a secluded section, she stops. <i>\"See this?\"</i> she asks, pointing " +
                            "towards a small sigil carved into the stone. <i>\"I can't pass by that, or touch it. So you " +
                            "just have to scratch it out with this.\"</i> She holds out a knife and you hesitantly take it. " +
                            "You've never seen the sigil before, it just looks like a random scribble, but you figure " +
                            "demonic 'tricks' are worth removing a scribble. You press the knife into the limestone " +
                            "and scratch over it. Soon enough, Reyka pats you on the shoulder and walks past you into " +
                            "the aisle of books. She quickly returns, dusty tome in hand, and gestures you to follow.<p>" +
                            "This time, she led you to her basement room beneath the chapel. Only slightly feeling like " +
                            "a sacrificial lamb, you watch as she puts the book away and turns to face you.<p>"
                            + "<i>\"You are probably " +
                            "curious as to what I'll teach you, but first there is a little something to take care of.\"</i> After " +
                            "she finishes saying this, the room lights up in purple fire. It doesn't burn you, but it sure " +
                            "does something. You feel tingly all over, and not the pleasant kind you've come to expect from Reyka. " +
                            "A few seconds later, the flames die down. You don't feel any different, but Reyka is looking at " +
                            "you in a way that makes you pretty sure it worked, whatever it was. <i>\"Now, just close your eyes, " +
                            "concentrate for a moment, and tell me if you feel any different.\"</i>  You do as she says, but at " +
                            "first there is nothing.<p>"
                            + "Just as you are about to tell her so, you notice some soft music, " +
                            "seemingly far away. She must have seen you cringe, as she says: <i>\"Good, focus on that. Hear " +
                            "it right in your soul and watch for any changes.\"</i> You listen carefully, and suddenly the " +
                            "music spikes, followed quickly by a dainty fist to your face. <i>\"Now you know what to listen " +
                            "for. Let me explain: I have given you the ability to tap into the minds of others. It's not " +
                            "strong enough to hear their thoughts or anything like that, but you'll notice when they are " +
                            "planning something. With practice, you might be able to tell just what they're going to do and " +
                            "then do something about it. Useful right? Now, about those rules; Creepy Voice told me they were " +
                            "because none of the other competitors could do what I do, keep things at least somewhat fair, you know? " +
                            "With you having this ability now, I can use it in the fights too, so we both win! But for now, you " +
                            "just need practice, so let's start!\"</i>.<p>" +
                            "You spend another half hour in the basement, trying to distinguish between the mild " +
                            "musical hints preceding all kinds of slaps and punches to your face, intermixed with caresses " +
                            "and licks everywhere else. You still need more practice, but <b>you have learned to tune in to " +
                            "your opponents' minds, making it easier to evade and counter their attacks</b>."
                )
                player.add(Trait.clairvoyance)
                reyka.add(Trait.clairvoyance)
            } else {
                Global.gui.message(
                    "<i>\"Ah, good. By now, you may have figured out that there really isn't that much room to grow, " +
                            "I can only do so much for a mortal such as yourself. Still, it's always fun getting a " +
                            "little practice in, so let's go to the gym.\"</i><p>" +
                            "In the gym, you go to the same room you've already gotten your ass kicked a couple times. " +
                            "This time, though, you feel a lot more confident. You get into your starting position, " +
                            "and concentrate on the faint music. It's so soft you don't even notice it unless you " +
                            "really try, doing it while fighting is a hefty challenge. Soon, the match is underway.<p>" +
                            "Unlike previously, Reyka doesn't stand idly, waiting for you to strike, but gets some " +
                            "attacks of her own in as well. To the clueless observer it would look pretty boring; " +
                            "neither of you is even touching the other. You are very evenly matched, it seems, you " +
                            "feel you might actually have a chance at beating her!<p>"
                            + "Those hopes are soon crushed, " +
                            "maybe or maybe not along with one of your ribs, as Reyka delivers a lightning-quick jab " +
                            "to your chest. The wind knocked out of you, you fall to your knees gasping for air. " +
                            "When you look up, you see Reyka with an almost cute smile, holding out her hand.<p>" +
                            "<i>\"I just wanted to make sure you remember who's boss here, but you did well.\"</i> You latch " +
                            "on to her hand and she pulls you up, dusting of your shoulders. <i>\"I'll give you a few minutes " +
                            "to catch your breath and then we'll go about defending against some... different moves.\"</i><p>" +
                            "When you have recovered from Reyka's disproportionately sharp punch, you retake your " +
                            "position opposite her and wait for her to explain the rules of this second round. " +
                            "<i>\"So here's the deal, I am not going to go all out on you, that wouldn't be any sport. " +
                            "Instead, I'll be performing at the level I think you can reach. I will attempt to touch " +
                            "you, and you will evade. Every time I succeed, you take off a piece of clothing and when " +
                            "you're naked, I'll try to get you to cum. When you do, I'll get to have a little fun with you. " +
                            "If you haven't cum ten minutes after the match starts, you can do anything you want to me. Deal?\"</i><p>" +
                            "You think it would be terribly unwise to argue with her now that she's gotten a bit into it, so " +
                            "you immediately accept.<p>"
                            + "The first two minutes of the match are uneventful as you manage to evade " +
                            "every lunge she makes. Once, you even managed to maneuver around her to a position in which, in " +
                            "a regular match, you could easily have taken her down. This is rewarded by an appreciative, if " +
                            "somewhat surprised glance over her shoulder. In response to this, she picks up the pace and you're " +
                            "losing clothes left and right.<p>"
                            + "At the six-minute mark, she starts sprinting circles around you " +
                            "inhumanly fast before grabbing you from behind. She holds you in place with one arm across your chest, " +
                            "while she uses her other hand to fondle your already rising dick. <i>\"Yum, playtime.\"</i><p>"
                            + "With that, she pulls " +
                            "down your boxers and lets you go. <i>\"Next time, boy, you're mine!\"</i> As you start the final round, three " +
                            "minutes remain on the clock. You aren't trying anymore fancy moves, instead opting to stay as far away " +
                            "as possible. She gets close a couple times though, slapping your ass a few times. Your escapes are " +
                            "hindered somewhat by the protrusion in your groin, but you make it to eight and a half minutes, " +
                            "nevertheless.<p>"
                            + "Aware that it's almost time, Reyka again intensifies her assault, working you " +
                            "to the ground in no time and mounting you in a 69. <i>\"Think you can last a minute? I don't.\"</i> " +
                            "As she starts giving you an expert blowjob, you slip your hands under her skirt and massage " +
                            "her ass. It won't make her cum, you're sure of that, but it might distract her enough to buy you enough " +
                            "time.<p>"
                            + "It didn't, however, as with only eight seconds left before your salvation, you are forced " +
                            "to give in to her perfect technique. After properly draining you of all you had to offer, she looks " +
                            "back at you, licking a last drop from her upper lip. <i>\"You did very well. I couldn't just let you win, " +
                            "of course. That would be a waste of a nice meal. As a reward, though, I'll let you off easy today.\"</i><p>" +
                            "After recuperating from from the intense orgasm, you get up, get dressed, and leave the gym. Reyka has " +
                            "already left, probably off to work out her pent up frustrations on some poor unsuspecting frat guy, " +
                            "you think."
                )
            }
            Global.gui.choose(this, "Leave")
            Daytime.train(player, reyka, Attribute.Power)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice == "Games") {
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "Seeing Reyka confused is certainly a change, she is usually the very image of confidence. " +
                        "<i>\"A game? You want to play a game? An actual, no-sex game?\"</i> You nod affirmatively, " +
                        "wary of what she might do next. <i>\"Well... I don't know any games. At least, none that do " +
                        "not involve any of the tortured souls Father used to keep around the house. What do you " +
                        "propose we do?\"</i><p>"
                        + "After a moment's careful deliberation, you decide to keep things close to " +
                        "her nature at first. You ask her what those games she used to play, hoping you might " +
                        "adapt one into a non-violent, non-sexual, non-tortury variant.<p>"
                        + "<i>\"Oh you know, the usual. " +
                        "Scaring them, planting all kinds of traps to decapitate, dismember or otherwise mutilate them. " +
                        "They always come back anyway, the already dead can't die again. You see, my brother is a " +
                        "demon of rage, so he's into that kind of stuff. And my powers only came with physical maturity, " +
                        "so I tagged along with him. My favorite was always making them think they had a chance at escape; they " +
                        "never seem to figure out there isn't any. Later, when I reached adulthood I much preferred going out " +
                        "on my own, usually up here somewhere, getting some... experience in. Along with other things.\"</i><p>" +
                        "You aren't sure what bothers you most, what she had said, or the casual tone in which she had " +
                        "said it. Still, you think you can play in on those predatory instincts of hers. So, demonic " +
                        "hide and seek it is.<p>"
                        + "You explain the rules to her. <i>\"That won't cut it. I have to exert myself " +
                        "finding you and not get to have a little snack? Oh, no. Not happening.\"</i> Ah, well, baby steps.<p>" +
                        "So it's going to be a good old stalk: You start at your dorm, Reyka in the student union. At " +
                        "a predetermined time, you will try to get to the engineering department and she will try to catch " +
                        "you. If she does, you'll have to go with her to her place and get the life sucked out of you. " +
                        "Well, some of it at least... You hope this will open the way to some more innocuous games.<p>" +
                        "You quickly get to your starting position and when the moment comes, you start running. " +
                        "You figure you'll be easy to catch in the Quad, so you decide to go through the tunnel and " +
                        "the cafeteria. There, you can take the back exit - off-limits during the night - and make your way " +
                        "around to the engineering department.<p>"
                        + "You reach the cafeteria without incident, save for people giving " +
                        "you some strange looks, and locate the door.<p>"
                        + "After looking around to make sure no one is watching, you " +
                        "open the door, check the alley and, finding it clear, step outside. As you walk towards the access road " +
                        "which runs behind both the cafeteria and the engineering building for deliveries, you hear a faint " +
                        "rustling above you.<p>"
                        + "You turn around just in time to see Reyka, wings outstretched, swooping down towards " +
                        "you. When she collides with you, you fall flat on your back with her on top. <i>\"Found you! Normally I'd " +
                        "take you right here, but a deal is a deal.\"</i> She gives you a pat on your head and lets you up.<p>"
                        + "When you " +
                        "look at her again, her wings are gone. You ask why she was allowed to use them here in the first place, " +
                        "since you thought she was not allowed to display any demonic traits in public. <i>\"I'm not, but no one " +
                        "ever comes back here. Except for deliveries and shady deals, neither of which take place at this hour. " +
                        "Now come along!\"</i><p>" +
                        "When you return to your room, you feel like you got run over by a train. Reyka exercises some restraint " +
                        "with her feeding if she beats you at night, since she knows that it'll make it more fun should you run into " +
                        "her again. She didn't do so just now. Still, after a quick meal and a hot shower, you are ready to go back " +
                        "out."
            )
            Global.gui.choose(this, "Leave")
            Daytime.train(player, reyka, Attribute.Cunning)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice == "Locator") {
            Global.gui.loadPortrait(player, sprite)
            if (!player.has(Trait.locator)) {
                Global.gui.message(
                    "<i>\"Another game? Great! I liked the way the last one turned out!\"</i> She winks as she says this, the " +
                            "dizziness already returning. You quickly suggest to play something else, saying you need all your " +
                            "faculties in good condition for some studying later.<p>"
                            + "<i>\"Hey, you came to me! But alright, I guess we can " +
                            "swap roles this time. I'll even level the playing field for you.\"</i> Unbidden images of wings sprouting " +
                            "from your back come to mind, and you waste no time making clear to her you don't want them.<p>"
                            + "<i>\"That's, " +
                            "not what I meant, you silly boy. That would require at least five souls and I'm not allowed to harvest " +
                            "any here. No, I meant the hunting instincts, you know, so you can track my movements?\"</i> Come again? " +
                            "That kind of defeats the point of the game, but it sure sounds interesting... You ask her to elaborate. " +
                            "<i>\"Well, it's quite simple really: I weave a little spell, and you gain a seventh sense!\"</i><p>"
                            + "You are too " +
                            "stunned by the prospect of a demon rummaging in your mind to notice the word 'seventh' where you " +
                            "would have expected 'sixth'. You probably wouldn't want to know, anyway.<p>"
                            + "<i>\"It works like this: everyone " +
                            "leaves a trace of their essence on everything they touch. You'll be able to trace that back to them. " +
                            "It wouldn't be perfect, of course, and the item in question would be destroyed, but it'll give you an " +
                            "idea of where to look.\"</i><p>"
                            + "You still aren't too thrilled about all this, but the possibilities are tempting. " +
                            "You agree to let her work her magic. Smiling, she holds her hand against your head, and pulls it away again " +
                            "after a few seconds. <i>\"What? Were you expecting fireworks? Let's go and give it a try.\"</i>"
                )
                player.add(Trait.locator)
            }
            Global.gui.message(
                "Reyka pulls you out towards the dorms. A lot of people, men and women alike, look your way, " +
                        "jealous at the guy who is getting tugged towards the dorms by a stunningly beautiful woman " +
                        "with a devilish grin on her face. You are not so sure if you are all that lucky, but hey, it " +
                        "could be worse. You just hope she won't drag you to a family dinner some time, that brother of " +
                        "hers doesn't sound like the type of guy you would get along with. That, and the cuisine would " +
                        "probably not be to your liking.<p>" +
                        "Arriving at the dorm, Reyka says the rules will be the same as last time, only she won't fly " +
                        "this time. She hands you a ring, presumably hers although you haven't seen it before. <i>\"Make sure " +
                        "to be somewhere private when you try and trace me, would you? We wouldn't want to draw any attention.\"</i> " +
                        "She briskly walks off to her own starting point. You'll have to intercept her before " +
                        "she reaches the showers. You thought it would be too easy that way, given how you are close to them. " +
                        "Then again, it's very busy here during the daytime, and it would be easy to get lost in the crowds. It's " +
                        "probably best if you try to catch her before she reaches the dorms.<p>"
                        + "First, however, you need to find a " +
                        "secluded spot so you can 'see' where she'll be coming from. Walking into the bathroom, you duck into a stall " +
                        "and pull out the ring Reyka gave you. You hold it in your clenched fist and concentrate on an mental image " +
                        "of her face. It's not hard to create that image, you committed every detail of it to memory the first time " +
                        "you laid eyes on her. Suddenly, an image of the library flashes in you mind. It's only for a second, but " +
                        "at least you now where she's starting. When your own vision returns, your fist is ablaze in purple flames.<p>" +
                        "Shocked, you let go of the ring, but all that's left is a ball of smoke which makes its way right up " +
                        "your nostrils. It smells... different. You are somewhat worried about what you just breathed in, but " +
                        "at least the flames are gone. You remember you should be trying to get to Reyka, she'll be on her way " +
                        "over by now. You walk out towards the door, but as soon as you see it Reyka is already walking through. " +
                        "How did she get here so quickly, especially without the use of her wings...<p>" +
                        "Seeing you, Reyka quickly ducks into a hallway to the side and you run after her. At this rate, she'll beat " +
                        "you to the showers, so you try to find a different route and cut her off. While running, you notice " +
                        "a stirring in your groin. Images of previous encounters with Reyka come to mind, but you push them aside " +
                        "and keep going. As you make your final turn, you can see the door to the women's showers right in front " +
                        "of you.<p>"
                        + "You hope you've gotten there in time, but when you are only a few steps away, Reyka crashes into " +
                        "you from the side. You get back up and stumble towards the door, reaching it seconds before she does, " +
                        "blocking her way. As she turns to look at you, you now have a full-on erection tenting your pants. " +
                        "Reyka sees this and licks her lips. <i>\"I probably should have told you, but that smoke is quite a potent " +
                        "aphrodisiac. Well, anyway, you won and now you can do anything to me you like. No one is taking a " +
                        "shower right now anyway.\"</i><p>"
                        + "You should have known she'd set you up. From the 'accidental' omission of " +
                        "some rather important information to the choice of destination; she planned for this all along. " +
                        "You want her, you want her badly, but you are also quite pissed at her. So as punishment, you'll not " +
                        "give her what she wants. You shake off the temptation and promise yourself a good jerk off session later. " +
                        "For now, you intent to use your prize to get some answers. The first of those being just how she got " +
                        "here that fast. Even at a dead run, it should have taken her three times longer to get here from the " +
                        "library.<p>"
                        + "<i>\"Who ever said that ring belonged to me? I 'borrowed' it from a girl who was visiting the chapel. " +
                        "The rules did not stipulate that I had to give you something of mine, you just assumed I did.\"</i> This answer " +
                        "does not do much good for your temper, and you angrily ask her if there's anything else you should know " +
                        "about this thing she made you able to do. <i>\"No, the smoke is it, really... Oh! You can't spy at anyone on " +
                        "consecrated ground; it would just give you a headache if you tried. And don't try to use it on yourself: " +
                        "it's pointless and the feedback loop is not something you want to experience. Apart from that, you know " +
                        "all there is to know, now are we going to do something about that problem of yours?\"</i><p>"
                        + "She runs her hand over " +
                        "your crotch for those last few words, but you quickly back away muttering something about studying and " +
                        "run straight to your room. For once, you arrived back from a visit to Reyka fully intact, just the raging " +
                        "boner to worry about. Ah, well. You did voluntarily visit a succubus, what did you expect?"
            )
            Global.gui.choose(this, "Leave")
            Daytime.train(player, reyka, Attribute.Cunning)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 1)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice == "Frustration") {
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "As you approach the chapel you get a sense that something is off, nothing you can put your finger on, but a distinct sense of unease.  You notice other people hurrying past the chapel and avoiding it with their eyes, as if on some level they too sense that something is wrong.  Unlike everyone else, you have a strong suspicion that the demonic presence who makes her home in the basement is the likely  cause of this unease.  Steeling your nerves, you head down to the unused basement of the chapel to check on Rekya.<br>" +
                        "<br>" +
                        "Entering her domain, it becomes immediately clear that her infernal origins and powers are the cause of the ‘wrongness' that can be felt, even outside the chapel walls.  The air is practically humming with demonic power.  It carries an exotic blend of scents, including the feminine musk you've come to associate with Rekya's lust. There is a spicy cinnamon scent, an exotic and biting anise smell, as well as other smells that you can't fully identify.  The air in the room feels heavy, oppressive, angry.  <br>" +
                        "<br>" +
                        "The room is lit with a small army of black wax candles; covering the shelves, tables, and portions of the floor, and illuminating the points of the pentagram drawn in chalk, in the middle of the room.  In the center of the drawing, sits Rekya, pointed fangs bared in a snarl, leathery bat wings extended behind her, fluttering erratically and annoyedly, and her tail swishing back and forth rapidly.<br>" +
                        "<br>" +
                        "The scene would be almost horrific, giving pure light to her demonic nature, were it not for one thing.  Sitting naked in the center of the pentagram, cross legged, she is lit not just by the candles around her but also the warm glow of an open laptop sitting on her thighs.  She looks up at you and her red eyes flash angrily.  <i>\"What do you want?  Now's not a good time.\"</i><br>" +
                        "<br>" +
                        "Still, it isn't anger you hear in her voice, but frustration; and so you can't help but ask her if everything is ok, and if you can help somehow.  For a moment, she looks like she's about to attack you, but she takes a deep breath, causing her glorious breasts to rise and fall in a most pleasing and arousing way.  <i>\"I'm annoyed,\"</i> she says stating the obvious.  <i>\"I'm not so egotistical as to think that I can't be beaten.  But this year, you, the other girls, it's something different.  The competition is far fiercer than it was when I was summoned.  For a succubus to be beaten in a contest of sexual stamina is,\"</i> she pauses and snarls at the laptop screen, as something there draws her attention.  She slams it shut and hurls it away from her.  It skitters across the floor and hits the far wall.  One small part of you can't help but wonder if she just broke it, but the succubus continues as if she hadn't paused, <i>\"well, it's annoying at the least.  Annoying for certain. Insulting as well. And in the worst case scenario, it suggests that I'm losing my connection to the Infernal realm.\"</i><br>" +
                        "<br>" +
                        "She stands up, and as usual you're taken by the unearthly beauty of her demonic form.  As a succubus her body is perfect and flawless, curved, strong, and tall.  Were it not for the glowing red eyes, the pointed ears, the forward-sweeping horns, the leathery bat wings, and (of course) the spade tipped tail that swished behind her, she might look just like a typical human supermodel.  Instantly you feel lust towards her. And how could you not, especially naked as she is?  She's a demonic embodiment of lust.  <br>" +
                        "<br>" +
                        "Rekya points to the laptop laying against the far wall and spits out in an accusing tone, <i>\"But all the research I did says that I'm not.\"</i>  She paused for a moment and glared at the laptop, then continued, <i>\"You know, it's not the easiest thing to conduct a ritual that opens a portal to allow communication with the infernal realms, but it is a little easier when you involve the magical technology of this world.  It's not something any demon could do, but I like computers.  Well, usually I do at least.<br>" +
                        "<br>" +
                        "She sighed a deep breath and her jawline clenched in restrained anger before turning her gaze back to you.  <i>\"A ritual blending of infernal magic and mortal internet technology, all so I could talk to,\"</i>  She looks over at you, and pauses to consider her next words carefully, <i>\"well, lets just say I talked to a mentor of sorts, back home.\"</i>  You have to remind yourself that ‘back home' isn't the next city over, or even across country, but another plane of existence, the infernal realm.  <i>\"This ‘mentor' assured me that my recent defeats aren't any sign that I'm growing weaker.  In fact, she even suggested that defeat might be good for me.  Hah!  Can you imagine?  She said that it would be good for my ego to taste defeat now and then.  Me!\"</i>  She turned and snarled at the laptop, obviously annoyed.<br>" +
                        "<br>" +
                        "You understand that demon or not, right now, Rekya is in an emotionally sensitive place and you need to play this out the right way.  This isn't the night games, so there's no need to try to ‘win' right now, but you want to help put her mind at ease.  You approach a few steps, closing half the distance between you, till you stand just outside the chalk pentagram on the floor.  You tell her that she is the most fierce competitor in the games, and the one you're most intimidated by.  Even if it's not strictly the truth, it's what she needs to hear right now.<br>" +
                        "<br>" +
                        "She looks at you, confused for a moment, then adopts an air of superiority.  She stands tall, her feet shoulder width apart, and plants her fists on her hips.  A pose that you can't help noticing puts her sexy pussy on display.  She chuckles as your eyes dip down to her sex, <i>\"Well of course I am, and of course you're the most intimidated by me.  I'm a succubus.  I could suck your soul out through your dick right now, if I wanted.\"</i><br>" +
                        "<br>" +
                        "Playing into her recovering sense of self, you throw yourself down at her feet, begging and pleading her to have mercy on your poor mortal soul.  She laughs and for a moment you look up at her.  In that moment that your eyes make contact, something unsaid passes between the two of you.  She recognizes that you're hamming it up, that you're nowhere near as scared of her as you're pretending to be.  But also, she recognizes that you're doing this for her, you're trying to make her feel better.  Her face softens just a fraction, and gone is that sense of annoyance that was carved upon her when you'd entered.  She knows what you're doing, and with the barest of nods of her head, you know that she accepts it, and she's willing to play along.<br>" +
                        "<br>" +
                        "She folds her arms across her chest, under her breasts, pushing them upwards.  She tilts her nose up as she looks down at you.  <i>\"Fine, I suppose I could leave you alive.  I'm not in the mood to consume your entire soul today anyways.\"</i>  The stress she puts on the word entire makes it clear she does plan to have a little taste, at least.  <i>\"You're lucky you amuse me mortal. I won't destroy you, but you'd better be willing to do as you're told.\"</i><br>" +
                        "<br>" +
                        "The oppressive feeling in the air, that insubstantial haze of annoyance, began to fade.  You nod your head, agreeing to do as she asks. Stretching forward, you make a show of timidly reaching to kiss her foot. <br>" +
                        "<br>" +
                        "Rekya makes a sound of disgust, <i>\"Tch, I didn't tell you to kiss my foot.  Obviously, you need better direction if you want to please a succubus.\"</i>   She utters some dark words and from the ground around you springs wavering black tentacles of shadow.  Keeping up the act, you put on a horrified face as they wrapped around you, quickly stripping off your clothing and spinning you around, suspending you on your back, a few inches off the ground, and bound by their inky grip.  You make a pretense of struggling against them without actually trying to slip free, and she smiles as you squirm.<br>" +
                        "<br>" +
                        "<i>\"First, you're going to going to put that tongue to work where it should be.\"</i> she said turning around and facing away from you.  Above you, that beautifully curved ass descends as she lowers herself down towards your face.  Planting her aroused cunt right above your mouth, she commands, <i>\"Now lick until I tell you otherwise.\"</i><br>" +
                        "<br>" +
                        "A task you were glad to oblige her with, even if you weren't playing the role of a submissive.  It only takes a few moments of your oral attention on her sex before her musky, spicy scent and taste fill your mouth and nose, making your dick rock hard.  You cannot touch, of course, bound by the shadowy tentacles that hold you inches from the ground.  You feel a warm touch running down the shaft of your cock, and know it's her spade tipped tail teasing you as you eat her out. <br>" +
                        "<br>" +
                        "Rekya grows wetter and wetter, her lustful juices coming more and more copiously to coat your lips as you devour her infernal sex.  You can sense her getting close, but then suddenly she stands up.  You find your neck stretching, your mouth trying to stay in contact with her pussy for every last moment it could. She giggles, an almost girlish laugh.  <i>\"Awwww, you really like the way I taste that much, do you?<br>" +
                        "<br>" +
                        "You nod your head emphatically, not having to pretend one iota for that response.  <i>\"Well, I'm done with your mouth and I'm ready for the main course.\"</i>  She saunters around your suspended body, and you repeat your performance, pretending to struggle against the infernal bindings, causing her to chuckle again.<br>" +
                        "<br>" +
                        "She stops and looks down at you, <i>\"You know succubus don't eat right?\"</i>  You don't answer as you're not sure where she's going with this.  She turns around and displays her curvy round backside to you.  Gripping a cheek, she pulls it aside revealing the tight pucker of her asshole to you, <i>\"This isn't for digestion, like you pathetic mortals use. It's purely for pleasure.\"</i><br>" +
                        "<br>" +
                        "Throwing a leg over your hips, she locks her red eyed glowing gaze with yours and practically purrs, <i>\"Let me show you.\"</i>  With that she reaches down and grips your hard dick as she lowers her hips.  The tentacles beneath you push the back of your head up, so you're looking down the length of your body.  You can see her dripping cunt as she descends lower and lower, the head of your cock pressing between her perfect ass cheeks and nudging against that tight knot that sits beneath her wet slit.  A heartbeat of hesitation, two, then she presses down.<br>" +
                        "<br>" +
                        "The tightness is intense, but you feel your dick pressing into her ass, and the sensation is unlike anal sex with any mortal woman, that's for certain.  It's moist as if she'd pre-lubed, tight, and oh-so-hot.  She squeezes you as she lowers, until she's got the entirety of your dick engulfed in her ass.  <i>\"Ahhh that's nice.  Now listen mortal, you don't cum until I say so, do you understand?\"</i>  Her long sinuous tail snakes down behind her and wraps around the base of your scrotum, squeezing it gently, but a subtle reminder that it could squeeze harder if she wanted.  You nod your head, determined not to fail her.<br>" +
                        "<br>" +
                        "The next half hour passes in an absolute blur.  Rekya rides you, her ass bouncing on your dick as she orgasms over and over.  Her fingers constantly playing with her clit, dipping into her sex, plunging in and out, then coming back out to rub her clit more.  Her other hand alternating between raking fingernails down your body and squeezing and playing with her own tits.  More than once you feel your own orgasm approaching, and you think you can't hold back any longer.  But almost as if she senses it, she stops you each time.  Her tail wrapped around your balls tugs, sharply but not too hard, and causes just enough discomfort to give you enough self control to hold out.<br>" +
                        "<br>" +
                        "Finally after her sixth orgasm she nods, gasping, sweat rolling down her body she commands, <i>\"Ok, fill me up mortal, give me everything you've got.\"</i>  Her tail unfurls from around your balls, and she begins riding you even faster than she has been previously.  Her hands come down to your chest, and she braces against you as she pumps her hips up and down, milking you with her asshole.  Against such an onslaught, and with so long wanting to cum, it's no surprise that it only takes a few moments before you're groaning and releasing.  You feel your hot spunk flooding into her bowels as she moans in delight.  There's a moment of dizzy weakness that comes over you, letting you know she's feeding on your sexual energy, then it's gone.  The inky black shadow tentacles lower you to the ground as she stands up, letting your cock slide free from her backside.  They vanish and leave you panting on the ground in the middle of the chalk pentagram as she stands astride your hips, looking down at you.<br>" +
                        "<br>" +
                        "The look on her face is one of pleasure and satisfaction, <i>\"Okay, I feel a lot better now.  You can go mortal.\"</i>  She points to the door, <i>\"Begone from my sight, you've done your job adequately, I suppose.\"</i><br>" +
                        "<br>" +
                        "As soon as your post-orgasmic and post-succubus-feeding muscles allow, you crawl up to a kneeling position, gather your clothing and then stand to leave.  But as your hand reaches the doorway, you hear her call out.  Turning, you look at her and she's over by her laptop, checking it over.  <br>" +
                        "<br>" +
                        "<i>\"Thanks, by the way, for everything.\"</i>  You know that she's talking about the light roleplaying as well as the sex.  Then she adds,  <i>\"Oh, and, you know, feel free to pop in again anytime.  You make me feel a lot better about losing to a mere mortal.\"</i>  Her smile is genuine and it could be your imagination, but her eyes barely seem red in that moment, almost human.  But then her smile turns into a grin, and you decide that you'd better get out of here before she wants a round two.<br>" +
                        "<br>" +
                        "Dressing before you leave the basement, you can't help but notice the oppressive attitude that filled the air is gone, and you're glad that you've done the campus a good deed for the day.  Certainly, it's a public service you wouldn't mind repeating again."
            )
            Global.gui.choose(this, "Leave")
            Global.flag(Flag.ReykaFrustration)
            Roster.gainAffection(ID.PLAYER, ID.REYKA, 2)
            Global.gui.message("<b>You gained affection with Reyka.</b>")
        } else if (choice.startsWith("Practice Fight")) {
            val fight = Combat(player, Roster[ID.REYKA], listOf(Modifier.practice))
            fight.setParent(this)
            fight.go()
        } else if (choice.startsWith("PostCombat")) {
            Global.gui
                .message("With flushed faces and lingering pleasure, you clean up after your practice fight and get dressed.")
            player.rest()
            Roster[ID.REYKA].rest()
            Global.gui.choose(this, "Leave")
        } else if (choice == "Leave") {
            Global.setCounter(Flag.ReykaDWV, 0.0)
            Global.gui.showNone()
            done(true)
        } else done(false)
    }

    private fun promptScenes() {
        Global.gui.choose(this, "Sex", "If Reyka wants sex, she won't ask twice")
        if (reyka.getAffection(player) >= 8) {
            Global.gui.choose(this, "Clearheaded Sex", "You've done this enough times to resist her magic")
        }
        Global.gui.choose(this, "Games", "Do they play games in... wherever Reyka is from?")
        if (reyka.getAffection(player) >= 16) {
            Global.gui.choose(this, "Locator", "Maybe you can use some of Reyka's magic to even the odds")
        }
        Global.gui.choose(this, "Sparring", "You'll probably have a better chance without the sex component")
        if (reyka.getAffection(player) >= 12) {
            Global.gui.choose(this, "Sex Sparring", "Reyka will teach you a new trick, but she's using her specialty")
        }
        if (Roster.getAffection(ID.REYKA, ID.PLAYER) >= 30 && reyka.has(Trait.limitedpotential)) {
            Global.gui.choose(
                this,
                "Frustration",
                "Reyka has been acting irritable recently. Maybe you should talk to her about it"
            )
        }
        Global.gui.choose(this, "Practice Fight", "Have a sex-fight off the record.")
    }

    override fun shop(npc: Character, budget: Int) {
        Roster.gainAffection(npc.id, ID.REYKA, 1)
    }
}

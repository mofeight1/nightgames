package daytime

import characters.Attribute
import characters.Character
import characters.ID
import global.Constants
import global.Flag
import global.Global
import global.Roster
import global.Scheduler
import items.Toy
import scenes.Event
import utilities.weightedRandom
import java.time.LocalTime

class MorningEvents(private val player: Character, private val day: Daytime) : Event {
    private val weekend = Scheduler.date % 7 == 6 || Scheduler.date % 7 == 0
    private val possible = HashMap<String, Int>()

    init {
        Global.current = this
        Global.unflag(Flag.night)
    }

    override fun respond(response: String) {
        day.plan()
    }

    override fun play(response: String): Boolean {
        Global.gui.clearCommand()
        if (!weekend) {
            Scheduler.time = LocalTime.of(15, 0)
        }
        if (response === "Awards") {
            Global.gui.message(
                "This season of the Games is over, and everyone has gathered at the Student Union for a simple awards ceremony. " +
                        "Everyone has probably been tracking the scores over the season, so the announcement is mostly a formality.<p>"
            )
            Global.gui.clearText()
            val ordering = Scheduler.rankParticipants()
            val highscore = Scheduler.getScore(ordering[0].id)
            val champions = ArrayList<Character>()
            for (c in ordering) {
                if (Scheduler.getScore(c.id) >= highscore) {
                    champions.add(c)
                }
            }
            if (champions.size == 1) {
                if (champions.contains(player)) {
                    Global.gui.message(
                        "You prevailed over all your opponents this season, earning the title of champion. Another season is going " +
                                "to start next week, so your title is a fleeting thing, but it does earn you a special prize."
                    )
                } else {
                    Global.gui
                        .message(champions[0].name + " was the overall winner this season, earning some bragging rights and a special prize.")
                }
            } else {
                if (champions.contains(player)) {
                    Global.gui
                        .message("Multiple participants share the top spot this season, including yourself. You'll get a special prize to use next season.")
                } else {
                    Global.gui
                        .message("Multiple participants share the top spot this season, but you are not among them.")
                }
            }
            for (victor in champions) {
                if (victor.human()) {
                    if (!victor.has(Toy.bloodhound)) {
                        Global.gui.message(
                            "You are awarded a a phone app called 'Bloodhound' that will notify you of your opponents' locations " +
                                    "during a match. It doesn't make for the prettiest trophy, but it sounds extremely useful.<br>"
                        )
                        victor.gain(Toy.bloodhound)
                    } else if (!victor.has(Toy.Paddle)) {
                        Global.gui.message(
                            "You are given an ornate and beautifully finished wooden paddle. Being able to smack your opponents' asses with " +
                                    "your championship trophy is pretty baller. It also feels like it would hurt quite a bit.<br>"
                        )
                        victor.gain(Toy.Paddle)
                    } else if (!victor.has(Toy.nippleclamp)) {
                        Global.gui.message(
                            "You are given a pair of gilded nipple clamps. Not exactly the sort of thing you'd wear as jewelry, but you can " +
                                    "probably find a use for them.<br>"
                        )
                        victor.gain(Toy.nippleclamp)
                    } else if (!victor.has(Toy.AnalBeads)) {
                        Global.gui.message(
                            "You are given a string of pearl anal beads. Quite the expensive material to shove up someone's ass. It'll be interesting " +
                                    "to see whether you can make effective use of it.<br>"
                        )
                        victor.gain(Toy.AnalBeads)
                    } else {
                        Global.gui
                            .message("You are given a substantial cash prize. Not like you were hurting for money, but a little extra is always welcome.<br>")
                        victor.money += 20000
                    }
                } else {
                    if (!victor.has(Toy.Paddle)) {
                        Global.gui
                            .message(victor.name + " is awarded an exquisitely crafted wooden paddle. As pretty as it it, you aren't looking forward to getting spanked by it.")
                        victor.gain(Toy.Paddle)
                    } else if (!victor.has(Toy.nippleclamp)) {
                        Global.gui
                            .message(victor.name + " is awarded a pair of dangerous looking gold nipple clamps. Hopefully you won't be on the receiving end of those.")
                        victor.gain(Toy.nippleclamp)
                    } else if (!victor.has(Toy.AnalBeads)) {
                        Global.gui.message(victor.name + " is awarded a string of beautiful pearl anal beads. ")
                        victor.gain(Toy.AnalBeads)
                    } else {
                        Global.gui.message("")
                        victor.money += 20000
                    }
                }
            }
            Scheduler.clearScores()
            Global.flag(Flag.OffSeason)
            Global.gui.message("A new season will start next week.")
        } else if (response === "RankOne") {
            Global.gui.clearText()
            Global.gui.message(
                "The next day, just after getting out of class you receive call from a restricted number. Normally you'd just ignore it, " +
                        "but for some reason you feel compelled to answer this one. You're greeted by a man with a clear deep voice. <i>\"Hello " + player.name + ", and " +
                        "congratulations. Your performance in your most recent matches has convinced me you're ready for a higher level of play. You're promoted to " +
                        "ranked status, effective immediately. This new status earns you a major increase in monetary rewards and many new opportunities. I'll leave " +
                        "the details to others. I just wanted to congratulate you personally.\"</i> Wait, wait. That's not the end of the call. This guy is clearly " +
                        "someone overseeing the Game, but he hasn't even given you his name. Why all the secrecy? <i>\"If you're looking for more information, you " +
                        "know someone who sells it.\"</i> There's a click and the call ends."
            )
            player.rankup()
            Global.gui.choose("Next")
            return true
        } else if (response === "RankTwo") {
            Global.gui.clearText()
            Global.gui.message(
                "In the morning, you receive a call from a restricted number. You have a pretty decent guess who it might be. Hopefully it is good " +
                        "news. <i>\"Hello again " + player.name + ".\"</i> You were right, that voice is pretty hard to forget. <i>\"I am impressed. You and your opponents are " +
                        "all quickly adapting to what most people would consider an extraordinary situation. If you are taking advantage of the people and services available " +
                        "to you, you could probably use more money. Therefore, I am authorizing another pay increase. Congratulations.\"</i> This is the mysterious Benefactor " +
                        "everyone keeps referring to, right? Is he ever planning to show himself in person? What is he getting out of all this? <i>\"Your curiosity is admirable. " +
                        "Keep searching. If you have as much potential as I think you do, we'll meet soon enough.\"</i>"
            )
            player.rankup()
            Global.gui.choose("Next")
            return true
        } else if (response === "RankThree") {
            Global.gui.clearText()
            Global.gui.message(
                "In the morning, you receive a call from a restricted number. You are not at all surprised to hear the voice of your anonymous "
                        + "Benefactor again. It did seem about time for him to call again. <i>\"Hello " + player.name + ". Have you been keeping busy? You've been putting "
                        + "on a good show in your matches, but when we last spoke, you had many questions. Are you any closer to finding your answers?\"</i><p>"
                        + "That's an odd question since it depends on whether or not he has become more willing to talk. Who else is going to fill you in about this "
                        + "apparently clandestine organization?<p>"
                        + "<i>\"Oh don't become lazy now. I chose you for this Game, in part, for your drive and initiative. Are "
                        + "you limited to just the information that has been handed to you? Just because Aesop does not have the answers for sale does not mean there "
                        + "are no clues. Will you simply give up?\"</i><p>"
                        + "You know he's trying to provoke you, but it's working anyway. If he's offering a challenge, you'll show him you can track him down. The next "
                        + "time you speak to this Benefactor, it will be in person.<p>"
                        + "<i>\"Excellent!\"</i> His voice has only a trace of mockery in it. <i>\"You are "
                        + "already justifying your new rank, which is what I am calling you about, incidently. Perhaps you can put your increased pay rate or the trust "
                        + "you've built with your opponents to good use. Well then, I shall wait to hear from you this time.\"</i> There's a click and the call ends."
            )
            player.rankup()
            Global.gui.choose("Next")
            return true
        } else if (response === "RankFour") {
            Global.gui.clearText()
            Global.gui.message(
                "In the morning, you half-expect another call from the Benefactor, but you are instead approached by Maya on your way to class. "
                        + "She gives you a polite smile and leads you to a quiet area so you can talk privately.<p>"
                        + "<i>\"First of all, congratulations. I'm here on behalf of our Benefactor to inform you of your promotion to Rank Four. Normally He insists "
                        + "on congratulating participants personally, but this morning He asked me to meet you instead. He said something cryptic about the bet being "
                        + "unfair if He wins so quickly. Do you know what He's talking about?\"</i><p>"
                        + "Apparently he hasn't forgotten that you said you'd track him down before he called you again. Some time has passed, but you don't feel "
                        + "much closer to finding the answers. Maybe some of the girls know something you don't. Aesop is another possibility, but whenever it "
                        + "relates to the organization of the Games, his prices get a little crazy.<p>"
                        + "Maya interrupts your thoughts with a stern glare. <i>\"Look, I don't know what kind of bet you made with our Benefactor, but you should "
                        + "know He's just playing with you.\"</i> She really pronounces the capital letters when she talking about him. It's a bit unnerving.<br>"
                        + "<i>\"Our Benefactor is so far beyond your understanding that the idea of a mere college student getting the best of him is laughable.\"</i><p>"
                        + "She regains her composure and her expression reverts to her usual elegant smile. <i>\"I'm sorry. I was meant to be congratulating you, "
                        + "wasn't I? You really are showing great improvement, and it seems to be pushing the other competitors to excel as well. If you continue "
                        + "to improve at this rate, I won't be surprised if our Benefactor invites you to meet him soon. Keep up the good work.\"</i><p>"
                        + "She gives you a polite nod and then departs. It sounds like you'll need to hurry if you want to win the bet and meet the Benefactor "
                        + "on your own terms."
            )
            player.rankup()
            Global.gui.choose("Next")
            return true
        } else if (response === "RankFive") {
            Global.gui.clearText()
            Global.gui.message(
                "On your way to class, you receive a call from a restricted number. You quickly hit the ignore button to decline the call. You still "
                        + "haven't found enough clues to locate the Benefactor, so accepting his call would be tantamount to admitting defeat. As long as you don't talk "
                        + "to him, the bet is still on, right? Besides, maybe that wasn't even him. Some telemarketers still use restricted numbers, right?<p>"
                        + "A minute later, you receive a text message:<br>"
                        + "'Really " + player.name + "? You're dodging my calls now? I admire your pluck at least. Very well. Even if you don't answer the call, you "
                        + "are still promoted to Rank Five. You're now at the highest rank a freelance competitor can reach. I was planning to invite you to meet, "
                        + "but if you insist on finding me yourself, I will wait a while longer. "
                        + "Just be aware that you are in overtime right now. I won't wait forever.'<p>"
                        + "It seems your time limit is almost up. Fortunately it looks like the Benefactor threw you a bone. Unlike the call, this text message has a "
                        + "real phone number attached. Aesop could probably do something with this. It won't be cheap, but your new promotion should help with that."
            )
            player.rankup()
            Global.gui.choose("Next")
            return true
        } else if (response === "Weekend") {
            Global.gui
                .message("You don't have any classes today, but you try to get up at a reasonable hour so you can make full use of your weekend.")
        } else if (response === "Weekend2") {
            Global.gui
                .message("It's a beautiful day outside and you don't have any classes today. You try to get up at a reasonable hour so you can make full use of your weekend.")
        } else if (response === "Normal") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>It's a nice day out today. You're done with classes by mid-afternoon" +
                        " and have the rest of the day free."
            )
        } else if (response === "Normal2") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>The sky is overcast and grey this morning You're done with classes by mid-afternoon" +
                        " and have the rest of the day free."
            )
        } else if (response === "Normal3") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>It's a bit chilly today. You're done with classes by mid-afternoon" +
                        " and have the rest of the day free."
            )
        } else if (response === "Normal4") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>It's unseasonably warm today. You're done with classes by mid-afternoon" +
                        " and have the rest of the day free."
            )
        } else if (response === "Cunning") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>All of your classes seem to be getting easier "
                        + "recently. Maybe you're just getting smarter."
            )
        } else if (response === "Cunning2") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>You are acing all your tests despite barely paying "
                        + "attention in class anymore. Your classes are too easy for you this semester. You end up spending more time thinking about the Games, "
                        + "which are the only challenge you get anymore."
            )
        } else if (response === "Seduction") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>You smile at a girl you don't know on the "
                        + "way to class today and she blushes bright red. Apparently you've become a bit more charming."
            )
        } else if (response === "Seduction2") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>You wink at a girl in your class and she almost faints. "
                        + "You need to consciously dial back your charm before someone gets hurts."
            )
        } else if (response === "Power") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>In the hallway, a careless linebacker for the school's football "
                        + "team runs straight into you and bounces off harmlessly. As you help him up, you reflect that you've become much stronger recently."
            )
        } else if (response === "Power2") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>Out of curiosity, you visit the gym in the morning to see "
                        + "how much you can lift. You max out several of the weight machines and people gawk as you deadlift more than a man twice your size. Your "
                        + "strength is in an entirely different class."
            )
        } else if (response === "Speed") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>You lose track of time before class and end up "
                        + "sprinting across the campus. Somehow you manage to make it in time anyway."
            )
        } else if (response === "Cassie") {
            Global.gui
                .message("You get up earlier than usual to have breakfast with Cassie at a nearby diner. Your morning is otherwise uneventful.")
        } else if (response === "Mara") {
            Global.gui.message(
                "On a hunch, you stop by Mara's lab on the way to class. You find her sleeping at her desk, presumably having worked through "
                        + "the night. You're annoyed at her recklessness, but the last thing you want to do is wake her up, so you just put your coat over her "
                        + "to keep her from catching a cold."
            )
        } else if (response === "Angel") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>Between classes, you meet up with Angel and her friends. "
                        + "you have a pleasant, if uneventful time."
            )
        } else if (response === "Jewel") {
            Global.gui
                .message("You get up early to join Jewel on a quick jog before class. Your morning is otherwise uneventful.")
        } else if (response === "Dark") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>During class, you accidently release your lust aura. By "
                        + "the time you notice, everyone in class is visibly aroused, but has no idea why."
            )
        } else if (response === "Arcane") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>On the way to class, you sneeze and a nearby girl's skirt "
                        + "is blown completely off. Your magic is becoming more powerful, now you just need to work on control."
            )
        } else if (response === "MaraWeekend") {
            Global.gui.message(
                "You wake up to find Mara sleeping comfortably in your bed. You don't know when she snuck in, but you don't feel like waking her "
                        + "up."
            )
        } else if (response === "Science") {
            Global.gui.message(
                "You end up getting a couple physics questions wrong that you were fairly confident about. After checking, you realize what Jett "
                        + "taught you doesn't match the textbook. You're pretty sure Jett was right, but you aren't going to be able to argue those point back."
            )
        } else if (response === "Kat") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>Between classes, you happen to run into Kat. The two of "
                        + "you spend some time chatting pleasantly."
            )
        } else if (response === "Perception") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>In the hallway, your sharp eyes spot a five dollar bill "
                        + "under a vending machine. It's your now."
            )
            player.money += 5
        } else if (response === "ReykaWeekend") {
            Global.gui.message(
                "You wake up in the morning to find Reyka perched over your morning wood. She gets a quick meal, and you end up sleeping in a little "
                        + "later than you planned."
            )
        } else if (response === "Fetish") {
            Global.gui.message(
                "You try to get as much sleep as you can before your morning classes.<p>You're talking with a girl in class and you suddenly realize she "
                        + "likes to be spanked, but you don't know how you know. Apparently your control of Fetish magic is making you able to sense people's kinks."
            )
        }
        return false
    }

    override fun morning(): String {
        if (Scheduler.matchNumber > Constants.SEASONLENGTH && !Global.checkFlag(Flag.OffSeason)) {
            return "Awards"
        } else if (player.level >= 10 && player.rank == 0) {
            return "RankOne"
        } else if (player.level >= 20 && player.rank == 1) {
            return "RankTwo"
        } else if (player.level >= 30 && player.rank == 2) {
            return "RankThree"
        } else if (player.level >= 40 && player.rank == 3) {
            return "RankFour"
        } else if (player.level >= 50 && player.rank == 4) {
            return "RankFive"
        }
        if (weekend) {
            possible["Weekend"] = 5
            possible["Weekend2"] = 5
            if (Roster.getAffection(ID.PLAYER, ID.MARA) >= 14) {
                possible["MaraWeekend"] = 5
            }
            if (Global.checkFlag(Flag.Reyka)) {
                possible["ReykaWeekend"] = 5
            }
        } else {
            possible["Normal"] = 5
            possible["Normal2"] = 5
            possible["Normal3"] = 5
            possible["Normal4"] = 5
            if (player.getPure(Attribute.Cunning) >= 15) {
                possible["Cunning"] = 5
            }
            if (player.getPure(Attribute.Cunning) >= 30) {
                possible["Cunning2"] = 3
            }
            if (player.getPure(Attribute.Seduction) >= 15) {
                possible["Seduction"] = 5
            }
            if (player.getPure(Attribute.Seduction) >= 30) {
                possible["Seduction2"] = 3
            }
            if (player.getPure(Attribute.Power) >= 15) {
                possible["Power"] = 5
            }
            if (player.getPure(Attribute.Power) >= 30) {
                possible["Power2"] = 3
            }
            if (player.getPure(Attribute.Speed) >= 12) {
                possible["Speed"] = 5
            }
            if (player.getPure(Attribute.Perception) >= 7) {
                possible["Perception"] = 5
            }
            if (player.getPure(Attribute.Arcane) >= 10) {
                possible["Arcane"] = 5
            }
            if (player.getPure(Attribute.Dark) >= 10) {
                possible["Dark"] = 5
            }
            if (player.getPure(Attribute.Science) >= 10) {
                possible["Science"] = 5
            }
            if (player.getPure(Attribute.Fetish) >= 10) {
                possible["Fetish"] = 5
            }
            if (Roster.getAffection(ID.PLAYER, ID.CASSIE) >= 14) {
                possible["Cassie"] = 5
            }
            if (Roster.getAffection(ID.PLAYER, ID.MARA) >= 14) {
                possible["Mara"] = 5
            }
            if (Roster.getAffection(ID.PLAYER, ID.CASSIE) >= 14) {
                possible["Cassie"] = 5
            }
            if (Roster.getAffection(ID.PLAYER, ID.ANGEL) >= 14) {
                possible["Angel"] = 5
            }
            if (Roster.getAffection(ID.PLAYER, ID.JEWEL) >= 14) {
                possible["Jewel"] = 5
            }
            if (Roster.getAffection(ID.PLAYER, ID.KAT) >= 14) {
                possible["Kat"] = 5
            }
        }
        return chooseScene()
    }

    private fun chooseScene(): String {
        val weights = possible.values
        return possible.keys.toTypedArray().weightedRandom(weights)
    }

    override fun mandatory(): String {
        return ""
    }

    override fun addAvailable(available: HashMap<String, Int>) {
        return
    }
}

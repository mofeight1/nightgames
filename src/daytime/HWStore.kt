package daytime

import characters.Character
import global.Flag
import global.Global
import items.Component
import kotlin.math.min

class HWStore(player: Character) : Store("Hardware Store", player) {
    init {
        add(Component.Tripwire)
        add(Component.Rope)
        add(Component.Spring)
        add(Component.Sprayer)
    }

    override fun known(): Boolean {
        return Global.checkFlag(Flag.basicStores)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (choice === "Start") {
            acted = false
        }
        if (choice === "Leave") {
            done(acted)
            return
        }
        checkSale(choice)
        if (player.human()) {
            Global.gui.message(
                "Nothing at the hardware store is designed for the sort of activities you have in mind, but there are components you could use to make some " +
                        "effective traps."
            )
            for (i in stock.keys) {
                Global.gui.message(i.getName() + ": $" + i.price)
            }
            Global.gui.message("You have: $" + player.money + " available to spend.")
            displayGoods()
            Global.gui.choose(this, "Leave")
        }
    }

    override fun shop(npc: Character, budget: Int) {
        var remaining = min(budget, 140)
        var bored = 0
        while (remaining > 10 && bored < 10) {
            for (i in stock.keys.shuffled()) {
                if (remaining > i.price && !npc.has(i, 20)) {
                    npc.gain(i)
                    npc.money -= i.price
                    remaining -= i.price
                } else {
                    bored++
                }
            }
        }
    }
}

package daytime

import characters.Attribute
import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import global.Flag
import global.Global
import global.Roster
import global.Roster.getAffection
import global.Scheduler
import scenes.Event
import scenes.SceneFlag
import scenes.SceneManager
import java.time.LocalTime
import kotlin.collections.set

class Threesomes(private val player: Character) : Event {
    private val angel = Dummy("Angel")
    private val mara = Dummy("Mara")
    private val cassie = Dummy("Cassie")
    private val jewel = Dummy("Jewel")
    private val reyka = Dummy("Reyka")
    private val kat = Dummy("Kat")
    private val yui = Dummy("Yui")
    private val proc = false


    override fun play(choice: String): Boolean {
        if (choice === "CassieJewel") {
            cassie.undress()
            cassie.blush = 3
            jewel.undress()
            jewel.blush = 3
            jewel.mood = Emotion.dominant
            Global.gui.loadTwoPortraits(jewel, cassie)
            SceneManager.play(SceneFlag.CassieJewelThreesome)
        }
        if (choice === "CassieAngel") {
            cassie.undress()
            cassie.blush = 3
            cassie.mood = Emotion.nervous
            angel.undress()
            angel.blush = 3
            Global.gui.loadTwoPortraits(cassie, angel)
            SceneManager.play(SceneFlag.CassieAngelThreesome)
        }
        if (choice === "MaraJewel") {
            jewel.blush = 2
            jewel.mood = Emotion.dominant
            mara.blush = 2
            mara.mood = Emotion.dominant
            Global.gui.loadTwoPortraits(jewel, mara)
            SceneManager.play(SceneFlag.MaraJewelThreesome)
        }
        if (choice === "CassieMara") {
            cassie.undress()
            cassie.blush = 3
            cassie.mood = Emotion.horny
            mara.undress()
            mara.blush = 3
            Global.gui.loadTwoPortraits(cassie, mara)
            SceneManager.play(SceneFlag.CassieMaraThreesome)
        }
        if (choice === "AngelMara") {
            mara.undress()
            mara.blush = 3
            mara.mood = Emotion.horny
            angel.undress()
            angel.blush = 3
            angel.mood = Emotion.horny
            Global.gui.loadTwoPortraits(mara, angel)
            SceneManager.play(SceneFlag.AngelMaraThreesome)
        }
        if (choice === "AngelReyka") {
            angel.undress()
            angel.blush = 3
            angel.mood = Emotion.horny
            reyka.undress()
            reyka.blush = 3
            Global.gui.loadTwoPortraits(angel, reyka)
            SceneManager.play(SceneFlag.AngelReykaThreesome)
        }
        if (choice === "CassieKat") {
            cassie.undress()
            cassie.blush = 3
            kat.undress()
            kat.blush = 3
            Global.gui.loadTwoPortraits(cassie, kat)
            SceneManager.play(SceneFlag.CassieKatThreesome)
        }
        if (choice === "AngelReykaImp") {
            angel.undress()
            angel.blush = 3
            angel.mood = Emotion.horny
            reyka.undress()
            reyka.blush = 3
            Global.gui.loadTwoPortraits(angel, reyka)
            if (Roster[ID.ANGEL][Attribute.Dark] +
                Global.random(10) > Global.player[Attribute.Dark] +
                Global.random(10)
            ) {
                SceneManager.play(SceneFlag.AngelReykaThreesome2a)
            } else {
                SceneManager.play(SceneFlag.AngelReykaThreesome2)
            }
        }
        if (choice === "MaraKat") {
            mara.undress()
            mara.blush = 3
            kat.undress()
            kat.blush = 3
            Global.gui.loadTwoPortraits(mara, kat)
            SceneManager.play(SceneFlag.MaraKatThreesome)
        }
        if (choice === "CassieYui") {
            cassie.undress()
            cassie.mood = Emotion.horny
            yui.dress()
            yui.mood = Emotion.horny
            SceneManager.play(SceneFlag.CassieYuiThreesome)
        }
        if (choice.equals("AngelJewel", ignoreCase = true)) {
            jewel.setTopOuter(false)
            jewel.setTopInner(false)
            jewel.mood = Emotion.angry
            jewel.blush = 2
            angel.mood = Emotion.dominant
            angel.blush = 2
            SceneManager.play(SceneFlag.AngelJewelThreesome)
        }
        Global.flag(Flag.threesome)
        Global.modCounter(Flag.ThreesomeCnt, 1.0)

        Global.current = this
        Global.gui.choose("Next")
        return true
    }

    override fun respond(response: String) {
        Global.gui.clearText()
        Scheduler.advanceTime(LocalTime.of(1, 0))
        Scheduler.day!!.plan()
    }

    override fun morning(): String {
        return ""
    }


    override fun mandatory(): String {
        return ""
    }


    override fun addAvailable(available: HashMap<String, Int>) {
        if (Global.checkFlag(Flag.threesome)) {
            return
        }
        if (getAffection(ID.PLAYER, ID.CASSIE) >= 20 &&
            getAffection(ID.PLAYER, ID.JEWEL) >= 20 &&
            getAffection(ID.JEWEL, ID.CASSIE) >= 5 &&
            Global.getValue(Flag.CassieDWV) >= 5
        ) {
            available["CassieJewel"] = 4
        }
        if (getAffection(ID.PLAYER, ID.MARA) >= 20 &&
            getAffection(ID.PLAYER, ID.JEWEL) >= 20 &&
            getAffection(ID.MARA, ID.JEWEL) >= 5
        ) {
            available["MaraJewel"] = 4
        }
        if (getAffection(ID.PLAYER, ID.MARA) >= 15 &&
            getAffection(ID.PLAYER, ID.ANGEL) >= 15 &&
            getAffection(ID.MARA, ID.ANGEL) >= 10
        ) {
            available["AngelMara"] = 4
        }
        if (getAffection(ID.PLAYER, ID.MARA) >= 15 &&
            getAffection(ID.PLAYER, ID.CASSIE) >= 15 &&
            getAffection(ID.MARA, ID.CASSIE) >= 10
        ) {
            available["CassieMara"] = 4
        }
        if (getAffection(ID.PLAYER, ID.ANGEL) >= 20 &&
            getAffection(ID.PLAYER, ID.CASSIE) >= 20 &&
            getAffection(ID.ANGEL, ID.CASSIE) >= 10
        ) {
            available["CassieAngel"] = 4
        }
        if (getAffection(ID.PLAYER, ID.ANGEL) >= 20 &&
            getAffection(ID.PLAYER, ID.REYKA) >= 20 &&
            getAffection(ID.REYKA, ID.ANGEL) >= 10
        ) {
            available["AngelReyka"] = 4
        }
        if (getAffection(ID.PLAYER, ID.KAT) >= 20 &&
            getAffection(ID.PLAYER, ID.CASSIE) >= 20 &&
            getAffection(ID.KAT, ID.CASSIE) >= 10
        ) {
            available["CassieKat"] = 4
        }
        if (getAffection(ID.PLAYER, ID.KAT) >= 20 &&
            getAffection(ID.PLAYER, ID.MARA) >= 20 &&
            getAffection(ID.KAT, ID.MARA) >= 10
        ) {
            available["MaraKat"] = 4
        }
        if (getAffection(ID.PLAYER, ID.ANGEL) >= 20 &&
            getAffection(ID.PLAYER, ID.REYKA) >= 20 &&
            getAffection(ID.REYKA, ID.ANGEL) >= 10 &&
            player.getPure(Attribute.Dark) >= 10
        ) {
            available["AngelReykaImp"] = 2
        }
        if (getAffection(ID.PLAYER, ID.YUI) >= 20 &&
            getAffection(ID.PLAYER, ID.CASSIE) >= 20 &&
            getAffection(ID.CASSIE, ID.YUI) >= 10
        ) {
            available["CassieYui"] = 4
        }
        if (getAffection(ID.PLAYER, ID.ANGEL) >= 20 &&
            getAffection(ID.PLAYER, ID.JEWEL) >= 20 &&
            getAffection(ID.ANGEL, ID.JEWEL) >= 10
        ) {
            available["AngelJewel"] = 4
        }
    }
}

package daytime

import characters.Character
import global.Flag
import global.Global
import items.Clothing

class ClothingStore(player: Character) : Store("Clothing Store", player) {
    init {
        add(Clothing.Tshirt)
        add(Clothing.shirt)
        add(Clothing.sweatshirt)
        add(Clothing.sweater)
        add(Clothing.silkShirt)
        add(Clothing.jeans)
        add(Clothing.shorts)
        add(Clothing.sweatpants)
        add(Clothing.dresspants)
        add(Clothing.boxers)
        add(Clothing.briefs)
        add(Clothing.sweatshirt)
        add(Clothing.undershirt)
        add(Clothing.jacket)
        add(Clothing.windbreaker)
        add(Clothing.blazer)
        add(Clothing.gothshirt)
        add(Clothing.gothpants)
        add(Clothing.bra)
        add(Clothing.panties)
        add(Clothing.blouse)
        add(Clothing.skirt)
    }

    override fun known(): Boolean {
        return Global.checkFlag(Flag.basicStores)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        if (choice === "Start") {
            acted = false
        }
        if (choice === "Leave") {
            done(acted)
            return
        }
        checkSale(choice)
        if (player.human()) {
            Global.gui
                .message("This is a normal retail clothing outlet. For obvious reasons, you'll need to buy anything you want to wear at night in bulk.")
            for (i in clothing.keys) {
                Global.gui.message(i.fullDesc + " : $" + i.price + (if (player.has(i)) " (Owned)" else ""))
            }
            Global.gui.message("You have: $" + player.money + " available to spend.")
            displayGoods()
            Global.gui.choose(this, "Leave")
        }
    }

    override fun shop(npc: Character, budget: Int) {
        // TODO Auto-generated method stub
    }
}

package daytime

import characters.Character
import global.Global
import global.Scheduler
import java.time.LocalTime

abstract class Activity(protected var name: String, protected var player: Character) {
    protected var time: LocalTime = LocalTime.of(1, 0)
    protected var page: Int = 0
    protected var tooltip: String
    private val randomScenes = HashMap<String, Int>()

    init {
        this.tooltip = ""
    }

    constructor(name: String, player: Character, tooltip: String) : this(name, player) {
        this.tooltip = tooltip
    }

    abstract fun known(): Boolean
    abstract fun visit(choice: String)
    fun time(): LocalTime {
        return time
    }

    fun next() {
        page++
    }

    fun done(acted: Boolean) {
        if (acted) {
            Scheduler.advanceTime(time)
        }
        page = 0
        Scheduler.day!!.plan()
    }

    override fun toString(): String {
        return name
    }

    abstract fun shop(npc: Character, budget: Int)
    fun clearRandomScenes() {
        randomScenes.clear()
    }

    fun addRandomScene(scene: String, weight: Int) {
        randomScenes[scene] = weight
    }

    val randomScene: String
        get() {
            if (randomScenes.isEmpty()) {
                return ""
            } else {
                var totalWeight = 0
                val count = randomScenes.size
                for (i in randomScenes.values) {
                    totalWeight += i
                }
                var sum = 0
                val r = Global.random(totalWeight)
                for (scene in randomScenes.keys) {
                    sum += randomScenes[scene]!!
                    if (r < sum) {
                        return scene
                    }
                }
                return randomScenes.keys.iterator().next()
            }
        }

    fun tooltip(): String {
        return tooltip
    }
}

package daytime

import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import combat.Combat
import global.Flag
import global.Global
import global.Modifier
import global.Roster
import scenes.SceneFlag
import scenes.SceneManager

class SamanthaTime(player: Character) : Activity("Samantha", player) {
    private var acted = false
    private val sprite = Dummy("Samantha")

    override fun known(): Boolean {
        return Global.checkFlag(Flag.Samantha)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        sprite.dress()
        sprite.blush = 0
        sprite.mood = Emotion.confident
        if (choice === "Start") {
            acted = true
            Global.gui.displayImage("chibi/samantha tea.png", "Art by AimlessArt")
            if (Roster.getAffection(ID.PLAYER, ID.SAMANTHA) >= 5) {
                Global.gui.loadPortrait(player, sprite)
                Global.gui.message(
                    "You're at the usual coffee shop, but this time you're prepared. You're sharply dressed, but not overdressed. Your hair has just the right amount of product to look its best. Most importantly, you've done your homework fixing your earlier financial planning blunder.<p>" +
                            "Knowing your luck, Samantha probably won't even be here today.<p>" +
                            "Nope. She and her laptop are in her usual spot, as reliable as clockwork. Maybe you'll actually impress her today. Or maybe you've overlooked something else and will make a fool of yourself again.<p>" +
                            "Samantha immediately looks up at you as you approach this time. <p>" +
                            "<i>\"I had a feeling you were going to come see me again, right around now.\"</i> She gives you a charming smile before looking you up and down. <i>\"That's a good look for you. I wouldn't mind you taking me to dinner if you were dressed like that.\"</i><p>" +
                            "Your soul does an internal fist pump as you sit down across from her. That's already more praise than you were expecting to get from her.<p>" +
                            "<i>\"I almost feel bad for looking such a mess. I don't actually, because we never made plans to meet up. I can't realistically look my best 24/7, just in case some eager puppy-dog comes to court me.\"</i> She's back to teasing, but there's no sting to her words this time. <p>" +
                            "Samantha's casual outfit and makeup-free appearance are clearly low maintenance, but she's far from a ‘mess'. She's already gorgeous in anything she's wearing (or occasionally not wearing), but that unkempt beauty look has a real appeal of its own.<p>" +
                            "She gives you a smile that, if you didn't know her better, you'd swear was bashful. She gestures for you to lean close and lowers her voice to a whisper.<p>" +
                            "<i>\"May I be a bit blunt, Love? If you want to have sex, you can just ask me.\"</i><p>" +
                            "You stare at her blankly, caught completely off guard. That's good to know, but came seemingly out of nowhere.<p>" +
                            "<i>\"I'm used to men trying to impress me, even when they're paying for my time. Planning for your future is good, but if you're just trying to get into my pants, you don't need to go through the effort. You don't need to pay either.\"</i><p>" +
                            "She sips her coffee thoughtfully, considering her words. <p>" +
                            "<i>\"What I sell isn't just my body, but a set of skills I've cultivated over years of experience. I give my clients an amazing time, and I am compensated financially, rather than in kind. You however, are competitively good at sex. Good enough, at least, that I won't split hairs over whose skills are superior.\"</i><p>" +
                            "<i>\"I like sex. I wouldn't be doing this job if I didn't. I'm a bit too mature for schoolgirl crushes, but I can reasonably say that an hour in bed with you sounds like a pretty good time. As long as you aren't expecting much more than that, you can consider it a standing offer.\"</i><p>" +
                            "You take a moment to consider her words before you reply, not wanting to sound too eager or flustered. Her invitation is extremely tempting, and you'll probably take her up on it, but that isn't why you've gone to all this effort.<p>" +
                            "Talking to Samantha here at this coffee shop was a wake-up call for you to think more about your future. You need to prove that you are capable of thinking like an adult, not just to her, but to yourself.<p>" +
                            "Samantha looks a bit surprised by your response, but flashes you a dazzling smile.<p>" +
                            "<i>\"Glad to hear it. Now, I bet you have a new portfolio to show me. Let's see if I have any more advice to offer.\"</i><p>"
                )
                Global.flag(Flag.SamanthaDate)
            } else if (Roster.getAffection(ID.PLAYER, ID.SAMANTHA) > 3) {
                Global.gui.loadPortrait(player, sprite)
                Global.gui.message(
                    "You pause to admire your reflection in the shop window, and adjust your tie just so.<p>" +
                            "Samantha is at her usual booth, poised in front of her laptop with one finger on the return key. She's got the hood of her sweatshirt up, and has tuned out the world around herself, to the point where she doesn't notice you when you sit down across from her.<p>" +
                            "A couple of minutes crawl by. You ask what she's doing, but she doesn't react until...<p>" +
                            "She punches her keyboard with a victorious smile, then sits back and takes a sip from her mug. That's when she and you make eye contact.<p>" +
                            "<i>\"Hello, " + player.name + ",\"</i> Samantha says. <i>\"Job interview today?\"</i><p>" +
                            "That's a little disappointing. You bought this suit in an attempt to class your look up a little bit, and you say as much.<p>" +
                            "<i>\"Where'd you buy it? That's clearly off the rack.\"</i><p>" +
                            "The local men's clothing store, you have to admit, and yeah, you just bought something in your size.<p>" +
                            "<i>\"You look like you're wearing your dad's clothes,\"</i> Samantha says. <i>\"Here, stand up a second.\"</i><p>" +
                            "You do, and she eyes you over the top of her reading glasses.<p>" +
                            "<i>\"The sleeves are too long,\"</i> she says. <i>\"So are the pant legs, and the shoulders on the jacket are just a little too broad for you. You should've put down the extra money for custom tailoring. Rookie move.\"</i> It's a little bit of a tease, but no more. <i>\"The color does work for you, though. Noble first effort.\"</i><p>" +
                            "You felt really good about this five minutes ago. The last time Samantha kicked you in the balls was somehow less emasculating than this conversation.<p>" +
                            "In an attempt to change the subject, you sit back down and ask what she was doing when you walked in.<p>" +
                            "<i>\"Oh, I'm day-trading,\"</i> she says. <i>\"It's basically a slot machine for stockbrokers. I shouldn't bother, but it's a rush when you win, so sometimes I dabble.\"</i><p>" +
                            "You vaguely recall hearing boring older relatives talking about it.<p>" +
                            "In an attempt to recoup some of your pride, you mention you just invested in a mutual fund yourself. Samantha got you thinking in that direction, so you did a little reading and put some money down.<p>" +
                            "She pushes her glasses back up her nose with the tip of her thumb, then pulls her laptop a little closer. <i>\"Which one?\"</i><p>" +
                            "You mention it, hoping for an impressed reaction. You don't get one.<p>" +
                            "<i>\"Oh,\"</i> Samantha says.<p>" +
                            "That can't be good.<p>" +
                            "<i>\"This... isn't great,\"</i> she says, and turns the laptop to face you.<p>" +
                            "It takes you a second, and a quick bit of research on your own phone, before you catch up with her. The fund you invested in is considered high-risk and low-yield; there are surprisingly few companies in its portfolio, and the fund manager has a nasty reputation. You didn't think to look him up before you put money down.<p>" +
                            "<i>\"Don't feel too bad,\"</i> Samantha says. <i>\"It's easy for a new investor to get led astray. I can set you up with some better reading.\"</i><p>" +
                            "That sounds good, and you say as much. You hope you don't sound too much like a kicked puppy when you do.<p>" +
                            "<i>\"Is this something you're interested in?\"</i> she asks. <i>\"Or is this just for my sake?\"</i><p>" +
                            "Lying to her seems like it'd backfire in the long run. It's a little from column A, a little from column B. Your income from the Games is high enough that what Samantha said made some sense.<p>" +
                            "She takes off her glasses and taps the end of one arm against her teeth, looking at her laptop screen. <i>\"That's... kind of cute, actually. What's your email address?\"</i><p>" +
                            "You give it to her.<p>" +
                            "<i>\"I need to jump back into this,\"</i> Samantha says. <i>\"Keep an eye on your inbox, okay? We'll talk later.\"</i><p>" +
                            "She doesn't wait for you to respond before becoming absorbed in her laptop again. You've just been dismissed. You try not to sulk on your way back out of the shop.<p>" +
                            "A couple of hours later, you get half a dozen new emails from what you suspect might be a throwaway account. Two are lists of links to men's fashion sites, detailing do's and don'ts; the other four are a series of articles on finance and investment, with terse annotations.<p>" +
                            "You just got several hours of additional homework from a high-class hooker. Life is weird."
                )
            } else {
                Global.gui.loadPortrait(player, sprite)
                Global.gui.message(
                    "This coffee shop is always crowded, due to the food. You only made it inside at all because it's mid-afternoon. You'd need a whip and a chair to get through the breakfast line. Apparently the pastries here can change your life, but you've got better things to do than show up at 4:30 in the morning to camp out for a donut.<p>"
                            + "Samantha has a seat at a booth against the shop's back wall, and you barely manage to spot her. Not only is the crowd making it difficult, but she's almost unrecognizable.<p>"
                            + "She always shows up to the Games in an expensive dress, silk stockings, and perfect makeup, with everything just so. It's a testament to her skill, now that you think about it, that she stays that way more often than not. You can't keep a pair of pants intact for more than ten minutes, but she can come out of the competition with the same silk dress she was wearing when she walked in.<p>"
                            + "This is Samantha on her own time, however, wearing tight workout pants, sneakers, and a baggy hooded sweatshirt. Her hair is pulled into a messy knot on the back of her head with a couple of pencils through it to hold it in place. Her face is clean and shiny, without makeup, and she peers at you for a moment over the top of a pair of reading glasses. If she didn't clearly recognize you, you'd think she was another person entirely.<p>"
                            + "She closes her laptop and waves you over, picking up her mug in both hands. You sit down across from her.<p>"
                            + "<i>\"And how are you today, " + player.name + "?\"</i> Samantha asks.<p>"
                            + "You shrug and ask what she's up to.<p>"
                            + "<i>\"Day-trading, mostly.\"</i><p>"
                            + "You weren't sure what she was going to say, but it certainly wasn't that. Samantha catches your momentary confusion on the topic and chuckles, but not like she actually thinks you're all that funny.<p>"
                            + "<i>\"The services I provide are very youth-driven, love. A smart professional starts thinking about her retirement plan very early, and I never wanted to end up as some old millionaire's trophy wife.\"</i> She taps the lid of her laptop with one finger. <i>\"So I make investments with the money from the Games, and babysit them carefully.\"</i> Samantha lowers her glasses and peers at you over their rims. <i>\"You ought to be thinking along similar lines.\"</i><p>"
                            + "It's not something that had occurred to you.<p>"
                            + "<i>\"Of course not. You're a college freshman. This is the most life experience you've ever had.\"</i> She isn't insulting you. It's a plain statement of fact, sledgehammer-blunt. <i>\"But no situation lasts forever. No one stays young. You may be a sexual freak of nature, but one day you won't be.\"</i><p>"
                            + "You don't actually know how much older Samantha is than you are. You've been assuming that it can't be much more than a few years. There's something about how she's speaking, though, that suggests she's seen and done a lot in those few years.<p>"
                            + "You want to ask her about that, to get some idea of what makes someone like her, if only for the same grab-bag of reasons that you've tried to get to know the other participants in the Games.<p>"
                            + "While you're scrambling for a way to put that into words, though, Samantha opens her laptop and goes back to work, just as if you aren't there. It isn't a hint; it's a dismissal. You wave away a waitress and leave the coffee shop, still thinking about how to put together that sentence."
                )
            }
            Roster.gainAffection(ID.PLAYER, ID.SAMANTHA, 2)
            Global.gui.message("<b>You've gained Affection with Samantha</b>")
            if (Global.checkFlag(Flag.SamanthaDate)) {
                Global.gui.choose(this, "Sex")
                if (Roster.getAffection(ID.PLAYER, ID.SAMANTHA) >= 12) {
                    Global.gui.choose(this, "Rough Sex")
                }
                if (Roster.getAffection(ID.PLAYER, ID.SAMANTHA) >= 14) {
                    Global.gui.choose(this, "Strip Poker")
                }
                Global.gui.choose(this, "Practice Fight", "Have a sex-fight off the record.")
            }
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Sex")) {
            sprite.undress()
            sprite.blush = 2
            Global.gui.message(
                "You and Samantha spent some time in the cafe discussing your portfolio, and how to create and update a resume. Things of that nature. All good things to know now, ahead of the time you'll actually be putting them to use. Samantha's lessons in how to present yourself will prove invaluable in the not so distant future, you're certain. As you finish up, you can't help but admire Samantha's beauty. Feeling rather horny anyway, you offer sex. She gives you a grin and closes her laptop, a hint of lust entering her eyes. <i>\"That sounds like a good idea, Love.\"</i> You head out to your dorm, the two of you having making it there in good time in anticipation of some fun.<p>" +
                        "When you close the door behind you, Samantha wastes no time in grabbing you by the collar of your shirt and pulling you in for a kiss. She starts undressing you while making out, surprising you with her skill for a moment before you remember how experienced she is at exactly this task. You begin stripping her as well as you make your way to the bed, leaving a trail of clothes scattered across the room. Once you're both naked, Samantha takes a seat on the edge of the bed, crossing her legs in a manner that leaves her vagina teasingly blocked from your view. <p>" +
                        "<i>\"You remember what I said, " + player.name + "? About how I provide a service for financial compensation because my clients can't pay back in kind?\"</i> You nod. Hard to forget that blunt conversation. \"Well, I just wanted to remind you that I fully expect you to make this feel as good for me as I will for you. Deal?\"</i> You nod without hesitation. After all, you're doing little else with your time recently than getting better at sex. She smiles in approval at your confidence, and spreads her legs, showing herself fully. <i>\"Then let's show each other a good time~\"</i><p>" +
                        "You start between her legs. After all, since you can be reasonably sure that much of her clientele are simply concerned about what she can do for them, you want to show Samantha what you can do for her. She moans in appreciation as you part her labia and begins eating her out. <i>\"Mmm~ So you are thinking of me. Thank you, Love.\"</i> Your hands massage her thighs and play with her clit, showing your appreciation for her lovely body. She wraps her legs around your head in response. <i>\"Since you seem to like it there so much, let me help you get a more intimate look~\"</i> You press your face in, more than happy to spend some extra time servicing her. <p>"
            )
            Global.gui.displayImage("premium/Samantha Licking.jpg", "Art by AimlessArt")
            Global.gui.message(
                "She makes little sounds of pleasure and approval as you continue the oral, but parts her legs not long later. <i>\"That's good. Your skill with your tongue is incredible~ But allow me to pleasure you as well, yes?\"</i> Samantha gestures for you to join her on the bed, and you climb up to meet her face to face. Her legs bind you once more, this time around your waist. She uses them to grind your dick against her pussy lips, and smiles as she feels your erect length, ready to enter her. And thanks to your oral ministrations, she's ready for you as well.<p>" +
                        "You thrust at the same time as she pulls you in, causing you to hilt yourself inside of her in a single go. Both of you moan in pleasure. You lean down and kiss her as you begin thrusting, the tight grip of her legs ensuring you're able to reach deep. Her folds are warm and inviting, clinging lovingly to your length as you thrust in and out.<p>" +
                        "You continue like this until she changes things up again. Suddenly, Samantha flips you over so that you're laying on your back and she's riding you. \"My turn, Love~\"</i>She starts slowly moving up and down on your dick, her lips still clinging to you as she slides you in and out. It feels incredible for both of you, each of you moaning luridly in pleasure. Still, you can't just let her totally control how this goes.<p>" +
                        "After enjoying her slow ride for a bit, you flip her back over to retake the dominant position. She gasps in surprise, but gives you a grin. \"Fine. I suppose we can both play like that~\"</i>The two if you make a game out of this, playfully wrestling for dominance. You fuck her for a while in the missionary position, her legs around your waist as you thrust in hard and deep, much to her moaning enjoyment. Then she flips you over and rides you cowgirl style, her hips practically dancing against yours as she bounces on your dick, watching your face twist in pleasure. Then you flip her over once more and continue the pattern.<p>" +
                        "Eventually, her bouncing starts to become uneven. She's gasping, out of breath, her face flush. Her hands are pressed to your chest in an attempt to keep her balance. It's clear she's about to cum. No surprise, with how long you've both been holding on. You're pretty much at the edge yourself as well.<p>" +
                        "At last, Samantha cums, her pussy clenching tightly around your dick, love juices leaking around you. She collapses atop you, her breasts pressing against your chest. You take advantage, flipping positions one last time. She lies there and takes it as you resume your thrusting, desperate to finish and finally cum yourself. Her tight, squeezing pussy helps with that, and you release your load into her. You bury yourself as deeply as you can and rest your head on her breasts for the duration of your orgasm.<p>" +
                        "When it finally ends, Samantha pulls you in for another kiss. The two of you make out and grope as you both recover, enjoying each others' bodies. At last, you pull out and away, leaving an attractive creampie leaking from her slit. She allows you to enjoy the view for a few moments before standing, cleaning herself up and getting dressed. \"As always, you give as good as you get, Love.\"</i>She gives you one last kiss before gathering her things and leaving. Ever the professional.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.SAMANTHA, 1)
            Global.gui.message("<b>You've gained Affection with Samantha</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Rough")) {
            sprite.undress()
            sprite.blush = 2
            sprite.mood = Emotion.desperate
            Global.gui.message(
                "Samantha's house, a modern, multi-story concrete and steel building with wall-sized windows on a hillside overlooking downtown must have cost several million dollars.<p>" +
                        "How much do escorts actually make?<p>" +
                        "<i>\"Oh I don't own the place, I'm just looking after it for a friend.\"</i><p>" +
                        "She enunciates the word 'friend' in a way that clearly communicates the nature of the friendship.<p>" +
                        "<i>\"He's an actor. You've probably seen him in the revengers movies\"</i>She explains, unbuttoning your shirt and scraping it off your shoulders like a layer of paint, her fingernails biting into your chest and arms as the garment yields, falling to the floor behind you.<p>" +
                        "When you wince, Samantha chuckles lightly.<p>" +
                        "<i>\"I do enjoy the cute little sounds you make when I fuck you.\"</i>she taunts, moving her hands to your pants.<p>" +
                        "Bold talk indeed from someone who is about to moan as loud and uncontrollably as she is.<p>" +
                        "Samantha raises a delighted eyebrow at you as you strip her of her shirt and bra, her delicate fingers tickling your scrotum as her palm rubs the underside of your swollen hard-on.<p>" +
                        "<i>\"YOU make ME Moan?! Well, I guess we'll just have to see which of us comes out of this upright and who can't do anything but lie there and whimper.\"</i><p>" +
                        "She punctuates her challenge with expert stroking of your cock before wrapping her legs around your waist and mounting you while you are still standing. As her warm sex envelops your shaft, she pulls you close. Wrapping her arms around your neck and cradling the back of your head as she begins to work herself up and down, fucking you as you fight to keep your knees from buckling in euphoric bliss.<p>" +
                        "Unable to remain balanced as Samantha hastens her thrusting, you stumble backwards, encountering a wall and leaning against it for support. Now effectively pinned to the wall, all you can think to do is dig your hands hungrily into the soft flesh of her thighs in an effort to support some of her weight as she rides you toward climax, pistoning your cock while you stand helplessly pinned to the wall, unable to affect her pace or rhythm. You know all too well from your experience with her in the games that if you let this continue much longer, you will lose all coherent thought as Samantha works you like a master mechanic dismantling a go-cart.<p>" +
                        "You attack her chest with your lips, working your tongue along her collarbone and up her neck toward her ear. This slows her, and brings goosebumps to her skin as she leans back to give you access, her confident bearing shaken. You make use of her waning initiative to roll sideways, pinning her into the wall and kissing her passionately as you thrust yourself deep inside her.<p>" +
                        "Samantha grunts euphorically into your mouth and returns the kiss, her arms relaxing around your neck, and you know the initiative is fully now yours. You waste no time, moving your hands from her thighs to the undersides of her knees, and disentangling her from your waist as you begin to fuck her in earnest, holding her fast against the wall as you thrust.<p>" +
                        "Samantha acquiesces fully to your control, allowing you to lift one of her knees over your shoulder and drop her other to the floor. She struggles to stand on one tip-toe in a humiliating, near-full split, as you continue to vigorously plow her soggy, exposed cunt, working her clit with one hand and forcing her where you were only a moment before, to the brink of orgasm.<p>" +
                        "When you feel her climax begin, you stop thrusting, and viciously pinch her rock-hard nipple between your thumb and index finger, robbing her of the orgasm and eliciting a moan that is one part agony, one part despondency and five parts ecstasy, then stepping back, you release her, and Samantha drops to her hands and knees on the floor, her perfect body shuddering with lust and want.<p>" +
                        "Maybe if she begged you, you'd give her the fucking she's obviously so desperate for.<p>" +
                        "<i>\"Uh, yes please. Whatever you want PLEASE FUCK ME!\"</i><p>" +
                        "You stand over her for a moment to relish the sight, and then take her from behind, jerking her hips sloppily into doggy-style fucking position against your groin. Pushing back against you in desperation, Samantha is like a different person. Her demure and manners, her regal veneer, her unflappable poise, all gone, and all that's left is a shameless slut with no greater yearning than the rapture of your cock.<p>" +
                        "You give it to her, parting her vulva with your thick meat, and fucking her like a bitch, one hand tangled into into her long silky hair, forcing her head skyward as she grunts and moans in pleasure. With your other hand you alternatively clutch her hips for leverage and spank her brightly on the ass.  The immodest slapping and smacking sounds echoing off the exposed concrete walls to a chorus of Samantha's heinous vocalizations.<p>" +
                        "It doesn't take long for Samantha to come to a throbbing climax around your thick cock. Succulent cum oozing from her crotch in long syrupy beads toward the floor. You expected a little more hang-time from her, you aren't even close yet.<p>" +
                        "Samantha blushes at your mocking taunt, but has no snappy retort, or clever comeback to deliver. Well, you're happy enough to give her snatch a break, but you're not done fucking her yet.<p>" +
                        "She gasps as you line your cock-head up against her asshole, but makes no move to resist, reaching back instead to caress your heavy ball-sack as you penetrate her ass, working your way quickly into a steady rhythm that Samantha seems to enjoy every bit as much as you do. You release her hair and grab her with both hands, penetrating her deeply with each thrust and enjoying the sight of the elegant musculature of her back, the way her creamy skin sets off the brilliant tone of her hair, and of course how her shapely ass envelopes your cock and shudders and jiggles with the force of your fucking.<p>" +
                        "When you feel your own orgasm begin to swell and churn within you, you grab Samantha by the hair and pull her up to your chest, and fondling her breasts with one hand and her hard, perl-like clit with your other, you lustily throb your load deep into her rectum.<p>" +
                        "Samantha moans and locks lips with you in a passionate kiss and cums again, this time into the palm of your hand. Then she collapses forward into semi-conscious bliss on the floor below you. You consider cuddling up with her there, but given the banter of your foreplay, you decide the cooler move would be to let her lie there and whimper, so you grab your clothes, and tell her you'll see her in the games.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.SAMANTHA, 1)
            Global.gui.message("<b>You've gained Affection with Samantha</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.equals("Strip Poker", ignoreCase = true)) {
            sprite.undress()
            sprite.blush = 2
            SceneManager.play(SceneFlag.SamanthaPoker)
            Roster.gainAffection(ID.PLAYER, ID.SAMANTHA, 1)
            Global.gui.message("<b>You've gained Affection with Samantha</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Practice Fight")) {
            val fight = Combat(player, Roster[ID.SAMANTHA], listOf(Modifier.practice))
            fight.setParent(this)
            fight.go()
        } else if (choice.startsWith("PostCombat")) {
            Global.gui
                .message("With flushed faces and lingering pleasure, you clean up after your practice fight and get dressed.")
            player.rest()
            Roster[ID.SAMANTHA].rest()
            Global.gui.choose(this, "Leave")
        } else if (choice === "Leave") {
            Global.gui.showNone()
            done(acted)
        }
    }

    override fun shop(npc: Character, budget: Int) {
        Roster.gainAffection(npc.id, ID.SAMANTHA, 1)
    }
}

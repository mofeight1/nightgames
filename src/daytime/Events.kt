package daytime

import characters.Character
import global.Global
import scenes.AngelEvent
import scenes.Event
import scenes.MaraEvent

class Events(player: Character, day: Daytime) {
    private val available = ArrayList<HashMap<String, Int>>()
    private val scenes = ArrayList<Event>()

    init {
        scenes.add(Threesomes(player))
        scenes.add(MaraEvent(player))
        scenes.add(AngelEvent(player))
        scenes.add(MorningEvents(player, day))

        for (i in scenes.indices) {
            available.add(HashMap())
            scenes[i].addAvailable(available[i])
        }
    }

    fun checkMorning(): Boolean {
        var mand: String
        for (events in scenes) {
            mand = events.morning()
            if (mand !== "") {
                return events.play(mand)
            }
        }
        return false
    }

    fun checkScenes(): Boolean {
        var mand: String
        for (i in scenes.indices) {
            mand = scenes[i].mandatory()
            if (mand !== "") {
                scenes[1].play(mand)
                return true
            }
        }
        available.clear()
        for (i in scenes.indices) {
            available.add(HashMap())
            scenes[i].addAvailable(available[i])
        }
        for (i in scenes.indices) {
            for (scene in available[i].keys) {
                if (Global.random(100) < available[i][scene]!!) {
                    return scenes[i].play(scene)
                }
            }
        }
        return false
    }
}

package daytime

import characters.Attribute
import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import combat.Combat
import global.Flag
import global.Global
import global.Modifier
import global.Roster
import items.Component
import items.Consumable
import items.Flask

class YuiTime(player: Character) : Activity("Yui", player) {
    private var acted = false
    private val sprite = Dummy("Yui")

    override fun known(): Boolean {
        return Global.checkFlag(Flag.YuiLoyalty)
    }

    override fun visit(choice: String) {
        Global.gui.clearText()
        Global.gui.clearCommand()
        sprite.dress()
        sprite.blush = 1
        sprite.mood = Emotion.confident
        if (choice === "Start") {
            acted = false
            if (Global.checkFlag(Flag.YuiUnlocking) && !Global.checkFlag(Flag.Yui)) {
                Global.gui.message(
                    "You head to Yui's hideout, but she is nowhere to be found. Since she doesn't have a phone, you have no way to contact "
                            + "her. The girl is frustratingly elusive when you actually need to talk to her. No wonder Maya and Aesop were having so much trouble trying "
                            + "to invite her to the Games."
                )
            }
            if (Global.checkFlag(Flag.YuiAvailable)) {
                Global.gui.displayImage("chibi/yui greet.png", "Art by AimlessArt")
                Global.gui.loadPortrait(player, sprite)
                Global.gui.message(
                    "Yui greets you happily when you arrive at her hideout/shed. <i>\"Master! It's good to see you. I've collected some items "
                            + "you may find useful.\"</i> She grins and hands you the items proudly. She's acting so much like an excited puppy that you end up patting "
                            + "her on the head. She blushes, but seems to like it. <i>\"Would you like to spend some time training? Or is there something else I can do "
                            + "to help you?\"</i><br>"
                )
                if (Global.checkFlag(Flag.Yui)) {
                    Global.gui.message(
                        "She gives you a shy, but slightly suggestive look. You're getting as much sex as you could possibly hope for each night, "
                                + "but there's no harm in giving your loyal ninja girl some personal attention."
                    )
                } else {
                    Global.gui.message(
                        "Hearing the offer from such an attractive blushing girl stirs up a natural temptation. She'd almost certainly agree to "
                                + "any sexual request you could think of. However, knowing Yui's sincerity and "
                                + "innocence, your conscience won't let you take advantage of her loyalty."
                    )
                }
                Global.gui.choose(this, "Train with Yui")
                if (Global.checkFlag(Flag.Yui)) {
                    Global.gui.choose(this, "Sex Practice", "Yui would probably enjoy some more hands-on practice")
                    if (Roster.getAffection(ID.PLAYER, ID.YUI) >= 16) {
                        Global.gui.choose(
                            this,
                            "Exposure Play",
                            "The campus is still populated this time of day, why not have a little fun?"
                        )
                    }
                    if (Roster.getAffection(ID.PLAYER, ID.YUI) >= 24) {
                        Global.gui.choose(
                            this,
                            "Double Exposure",
                            "Yui handled your streaking challenge without breaking a sweat. Now she has a challenge for you."
                        )
                    }
                }
                Global.gui.choose(this, "Practice Fight", "Have a sex-fight off the record.")
                Global.unflag(Flag.YuiAvailable)
                acted = true
                player.gain(Component.Tripwire)
                player.gain(Component.Rope)
                if (player.getPure(Attribute.Ninjutsu) >= 1) {
                    player.gain(Consumable.needle, 3)
                }
                if (player.getPure(Attribute.Ninjutsu) >= 3) {
                    player.gain(Consumable.smoke, 2)
                }
                if (player.getPure(Attribute.Ninjutsu) >= 18) {
                    player.gain(Flask.Sedative, 1)
                }
            } else {
                Global.gui.message(
                    "You head to Yui's hideout, but find it empty. She must be out doing something. It would be a lot easier to track her down if she "
                            + "had a phone."
                )
            }
            Global.gui.choose(this, "Leave")
        } else if (choice === "Leave") {
            Global.gui.showNone()
            done(acted)
        } else if (choice.startsWith("Train")) {
            if (player.getPure(Attribute.Ninjutsu) >= 5) {
                sprite.blush = 0
                sprite.mood = Emotion.nervous
                Global.gui.loadPortrait(player, sprite)
                Global.gui.message(
                    "You meet up with Yui in a small park off-campus, and find a shady, secluded spot to train.  After warming up, Yui says, <i>\"Master, today I will be teaching you a strong technique.  As you know, it is important to aim your strikes at the opponent's most sensitive and vital areas; such as the testicles, breasts, and solar plexus.  However, those are the areas that your opponent will be guarding most closely.\"</i>  She punctuates this statement by lightly throwing a quick kick at your groin, which you easily block. <p>" +
                            "<i>\"The limiting factor here is usually your opponent's awareness; as long as remain aware of my attacks, you can usually stay ahead of them.\"</i> Again, she demonstrates her point by stepping in with a punch to your midsection, which you block; she then reaches down, trying to grab your balls, but you catch her hand before she gets them in her grip.  She quickly steps forward, trying to get behind you, and she throws a wild slap at your ass, but you sidestep her attack; finally, she throws a snap-kick up between your legs, but you manage to deflect it with your knees. <p>" +
                            "Yui stops her attack with a bow, <i>\"Very good, master.  I will now demonstrate the technique for getting around your opponent's perception.  It is based on overwhelming them with sheer numbers to flood their senses and find your opening…\"</i> as she trails off, she begins to blur, and your eyes are having trouble focusing on her.  In a flash, you find yourself surrounded by four shadowy copies of Yui, and they quickly surround you.  Moving in uncanny unison, the four of them advance on you, and you whirl around in circles, unsure which direction the attack will be coming from.<p>" +
                            "It is impossible to keep track of all four of them at once, so as your attention is pulled in all directions, one of the clones catches you with a strong punch deep in your gut, knocking the wind out of you.  You bend forward, clutching your abdomen and trying to catch your breath, when you feel a hand reach up between your legs and grab your balls from behind.  You barely register the grab before you feel the small fingers begin to constrict around your testicles, painfully squeezing them against each other.  You barely manage to wiggle your nuts of the grip when you feel an open palm smack you on the ass, causing you to involuntarily jerk your body upright – unfortunately, the final clone is standing right in front of you, giving her the exact opportunity she needed for a swift front kick right in your family jewels.<p>" +
                            "You drop to the ground with your hands over your battered nuts, still trying to catch your breath.  Yui stands over you, a mixed look of both duty and sympathy on her face.  <i>\"Please forgive me, master, but it is imperative that you experienced this technique for yourself before learning how to execute it.  When you have recovered, I will begin instruction.\"</i><p>" +
                            "You eventually get back to your feet, and Yui shows you the finer points of the technique.  <i>\"Master, simply visualize your opponent's weak spots, visualize exactly how you would attack them, and with the right amount of focus, you won't even need to strike – rather, the opponent's opening will strike itself.\"</i>  After an hour of drills, sparring, and meditation, Yui finally says <i>\"Okay master, you are ready, show me what you learned,\"</i> and she readies her fighting stance. <p>" +
                            "You square yourself up with her, and focus your mind on her vulnerabilities; you visualize her perky breasts, her little pubic mound, and her soft belly.  You begin to envision exactly how you would exploit those weaknesses when you feel your mind split into pieces, each piece focused on a different specific technique.  You blink, and you suddenly find yourself in the presence of three shadowy figures, moving in unison right along with your thoughts – each directed at a different weak point.<p>" +
                            "Yui does not look surprised, but she does appear overwhelmed.  As the clones swarm her, she doesn't know which way to turn, and one of your clones manages to catch her square in the tit with a roundhouse kick.  Yui tries to roll off the hit, but the grimace on her face says that your clone got her good.  Another one of your clones steps in and plants his fist deep into Yui's solar plexus, staggering her as she lets out a weak cough.  The third clone circles around behind her, pulverizing her other tit with a quick uppercut as she turns to protect herself.  Absolutely reeling at this point, Yui desperately covers her battered breasts, leaving her lower body completely unguarded. <p>" +
                            "You decide to repay Yui's earlier kick with one of your own, and punt your foot up between her legs, connecting cleanly with her vulva.  Yui's hands immediately drop down to her crotch, and she falls forwards onto her knees, cupping herself.  In a high, weak voice, she says <i>\"Very good, master,\"</i> as she rolls onto her side to wait out the pain.  You sit down next to Yui and rub her back softly to console her; she seems to appreciate it, but it still takes her a few minutes before she gets back on her feet, and the two of you walk slowly back to campus.<p>"
                )
            } else {
                sprite.blush = 0
                sprite.mood = Emotion.confident
                Global.gui.loadPortrait(player, sprite)
                Global.gui.message(
                    "Yui's skills at subterfuge turn out to be as strong as she claimed. She's also quite a good teacher. Apparently she helped train her "
                            + "younger sister, so she's used to it. Nothing she teaches you is overtly sexual, but you can see some useful applications for the Games."
                )
            }

            player.mod(Attribute.Ninjutsu, 1)

            Roster.gainAffection(ID.PLAYER, ID.YUI, 1)
            Global.gui.message("<b>You gained affection with Yui.</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Sex Practice")) {
            sprite.blush = 3
            sprite.undress()
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "You reach out and stroke Yui's long bangs out of her face and she looks up at you with those wide, green eyes. "
                        + "She's so cute, you can't help but kiss her. She makes a small noise into your mouth, her hands on your chest, then begins "
                        + "to kiss you back. Her tongue moves so fluidly against yours that for a moment you're completely absorbed in the sensation.<p>"
                        + "She pulls away, breathing a little heavy. <i>\"Master...\"</i> Her breath is hot against your lips. <i>\"D-Do you think I "
                        + "could practice my technique on you?\"</i> You ask her what she means and she looks slightly embarrassed. <i>\"I mean...! It's "
                        + "one thing practicing on toys, but I'm finding real human parts to be a whole new experience! I'm so curious to learn more...\"</i> "
                        + "she trails off, looking down, her bangs hiding her eyes again.<p>"
                        + "You present yourself for her use and she smiles excitedly at you and begins to undress you. In a moment's time, you find "
                        + "yourself naked before her and she quickly does the same. You let your hand trail over her pale breasts and down over her "
                        + "milky, taut stomach. She reaches down and begins to stroke your cock, which is still half flaccid. She frowns a little "
                        + "seeing that she hasn't completely won you over, and begins to quicken her pace, her deft fingers sliding up and down and "
                        + "massaging you as you stiffen to her touch. You wrap your hand around her ass and pull her close, kissing her passionately.<p>"
                        + "As she kisses you back, she leads you to sit down, crawling on top of you and pressing her body against yours as your hands "
                        + "stroke her back and you play with her tongue. She breaks the kiss and moves between your legs, her hands still working up "
                        + "and down your dick.<p>"
                        + "She leans in and examines it closely, peering up at you, a blush creeping across her cheeks. <i>\"I'm very excited to try "
                        + "this, Master!\"</i> She leans in, gently kissing the base of your dick and working her way up the shaft. Her lips are soft "
                        + "and warm, and the sensation is slightly ticklish. When she reaches the tip, she parts her lips and gently licks the head. "
                        + "You moan at the sensation. Hearing your response, she enthusiastically takes the head in her mouth and begins to suck on it.<p>"
                        + "You can feel her tongue exploring the sensitive ridges with an urgent curiosity, but after a few moments, her training "
                        + "kicks in and she begins to slide you into her mouth. With one hand, she tries to tuck her bangs behind her ear, but they "
                        + "quickly fall back into her face as her head moves up and down, letting you push her tongue down and slide deeper into "
                        + "her mouth. She's still stroking the base of your cock."
            )
            Global.gui.displayImage("premium/Yui BJ Clean.jpg", "Art by AimlessArt")
            Global.gui.message(
                "She pulls back to catch her breath and smiles up at you hungrily. <i>\"Mmm, you taste so much better than my toys, Master!\"</i> "
                        + "You settle back a little and encourage her to continue. She positions herself between your legs and suddenly you feel her "
                        + "mouth on your balls, her tongue probing them and feeling their shape as she sucks on them. She moves up; you can feel your "
                        + "balls resting on her cleavage as she begins to suck you again, her head moving up and down as she slides you deeper and "
                        + "deeper into her mouth.<p>"
                        + "Despite being unfamiliar with an actual penis, you can tell she is experienced at giving pleasure as the sensations of "
                        + "her tongue are quickly bringing you close to the edge. You reach out and grab her head, holding it gently but firmly as "
                        + "your hips move beneath her, forcing her to suck you deeper. She makes a gulping noise deep in her throat, whimpering in "
                        + "pleasure. With one hand, she massages your balls and cups them close to your body. You can see her hips grinding in "
                        + "arousal as you use her mouth. You groan with desire.<p>"
                        + "When she feels your cock tensing up and twitching as it approaches orgasm, her fingers grip the base and stroking "
                        + "fast as she pulls back to avoid choking. You're already cumming before she even gets her mouth off you, and creamy "
                        + "hot semen spills across her tongue and over her lips. She makes a small noise of delight as you continue to spurt your "
                        + "load onto her pale, pretty face.<p>"
                        + "Yui licks her lips and wipes your cum off her cheek, sucking on her finger and beaming at you. <i>\"Wow, you came so "
                        + "quickly, Master! I must have done a good job!\"</i> You pet her head as she dutifully cleans up your cock with her mouth, "
                        + "looking a little shy but very content as well.<p>"
                        + "You tell her that she did a good job, but that practice never hurts. She grabs your hand and looks at you with a "
                        + "serious expression.<p>"
                        + "<i>\"Then promise you'll come back, Master! To help me again!\"</i>"
            )
            Roster.gainAffection(ID.PLAYER, ID.YUI, 2)
            Global.gui.message("<b>You gained affection with Yui.</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Exposure Play")) {
            sprite.blush = 3
            sprite.undress()
            sprite.mood = Emotion.nervous
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "You decide to have some fun with Yui. She seems eager to follow your orders, so perhaps you should order her to do "
                        + "something exciting.<p>"
                        + "You instruct her to undress so you can help her train for the Games. Her cheeks redden, but she grins eagerly as she strips naked. "
                        + "She's still embarrassed to show you her slim body and perky breasts, but she's obviously hoping for some intimate attention. "
                        + "However, she's going to need to be patient.<p>"
                        + "While she stands there, naked and blushing, you give her a brief lecture about the importance of stealth during the Games. Her "
                        + "ninja training has surely given her a strong understanding of concealment, but it focused heavily on camouflage and clothing. "
                        + "In the Games, she's most vulnerable when she's naked. Her fair skin will stand out more at night, and an attractive naked girl "
                        + "tends to draw the eye.<p>"
                        + "Yui squirms with embarrassment as you draw more attention to her bare body, but you're just getting started. In order to help her "
                        + "traverse the campus while naked, you're going to have her practice it during the day.<p>"
                        + "<i>\"Master!?\"</i> Her eyes widen with panic at the idea of streaking across a populated campus. Being seen naked by her fellow "
                        + "competitors doesn't really compare to being seen naked by random students. However, you have faith that her ninjutsu training "
                        + "should let her remain unseen as long as she's careful. You'll wait for her behind the football field bleachers, and give her a "
                        + "nice reward if she can make it to you.<p>"
                        + "Yui's face grows even redder as she considers the task. The football field is a considerable distance away, and she'll have to "
                        + "pass several academic buildings on the way. While the field itself should be empty right now, the areas in between will probably "
                        + "be well populated. However, the promised reward definitely has her intrigued.<p>"
                        + "After a long hesitation, she quietly nods. You pack her clothes into your backpack, give her a quick kiss for luck, and head for "
                        + "the football field.<p>"
                        + "...<p>"
                        + "It takes you about ten minutes to reach the football field, but of course you don't have to worry about being noticed. It'll "
                        + "probably take Yui much longer. There's a decent amount of foot traffic along the way as expected.<p>"
                        + "However, as you approach the field, you realize there's a problem. You expected the field to be unoccupied, but a group of "
                        + "students are there, playing ultimate frisbee. Even if Yui can get across campus without getting spotted, you don't see a way "
                        + "for her to approach the bleachers without being clearly visible to the frisbee players.<p>"
                        + "Yui is probably loyal enough to try to complete her task despite the risk. You need to intercept her along the way and pick "
                        + "a different meeting place. Unfortunately, you don't know what route she's taking. She's also being as stealthy as she can, so "
                        + "even if you guess right, you probably won't be able to spot her. Is there a safe place you can wait for her, where she's "
                        + "guaranteed to notice you?<p>"
                        + "<i>\"Master, over here!\"</i> While you're considering your options, you hear Yui's hushed voice from behind the nearby "
                        + "bleachers. You approach and find her still naked, trembling with nerves. Apparently, your concerns were unfounded. She even "
                        + "got here much faster than you imagined.<p>"
                        + "Yui practically leaps into your arms as you approach. The trip here was clearly a little overwhelming, but as you slip a hand "
                        + "between her legs, you find her completely drenched. She lets out a moan at your touch and you have to warn her to stay quiet. "
                        + "The ultimate frisbee players probably won't see the two of you here, but they're probably close enough to hear her if she's too "
                        + "loud. You had hoped for a more private rendezvous, but Yui went above and beyond the challenge you gave her. She clearly deserves "
                        + "her reward.<p>"
                        + "You press her against the nearby wall and kiss her passionately. She practically melts in your arms, and softly whimpers "
                        + "approvingly. You kneel on the ground, leaving a trail of kisses down the front of her nude body. She covers her mouth to stifle "
                        + "her moans as you reach her wet flower and lavish it with attention.<p>"
                        + "You lap up her juices, exploring her lower lips with your tongue. She's extremely sensitive and responsive. It's a lot of fun "
                        + "making her squirm with pleasure. You decide to turn up the intensity a bit by focusing on her little love bud. A high pitched "
                        + "moan escapes her hands, and you wait for a few tense seconds to see if anyone heard it.<p>"
                        + "When you're confident your activities have gone unnoticed, Yui gives you a pleading look. <i>\"Please, Master. I love your tongue, "
                        + "but I need you inside!\"</i><p>"
                        + "This situation is hot enough that you're happy to comply. You free your stiff dick from your pants while Yui turns to face the "
                        + "bleachers, sticking her ass out towards you. You take a moment to line up your cock with her slick pussy, then thrust deep into "
                        + "her hot core. This time you have to cover her mouth to stifle her moan. She appeared to orgasm immediately, and her own hands are "
                        + "occupied trying to hold herself up."
            )
            Global.gui.displayImage("premium/Yui Public.jpg", "Art by AimlessArt")
            Global.gui.message(
                "Without waiting for her to recover, you fuck her passionately from behind. You both need to be careful not to make too much noise, "
                        + "but the risk of discovery just makes it more exciting. You keep your left hand over Yui's mouth, while your right hand slips down "
                        + "to tease her clit. Her body trembles uncontrollably, not calming at all since her first orgasm.<p>"
                        + "It's not long before you feel your own climax approach. Right as you shoot your load inside Yui, you lean in and suck on her neck "
                        + "hard enough to leave a hickey. Judging by her twitching body and barely stifled scream, she probably came again.<p>"
                        + "You weren't as quiet as you should have been, but fortunately no one seems to have noticed you. You return Yui's clothes, and she "
                        + "gratefully gets dressed. The load of cum you left inside her will probably stain her panties, but if she minds, she sure doesn't "
                        + "show it. She gives you a smile full of adoration and embraces you tightly.<p>"
                        + "<i>\"So, did I do a good job, Master?\"</i> In lieu of a response, you give her a tender kiss."
            )
            Roster.gainAffection(ID.PLAYER, ID.YUI, 2)
            Global.gui.message("<b>You gained affection with Yui.</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Double Exposure")) {
            sprite.blush = 3
            sprite.undress()
            sprite.mood = Emotion.confident
            Global.gui.loadPortrait(player, sprite)
            Global.gui.message(
                "You decide that it is time to challenge Yui's stealth abilities once again with a bit tougher challenge.  She succeeded at the last challenge much better than you had expected, but you are still concerned that she will stand out too easily at night against the more practice challengers.  So you once again instruct her to strip down and tell her what you are planning.  You see her blushing along her whole body as you explain to her that you expect her to not only streak across campus, but to actually leave the grounds as well.  You set the goal point at the middle of the park, about a half mile from campus.<p>" +
                        "<i>\"Master,\"</i> she speaks up when you finish your explanation, <i>\"I enjoy these challenges you give me, but am concerned that you are neglecting your own training as you try to help me.  I think it would be best if you would join your kunoichi in this challenge, then it can be a race.\"</i>  You look at her, and can see the genuine concern in her eyes.  You are reluctant to join her, you aren't sure about streaking through the town in broad daylight.  But she looks to you as her master, and how can a good master ask their pupil to do something that they themselves won't do?  Finally you realize that you have to agree to her request.  You strip off your clothes and add them to the pile with hers.<p>" +
                        "Once you are naked you again feel reluctant about taking on Yui's request.  But one look at her face changes your mind as you can tell she is thrilled that you agreed to join her on this challenge.  She is smiling at you and seems to be standing much more comfortably despite her nakedness.  You also begin to feel more comfortable and move over to stand next to her.<p>" +
                        "<i>\"First one to the finish gets a reward?\"</i> she asks expectantly.  You nod your head and you both take your places.  You yell go and the race begins.  Yui takes off like a flash, you almost miss her naked butt moving off into the distance.  You dash to the first cover that you can find, behind a dumpster in the distance.  You take a quick look from behind the dumpster and see that the coast is clear.  You identify a rock in the distance that will work and speed over and dive behind it.  You manage to continue this cycle all the way to the edge of campus without seeing or being spotted by another human being. <p>" +
                        "As you plot your next dash, you can hear the sounds of talking coming your way.  You duck down behind the tree that you are currently hiding behind and hold your breath.  You hope and hope that they will pass quickly both because you don't want to get caught and you are sure that Yui is well ahead.  You never thought that you had a chance of beating her to the finish line, but you also don't want to embarrass yourself in front of her by being very far behind.  Finally the voices begin to fade and you are able to make your next dash to cover.<p>" +
                        "Over the next half mile, you face only one more challenge.  As you approach the edge of the park, you are forced to face a stretch of road between you and the goal.  It's not a super busy road, but it does get a fair amount of traffic and you aren't sure it will ever be completely clear.  You watch a few cars go by as you contemplate the frequency as well as how far you think the cars need to be from you to minimize the chance you will be seen clearly.  Finally you decide that you just need to go for it and prepare to run full out.  The next car passes and you count to 5 before you take off.  As you are running you see brake lights on the car that had just passed come on.  However you are safely into the trees of the park before you can figure out if it was because they were stopping to watch you or because of some other reason.<p>" +
                        "You fairly easily make it to the center of the park where you set the goal to be.  Most of the park is wooded and you are able to steer clear of the walking paths through the trees.  You arrive expecting to find Yui standing there waiting for you, but she is nowhere to be seen.  You stand there looking around in all directions, starting to worry that something had happened to Yui during her trip, sure that she had beaten you there considering her quicker speed.  Suddenly something hits your shoulders from above.<p>" +
                        "<i>\"Master!\"</i> You hear her cry above you.  <i>\"You finally made it!\"</i>  <p>" +
                        "You look up and see her looking down at you with a big smile.  She shifts quickly and drops off your shoulders to the ground in front of you.<p>" +
                        "<i>\"I was worried you had gotten into trouble,\"</i> she says once she is facing you.  Then she looks puzzled. <i>\"Aren't you excited to see your little kunoichi?\"</i>  You realize she is looking down and when you follow her gaze you see that she is staring at your completely flaccid cock.  The nervousness of streaking across campus and part of town in broad daylight had kept you soft, which is concerning her now as she thinks there is a problem.  But now that you are relatively safe, and she is focused on your crotch, it begins to spring to life and when you look up you see a relieved smile returning to Yui's face.<p>" +
                        "<i>\"Oh good,\"</i> she says, <i>\"I was afraid you wouldn't be able to give me my reward.\"</i>  You pull Yui close, trapping your hardening cock between the two of you.  You lift her chin up and lean down, giving her a long, deep kiss.  She moans gently against you as your tongue pushes between her lips and into her mouth.  You let your hands travel down her back and grab her ass, which causes her to squirm against you.  Eventually you break the kiss so the two of you can get some air.<p>" +
                        "<i>\"That was nice master,\"</i> Yui says between breaths, <i>\"but I think you know that's not what I was hoping for.\"</i>  You smile and push her away slightly so that your hand can travel down between her legs.  You begin to toy with her lower lips and she begins to coo.  <i>\"Master, your fingers are so skilled, but I need something else inside me now.\"</i> You know what she wants, and have reached a point where you don't want to toy with her any longer either.  You pick up the small framed kunoichi in front of you and in the most fluid motion you can manage, impale her on your hard cock.  She groans as you enter her quickly.  You bottom out and immediately push her back, until you almost pop free.  Then you let her go so she slides back down your pole.  You keep up the pace, moving her back and forth on your shaft as she starts to get louder and louder.  <p>" +
                        "You cover her mouth and she looks down at you.  You shake your head to indicate she needs to be quiet and she nods in understanding.  You take your hand away and grab her ass with both hands, pushing her up and down on you with all the force you can muster.  You are both getting close, but since this is her reward you want to make sure she comes first.  You quickly pull her off of you, leaving your wet cock waving in the cool air, and pull her up to your shoulders.  You position her so she is sitting on your shoulders as your tongue invades her pussy.  You tongue fuck her furiously for a number of strokes, then pull out and find her clit.  She immediately goes over the edge with the first licks but you continue letting your tongue play with her clit through her first orgasm. <p>" +
                        "Once she is done you begin nibbling on her clit, trying to bring her close to orgasm again as quickly as possible.  She doesn't disappoint and in no time she is trying to wiggle so that something, anything moves back inside of her.  You decide not to leave her waiting anymore and lower her back down so you can impale her again.  This time though you move over to a big rock that is just to the side so that you can lean her back against it.  Once she is positioned, you let go of her and focus on pounding in and out of her.  She is starting to get loud again but you don't care at this point, the thrill of potentially getting caught just drives you harder.  Suddenly you feel her begin to squeeze your length and that sends you over the edge.  You slump forward, trapping her against the rock and on your cock as you fill her with your cum.  <p>" +
                        "You rest there for a bit, eventually letting Yui down to the ground with a small kiss on the lips.  She smiles at you and then disappears into the trees.<p>"
            )
            Roster.gainAffection(ID.PLAYER, ID.YUI, 2)
            Global.gui.message("<b>You gained affection with Yui.</b>")
            Global.gui.choose(this, "Leave")
        } else if (choice.startsWith("Practice Fight")) {
            val fight = Combat(player, Roster[ID.YUI], listOf(Modifier.practice))
            fight.setParent(this)
            fight.go()
        } else if (choice.startsWith("PostCombat")) {
            Global.gui
                .message("With flushed faces and lingering pleasure, you clean up after your practice fight and get dressed.")
            player.rest()
            Roster[ID.EVE].rest()
            Global.gui.choose(this, "Leave")
        }
    }

    override fun shop(npc: Character, budget: Int) {
        npc.gain(Consumable.needle, 3)
        npc.gain(Consumable.smoke, 2)
        npc.gain(Flask.Sedative, 1)
    }
}

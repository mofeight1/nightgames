package skills

import characters.Character
import combat.Combat
import combat.Result

class Leverage(user: Character) : Skill("Leverage", user) {
    override fun requirements(user: Character): Boolean {
        return false
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return false
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
    }

    override fun copy(user: Character): Skill {
        return Leverage(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

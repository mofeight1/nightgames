package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Beastform
import status.Stsflag

class BeastTF(user: Character) : Skill("Beast Transform", user) {
    init {
        addTag(Attribute.Animism)
        addTag(SkillTag.BUFF)
        addTag(SkillTag.BEAST)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Animism) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Animism to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.canSpend(Pool.MOJO, 30) &&
                user.has(Stsflag.feral) &&
                !user.has(Stsflag.beastform)
    }

    override fun describe(): String {
        return "Unleash your animal side: 30 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(Beastform(user), c)
    }

    override fun copy(user: Character): Skill {
        return BeastTF(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ("You physically manifest your animal spirit, growing fur, ears, and a tail. "
                + "You feel the bestial power warp your muscles, making you faster and stronger.")
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return (user.name + " lets out an animal roar and her bestial features become much more pronounced. Fur rapidly grows on her limbs, "
                + "her teeth sharpen, and she crouches like a predator.")
    }
}

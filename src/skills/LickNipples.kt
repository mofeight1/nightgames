package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class LickNipples(user: Character) : Skill("Lick Nipples", user) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isTopless &&
                c.stance.reachTop(user) &&
                !c.stance.behind(user) &&
                user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target)
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Cassie")) {
                c.offerImage("LickNipples.jpg", "Art by Fujin Hitokiri")
            }
        }
        var m: Double = Global.random(4) + user[Attribute.Seduction] / 4.0 + target[Attribute.Perception] / 2.0
        m = user.bonusProficiency(Anatomy.mouth, m)
        target.pleasure(m, Anatomy.chest, combat = c)
        user.buildMojo(10)
        target.emote(Emotion.horny, 5)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 14 && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 14)
    }

    override fun copy(user: Character): Skill {
        return LickNipples(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You go after " + target.name + "'s nipples, but she pushes you away."
        } else {
            if (target.has(Trait.petite) && Global.random(3) == 0) {
                if (target.arousal.percent() > 75) {
                    target.name + " groans as your lips and tongue aggressively pleasure her erect nipples. " +
                            "Closing her eyes for a moment, she pushes her small breasts into your face to increase the pressure from your tongue."
                } else {
                    "Pulling her small frame closer to you, you take one of " + target.name + "'s nipples into your mouth and lick all around it, enjoying the perkiness of her petite breasts."
                }
            } else if (target.arousal.percent() > 75 && Global.random(2) == 0) {
                "You run your tongue roughly across " + target.name + "'s nipples, although by this point it feels like your input is hardly needed as her juices are flowing freely and she " +
                        "seems to be verge of giving up and just fingering herself."
            } else when (Global.random(4)) {
                3 -> "You pull each of " + target.name + "'s nipples a little closer together and furiously run your tongue all over them."
                0 -> "You gently rub the tip of your tongue up and down each of " + target.name + "'s nipples, eliciting a small moan."
                1 -> "You place your lips around one of " + target.name + "'s nipples and attack it with your tongue. She shivers in pleasure and you feel her nipple stiffening in your mouth."
                else -> "You slowly circle your tongue around each of " + target.name + "'s nipples, making her moan and squirm in pleasure."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " tries to suck on your chest, but you avoid her."
        } else {
            user.name + " licks and sucks your nipples, sending a surge of excitement straight to your groin."
        }
    }

    override fun describe(): String {
        return "Suck your opponent's nipples"
    }
}

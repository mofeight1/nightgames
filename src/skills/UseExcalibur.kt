package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Global
import items.Attachment
import items.Flask
import items.Toy
import status.Stsflag

class UseExcalibur(user: Character) : Skill(Toy.Excalibur.getName(), user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.has(Toy.Excalibur) &&
                user.canAct() &&
                target.hasPussy &&
                c.stance.reachBottom(user) &&
                (target.isPantsless || (user.has(Attachment.ExcaliburScience) && user.has(Flask.DisSol))) &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target) && !user.has(Attachment.ExcaliburNinjutsu)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m: Double = 5 + (target[Attribute.Perception] / 2.0)
        var level = 1
        if (user.has(Attachment.Excalibur2)) {
            level++
            m += 1 + Global.random(6)
        }
        if (user.has(Attachment.Excalibur3)) {
            level++
            m *= 2
        }
        if (user.has(Attachment.Excalibur4)) {
            level++
            m *= 1.5
        }
        if (user.has(Attachment.Excalibur5)) {
            level++
            m *= 2
        }
        m += (user[Attribute.Science] / 2)
        if (user.has(Attachment.ExcaliburNinjutsu)) {
            writeOutput(c, target, Result.upgrade, 0)
        }
        if (user.has(Attachment.ExcaliburScience) && !target.isPantsless && user.has(Flask.DisSol)) {
            user.consume(Flask.DisSol, 1)

            writeOutput(c, target, Result.upgrade, 1)
            target.shred(Character.OUTFITBOTTOM)
        }
        writeOutput(c, target, Result.normal, level)
        if (user.has(Attachment.ExcaliburFetish)) {
            m *= 1 + user.arousal.percent() / 100
            writeOutput(c, target, Result.upgrade, 2)
        }
        if (user.has(Attachment.ExcaliburDark) && target.has(Stsflag.horny)) {
            m *= 1 + (target.getStatusMagnitude("Horny") / 10)
            writeOutput(c, target, Result.upgrade, 3)
        }
        if (user.has(Attachment.ExcaliburAnimism) && user.has(Stsflag.feral)) {
            m *= 1.5
            if (user.has(Stsflag.beastform)) {
                m *= 1.5
            }
            writeOutput(c, target, Result.upgrade, 4)
        }
        if (user.has(Attachment.ExcaliburArcane)) {
            writeOutput(c, target, Result.upgrade, 5)
            target.spendMojo(10)
            user.buildMojo(30)
        }
        if (user.has(Attachment.ExcaliburKi)) {
            writeOutput(c, target, Result.upgrade, 6)
            user.heal(10.0, c)
        }
        m = user.bonusProficiency(Anatomy.toy, m)
        target.pleasure(m, Anatomy.genitals, combat = c)
    }

    override fun copy(user: Character): Skill {
        return UseExcalibur(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You thrust Sexcalibur at ${target.name}'s groin, but she blocks it."
            }
            Result.upgrade -> {
                when (damage) {
                    0 ->                //Concealed Holster
                        "In one quick motion, you draw Sexcalibur from your concealed holster and attack, catching ${target.name} by surprise"

                    1 ->                //Dissolver
                        "On contact with her ${target.bottoms.last()}, the head sprays enough dissolving solution to quickly melt through the garment."

                    2 ->                //Tentacles
                        "Small tentacles uncoil from the shaft and attack her sensitive skin."

                    3 ->                //Lust Curse
                        "You feel the cursed sex toy response to her horny thoughts and grow more powerful."

                    4 ->                //Furry
                        "The soft fur on the vibrating head grows into a bushy tail that caresses her privates, " +
                            "guided by the animal spirit possessing it."

                    5 ->                //Enchantment
                        "The enchanted rod glows slightly as it pulls ${target.name}'s mana into you."

                    6 ->                //Ki
                        "Simply wielding the two improves your Ki flow and restores your stamina."

                    else -> ""
                }
            }
            else -> {
                when (damage) {
                    2 ->                //Grand
                        "You touch your Grand Sexcalibur to ${target.name}'s bare slit, and she whimpers with pleasure at the intense vibration."

                    3 ->                //Masterwork
                        "You attack ${target.name}'s pussy with your Masterwork Sexcalibur, dealing intense pleasure with the finely crafted sex toy. " +
                            "She lets out an audible moan at the sensation."

                    4 ->                //Legendary
                        "You thrust the Legendary Sexcaibur between ${target.name}'s nethers, dealing a critical hit of pleasure to her " +
                            "pussy and clit."

                    5 ->                //Perfect
                        "You touch the vibrating head of your Perfect Sexcalibur to ${target.name}'s vulva. She gasps at the unfathomable pleasure " +
                            "and her knees buckle."

                    else -> "You press Sexcalibur's vibrating head against ${target.name}'s clit, causing her to flinch and let out a yelp of " +
                        "pleasure."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun describe(): String {
        return String.format("Pleasure opponent with your %s", Toy.Excalibur.getFullName(user))
    }
}

package skills

import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Neutral
import status.Braced
import status.Stsflag

class Recover(user: Character) : Skill("Recover", user) {
    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.prone(user) && c.stance.mobile(user) && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        if (c.stance.prone(user) && !user.has(Stsflag.braced)) {
            user.add(Braced(user))
        }
        c.stance = Neutral(user, target)
        if (user.has(Trait.determined)) {
            user.calm(user.arousal.max / 4.0, c)
            user.heal(user.stamina.max / 4.0, c)
        } else {
            user.heal(Global.random(3).toDouble(), c)
        }
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Recover(user)
    }

    override var speed = 0

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You pull yourself up, taking a deep breath to restore your focus."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " scrambles back to her feet."
    }

    override fun describe(): String {
        return "Stand up"
    }
}

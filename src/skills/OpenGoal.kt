package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import status.Stsflag

class OpenGoal(user: Character) : Skill("Open Goal", user) {
    init {
        addTag(Attribute.Footballer)
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.feet(user) &&
                user.canAct() &&
                !user.has(Trait.sportsmanship) &&
                (!c.stance.prone(user) || user.has(Trait.dirtyfighter)) &&
                !c.stance.penetration(user) &&
                target.has(Stsflag.masochism) && target.has(Stsflag.horny)
    }

    override fun describe(): String {
        return "Convince a horny, masochistic opponent to let you finish them off with a hard kick to the groin."
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        target.pain(user[Attribute.Footballer] * 10.0, Anatomy.genitals, c)
    }

    override fun copy(user: Character): Skill {
        return OpenGoal(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override var speed = 2

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You lean in to " + target.name + "'s ear and suggest that you'll get too turned on to fight back if you can just kick her in the groin. " +
                "In her lust-addled state, she can't resist temptation and spreads her legs for you. You take a step back to get " +
                "the right distance, before giving her your best kick to the crotch. Her eyes roll back in her head as she " +
                "lets out a quiet noise between a moan and a whimper."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " gives you a naughty smile. <i>\"Come on, baby. Let me give your big balls my best kick. " +
                "It'll get me so hot, I'll probably lose on the spot.\"</i> Your overwhelming lust and masochism " +
                "agree with her suggestion, while the dissenting voice of reason in your head is strangely quiet. " +
                user.name + " winds back and gives you a devastating punt in the groin. You howl in agony, doubling over " +
                "and cupping your throbbing testicles."
    }
}

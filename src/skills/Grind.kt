package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import stance.Stance
import status.ProfMod
import status.ProfessionalMomentum

class Grind(user: Character) : Skill("Grind", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 14
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 14)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(user) &&
                c.stance.penetration(user) &&
                c.stance.en != Stance.anal
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        var m = user.getSexPleasure(1, Attribute.Seduction) + target[Attribute.Perception]
        if (user.has(Trait.strapped)) {
            m += user[Attribute.Science] / 2
            m = user.bonusProficiency(Anatomy.toy, m)
        }
        target.pleasure(m, Anatomy.genitals, combat = c)
        var r = target.getSexPleasure(1, Attribute.Seduction, Anatomy.genitals) / 3
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }

        user.pleasure(r, Anatomy.genitals, combat = c)
        if (user.getPure(Attribute.Professional) >= 11) {
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Sexual Momentum", user, Anatomy.genitals, user[Attribute.Professional] * 5.0), c)
            }
        }
        c.stance.pace = 0
        user.buildMojo(10)
    }

    override fun copy(user: Character): Skill {
        return Grind(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You grind your hips against " + target.name + " without thrusting. She trembles and gasps as the movement stimulates her clit and the walls of her pussy."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " grinds against you, stimulating your entire manhood and bringing you closer to climax."
    }

    override fun describe(): String {
        return "Grind against your opponent with minimal thrusting. Extremely consistent pleasure"
    }
}

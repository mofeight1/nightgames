package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import items.Clothing

class Fabricator(user: Character) : Skill("Fabricator", user) {
    init {
        addTag(Attribute.Science)
        addTag(SkillTag.DRESSING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.sub(user) &&
                user.canSpend(Pool.BATTERY, 7) &&
                (user.isTopless || user.isPantsless) &&
                !c.stance.penetration(user) && !c.stance.penetration(target)
    }

    override fun describe(): String {
        return "3D prints some temporary clothing: 7 Battery"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 7)
        if (user.isPantsless) {
            user.wear(Clothing.fabloincloth)
            writeOutput(c, target, Result.bottom)
        } else {
            user.wear(Clothing.fabwrap)
            writeOutput(c, target, Result.top)
        }
    }

    override fun copy(user: Character): Skill {
        return Fabricator(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.top) {
            "You activate your Material Fabrication Unit to craft a crude chest wrap."
        } else {
            "You activate your Material Fabrication Unit to craft a crude loincloth."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.top) {
            user.name + " activates a strange device, which covers her breasts with a simple fabric wrap."
        } else {
            user.name + " activates a strange device. A primitive cloth loincloth appears around her waist, barely covering her privates."
        }
    }
}

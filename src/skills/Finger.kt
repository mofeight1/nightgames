package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.ProfMod
import status.ProfessionalMomentum
import status.Stsflag

class Finger(user: Character) : Skill("Finger", user) {
    init {
        addTag(Result.touch)
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.genitalsAvailable(c) && target.hasPussy && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = 4.0 + target[Attribute.Perception]
        var type = Result.normal
        if (user[Attribute.Seduction] >= 8) {
            m += Global.random(user[Attribute.Seduction] / 2)
            user.buildMojo(10)
        } else {
            type = Result.weak
        }
        if (user.getPure(Attribute.Professional) >= 3) {
            m += user[Attribute.Professional]
            type = Result.special
            user.buildMojo(15)
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Dexterous Momentum", user, Anatomy.fingers, user[Attribute.Professional] * 5.0), c)
            }
        }
        if (user.has(Stsflag.shadowfingers)) {
            type = Result.critical
        }
        writeOutput(c, target, type)
        m = user.bonusProficiency(Anatomy.fingers, m)
        target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        c.offerImage("Fingering.jpg", "Art by AimlessArt")
        c.offerImage("Fingering2.jpg", "Art by AimlessArt")
        if (user.has(Trait.roughhandling)) {
            target.weaken(m / 2, c)
            writeOutput(c, target, Result.unique)
        }
    }

    override var accuracy = 7

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Finger(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.unique) {
            return "You opportunistically give her clit a sharp pinch."
        }
        return if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                "You try to slide your hand down to " + target.name + "'s pussy, but she bats your hand away."
            } else {
                "You grope at " + target.name + "'s pussy, but miss."
            }
        } else if (target.bottoms.isNotEmpty()) {
            if (modifier == Result.weak) {
                "You fumble as you slip your hand under ${target.name}'s ${target.bottoms.last()}. " +
                        "You manage to get under it and run your fingers across her " +
                        "soft flesh, hoping for a positive response. After some poking and prodding, you withdraw your hand."
            } else {
                if (Global.random(2) == 0) {
                    "You slip your hand under ${target.name}'s ${target.bottoms.last()} and curl a finger inside of her."
                } else {
                    "You slip your hand under ${target.name}'s ${target.bottoms.last()} and stroke her sensitive petals."
                }
            }
        } else if (modifier == Result.critical) {
            String.format(
                "Your shadowy finger-tendrils easily slide between %s's nethers and explore every nook and crannie of her sensitive pussy.",
                target.name
            )
        } else if (modifier == Result.weak) {
            "You grope between " + target.name + "'s legs, not really knowing what you're doing. You don't know where she's the most sensitive, so you rub and " +
                    "stroke every bit of moist flesh under your fingers."
        } else {
            if (target.arousal.current <= 15) {
                "You softly rub the petals of " + target.name + "'s closed flower."
            } else if (target.arousal.percent() < 50) {
                if (Global.random(2) == 0) {
                    ("You are gently stroking up and down " + target.name + "'s sex when you feel your fingertip become wet with her juices. "
                            + "You speed up your movement, coating her in the supplies lubricant.")
                } else {
                    target.name + "'s sensitive lower lips start to open up under your skilled touch and you can feel her becoming wet."
                }
            } else if (target.arousal.percent() < 80) {
                if (Global.random(2) == 0) {
                    "You spread " + target.name + "'s lower lips and tease her wet entrance with another finger."
                } else {
                    "You locate " + target.name + "'s clitoris and caress it directly, causing her to tremble from the powerful stimulation."
                }
            } else {
                if (Global.random(2) == 0) {
                    "Two of your fingers gracefully penetrate " + target.name + " and press against her G-spot, making her gasp. She moans in chorus as your fingers continue their skilled work."
                } else {
                    "You stir " + target.name + "'s increasingly soaked pussy with your fingers and rub her clit with your thumb."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.unique) {
            return String.format(
                "%s gives your poor clit a painful pinch.",
                user.name
            )
        }
        return if (modifier == Result.miss) {
            String.format(
                "%s gropes at your pussy, but misses the mark.",
                user.name
            )
        } else if (modifier == Result.critical) {
            String.format(
                "%s slides her shadow tentacles between your legs. The tendrils delve into your slick hole, overwhelming "
                        + "you with a strange pleasure", user.name
            )
        } else {
            if (target.arousal.current <= 15) {
                String.format(
                    "%s softly rubs your sensitive lower lips. You aren't very aroused yet, but %s gentle touch "
                            + "gives you a ticklish pleasure",
                    user.name, user.possessive(false)
                )
            } else if (target.arousal.percent() < 50) {
                String.format(
                    "%s skillfully fingers your sensitive pussy. You bite your lip and try to ignore the pleasure, "
                            + "but despite your best efforts, you feel yourself growing wet.",
                    user.name
                )
            } else if (target.arousal.percent() < 80) {
                String.format(
                    "%s locates your clitoris and caress it directly, causing"
                            + " you to tremble from the powerful stimulation.",
                    user.name
                )
            } else {
                String.format(
                    "%s stirs your increasingly soaked pussy with %s fingers and "
                            + "rubs your clit directly with %s thumb.",
                    user.name, user.possessive(false), user.possessive(false)
                )
            }
        }
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Professional) >= 3) {
            "Pro Finger"
        } else {
            "Finger"
        }
    }


    override fun describe(): String {
        return if (user.getPure(Attribute.Professional) >= 3) {
            "A professional fingering technique that increases effectiveness with repeated use"
        } else {
            "Digitally stimulate opponent's pussy"
        }
    }
}

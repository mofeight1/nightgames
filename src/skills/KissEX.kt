package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Charmed

class KissEX(user: Character) : Skill("Passionate Kiss", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.kiss)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.kiss(user) && user.canAct() && user.canSpend(Pool.MOJO, 15)
    }

    override fun resolve(c: Combat, target: Character) {
        var m: Double
        var style = Result.seductive
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            style = Result.powerful
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            style = Result.clever
        }
        user.spendMojo(15)
        when (style) {
            Result.powerful -> {
                m = Global.random(4) + user[Attribute.Power] / 4.0
            }
            Result.clever -> {
                m = Global.random(4) + user[Attribute.Cunning] / 4.0
            }
            else -> {
                m = Global.random(4) + user[Attribute.Seduction] / 4.0
                m *= 1.3
            }
        }

        if (user.has(Trait.romantic)) {
            m *= 1.2
        }
        writeOutput(c, target, style)
        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.JEWEL) {
            c.offerImage("Kiss.jpg", "Art by AimlessArt")
        }
        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.CASSIE) {
            c.offerImage("Cassie Kiss.jpg", "Art by AimlessArt")
        }
        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.ANGEL) {
            c.offerImage("Angel Kiss.jpg", "Art by AimlessArt")
        }
        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.SELENE) {
            c.offerImage("Selene Kissed.png", "Art by AimlessArt")
        }

        target.tempt(m, user.bonusTemptation(), mod = Result.foreplay, combat = c)
        target.pleasure(1.0, Anatomy.mouth, combat = c)
        if (user.has(Trait.greatkiss) && user.canSpend(Pool.MOJO, 10) && Global.random(2) == 0) {
            target.add(Charmed(target, 2 + user.bonusCharmDuration()), c)
            user.spendMojo(10)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user[Attribute.Seduction] >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 9)
    }

    override fun copy(user: Character): Skill {
        return KissEX(user)
    }

    override var speed: Int = 6

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.powerful -> {
                String.format(
                    "You aggressively pull %s into an intense kiss. You push your tongue into her mouth before she has a "
                            + "chance to react. By her response, she clearly doesn't mind a little assertiveness.",
                    target.name
                )
            }
            Result.clever -> {
                String.format(
                    "You steal a quick kiss from %s, pulling back before she can respond. As she hesitates in confusion, you kiss her twice more, " +
                            "lingering on the last to run your tongue over her lips.", target.name
                )
            }
            else -> {
                String.format(
                    "You pull %s close for a passionate kiss. She responds eagerly, parting her lips and bringing out her tongue to tangle with yours. "
                            + "Your own tongue proves superior though, and she soon melts into your arms as you explore the sensitive corners of her mouth.",
                    target.name
                )
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.powerful -> {
                String.format(
                    "%s pulls you into an aggressive kiss, forcing her tongue into your mouth. "
                            + "It catches you off guard, but her assertiveness is quite arousing.", user.name
                )
            }
            Result.clever -> {
                String.format(
                    "%s leans in close to capture your lips, a lusty look in her eyes. You instinctively close your eyes "
                            + "to meet the kiss, but it doesn't arrive. Confused, you open your eyes to see her mischievous grin only inches away. "
                            + "She quickly darts in and steals a kiss, catching you by surprise.", user.name
                )
            }
            else -> {
                String.format(
                    "%s licks her lips seductively, before leaning in to claim yours. Despite the feeling that she has the "
                            + "advantage here, you can't resist accepting the kiss eagerly. Her tongue lightly traces your lips, then slips in to "
                            + "explore your mouth. You have a lot of practice kissing, but she's so seductive and talented that you can barely respond.",
                    user.name
                )
            }
        }
    }

    override fun toString(): String {
        return if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            "Forceful Kiss"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            "Steal Kiss"
        } else {
            name
        }
    }

    override fun describe(): String {
        return if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            "Strong kiss using Power: 15 Mojo"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            "Sneaky kiss using Cunning: 15 Mojo"
        } else {
            "Mojo boosted kiss: 15 Mojo"
        }
    }
}

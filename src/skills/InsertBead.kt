package skills

import characters.Character
import combat.Combat
import combat.Result
import items.Toy
import status.Beaded

class InsertBead(user: Character) : Skill("Insert Anal Bead", user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.has(Toy.AnalBeads) &&
                user.canAct() && c.stance.reachBottom(user) && target.isPantsless &&
                !user.human()
    }

    override fun describe(): String {
        return "Insert an anal bead into your opponent's ass"
    }

    override fun resolve(c: Combat, target: Character) {
        if (c.attackRoll(this, user, target)) {
            writeOutput(c, target)
            target.add(Beaded(target, 1), c)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return InsertBead(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You try to insert an anal bead into " + target.name + "'s ass, but she's struggling too much."
        } else {
            "You manage to push an anal bead into " + target.name + "'s butt. You could pull it out now, but it would " +
                    "make a bigger impact if you can get some more in there."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " tries to stick a bead up your ass, but you manage to get away."
        } else {
            user.name + " shoves an anal bead up your ass before you can resist. As uncomfortable as it is, you're sure getting it " +
                    "pulled out will be worse."
        }
    }
}

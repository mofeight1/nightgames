package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.ProfMod
import status.ProfessionalMomentum

class ThrustEX(user: Character) : Skill("Smooth Thrust", user) {
    init {
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(user) &&
                c.stance.penetration(user) &&
                user.canSpend(Pool.MOJO, 15)
    }

    override fun describe(): String {
        return if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            "A deep, powerful Thrust: 15 Mojo"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            "An accurate, cunning Thrust: 15 Mojo"
        } else {
            "A more potent fuck: 15 Mojo"
        }
    }

    override fun resolve(c: Combat, target: Character) {
        var m: Double
        val inside: Anatomy
        var style = Result.seductive
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            style = Result.powerful
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            style = Result.clever
        }
        user.spendMojo(15)
        when (style) {
            Result.powerful -> {
                m = Global.random(5 + user[Attribute.Power]) + target[Attribute.Perception].toDouble()
            }
            Result.clever -> {
                m = Global.random(5 + user[Attribute.Cunning]) + target[Attribute.Perception].toDouble()
            }
            else -> {
                m = Global.random(5 + user[Attribute.Seduction]) + target[Attribute.Perception].toDouble()
                m *= 1.3
            }
        }
        if (c.stance.anal()) {
            writeOutput(c, target, style, 1)
            inside = Anatomy.ass
        } else {
            inside = Anatomy.genitals
            writeOutput(c, target, style, 0)
            if (user.human()) {
                if (c.stance.behind(user)) {
                    if (!Global.checkFlag(Flag.exactimages) || target.id == ID.KAT) {
                        c.offerImage("Doggy Style.jpg", "Art by AimlessArt")
                    }
                    if (!Global.checkFlag(Flag.exactimages) || target.id == ID.MARA) {
                        c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt")
                    }
                } else {
                    if (!Global.checkFlag(Flag.exactimages) || target.id == ID.ANGEL) {
                        c.offerImage("Angel_Sex.jpg", "Art by AimlessArt")
                        c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri")
                    }
                    c.offerImage("Thrust.jpg", "Art by AimlessArt")
                }
            } else if (target.human()) {
                if (!Global.checkFlag(Flag.exactimages) || user.id == ID.CASSIE) {
                    c.offerImage("Cowgirl.jpg", "Art by AimlessArt")
                }
                if (!Global.checkFlag(Flag.exactimages) || user.id == ID.SAMANTHA) {
                    c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt")
                }
            }
        }
        if (user.getPure(Attribute.Professional) >= 11) {
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Sexual Momentum", user, Anatomy.genitals, user[Attribute.Professional] * 5.0), c)
            }
        }
        if (user.has(Trait.strapped)) {
            m += user[Attribute.Science] / 2
            m = user.bonusProficiency(Anatomy.toy, m)
        }
        target.pleasure(m, inside, combat = c)
        var r = (target.getSexPleasure(2, Attribute.Seduction, inside) * 2) / 3
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }

        user.pleasure(r, Anatomy.genitals, combat = c)
        c.stance.pace = 1
    }

    override fun copy(user: Character): Skill {
        return ThrustEX(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.powerful -> {
                String.format(
                    "You thrust deep into %s's pussy, in slow, powerful strokes. You push so deep with every thrust that your "
                            + "cock bumps against her cervix.", target.name
                )
            }
            Result.clever -> {
                String.format(
                    "You angle your hips and thrust into %s, carefully aiming for her G-spot. You make sure each stroke brushes your "
                            + "pelvis against her clit, maximizing her pleasure.", target.name
                )
            }
            else -> {
                String.format(
                    "You thrust your hips rhythmically, creating a smooth and constant motion. Your sensual strokes have no clear "
                            + "start or finish, and %s is quickly pulled into your rhythm as you pleasure her pussy.",
                    target.name
                )
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (damage == 1) {
            var using = "cock"
            if (user.has(Trait.strapped)) {
                using = "strapon dildo"
            }
            return when (modifier) {
                Result.powerful -> {
                    String.format(
                        "%s shoves her %s deep into your ass with such a powerful stroke that you barely stay standing.",
                        user.name,
                        using
                    )
                }
                Result.clever -> {
                    String.format(
                        "%s skillfully prods and torments your prostate with the head of her %s.",
                        user.name,
                        using
                    )
                }
                else -> {
                    String.format(
                        "%s thrusts her hips, pumping her %s in and out of your ass like a well-oiled machine. The rhythmic strokes "
                                + "send dizzying jolts of pleasure up your spine.",
                        user.name, using
                    )
                }
            }
        } else {
            return when (modifier) {
                Result.powerful -> {
                    String.format(
                        "%s lifts herself almost off your cock before tightening her vaginal walls around your glans. She "
                                + "drops her hips, taking your dick completely inside. The tight sensation along your entire length takes your "
                                + "breath away.", user.name
                    )
                }

                Result.clever -> {
                    String.format(
                        "%s switches up her movements, stopping briefly at the top of each thrust to stimulate your sensitive "
                                + "cockhead with her entrance. The extra attention to the sensitive area arouses you faster than usual.",
                        user.name
                    )
                }

                else -> {
                    String.format(
                        "%s moves her hips like a skilled dancer, riding you with smooth, continuous motion. The constant stimulation "
                                + "effectively milks your dick, bringing you closer and closer to cumming.", user.name
                    )
                }
            }
        }
    }

    override fun toString(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Strong Thrust"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            return "Precise Thrust"
        }
        return name
    }
}

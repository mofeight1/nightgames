package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Abuff

class ArmBar(user: Character) : Skill("Armbar", user) {
    init {
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.CRIPPLING)
        addTag(Attribute.Power)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.dom(user) &&
                c.stance.reachTop(user) &&
                user.canAct() &&
                !user.has(Trait.undisciplined) &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        val m = Global.random(10) + user[Attribute.Power] / 2.0
        writeOutput(c, target)
        target.pain(m, Anatomy.arm, c)
        target.add(Abuff(target, Attribute.Power, -4.0, 5), c)
        target.emote(Emotion.angry, 25)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 20 && !user.has(Trait.undisciplined)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 20)
    }

    override fun copy(user: Character): Skill {
        return ArmBar(user)
    }

    override var speed: Int = 2

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You grab at ${target.name}'s arm, but ${target.pronounSubject(false)} pulls it free."
        } else {
            "You grab ${target.name}'s arm at the wrist and pull it to your chest in the traditional judo submission technique."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "${user.name} grabs your wrist, but you pry it out of ${target.possessive(false)} grasp."
        } else {
            "${user.name} pulls your arm between ${user.possessive(false)} legs, forcibly overextending your elbow. The pain almost makes you tap out, but you manage to yank your arm out of ${user.possessive(false)} grip."
        }
    }

    override fun describe(): String {
        return "A judo submission hold that hyperextends the arm."
    }
}

package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Stance
import status.Flatfooted
import status.Stsflag

class Feint(user: Character) : Skill("Charge Feint", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && c.stance.en == Stance.neutral
    }

    override fun describe(): String {
        return "Fake out your opponent with a lunge."
    }

    override fun resolve(c: Combat, target: Character) {
        if (c.effectRoll(this, user, target, user[Attribute.Speed] + user[Attribute.Footballer] / 2)
            && !target.has(Stsflag.cynical)
        ) {
            writeOutput(c, target)
            target.add(Flatfooted(target, 1), c)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override var accuracy  = user[Attribute.Cunning]

    override fun copy(user: Character): Skill {
        return Feint(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

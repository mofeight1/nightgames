package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result

class AttireShift(user: Character) : Skill("Attire Shift", user) {
    init {
        addTag(Attribute.Temporal)
        addTag(SkillTag.STRIPPING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !target.isNude &&
                user.canSpend(Pool.TIME, 2)
    }

    override fun describe(): String {
        return "Separate your opponent from her clothes: 2 charges"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.TIME, 2)
        target.nudify()
        writeOutput(c, target)
        user.emote(Emotion.dominant, 15)
        target.emote(Emotion.nervous, 10)
    }

    override fun copy(user: Character): Skill {
        return AttireShift(user)
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You trigger a small temporal disturbance, sending %s's clothes a couple seconds back in time. "
                    + "Due to the speed and rotation of the Earth, they probably ended up somewhere over the Pacific Ocean.",
            target.name
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s triggers a device on her arm and your clothes suddenly vanish. "
                    + "What the fuck did %s just do?", user.name, user.pronounSubject(false)
        )
    }
}

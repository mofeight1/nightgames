package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Attachment
import items.Toy
import status.Stsflag

class UseCrop(user: Character) : Skill(Toy.Crop.getName(), user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (user.has(Toy.Crop) || user.has(Toy.Crop2) || user.has(Toy.Crop3)) &&
                user.canAct() &&
                c.stance.mobile(user) &&
                (c.stance.reachTop(user) || c.stance.reachBottom(user))
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m: Double
        if (target.isPantsless && c.stance.reachBottom(user)) {
            val tresurehunter = (user.has(Toy.Crop2) && Global.random(10) > 7) || (user.has(Attachment.CropKeen) && Global.random(10) > 7)
            val discipline = user.getPure(Attribute.Discipline) >= 15 && user.has(Stsflag.composed) && target.isPantsless && target.has(Stsflag.masochism)
            if (tresurehunter) {
                writeOutput(c, target, Result.critical)
                m = 8 + Global.random(14) + target[Attribute.Perception] + (user[Attribute.Science] / 2.0) + (user[Attribute.Discipline] / 2.0)
                target.emote(Emotion.angry, 15)
            } else if (discipline) {
                writeOutput(c, target, Result.special)
                m = 5 + Global.random(12) + target[Attribute.Perception] / 2.0 + (user[Attribute.Science] / 2.0)
                target.pleasure(m, Anatomy.genitals, combat = c)
            } else {
                m = 5 + Global.random(12) + target[Attribute.Perception] / 2.0 + (user[Attribute.Science] / 2.0) + (user[Attribute.Discipline] / 2.0)
                if (user.has(Toy.Crop3)) {
                    writeOutput(c, target, Result.unique)
                } else if (user.has(Attachment.CropShocker)) {
                    writeOutput(c, target, Result.upgrade)
                } else {
                    writeOutput(c, target, Result.normal)
                }
            }
            if (!tresurehunter) {
                if (user.human()) {
                    if (target.isPantsless) {
                        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.YUI) {
                            c.offerImage("Crop Nude.jpg", "Art by AimlessArt")
                        }
                    } else {
                        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.YUI) {
                            c.offerImage("Crop.jpg", "Art by AimlessArt")
                        }
                    }
                } else if (target.human()) {
                    if (!Global.checkFlag(Flag.exactimages) || user.id == ID.VALERIE) {
                        c.offerImage("Valerie Crop.png", "Art by AimlessArt")
                    }
                }
            }
            m = user.bonusProficiency(Anatomy.toy, m)
            if (user.has(Toy.Crop3)) {
                m *= 2.5
            }
            if (user.has(Attachment.CropShocker)) {
                m *= 2
            }
            if (user.has(Trait.cropexpert)) {
                m *= 1.5
            }
            target.pain(m, Anatomy.ass, c)
        } else {
            writeOutput(c, target, Result.weak)
            m = 5 + Global.random(12) + (user[Attribute.Science] / 2.0)
            if (user.has(Trait.cropexpert)) {
                m *= 1.5
            }
            m = user.bonusProficiency(Anatomy.toy, m)
            target.pain(m, Anatomy.chest, c)
        }
        target.emote(Emotion.angry, 45)
    }

    override fun copy(user: Character): Skill {
        return UseCrop(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                if (!target.has(Toy.Crop)) {
                    "You lash out with your riding crop, but it fails to connect."
                } else {
                    "You try to hit " + target.name + " with your riding crop, but she deflects it with her own."
                }
            }
            Result.unique -> {
                "You strike " + target.name + "'s soft, bare skin with your riding crop, leaving a visible red mark."
            }
            Result.special -> {
                target.name + " seems to be in the mood to be hurt right now, and you have no objection to that. Letting her know just how good a girl she's being right now, you give her just the lash with your riding crop that she's asking for."
            }
            Result.critical -> {
                if (target.hasBalls) {
                    "You strike " + target.name + "'s bare ass with your crop and the 'Treasure Hunter' attachment slips between her legs, hitting one of her hanging testicles " +
                            "squarely. She lets out a shriek and clutches her sore nut"
                } else {
                    "You strike " + target.name + "'s bare ass with your crop and the 'Treasure Hunter' attachment slips between her legs, impacting on her sensitive pearl. She " +
                            "lets out a high pitched yelp and clutches her injured anatomy."
                }
            }
            Result.weak -> {
                "You hit " + target.name + " with your riding crop."
            }
            Result.upgrade -> {
                "You hit " + target.name + "'s bare ass with your shock crop, delivering a painful zap on contact."
            }
            else -> {
                "You strike " + target.name + "'s soft, bare skin with your riding crop, leaving a visible red mark."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                if (!target.has(Toy.Crop)) {
                    "You duck out of the way, as " + user.name + " swings her riding crop at you."
                } else {
                    user.name + " swings her riding crop, but you draw your own crop and parry it."
                }
            }
            Result.unique -> {
                user.name + " hits you with her exceptional riding crop. You discover the hard way that the impressive craftsmanship isn't just for show. The skin where it hits feels like it's on fire."
            }
            Result.special -> {
                user.name + " gives you a knowing glance. She seems to know how much you want to be hurt right now, and she's in just the mood to oblige. Almost before you know it, her riding crop strikes across your skin, delighting it with both pain and pleasure simultaneously."
            }
            Result.critical -> {
                user.name + " hits you on the ass with her riding crop. The attachment on the end delivers a painful sting to your jewels. You groan in pain and fight the urge to " +
                        "curl up in the fetal position."
            }
            Result.weak -> {
                user.name + " strikes you with a riding crop."
            }
            Result.upgrade -> {
                user.name + " smacks your ass with her shock crop, and you feel a painful electrical discharge where it hits."
            }
            else -> {
                user.name + " hits your bare ass with a riding crop hard enough to leave a painful welt."
            }
        }
    }

    override fun describe(): String {
        return "Strike your opponent with riding crop. More effective if she's naked"
    }
}

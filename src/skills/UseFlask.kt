package skills

import characters.Character
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Flask
import items.Toy

class UseFlask : Skill {
    constructor(user: Character) : super("UseFlask", user)
    lateinit var item: Flask

    init {
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    constructor(user: Character, item: Flask) : super (item.getName(), user) {
        this.item = item
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user)
                && user.canAct() &&
                user.has(item) &&
                !c.stance.prone(user) &&
                item.canUse(c, user, target)
    }

    override fun describe(): String {
        return item.desc
    }

    override fun resolve(c: Combat, target: Character) {
        user.consume(item, 1)
        if (user.has(Toy.Aersolizer)) {
            writeOutput(c, target, Result.special)
            if (item == Flask.DisSol) {
                if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                    c.offerImage("Dissolving.jpg", "Sky Relyks")
                }
            }
            target.add(item.effect.copy(target), c)
        } else if (c.attackRoll(this, user, target)) {
            writeOutput(c, target)
            if (item == Flask.DisSol) {
                if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                    c.offerImage("Dissolving.jpg", "Sky Relyks")
                }
            }
            target.add(item.effect.copy(target), c)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return UseFlask(user, item)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                "You pop " + item.prefix + item.getName() + " into your Aerosolizer and spray " + target.name + " with a cloud of " + item.color + " mist. " +
                        item.getText(target)
            }
            Result.miss -> {
                "You throw a bottle of " + item.getName() + " at " + target.name + ", but she ducks out of the way and it splashes harmlessly on the ground. What a waste."
            }
            else -> {
                "You thow a flask of " + item.getName() + " at " + target.name + ". " + item.getText(target)
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " inserts a bottle into the attachment on her arm. You're suddenly surrounded by a cloud of " + item.color + " gas. " +
                        item.getText(target)
            }
            Result.miss -> {
                user.name + " splashes a bottle of " + item.color + " liquid in your direction, but none of it hits you."
            }
            else -> {
                user.name + " throws a bottle of " + item.color + " liquid at you. " + item.getText(target)
            }
        }
    }
}

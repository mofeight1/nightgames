package skills

import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Behind
import status.Flatfooted

class Diversion(self: Character) : Skill("Diversion", self) {
    init {
        addTag(Attribute.Cunning)
        addTag(SkillTag.OUTMANEUVER)
    }

    override fun requirements(user: Character): Boolean {
        return user.has(Trait.misdirection)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct()
                && c.stance.mobile(user)
                && !user.isNude &&
                !c.stance.prone(user) &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.isTopless) {
            writeOutput(c, target, Result.special)
            user.strip(1, c)
        } else {
            writeOutput(c, target, Result.normal)
            user.strip(0, c)
        }
        c.stance = Behind(user, target)
        target.add(Flatfooted(target, 1), c)
    }

    override fun copy(user: Character): Skill {
        return Diversion(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.normal) {
            when (Global.random(2)) {
                1 -> "In one swift motion, you pull off your " + user.tops.last() +
                        " and toss it in the air, leaving " + target.name + " to wonder where you went as it falls into her hands."

                else -> "You quickly strip off your " + user.tops.last() +
                        " and throw it to the right, while you jump to the left. " + target.name + " catches your discarded clothing, " +
                        "losing sight of you in the process."
            }
        } else {
            when (Global.random(2)) {
                1 -> "In one swift motion, you pull off your " + user.bottoms.last() +
                        " and toss it in the air, leaving " + target.name + " to wonder where you went as it falls into her hands."

                else -> "You quickly strip off your " + user.bottoms.last() +
                        " and throw it to the right, while you jump to the left. " + target.name + " catches your discarded clothing, " +
                        "losing sight of you in the process."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.normal) {
            when (Global.random(2)) {
                1 -> user.name + " surprises you by charging straight at you. You put your hand out to stop her from crashing into you and are left holding her " + user.tops.last() +
                        " while she is nowhere to be found."

                else -> "You lose sight of " + user.name + " for just a moment, but then see her moving behind you in your peripheral vision. You quickly spin around and grab her, " +
                        "but you find yourself holding just her " + user.tops.last() +
                        ". Wait... what the fuck?"

            }
        } else {
            when (Global.random(2)) {
                1 -> user.name + " surprises you by charging straight at you. You put your hand out to stop her from crashing into you and are left holding her " + user.bottoms.last() +
                        " while she is nowhere to be found."

                else -> "You lose sight of " + user.name + " for just a moment, but then see her moving behind you in your peripheral vision. You quickly spin around and grab her, " +
                        "but you find yourself holding just her " + user.bottoms.last() +
                        ". Wait... what the fuck?"
            }
        }
    }

    override fun describe(): String {
        return "Throws your clothes as a distraction"
    }
}

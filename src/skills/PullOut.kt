package skills

import characters.Character
import combat.Combat
import combat.Result
import stance.Stance

class PullOut(user: Character) : Skill("Pull Out", user) {
    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.penetration(user) && c.stance.dom(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (c.stance.en == Stance.anal) {
            writeOutput(c, target, Result.anal)
        } else {
            writeOutput(c, target, Result.normal)
        }
        c.stance = c.stance.insert(user)
    }

    override fun copy(user: Character): Skill {
        return PullOut(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.anal) {
            return "You pull your dick completely out of " + target.name + "'s ass."
        }
        return "You pull completely out of " + target.name + "'s pussy, causing her to let out a disappointed little whimper."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            "You feel the pressure in your anus recede as " + user.name + " pulls out."
        } else {
            user.name + " lifts her hips more than normal, letting your dick slip completely out of her."
        }
    }

    override fun describe(): String {
        return "Aborts penetration"
    }
}

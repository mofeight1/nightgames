package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Global
import status.Charmed
import status.Stsflag

class Beg(user: Character) : Skill("Beg", user) {
    init {
        addTag(Attribute.Submissive)
        addTag(SkillTag.CHARMING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && !c.stance.dom(user)
    }

    override fun describe(): String {
        return "Beg your opponent to go easy on you"
    }

    override fun resolve(c: Combat, target: Character) {
        var res = Result.normal
        if (target.mood == Emotion.angry || target.mood == Emotion.desperate)
            res = Result.miss
        if (res != Result.miss && target.mood != Emotion.dominant) {
            if (target.has(Stsflag.cynical) || Global.random(30) > user[Attribute.Submissive] - target[Attribute.Cunning] / 2)
                res = Result.miss
        }
        writeOutput(c, target, res)
        if (res != Result.miss)
            target.add(Charmed(target, 1 + user[Attribute.Submissive] / 10 + user.bonusCharmDuration()), c)
    }

    override fun copy(user: Character): Skill {
        return Beg(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                ("You give in and humble yourself while pleading with " + target.name + " to go easier on you. "
                        + "She just smiles and shakes her head and you get the feeling it had the opposite effect.")
            } else {
                "You throw away your pride and ask " + target.name + " for mercy. This just seems to encourage her sadistic side."
            }
        } else {
            if (Global.random(2) == 0) {
                ("You plead with " + target.name + " for a brief moment of reprieve. You aren't sure how you manage, "
                        + "but she agrees and you have a moment to rest.")
            } else {
                "You put yourself completely at " + target.name + "'s mercy. She takes pity on you and gives you a moment to recover."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                (user.name + " bats her eyes at you and gives you a desperate plea to ease up. "
                        + "You think it's time you remind her you're here to win, not show mercy.")
            } else {
                (user.name + " gives you a pleading look and asks you to go light on her. "
                        + "She's cute, but she's not getting away that easily.")
            }
        } else {
            if (Global.random(2) == 0) {
                ("When " + user.name + " looks at you like a kicked puppy and requests you give her a fighting chance, "
                        + "you can't help but give her a second to recover.")
            } else {
                user.name + " begs you for mercy, looking ready to cry. Maybe you should give her a break."
            }
        }
    }
}

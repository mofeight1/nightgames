package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import global.Global

class MagicMissile(user: Character) : Skill("Magic Missile", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canSpend(Pool.MOJO, 5)
    }

    override fun describe(): String {
        return "Fires a small magic projectile: 5 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(5)
        if (c.effectRoll(this, user, target, user[Attribute.Arcane])) {
            if (target.isNude && Global.random(3) == 2) {
                writeOutput(c, target, Result.critical)
                target.pain(9.0 + Global.random(2 * user[Attribute.Arcane] + 1), Anatomy.genitals, c)
                target.emote(Emotion.angry, 10)
            } else {
                writeOutput(c, target, Result.normal)
                target.pain(6.0 + Global.random(user[Attribute.Arcane] + 2), Anatomy.chest, c)
                target.emote(Emotion.angry, 5)
            }
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return MagicMissile(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override var accuracy = 7

    override var speed = 8

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You fire a bolt of magical energy, but " + target.name + " narrowly dodges out of the way."
            }
            Result.critical -> {
                if (target.hasBalls) {
                    "You cast and fire a magic missile at " + target.name + ". Just by luck, it hits her directly in the jewels. She cringes in pain, cradling her bruised parts."
                } else {
                    "You cast and fire a magic missile at " + target.name + ". By chance, it flies under her guard and hits her solidly in the pussy. She doubles over " +
                            "with a whimper, holding her bruised parts."
                }
            }
            else -> {
                "You hurl a magic missile at " + target.name + ", hitting and staggering her a step."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You see " + user.name + " start to cast a spell and you dive to the left, just in time to avoid the missile."
            }
            Result.critical -> {
                user.name + " casts a quick spell and fires a bolt of magic into your vulnerable groin. You cradle your injured plums as pain saps the strength from your " +
                        "legs."
            }
            else -> {
                user.name + "'s hand glows as she casts a spell. Before you can react, you're struck with an impact like a punch in the gut."
            }
        }
    }
}

package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result

class FlyCatcher(user: Character) : Skill("Fly Catcher", user) {
    init {
        addTag(Attribute.Ki)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.pet != null &&
                user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user)
    }

    override fun describe(): String {
        return "Focus on eliminating the enemy pet: 5 Stamina"
    }

    override fun resolve(c: Combat, target: Character) {
        user.weaken(5.0)
        writeOutput(c, target)
        target.pet?.caught(c, user)
    }

    override fun copy(user: Character): Skill {
        return FlyCatcher(user)
    }

    override fun type(): Tactics {
        return Tactics.summoning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        // TODO Auto-generated method stub
        return "You lunge with a sudden burst of speed and capture " + target.name + "'s pet."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " moves faster than you can follow to grab your pet."
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Global
import status.Hypersensitive
import status.Stsflag

class HeightenSenses(user: Character) : Skill("Heighten Senses", user) {
    init {
        addTag(Attribute.Hypnosis)
        // TODO Auto-generated constructor stub
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Hypnosis) >= 5
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Hypnosis to 5)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                !c.stance.sub(user) &&
                (target.has(Stsflag.charmed) || target.has(Stsflag.enthralled)) &&
                (!target.has(Stsflag.hypersensitive) || target.getPure(Attribute.Perception) < 9)
    }

    override fun describe(): String {
        return "Hypnotize the target to temporarily become more sensitive"
    }

    override fun resolve(c: Combat, target: Character) {
        if (target.has(Stsflag.hypersensitive) && Global.random(2) == 0) {
            writeOutput(c, target, Result.strong)
            target.mod(Attribute.Perception, 1)
        } else {
            writeOutput(c, target, Result.normal)
            target.add(Hypersensitive(target), c)
        }
        target.emote(Emotion.nervous, 20)
    }

    override fun copy(user: Character): Skill {
        return HeightenSenses(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            String.format(
                "You plant a suggestion in %s's head to increase %s sensitivity. %s accepts the suggestion so easily and "
                        + "strongly that you suspect it may have had a permanent effect.",
                target.name,
                target.possessive(false),
                target.pronounSubject(false)
            )
        } else {
            String.format(
                "You plant a suggestion in %s's head to increase %s sensitivity. %s shivers as %s sense of touch is "
                        + "amplified",
                user.name,
                target.possessive(false),
                target.pronounSubject(false),
                target.possessive(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            String.format(
                "%s explains to you that your body, especially your erogenous zones, have become more sensitive. %s's right. "
                        + "All your senses feel heightened. You feel almost like a superhero. It's ok if this is permanent, right?",
                user.name, user.pronounSubject(false)
            )
        } else {
            String.format(
                "%s explains to you that your body, especially your erogenous zones, have become more sensitive. "
                        + "You feel goosebumps cover your skin as if you've been hit by a Sensitivity Flask. Maybe you were and just "
                        + "didn't notice", user.name
            )
        }
    }
}

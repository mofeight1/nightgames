package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import items.Clothing
import items.OwnedItem

class Tear(user: Character) : Skill("Tear Clothes", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.STRIPPING)
    }

    override fun requirements(user: Character): Boolean {
        return (user.getPure(Attribute.Power) >= 32 || user[Attribute.Animism] >= 15) && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 32, Attribute.Animism to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return ((c.stance.reachTop(user) && !target.isTopless) || ((c.stance.reachBottom(user) && !target.isPantsless))) && user.canAct()
    }

    override fun describe(): String {
        return "Rip off your opponents clothes"
    }

    override fun resolve(c: Combat, target: Character) {
        var clothing: OwnedItem<Clothing>? = null
        var top = c.stance.reachTop(user)
        if (top) {
            clothing = target.tops.lastOrNull()
            top = clothing != null && clothing.item.attribute !== Trait.indestructible
        }
        var bottom = !top && c.stance.reachBottom(user)
        if (bottom) {
            val bClothing = target.bottoms.lastOrNull()
            bottom = bClothing != null && bClothing.item.attribute !== Trait.indestructible
            clothing = if (bottom) bClothing
            else clothing ?: bClothing
        }

        if (!top && !bottom) {
            if (clothing != null) {
                if (clothing.item.isTop) writeOutput(c, target, Result.miss, 0)
                else writeOutput(c, target, Result.miss, 1)
            }
            return
        }
        val animal = user.getPure(Attribute.Animism) >= 12

        var att = user[Attribute.Power]
        if (animal) att += (user[Attribute.Animism] * user.arousal.percent() / 100).toInt()
        val success = target.stripAttempt(att, user, c, clothing!!.item)

        val r = if (!success) Result.miss
        else if (animal) Result.strong
        else Result.normal

        if (top) writeOutput(c, target, r, 0)
        else writeOutput(c, target, r, 1)
        if (!success) return

        if (top) target.shred(0) else target.shred(1)
        if (target.isNude) {
            c.write(target.nakedLiner())
        }
        if (bottom){
            if (target.human() && target.isPantsless) {
                if (target.arousal.current >= 15) {
                    c.write("Your boner springs out, no longer restrained by your pants.")
                } else {
                    c.write("${user.name} giggles as your flaccid dick is exposed")
                }
            }
            target.emote(Emotion.nervous, 10)
        }
    }

    override fun copy(user: Character): Skill {
        return Tear(user)
    }

    override fun toString(): String {
        return if (user[Attribute.Animism] >= 12) {
            "Shred Clothes"
        } else {
            "Tear Clothes"
        }
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        val clothing = if (damage == 0) target.tops.last() else target.bottoms.last()
        return when(modifier) {
            Result.normal -> "${target.name} yelps in surprise as you rip her $clothing apart."
            Result.strong -> "You channel your animal spirit and shred ${target.name}'s $clothing with claws you don't actually have."
            Result.miss -> "You try to tear apart ${target.name}'s $clothing, but the material is more durable than you expected."
            else -> ""
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        val clothing = if (damage == 0) target.tops.last() else target.bottoms.last()
        return when(modifier) {
            Result.normal -> "${user.name} violently rips your $clothing off."
            Result.strong -> "${user.name} lunges toward you and rakes her nails across your $clothing, shredding the garment. That shouldn't be possible. Her " +
                "nails are not that sharp, and if they were, you surely wouldn't have gotten away unscathed."
            Result.miss -> "${user.name} yanks on your $clothing, but fails to remove it."
            else -> ""
        }
    }
}

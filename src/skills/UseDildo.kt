package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Attachment
import items.Flask
import items.Toy
import status.Oiled
import status.Stsflag

class UseDildo(user: Character) : Skill(Toy.Dildo.getFullName(user), user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (user.has(Toy.Dildo) || user.has(Toy.Dildo2)) &&
                user.canAct() &&
                target.hasPussy &&
                c.stance.reachBottom(user) &&
                target.isPantsless &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        var block = 0
        val lastAction = c.lastact(target)
        if (lastAction != null &&
            lastAction.name === Toy.Onahole.getFullName(target)) {
            block = 5
        }
        if (!c.attackRoll(this, user, target) || Global.random(20) < block) {
            writeOutput(c, target, Result.miss, block)
            return
        }
        var level = 1
        var m = 6.0 + (target[Attribute.Perception] / 2) + (user[Attribute.Science] / 2)
        if (user.has(Attachment.DildoSlimy)) {
            m *= 2
            level += 1
        }
        if (user.has(Toy.Dildo2)) {
            m += 5
            level += 1
        }
        writeOutput(c, target, Result.normal, level)
        if (user.has(Attachment.DildoLube) && user.has(Flask.Lubricant) && !target.has(Stsflag.oiled)) {
            writeOutput(c, target, Result.upgrade, 1)
            user.consume(Flask.Lubricant, 1)
            target.add(Oiled(target))
        }
        if (user.has(Attachment.DildoSlimy)) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                c.offerImage("Dildo4.png", "Art by AimlessArt")
            }
        } else if (user.has(Attachment.DildoLube)) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                c.offerImage("Dildo3.png", "Art by AimlessArt")
            }
        } else if (user.has(Toy.Dildo2)) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                c.offerImage("Dildo2.png", "Art by AimlessArt")
            }
        } else {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                c.offerImage("Dildo1.png", "Art by AimlessArt")
            }
        }
        m = user.bonusProficiency(Anatomy.toy, m)
        if (user.has(Toy.Dildo2)) {
            target.pleasure(m, Anatomy.genitals, combat = c)
        } else {
            target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        }
    }

    override fun copy(user: Character): Skill {
        return UseDildo(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (damage > 0) {
                "You try to slip a dildo into " + target.name + ", but she catches it with her " +
                        Toy.Onahole.getFullName(target) + "."
            } else {
                "You try to slip a dildo into " + target.name + ", but she blocks it."
            }
        } else if (modifier == Result.upgrade) {
            "As you rub the head of the dildo against " + target.name + "'s pussy, it shoots out a generous load of lubricant to " +
                    "coat her groin."
        } else {
            when (damage) {
                3 -> "As the slimy dildo touches " + target.name + "'s nethers, it starts to react eagerly. It delves into her vagina and writhes around, exploring " +
                        "her intimate depths. She trembles at the intense sensation and moans loudly."

                2 -> when (Global.random(2)) {
                    1 -> "You push the dildo into " + target.name + ". While she's reeling, you crank up the sonic vibration, and she nearly falls to her knees from the sensation."
                    else -> "You touch the imperceptibly vibrating dildo to " + target.name + "'s love button and she jumps as if shocked. Before she can defend herself, you " +
                            "slip it into her pussy. She starts moaning in pleasure immediately."
                }

                else -> when (Global.random(2)) {
                    1 -> "You quickly slip the tip of your dildo into " + target.name + "'s pussy. Before she can react, you push it in further, eliciting a pleasured moan from her lips."
                    else -> "You rub the dildo against " + target.name + "'s lower lips to lubricate it before you thrust it inside her. She can't help moaning a little as you " +
                            "pump the rubber toy in and out of her pussy."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun describe(): String {
        return "Pleasure opponent with your dildo"
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Toy

class Squeeze(user: Character) : Skill("Squeeze Balls", user) {
    init {
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.DOM)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.hasBalls &&
                c.stance.reachBottom(user) &&
                user.canAct() &&
                !user.has(Trait.shy)
    }

    override fun resolve(c: Combat, target: Character) {
        if (c.attackRoll(this, user, target)) {
            var m = Global.random(user[Attribute.Power]) + target[Attribute.Perception] - (2.0 * target.bottoms.size)
            var type = Result.normal
            if (target.isPantsless) {
                if (user.has(Toy.ShockGlove) && user.canSpend(Pool.BATTERY, 2)) {
                    user.spend(Pool.BATTERY, 2)
                    type = Result.special
                    m += user[Attribute.Science]
                } else if (target.has(Trait.armored)) {
                    type = Result.item
                }
            } else if (user.name === "Mara" && user.rank >= 2) {
                type = Result.unique
                m += Global.random(user.level)
            } else {
                type = Result.weak
            }
            writeOutput(c, target, type)
            if (target.human()) {
                if (!Global.checkFlag(Flag.exactimages) || target.id == ID.MARA) {
                    c.offerImage("Squeeze Balls.jpg", "Art by AimlessArt")
                }
                c.offerImage("Balls.jpg", "Art by AimlessArt")
            }
            target.pain(m, Anatomy.genitals, c)
            if (user.has(Trait.wrassler)) {
                target.calm(m / 4, c)
            } else {
                target.calm(m / 2, c)
            }
            user.buildMojo(10)
            target.emote(Emotion.angry, 15)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 9)
    }

    override fun copy(user: Character): Skill {
        return Squeeze(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You try to grab ${target.name}'s balls, but she avoids it."
            }
            Result.special -> {
                "You use your shock glove to deliver a painful jolt directly into ${target.name}'s testicles."
            }
            Result.weak -> {
                "You grab the bulge in ${target.name}'s ${target.bottoms.last()} and squeeze."
            }
            Result.item -> {
                "You grab the bulge in ${target.name}'s ${target.bottoms.last()}, but find it solidly protected."
            }
            else -> {
                "You manage to grab ${target.name}'s balls and squeeze them hard. You feel a twinge of empathy when she cries out in pain, but you maintain your grip."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "${user.name} grabs at your balls, but misses."
            }
            Result.unique -> {
                "Mara deftly slips her hand down the front of your pants and tightly wraps her small fingers around your testicles, " +
                        "holding your balls firmly in her gloved hand.  You are thankful that she isn't squeezing them very hard, " +
                        "but you suddenly feel a debilitating shock of electricity being delivered directly to your ballsack."
            }
            Result.special -> {
                "${user.name} grabs your naked balls roughly in her gloved hand. A painful jolt of electricity shoots through your groin, sapping your will to fight."
            }
            Result.weak -> {
                "${user.name} grabs your balls through your ${target.bottoms.last()} and squeezes hard."
            }
            Result.item -> {
                "${user.name} grabs your crotch through your ${target.bottoms.last()}, but you can barely feel it."
            }
            else -> {
                when (Global.random(2)) {
                    1 -> user.name + " grabs both your balls roughly in her hand, crushing them in her grip.  " +
                            "You feel your voice momentarily raise an octave as your testicles are squeezed together."

                    else -> "${user.name} reaches between your legs and grabs your exposed balls. You writhe in pain as she pulls and squeezes them."
                }
            }
        }
    }

    override fun toString(): String {
        return if (user.has(Toy.ShockGlove)) {
            "Shock Balls"
        } else {
            name
        }
    }

    override fun describe(): String {
        return "Grab opponent's groin, deals more damage if naked"
    }
}

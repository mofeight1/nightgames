package skills

import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class Bravado(user: Character) : Skill("Determination", user) {
    override fun requirements(user: Character): Boolean {
        return user.has(Trait.fearless)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.mobile(user) && user.canSpend(Pool.MOJO, 20)
    }

    override fun resolve(c: Combat, target: Character) {
        val x: Int = user.mojo.current
        writeOutput(c, target)
        user.spendMojo(x)
        user.calm(x / 2.0, c)
        user.heal(x.toDouble(), c)
        user.emote(Emotion.confident, 30)
        user.emote(Emotion.dominant, 20)
        user.emote(Emotion.nervous, -20)
        user.emote(Emotion.desperate, -30)
    }

    override fun copy(user: Character): Skill {
        return Bravado(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (Global.random(2) == 0) {
            "You refuse to give in and use all your pent up will to keep fighting on."
        } else {
            "You grit your teeth and put all your willpower into the fight."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (Global.random(2) == 0) {
            "You can tell by the glint in " + user.name + "'s eye that she has a renewed sense of purpose in this fight."
        } else {
            user.name + " gives you a determined glare as she seems to gain a second wind."
        }
    }

    override fun describe(): String {
        return "Consume mojo to restore stamina and reduce arousal"
    }
}

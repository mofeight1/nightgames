package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Horny
import status.Stsflag

class KissBetter(user: Character) : Skill("Kiss Better", user) {
    init {
        addTag(Attribute.Footballer)
        addTag(Result.oral)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                c.stance.oral(user) &&
                !c.stance.behind(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
                && target.has(Stsflag.stunned)
    }

    override fun describe(): String {
        return "Lick and suck a stunned opponent's genitals to make them feel better."
    }

    override fun resolve(c: Combat, target: Character) {
        if (target.hasBalls) {
            writeOutput(c, target, Result.strong)
        } else {
            writeOutput(c, target)
        }
        target.pleasure(
            user[Attribute.Footballer] + (2 * user[Attribute.Seduction] / 3.0) + target[Attribute.Perception],
            Anatomy.genitals,
            combat = c
        )
        target.heal(target.stamina.max / 3.0, c)
        target.add(Horny(target, 2.0, 8), c)
    }

    override fun copy(user: Character): Skill {
        return KissBetter(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.strong) {
            return "You lean over " + target.name + "'s unresisting body and suck on her tender balls. She coos with pleasure as her pain is replaced with arousal."
        }
        return "You lean over " + target.name + "'s unresisting body and lavish her exposed pussy with licking and sucking. She coos with pleasure as her pain is replaced with arousal."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " gives your naked balls a sloppy kiss before taken them into her mouth one at a time and sucking them. " +
                "You feel better, both in the oral pleasure sense, and the sense of your pain easing."
    }
}

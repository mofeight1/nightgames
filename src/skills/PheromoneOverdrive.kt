package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Stsflag
import kotlin.math.roundToInt

class PheromoneOverdrive(user: Character) : Skill("Pheromone Pink Overdrive", user) {
    init {
        addTag(Attribute.Animism)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Animism) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Animism to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.dom(user) &&
                user.canAct() &&
                user.has(Stsflag.feral) &&
                user.canSpend(Pool.MOJO, 30) &&
                user.arousal.percent() > target.arousal.percent()
    }

    override fun describe(): String {
        return "Use an intense burst of pheromones to make your opponent as horny as you are: 30 mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(30)
        target.arousal.set(((user.arousal.percent() * target.arousal.max) / 100).roundToInt())
        writeOutput(c, target)
        target.emote(Emotion.horny, 30)
    }

    override fun copy(user: Character): Skill {
        return PheromoneOverdrive(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You take a deep breath to focus all your primal pheromones into a thick aura. You grab %s and give %s "
                    + "a passionate kiss while you channel your pheromones into %s body, instantly driving %s into heat.",
            target.name, target.pronounTarget(false), target.pronounTarget(false), target.pronounTarget(false)
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s seems to glow with pink energy. It can't be! Is that a cloud of pheromones so dense it's visible to "
                    + "the naked eye? Before you can escape, %s pulls you into a deep kiss. With your mouth occupied, you're forced to "
                    + "breathe %s heady scent through your nose. The pheromone-filled smell overwhelms you and makes your cock painfully hard.",
            user.name, user.pronounSubject(false), user.possessive(false)
        )
    }
}

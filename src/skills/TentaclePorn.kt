package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import status.Bound
import status.Oiled
import status.Stsflag

class TentaclePorn(user: Character) : Skill("Tentacle Porn", user) {
    init {
        addTag(Attribute.Fetish)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.sub(user) &&
                !c.stance.prone(user) &&
                !c.stance.prone(target) &&
                user.canAct() &&
                user.arousal.current >= 20 &&
                user.canSpend(Pool.MOJO,10)
    }

    override fun describe(): String {
        return "Create a bunch of hentai tentacles."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        if (!c.effectRoll(this, user, target, user[Attribute.Fetish])) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (!target.isNude) {
            writeOutput(c, target, Result.weak)
            target.add(Bound(target, 10 + user[Attribute.Fetish] / 3, "tentacles"))
            return
        }
        if (target.bound()) {
            writeOutput(c, target, Result.special)
            target.pleasure(
                Global.random(user[Attribute.Fetish] + target[Attribute.Perception]).toDouble(),
                Anatomy.genitals,
                combat = c
            )
            target.emote(Emotion.horny, 10)
        } else {
            writeOutput(c, target, Result.normal)
            if (!target.has(Stsflag.oiled)) {
                target.add(Oiled(target), c)
            }
            target.pleasure(Global.random(user[Attribute.Fetish] / 2).toDouble(), Anatomy.genitals, combat = c)
            target.emote(Emotion.horny, 20)
            target.add(Bound(target, 10 + user[Attribute.Fetish] / 3, "tentacles"))
        }
    }

    override fun copy(user: Character): Skill {
        return TentaclePorn(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You summon a mass of tentacles that try to snare " + target.name + ", but she nimbly dodges them."
            }
            Result.weak -> {
                "You summon a mass of phallic tentacles that wrap around " + target.name + "'s arms, holding her in place."
            }
            Result.normal -> {
                "You summon a mass of phallic tentacles that wrap around " + target.name + "'s naked body. They squirm against her and squirt slimy fluids on her body."
            }
            else -> {
                "You summon tentacles to toy with " + target.name + "'s helpless form. The tentacles toy with her breasts and penetrate her pussy and ass."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " stomps on the ground and a bundle of tentacles erupt from the ground. You're barely able to avoid them."
            }
            Result.weak -> {
                user.name + " stomps on the ground and a bundle of tentacles erupt from the ground around you, entangling your arms and legs."
            }
            Result.normal -> {
                user.name + " stomps on the ground and a bundle of tentacles erupt from the ground around you, entangling your arms and legs. The slimy appendages " +
                        "wriggle over your body and coat you in the slippery liquid."
            }
            else -> {
                user.name + " summons slimy tentacles that cover your helpless body, tease your dick, and probe your ass."
            }
        }
    }
}

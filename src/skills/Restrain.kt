package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import stance.Pin
import stance.PleasurePin
import stance.ReversePin
import stance.Stance
import stance.SubmissionPin

class Restrain(user: Character) : Skill("Pin", user) {
    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                c.stance.prone(target) &&
                user.canAct() &&
                !c.stance.penetration(user) &&
                !c.stance.penetration(target) &&
                (c.stance.en == Stance.mount || c.stance.en == Stance.reversemount)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.effectRoll(this, user, target, user[Attribute.Power] / 3)) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (user.getPure(Attribute.Ki) >= 12) {
            if (c.stance.en == Stance.reversemount) {
                c.stance = SubmissionPin(user, target)
                writeOutput(c, target, Result.special, 1)
            } else {
                c.stance = PleasurePin(user, target)
                writeOutput(c, target, Result.special, 0)
            }
        } else {
            writeOutput(c, target, Result.normal)
            if (c.stance.en == Stance.reversemount) {
                c.stance = ReversePin(user, target)
            } else {
                c.stance = Pin(user, target)
            }
        }
        target.emote(Emotion.nervous, 10)
        target.emote(Emotion.desperate, 10)
        user.emote(Emotion.dominant, 20)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 8)
    }

    override fun copy(user: Character): Skill {
        return Restrain(user)
    }

    override var speed = 2

    override var accuracy = 4

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.special) {
            if (damage == 1) {
                "You restrain " + target.name + "'s arms and put intense pressure on her groin."
            } else {
                "You grab " + target.name + "'s wrists and press your thigh between her legs."
            }
        } else if (modifier == Result.miss) {
            "You try to catch " + target.name + "'s hands, but she squirms to much to keep your grip on her."
        } else {
            "You manage to restrain " + target.name + ", leaving her helpless and vulnerable beneath you."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.special) {
            if (damage == 1) {
                user.name + " pins your arms with her thighs and grabs you by the balls."
            } else {
                user.name + " pins your hands and presses her knee into your groin, leaving you at her mercy."
            }
        } else if (modifier == Result.miss) {
            user.name + " tries to pin you down, but you keep your arms free."
        } else {
            user.name + " pounces on you and pins your arms in place, leaving you at her mercy."
        }
    }

    override fun describe(): String {
        return "Restrain opponent until she struggles free"
    }
}

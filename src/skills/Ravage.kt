package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Stsflag

class Ravage(user: Character) : Skill("Ravage", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Eldritch) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Eldritch to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !user.stunned() &&
                !user.distracted() &&
                !user.has(Stsflag.enthralled) &&
                target.getStatusMagnitude("Entangled") >= 100
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
    }

    override fun copy(user: Character): Skill {
        return Ravage(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

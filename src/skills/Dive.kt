package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Stance
import stance.StandingOver

class Dive(user: Character) : Skill("Dive", user) {
    init {
        addTag(Attribute.Submissive)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.en == Stance.neutral
    }

    override fun describe(): String {
        return "Hit the deck! Avoids some attacks."
    }

    override fun resolve(c: Combat, target: Character) {
        c.stance = StandingOver(target, user)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return Dive(user)
    }

    override fun type(): Tactics {
        return Tactics.negative
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You take evasive action and dive to the floor. Ok, you're on the floor now. That's as far as you planned."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " dives dramatically away and lands flat on the floor."
    }
}

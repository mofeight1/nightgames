package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Behind
import status.Flatfooted

class ChargeFeint(user: Character) : Skill("Charge Feint", user) {
    init {
        addTag(Attribute.Footballer)
        addTag(SkillTag.OUTMANEUVER)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c)
    }

    override fun describe(): String {
        return "Fake out your opponent with fancy footwork"
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.effectRoll(this, user, target, user[Attribute.Footballer])) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target)
        var duration = 1
        if (user.getPure(Attribute.Footballer) >= 12) {
            duration++
        }
        target.add(Flatfooted(target, duration), c)
        c.stance = Behind(user, target)
    }

    override fun copy(user: Character): Skill {
        return ChargeFeint(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You rush at " + target.name + " and attempt to dodge behind her, but she's too quick."
        } else {
            "You rush straight at " + target.name + " and feint to the right before dodging left. She stumbles and you slip behind her."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " rushes at you with quick footwork, but you stop her from getting behind you."
        } else {
            user.name + " rushes straight at you. At the last moment, she dodges as expected, but when you try to follow her, her quick footwork trips you up."
        }
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class Knee(user: Character) : Skill("Knee", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.PAINFUL)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                !c.stance.behind(target) &&
                !c.stance.penetration(target) &&
                !user.has(Trait.sportsmanship)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (c.stance.prone(target)) {
            writeOutput(c, target, Result.special)
        } else {
            writeOutput(c, target, Result.normal)
        }
        if (target.human()) {
            if (Global.random(5) >= 3) {
                c.write(user, user.bbLiner())
            }
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.CASSIE) {
                c.offerImage("Knee.png", "Art by AimlessArt")
            }
        }

        val m = 4.0 + Global.random(11) + user[Attribute.Power]
        target.pain(m, Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 2, c)
        } else {
            target.calm(m, c)
        }
        user.buildMojo(10)
        target.emote(Emotion.angry, 75)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 10 && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 10)
    }

    override fun copy(user: Character): Skill {
        return Knee(user)
    }

    override var speed = 4

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            return target.name + " blocks your knee strike."
        }
        return if (target.hasBalls) {
            String.format(
                "You deliver a powerful knee strike to %s's dangling balls. %s lets out a pained whimper and nurses %s injured parts.",
                target.name, target.pronounSubject(false), target.possessive(false)
            )
        } else {
            "You deliver a powerful knee strike to " + target.name + "'s delicate lady flower. She lets out a pained whimper and nurses her injured parts."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            return user.name + " tries to knee you in the balls, but you block it."
        }
        if (modifier == Result.special) {
            return user.name + " raises one leg into the air, then brings her knee down like a hammer onto your balls. You cry out in pain and instinctively try " +
                    "to close your legs, but she holds them open."
        } else {
            when (Global.random(2)) {
                1 -> return (user.name + " manages to get one of her legs in between yours, and jerks her knee upwards, hitting you squarely in the balls.  "
                        + "You go cross-eyed for a moment as the pain works its way up your stomach and into your throat.")
            }
            return user.name + " steps in close and brings her knee up between your legs, crushing your fragile balls. You groan and nearly collapse from the " +
                    "intense pain in your abdomen."
        }
    }

    override fun describe(): String {
        return "Knee opponent in the groin"
    }
}

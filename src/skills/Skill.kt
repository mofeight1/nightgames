package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import combat.Tag


abstract class Skill(
    var name: String,
    var user: Character
) : Comparable<Skill> {

    var target: Character? = null
    protected var image: String? = null
    protected var artist: String? = null
    protected var sortOrder: Int? = null
    protected var tags: HashSet<Tag> = HashSet()
    open var accuracy: Int = 5
    open var speed: Int = 5
    abstract fun requirements(user: Character): Boolean
    open fun attRequired(): Map<Attribute, Int> {
        return emptyMap()
    }
    abstract fun usable(c: Combat, target: Character): Boolean
    abstract fun describe(): String
    abstract fun resolve(c: Combat, target: Character)
    abstract fun copy(user: Character): Skill
    abstract fun type(): Tactics
    abstract fun deal(target: Character, modifier: Result? = Result.normal, damage: Int = 0): String
    abstract fun receive(target: Character, modifier: Result? = Result.normal, damage: Int = 0): String
    fun text(target: Character, modifier: Result? = Result.normal, damage: Int = 0): String {
        return if (user.human()) deal(target, modifier, damage)
        else receive(target, modifier, damage)
    }
    fun writeOutput(combat: Combat, target: Character, modifier: Result? = Result.normal, damage: Int = 0) {
        combat.write(user, text(target, modifier, damage))
    }

    override fun toString(): String {
        return name
    }

    fun addTag(tag: Tag) {
        tags.add(tag)
    }

    fun hasTag(tag: Tag): Boolean {
        return tags.contains(tag)
    }

    fun getSortOrder(): Int {
        if (sortOrder != null) return sortOrder!!
        return type().ordinal
    }


    override fun equals(other: Any?): Boolean {
        return this.javaClass == other?.javaClass
    }

    override fun compareTo(other: Skill): Int {
        val ret = getSortOrder() - other.getSortOrder()
        if (ret != 0) return ret

        return name.compareTo(other.name)
    }
}

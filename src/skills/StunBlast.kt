package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global

class StunBlast(user: Character) : Skill("Stun Blast", user) {
    init {
        addTag(Attribute.Science)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.behind(user) &&
                user.canSpend(Pool.BATTERY, 4)
    }

    override fun describe(): String {
        return "A blast of light and sound with a chance to stun: 4 Battery"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 4)
        if (Global.random(10) >= 5) {
            writeOutput(c, target)
            target.pain(target.stamina.current.toDouble(), Anatomy.head, c)
            target.stamina.empty()
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return StunBlast(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You overload the emitter on your arm, but " + target.name + " shields her face to avoid the flash."
        } else {
            "You overload the emitter on your arm, duplicating the effect of a flashbang. " + target.name + " staggers as the blast disorients her."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " covers her face and points a device in your direction. Sensing danger, you shield you eyes just as the flashbang goes off."
        } else {
            user.name + " points a device in your direction that glows slightly. A sudden flash of light disorients you and your ears ring from the blast."
        }
    }
}

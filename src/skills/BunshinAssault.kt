package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import kotlin.math.min

class BunshinAssault(user: Character) : Skill("Bunshin Assault", user) {
    init {
        addTag(Attribute.Ninjutsu)
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.CLONE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !c.stance.behind(target) &&
                !c.stance.penetration(target) && user.canSpend(Pool.MOJO, 6)
    }

    override fun describe(): String {
        return "Attack your opponent with shadow clones: 3 Mojo per attack (min 2)"
    }

    override fun resolve(c: Combat, target: Character) {
        val clones = min(min(user.mojo.current / 3, user[Attribute.Ninjutsu] / 3), 6)
        var r: Result
        user.spendMojo(clones * 3)
        if (user.human()) {
            c.write("You form $clones shadow clones and rush forward.")
        } else if (target.human()) {
            c.write(
                String.format(
                    "%s moves in a blur and suddenly you see %d of %s approaching you.",
                    user.name,
                    clones,
                    user.pronounTarget(false)
                )
            )
        }
        for (i in 0 until clones) {
            if (c.attackRoll(this, user, target)) {
                when (Global.random(4)) {
                    0 -> {
                        r = Result.weak
                        writeOutput(c, target, r)
                        target.pain(Global.random(4) + 3.0, Anatomy.ass, c)
                        target.calm(3.0, c)
                    }

                    1 -> {
                        r = Result.normal
                        writeOutput(c, target, r)
                        target.pain(Global.random(4) + user[Attribute.Power] / 3.0, Anatomy.chest, c)
                        target.calm(3.0, c)
                    }

                    2 -> {
                        r = Result.strong
                        writeOutput(c, target, r)
                        target.pain(Global.random(8) + user[Attribute.Power] / 2.0, Anatomy.genitals, c)
                        if (!user.has(Trait.wrassler)) {
                            target.calm(3.0, c)
                        }
                    }

                    else -> {
                        r = Result.critical
                        writeOutput(c, target, r)
                        target.pain(Global.random(12) + user[Attribute.Power].toDouble(), Anatomy.genitals, c)
                        if (!user.has(Trait.wrassler)) {
                            target.calm(3.0, c)
                        }
                    }
                }
                target.emote(Emotion.angry, 30)
            } else {
                writeOutput(c, target, Result.miss)
            }
        }
    }

    override fun copy(user: Character): Skill {
        return BunshinAssault(user)
    }

    override var speed = 4

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format("%s dodges one of your shadow clones.", target.name)
        } else if (modifier == Result.weak) {
            String.format(
                "Your shadow clone gets behind %s and slaps %s hard on the ass.",
                target.name,
                target.pronounTarget(false)
            )
        } else if (modifier == Result.strong) {
            if (target.hasBalls) {
                String.format("One of your clones gets grabs and squeezes %s's balls.", target.name)
            } else {
                String.format(
                    "One of your clones hits %s on %s sensitive tit.",
                    target.name,
                    target.possessive(false)
                )
            }
        } else if (modifier == Result.critical) {
            if (target.hasBalls) {
                String.format("One lucky clone manages to deliver a clean kick to %s's fragile balls.", target.name)
            } else {
                String.format(
                    "One lucky clone manages to deliver a clean kick to %s's sensitive vulva.",
                    target.name
                )
            }
        } else {
            String.format("One of your shadow clones lunges forward and strikes %s in the stomach.", target.name)
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format("You quickly dodge a shadow clone's attack.")
        } else if (modifier == Result.weak) {
            String.format("You lose sight of one of the clones until you feel a sharp spank on your ass cheek.")
        } else if (modifier == Result.strong) {
            if (target.hasBalls) {
                String.format("A %s clone gets a hold of your balls and squeezes them painfully.", user.name)
            } else {
                String.format(
                    "A %s clone unleashes a quick roundhouse kick that hits your sensitive boobs.",
                    user.name
                )
            }
        } else if (modifier == Result.critical) {
            if (target.hasBalls) {
                String.format(
                    "One lucky %s clone manages to land a snap-kick squarely on your unguarded jewels.",
                    user.name
                )
            } else {
                String.format("One %s clone hits you between the legs with a fierce cunt-punt.", user.name)
            }
        } else {
            String.format("One of %s clones delivers a swift punch to your solar plexus.", user.possessive(false))
        }
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global

class Defabricator(user: Character) : Skill("Defabricator", user) {
    init {
        addTag(Attribute.Science)
        addTag(SkillTag.STRIPPING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                !target.isNude && user.canSpend(Pool.BATTERY, 8)
    }

    override fun describe(): String {
        return "Does what it says on the tin."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 8)
        if (!target.check(Attribute.Speed, 5 + user[Attribute.Science] / 2)) {
            writeOutput(c, target)
            target.nudify()
            if (target.isNude) c.write(target, target.nakedLiner())
        } else {
            writeOutput(c, target, Result.weak)
            if (target.isNude) c.write(target, target.nakedLiner())
        }
    }

    override fun copy(user: Character): Skill {
        return Defabricator(user)
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.weak) {
            return String.format(
                "You charge up your Defabricator and point it in %s's general direction. "
                        + "%s jumps away just as it activates and you're only able to get %s %s.",
                target.name, target.pronounSubject(true), target.possessive(false), target.shredRandom()!!
            )
        }
        return if (Global.random(2) == 0) {
            ("You ready your Defabricator then take aim at " + target.name + ", who tries to dash away. You're too fast for her "
                    + "and she soon finds herself basking in the light then completely naked. Isn't technology awesome?")
        } else {
            String.format(
                "You charge up your Defabricator and point it in %s's general direction. "
                        + "A bright light engulfs %s and %s clothes are disintegrated in moment.",
                target.name,
                target.pronounTarget(false),
                target.possessive(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.weak) {
            return String.format(
                "%s points a device at you and light shines from it like it's a simple flashlight. Instinct causes you to dive out of the way "
                        + "at the last moment. When you land, you realize your %s is gone.",
                user.name,
                target.shredRandom()
            )
        }
        return if (Global.random(2) == 0) {
            (user.name + " fidgets with a device on her arm, the extends it towards you. You are immediately engulfed in a "
                    + "bright that that leaves you without any clothing, much to her pleasure.")
        } else {
            String.format(
                "%s points a device at you and light shines from it like it's a simple flashlight. The device's function is immediately revealed as your clothes just vanish " +
                        "in the light. You're left naked in seconds.", user.name
            )
        }
    }
}

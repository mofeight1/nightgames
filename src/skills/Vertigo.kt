package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import stance.StandingOver

class Vertigo(user: Character) : Skill("Vertigo", user) {
    init {
        addTag(Attribute.Unknowable)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Unknowable) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Unknowable to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !c.stance.prone(target) &&
                user.canSpend(Pool.ENIGMA, 2)
    }

    override fun describe(): String {
        return "Incapacitate your opponent and deliver a coup de grace: 2 Enigma"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.ENIGMA, 2)
        if (target[Attribute.Vigilantism] >= 2) {
            writeOutput(c, target, Result.miss)
        } else if (c.isWatching(ID.JEWEL)) {
            writeOutput(c, target, Result.defended)
        } else {
            c.stance = StandingOver(user, target)
            val pow = 30 + user[Attribute.Power]

            writeOutput(c, target)
            if (target.human()) {
                if (Global.random(5) >= 1) {
                    c.write(user, user.bbLiner())
                }
            }
            target.pain(pow.toDouble() - (Global.random(2) * target.bottoms.size), Anatomy.genitals, c)
            target.calm(Global.random(pow).toDouble(), c)
        }
    }

    override fun copy(user: Character): Skill {
        return Vertigo(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                ""
            }
            Result.defended -> {
                ""
            }
            else -> {
                ""
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "The world suddenly seems to warp around you until you can't tell up from down. As you come to your senses, " +
                        "you realize you are lying on the floor and " + user.name + " is standing over you with one foot raised. With almost " +
                        "supernatural reflexes, you launch yourself out of the way before her foot can come down."
            }
            Result.defended -> {
                "[Placeholder: Jewel Assists]"
            }
            else -> {
                "The world suddenly seems to warp around you until you can't tell up from down. As you come to your senses, " +
                        "you realize you are lying on the floor and " + user.name + " is standing over you with one foot raised. Before you " +
                        "can react, she stomps down on your groin."
            }
        }
    }
}

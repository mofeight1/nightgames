package skills

import characters.Character
import combat.Combat
import combat.Result
import stance.ReverseMount

class ReverseStraddle(user: Character) : Skill("Mount(Reverse)", user) {
    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                c.stance.mobile(target) &&
                c.stance.prone(target) &&
                user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        c.stance = ReverseMount(user, target)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return ReverseStraddle(user)
    }

    override var speed = 6

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You straddle " + target.name + ", facing her feet."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " sits on your chest, facing your crotch."
    }

    override fun describe(): String {
        return "Straddle facing groin"
    }
}

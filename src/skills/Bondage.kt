package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.BD
import status.Stsflag

class Bondage(user: Character) : Skill("Bondage", user) {
    init {
        addTag(Attribute.Fetish)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.arousal.current >= 5 &&
                !user.has(Stsflag.bondage)
    }

    override fun describe(): String {
        return "You and your opponent become aroused by being tied up for five turns: Arousal at least 5"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(BD(user), c)
        target.add(BD(target), c)
    }

    override fun copy(user: Character): Skill {
        return Bondage(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You imagine the exhilarating feeling of ropes digging into your skin and binding you. You push this feeling into " + target.name + "'s libido."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " flushes and wraps her arms around herself tightly. Suddenly the thought of being tied up and dominated slips into your head."
    }
}

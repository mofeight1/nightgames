package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Flying
import stance.Stance

class Fly(user: Character) : Skill("Fly", user) {
    init {
        addTag(Attribute.Dark)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(this.user) &&
                !c.stance.prone(this.user) &&
                user.canFuck && target.isPantsless &&
                user.stamina.current >= 15 &&
                user.isErect &&
                target.isErect &&
                c.stance.en != Stance.flying
    }

    override fun describe(): String {
        return "Take off while holding your opponent and have your way " +
                "with them in the air."
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.REYKA) {
                c.offerImage("Reyka Fly.jpg", "Art by AimlessArt")
            }
        }
        var m = user[Attribute.Dark] / 2.0 + target[Attribute.Perception]
        if (user.has(Trait.strapped)) {
            m += user[Attribute.Science] / 2.0
            m = user.bonusProficiency(Anatomy.toy, m)
        } else {
            m = user.bonusProficiency(Anatomy.genitals, m)
        }
        target.pleasure(m, Anatomy.genitals, combat = c)
        var r = user[Attribute.Dark] / 2.0 + user[Attribute.Perception]
        r = target.bonusProficiency(Anatomy.genitals, r)
        r += target.bonusRecoilPleasure(r)
        user.pleasure(r, Anatomy.genitals, combat = c)
        user.emote(Emotion.dominant, 50)
        user.emote(Emotion.horny, 30)
        target.emote(Emotion.desperate, 50)
        target.emote(Emotion.nervous, 75)
        c.stance = Flying(this.user, target)
    }

    override fun copy(user: Character): Skill {
        return Fly(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    fun isPrefferedBySuccubus(target: Character): Boolean {
        return target.isNude
    }

    override fun deal(
        target: Character, modifier: Result?, damage: Int
    ): String {
        return "Summoning demonic wings, you grab " + target.name + " tightly and take off, " +
                if (target.hasDick && user.hasPussy) "inserting his dick into your hungry pussy."
                else " holding her helpless in the air as you thrust deep into her wet pussy."
    }

    override fun receive(
        target: Character, modifier: Result?, damage: Int
    ): String {
        return "Suddenly, " + target.name + " leaps at you, embracing you tightly" +
                ". She then flaps her powerful wings hard and before you know it" +
                " you are twenty feet in the sky held up by her arms and legs." +
                " Somehow, your dick ended up inside of her in the process and" +
                " the rhythmic movements of her flying arouse you to no end"
    }
}

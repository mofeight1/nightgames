package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class StripBottomEX(user: Character) : Skill("Tricky Strip Bottoms", user) {
    init {
        addTag(SkillTag.STRIPPING)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachBottom(user) &&
                !target.isPantsless &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 15)
    }

    override fun resolve(c: Combat, target: Character) {
        var strip: Int
        var style = Result.clever
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            style = Result.powerful
        } else if (user[Attribute.Seduction] > user[Attribute.Cunning]) {
            style = Result.seductive
        }
        user.spendMojo(15)
        when (style) {
            Result.powerful -> {
                strip = user[Attribute.Power]
            }
            Result.clever -> {
                strip = user[Attribute.Cunning]
                strip *= 3
            }
            else -> {
                strip = user[Attribute.Seduction]
            }
        }
        if (!target.stripAttempt(strip, user, c, target.bottoms.last().item)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target, style)
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Yui")) {
                c.offerImage("Strip Bottom Female.jpg", "Art by AimlessArt")
            }
            if (!Global.checkFlag(Flag.exactimages) || target.id === ID.MARA) {
                c.offerImage("Mara StripBottom.jpg", "Art by AimlessArt")
            }
        } else if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.MARA) {
                c.offerImage("Strip Bottom Male.jpg", "Art by AimlessArt")
            }
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.ANGEL) {
                c.offerImage("Angel StripBottom.jpg", "Art by AimlessArt")
            }
        }

        target.strip(1, c)
        if (user.getPure(Attribute.Cunning) >= 30 && !target.isPantsless) {
            if (target.stripAttempt(strip, user, c, target.bottoms.last().item)) {
                writeOutput(c, target, Result.strong)
                target.strip(1, c)
                target.emote(Emotion.nervous, 10)
            }
        }
        if (target.isNude) {
            c.write(target, target.nakedLiner())
        }
        if (target.human() && target.isPantsless) {
            if (target.arousal.current >= 15) {
                c.write("Your boner springs out, no longer restrained by your pants.")
            } else {
                c.write(user.name + " giggles as your flacid dick is exposed")
            }
        }
        target.emote(Emotion.nervous, 10)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return StripBottomEX(user)
    }

    override var speed = 3

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You grab ${target.name}'s ${target.bottoms.last()}, but ${target.pronounSubject(false)} scrambles away before you can strip ${target.pronounTarget(false)}."
            }
            Result.strong -> {
                "Taking advantage of the situation, you also manage to snag ${target.possessive(false)} ${target.bottoms.last()}."
            }
            Result.powerful -> {
                "You forcefully rip ${target.name}'s ${target.bottoms.last()} down to the floor before ${target.pronounSubject(false)} can resist."
            }
            Result.clever -> {
                "You playfully give ${target.name} a sharp pinch on the butt. When ${target.pronounSubject(false)} yelps and reflexively covers ${target.possessive(false)} bottom, " +
                        "you switch to your real target and pull down ${target.possessive(false)} ${target.bottoms.last()}."
            }
            Result.seductive -> {
                "With a smile and a wink, you manage to literally charm the ${target.bottoms.last()} off of ${target.name}."
            }
            else -> {
                "With a smile and a wink, you manage to literally charm the ${target.bottoms.last()} off of ${target.name}."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "${user.name} tries to pull down your ${target.bottoms.last()}, but you hold them up."
            }
            Result.strong -> {
                "Before you can react, ${user.pronounSubject(false)} also strips off your ${target.bottoms.last()}."
            }
            Result.powerful -> {
                "${user.name} lunges at you and yanks down your ${target.bottoms.last()} with unexpected force."
            }
            Result.clever -> {
                "${user.name} slips a hand between your legs to lightly pinch your balls. You reflexively flinch and move to protect your delicate parts, but ${user.pronounSubject(false)} " +
                    "takes advantage of your momentary panic to pull down your ${target.bottoms.last()}."
            }
            Result.seductive -> {
                "${user.name} gives you a dazzling smile and suggestively tugs at the waistband of your ${target.bottoms.last()}. It would be trivially easy to stop ${user.pronounTarget(false)}, " +
                    "but your desire gets the better of you, and you let yourself be stripped."
            }
            else -> {
                "${user.name} gives you a dazzling smile and suggestively tugs at the waistband of your ${target.bottoms.last()}. It would be trivially easy to stop ${user.pronounTarget(false)}, " +
                    "but your desire gets the better of you, and you let yourself be stripped."
            }
        }
    }

    override fun toString(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Forceful Strip Bottoms"
        } else if (user[Attribute.Seduction] > user[Attribute.Cunning]) {
            return "Charming Strip Bottoms"
        }
        return name
    }

    override fun describe(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Remove opponent's pants with great force: 15 Mojo"
        } else if (user[Attribute.Seduction] > user[Attribute.Cunning]) {
            return "Attempt to remove opponent's pants by seduction: 15 Mojo"
        }
        return "Attempt to remove opponent's pants with extra Mojo: 15 Mojo"
    }
}

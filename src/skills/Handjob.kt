package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Stance
import status.ProfMod
import status.ProfessionalMomentum
import status.Stsflag

class Handjob(user: Character) : Skill("Handjob", user) {
    init {
        addTag(Result.touch)
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachBottom(user) &&
                (target.isPantsless || (user.has(Trait.dexterous) && target.bottoms.size <= 1))
                && target.hasDick &&
                user.canAct() &&
                (!c.stance.penetration(target) || c.stance.en == Stance.anal)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }

        var m = 4.0 + target[Attribute.Perception]
        var type = Result.normal
        if (user[Attribute.Seduction] >= 8) {
            m += Global.random(user[Attribute.Seduction] / 2)
            user.buildMojo(10)
        } else if (target.human()) {
            type = Result.weak
        }
        if (user.getPure(Attribute.Professional) >= 3) {
            m += user[Attribute.Professional]
            type = Result.special
            user.buildMojo(15)
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Dexterous Momentum", user, Anatomy.fingers, user[Attribute.Professional] * 5.0), c)
            }
        }
        if (user.has(Stsflag.shadowfingers)) {
            type = Result.critical
        }
        writeOutput(c, target, type)
        if (target.human()) {
            c.offerImage("Handjob.jpg", "Art by AimlessArt")
            c.offerImage("Handjob2.jpg", "Art by AimlessArt")
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.JEWEL) {
                c.offerImage("Jewel Handjob.png", "Art by Fujin Hitokiri")
            }
        }
        m = user.bonusProficiency(Anatomy.fingers, m)
        target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        if (user.has(Trait.roughhandling)) {
            target.weaken(m / 2, c)
            writeOutput(c, target, Result.unique)
        }
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Handjob(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.unique -> {
                "You opportunistically give her balls some rough treatment."
            }
            Result.miss -> {
                "You reach for " + target.name + "'s dick but miss."
            }
            Result.critical -> {
                String.format(
                    "Your dexterous shadow tendrils wrap around %s's shaft. As you stroke, you let your shadows go wild, "
                            + "rubbing and massaging her sensitive member in a way no human appendages could.",
                    target.name
                )
            }
            Result.special -> {
                when (Global.random(3)) {
                    0 -> String.format(
                        "You take hold of %s's cock and run your fingers over it briskly, hitting all the right spots.",
                        target.name
                    )

                    1 -> String.format(
                        "Your hold on %s's dick tightens, and where once there were gentle touches there are now firm jerks.",
                        target.name
                    )

                    else -> String.format(
                        "You have latched on to %s's cock with both hands now, twisting them in a fierce milking movement and eliciting pleasured groans from %s.",
                        target.name, target.pronounTarget(false)
                    )
                }
            }
            else -> {
                "You grab " + target.name + "'s girl-cock and stroke it using the techniques you use when masturbating."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        when (modifier) {
            Result.unique -> {
                return "She roughly womanhandles your balls, sapping some of your strength."
            }
            Result.miss -> {
                return user.name + " grabs for your dick and misses."
            }
            Result.critical -> {
                return String.format(
                    "%s bizarre shadow fingers coil around your dick and wriggle all over your length. A shiver runs through "
                            + "you at the strange, but extremely pleasurable sensation.", user.name
                )
            }
            Result.special -> {
                return when (Global.random(3)) {
                    0 -> String.format(
                        "%s takes hold of your cock and run %s fingers over it briskly, hitting all the right spots.",
                        user.name, user.possessive(false)
                    )

                    1 -> String.format(
                        "%s's hold on your dick tightens, and where once there were gentle touches there are now firm jerks.",
                        user.name
                    )

                    else -> String.format(
                        "%s has latched on to your cock with both hands now, twisting them in a fierce milking movement and eliciting pleasured groans from you.",
                        user.name
                    )
                }
            }
            else -> {
                var r: Int
                return if (target.bottoms.isNotEmpty()) {
                    "${user.name} slips her hand into your ${target.bottoms.last()} and strokes your dick."
                } else if (modifier == Result.weak) {
                    "${user.name} clumsily fondles your crotch. It's not skillful by any means, but it's also not entirely ineffective."
                } else {
                    if (target.arousal.current < 15) {
                        "${user.name} grabs your soft penis and plays with the sensitive organ until it springs into readiness."
                    } else if ((Global.random(3).also { r = it }) == 0) {
                        "${user.name} strokes and teases your dick, sending shivers of pleasure up your spine."
                    } else if (r == 1) {
                        "${user.name} rubs the sensitive head of your penis and fondles your balls."
                    } else {
                        "${user.name} jerks you off like she's trying to milk every drop of your cum."
                    }
                }
            }
        }
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Professional) >= 3) {
            "Pro Handjob"
        } else {
            "Handjob"
        }
    }

    override fun describe(): String {
        return if (user.getPure(Attribute.Professional) >= 3) {
            "A professional handjob that increases effectiveness with repeated use"
        } else {
            "Rub your opponent's dick"
        }
    }
}

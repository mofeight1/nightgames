package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import kotlin.math.min

class WindUp(user: Character) : Skill("Wind-up", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                !user[Pool.TIME].isFull
    }

    override fun describe(): String {
        return "Primes time charges: first charge free, 10 Mojo for each additional charge"
    }

    override fun resolve(c: Combat, target: Character) {
        var charges = min(4, user.mojo.current / 10)
        if (charges + user[Pool.TIME].current > user[Pool.TIME].max) {
            charges = user[Pool.TIME].max - user[Pool.TIME].current
        }

        user.spendMojo(charges * 10)
        writeOutput(c, target, Result.normal, charges)
        user[Pool.TIME].restore(charges)
    }

    override fun copy(user: Character): Skill {
        return WindUp(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format("You take advantage of a brief lull in the fight to wind up your Procrastinator, priming $damage time charges for later use.")
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format("%s fiddles with a small device on %s wrist.", user.name, user.possessive(false))
    }
}

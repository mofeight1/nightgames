package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Neutral
import status.Braced
import status.Stsflag
import kotlin.math.max

class Escape(user: Character) : Skill("Escape", user) {
    init {
        addTag(Attribute.Cunning)
        addTag(SkillTag.ESCAPE)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        val sub = c.stance.sub(user) &&
                !c.stance.mobile(user) &&
                !c.stance.penetration(user) &&
                !c.stance.penetration(target)

        return (sub || user.bound()) &&
                !user.stunned() &&
                !user.distracted() &&
                !user.has(Stsflag.enthralled)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.bound()) {
            if (user.check(Attribute.Cunning, 10 - user.escape())) {
                if (user.human()) {
                    c.write(user, "You slip your hands out of your restraints.")
                } else if (target.human()) {
                    c.write(user, user.name + " manages to free herself.")
                }
                user.free()
            } else {
                if (user.human()) {
                    c.write(user, "You try to slip your restraints, but can't get free.")
                } else if (target.human()) {
                    c.write(user, user.name + " squirms against her restraints fruitlessly.")
                }
            }
        } else if (user.check(
                Attribute.Cunning,
                c.stance.escapeDC(target, user) +
                        target.stamina.current / 4 - user.stamina.current / 4 +
                        target[Attribute.Power] / 2 - max(user[Attribute.Power], user[Attribute.Cunning]) / 2
            )
        ) {
            if (user.human()) {
                c.write(user, "Your quick wits find a gap in " + target.name + "'s hold and you slip away.")
            } else if (target.human()) {
                c.write(
                    user,
                    user.name + " goes limp and you take the opportunity to adjust your grip on her. As soon as you move, she bolts out of your weakened hold. " +
                            "It was a trick!"
                )
            }
            if (c.stance.prone(user) && !user.has(Stsflag.braced)) {
                user.add(Braced(user))
            }
            c.stance = Neutral(user, target)
        } else {
            if (user.human()) {
                if (user.isNude) {
                    c.write(
                        user,
                        "You try to take advantage of an opening in " + target.name + "'s stance to slip away, but she catches you by your protruding penis and reasserts her position."
                    )
                } else {
                    c.write(
                        user,
                        "You think you see an opening in " + target.name + "'s stance, but she corrects it before you can take advantage."
                    )
                }
            } else if (target.human()) {
                c.write(
                    user,
                    user.name + " manages to slip out of your grip for a moment, but you tickle her before she can get far and regain control."
                )
            }
            c.stance.struggle(c)
            c.stance.decay()
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 12)
    }

    override fun copy(user: Character): Skill {
        return Escape(user)
    }

    override var speed = 1

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun describe(): String {
        return "Uses Cunning to try to escape a submissive position"
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Toy
import kotlin.math.roundToInt

class WarningCrop(user: Character) : Skill("Warning Crop", user) {
    init {
        addTag(Attribute.Discipline)
        addTag(SkillTag.TOY)
        addTag(SkillTag.CROP)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Discipline) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Discipline to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (user.has(Toy.Crop) || user.has(Toy.Crop2) || user.has(Toy.Crop3)) &&
                user.canAct() &&
                c.stance.mobile(user) &&
                (c.stance.reachTop(user) || c.stance.reachBottom(user))
    }

    override fun resolve(c: Combat, target: Character) {
        var m = 2 + Global.random(8) + (user[Attribute.Discipline] / 3.0)
        m = user.bonusProficiency(Anatomy.toy, m)
        writeOutput(c, target)
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.VALERIE) {
                c.offerImage("Valerie Crop.png", "Art by AimlessArt")
            }
        }
        target.pain(m, Anatomy.fingers, c)
        target.spendMojo((2 * m).roundToInt())
        target.emote(Emotion.nervous, 70)
    }

    override fun copy(user: Character): Skill {
        return WarningCrop(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Just as it looks like " + target.name + " is thinking of striking you, you lash out with your riding crop. The tip of it strikes across the back of her hand and she immediately pulls it back. That should make her think twice about that."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "Just as your mind flashes to the possibility of striking at " + user.name + ", she lashes out with her riding crop, striking across the back of your hand. You pull your hand back on instinct. " + user.pronounSubject(
            true
        ) + " must have seen through you and knew what you were thinking. You’ll have to be more careful in the future."
    }

    override fun describe(): String {
        return "Use a light crop strike to demoralize your opponent"
    }
}

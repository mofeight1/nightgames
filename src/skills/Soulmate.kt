package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Soulbound
import status.Stsflag

class Soulmate(user: Character) : Skill("Soulmate", user) {
    init {
        addTag(Attribute.Contender)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Contender) >= 7
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Contender to 7)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.kiss(user) &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 50) &&
                !user.has(Stsflag.soulbound)
    }

    override fun describe(): String {
        return "Share your sense of touch with your opponent"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        val duration = user.getPure(Attribute.Contender) / 2
        user.add(Soulbound(user, target, duration), c)
    }

    override fun copy(user: Character): Skill {
        return Soulmate(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You pull " + target.name + " close and capture her lips. While she's focused on the kiss, you establish a single directional mana link. It takes her a moment to " +
                "notice the new sensation and even longer to understand it. Until this wears off, she'll feel anything she does to your body."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " kisses you softly and romantically, slowly drawing you into her embrace. As she pulls away, you feel something unnatural, but you can't put your finger on it. " +
                "She grins mischievously and softly runs her hand down her body. You feel a jolt of pleasure as if she had touched you."
    }
}

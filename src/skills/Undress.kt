package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Charmed

class Undress(user: Character) : Skill("Undress", user) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        if (user.canAct() &&
            !c.stance.sub(user)
            && !user.isNude &&
            !c.stance.prone(user) &&
            !user.has(Trait.strapped) &&
            !c.stance.behind(user)
        ) {
            name = if (striptease()) {
                "Strip Tease"
            } else {
                "Undress"
            }
            return true
        } else {
            return false
        }
    }

    override fun describe(): String {
        return if (striptease()) {
            "Tempt opponent by removing your clothes"
        } else {
            "Removes your own clothes"
        }
    }

    override fun resolve(c: Combat, target: Character) {
        if (!striptease() || c.stance.behind(user)) {
            writeOutput(c, target, Result.normal)
            user.undress(c)
            return
        }
        if (user.getPure(Attribute.Professional) >= 7) {
            writeOutput(c, target, Result.special)
            target.add(Charmed(target, 2 + user.bonusCharmDuration()))
        } else {
            writeOutput(c, target, Result.strong)
        }
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Angel")) {
                c.offerImage("Strip Tease.jpg", "Art by AimlessArt")
            }
        }
        target.tempt(
            (user[Attribute.Seduction] / 4.0) * (user.tops.size + user.bottoms.size) + user[Attribute.Professional],
            combat = c
        )
        user.buildMojo(20)
        target.emote(Emotion.horny, 30)
        user.emote(Emotion.confident, 15)
        user.emote(Emotion.dominant, 15)
        user.undress(c)
    }

    override fun copy(user: Character): Skill {
        return Undress(user)
    }

    override fun type(): Tactics {
        return if (striptease()) {
            Tactics.pleasure
        } else {
            Tactics.negative
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                "You seductively perform a short dance, shedding clothes as you do so. " +
                        target.name + " seems quite taken with it, as " +
                        target.pronounSubject(false) + " is practically drooling onto the ground."
            }
            Result.strong -> {
                "During a brief respite in the fight as " + target.name + " is catching her breath, you make a show of seductively removing your clothes. " +
                        "By the time you finish, she's staring with undisguise arousal, pressing a hand unconsciously against her groin."
            }
            else -> {
                "You quickly strip off your clothes and toss them aside."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " takes a few steps back and starts " +
                        "moving sinuously. She sensually runs her hands over her body, " +
                        "undoing straps and buttons where she encounters them, and starts" +
                        " peeling her clothes off slowly, never breaking eye contact." +
                        " You can only gawk in amazement as her perfect body is revealed bit" +
                        " by bit, and the thought of doing anything to blemish such" +
                        " perfection seems very unpleasant indeed."
            }
            Result.strong -> {
                user.name + " asks for a quick time out and starts sexily slipping her her clothes off. Although there are no time outs in the rules, you can't help staring " +
                        "at the seductive display until she finishes with a cute wiggle of her naked ass."
            }
            else -> {
                user.name + " puts some space between you and strips naked."
            }
        }
    }

    private fun striptease(): Boolean {
        return user.getPure(Attribute.Seduction) >= 24 &&
                !user.has(Trait.direct) &&
                !user.has(Trait.shy) &&
                !user.has(Trait.cursed)
    }
}

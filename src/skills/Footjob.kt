package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Stsflag

class Footjob(user: Character) : Skill("Footjob", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.feet)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 22
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 22)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.feet(user) &&
                target.isPantsless &&
                c.stance.prone(user) != c.stance.prone(target)
                && user.canAct() &&
                !c.stance.penetration(target)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = Global.random(2) + user[Attribute.Seduction] / 2.0 + target[Attribute.Perception] / 2.0 + 4
        var r = Result.normal
        if (user.getPure(Attribute.Footballer) >= 18) {
            m += user[Attribute.Footballer] / 2.0
            r = Result.upgrade
            if (target.has(Stsflag.masochism)) {
                r = Result.special
                target.pain(user[Attribute.Footballer] / 4.0, Anatomy.genitals, c)
            }
        }
        writeOutput(c, target, r)
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Mara")) {
                c.offerImage("Footjob.jpg", "Art by Sky Relyks")
            }
            c.offerImage("Footjob2.jpg", "Art by AimlessArt")
        }
        m += user.mojo.current / 10.0
        m = user.bonusProficiency(Anatomy.feet, m)
        target.pleasure(m, Anatomy.genitals, combat = c)
        if (c.stance.dom(user)) {
            user.buildMojo(20)
        }
    }

    override fun copy(user: Character): Skill {
        return Footjob(user)
    }

    override var speed = 4

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                return "You try to slip your foot between " + target.name + "'s legs, but she slips away."
            } else {
            }
            return "You aim your foot between " + target.name + "'s legs, but miss."
        } else if (target.hasDick) {
            return if (Global.random(2) == 0) {
                ("You run your toes along the head of " + target.name + "'s girl-cock and watch her twitch. "
                        + "You then press your foot across her shaft and trap her cock between your sole and her stomach.")
            } else {
                "You press your foot against " + target.name + "'s girl-cock and stimulate it by grinding it with the sole."
            }
        } else {
            return when (Global.random(3)) {
                2 -> ("You slip a toe between " + target.name + "'s pussy lips, her own wetness making it easy, and pump it in and out. "
                        + "Once your foot is sufficiently lubricated, you press it against her clit and swirl it as your toe probes her.")

                1 -> "You prod at " + target.name + "'s clit with your big toe, making her a little more wet before you slip it just inside her pussy."
                else -> "You rub your foot against " + target.name + "'s pussy lips, using her own wetness as lubricant, and stimulate her love button with your toe."
            }
        }
    }

    override fun receive(attacker: Character, modifier: Result?, damage: Int): String {
        when (modifier) {
            Result.miss -> {
                if (Global.random(2) == 0) {
                    return user.name + " tries to press her foot against your cock, but you are wise to her ploy and dodge."
                } else {
                }
                return user.name + " swings her foot at your groin, but misses."
            }
            Result.upgrade -> {
                return user.name + " plants her foot in your groin, expertly wedging her toes firmly between your testicles.  With a look of both fascination and arousal, " +
                        "she playfully wriggles her toes against your defenseless nuts, watching them roll and squirm in your sack.  The caress of her smooth, supple feet " +
                        "against your scrotum feels incredible, and you can’t bring yourself to resist her as she strokes the sole of her foot up and down your grateful shaft."
            }
            Result.special -> {
                return user.name + " plants her foot in your groin, expertly wedging her toes firmly between your testicles.  With a look of both fascination and arousal, " +
                        "she playfully wriggles her toes against your defenseless nuts, watching them roll and squirm in your sack.  The caress of her smooth, supple feet " +
                        "against your scrotum feels incredible, and you can’t bring yourself to resist her as she strokes the sole of her foot up and down your grateful shaft, " +
                        "teasingly smacking your balls around with a few playful kicks.  She traps your nuts against your pubic bone with her foot and slowly begins twisting, " +
                        "gently grinding your delicates under the balls of her foot, causing them to ache and swell with desire."
            }
            else -> {
                return if (Global.random(2) == 0) {
                    (user.name + " tickles the shaft of your cock with her toes before rolling it over and rubbing it up and down with "
                            + "her silky soles. You're surprised at how good her foot feels and she seems ecstatic when your cock shows its "
                            + "thanks by leaking precum all over her tootsies.")
                } else {
                    user.name + " rubs your dick with the sole of her soft foot. From time to time, she teases you by pinching the glans between her toes and jostling your balls."
                }
            }
        }
    }

    override fun toString(): String {
        if (user.getPure(Attribute.Footballer) >= 18) {
            return "Dribble"
        }
        return name
    }

    override fun describe(): String {
        return "Pleasure your opponent with your feet, more effective with high Mojo"
    }
}

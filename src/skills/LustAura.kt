package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Horny

class LustAura(self: Character) : Skill("Lust Aura", self) {
    init {
        addTag(Attribute.Dark)
    }


    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canSpend(Pool.MOJO, 5) &&
                user.canSpend(Pool.AROUSAL, 5)
    }

    override fun describe(): String {
        return "Inflicts arousal over time: 5 Arousal, 5 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(5)
        user.spendArousal(5)
        writeOutput(c, target)
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.REYKA) {
                c.offerImage("Lust Aura.png", "Art by AimlessArt")
            }
        }
        target.add(Horny(target, 3.0 + 2 * user.skimpiness, 3 + Global.random(3)), c)
        target.emote(Emotion.horny, 10)
    }

    override fun copy(user: Character): Skill {
        return LustAura(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You allow the corruption in your libido to spread out from your body. " + target.name + " flushes with arousal and presses her thighs together as the aura taints her."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " releases an aura of pure sex. You feel your body becoming hot just being near her."
    }
}

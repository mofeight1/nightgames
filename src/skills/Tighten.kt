package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Stance

class Tighten(user: Character) : Skill("Tighten", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 28 && user.hasPussy
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.penetration(user) &&
                user.hasPussy &&
                target.hasDick &&
                c.stance.en !== Stance.anal
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        val m = user.getSexPleasure(2, Attribute.Power, Anatomy.genitals)
        target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        c.stance.pace = 0
    }

    override fun copy(user: Character): Skill {
        return Tighten(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " gives you a seductive wink and suddenly her pussy squeezes around your dick as though it's trying to milk you."
    }

    override fun describe(): String {
        return "Squeeze opponent's dick, no pleasure to user"
    }
}

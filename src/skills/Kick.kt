package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class Kick(user: Character) : Skill("Kick", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 17
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 17)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.feet(user) &&
                user.canAct() &&
                !user.has(Trait.sportsmanship) &&
                (!c.stance.prone(user) || user.has(Trait.dirtyfighter)) &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = Global.random(12) + user[Attribute.Power].toDouble()
        var r = Result.normal
        if (c.stance.prone(user)) {
            r = Result.strong
            m *= 1.5
        }
        if (user.getPure(Attribute.Footballer) >= 15 && target.distracted()) {
            r = Result.critical
            m *= 1.5
        }
        if (target.bottoms.isNotEmpty() && user.getPure(Attribute.Ki) >= 21) {
            r = Result.special
        }
        writeOutput(c, target, r)
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Yui")) {
                c.offerImage("Yui Kicked.png", "Art by AimlessArt")
            }
        } else if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Jewel")) {
                c.offerImage("Kick.jpg", "Art by AimlessArt")
            }
        }
        if (r == Result.special) target.shred(1)
        user.buildMojo(10)
        m = user.bonusProficiency(Anatomy.feet, m)
        target.pain(m, Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 4, c)
        } else {
            target.calm(m / 2, c)
        }
        target.emote(Emotion.angry, 80)
    }

    override fun copy(user: Character): Skill {
        return Kick(user)
    }

    override var speed = 8

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Ki) >= 21) {
            "Shatter Kick"
        } else {
            "Kick"
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "Your kick hits nothing but air."
            }
            Result.special -> {
                "You focus your ki into a single kick, targeting not ${target.name}'s body, but ${target.possessive(false)} ${target.bottoms.last().item.name}. The garment is completely destroyed, and your foot continues into ${target.possessive(false)} exposed groin."
            }
            Result.critical -> {
                if (target.hasBalls) {
                    "You capitalize on ${target.name}'s momentary inability to defend ${target.pronounSubject(false)}self – you quickly sweep the inside of ${target.possessive(false)} foot with yours, kicking ${target.possessive(false)} legs apart. " +
                            "${target.possessive(true)} arms flail out to her sides as ${target.pronounSubject(false)} struggles to maintain her footing, and you take a short step back, lining up your attack. " +
                            "You wind up and launch a merciless kick right between ${target.possessive(false)} wide-open legs, your instep colliding solidly with ${target.possessive(false)} tender balls."
                } else {
                    "You capitalize on ${target.name}'s momentary inability to defend ${target.pronounSubject(false)}self – you quickly sweep the inside of ${target.possessive(false)} foot with yours, kicking ${target.possessive(false)} legs apart. " +
                            "${target.possessive(true)} arms flail out to her sides as ${target.pronounSubject(false)} struggles to maintain her footing, and you take a short step back, lining up your attack. " +
                            "You wind up and launch a merciless kick right between ${target.possessive(false)} wide-open legs, your instep colliding solidly with ${target.possessive(false)} tender little pussy."
                }
            }
            Result.strong -> {
                if (target.hasBalls) {
                    "Lying on the floor, you feign exhaustion, hoping ${target.name} will lower her guard. As ${target.pronounSubject(false)} approaches unwarily, you suddenly kick up between " +
                            "${target.possessive(false)} legs, delivering a painful hit to ${target.possessive(false)} family jewels."
                } else {
                    "Lying on the floor, you feign exhaustion, hoping ${target.name} will lower her guard. As she approaches unwarily, you suddenly kick up between " +
                            "her legs, delivering a painful hit to her sensitive vulva."
                }
            }
            else -> {
                if (target.hasBalls) {
                    String.format(
                        "You deliver a swift kick between %s's legs, hitting %s squarely on the balls.",
                        target.name,
                        target.pronounTarget(false)
                    )
                } else {
                    "You deliver a swift kick between " + target.name + "'s legs, hitting her squarely on the baby maker."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + "'s kick hits nothing but air."
            }
            Result.special -> {
                user.name + " launches a powerful kick straight at your groin, battering your poor testicles. Your " + target.bottoms.last() + " crumble off your body, adding nudity to injury."
            }
            Result.critical -> {
                user.name + " capitalizes on your momentary inability to defend yourself; with a mischievous smirk, she sweeps the inside of your foot with hers, kicking your legs apart.  " +
                        "Your arms flail out to your sides as you struggle to maintain your footing, but in the split-second it takes you to regain your balance, " + user.name + " has already taken a short step back, " +
                        "and is lining you up for the coup-de-grace.  Smirking slightly, " + user.name + " winds up and launches a merciless kick squarely into your dangling and unprotected testicles."
            }
            Result.strong -> {
                "With " + user.name + " flat on her back, you quickly move in to press your advantage. Faster than you can react, her foot shoots up between " +
                        "your legs, dealing a critical hit on your unprotected balls."
            }
            else -> {
                when (Global.random(2)) {
                    1 -> (user.name + " delivers a swift and confident kick right into your sensitive nutsack.  "
                            + "You can almost taste your balls in the back of your throat.")

                    else -> user.name + "'s foot lashes out into your delicate testicles with devastating force. "
                }
            }
        }
    }

    override fun describe(): String {
        return if (user.getPure(Attribute.Ki) >= 21) {
            "A precise, Ki-powered kick that can destroy clothing"
        } else {
            "Kick your opponent in the groin"
        }
    }
}

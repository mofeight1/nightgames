package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Erebia
import status.Stsflag

class ManaFortification(user: Character) : Skill("Mana Fortification", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.sub(user) &&
                user.canSpend(Pool.MOJO, 80) &&
                !user.has(Stsflag.erebia)
    }

    override fun describe(): String {
        return "Dramatically buffs Power, Speed, Seduction. More mojo increases duration: 80 Mojo + 10/turn"
    }

    override fun resolve(c: Combat, target: Character) {
        var m = user.mojo.current
        m -= 80
        m /= 10
        user.spendMojo(80 + 10 * m)

        writeOutput(c, target)
        user.add(Erebia(user, m + 2), c)
        user.emote(Emotion.confident, 20)
        user.emote(Emotion.dominant, 20)
    }

    override fun copy(user: Character): Skill {
        return ManaFortification(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ("You gather all your available mana in your hand, but instead of releasing it, you absorb it back into your body. "
                + "The magical energy rushes through your limbs, boosting your physical abilities.")
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s gathers up a dangerous quantity of magic in %s hand. You brace yourself for the incoming spell, "
                    + "but instead the mana flows back into %s. Causing %s to glow with power.",
            user.name, user.possessive(false), user.pronounTarget(false), user.pronounTarget(false)
        )
    }
}

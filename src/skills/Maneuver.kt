package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import stance.Behind

class Maneuver(user: Character) : Skill("Maneuver", user) {
    init {
        addTag(Attribute.Cunning)
        addTag(SkillTag.OUTMANEUVER)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                !c.stance.prone(target) &&
                user.canSpend(Pool.MOJO, 8) &&
                !c.stance.behind(user) &&
                user.canAct() &&
                !user.has(Trait.undisciplined) &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(8)
        if (target.has(Trait.reflexes)) {
            writeOutput(c, target, Result.unique)
            user.pain(target.level.toDouble(), Anatomy.genitals)
            user.emote(Emotion.nervous, 10)
            target.emote(Emotion.dominant, 15)
        } else if (c.effectRoll(this, user, target, user[Attribute.Cunning] / 4)) {
            writeOutput(c, target)
            c.stance = Behind(user, target)
            user.emote(Emotion.confident, 15)
            user.emote(Emotion.dominant, 15)
            target.emote(Emotion.nervous, 10)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 20
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 20)
    }

    override fun copy(user: Character): Skill {
        return Maneuver(user)
    }

    override var speed = 8

    override var accuracy = 6

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.unique) {
            return ("You dodge past " + target.name + ", but when you try to grab her, she dodges forward and counters with a heel kick to the groin.<p>"
                    + "<i>\"Way too slow.\"</i> She scolds you as you hold your injured plums.")
        }
        return if (modifier == Result.miss) {
            "You try to get behind " + target.name + " but are unable to."
        } else {
            "You dodge past " + target.name + "'s guard and grab her from behind."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " tries to slip behind you, but you're able to keep her in sight."
        } else {
            user.name + " lunges at you, but when you try to grab her, she ducks out of sight. Suddenly her arms are wrapped around you. How did she get behind you?"
        }
    }

    override fun describe(): String {
        return "Get behind opponent: 8 Mojo"
    }
}

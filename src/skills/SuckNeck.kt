package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Global

class SuckNeck(user: Character) : Skill("Suck Neck", user) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.kiss(user) && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = 0.0
        if (user.getPure(Attribute.Dark) >= 1) {
            writeOutput(c, target, Result.special)
            m = user[Attribute.Dark].toDouble()
            target.weaken(m, c)
            user.heal(m, c)
            user.spendArousal(user[Attribute.Dark])
        } else {
            writeOutput(c, target, Result.normal)
        }
        target.pleasure((3 + m + Global.random(user[Attribute.Seduction] / 2)), Anatomy.neck, combat = c)
        user.buildMojo(5)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 12)
    }

    override fun copy(user: Character): Skill {
        return SuckNeck(user)
    }

    override var speed = 5

    override var accuracy = 7

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Dark) >= 1) {
            "Energy Drain"
        } else {
            name
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You lean in to kiss " + target.name + "'s neck, but she slips away."
            }
            Result.special -> {
                "You draw close to " + target.name + " as she's momentarily too captivated to resist. You run your tongue along her neck and bite gently. She shivers and you " +
                        "can feel the energy of her pleasure flow into you, giving you strength."
            }
            else -> {
                when (Global.random(2)) {
                    1 -> "You lick, kiss, suck, and nibble all over " + target.name + "'s neck, causing her to moan under your ministrations."
                    else -> "You lick and suck " + target.name + "'s neck hard enough to leave a hickey."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " goes after your neck, but you push her back."
            }
            Result.special -> {
                user.name + " presses her lips against your neck. She gives you a hickey and your knees start to go weak. It's like your strength is being sucked out through " +
                        "your skin."
            }
            else -> {
                user.name + " licks and sucks your neck, biting lightly when you aren't expecting it."
            }
        }
    }

    override fun describe(): String {
        return "Suck on opponent's neck. Highly variable effectiveness"
    }
}

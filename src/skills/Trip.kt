package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import stance.StandingOver
import status.Braced
import status.Stsflag

class Trip(user: Character) : Skill("Trip", user) {
    init {
        addTag(Attribute.Cunning)
        addTag(SkillTag.KNOCKDOWN)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(target) &&
                !c.stance.behind(user) &&
                user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.effectRoll(this, user, target, user[Attribute.Cunning] / 4) || !user.check(Attribute.Cunning, target.knockdownDC())) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (user.name === "Reyka" && !c.stance.prone(user) && user.canSpend(Pool.MOJO, 20)) {
            user.spendMojo(20)
            target.pain(user.level / 2.0, Anatomy.genitals)
            //writeOutput(c, target, Result.unique)
            c.write(user, receive(target, Result.unique, 0))
        } else writeOutput(c, target)
        if (c.stance.prone(user) && !user.has(Stsflag.braced)) {
            user.add(Braced(user))
        }
        c.stance = StandingOver(user, target)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 16
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 16)
    }

    override fun copy(user: Character): Skill {
        return Trip(user)
    }

    override var speed = 2

    override var accuracy = 2

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You try to trip " + target.name + ", but she keeps her balance."
        } else {
            "You catch " + target.name + " off balance and trip her."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " hooks your ankle, but you recover without falling."
            }
            Result.unique -> {
                "Reyka steps in close, hooks your ankle and trips you.  As you fall on your back, she quickly plants her foot on your groin, " +
                        "pinning your balls with the tip of her foot.  While it is still quite painful, she seems to be taking great care so as " +
                        "to not press down too hard, and she twists and wiggles her toes to gently grind your nuts.  " +
                        "<i>\"You're lucky I need these things...intact...\"</i> she says with a sympathetic smile."
            }
            else -> {
                user.name + " takes your feet out from under you and sends you sprawling to the floor."
            }
        }
    }

    override fun describe(): String {
        return "Attempt to trip your opponent"
    }
}

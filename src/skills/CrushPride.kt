package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import status.Stsflag

class CrushPride(user: Character) : Skill("Crush Pride", user) {
    init {
        addTag(Attribute.Hypnosis)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Hypnosis) >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Hypnosis to 8)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                (target.has(Stsflag.charmed) || target.has(Stsflag.enthralled)) &&
                target.has(Stsflag.shamed)
    }

    override fun describe(): String {
        return "Exploit opponent's shame to give them extreme masochistic pleasure"
    }

    override fun resolve(c: Combat, target: Character) {
        val sts = target.getStatus(Stsflag.shamed)
        if (sts != null) {
            var m = sts.magnitude
            m += target.getStatus(Stsflag.horny)?.magnitude ?: 0.0
            target.tempt(m * user[Attribute.Hypnosis] * 2, combat = c)
            target.pleasure(1.0, Anatomy.head, combat = c)
        }
        target.removeStatus(sts!!, c)
        writeOutput(c, target)
        target.emote(Emotion.horny, 30)
        target.emote(Emotion.desperate, 20)
    }

    override fun copy(user: Character): Skill {
        return CrushPride(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s is so embarrassed and horny, that it only takes a little push to link the two emotions. You place a "
                    + "simple suggestion in %s head that %s gets off on humiliation. In seconds, %s expression shifts from shame to pure lust.",
            target.name, target.possessive(false), target.pronounSubject(false), target.possessive(false)
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "As your shame threatens to overwhelm you, %s laughs mockingly and points out that your cock is responding "
                    + "eagerly. Your humiliation fetish is getting the better of you, and %s scornful look is almost enough to make you cum. "
                    + "Since when have you gotten off on humiliation? Your mind is too foggy right now. You'll think about it later.",
            user.name, user.possessive(false)
        )
    }
}

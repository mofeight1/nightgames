package skills

import characters.Character
import combat.Combat
import combat.Result
import items.Potion

class UsePotion(user: Character, private val item: Potion) : Skill(item.getName(), user) {
    init {
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) && user.canAct() && user.has(item)
    }

    override fun describe(): String {
        return item.desc
    }

    override fun resolve(c: Combat, target: Character) {
        user.consume(item, 1)
        writeOutput(c, target)
        user.add(item.getEffect(user), c)
    }

    override fun copy(user: Character): Skill {
        return UsePotion(user, item)
    }

    override fun type(): Tactics {
        return item.tactic
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return item.getMessage(user)
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + item.getMessage(user)
    }
}

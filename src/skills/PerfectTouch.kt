package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class PerfectTouch(user: Character) : Skill("Sleight of Hand", user) {
    init {
        addTag(Attribute.Cunning)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !target.isNude &&
                user.canSpend(Pool.MOJO, 40) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(40)
        if (target.has(Trait.reflexes)) {
            writeOutput(c, target, Result.unique)
            user.pain(5.0, Anatomy.head)
            user.emote(Emotion.nervous, 10)
            target.emote(Emotion.dominant, 15)
        } else if (c.effectRoll(this, user, target, user[Attribute.Cunning] / 4)) {
            writeOutput(c, target, Result.normal)
            if (target.isNude) target.nakedLiner()
            target.undress(c)
            target.emote(Emotion.nervous, 10)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.has(Trait.amateurMagician)
    }

    override fun copy(user: Character): Skill {
        return PerfectTouch(user)
    }

    override var speed = 2

    override var accuracy = 1

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.unique -> {
                ("You feint to the left while your right hand grabs " + target.name + "'s clothes. Before you can remove anything, she hits you across the face with a lightning fast backfist. "
                        + "You stumble back, as your head rings from the impact.")
            }
            Result.miss -> {
                "You try to steal " + target.name + "'s clothes off of her, but she catches you."
            }
            else -> {
                when (Global.random(3)) {
                    2 -> "In a flurry of movement, you strip each piece of " + target.name + "'s clothing off. By the time she can block your hands, she's already naked."
                    1 -> "You divert " + target.name + "'s attention with a small show of footwork. While she's distracted, you effortlessly strip each piece of her clothing off before she notices what's happening."
                    else -> "You feint to the left while your right hand makes quick work of " + target.name + "'s clothes. By the time she realizes what's happening, you've " +
                            "already stripped all her clothes off."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " lunges toward you, but you catch her hands before she can get ahold of your clothes."
        } else {
            user.name + " lunges towards you, but dodges away without hitting you. She tosses aside a handful of clothes, at which point you realize you're " +
                    "naked. How the hell did she manage that?"
        }
    }

    override fun describe(): String {
        return "Strips opponent completely: 40 Mojo"
    }
}

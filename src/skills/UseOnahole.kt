package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Global
import items.Attachment
import items.Toy

class UseOnahole(user: Character) : Skill(Toy.Onahole.getFullName(user), user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (user.has(Toy.Onahole) || user.has(Toy.Onahole2))
                && user.canAct() &&
                target.hasDick &&
                c.stance.reachBottom(user) &&
                target.isPantsless &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        var block = 0
        if (c.lastact(target) != null && c.lastact(target)!!.name === Toy.Dildo.getFullName(target)) {
            block = 5
        }
        if (!c.attackRoll(this, user, target) || Global.random(20) < block) {
            writeOutput(c, target, Result.miss, block)
            return
        }
        var m = 6 + (target[Attribute.Perception] / 2.0) + (user[Attribute.Science] / 2.0)
        var level = 1
        if (user.has(Toy.Onahole2)) {
            m += 5
            level++
        }
        if (user.has(Attachment.OnaholeSlimy)) {
            m *= 2
            level++
        }
        writeOutput(c, target, Result.normal, level)
        if (user.has(Attachment.OnaholeVibe)) {
            writeOutput(c, target, Result.upgrade, 1)
        }
        m = user.bonusProficiency(Anatomy.toy, m)
        if (user.has(Toy.Onahole2)) {
            target.pleasure(m, Anatomy.genitals, combat = c)
        } else {
            target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        }
        if (target.human()) {
            if (user.has(Attachment.OnaholeSlimy)) {
                c.offerImage("Onahole4.jpg", "Art by AimlessArt")
            } else if (user.has(Attachment.OnaholeVibe)) {
                c.offerImage("Onahole3.jpg", "Art by AimlessArt")
            }
            if (user.has(Toy.Onahole2)) {
                c.offerImage("Onahole2.jpg", "Art by AimlessArt")
            } else {
                c.offerImage("Onahole1.jpg", "Art by AimlessArt")
            }
        }
    }

    override fun copy(user: Character): Skill {
        return UseOnahole(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (damage > 0) {
                "You try to stick your onahole onto " + target.name + "'s dick, but she shoves a dildo into the entrance, blocking it."
            } else {
                "You try to stick your onahole onto " + target.name + "'s dick, but she manages to avoid it."
            }
        } else if (modifier == Result.upgrade) {
            "The internal vibrator adds to her pleasure."
        } else {
            when (damage) {
                3 -> ("You slide your living onahole over " + target.name + "'s dick. You can feel the toy reshape itself in your hand to perfectly fit the shape of her member. "
                        + "You don't even need to move the toy. You just hold the slime as it eagerly milks " + target.name + "'s rod.")

                2 -> "You slide your onahole over " + target.name + "'s dick. The well-lubricated toy covers her member with minimal resistance. As you pump it, she moans in " +
                        "pleasure and her hips buck involuntarily."

                else -> "You stick your cocksleeve onto " + target.name + "'s erection and rapidly pump it. She squirms a bit at the sensation and can't quite stifle a moan."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (damage > 0) {
                user.name + " tries to stick a cocksleeve on your dick, but you intercept it with the head of your " + Toy.Dildo.getFullName(
                    target
                ) + "."
            } else {
                user.name + " tries to stick a cocksleeve on your dick, but you manage to avoid it."
            }
        } else if (modifier == Result.upgrade) {
            "The toy vibrates around your penis, giving you additional pleasure"
        } else {
            when (damage) {
                3 -> (user.name + " slicks her slimy cocksleeve onto your dick. Before your shaft is even fully inside, the toy expands to engulf your "
                        + "entire cock and balls. You groan with pleasure as it wriggles around and squeezes your genitals as if alive.")

                2 -> user.name + " slides her cocksleeve over your dick and starts pumping it. The sensation is the same as if she was riding you, but you're the only " +
                        "one who's feeling anything."

                else -> user.name + " forces a cocksleeve over your erection and begins to pump it. At first the feeling is strange and a little bit uncomfortable, but the " +
                        "discomfort gradually becomes pleasure."
            }
        }
    }

    override fun describe(): String {
        return "Pleasure opponent with an Onahole"
    }
}

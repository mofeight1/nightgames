package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import status.Rewired

class ShortCircuit(user: Character) : Skill("Short-Circuit", user) {
    init {
        addTag(Attribute.Science)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                target.isNude &&
                user.canSpend(Pool.BATTERY, 3)
    }

    override fun describe(): String {
        return "A blast of energy with confused your opponent's nerves so she can't tell pleasure from pain: 3 Batteries."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 3)
        writeOutput(c, target)
        target.add(Rewired(target, 4 + Global.random(3)), c)
    }

    override fun copy(user: Character): Skill {
        return ShortCircuit(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You send a light electrical current through " + target.name + "'s body, disrupting her nerve endings. She'll temporarily feel pleasure as pain and pain as pleasure."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " aims a devices at you and you feel a strange shiver run across your skin. You feel indescribably weird. She's done something to your sense of touch."
    }
}

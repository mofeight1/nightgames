package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class Paizuri(user: Character) : Skill("Use Breasts", user) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.hasBreasts && user.isTopless &&
                target.hasDick && target.isPantsless &&
                c.stance.reachBottom(user) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        c.offerImage("Paizuri.jpg", "Art by AimlessArt")
        var m = Global.random(6) + user[Attribute.Seduction] / 2.0 + target[Attribute.Perception] / 2.0
        m += user.mojo.current / 10.0
        m = user.bonusProficiency(Anatomy.chest, m)
        target.pleasure(m, Anatomy.genitals, combat = c)
        user.buildMojo(25)
        target.emote(Emotion.horny, 10)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 28 &&
                !user.has(Trait.petite) && user.hasBreasts
    }

    override fun copy(user: Character): Skill {
        return Paizuri(user)
    }

    override var speed = 4

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You rub " + target.name + "'s penis between your breasts."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(2)) {
            1 -> user.name + " gently wraps her beautiful breasts around your cock and starts stroking, working each up and down individually."
            else -> user.name + " squeezes your dick between her soft breasts. She rubs them up and down your shaft and teasingly licks your tip."
        }
    }

    override fun describe(): String {
        return "Rub your opponent's dick between your boobs, more effective at high Mojo"
    }
}

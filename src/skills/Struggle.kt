package skills

import characters.Attribute
import characters.Character
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Cowgirl
import stance.Doggy
import stance.Flying
import stance.Missionary
import stance.Neutral
import stance.ReverseCowgirl
import stance.Stance
import stance.Standing
import stance.StandingOver
import status.Bound
import status.Stsflag
import kotlin.math.max

class Struggle(user: Character) : Skill("Struggle", user) {
    init {
        addTag(SkillTag.ESCAPE)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return ((!c.stance.mobile(user) && !c.stance.dom(user)) || user.bound()) &&
                !user.stunned() &&
                !user.distracted() &&
                !user.has(Stsflag.enthralled)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.bound()) {
            val status = target.getStatus(Stsflag.bound) as Bound?
            if (user.check(Attribute.Power, 10 - user.escape())) {
                if (user.human()) {
                    if (status != null) {
                        c.write(user, "You manage to break free from the $status.")
                    } else {
                        c.write(user, "You manage to snap the restraints that are binding your hands.")
                    }
                } else if (target.human()) {
                    if (status != null) {
                        c.write(user, user.name + " slips free from the " + status + ".")
                    } else {
                        c.write(user, user.name + " breaks free.")
                    }
                }
                user.free()
            } else {
                if (user.human()) {
                    if (status != null) {
                        c.write(user, "You struggle against the $status, but can't get free.")
                    } else {
                        c.write(user, "You struggle against your restraints, but can't get free.")
                    }
                } else if (target.human()) {
                    if (status != null) {
                        c.write(user, user.name + " struggles against the " + status + ", but can't free her hands.")
                    } else {
                        c.write(user, user.name + " struggles, but can't free her hands.")
                    }
                }
            }
        } else if (c.stance.penetration(user)) {
            if (c.stance.en == Stance.anal) {
                if (user.check(
                        Attribute.Power,
                        c.stance.escapeDC(
                            target,
                            user
                        ) + (target.stamina.current / 2 - user.stamina.current / 2) + (target[Attribute.Power] - user[Attribute.Power])
                    )
                ) {
                    if (user.human()) {
                        c.write(user, "You manage to break away from " + target.name + ".")
                    } else if (target.human()) {
                        c.write(user, user.name + " pulls away from you and your dick slides out of her butt.")
                    }
                    c.stance.insert(target)
                } else {
                    if (user.human()) {
                        c.write(user, "You try to pull free, but " + target.name + " has a good grip on your waist.")
                    } else if (target.human()) {
                        c.write(user, user.name + " tries to squirm away, but you have better leverage.")
                    }
                }
            } else {
                if (user.check(
                        Attribute.Power,
                        (
                                c.stance.escapeDC(target, user) +
                                        (target.stamina.current / 4 - user.stamina.current / 4) +
                                        (target[Attribute.Power] / 2 - max(user[Attribute.Power], user[Attribute.Cunning]) / 2))
                    )
                ) {
                    if (user.human()) {
                        if (c.stance.en == Stance.flying) {
                            if (user.getPure(Attribute.Dark) >= 18) {
                                c.stance = Flying(user, target)
                                c.write(
                                    user,
                                    "You struggle with " + target.name + " until she is unable to keep the two of you in the air. "
                                            + "You start to plummet rapidly to the ground. Fortunately you can fly too. You summon your own wings "
                                            + "and snatch " + target.name + " out of the air, thrusting your cock back into her. Now you're in control."
                                )
                            } else if (user.getPure(Attribute.Ninjutsu) >= 5) {
                                c.stance = Neutral(user, target)
                                c.write(
                                    user, "You pry yourself free from your captor and drop to the ground. "
                                            + "The drop would be a problem for someone without ninja training, "
                                            + "but you mange to roll safely and hop back to your feet."
                                )
                            } else {
                                c.write(
                                    user, """
     You manage to shake yourself loose from the demoness.
     Immediatly afterwards you realize letting go of the person holding you a good distance up from the ground may not have been the smartest move you've ever made, as the ground is quickly approaching your face.
     """.trimIndent()
                                )
                                c.stance = c.stance.insert(user)
                            }
                        } else if (c.stance.en == Stance.standing) {
                            c.write(
                                user,
                                "You grab " + target.name + "'s ass and hold her up, taking control of the pace."
                            )
                            c.stance = Standing(user, target)
                        } else if (c.stance.behind(user)) {
                            c.write(
                                user,
                                "You manage unbalance " + target.name + " and push her forward onto her hands and knees. You follow her, still inside her tight wetness, and continue " +
                                        "to fuck her from behind."
                            )
                            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                                c.offerImage("Doggy Style.jpg", "Art by AimlessArt")
                            }
                            c.stance = Doggy(user, target)
                        } else {
                            c.write(
                                user,
                                "You surpise " + target.name + " by hugging her close to your chest, preventing her from using stabilizing her position with her arms. You " +
                                        "roll on top of her into traditional missionary position, careful not to let your cock slip out of her."
                            )
                            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Angel")) {
                                c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri")
                            }
                            c.stance = Missionary(user, target)
                        }
                    } else if (target.human()) {
                        if (c.stance.en == Stance.standing) {
                            c.write(
                                user,
                                user.name + " wraps her legs around your waist and manages to take control of your fucking."
                            )
                            c.stance = Standing(user, target)
                        } else if (c.stance.prone(user)) {
                            c.write(
                                user,
                                user.name + " wraps her legs around your waist and suddenly pulls you into a deep kiss. You're so surprised by this sneak attack that you " +
                                        "don't even notice her roll you onto your back until you feel her weight on your hips. She moves her hips experimentally, enjoying the control " +
                                        "she has in cowgirl position."
                            )
                            if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Cassie")) {
                                c.offerImage("Cowgirl.jpg", "Art by AimlessArt")
                            }
                            c.stance = Cowgirl(user, target)
                        } else if (c.stance.en == Stance.flying) {
                            if (user.getPure(Attribute.Dark) >= 18) {
                                c.stance = Flying(user, target)
                                c.write(
                                    user,
                                    user.name + " spreads her own wings and pulls you into a nose-dive with a strong flap. While you're busy "
                                            + "frantically trying to recover, she secures her grip on you. She catches you before you hit the ground and pulls "
                                            + "you back into the air."
                                )
                            } else if (user.getPure(Attribute.Ninjutsu) >= 5) {
                                c.stance = Neutral(user, target)
                                c.write(
                                    user,
                                    user.name + " shakes herself out of your arms and off your dick. She drops to the ground, but lands "
                                            + "gracefully, thanks to her extensive training."
                                )
                            } else {
                                c.write(
                                    user,
                                    user.name + " shakes herself out of your arms and off your dick. She drops to the ground, landing "
                                            + "hard. You descend rapidly and confirm she's not seriously hurt. Fortunately she seems fine, and also "
                                            + "fortunately, you have the drop on her."
                                )
                                c.stance = c.stance.insert(user)
                            }
                        } else {
                            c.write(
                                user,
                                user.name + " manages to reach between her legs and grab hold of your ballsack, stopping you in mid thrust. She smirks at you over her shoulder " +
                                        "and pushes her butt against you, using the leverage of " +
                                        "your testicles to keep you from backing away to maintain you balance. She forces you onto your back, while never breaking your connection. after " +
                                        "some complex maneuvering, you end up on the floor while she straddles your hips in a reverse cowgirl position."
                            )
                            c.stance = ReverseCowgirl(user, target)
                        }
                    } else {
                        if (user.hasDick || user.has(Trait.strapped)) {
                            if (c.stance.behind(user)) {
                                c.stance = Doggy(user, target)
                                if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                                    c.offerImage("Doggy Style.jpg", "Art by AimlessArt")
                                }
                                if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                                    c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt")
                                }
                            } else if (c.stance.en == Stance.flying) {
                                c.stance = c.stance.insert(user)
                            } else {
                                if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Angel")) {
                                    c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri")
                                    c.offerImage("Angel_Sex.jpg", "Art by AimlessArt")
                                }
                                c.stance = Missionary(user, target)
                            }
                        } else {
                            if (c.stance.prone(user)) {
                                if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Cassie")) {
                                    c.offerImage("Cowgirl.jpg", "Art by AimlessArt")
                                }
                                if (!Global.checkFlag(Flag.exactimages) || user.id == ID.SAMANTHA) {
                                    c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt")
                                }
                                c.stance = Cowgirl(user, target)
                            } else {
                                c.stance = ReverseCowgirl(user, target)
                            }
                        }
                    }
                } else {
                    if (user.human()) {
                        c.write(
                            user,
                            "You try to tip " + target.name + " off balance, but she drops her hips firmly, pushing your cock deep inside her and pinning you to the floor."
                        )
                    } else if (target.human()) {
                        if (c.stance.behind(target)) {
                            c.write(
                                user,
                                user.name + " struggles to gain a more dominant position, but with you behind her, holding her waist firmly, there is nothing she can do."
                            )
                        } else {
                            c.write(
                                user,
                                user.name + " tries to roll on top of you, but you use you superior upper body strength to maintain your position."
                            )
                        }
                    }
                    c.stance.decay()
                }
            }
        } else {
            if (user.check(
                    Attribute.Power,
                    c.stance.escapeDC(
                        target,
                        user
                    ) + (target.stamina.current / 2 - user.stamina.current / 2) + (target[Attribute.Power] - user[Attribute.Power])
                )
            ) {
                if (user.human()) {
                    c.write(user, "You manage to scramble out of " + target.name + "'s grip.")
                } else if (target.human()) {
                    c.write(user, user.name + " squirms out from under you.")
                }
                if (c.stance.prone(user)) {
                    c.stance = StandingOver(target, user)
                }
                c.stance = Neutral(user, target)
            } else {
                if (user.human()) {
                    c.write(
                        user,
                        "You try to free yourself from " + target.name + "'s grasp, but she has you pinned too well."
                    )
                } else if (target.human()) {
                    c.write(user, user.name + " struggles against you, but you maintain your position.")
                }
                c.stance.struggle(c)
                c.stance.decay()
            }
        }
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Struggle(user)
    }

    override var speed = 0

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun describe(): String {
        return "Attempt to escape a submissive position using Power"
    }
}

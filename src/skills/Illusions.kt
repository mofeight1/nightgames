package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Distorted

class Illusions(user: Character) : Skill("Illusions", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.CLONE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canSpend(Pool.MOJO, 10)
    }

    override fun describe(): String {
        return "Create illusions to act as cover: 10 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        writeOutput(c, target)
        user.add(Distorted(user, (user[Attribute.Arcane] / 3).toDouble()), c)
    }

    override fun copy(user: Character): Skill {
        return Illusions(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You cast an illusion spell to create several images of yourself."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " casts a brief spell and your vision is filled with naked copies of her. You can still tell which " + user.name + " is real, but it's still a distraction."
    }
}

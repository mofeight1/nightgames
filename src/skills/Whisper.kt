package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Enthralled
import status.Sensitive

class Whisper(self: Character) : Skill("Whisper", self) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.kiss(user) && user.canAct() && !user.has(Trait.direct)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.has(Trait.darkpromises) && Global.random(5) == 4 && user.canSpend(Pool.MOJO, 15)) {
            writeOutput(c, target, Result.special)
            if (target.human()) {
                if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Angel")) {
                    c.offerImage("Dark Promise.jpg", "Art by AimlessArt")
                }
            }
            user.spendMojo(15)
            target.add(Enthralled(target, user), c)
        }
        if (user.getPure(Attribute.Professional) >= 1) {
            writeOutput(c, target, Result.strong)
            target.add(Sensitive(target, 2, Anatomy.genitals, 1 + user[Attribute.Professional] / 10.0), c)
        } else {
            writeOutput(c, target)
        }

        target.tempt(user[Attribute.Seduction] / 2.0, mod = Result.foreplay, combat = c)
        if (user.has(Trait.secretkeeper)) {
            target.emote(Emotion.nervous, 50)
        }
        target.emote(Emotion.horny, 30)
        user.buildMojo(10)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 32 && !user.has(Trait.direct)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 32)
    }

    override fun copy(user: Character): Skill {
        return Whisper(user)
    }

    override var speed: Int = 9

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                "You whisper words of domination in " + target.name + "'s ear, filling her with your darkness. The spirit in her eyes seems to dim as she submits to your will."
            }
            Result.strong -> {
                "You whisper extraordinarily suggestive promises into " + target.name + "'s ear until " +
                        target.pronounSubject(false) + "'s primed to receive your touch."
            }
            else -> {
                "You whisper sweet nothings in " + target.name + "'s ear. Judging by her blush, it was fairly effective."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " whispers in your ear in some eldritch language." +
                        " Her words echo through your head and you feel a" +
                        " strong compulsion to do what she tells you"
            }
            Result.strong -> {
                user.name + " whispers some deliciously seductive suggestions in your ear. " + user.pronounSubject(false) + " paints such an erotic mental picture " +
                        "for you that you find your dick straining with anticipation."
            }
            else -> {
                user.name + " whispers some deliciously seductive suggestions in your ear."
            }
        }
    }

    override fun describe(): String {
        return "Arouse opponent by whispering in her ear"
    }
}

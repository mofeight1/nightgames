package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Nimble

class CatsGrace(user: Character) : Skill("Cat's Grace", user) {
    init {
        addTag(Attribute.Animism)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Animism) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Animism to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && user.arousal.percent() >= 20
    }

    override fun describe(): String {
        return "Use your instinct to nimbly avoid attacks"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(Nimble(user, 4), c)
    }

    override fun copy(user: Character): Skill {
        return CatsGrace(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You rely on your animal instincts to quicken your movements and avoid attacks."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " focuses for a moment and her movements start to speed up and become more animalistic."
    }
}

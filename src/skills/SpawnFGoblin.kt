package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import pet.FGoblin

class SpawnFGoblin(user: Character) : Skill("Summon Fetish Goblin", user) {
    init {
        addTag(Attribute.Fetish)
        addTag(SkillTag.PET)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.pet == null &&
                user.arousal.current >= 25
    }

    override fun describe(): String {
        return "Summons a hermaphroditic goblin embodying multiple fetishes: Arousal at least 25"
    }

    override fun resolve(c: Combat, target: Character) {
        val power = 3 + user.bonusPetPower() + (user[Attribute.Fetish] / 10)
        val ac = 2 + user.bonusPetEvasion() + (user[Attribute.Fetish] / 10)
        writeOutput(c, target)
        user.pet = FGoblin(user, power, ac)
    }

    override fun copy(user: Character): Skill {
        return SpawnFGoblin(user)
    }

    override fun type(): Tactics {
        return Tactics.summoning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You channel all the fetishes in your twisted libido into a single form. The creature is about 4 feet tall and has a shapely female body covered "
                    + "with bondage gear. Her face is completely obscured by a latex mask, but her big tits and her crotch are completely exposed. She has a large cock, "
                    + "which looks ready to burst if it wasn't tightly bound at the base. Past her heavy sack, you can see sex toys sticking out of both her pussy and ass."
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s shivers and moans as she sinks into her darkest fantasies. Something dangerous is coming. Sure enough a short feminine figure in bondage gear appears "
                    + "before you. Her face is completely obscured by a latex mask, but her big tits and her crotch are completely exposed. She has a large cock, "
                    + "which looks ready to burst if it wasn't tightly bound at the base. Past her heavy sack, you can see sex toys sticking out of both her pussy and ass.",
            user.name
        )
    }
}

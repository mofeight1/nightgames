package skills

import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Consumable
import status.Bound
import status.Stsflag

class Tie(user: Character) : Skill("Bind", user) {
    init {
        addTag(SkillTag.BIND)
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.reachTop(user) &&
                !c.stance.mobile(target) &&
                (user.has(Consumable.ZipTie) || user.has(Consumable.Handcuffs)) &&
                c.stance.dom(user) &&
                !target.has(Stsflag.bound)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.has(Consumable.Handcuffs, 1)) {
            user.consume(Consumable.Handcuffs, 1)
            writeOutput(c, target, Result.special)
            target.add(Bound(target, 40, "handcuffs"), c)
        } else {
            user.consume(Consumable.ZipTie, 1)
            if (!c.attackRoll(this, user, target)) {
                writeOutput(c, target, Result.miss)
                return
            }
            if (user.has(Trait.tieGuy) && user.has(Consumable.ZipTie)) {
                user.consume(Consumable.ZipTie, 1)
                writeOutput(c, target, Result.strong)
                if (target.human()) {
                    if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                        c.offerImage("Zip Tie.jpg", "Art by AimlessArt")
                    }
                }
                target.add(Bound(target, 30, "zipties"), c)
            } else {
                writeOutput(c, target, Result.normal)
                if (target.human()) {
                    if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                        c.offerImage("Zip Tie.jpg", "Art by AimlessArt")
                    }
                }
                target.add(Bound(target, 20, "ziptie"), c)
            }
        }
    }

    override fun copy(user: Character): Skill {
        return Tie(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override var speed = 2

    override var accuracy = 1

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You try to catch " + target.name + "'s hands, but she squirms too much to keep your grip on her."
            }
            Result.special -> {
                "You catch " + target.name + "'s wrists and slap a pair of cuffs on her."
            }
            Result.strong -> {
                ("You catch both of " + target.name + "'s hands and wrap a zip tie around her wrists. You get a second tie "
                        + "around her elbows to make it harder for her to escape.")
            }
            else -> {
                "You catch both of " + target.name + "'s hands and wrap a zip tie around her wrists."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " tries to tie you down, but you keep your arms free."
            }
            Result.special -> {
                user.name + " restrains you with a pair of handcuffs."
            }
            Result.strong -> {
                user.name + " secures your hands with a zip tie and ties a second one around your elbows."
            }
            else -> {
                user.name + " secures your hands with a zip tie."
            }
        }
    }

    override fun describe(): String {
        return "Tie up your opponent's hands with a zip tie"
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import status.Abuff

class Drain(user: Character) : Skill("Drain", user) {
    init {
        addTag(Attribute.Dark)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(this.user) &&
                c.stance.penetration(this.user) &&
                user.canSpend(Pool.MOJO, 20)
    }

    override fun describe(): String {
        return "Drain your opponent of their energy"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        writeOutput(c, target)

        val type = if (Global.random(10) == 0) 6 else Global.random(6)
        when (type) {
            0 -> {
                target.weaken(10.0, c)
                user.heal(10.0, c)
            }

            1 -> {
                target.spendMojo(10)
                user.buildMojo(10)
            }

            2 -> {
                target.add(Abuff(target, Attribute.Power, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Power, 1.0, 20), c)
                target.weaken(10.0, c)
            }

            3 -> {
                target.add(Abuff(target, Attribute.Seduction, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Seduction, 1.0, 20), c)
                target.tempt(10.0, combat = c)
            }

            4 -> {
                target.add(Abuff(target, Attribute.Cunning, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Cunning, 1.0, 20), c)
                target.spendMojo(target.mojo.current)
            }

            5 -> {
                user.pleasure(
                    user.arousal.max - user.arousal.current - 1.0,
                    Anatomy.genitals, combat = c
                )
                target.add(Abuff(target, Attribute.Power, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Power, 1.0, 20), c)
                target.add(Abuff(target, Attribute.Seduction, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Seduction, 1.0, 20), c)
                target.add(Abuff(target, Attribute.Cunning, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Cunning, 1.0, 20), c)
                if (target.getPure(Attribute.Perception) < 9) {
                    target.mod(Attribute.Perception, 1)
                }
                target.weaken(target.stamina.current.toDouble(), c)
                target.stamina.set(1)
                target.spendMojo(target.mojo.current)
                target.pleasure(
                    target.arousal.max - target.arousal.current - 1.0,
                    Anatomy.genitals, combat = c
                )
            }

            6 -> {
                target.add(Abuff(target, Attribute.Power, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Power, 1.0, 20), c)
                target.add(Abuff(target, Attribute.Seduction, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Seduction, 1.0, 20), c)
                target.add(Abuff(target, Attribute.Cunning, -1.0, 20), c)
                user.add(Abuff(user, Attribute.Cunning, 1.0, 20), c)
                if (target.getPure(Attribute.Perception) < 9) {
                    target.mod(Attribute.Perception, 1)
                }
                target.weaken(target.stamina.current.toDouble(), c)
                target.stamina.set(1)
                target.spendMojo(target.mojo.current)
                target.pleasure(
                    target.arousal.max - target.arousal.current - 1.0,
                    Anatomy.genitals, combat = c
                )
            }

            else -> {}
        }
        user.mojo.gainMax(1)
    }

    override fun copy(user: Character): Skill {
        return Drain(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (user.hasPussy) {
            val base = ("You put your powerful vaginal muscles to work whilst"
                    + " transfixing " + target.name
                    + "'s gaze with your own, goading his energy into his cock."
                    + " Soon it erupts from him, ")
            return when (damage) {
                2 -> base + "and you can feel his strength pumping into you."
                3 -> (base + "and you can feel his memories and experiences flow"
                        + " into you, adding to your skill.")

                4 -> (base + "taking his fighting spirit with him and"
                        + " adding it to your own")

                0 -> ("but unfortunately you made a mistake, and only feel a small"
                        + " bit of his energy traversing the space between you.")

                1 -> ("but unfortunately you made a mistake, and only feel a small"
                        + " bit of his energy traversing the space between you.")

                6 -> (base
                        + "far more powerfully than you even thought possible."
                        + " You feel a fragment of his soul break away from him and"
                        + " spew into you, taking with it a portion of his strength,"
                        + " skill and wits and merging with your own. You have clearly"
                        + " won this fight, and a lot more than that.")

                else ->                // Should never happen
                    " but nothing happens, you feel strangely impotent."
            }
        } else {
            val base =
                "With your cock deep inside " + target.name + ", you can feel the heat from her core. You draw the energy from her, mining her depths. "
            return when (damage) {
                2 -> base + "You feel yourself grow stronger as you steal her physical power."
                3 -> base + "You manage to steal some of her sexual experience and skill at seduction."
                4 -> base + "You draw some of her wit and cunning into yourself."
                0 -> "You attempt to drain " + target.name + "'s energy through your intimate connection, taking a bit of her energy."
                1 -> "You attempt to drain " + target.name + "'s energy through your intimate connection, stealing some of her restraint."
                5 -> "You attempt to drain " + target.name + "'s energy through your intimate connection, but it goes wrong. You feel intense pleasure feeding " +
                        "back into you and threatening to overwhelm you. You brink the spiritual link as fast as you can, but you're still left on the brink of " +
                        "climax."

                6 -> (base
                        + "You succeed in siphoning off a portion of her soul, stealing both her physical and mental strength. This energy will eventually " +
                        "return to its owner, but for now, you're very powerful!")

                else ->                // Should never happen
                    " but nothing happens, you feel strangely impotent."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        val base = ("You feel the succubus' pussy suddenly tighten around you. "
                + "She starts kneading your dick bringing you immense pleasure and soon"
                + " you feel yourself erupt into her, but you realize your are shooting"
                + " something far more precious than semen into her; as more of the ethereal"
                + " fluid leaves you, you feel ")
        return when (damage) {
            2 -> (base + "your strength leaving you with it, "
                    + "making you more tired than you have ever felt.")

            3 -> (base
                    + "memories of previous sexual experiences escape your mind,"
                    + " numbing your skills, rendering you more sensitive and"
                    + " perilously close to the edge of climax.")

            4 -> (base + "your mind go numb, causing your confidence and"
                    + "cunning to flow into her.")

            0 -> ("Clearly the succubus is trying to do something really special to you, "
                    + "as you can feel the walls of her vagina squirm against you in a way "
                    + "no human could manage, but all you feel is some drowsiness.")

            1 -> ("Clearly the succubus is trying to do something really special to you, "
                    + "as you can feel the walls of her vagina squirm against you in a way "
                    + "no human could manage, but all you feel is your focus waning somewhat.")

            5 -> user.name + " squeezes you with her pussy and starts to milk you, but you suddenly feel her shudder and moan loudly. Looks like her plan backfired."
            6 -> (base
                    + "something snap loose inside of you and it seems to flow right "
                    + "through your dick and into her. When it is over you feel... empty "
                    + "somehow. At the same time, " + user.name + " seems radiant, looking more powerful,"
                    + " smarter and even more seductive than before. Through all of this,"
                    + " she has kept on thrusting and you are right on the edge of climax."
                    + " Your defeat appears imminent, but you have already lost something"
                    + " far more valuable than a simple sex fight...")

            else ->            // Should never happen
                " nothing. You should be feeling something, but you're not."
        }
    }
}

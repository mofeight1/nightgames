package skills

import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import pet.FairyFem
import pet.FairyFemRoyal
import pet.FairyMale
import pet.FairyMaleRoyal
import pet.Ptype

class SpawnFaerie(user: Character, var gender: Ptype) : Skill("Summon Faerie", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.PET)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        var cost = 15
        if (user.has(Trait.faefriend)) {
            cost = 10
        }
        return user.canActNormally(c) && user.pet == null && user.canSpend(Pool.MOJO, cost)
    }

    override fun describe(): String {
        var cost = 15
        if (user.has(Trait.faefriend)) {
            cost = 10
        }
        return "Summon a Faerie familiar to support you: $cost Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        var cost = 15
        if (user.has(Trait.faefriend)) {
            cost = 10
        }
        user.spendMojo(cost)
        if (user.getPure(Attribute.Arcane) >= 24) {
            val power = 5 + user.bonusPetPower() + (user[Attribute.Arcane] / 10)
            val ac = 8 + user.bonusPetEvasion() + (user[Attribute.Arcane] / 10)
            writeOutput(c, target, Result.strong)
            if (gender == Ptype.fairyfem) {
                user.pet = FairyFemRoyal(user, power, ac)
                c.offerImage("Fairy_Royal.png", "Art by AimlessArt")
            } else {
                user.pet = FairyMaleRoyal(user, power, ac)
            }
        } else {
            val power = 2 + user.bonusPetPower() + (user[Attribute.Arcane] / 10)
            val ac = 4 + user.bonusPetEvasion() + (user[Attribute.Arcane] / 10)
            writeOutput(c, target, Result.normal)
            if (gender == Ptype.fairyfem) {
                user.pet = FairyFem(user, power, ac)
                c.offerImage("Fairy.png", "Art by AimlessArt")
            } else {
                user.pet = FairyMale(user, power, ac)
            }
        }
    }

    override fun copy(user: Character): Skill {
        return SpawnFaerie(user, gender)
    }

    override fun type(): Tactics {
        return Tactics.summoning
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Arcane) >= 24) {
            if (gender == Ptype.fairyfem) {
                "Faerie Princess"
            } else {
                "Faerie Prince"
            }
        } else {
            if (gender == Ptype.fairyfem) {
                "Faerie (female)"
            } else {
                "Faerie (male)"
            }
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            if (gender == Ptype.fairyfem) {
                ("You start a summoning chant and in your mind, seek out a familiar. A beautiful faerie girl appears in front of you. Despite her nakedness, she wears exquisitely crafted "
                        + "jewelry and hair decorations. She flies around you gracefully before landing delicately on your shoulder.")
            } else {
                "You start a summoning chant and in your mind, seek out a familiar. A six inch tall faerie prince winks into existence in response to your invitation. His tiny crown " +
                        "glows with powerful magic as he hovers in the air on dragonfly wings."
            }
        } else {
            if (gender == Ptype.fairyfem) {
                "You start a summoning chant and in your mind, seek out a familiar. A pretty little faerie girl appears in front of you and gives you a friendly wave before " +
                        "landing softly on your shoulder."
            } else {
                "You start a summoning chant and in your mind, seek out a familiar. A six inch tall faerie boy winks into existence in response to your call. The faerie " +
                        "hovers in the air on dragonfly wings."
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is SpawnFaerie) return false
        return toString() === other.toString()
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            user.name + " casts a spell as she extends her hand. In a flash of magic, a small, naked girl with butterfly wings appears in her palm."
        } else {
            user.name + " casts a spell as she extends her hand. In a flash of magic, a small, naked girl with butterfly wings appears in her palm."
        }
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Anal
import status.Shamed

class Invite(user: Character) : Skill("Invite", user) {
    init {
        addTag(Attribute.Submissive)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return ((target.isPantsless || target.has(Trait.strapped))
                && user.isPantsless
                && c.stance.mobile(target)
                && !c.stance.mobile(user)
                && (
                (target.hasDick || (target.has(Trait.strapped)) && !c.stance.behind(user))
                        || !c.stance.behind(target))
                && user.canAct()
                && target.canAct()
                && !c.stance.penetration(user)
                && !c.stance.penetration(target))
    }

    override fun describe(): String {
        return "Invite your opponent to give you a good fucking"
    }

    override fun resolve(c: Combat, target: Character) {
        if (!target.isErect) {
            writeOutput(c, target, Result.miss)
            user.add(Shamed(user), c)
            if (user.arousal.current < 15) {
                user.arousal.set(15)
            }
            return
        }
        val anal = anal(c, target)

        if (anal) {
            writeOutput(c, target, Result.anal)
            c.stance = Anal(target, user)
        } else {
            writeOutput(c, target, Result.normal)
            if (target.human()) {
                c.offerImage("Invite.jpg", "Art by AimlessArt")
            }
            c.stance = c.stance.insert(target)
        }
        var r = (Global.random(5) + user[Attribute.Perception]).toDouble()
        var m = (Global.random(5) + target[Attribute.Perception]).toDouble()
        val a: Anatomy = if (c.stance.anal()) Anatomy.ass
        else Anatomy.genitals
        m = target.bonusProficiency(Anatomy.genitals, m)
        r = user.bonusProficiency(a, r)
        r += user.bonusRecoilPleasure(r)
        user.pleasure(m, a, combat =  c)
        target.pleasure(r, Anatomy.genitals, combat = c)
        if (user.arousal.current < 15) {
            user.arousal.set(15)
        }
    }

    override fun copy(user: Character): Skill {
        return Invite(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You offer your manly pole to " + target.name + ", but she just laughs at you. That's a bit of a blow to your pride."
            }
            Result.anal -> {
                "You spread your buttcheeks, offering your puckered anus. You try to contain a moan as " + target.name + " accepts your invitation and thrusts firmly " +
                        "into you."
            }
            else -> {
                "You slide your eager cock against " + target.name + "'s pussy lips, making it clear what you're craving. She grins, confident in her superior position and " +
                        "maneuvers your member into her tight entrance."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " spreads her lower lips and practically begs you to take her. You hate to disappoint her, but you're not quite hard enough to manage it."
            }
            Result.anal -> {
                user.name + " spreads her tight buttcheeks apart, looking away from you in shame. Well, if she's that eager, you could always use a change of pace. You slide " +
                        "the tip of your cock into her tight anus and thrust the rest of the way inside her."
            }
            else -> {
                user.name + " opens her lower lips in a lewd invitation and looks at you with needy eyes. There's no way you'd turn down such a tempting offer. You shove your " +
                        "cock into her as deep as it can go."
            }
        }
    }

    private fun anal(c: Combat, target: Character): Boolean {
        return (target.hasDick || target.has(Trait.strapped)) && !user.hasPussy && c.stance.behind(target)
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Shield

class Barrier(user: Character) : Skill("Barrier", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.DEFENSIVE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && user.canSpend(Pool.MOJO, 3)
    }

    override fun describe(): String {
        return "Creates a magical barrier to protect you from physical damage: 3 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(3)
        writeOutput(c, target)
        user.add(Shield(user, user[Attribute.Arcane] / 2.0), c)
    }

    override fun copy(user: Character): Skill {
        return Barrier(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You conjure a simple magic barrier around youruser, reducing physical damage. Unfortunately, it will do nothing against a gentle caress."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " holds a hand in front of her and you see a magical barrier appear briefly, before it becomes invisible."
    }
}

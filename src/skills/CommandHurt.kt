package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Global

class CommandHurt(user: Character) : PlayerCommand("Force Pain", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun describe(): String {
        return "Convince your thrall running into the nearest wall is a good idea."
    }

    override fun resolve(c: Combat, target: Character) {
        if (Global.random(2) == 0) {
            target.pain(Global.random(10) + target[Attribute.Power] / 2.0, Anatomy.genitals, c)
            writeOutput(c, target, Result.strong)
        } else {
            target.pain((Global.random(10) + target[Attribute.Speed]).toDouble(), Anatomy.head, c)
            writeOutput(c, target, Result.strong)
        }
    }

    override fun copy(user: Character): Skill {
        return CommandHurt(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.strong) {
            return " You glare at " + target.name + " and point towards your own groin with a nod.  " +
                    target.name + "'s hand reaches out in front of them, then they swiftly and firmly slap themselves in the privates."
        }
        return "Grinning, you point towards the nearest wall. " + target.name +
                " seems confused for a moment, but soon she understands your" +
                " meaning and runs headfirst into it."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ("<<This should not be displayed, please inform The"
                + " Silver Bard: CommandHurt-receive>>")
    }
}

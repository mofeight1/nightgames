package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Bound

class Binding(user: Character) : Skill("Binding", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.BIND)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && user.canSpend(Pool.MOJO,20)
    }

    override fun describe(): String {
        return "Bind your opponent's hands with a magic seal: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        writeOutput(c, target)
        target.add(Bound(target, 10 + user[Attribute.Arcane], "seal"), c)
        target.emote(Emotion.nervous, 5)
        user.emote(Emotion.confident, 20)
        user.emote(Emotion.dominant, 10)
    }

    override fun copy(user: Character): Skill {
        return Binding(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You cast a binding spell on " + target.name + ", holding her in place."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " gestures at you and casts a spell. A ribbon of light wraps around your wrists and holds them in place."
    }
}

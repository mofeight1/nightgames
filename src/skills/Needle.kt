package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import items.Consumable
import status.Drowsy
import status.Horny

class Needle(user: Character) : Skill("Needle Throw", user) {
    init {
        addTag(Attribute.Ninjutsu)
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
            user.canAct() &&
            user.has(Consumable.needle) &&
            user.canUse(Consumable.needle, c) &&
            !c.stance.prone(user)
    }

    override fun describe(): String {
        return "Throw a drugged needle at your opponent."
    }

    override fun resolve(c: Combat, target: Character) {
        user.consume(Consumable.needle, 1)
        if (c.attackRoll(this, user, target)) {
            writeOutput(c, target)
            target.add(Horny(target, 3.0, 4), c)
            target.add(Drowsy(target, 4.0), c)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return Needle(user)
    }

    override var accuracy = 8

    override var speed = 9

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format(
                "You throw a small, drugged needle at %s, but %s dodges it.",
                target.name,
                target.pronounSubject(false)
            )
        } else {
            String.format(
                "You hit %s with one of your drugged needles. She flushes with arousal and appears to have trouble standing.",
                target.name
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format(
                "You spot a glint of metal and dodge out of the way just in time to avoid a small needle. You don't know what %s coated it with, "
                        + "but it's probably a good thing it missed.", user.name
            )
        } else {
            String.format(
                "You feel a tiny prick on your neck and discover you've been stuck by a small needle. You pull it out quickly, but already feel "
                        + "the drug going to work. Your body feels heavy and dull, except for your cock, which springs to attention."
            )
        }
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Stance
import stance.Standing

class Carry(user: Character) : Skill("Carry", user) {
    init {
        addTag(Attribute.Power)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 30 && !user.has(Trait.petite)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !c.stance.prone(target) &&
                user.canFuck &&
                target.isPantsless &&
                user.stamina.current >= 15 &&
                user.canSpend(Pool.MOJO, 10) &&
                user.isErect &&
                target.isErect &&
                c.stance.en != Stance.standing
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        if (!c.effectRoll(this, user, target, user[Attribute.Power] / 4)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target)
        var m = Global.random(5) + target[Attribute.Perception].toDouble()
        if (user.has(Trait.strapped)) {
            m += user[Attribute.Science] / 2.0
            m = user.bonusProficiency(Anatomy.toy, m)
        } else {
            m = user.bonusProficiency(Anatomy.genitals, m)
        }
        target.pleasure(m, Anatomy.genitals, combat = c)
        var r = Global.random(5) + user[Attribute.Perception].toDouble()
        r = target.bonusProficiency(Anatomy.genitals, r)
        r += target.bonusRecoilPleasure(r)
        user.pleasure(r, Anatomy.genitals, combat = c)

        c.stance = Standing(user, target)
        if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara") || user.name.startsWith("Mara")) {
            c.offerImage("Carry.jpg", "AimlessArt")
        }
    }

    override fun copy(user: Character): Skill {
        return Carry(user)
    }

    override var accuracy = 2

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                "As you go to scoop up " + target.name + ", she slides back, right out of your grip."
            } else {
                "You pick up " + target.name + ", but she scrambles out of your arms."
            }
        } else {
            if (Global.random(2) == 0) {
                "You swing " + target.name + " up into air and position her so she comes sinking down onto your cock. You both clench at each other while your cock buries itself deeper inside her pussy."
            } else {
                "You scoop up " + target.name + ", lifting her into the air and simultaneously thrusting your dick into her hot depths. She lets out a noise that's " +
                        "equal parts surprise and delight as you bounce her on your pole."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                user.name + " launches herself at you with her arms and legs spread but you dodge her at the last moment."
            } else {
                user.name + " jumps onto you, but you deposit her back on the floor."
            }
        } else {
            if (Global.random(2) == 0) {
                (user.name + " jumps up and latches on to you. You're about to toss her to the ground when "
                        + "she slides herself onto your twitching cock. She wraps her legs around you, leaving you "
                        + "have no choice to support the both of you, pulling yourself deeper into her warm depths in the process.")
            } else {
                user.name + " leaps into your arms and impales herself on your cock. She wraps her legs around your torso and you quickly support her so she doesn't " +
                        "fall and injure herself or you."
            }
        }
    }

    override fun describe(): String {
        return "Picks up opponent and penetrates her: Mojo 10."
    }
}

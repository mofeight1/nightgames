package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Horny

class KoushoukaBurst(user: Character) : Skill("Fertility Rite", user) {
    init {
        addTag(Attribute.Ninjutsu)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 30)
    }


    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                !target.canAct() &&
                target.isPantsless &&
                user.canSpend(Pool.MOJO, 25) &&
                c.stance.reachBottom(user)
    }

    override fun describe(): String {
        return "A lengthy ritual that causes an overwhelming urge to have sex: 25 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(25)
        writeOutput(c, target)
        target.add(Horny(target, 20.0, 10), c)
    }

    override fun copy(user: Character): Skill {
        return KoushoukaBurst(user)
    }

    override var speed = 1

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (target.hasBalls) {
            String.format(
                "You carefully grab %s balls between your fingers and quickly complete the gestures Yui showed you. This "
                        + "should briefly boost %s sperm and testosterone production, giving %s an uncontrollable need to reproduce.",
                target.name, target.possessive(false), target.pronounTarget(false)
            )
        } else {
            String.format(
                "You make a series of gestures on %s bare abdomen, which should, in theory, stimulate the pressure points "
                        + "around her ovaries and womb. You see her face quickly flush as she starts to tremble with an intense need for sex.",
                target.name
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s kneels between your legs and carefully holds your bare balls. %s makes a series of hand movements, "
                    + "while muttering something under %s breath. When %s finishes, a desperate heat starts to grow in your testicles, "
                    + "and you feel overwhelmed by a desire to cum.",
            user.name, user.pronounSubject(true), user.possessive(false), user.pronounSubject(false)
        )
    }
}

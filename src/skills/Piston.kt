package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Stance
import status.ProfMod
import status.ProfessionalMomentum

class Piston(user: Character) : Skill("Piston", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.dom(user) && c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        var m = user.getSexPleasure(3, Attribute.Seduction) + target[Attribute.Perception]
        val inside: Anatomy
        if (c.stance.en == Stance.anal) {
            if (c.stance.behind(user)) {
                writeOutput(c, target, Result.anal)
            } else {
                writeOutput(c, target, Result.upgrade)
            }
            inside = Anatomy.ass
        } else {
            writeOutput(c, target, Result.normal)
            if (user.human()) {
                if (c.stance.behind(user)) {
                    if (!Global.checkFlag(Flag.exactimages) || target.id == ID.KAT) {
                        c.offerImage("Doggy Style.jpg", "Art by AimlessArt")
                    }
                    if (!Global.checkFlag(Flag.exactimages) || target.id == ID.MARA) {
                        c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt")
                    }
                } else {
                    if (!Global.checkFlag(Flag.exactimages) || target.id == ID.ANGEL) {
                        c.offerImage("Angel_Sex.jpg", "Art by AimlessArt")
                        c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri")
                    }
                    c.offerImage("Thrust.jpg", "Art by AimlessArt")
                }
            }
            inside = Anatomy.genitals
        }
        m += user.mojo.current / 10.0
        if (user.has(Trait.strapped)) {
            m += user[Attribute.Science] / 2.0
            m = user.bonusProficiency(Anatomy.toy, m)
        }
        if (user.getPure(Attribute.Professional) >= 11) {
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Sexual Momentum", user, Anatomy.genitals, user[Attribute.Professional] * 5.0), c)
            }
        }
        target.pleasure(m, inside, Result.finisher, c)
        var r = target.getSexPleasure(3, Attribute.Seduction, inside)
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }

        user.pleasure(r, Anatomy.genitals, Result.finisher, c)
        user.buildMojo(20)
        c.stance.pace = 2
    }

    override fun copy(user: Character): Skill {
        return Piston(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal || modifier == Result.upgrade) {
            "You pound " + target.name + " in the ass. She whimpers in pleasure and can barely summon the strength to hold herself off the floor."
        } else {
            "You rapidly pound your dick into " + target.name + "'s pussy. Her pleasure filled cries are proof that you're having an effect, but you're feeling it " +
                    "as much as she is."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.anal -> {
                user.name + " relentlessly pegs you in the ass as you groan and try to endure the sensation."
            }
            Result.upgrade -> {
                user.name + " pistons into you while pushing your shoulders on the ground. While her Strap-On stimulates your prostate, " +
                        user.name + "'s tits are shaking above your head."
            }
            else -> {
                user.name + " enthusiastically moves her hips, bouncing on your cock. Her intense movements relentlessly drive you both toward orgasm."
            }
        }
    }

    override fun describe(): String {
        return "Fucks opponent without holding back. Very effective, but dangerous"
    }
}

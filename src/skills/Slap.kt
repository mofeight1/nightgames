package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class Slap(user: Character) : Skill("Slap", user) {
    init {
        addTag(SkillTag.PAINFUL)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachTop(user) &&
                user.canAct() &&
                !user.has(Trait.softheart) &&
                !c.stance.behind(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (user.getPure(Attribute.Animism) >= 12) {
            writeOutput(c, target, Result.special)
            if (user.has(Trait.pimphand)) {
                target.pain(
                    (Global.random((16 * (user.arousal.percent() / 100)).toInt()) + user[Attribute.Power] / 2 + target[Attribute.Perception]).toDouble(),
                    Anatomy.head,
                    c
                )
                target.calm((Global.random(4) + 2).toDouble(), c)
                target.emote(Emotion.nervous, 40)
                target.emote(Emotion.angry, 30)
                user.buildMojo(20)
            } else {
                target.pain(
                    (Global.random((12 * (user.arousal.percent() / 100) + 1).toInt()) + user[Attribute.Power] / 2).toDouble(),
                    Anatomy.head,
                    c
                )
                target.calm((Global.random(5) + 4).toDouble(), c)
                target.emote(Emotion.nervous, 25)
                target.emote(Emotion.angry, 30)
                user.buildMojo(10)
            }
        } else {
            writeOutput(c, target)
            if (user.has(Trait.pimphand)) {
                target.pain((Global.random(8) + 5 + target[Attribute.Perception]).toDouble(), Anatomy.head, c)
                target.calm((Global.random(4) + 2).toDouble(), c)
                target.emote(Emotion.nervous, 20)
                target.emote(Emotion.angry, 60)
                user.buildMojo(20)
            } else {
                target.pain((Global.random(5) + 4).toDouble(), Anatomy.head, c)
                target.calm((Global.random(5) + 4).toDouble(), c)
                target.emote(Emotion.nervous, 10)
                target.emote(Emotion.angry, 60)
                user.buildMojo(10)
            }
        }
    }

    override fun requirements(user: Character): Boolean {
        return !user.has(Trait.softheart) &&
                (user.getPure(Attribute.Power) >= 7 || user.has(Trait.pimphand)) &&
                !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 7)
    }

    override fun copy(user: Character): Skill {
        return Slap(user)
    }

    override var speed = 8

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Animism) >= 12) {
            "Tiger Claw"
        } else {
            "Slap"
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                target.name + " avoids your slap."
            }
            Result.special -> {
                "You channel your bestial power and strike" + target.name + " with a solid open hand strike."
            }
            else -> {
                "You slap " + target.name + "'s cheek; not hard enough to really hurt her, but enough to break her concentration."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " tries to slap you but you catch her wrist."
            }
            Result.special -> {
                user.name + "'s palm hits you in a savage strike that makes your head ring."
            }
            else -> {
                user.name + " slaps you across the face, leaving a stinging heat on your cheek."
            }
        }
    }

    override fun describe(): String {
        return "Slap opponent across the face"
    }
}

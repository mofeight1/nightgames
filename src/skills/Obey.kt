package skills

import characters.Character
import combat.Combat
import combat.Result
import status.Stsflag

class Obey(user: Character) : Skill("Obey", user) {
    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.has(Stsflag.enthralled)
    }

    override fun describe(): String {
        return "Obey the succubus' every command"
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.human()) c.write(user, "You patiently await your mistress' command")
        else if (target.human()) c.write(user, user.name + " stares ahead blankly, waiting for her orders.")
    }

    override fun copy(user: Character): Skill {
        return Obey(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

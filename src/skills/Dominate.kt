package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.StandingOver
import status.Stsflag

class Dominate(user: Character) : Skill("Dominate", user) {
    init {
        addTag(Attribute.Dark)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.sub(user) &&
                !c.stance.prone(user) &&
                !c.stance.prone(target) &&
                user.canAct()
    }

    override fun describe(): String {
        return "Overwhelm your opponent to force her to lie down: 10 Arousal"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendArousal(10)
        if (target.has(Stsflag.enthralled)) {
            writeOutput(c, target, Result.strong)
        } else {
            writeOutput(c, target, Result.normal)
        }
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Cassie")) {
                c.offerImage("Dominating.jpg", "Art by AimlessArt")
            }
        } else if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Angel")) {
                c.offerImage("Dominated.jpg", "Art by AimlessArt")
            }
        }
        c.stance = StandingOver(user, target)
        user.emote(Emotion.dominant, 20)
        target.emote(Emotion.nervous, 20)
    }

    override fun copy(user: Character): Skill {
        return Dominate(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.strong) {
            return "You take a deep breath, gathering dark energy into your lungs. You expend the power to command " + target.name + " to submit. The demonic command renders her " +
                    "unable to resist and she drops to floor, spreading her legs open to you. As you step closer, she thrusts her hips off the floor, inviting you to take her."
        }
        return "You take a deep breath, gathering dark energy into your lungs. You expend the power to command " + target.name + " to submit. The demonic command renders her " +
                "unable to resist and she drops to floor, spreading her legs open to you. As you approach, she comes to her senses and quickly closes her legs. Looks like her " +
                "will is still intact."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.strong) {
            return user.name + " forcefully orders you to \"Kneel!\" Your body complies without waiting for your brain and you drop to your knees in front of her. She smiles and " +
                    "pushes you onto your back. You present yourself as best you can, eager to please your master."
        }
        return user.name + " forcefully orders you to \"Kneel!\" Your body complies without waiting for your brain and you drop to your knees in front of her. She smiles and " +
                "pushes you onto your back. By the time you break free of her suggestion, you're flat on the floor with her foot planted on your chest."
    }
}

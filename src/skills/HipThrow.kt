package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.StandingOver

class HipThrow(user: Character) : Skill("HipThrow", user) {
    init {
        addTag(Attribute.Power)
    }

    override fun requirements(user: Character): Boolean {
        return user.has(Trait.judonovice)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                c.stance.mobile(target) &&
                !c.stance.prone(user) &&
                !c.stance.prone(target) &&
                user.canAct() &&
                !c.stance.penetration(user) &&
                user.canSpend(Pool.MOJO, 10)
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        if (user.check(Attribute.Power, target.knockdownDC())) {
            val m = Global.random(6) + target[Attribute.Power] / 2.0
            writeOutput(c, target)
            target.pain(m, Anatomy.back, c)
            c.stance = StandingOver(user, target)
            target.emote(Emotion.angry, 5)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return HipThrow(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.normal) {
            target.name + " rushes toward you, but you step in close and pull her towards you, using her momentum to throw her across your hip and onto the floor."
        } else {
            "As " + target.name + " advances, you pull her towards you and attempt to throw her over your hip, but she steps away from the throw and manages to keep her footing."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.normal) {
            "You see a momentary weakness in " + user.name + "'s guard and lunge toward her to take advantage of it. The next thing you know, you're hitting the floor behind her."
        } else {
            user.name + " grabs your arm and pulls you off balance, but you manage to plant your foot behind her leg sweep. This gives you a more stable stance than her and she has " +
                    "to break away to stay on her feet."
        }
    }

    override fun describe(): String {
        return "Throw your opponent to the ground, dealing some damage: 10 Mojo"
    }
}

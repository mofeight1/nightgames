package skills

import characters.Character
import combat.Combat
import combat.Result
import stance.Neutral

class Release(user: Character) : Skill("Release", user) {
    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.dom(user)
    }

    override fun describe(): String {
        return "Release your opponent and return to neutral standing position."
    }

    override fun resolve(c: Combat, target: Character) {
        c.stance = Neutral(target, user)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return Release(user)
    }

    override fun type(): Tactics {
        return Tactics.negative
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You back off and let " + target.name + " recover."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " takes a step back and lets you recover."
    }
}

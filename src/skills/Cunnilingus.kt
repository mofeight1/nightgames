package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.ReverseMount
import stance.SixNine
import status.Enthralled
import status.ProfMod
import status.ProfessionalMomentum

class Cunnilingus(user: Character) : Skill("Lick Pussy", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.oral)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                target.hasPussy &&
                c.stance.oral(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        val pro = user.getPure(Attribute.Professional) >= 5
        val silvertongue = user.has(Trait.silvertongue)


        val res: Result
        if (pro) res = Result.special
        else if (silvertongue) res = Result.strong
        else res = Result.normal
        var succubus = 0
        if (target.has(Trait.succubus) && Global.random(5) == 0) {
            user.tempt(5.0, combat = c)
            if (target[Attribute.Dark] >= 6 && Global.random(2) == 0) {
                succubus = -2
            }
            else succubus = -1
        }
        writeOutput(c, target, res, succubus)
        if (succubus == -2) user.add(Enthralled(user, target), c)
        if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Yui")) {
            c.offerImage("Cunnilingus.jpg", "Art by AimlessArt")
        }
        c.offerImage("Cunnilingus2.jpg", "Art by AimlessArt")

        var m: Double
        if (silvertongue)
            m = target[Attribute.Perception] + Global.random(8) + 2 * user[Attribute.Seduction] / 3.0
        else m = target[Attribute.Perception] + Global.random(6) + user[Attribute.Seduction] / 2.0
        if (pro) {
            m += user[Attribute.Professional]
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Oral Momentum", user, Anatomy.mouth, user[Attribute.Professional] * 5.0), c)
            }
        }
        m += user.mojo.current / 10
        if (target.has(Trait.lickable)) {
            m *= 1.5
        }
        m = user.bonusProficiency(Anatomy.mouth, m)
        target.pleasure(m, Anatomy.genitals, combat = c)
        if (c.stance is ReverseMount) {
            c.stance = SixNine(user, target)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 10
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 10)
    }

    override fun copy(user: Character): Skill {
        return Cunnilingus(user)
    }

    override var speed = 2

    override var accuracy = 6

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            return if (Global.random(2) == 0) {
                "You try to get between " + target.name + "'s legs to use your tongue but she kicks you away."
            } else {
                "You try to eat out " + target.name + ", but she pushes your head away."
            }
        }
        if (target.arousal.current < 10) {
            return if (Global.random(2) == 0) {
                "You part " + target.name + "'s dry lips with your wet tongue and lap between them until her own lubrication starts to mix with yours."
            } else {
                "You run your tongue over " + target.name + "'s dry vulva, lubricating it with your saliva."
            }
        }
        if (modifier == Result.special) {
            return ("Your skilled tongue explores "
                    + target.name
                    + "'s pussy, finding and pleasuring her more sensitive areas. You frequently tease her clitoris until she "
                    + "can't suppress her pleasured moans."
                    + (if (damage == -1) " Under your skilled ministrations, her juices flow freely, and they have an unmistakable"
                    + " effect on you"
            else "")
                    + (if (damage == -2) " You feel a strange pull on you mind,"
                    + " somehow she has managed to enthrall you with her juices"
            else ""))
        }
        if (target.arousal.percent() > 80) {
            return ("You relentlessly lick and suck the lips of "
                    + target.name
                    + "'s pussy as she squirms in pleasure. You let up just for a second before kissing her"
                    + " swollen clit, eliciting a cute gasp."
                    + (if (damage == -1) " The highly aroused succubus' vulva is dripping with her "
                    + "aphrodisiac juices and you consume generous amounts of them"
            else "")
                    + (if (damage == -2) " You feel a strange pull on you mind,"
                    + " somehow she has managed to enthrall you with her juices"
            else ""))
        }
        val r = Global.random(4)
        if (r == 0) {
            return ("You gently lick "
                    + target.name
                    + "'s pussy and sensitive clit."
                    + (if (damage == -1) " As you drink down her juices, they seem to flow "
                    + "straight down to your crotch, lighting fires when they arrive"
            else "")
                    + (if (damage == -2) " You feel a strange pull on you mind,"
                    + " somehow she has managed to enthrall you with her juices"
            else ""))
        }
        if (r == 1) {
            return ("You thrust your tongue into "
                    + target.name
                    + "'s hot vagina and lick the walls of her pussy."
                    + (if (damage == -1) " Your tongue tingles with her juices,"
                    + " clouding your mind with lust"
            else "")
                    + (if (damage == -2) " You feel a strange pull on you mind,"
                    + " somehow she has managed to enthrall you with her juices"
            else ""))
        }
        if (r == 2) {
            return ("Your own enjoyment of pleasing your partner feeds in to " + target.name + "'s reactions as she moans with every flick of your tongue."
                    + (if (damage == -1) (" Your tongue tingles with her juices,"
                    + " clouding your mind with lust"
                    ) else "")
                    + (if (damage == -2) (" You feel a strange pull on you mind,"
                    + " somehow she has managed to enthrall you with her juices"
                    ) else ""))
        }
        return ("You locate and capture "
                + target.name
                + "'s clit between your lips and attack it with your tongue"
                + (if (damage == -1) " Her juices taste wonderful and you cannot"
                + " help but desire more" else "")
                + (if (damage == -2) " You feel a strange pull on you mind,"
                + " somehow she has managed to enthrall you with her juices"
        else ""))
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        val special = when (damage) {
            -1 -> String.format(
                " Your aphrodisiac juices manage to arouse %s as much as %s aroused you.",
                user.name, user.pronounSubject(false)
            )

            -2 -> String.format(
                " Your tainted juices quickly reduce %s into a willing thrall.",
                user.name
            )

            else -> ""
        }
        if (modifier == Result.miss) {
            return String.format(
                "%s tries to tease your cunt with %s mouth, but you push %s face away from you.",
                user.name, user.possessive(false), user.possessive(false)
            )
        } else if (modifier == Result.special) {
            return String.format(
                "%s skilled tongue explores your pussy, finding and pleasuring your more sensitive areas. "
                        + "%s repeatedly attacks your clitoris until you can't suppress your pleasured moans.%s",
                user.name, user.pronounSubject(true), special
            )
        }
        return String.format(
            "%s locates and captures your clit between %s lips and attacks it with %s tongue.%s",
            user.name, user.possessive(false), user.possessive(false), special
        )
    }

    override fun describe(): String {
        return "Perform cunnilingus on opponent, more effective at high Mojo"
    }
}

package skills

import java.awt.Color

enum class Tactics(val group: TacticGroup, val color: Color) {
    damage(TacticGroup.Hurt, Color(150, 0, 0)),
    pleasure(TacticGroup.Pleasure, Color.PINK),
    fucking(TacticGroup.Pleasure, Color(255, 100, 200)),
    positioning(TacticGroup.Positioning, Color(0, 100, 0)),
    stripping(TacticGroup.Positioning, Color(0, 100, 0)),
    recovery(TacticGroup.Recovery, Color.WHITE),
    calming(TacticGroup.Recovery, Color.WHITE),
    status(TacticGroup.Manipulation, Color.CYAN),
    summoning(TacticGroup.Manipulation, Color.YELLOW),
    misc(TacticGroup.Misc, Color(200, 200, 200)),
    preparation(TacticGroup.Manipulation, Color.GREEN),
    negative(TacticGroup.Misc, Color(200, 200, 200)),
    demand(TacticGroup.Demand, Color.RED)
}
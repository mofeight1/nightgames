package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import items.Toy

class Nurple(user: Character) : Skill("Twist Nipples", user) {
    init {
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.MISCHIEF)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 13 && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 13)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isTopless && c.stance.reachTop(user) && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        val m = Global.random(9) + target[Attribute.Perception]
        if (user.has(Toy.ShockGlove) && user.canSpend(Pool.BATTERY, 2)) {
            writeOutput(c, target, Result.special)
            user.spend(Pool.BATTERY, 2)
            target.pain(m.toDouble(), Anatomy.chest, c)
            target.calm(m.toDouble(), c)
        } else {
            writeOutput(c, target)
            target.pain((m / 2).toDouble(), Anatomy.chest, c)
            target.calm((m / 2).toDouble(), c)
        }
        user.buildMojo(10)
        target.emote(Emotion.angry, 25)
    }

    override fun copy(user: Character): Skill {
        return Nurple(user)
    }

    override var speed = 7

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun toString(): String {
        return if (user.has(Toy.ShockGlove)) {
            "Shock Breasts"
        } else {
            name
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You grope at " + target.name + "'s breasts, but miss."
            }
            Result.special -> {
                "You grab " + target.name + "'s boob with your shock-gloved hand, painfully shocking her."
            }
            else -> {
                when (Global.random(2)) {
                    1 -> "You grab each of " + target.name + "'s nipples in your hands and wrench them in opposite direction, eliciting a sharp yelp from her."
                    else -> "You pinch and twist " + target.name + "'s nipples, causing her to yelp in surprise."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " tries to grab your nipples, but misses."
            }
            Result.special -> {
                user.name + " touches your nipple with her glove and a jolt of electricity hits you."
            }
            else -> {
                when (Global.random(2)) {
                    1 -> user.name + " twists your sensitive nipples, giving you a jolt of pain."
                    else -> user.name + " takes your nipples in her fingers and starts twisting, not stopping until she hears a small cry of pain."
                }
            }
        }
    }

    override fun describe(): String {
        return "Twist opponent's nipples painfully"
    }
}

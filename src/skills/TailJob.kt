package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class TailJob(user: Character) : Skill("Tailjob", user) {
    init {
        addTag(Attribute.Animism)
        addTag(SkillTag.TAIL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Animism) >= 6
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                target.isPantsless &&
                c.stance.mobile(user) &&
                !c.stance.mobile(target) &&
                !c.stance.penetration(target) &&
                user.has(Trait.tailed)
    }

    override fun describe(): String {
        return "Use your tail to tease your opponent"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        target.pleasure(
            user[Attribute.Animism] * user.arousal.percent() / 100 + Global.random(target[Attribute.Perception]),
            Anatomy.genitals,
            combat = c
        )
    }

    override fun copy(user: Character): Skill {
        return TailJob(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You skillfully use your flexible tail to stroke and tease " + target.name + "'s sensitive girl parts."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " teases your sensitive dick and balls with her soft tail. It wraps completely around your shaft and strokes firmly."
    }
}

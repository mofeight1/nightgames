package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Stance
import status.Shamed

class TailPeg(user: Character) : Skill("Tail Peg", user) {
    init {
        addTag(Attribute.Dark)
        addTag(Result.anal)
        addTag(SkillTag.TAIL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) > 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.arousal.current >= 30 &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 20) &&
                target.isPantsless &&
                c.stance.en != Stance.standing &&
                c.stance.en != Stance.standingover &&
                !(c.stance.en == Stance.anal && c.stance.dom(user)) &&
                user.has(Trait.tailed)
    }

    override fun describe(): String {
        return "Shove your tail up your opponent's ass."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var shamed = false
        if (Global.random(4) == 2) {
            target.add(Shamed(target), c)
            shamed = true
        }
        if (c.stance.penetration(user)) writeOutput(c, target, Result.special)
        else if (c.stance.dom(target)) writeOutput(c, target, Result.critical)
        else if (c.stance.behind(user)) writeOutput(c, target, Result.strong)
        else writeOutput(c, target, Result.normal)
        if (shamed) {
            if (target.human()) {
                c.write(
                    user, "The shame of having your ass violated by " + user.name + " has destroyed your confidence."
                )
            } else if (user.human()) {
                c.write(
                    user, "Her face flushes in shame at the foreign object invading her " + "ass."
                )
            }
        }
        target.pleasure(10.0 + Global.random(10), Anatomy.ass, combat = c)
        target.pain(3.0 + Global.random(5), Anatomy.ass, c)
        target.emote(Emotion.nervous, 10)
        target.emote(Emotion.desperate, 10)
        user.emote(Emotion.confident, 15)
        user.emote(Emotion.dominant, 25)
    }

    override fun copy(user: Character): Skill {
        return TailPeg(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.critical -> String.format(
                "With your superior position, %s is helpless to resist as the tip of your tail teases her anus. "
                        + "You decide to go even further and thrust it several inches into the tight hole.",
                target.name
            )

            Result.miss -> String.format(
                "You try to sneak your tail into %s's back door, but she catches it and pushes it away.",
                target.name
            )

            Result.normal -> String.format(
                "You close in on %s, and your tail curls around outside her peripheral vision. Before she realizes "
                        + "what's happening, you thrust it right up her bum.", target.name
            )

            Result.special -> String.format(
                "As your cock fills %s pussy, your prehensile tail snakes between her butt cheeks and thrusts into "
                        + "her ass.", target.name
            )

            Result.strong -> String.format(
                "Holding %s from behind, you have great access to her bare ass. You thrust your tail inside, making her let out a startled "
                        + "yelp.", target.name
            )

            else -> "<<This should not be displayed, please inform The Silver Bard: TailPeg-deal>>"
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.critical -> ("Smiling down on you, "
                    + user.name
                    + " spreads your legs and tickles your butt with her tail."
                    + " You notice how the tail itself was slick and wet as it"
                    + " slowly pushes through your anus, spreading your cheeks apart. "
                    + user.name
                    + " pumps it in and out a for a few times before taking "
                    + "it out again.")

            Result.miss -> (user.name
                    + " tries to peg you with her tail but you manage to push"
                    + " your butt cheeks together in time to keep it out.")

            Result.normal -> (user.name
                    + " suddenly moves very close to you. You expect an attack from the front"
                    + " and try to move back, but end up shoving her tail right up your ass.")

            Result.special -> (user.name
                    + " smirks and wiggles her tail behind her back. You briefly look "
                    + "at it and the see the appendage move behind you. You try to keep it"
                    + " out by clenching your butt together, but a squeeze of "
                    + user.name
                    + "'s vagina breaks your concentration, so the tail slides up your ass"
                    + " and you almost lose it as your cock and ass are stimulated so thoroughly"
                    + " at the same time.")

            Result.strong -> (user.name
                    + " hugs you from behind and rubs her chest against your back."
                    + " Distracted by that, she managed to push her tail between your"
                    + " ass cheeks and started tickling your prostrate with the tip.")

            else -> "<<This should not be displayed, please inform The Silver Bard: TailPeg-receive>>"
        }
    }
}

package skills

enum class TacticGroup {
    All,
    Pleasure,
    Positioning,
    Hurt,
    Misc,
    Recovery,
    Manipulation,
    Flask,
    Potion,
    Demand,
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Horny
import status.ProfMod

class LustOverflow(user: Character) : Skill("Lust Overflow", user) {
    init {
        addTag(Attribute.Dark)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(user) &&
                c.stance.penetration(user) &&
                user.canSpend(Pool.MOJO, 20)
    }

    override fun describe(): String {
        return "Dramatically increases power of sex moves, but increases arousal each turn: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.spendMojo(20)
        user.add(Horny(user, 10.0, 10), c)
        user.add(ProfMod("Sex Mastery", user, Anatomy.genitals, 200.0), c)
        target.emote(Emotion.horny, 50)
        user.emote(Emotion.horny, 50)
        target.emote(Emotion.desperate, 30)
    }

    override fun copy(user: Character): Skill {
        return LustOverflow(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You channel the power of your lust into your cock, letting the infernal energy guide your hips."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (user.hasDick) {
            String.format(
                "%s spreads %s wings as %s dark aura turns inward. %s cock seems to expand, as its power starts to overwhelm you.",
                user.name, user.possessive(false), user.possessive(false), user.possessive(true)
            )
        } else {
            String.format(
                "%s spreads %s wings as %s dark aura turns inward. "
                        + "You suddenly feel the sensation of %s pussy on your dick change as it actively starts to milk you.",
                user.name, user.possessive(false), user.possessive(false), user.possessive(false)
            )
        }
    }
}

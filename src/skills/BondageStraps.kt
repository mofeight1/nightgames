package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import items.Clothing
import status.Stsflag

class BondageStraps(user: Character) : Skill("Bondage Straps", user) {
    init {
        addTag(Attribute.Fetish)
        addTag(SkillTag.DRESSING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.has(Stsflag.bondage) &&
                user.isNude &&
                user.canSpend(Pool.MOJO, 20) &&
                !c.stance.penetration(user) && !c.stance.penetration(target)
    }

    override fun describe(): String {
        return "Summon a kinky outfit suitable for your bondage fetish: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.canWear(Clothing.bharness)) {
            user.wear(Clothing.bharness)
        }
        if (user.canWear(Clothing.bstraps)) {
            user.wear(Clothing.bstraps)
        }
        user.spendMojo(20)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return BondageStraps(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "The power of your fetish manifests as a kinky leather body harness, digging tightly into your body."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " glows with kinky energy and is suddenly covered with tight, leather straps."
    }
}

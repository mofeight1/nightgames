package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import items.Attachment
import items.Toy
import status.Stsflag

class BerserkerBarrage(user: Character) : Skill("Berserker Barrage", user) {
    init {
        addTag(Attribute.Discipline)
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.CROP)
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Discipline) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Discipline to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (user.has(Toy.Crop) || user.has(Toy.Crop2) || user.has(Toy.Crop3)) &&
                user.canAct() &&
                c.stance.mobile(user) &&
                user.has(Stsflag.broken) &&
                user.canSpend(Pool.MOJO, 20) &&
                (c.stance.reachTop(user) || c.stance.reachBottom(user)) &&
                !user.human()
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)

        var m = 10 + Global.random(14) + target[Attribute.Perception] + (user[Attribute.Science] / 2.0) + user[Attribute.Discipline]
        m = user.bonusProficiency(Anatomy.toy, m)
        if (user.has(Toy.Crop3)) {
            m *= 2.5
        } else if (user.has(Attachment.CropShocker)) {
            m *= 2
        }
        writeOutput(c, target)
        target.pain(m, Anatomy.ass, c)
        target.emote(Emotion.nervous, 30)
    }

    override fun copy(user: Character): Skill {
        return BerserkerBarrage(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Something within you snaps and anger overwhelms you so much you lose all sense of what you're doing for a few moments. " +
                "By the time you realize what you're doing again, the only evidence to what happened in the lost time is a set of marks on " + target.name + "'s skin, " +
                "evidence to a series of vicious strikes with your riding crop. You almost feel sorry for her. Almost."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "<i>\"Grrrahhhh!\"</i> The way cry barely serves as warning before " + user.name + " lets loose with a flurry of strikes with her riding crop. " +
                "You try in vain to shield yourself from the strikes, but there's nothing you can do to keep from being hit again and again by her crop, " +
                "each strike inflicting more pain than the last. When it's all over, you catch sight of " + user.name + " " +
                "looking at you in mild confusion, as if she doesn't even know quite what she's just done herself. Not that that'll make you forgive her for this."
    }

    override fun describe(): String {
        return "Wildly strike your opponent with repeated crop strikes"
    }
}

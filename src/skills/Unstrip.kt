package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import global.Modifier

class Unstrip(user: Character) : Skill("Unstrip", user) {
    init {
        addTag(Attribute.Temporal)
        addTag(SkillTag.DRESSING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 8)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                user.canSpend(Pool.TIME, 6) &&
                user.isNude &&
                !user.human() &&
                c.hasModifier(Modifier.nudist) &&
                !c.stance.penetration(user) &&
                !c.stance.penetration(target)
    }

    override fun describe(): String {
        return "Rewinds your clothing's time to when you were still wearing it: 6 charges"
    }

    override fun resolve(c: Combat, target: Character) {
        user.change(Modifier.normal)
        user.spend(Pool.TIME, 6)
        writeOutput(c, target)
        user.emote(Emotion.confident, 20)
    }

    override fun copy(user: Character): Skill {
        return Unstrip(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "It's tricky, but with some clever calculations, you restore the state of your outfit. Your outfit from the "
                    + "start of the night reappears on your body."
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You lose sight of %s for just a moment and almost do a double-take when you see %s again, fully dressed. "
                    + "In the second you looked away, how did %s find the time to put %s clothes on?!",
            user.name, user.pronounTarget(false), user.pronounSubject(false), user.possessive(false)
        )
    }
}

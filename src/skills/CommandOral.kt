package skills

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class CommandOral(user: Character) : PlayerCommand("Force Oral", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return super.usable(c, target) &&
                user.isPantsless &&
                c.stance.reachBottom(user)
    }

    override fun describe(): String {
        return "Force your opponent to go down on you."
    }

    override fun resolve(c: Combat, target: Character) {
        val silvertounge = target.has(Trait.silvertongue)
        val lowStart = user.arousal.current < 15
        user.pleasure(
            (if (silvertounge) 8.0 else 5.0)
                    + Global.random(10),
            Anatomy.genitals, combat = c
        )
        user.buildMojo(30)
        val lowEnd = user.arousal.current < 15
        val res = if (lowStart) {
            if (lowEnd) Result.weak
            else Result.strong
        } else Result.normal
        writeOutput(c, target, res)
    }

    override fun copy(user: Character): Skill {
        return CommandOral(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.normal -> target.name + " is ecstatic at being given the privilege of" +
                    " pleasuring you and does a fairly good job at it, too. She" +
                    " sucks your hard dick powerfully while massaging your balls."

            Result.strong -> target.name + " seems delighted to 'help' you, and makes short work" +
                    " of taking your flaccid length into her mouth and getting it " +
                    "nice and hard."

            Result.weak -> target.name + " tries her very best to get you ready by running" +
                    " her tongue all over your groin, but even" +
                    " her psychically induced enthusiasm can't get you hard."

            else -> "<<This should not be displayed, please inform The" +
                    " Silver Bard: CommandOral-deal>>"
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "<<This should not be displayed, please inform The" +
                " Silver Bard: CommandOral-receive>>"
    }
}

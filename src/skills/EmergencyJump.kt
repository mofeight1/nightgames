package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import stance.Behind

class EmergencyJump(user: Character) : Skill("Emergency Jump", user) {
    init {
        addTag(Attribute.Temporal)
        addTag(SkillTag.ESCAPE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 4
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 4)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return ((
                c.stance.sub(user) &&
                        !c.stance.mobile(user) &&
                        !c.stance.penetration(user) &&
                        !c.stance.penetration(target)) || user.bound()) &&
                !user.stunned() &&
                !user.distracted() &&
                user.canSpend(Pool.TIME, 2)
    }

    override fun describe(): String {
        return "Escape from a disadvantageous position and/or bind: 2 charges"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.TIME, 2)
        user.free()
        c.stance = Behind(user, target)
        writeOutput(c, target)
        user.emote(Emotion.confident, 15)
        target.emote(Emotion.nervous, 15)
    }

    override fun copy(user: Character): Skill {
        return EmergencyJump(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You're in trouble for a moment, so you trigger your temporal manipulator and stop time just long "
                    + "enough to free yourself."
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You thought you had %s right where you want %s, but %s seems to vanish completely and escape.",
            user.name, user.pronounTarget(false), user.pronounSubject(false)
        )
    }
}

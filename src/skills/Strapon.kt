package skills

import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import items.Clothing
import items.Toy

class Strapon(user: Character) : Skill("Strap On", user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.isPantsless &&
                (user.has(Toy.Strapon) || user.has(Toy.Strapon2)) &&
                !user.hasDick &&
                !c.stance.penetration(user) &&
                !c.stance.penetration(target) &&
                !user.human()
    }

    override fun describe(): String {
        return "Put on the strapon dildo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.wear(Clothing.strapon)
        if (user.has(Toy.Strapon2)) {
            c.drop(user, Toy.Strapon2)
        }
        if (user.has(Toy.Strapon)) {
            c.drop(user, Toy.Strapon)
        }
        writeOutput(c, target)
        user.buildMojo(10)
        target.buildMojo(-10)
        target.emote(Emotion.nervous, 10)
        user.emote(Emotion.confident, 30)
        user.emote(Emotion.dominant, 40)
    }

    override fun copy(user: Character): Skill {
        return Strapon(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You put on a strap on dildo."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " straps on a thick rubber cock and grins at you in a way that makes you feel a bit nervous."
    }
}

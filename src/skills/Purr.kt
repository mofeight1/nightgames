package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Global
import status.Charmed

class Purr(user: Character) : Skill("Purr", user) {
    init {
        addTag(Attribute.Animism)
        addTag(SkillTag.CHARMING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Animism) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Animism to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.mobile(user) && user.arousal.percent() >= 20
    }

    override fun describe(): String {
        return "Purr cutely to try to charm your opponent"
    }

    override fun resolve(c: Combat, target: Character) {
        if (Global.random(target.level) <= user[Attribute.Animism] * user.arousal.percent() / 100 &&
            target.mood != Emotion.angry && target.mood != Emotion.desperate) {
            writeOutput(c, target)
            target.add(Charmed(target, 2 + user.bonusCharmDuration()), c)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return Purr(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            when (Global.random(2)) {
                1 -> "You let out a soft purr and give " + target.name + " your best puppy dog eyes. She smiles, but then aims a quick punch at your groin, which you barely avoid. " +
                        "Maybe you shouldn't have mixed your animal metaphors."

                else -> "You start moving closer to " + target.name + ", giving her your most charming purr. Unfortunately, this only gives her an easier time lining up an attack, which barely misses your face."
            }
        } else {
            when (Global.random(2)) {
                1 -> "You give " + target.name + " an affectionate purr and your most disarming smile. Her battle aura melts away and she pats your head, completely taken with your " +
                        "endearing behavior."

                else -> "You get close to " + target.name + " and purr gently in her ear. She's taken in by your charm, so much so that she forgets she supposed to be fighting you for a moment."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            when (Global.random(2)) {
                1 -> user.name + " slumps submissively and purrs. It's cute, but she's not going to get the better of you."
                else -> user.name + " gives you an adorable smile and a gentle purr as she starts inching toward you. It's cute, but not cute enough to stop you from flicking her forehead to get her to back up."
            }
        } else {
            when (Global.random(2)) {
                1 -> user.name + " purrs cutely, and looks up at you with sad eyes. Oh God, she's so adorable! It'd be mean to beat her too quickly. Maybe you should let her get some " +
                        "attacks in while you enjoy watching her earnest efforts."

                else -> user.name + " moves in close to you. As you brace for an attack, she instead gives you a cute smile and a soft purr, catching you completely off-guard."
            }
        }
    }
}

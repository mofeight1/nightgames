package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import status.Shamed
import status.Stsflag

class Humiliate(user: Character) : Skill("Humiliate", user) {
    init {
        addTag(Attribute.Hypnosis)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Hypnosis) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Hypnosis to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                !c.stance.sub(user) &&
                (target.has(Stsflag.charmed) || target.has(Stsflag.enthralled))
    }

    override fun describe(): String {
        return "Use hypnotic suggestion to fill your opponent with shame."
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        if (target.has(Stsflag.shamed)) {
            val sts = target.getStatus(Stsflag.shamed)
            if (sts != null) {
                target.add(Shamed(target, target.getStatusMagnitude("Shamed")), c)
            } else {
                target.add(Shamed(target), c)
            }
        } else {
            target.add(Shamed(target), c)
        }
        target.emote(Emotion.nervous, 30)
        target.emote(Emotion.desperate, 20)
    }

    override fun copy(user: Character): Skill {
        return Humiliate(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You manipulate all of %s's remaining inhibitions, making %s believe %s "
                    + "is in the most shameful situation %s can imagine.",
            target.name, target.pronounTarget(false), target.pronounSubject(false), target.pronounSubject(false)
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s points out that you are exposing quite a lot to the audience. Somehow you failed to notice "
                    + "that you are naked and visibly aroused while the entire student body is watching. The Games are usually a "
                    + "more private matter, you didn't even realize you were the center of so much attention before %s said something.",
            user.name, user.pronounSubject(false)
        )
    }
}

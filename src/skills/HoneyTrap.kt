package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import kotlin.math.max

class HoneyTrap(user: Character) : Skill("Honey Trap", user) {
    init {
        addTag(Attribute.Submissive)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user[Attribute.Submissive] >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (target.isPantsless || target.has(Trait.strapped))
                && user.isPantsless
                && c.stance.mobile(target)
                && !c.stance.mobile(user)
                && (target.hasDick || target.has(Trait.strapped))
                && (!c.stance.behind(user) || !c.stance.behind(target))
                && user.canAct()
                && target.canAct()
                && !c.stance.penetration(user)
                && !c.stance.penetration(target)
                && user.canSpend(Pool.MOJO, 20)
    }

    override fun describe(): String {
        return "A sneak attack right at the moment of greatest anticipation: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        writeOutput(c, target)
        if (target.human()) {
            if (Global.random(5) >= 3) {
                c.write(user, user.bbLiner())
            }
        }

        val m = maxOf(user[Attribute.Power], user[Attribute.Cunning], user[Attribute.Seduction]).toDouble()
        var bonus = user[Attribute.Submissive]
        if (target.mood == Emotion.dominant || target.mood == Emotion.horny) {
            bonus *= 2
        }
        target.pain(m + bonus, Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 2, c)
        } else {
            target.calm(m, c)
        }
    }

    override fun copy(user: Character): Skill {
        return HoneyTrap(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You slide your eager cock against %s's pussy lips, making it clear what you're craving. She grins, confident in her victory, and positions herself over you. "
                    + "You wait patiently until the moment her guard is completely down. Before she can fuck you, you deliver a sudden painful slap to her exposed pussy. For a moment, her "
                    + "expression is pure shock, then her eyes water as the pain hits her.", target.name
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s opens her lower lips in a lewd invitation and looks at you with needy eyes. There's no way you'd turn down such a tempting offer. You line up your cock with "
                    + "her dripping entrance and prepare to penetrate her. Suddenly her knee darts up and impacts your dangling balls. It's so fast that for a moment, you aren't even sure what happened. "
                    + "The pain hits you like a truck, and you're soon unable to do anything but clutch your poor testicles.",
            user.name
        )
    }
}

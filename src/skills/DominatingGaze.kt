package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Masochistic
import status.Stsflag

class DominatingGaze(user: Character) : Skill("Dominating Gaze", user) {
    init {
        addTag(Attribute.Discipline)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Discipline) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Discipline to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                user.has(Stsflag.composed) &&
                (target.mood == Emotion.nervous || target.mood == Emotion.desperate) &&
                user.canSpend(Pool.MOJO, 10) &&
                !target.has(Stsflag.masochism)
    }

    override fun describe(): String {
        return "Use your dominant demeanor to put your opponent in a masochistic mood"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.spendMojo(10)
        target.add(Masochistic(target), c)
    }

    override fun copy(user: Character): Skill {
        return DominatingGaze(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "As " + target.name + " looks at you with a hint of nervousness in her eyes, you meet her gaze. You are going to hurt her. You know that. She knows that. So wouldn’t it be a lot better if she simply allowed herself to enjoy it, as you make her hurt in just the right way? " + target.name + " shudders slightly, and you press your advantage. You let her know that all she has to do is let you take control, and just worry about enjoying herself.<p>" +
                "" + target.name + " seems to shrink into herself as your presence overwhelms her. You’ve got her."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "You make the mistake of looking directly at " + user.name + "’s face, letting your nervousness show through. " + user.name + " isn’t one to let this go, unfortunately. <i>\"You really needn’t be so concerned,\"</i> she says, though her smile says otherwise. <i>\"Please don’t get me wrong, I do plan to let you feel the delicious taste of my crop many more times, but would that truly be so bad?\"</i> A chill runs up your spine as she says this, and it only gets worse as she leans toward you. <i>\"Why don’t you let me show you just how enjoyable it can be to let your Mistress punish you? Leave everything to me.\"</i><p>" +
                "You feel like you’re shrinking into yourself right now, but you can’t help but admit that the thought of letting " + user.name + " take control and hurt you as she wishes sounds rather enticing right now.\n"
    }
}

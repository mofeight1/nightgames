package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Abuff

class LegLock(user: Character) : Skill("Leg Lock", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.PAINFUL)
        addTag(SkillTag.CRIPPLING)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.dom(user) &&
                c.stance.reachBottom(user) &&
                c.stance.prone(target) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (c.attackRoll(this, user, target)) {
            writeOutput(c, target)
            target.add(Abuff(target, Attribute.Speed, -2.0, 5), c)
            target.pain(Global.random(10) + 7.0, Anatomy.leg, c)
            target.emote(Emotion.angry, 25)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 24 && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 24)
    }

    override fun copy(user: Character): Skill {
        return LegLock(user)
    }

    override var speed: Int = 2

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You grab " + target.name + "'s leg, but she kicks free."
        } else {
            "You take hold of " + target.name + "'s ankle and force her leg to extend painfully."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " tries to put you in a leglock, but you slip away."
        } else {
            user.name + " pulls your leg across her in a painful submission hold."
        }
    }

    override fun describe(): String {
        return "A submission hold on your opponent's leg"
    }
}

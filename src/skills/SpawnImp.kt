package skills

import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import pet.ImpFem
import pet.ImpMale
import pet.Ptype

class SpawnImp(user: Character) : Skill("Summon Imp", user) {
    constructor(user: Character, gender: Ptype) : this(user) {
        this.gender = gender
    }
    private lateinit var gender: Ptype
    init {
        addTag(Attribute.Dark)
        addTag(SkillTag.PET)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.pet == null &&
                user.canSpend(Pool.MOJO, 10)
    }

    override fun describe(): String {
        return "Summon a demonic Imp: 15 arousal"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendArousal(15)
        var type = Result.normal
        var power = 3 + user.bonusPetPower() + (user[Attribute.Dark] / 10.0)
        var ac = 2 + user.bonusPetEvasion() + (user[Attribute.Dark] / 10)
        if (user.has(Trait.royalguard)) {
            power += 6
            ac += 6
            type = Result.strong
        }
        writeOutput(c, target, type)
        if (gender == Ptype.impfem) {
            user.pet = ImpFem(user, power, ac)
            c.offerImage("FemaleImp.png", "Art by AimlessArt")
        } else {
            user.pet = ImpMale(user, power, ac)
        }
    }

    override fun copy(user: Character): Skill {
        return SpawnImp(user, gender)
    }

    override fun type(): Tactics {
        return Tactics.summoning
    }

    override fun toString(): String {
        return if (gender == Ptype.impfem) {
            "Imp (female)"
        } else {
            "Imp (male)"
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is SpawnImp) return false
        return this.toString() === other.toString()
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (gender == Ptype.impfem) {
            "You focus your dark energy and summon a minion to fight for you. A naked, waist high, female imp steps out of a small burst of flame. She stirs up her honey " +
                    "pot and despite yourself, you're slightly affected by the pheromones she's releasing."
        } else {
            "You focus your dark energy and summon a minion to fight for you. A brief burst of flame reveals a naked imp. He looks at " + target.name + " with hungry eyes " +
                    "and a constant stream of pre-cum leaks from his large, obscene cock."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " spreads out her dark aura and a demonic imp appears next to her in a burst of flame. The imp stands about waist height, with bright red hair, " +
                "silver skin and a long flexible tail. It's naked, clearly female, and surprisingly attractive given its inhuman features."
    }
}

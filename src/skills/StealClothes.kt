package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result

class StealClothes(user: Character) : Skill("Steal", user) {
    init {
        addTag(Attribute.Ninjutsu)
        addTag(SkillTag.DRESSING)
        addTag(SkillTag.STRIPPING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        val canWearTop = c.stance.reachTop(user) && !target.isTopless && user.canWear(target.tops.last().item)
        val canWearBottom = c.stance.reachBottom(user) && !target.isPantsless && user.canWear(target.bottoms.last().item)
        return (canWearBottom || canWearTop) &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 10)
    }

    override fun describe(): String {
        return "Steal and put on an article of clothing: 10 Mojo."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        if (c.stance.reachTop(user) && !target.isTopless && user.canWear(target.tops.last().item)) {
            writeOutput(c, target, Result.top)
            val stolen = target.stripOwner(0, c)!!
            c.clothespile.remove(stolen)
            user.wear(stolen)
        } else if (c.stance.reachBottom(user) && !target.isPantsless && user.canWear(target.bottoms.last().item)) {
            writeOutput(c, target, Result.bottom)
            val stolen = target.stripOwner(1, c)!!
            c.clothespile.remove(stolen)
            user.wear(stolen)
        }
    }

    override fun copy(user: Character): Skill {
        return StealClothes(user)
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override var speed = 6

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.top) {
            "Using your speed and trickery, you swipe ${target.name}'s ${target.tops.last()} and put on your ill-gotten new duds."
        } else {
            "Using your speed and trickery, you swipe ${target.name}'s ${target.bottoms.last()} and put on your ill-gotten spoils."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.top) {
            "${user.name} dodges pash you, fast enough that you lose sight of ${user.pronounTarget(false)}. You notice the absence of your ${target.tops.last()} at almost the same time as you spot ${user.pronounTarget(false)} wearing it."
        } else {
            "${user.name} dodges pash you, fast enough that you lose sight of ${user.pronounTarget(false)}. You notice the absence of your ${target.bottoms.last()} at almost the same time as you spot ${user.pronounTarget(false)} wearing them."
        }
    }
}

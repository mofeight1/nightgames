package skills

import characters.Character
import combat.Combat
import combat.Result
import global.Global
import status.Stsflag

class Distracted(user: Character) : Skill("Distracted", user) {
    override fun requirements(user: Character): Boolean {
        return false
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.distracted() && !user.has(Stsflag.enthralled)
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return Distracted(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(4)) {
            0 -> "You can't seem to keep your focus."
            1 -> "Your head just isn't in the game right now."
            2 -> "You feel out of it and fail to do anything meaningful."
            else -> "You miss your opportunity to act."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(3)) {
            0 -> user.name + " must have her head in the clouds, but you aren't one to miss an open opportunity.."
            1 -> user.name + " doesn't seem like her mind is keeping up with the rest of her and is full of openings."
            else -> user.name + " looks a little unfocused and makes no attempt to defend herself."
        }
    }

    override fun describe(): String {
        return "Caught off guard"
    }
}

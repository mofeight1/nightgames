package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.ReverseMount
import stance.SixNine
import status.ProfMod
import status.ProfessionalMomentum

class Blowjob(user: Character) : Skill("Blow", user) {
    init {
        addTag(Result.oral)
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                target.hasDick &&
                c.stance.oral(user) &&
                !c.stance.behind(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        val pro = user.getPure(Attribute.Professional) >= 5
        val silvertongue = user.has(Trait.silvertongue)

        val res: Result
        if (pro) res = Result.special
        else if (silvertongue) res = Result.strong
        else res = Result.normal
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Cassie")) {
                c.offerImage("Blowjob.png", "Art by Phanaxial")
            }
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.SAMANTHA) {
                c.offerImage("Samanatha Blowjob.jpg", "Art by AimlessArt")
            }
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.SELENE) {
                c.offerImage("Selene Blowjob.jpg", "Art by AimlessArt")
            }
            c.offerImage("Blowjob.jpg", "Art by AimlessArt")
        }
        writeOutput(c, target, res)

        var m: Double
        if (silvertongue)
            m = target[Attribute.Perception] + Global.random(8) + 2 * user[Attribute.Seduction] / 3.0
        else m = target[Attribute.Perception] + Global.random(6) + user[Attribute.Seduction] / 2.0
        if (pro) {
            m += user[Attribute.Professional]
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Oral Momentum", user, Anatomy.mouth, user[Attribute.Professional] * 5.0), c)
            }
        }
        m += user.mojo.current / 10.0
        if (target.has(Trait.lickable)) {
            m *= 1.5
        }
        m = user.bonusProficiency(Anatomy.mouth, m)
        target.pleasure(m, Anatomy.genitals, combat =  c)
        if (c.stance is ReverseMount) {
            c.stance = SixNine(user, target)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 10
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 10)
    }

    override var accuracy = 6

    override fun copy(user: Character): Skill {
        return Blowjob(user)
    }

    override var speed = 2

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when {
            modifier == Result.miss -> {
                if (Global.random(2) == 0) {
                    "You thrust your head towards " + target.name + "'s penis, but miss and it brushes against your cheek instead."
                } else {
                    "You try to take " + target.name + "'s penis into your mouth, but she manages to pull away."
                }
            }
            target.arousal.current < 15 -> {
                "You suck on " + target.name + " flaccid little penis until it grows into an intimidatingly large erection."
            }
            target.arousal.percent() >= 90 -> {
                if (Global.random(2) == 0) {
                    ("You suck away on " + target.name + "'s girl-cock and you've developed a new appreciation for all the blowjobs you've "
                            + "received in the past. You lick and slurp the underside of the shaft before choking on a mix of your "
                            + "own saliva and precum. Thankfully, this display only seems to turn her on more.")
                } else {
                    target.name + "'s girl-cock seems ready to burst, so you suck on it strongly and attack the glans with your tongue fiercely."
                }
            }
            modifier == Result.strong -> {
                "You put your skilled tongue to good use tormenting and teasing her unnatural member."
            }
            modifier == Result.special -> {
                when (Global.random(3)) {
                    0 -> String.format(
                        "You go to town on %s's cock, licking it all over."
                                + " Long, slow licks along the shaft and small, swift licks"
                                + " around the head cause %s to groan in pleasure.",
                        target.name, target.pronounTarget(false)
                    )

                    1 -> String.format(
                        "You lock your lips around the head of %s's hard, wet dick "
                                + "and suck on it forcefully while swirling your tongue rapidly"
                                + " around. At the same time, your hands massage and"
                                + " caress every bit of sensitive flesh not covered by"
                                + " your mouth.",
                        target.name
                    )

                    else -> String.format(
                        "You are bobbing up and down now, hands still working"
                                + " on any exposed skin while you lick, suck and even nibble all over %s's"
                                + " over-stimulated manhood. %s is not even trying to hide %s"
                                + " enjoyment, and %s grunts loudly every time your teeth graze"
                                + " %s shaft.",
                        target.name, target.pronounSubject(true),
                        target.possessive(false), target.pronounSubject(false), target.possessive(false)
                    )
                }
            }
            else -> {
                if (Global.random(2) == 0) {
                    ("You settle into a rhythm and find yourself surprisingly sucked into teasing " + target.name + "'s cock with your mouth. "
                            + "Based on her groans, you are definitely doing something right. As if in confirmation, she thrusts "
                            + "her girl-cock against the back of your throat and you both jerk in reaction.")
                } else {
                    "You feel a bit odd, faced with " + target.name + "'s rigid cock, but as you lick and suck on it, you discover the taste is quite palatable. Besides, " +
                            "making " + target.name + " squirm and moan in pleasure is well worth it."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when {
            modifier == Result.miss -> {
                if (Global.random(2) == 0) {
                    user.name + "clearly intends to try and suck you off, so as she lowers her head, you juke to the side instead."
                } else {
                    user.name + " tries to suck your cock, but you pull your hips back to avoid her."
                }
            }
            target.arousal.current < 15 -> {
                if (Global.random(2) == 0) {
                    "Your penis reacts almost immediately to the warmth of " + user.name + "'s mouth and instantly hardens in her mouth."
                } else {
                    user.name + " takes your soft penis into her mouth and sucks on it until it hardens"
                }
            }
            target.arousal.percent() >= 90 -> {
                user.name + " laps up the precum leaking from your cock and takes the entire length into her mouth, sucking relentlessly"
            }
            modifier == Result.strong -> {
                user.name + "'s soft lips and talented tongue work over your dick, drawing out dangerously irresistible pleasure with each touch."
            }
            modifier == Result.special -> {
                when (Global.random(3)) {
                    0 -> String.format(
                        "%s goes to town on your cock, licking it all over."
                                + " Long, slow licks along the shaft and small, swift licks"
                                + " around the head cause you to groan in pleasure.",
                        user.name
                    )

                    1 -> String.format(
                        "%s locks %s lips around the head of your hard and wet dick "
                                + "and sucks on it forcefully while swirling %s tongue rapidly"
                                + " around. At the same time, %s hands are massaging and"
                                + " caressing every bit of sensitive flesh not covered by"
                                + " %s mouth.",
                        user.name, user.possessive(false), user.possessive(false),
                        user.possessive(false), user.possessive(false)
                    )

                    else -> String.format(
                        "%s is bobbing up and down now, hands still working"
                                + " on any exposed skin while %s licks, sucks and even nibbles all over your"
                                + " over-stimulated manhood. you are not even trying to hide your"
                                + " enjoyment, and you grunt loudly every time %s teeth graze"
                                + " your shaft.",
                        user.name, user.pronounSubject(false),
                        user.possessive(false)
                    )
                }
            }
            else -> {
                val r = Global.random(4)
                when (r) {
                    0 -> {
                        user.name + " runs her tongue up the length of your dick, sending a jolt of pleasure up your spine. She slowly wraps her lips around your dick and sucks."
                    }
                    1 -> {
                        user.name + " sucks on the head of your cock while her hand strokes the shaft."
                    }
                    2 -> {
                        user.name + " licks her way down to the base of your cock and gently sucks on your balls."
                    }
                    else -> {
                        user.name + " runs her tongue around the glans of your penis and teases your urethra."
                    }
                }
            }
        }
    }

    override fun describe(): String {
        return "Lick and suck your opponent's dick, more effective at high Mojo"
    }
}

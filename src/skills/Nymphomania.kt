package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import pet.Ptype
import status.BD
import status.Horny
import status.Masochistic
import status.Shamed

class Nymphomania(user: Character) : Skill("Nymphomania", user) {
    init {
        addTag(Attribute.Fetish)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                user.arousal.current >= 100 &&
                !c.stance.prone(user) &&
                user.pet != null &&
                user.pet!!.type() == Ptype.fgoblin
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.pet!!.remove()
        target.add(Masochistic(target), c)
        target.add(BD(target), c)
        target.add(Shamed(target), c)
        target.add(Horny(target, 5.0, 5), c)
        target.emote(Emotion.horny, 60)
    }

    override fun copy(user: Character): Skill {
        return Nymphomania(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You pull off your fetish goblin's cock ring and fill her with sexual energy. She orgasms hard, shooting a stream of "
                    + "jizz that hits %s right in the face. The goblin's potent seed is capable of spreading her perverse and overactive libido. Soon, "
                    + "%s is bombarded by conflicting desires.", target.name, target.name
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s grabs her goblin by the dick and quickly strokes her to ejaculation. The load hits you like a squirt gun, and you "
                    + "immediately feel feverish. A dozen new fetishes vie for your attention. Oh God. Everything in the world is so sexual! Does the "
                    + "fetish goblin feel this way all the time? You aren't sure whether to pity her or envy her.",
            user.name
        )
    }
}

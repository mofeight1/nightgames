package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result

class Delusion(user: Character) : Skill("Delusion", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Unknowable) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Unknowable to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canSpend(Pool.ENIGMA, 2) &&
                user.canAct() &&
                c.stance.penetration(user)
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.ENIGMA, 2)
        if (target[Attribute.Spirituality] >= 2) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (c.isWatching(ID.ANGEL)) {
            writeOutput(c, target, Result.defended)
            return
        }
        var m = user.getSexPleasure(3, Attribute.Seduction) + target[Attribute.Perception] + user[Attribute.Unknowable]
        m += user.mojo.current / 5.0

        writeOutput(c, target)

        target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        var r = target.getSexPleasure(3, Attribute.Seduction, Anatomy.genitals)
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }
        user.pleasure(r, Anatomy.genitals, Result.finisher, c)
    }

    override fun copy(user: Character): Skill {
        return Delusion(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                ""
            }
            Result.defended -> {
                ""
            }
            else -> {
                ""
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                ""
            }
            Result.defended -> {
                "[Placeholder: Angel Assists]"
            }
            else -> {
                ""
            }
        }
    }
}

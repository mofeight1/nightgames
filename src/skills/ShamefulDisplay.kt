package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import status.Horny
import status.Shamed

class ShamefulDisplay(user: Character) : Skill("Shameful Display", user) {
    init {
        addTag(Attribute.Submissive)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.mobile(user) && user.isNude
    }

    override fun describe(): String {
        return "Degrade yourself to entice your opponent"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(Shamed(user), c)
        if (target.mood == Emotion.dominant) {
            target.add(Horny(target, user[Attribute.Submissive] / 3.0, 2), c)
        } else {
            target.add(Horny(target, user[Attribute.Submissive] / 4.0, 2), c)
        }
    }

    override fun copy(user: Character): Skill {
        return ShamefulDisplay(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You spread your legs, exposing your naked cock and balls, and thrust your hips out in a show of submission. " + target.name + " practically drools at the sight, " +
                "while you struggle to bear the shame."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " lifts her hips and spreads her pussy lips open. She's bright red with shame, but the sight is lewd enough to drive you wild."
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Isolated

class Banish(user: Character) : Skill("Banish", user) {
    init {
        addTag(Attribute.Spirituality)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Spirituality) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Spirituality to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.canSpend(Pool.FOCUS, 1) &&
                target.pet != null
    }

    override fun describe(): String {
        return "Banish your opponent's pet and prevent them from re-summoning them: 1 Focus"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.FOCUS, 1)
        writeOutput(c, target)
        target.pet?.remove()
        target.add(Isolated(target), c)
    }

    override fun copy(user: Character): Skill {
        return Banish(user)
    }

    override fun type(): Tactics {
        return Tactics.summoning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You banish " + target.name + "'s pet. The residual spiritual energy should disrupt summoning for a while."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " gestures at your pet, who suddenly vanishes. You feel an uneasy sense of isolation, as though " +
                "your connection is severed."
    }
}

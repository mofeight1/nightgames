package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import kotlin.math.roundToInt

class DramaticPose(user: Character) : Skill("Dramatic Pose", user) {
    init {
        addTag(Attribute.Contender)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Contender) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Contender to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c)
    }

    override fun describe(): String {
        return "Gain mojo when close to being stunned or cumming."
    }

    override fun resolve(c: Combat, target: Character) {
        var x = (100 - user.stamina.percent()) / 2
        x += user.arousal.percent() / 2
        writeOutput(c, target)
        user.emote(Emotion.confident, 100)
        user.buildMojo(x.roundToInt())
    }

    override fun copy(user: Character): Skill {
        return DramaticPose(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Even though the fight isn't going well, you strike a confident pose. Your counterattack starts now."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "Despite being worn down, " + user.name + " poses dramatically."
    }
}

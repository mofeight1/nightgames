package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class FondleBreasts(user: Character) : Skill("Fondle Breasts", user) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachTop(user) && target.hasBreasts && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        val topless = target.isTopless
        writeOutput(c, target)
        if (topless) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Angel")) {
                c.offerImage("Breast Fondle_nude.jpg", "Art by AimlessArt")
            }
            c.offerImage("Fondle Breasts.jpg", "Art by AimlessArt")
        } else {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Angel")) {
                c.offerImage("Breast Fondle_cloth.jpg", "Art by AimlessArt")
            }
        }
        if (topless) {
            var m = Global.random(3 + user[Attribute.Seduction] / 2) + target[Attribute.Perception] / 2.0
            m = user.bonusProficiency(Anatomy.fingers, m)
            target.pleasure(m, Anatomy.chest, combat = c)
        } else {
            var m = (Global.random(2 + user[Attribute.Seduction] / 2) - target.tops.size + 2).coerceAtLeast(1).toDouble()
            m = user.bonusProficiency(Anatomy.fingers, m)
            target.pleasure(m, Anatomy.chest, Result.foreplay, c)
        }
        user.buildMojo(10)
    }

    override fun requirements(user: Character): Boolean {
        return !user.has(Trait.cursed)
    }

    override fun copy(user: Character): Skill {
        return FondleBreasts(user)
    }

    override var speed = 6

    override var accuracy = 7

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (Global.random(2) == 0) {
                "You reach towards " + target.name + "'s chest, but come up short."
            } else {
                "You grope at " + target.name + "'s breasts, but miss."
            }
        } else if (target.tops.isEmpty()) {
            when (Global.random(3)) {
                2 -> "You take your time and gently caress both of " + target.name + "'s tender breasts with your fingers. You occasionally brush her nipple to make her coo."
                1 -> "You massage " + target.name + "'s soft breasts and pinch her nipples, causing her to moan with desire."
                else -> "You slowly tease " + target.name + "'s exposed chest, running your hands all over her boobs."
            }
        } else {
            when (Global.random(3)) {
                2 -> "You play with ${target.name}'s breasts, trying to find her nipples through her ${target.tops.last()}."

                1 -> "You tease ${target.name}'s boobs through her ${target.tops.last()}."

                else -> "You massage ${target.name}'s breasts over her ${target.tops.last()}."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun describe(): String {
        return "Grope your opponent's breasts. More effective if she's topless"
    }
}

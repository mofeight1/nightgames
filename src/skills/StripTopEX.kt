package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class StripTopEX(user: Character) : Skill("Tricky Strip Top", user) {
    init {
        addTag(SkillTag.STRIPPING)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachTop(user) &&
                !target.isTopless &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 15)
    }

    override fun resolve(c: Combat, target: Character) {
        var strip: Int
        var style = Result.clever
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            style = Result.powerful
        } else if (user[Attribute.Seduction] > user[Attribute.Cunning]) {
            style = Result.seductive
        }
        user.spendMojo(15)
        when (style) {
            Result.powerful -> {
                strip = user[Attribute.Power]
            }
            Result.clever -> {
                strip = user[Attribute.Cunning]
                strip *= 3
            }
            else -> {
                strip = user[Attribute.Seduction]
            }
        }

        if (!target.stripAttempt(strip, user, c, target.tops.last().item)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target, style)
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Jewel")) {
                c.offerImage("Strip Top Female.jpg", "Art by AimlessArt")
            }
        }
        target.strip(0, c)
        if (user.getPure(Attribute.Cunning) >= 30 && !target.isTopless) {
            if (target.stripAttempt(strip, user, c, target.tops.last().item)) {
                writeOutput(c, target, Result.strong)
                target.strip(0, c)
                target.emote(Emotion.nervous, 10)
            }
        }
        if (target.isNude) {
            c.write(target, target.nakedLiner())
        }
        target.emote(Emotion.nervous, 10)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return StripTopEX(user)
    }

    override var speed = 3

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You attempt to strip off ${target.name}'s ${target.tops.last()}, but ${target.pronounSubject(false)} shoves you away."
            }
            Result.strong -> {
                "Taking advantage of the situation, you also manage to snag ${target.possessive(false)} ${target.tops.last()}"
            }
            Result.powerful -> {
                "You grab ${target.name}'s ${target.tops.last()} and yank it off with enough force that you almost pull ${target.pronounTarget(false)} off the ground."
            }
            Result.clever -> {
                "You slip your hands under ${target.name}'s ${target.tops.last()} to caress ${target.pronounTarget(false)}. You swipe the garment with a flick of your wrist when ${target.pronounSubject(false)} least expects it."
            }
            Result.seductive -> {
                "You turn up the charm to maximum, and help ${target.name} out of ${target.possessive(false)} ${target.tops.last()} before ${target.pronounSubject(false)} can remember to resist."
            }
            else -> {
                "You turn up the charm to maximum, and help ${target.name} out of ${target.possessive(false)} ${target.tops.last()} before ${target.pronounSubject(false)} can remember to resist."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "${user.name} tries to yank off your ${target.tops.last()}, but you manage to hang onto it."
            }
            Result.strong -> {
                "Before you can react, ${user.pronounSubject(false)} also strips off your ${target.tops.last()}"
            }
            Result.powerful -> {
                "${user.name} forcefully pulls your ${target.tops.last()} over your head, blinding you. As you struggle to restore your vision, " +
                    "${user.pronounSubject(false)} easily yanks the garment completely off."
            }
            Result.clever -> {
                "You brace yourself for an attack as ${user.name} lunges toward you, only to playfully lick your cheek. " +
                    "You're so shocked by the unexpected action that ${user.pronounSubject(false)} manages to steal your ${target.tops.last()} in the confusion."
            }
            Result.seductive -> {
                "${user.name} coyly saunters up to you and gently removes your ${target.tops.last()}. " +
                    "Since ${user.pronounSubject(false)} is acting much more intimate than competitive, you end up letting ${user.pronounTarget(false)} out of habit."
            }
            else -> {
                "${user.name} coyly saunters up to you and gently removes your ${target.tops.last()}. " +
                    "Since ${user.pronounSubject(false)} is acting much more intimate than competitive, you end up letting ${user.pronounTarget(false)} out of habit."
            }
        }
    }

    override fun toString(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Forceful Strip Top"
        } else if (user[Attribute.Seduction] > user[Attribute.Cunning]) {
            return "Charming Strip Top"
        }
        return name
    }

    override fun describe(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Attempt to remove opponent's top by force: 15 Mojo"
        } else if (user[Attribute.Seduction] > user[Attribute.Cunning]) {
            return "Attempt to remove opponent's top by seduction: 15 Mojo"
        }
        return "Attempt to remove opponent's top with extra Mojo: 15 Mojo"
    }
}

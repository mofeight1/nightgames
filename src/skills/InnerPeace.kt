package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Stsflag
import status.Unwavering

class InnerPeace(user: Character) : Skill("Inner Peace", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Spirituality) >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Spirituality to 8)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !user.has(Stsflag.unwavering) &&
                user.canSpend(Pool.FOCUS, 5)
    }

    override fun describe(): String {
        return "Become temporarily immune to temptation."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.FOCUS, 5)
        writeOutput(c, target)
        user.add(Unwavering(user), c)
    }

    override fun copy(user: Character): Skill {
        return InnerPeace(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You focus on your inner peace, blocking out all temptation."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " takes a deep breath and looks at you with unwavering focus."
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import status.Shamed
import status.Stsflag
import kotlin.math.max

class PleasureSlave(user: Character) : Skill("Pleasure Slave", user) {
    init {
        addTag(Attribute.Submissive)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user[Attribute.Submissive] >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                c.stance.sub(user) &&
                !user.bound() &&
                !user.distracted() &&
                !c.stance.penetration(user) &&
                user.has(Stsflag.shamed) &&
                user.getStatus(Stsflag.shamed)!!.magnitude >= 5
    }

    override fun describe(): String {
        return "Use the power of your shame to service your mistress"
    }

    override fun resolve(c: Combat, target: Character) {
        var m = max(user[Attribute.Seduction], user[Attribute.Submissive]).toDouble() + target[Attribute.Perception]
        m *= user.getStatusMagnitude("Shamed")
        m = user.bonusProficiency(Anatomy.mouth, m)
        if (user.has(Trait.silvertongue)) {
            m *= 1.2
        }
        user.add(Shamed(user, -5.0))
        writeOutput(c, target)
        target.pleasure(m, Anatomy.genitals, combat = c)
    }

    override fun copy(user: Character): Skill {
        return PleasureSlave(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "Feeling like a worthless slave boy, you devote your entire being to servicing %s. You lavish her lovely flower with your tongue, "
                    + "as though her pleasure is the only thing that can redeem you. Judging by your mistress moaning and trembling, you are doing an acceptable job.",
            target.name
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s holds your penis worshipfully, before taking the entire length in her mouth. She licks and sucks with tremendous enthusiasm, "
                    + "giving her all to service you. The pleasure is too much to handle, and despite her attitude, you're completely at her mercy.",
            user.name
        )
    }
}

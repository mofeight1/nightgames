package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Facesitting
import status.Enthralled
import status.Shamed

class FaceSit(self: Character) : Skill("Facesit", self) {
    init {
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.level >= 15
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.dom(user) && c.stance.reachTop(user) && user.isPantsless &&
                !c.stance.penetration(user) && !c.stance.penetration(target) && c.stance.prone(target) &&
                !user.has(Trait.shy)
    }

    override fun describe(): String {
        return "Shove your crotch into your opponent's face to demonstrate your superiority"
    }

    override fun resolve(c: Combat, target: Character) {
        var m = user.mojo.current / 10.0
        var type = Result.normal
        if (user.has(Trait.succubus) && Global.random(5) == 0) {
            m += 5
            if (user[Attribute.Dark] >= 6 && Global.random(2) == 0) {
                type = Result.critical
                target.add(Enthralled(target, user), c)
            } else {
                type = Result.strong
            }
        }
        if (user.getPure(Attribute.Footballer) >= 21) {
            type = Result.special
            m += user[Attribute.Footballer] * user.arousal.percent() / 100.0
        }
        if (user.has(Trait.pheromones)) {
            m *= 2
        }
        writeOutput(c, target, type)
        target.tempt(m)
        var r = Global.random(user[Attribute.Perception] / 2) + target[Attribute.Seduction] / 3.0
        r = target.bonusProficiency(Anatomy.mouth, r)
        user.pleasure(r, Anatomy.genitals, combat = c)
        target.add(Shamed(target), c)
        user.buildMojo(30)
        c.stance = Facesitting(user, target)
    }

    override fun copy(user: Character): Skill {
        return FaceSit(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun toString(): String {
        if (user.getPure(Attribute.Footballer) >= 21) {
            return "Unsporting Celebration"
        }
        return if (user.hasBalls) {
            "Teabag"
        } else {
            "Facesit"
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (user.hasBalls) {
            if (modifier == Result.special) {
                return "You crouch over " + target.name + "'s face and rub your dick and balls all over her. She can do little except lick them submissively, which does feel " +
                        "pretty good. You shake your hips to make sure she's got a face-full of your ball sweat and precum."
            }
            return when (modifier) {
                Result.critical -> {
                    "You crouch over " + target.name + "'s face and dunk your balls into her mouth. She can do little except lick them submissively, which does feel " +
                            "pretty good. She's so affected by your manliness that her eyes glaze over and she falls under your control. Oh yeah. You're awesome."
                }
                Result.strong -> {
                    "You crouch over " + target.name + "'s face and dunk your balls into her mouth. She can do little except lick them submissively, which does feel " +
                            "pretty good. Your powerful musk is clearly starting to turn her on. Oh yeah. You're awesome."
                }
                else -> {
                    "You crouch over " + target.name + "'s face and dunk your balls into her mouth. She can do little except lick them submissively, which does feel " +
                            "pretty good. Oh yeah. You're awesome."
                }
            }
        } else {
            return when (modifier) {
                Result.special -> {
                    "You straddle " + target.name + "'s face and grind your pussy against her mouth, forcing her to eat you out. You shake your hips, grinding yourself all over " +
                            "her face and making sure she gets a good whiff of your sweaty musk."
                }
                Result.critical -> {
                    "You straddle " + target.name + "'s face and grind your pussy against her mouth, forcing her to eat you out. Your juices take control of her lust and " +
                            "turn her into a pussy licking slave. Ooh, that feels good. You better be careful not to get carried away with this."
                }
                Result.strong -> {
                    "You straddle " + target.name + "'s face and grind your pussy against her mouth, forcing her to eat you out. She flushes and seeks more of your tainted juices. " +
                            "Ooh, that feels good. You better be careful not to get carried away with this."
                }
                else -> {
                    "You straddle " + target.name + "'s face and grind your pussy against her mouth, forcing her to eat you out. Ooh, that feels good. You better be careful " +
                            "not to get carried away with this."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (user.hasBalls) {
            if (modifier == Result.special) {
                return user.name + " straddles your head and confidently puts her balls in your mouth. She gives a superior smile as you obediently suck on her nuts. " +
                        "She gyrates her hips like she's doing a lap dance and rubs her chest sensually. Your sense of smell and vision are dominated by her sweaty balls and " +
                        "musk."
            }
            return when (modifier) {
                Result.critical -> {
                    user.name + " straddles your head and dominates you by putting her balls in your mouth. For some reason, your mind seems to cloud over and you're " +
                            "desperate to please her. She gives a superior smile as you obediently suck on her nuts."
                }
                Result.strong -> {
                    user.name + " straddles your head and dominates you by putting her balls in your mouth. Despite the humiliation, her scent is turning you on incredibly. " +
                            "She gives a superior smile as you obediently suck on her nuts."
                }
                else -> {
                    user.name + " straddles your head and dominates you by putting her balls in your mouth. She gives a superior smile as you obediently suck on her nuts."
                }
            }
        } else {
            return when (modifier) {
                Result.special -> {
                    user.name + " straddles your head and grinds her pussy on your face. You open your mouth and start to lick her freely offered muff, but she keeps moving out of reach" +
                            "She gyrates her hips like she's doing a lap dance and rubs her chest sensually. Your sense of smell and vision are dominated by her juicy pussy and " +
                            "sweaty musk."
                }
                Result.critical -> {
                    user.name + " straddles your face and presses her pussy against your mouth. You open your mouth and start to lick her freely offered muff, but she just smiles " +
                            "while continuing to queen you. As you swallow her juices, you feel her eyes start to bore into your mind. You can't resist her. You don't even want to."
                }
                Result.strong -> {
                    user.name + " straddles your face and presses her pussy against your mouth. You open your mouth and start to lick her freely offered muff, but she just smiles " +
                            "while continuing to queen you. You feel your body start to heat up as her juices flow into your mouth. She's giving you a mouthful of aphrodisiac straight from " +
                            "the source."
                }
                else -> {
                    user.name + " straddles your face and presses her pussy against your mouth. You open your mouth and start to lick her freely offered muff, but she just smiles " +
                            "while continuing to queen you. She clearly doesn't mind accepting some pleasure to demonstrate her superiority."
                }
            }
        }
    }
}

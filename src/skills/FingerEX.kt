package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Stance
import status.ProfMod
import status.ProfessionalMomentum

class FingerEX(user: Character) : Skill("Shining Finger", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.touch)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachBottom(user) &&
                (target.isPantsless || (user.has(Trait.dexterous) && target.bottoms.size <= 1)) &&
                target.hasPussy &&
                user.canAct() &&
                (!c.stance.penetration(target) || c.stance.en == Stance.anal) &&
                user.canSpend(Pool.MOJO, 20)
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m: Double
        var style = Result.seductive
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            style = Result.powerful
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            style = Result.clever
        }
        when (style) {
            Result.powerful -> {
                m = 4.0 + Global.random(user[Attribute.Power] + user[Attribute.Professional]) + target[Attribute.Perception]
            }

            Result.clever -> {
                m = 4.0 + Global.random(user[Attribute.Cunning] + user[Attribute.Professional]) + target[Attribute.Perception]
            }

            else -> {
                m = 4.0 + Global.random(user[Attribute.Seduction] + user[Attribute.Professional]) + target[Attribute.Perception]
                m *= 1.3
            }
        }

        if (user.getPure(Attribute.Professional) >= 3) {
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Dexterous Momentum", user, Anatomy.fingers, user[Attribute.Professional] * 5.0), c)
            }
        }
        writeOutput(c, target, style)
        if (user.human()) {
            c.offerImage("Fingering.jpg", "Art by AimlessArt")
            c.offerImage("Fingering2.jpg", "Art by AimlessArt")
        }
        m = user.bonusProficiency(Anatomy.fingers, m)
        target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        if (user.has(Trait.roughhandling)) {
            target.weaken(m / 2, c)
            writeOutput(c, target, Result.unique)
        }
    }

    override var accuracy = 7

    override fun requirements(user: Character): Boolean {
        return user[Attribute.Seduction] >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 8)
    }

    override fun copy(user: Character): Skill {
        return FingerEX(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.unique -> "You opportunistically give her clit a sharp pinch."
            Result.miss -> {
                if (Global.random(2) == 0) {
                    "You try to slide your hand down to " + target.name + "'s pussy, but she bats your hand away."
                } else {
                    "You grope at " + target.name + "'s pussy, but miss."
                }
            }
            Result.powerful -> {
                String.format(
                    "You plunge two fingers deep into %s's wet pussy. She gasps at the strong penetration, and can barely stay "
                            + "standing as you jackhammer your fingers inside her.", target.name
                )
            }
            Result.clever -> {
                String.format(
                    "You explore %s's feminine garden with your finger, watching her reactions closely. Whenever you find a spot "
                            + "that provokes a stronger reaction, you focus on rubbing and teasing it until she's trembling with pleasure.",
                    target.name
                )
            }
            else -> {
                when (Global.random(2)) {
                    1 -> "Your hand practically glows with an awesome power. You slip two fingers into " + target.name + "'s pussy. You pour your love, your anger, even your sorrow into her, leaving her a quivering mess when you're done."
                    else -> String.format(
                        "You skillfully pleasure %s's eager vagina with your best fingering technique. You cycle between tracing her labia, "
                                + "teasing her clit, and rubbing her G-spot. You switch targets frequently enough that she never gets used to the sensation.",
                        target.name
                    )
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when(modifier) {
            Result.unique -> {
                String.format(
                    "%s gives your poor clit a painful pinch.",
                    user.name
                )
            }
            Result.miss -> {
                String.format(
                    "%s gropes at your pussy, but misses the mark.",
                    user.name
                )
            }
            Result.critical -> {
                String.format(
                    "%s slides her shadow tentacles between your legs. The tendrils delve into your slick hole, overwhelming "
                            + "you with a strange pleasure", user.name
                )
            }
            else -> {
                if (target.arousal.current <= 15) {
                    String.format(
                        "%s softly rubs your sensitive lower lips. You aren't very aroused yet, but %s gentle touch "
                                + "gives you a ticklish pleasure",
                        user.name, user.possessive(false)
                    )
                } else if (target.arousal.percent() < 50) {
                    String.format(
                        "%s skillfully fingers your sensitive pussy. You bite your lip and try to ignore the pleasure, "
                                + "but despite your best efforts, you feel yourself growing wet.",
                        user.name
                    )
                } else if (target.arousal.percent() < 80) {
                    String.format(
                        "%s locates your clitoris and caress it directly, causing"
                                + " you to tremble from the powerful stimulation.",
                        user.name
                    )
                } else {
                    String.format(
                        "%s stirs your increasingly soaked pussy with %s fingers and "
                                + "rubs your clit directly with %s thumb.",
                        user.name, user.possessive(false), user.possessive(false)
                    )
                }
            }
        }
    }

    override fun toString(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Intense Finger"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            return "Probing Finger"
        }
        return name
    }


    override fun describe(): String {
        return if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            "Strong forceful fingering: 20 Mojo"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            "Exploratory fingering to identify sensitive areas: 20 Mojo"
        } else {
            "Mojo boosted shining finger: 20 Mojo"
        }
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result

class EcstasyWave(user: Character) : Skill("Ecstasy Wave", user) {
    init {
        addTag(Attribute.Unknowable)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Unknowable) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Unknowable to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && user.canSpend(Pool.ENIGMA, 4)
    }

    override fun describe(): String {
        return "Unleash a massive blast of pleasure"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.ENIGMA, 4)
        if (target[Attribute.Contender] >= 2) {
            writeOutput(c, target, Result.miss)
        } else if (c.isWatching(ID.CASSIE)) {
            writeOutput(c, target, Result.defended)
        } else {
            writeOutput(c, target)
            val pow = 60 + user[Attribute.Unknowable]
            target.pleasure(pow.toDouble(), Anatomy.soul, combat = c)
        }
    }

    override fun copy(user: Character): Skill {
        return EcstasyWave(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                ""
            }
            Result.defended -> {
                ""
            }
            else -> {
                ""
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " steps back and charges something in " + user.possessive(false) + " hands. You catch a glimpse " +
                        "of a dense ball of particularly lascivious looking energy, before it explodes toward you. There is no time to dodge. You " +
                        "brace yourself for the attack by sheer force of will. The energy somehow washes over you, leaving you no worse for wear."
            }
            Result.defended -> {
                ""
            }
            else -> {
                user.name + " steps back and charges something in " + user.possessive(false) + " hands. You catch a glimpse " +
                        "of a dense ball of particularly lascivious looking energy, before it explodes toward you. There is no time to dodge. " +
                        "The energy hits you and overloads your nervous system with intense pleasure coming from everywhere and nowhere."
            }
        }
    }
}

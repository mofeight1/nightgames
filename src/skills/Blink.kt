package skills

import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result
import stance.Pin

class Blink(user: Character) : Skill("Blink", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Unknowable) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Unknowable to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && user.canSpend(Pool.ENIGMA, 2)
    }

    override fun describe(): String {
        return "Teleport on top of your opponent to pin them"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.ENIGMA, 4)
        if (target[Attribute.Ninjutsu] >= 10) {
            writeOutput(c, target, Result.miss, 0)
        } else if (target[Attribute.Animism] >= 10) {
            writeOutput(c, target, Result.miss, 1)
        } else if (c.isWatching(ID.YUI)) {
            writeOutput(c, target, Result.defended, 0)
        } else if (c.isWatching(ID.KAT)) {
            writeOutput(c, target, Result.defended, 1)
        } else {
            writeOutput(c, target, Result.normal, 0)
            c.stance = Pin(user, target)
        }
    }

    override fun copy(user: Character): Skill {
        return Blink(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (damage == 1) {
                ""
            } else {
                ""
            }
        } else if (modifier == Result.defended) {
            if (damage == 1) {
                "[Placeholder: Kat Assists]"
            } else {
                "[Placeholder: Yui Assists]"
            }
        } else {
            ""
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            if (damage == 1) {
                user.name + " suddenly disappears from your vision. Before you can even think to react, you feel the " +
                        "sudden existence of a woman-sized, woman-shaped object on top of you. It turns out to be " + user.name +
                        " which is physically impossible, but still makes more sense than if it was anyone else. Fortunately, your " +
                        "animal instincts are quicker than your brain, kicking up with both feet to push " +
                        user.pronounTarget(false) + " off."
            } else {
                user.name + " suddenly disappears from your vision. Before you can even think to react, you feel the " +
                        "sudden existence of a woman-sized, woman-shaped object on top of you. It turns out to be " + user.name +
                        " which is physically impossible, but still makes more sense than if it was anyone else. You have just enough " +
                        "time to drop a smoke bomb and dodge out of the way before " + user.pronounSubject(false) + " can grab you."
            }
        } else if (modifier == Result.defended) {
            if (damage == 1) {
                "[Placeholder: Kat Assists]"
            } else {
                "[Placeholder: Yui Assists]"
            }
        } else {
            user.name + " suddenly disappears from your vision. Before you can even think to react, you feel the " +
                    "sudden existence of a woman-sized, woman-shaped object on top of you. It turns out to be " + user.name +
                    " which is physically impossible, but still makes more sense than if it was anyone else. However, this realization " +
                    "comes too late to prevent you from being pinned on the floor."
        }
    }
}

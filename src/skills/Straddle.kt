package skills

import characters.Character
import combat.Combat
import combat.Result
import stance.Mount

class Straddle(user: Character) : Skill("Mount", user) {
    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                c.stance.mobile(target) &&
                c.stance.prone(target) &&
                user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        c.stance = Mount(user, target)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Straddle(user)
    }

    override var speed = 6

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You straddle " + target.name + " using your body weight to hold her down."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " plops herself down on top of your stomach."
    }

    override fun describe(): String {
        return "Straddles opponent"
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import items.Toy
import stance.Stance

class VibroTease(user: Character) : Skill("Vibro-Tease", user) {
    init {
        addTag(SkillTag.TOY)
        addTag(Result.anal)
    }

    override fun requirements(user: Character): Boolean {
        return user.has(Toy.Strapon2)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(user) &&
                c.stance.en == Stance.anal &&
                user.has(Toy.Strapon2)
    }

    override fun describe(): String {
        return "Turn up the strapon vibration"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        var m = 3 + user[Attribute.Seduction] / 2.0 + target[Attribute.Perception] + (user[Attribute.Science] / 2.0)
        m = user.bonusProficiency(Anatomy.toy, m)
        target.pleasure(m, Anatomy.genitals, combat = c)
        var r = (2 + user[Attribute.Perception]).toDouble()
        r = target.bonusProficiency(Anatomy.genitals, r)
        r = target.bonusRecoilPleasure(r)
        user.pleasure(r, Anatomy.genitals, combat = c)
        user.buildMojo(20)
        c.stance.pace = 0
    }

    override fun copy(user: Character): Skill {
        return VibroTease(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " cranks up the vibration of her strapon to maximum level which stirs up your insides. " +
                "She teasingly pokes the tip against your prostate which causes your limbs to get shaky from the pleasure."
    }
}

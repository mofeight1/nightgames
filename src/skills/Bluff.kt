package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Unreadable

class Bluff(user: Character) : Skill("Bluff", user) {
    init {
        addTag(Attribute.Cunning)
    }

    override fun requirements(user: Character): Boolean {
        return user.has(Trait.pokerface) && user.getPure(Attribute.Cunning) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.mobile(user) && user.canSpend(Pool.MOJO, 20)
    }

    override fun resolve(c: Combat, target: Character) {
        val m = Global.random(25).toDouble()
        writeOutput(c, target)
        user.spendMojo(20)
        user.add(Unreadable(user), c)
        user.heal(m, c)
        user.calm(25 - m, c)
        user.emote(Emotion.confident, 30)
        user.emote(Emotion.dominant, 20)
        user.emote(Emotion.nervous, -20)
    }

    override fun copy(user: Character): Skill {
        return Bluff(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You force yourself to look less tired and horny than you actually are. You even start to believe it yourself."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "Despite your best efforts, " + user.name + " is still looking as calm and composed as ever. Either you aren't getting to her at all, or she's good at hiding it."
    }

    override fun describe(): String {
        return "Regain some stamina and lower arousal. Hides current status from opponent."
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.ProfMod
import status.ProfessionalMomentum

class Thrust(user: Character) : Skill("Thrust", user) {
    init {
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.dom(user) && c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        var m = user.getSexPleasure(2, Attribute.Seduction) + target[Attribute.Perception]

        val inside: Anatomy
        val pro = user.getPure(Attribute.Professional) >= 11
        if (c.stance.anal()) {
            writeOutput(c, target, Result.anal)
            inside = Anatomy.ass
        } else {
            inside = Anatomy.genitals
            if (pro) {
                writeOutput(c, target, Result.special)
            } else {
                writeOutput(c, target, Result.normal)
                if (user.human()) {
                    if (c.stance.behind(user)) {
                        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.KAT) {
                            c.offerImage("Doggy Style.jpg", "Art by AimlessArt")
                        }
                        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.MARA) {
                            c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt")
                        }
                    } else {
                        if (!Global.checkFlag(Flag.exactimages) || target.id == ID.ANGEL) {
                            c.offerImage("Angel_Sex.jpg", "Art by AimlessArt")
                            c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri")
                        }
                        c.offerImage("Thrust.jpg", "Art by AimlessArt")
                    }
                } else if (target.human()) {
                    if (!Global.checkFlag(Flag.exactimages) || user.id == ID.CASSIE) {
                        c.offerImage("Cowgirl.jpg", "Art by AimlessArt")
                    }
                    if (!Global.checkFlag(Flag.exactimages) || user.id == ID.SAMANTHA) {
                        c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt")
                    }
                }
            }
        }

        if (pro) {
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Sexual Momentum", user, Anatomy.genitals, user[Attribute.Professional] * 5.0), c)
            }
        }
        user.buildMojo(10)
        if (user.has(Trait.strapped)) {
            m += user[Attribute.Science] / 2.0
            m = user.bonusProficiency(Anatomy.toy, m)
        }
        target.pleasure(m, inside, combat = c)
        var r = (target.getSexPleasure(2, Attribute.Seduction, inside) * 2) / 3.0
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }

        user.pleasure(r, Anatomy.genitals, combat = c)
        c.stance.pace = 1
    }

    override fun copy(user: Character): Skill {
        return Thrust(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            "You thrust steadily into " + target.name + "'s ass, eliciting soft groans of pleasure."
        } else {
            "You thrust into " + target.name + " in a slow, steady rhythm. She lets out soft breathy moans in time with your lovemaking. You can't deny you're feeling " +
                    "it too, but by controlling the pace, you can hopefully last longer than she can."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            if (user.has(Trait.strapped)) {
                user.name + " thrusts her hips, pumping her artificial cock in and out of your ass and pushing on your prostate."
            } else {
                user.name + "'s cock slowly pumps the inside of your rectum."
            }
        } else {
            if (modifier == Result.special) {
                when (Global.random(3)) {
                    0 -> String.format(
                        "%s is holding just the tip of your dick inside of her slick pussy,"
                                + " rubbing it against the soft, tight entrance. Slowly	moving"
                                + " her hips, she is driving you crazy.",
                        user.name
                    )

                    1 -> String.format(
                        "%s slides down fully onto you, squeezing you tightly"
                                + " with her vaginal walls. The muscles are wound so tight that it's nearly"
                                + " impossible to move at all, but she pushes down hard and eventually"
                                + " all of your cock is lodged firmly inside of her.",
                        user.name
                    )

                    else -> String.format(
                        "%s moves up and down your rock-hard cock while the velvet vise"
                                + " of her pussy is undulating on your shaft, sending ripples along it"
                                + " as if milking it. Overcome with pleasure, your entire body tenses up and"
                                + " you throw your head back, trying hard not to cum instantly.",
                        user.name
                    )
                }
            } else {
                user.name + " rocks her hips against you, riding you smoothly and deliberately. Despite the slow pace, the sensation of her hot, wet pussy surrounding " +
                        "your dick is gradually driving you to your limit."
            }
        }
    }

    override fun toString(): String {
        return if (user.getPure(Attribute.Professional) >= 11) {
            "Pro Thrust"
        } else {
            "Thrust"
        }
    }

    override fun describe(): String {
        return if (user.getPure(Attribute.Professional) >= 11) {
            "A professional thrusting technique that gains momentum over time"
        } else {
            "Slow fuck, minimizes own pleasure"
        }
    }
}

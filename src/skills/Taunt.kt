package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Shamed

class Taunt(user: Character) : Skill("Taunt", user) {
    init {
        addTag(SkillTag.MISCHIEF)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isNude &&
                !c.stance.sub(user) &&
                user.canSpend(Pool.MOJO, 5) &&
                user.canAct() &&
                !user.has(Trait.shy)
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(5)
        writeOutput(c, target)
        var t = 3 + user[Attribute.Seduction] / 2.0
        if (c.stance.dom(user)) {
            t *= 1.5
            target.add(Shamed(target), c)
        }
        target.tempt(t, user.bonusTemptation(), combat = c)
        target.emote(Emotion.angry, 30)
        target.emote(Emotion.nervous, 15)
        user.emote(Emotion.dominant, 20)
        target.mojo.reduce(10)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 8)
    }

    override fun copy(user: Character): Skill {
        return Taunt(user)
    }

    override var speed = 9

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(2)) {
            1 -> "You tell " + target.name + " that she's free to stop fighting back whenever she's ready to get fucked."
            else -> "You tell " + target.name + " that if she's so eager to be fucked senseless, you're available during off hours."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.taunt()
    }

    override fun describe(): String {
        return "Embarrass your opponent, may inflict Shamed"
    }
}

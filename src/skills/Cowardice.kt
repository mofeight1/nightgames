package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Behind
import stance.Stance

class Cowardice(user: Character) : Skill("Cowardice", user) {
    init {
        addTag(Attribute.Submissive)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && target.canAct() && c.stance.en == Stance.neutral
    }

    override fun describe(): String {
        return "Turning your back to an opponent will likely get you attacked from behind."
    }

    override fun resolve(c: Combat, target: Character) {
        c.stance = Behind(target, user)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return Cowardice(user)
    }

    override fun type(): Tactics {
        return Tactics.negative
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You try to run away, but " + target.name + " catches you and grabs you from behind."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " tries to sprint away, but you quickly grab her from behind before she can escape."
    }
}

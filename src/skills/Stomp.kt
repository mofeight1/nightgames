package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class Stomp(user: Character) : Skill("Stomp", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.PAINFUL)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.prone(user) &&
                c.stance.prone(target) &&
                c.stance.feet(user) &&
                user.canSpend(Pool.MOJO, 20) &&
                user.canAct() &&
                !user.has(Trait.softheart) && !user.has(Trait.sportsmanship) &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.has(Trait.heeldrop) && target.isPantsless) {
            writeOutput(c, target, Result.special)
            if (target.human()) {
                if (Global.random(5) >= 1) {
                    c.write(user, user.bbLiner())
                }
            }
            target.pain(30.0 - (Global.random(2) * target.bottoms.size), Anatomy.genitals, c)
            target.calm(Global.random(30).toDouble(), c)
        } else if (target.has(Trait.armored)) {
            writeOutput(c, target, Result.weak)
            target.pain(20.0 - (Global.random(3) * target.bottoms.size), Anatomy.genitals, c)
            target.calm(Global.random(10) + 10.0, c)
        } else {
            writeOutput(c, target, Result.normal)
            if (target.human()) {
                if (Global.random(5) >= 1) {
                    c.write(user, user.bbLiner())
                }
            }
            target.pain(20.0 - (Global.random(3) * target.bottoms.size), Anatomy.genitals, c)
            target.calm(Global.random(30) + 10.0, c)
        }
        target.emote(Emotion.angry, 25)
        user.spendMojo(20)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 16 && !user.has(Trait.softheart)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 16)
    }

    override fun copy(user: Character): Skill {
        return Stomp(user)
    }

    override var speed= 4

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun toString(): String {
        return if (user.has(Trait.heeldrop)) {
            "Double Legdrop"
        } else {
            name
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                if (target.hasBalls) {
                    "You push " + target.name + "'s legs apart, exposing her private parts. Her girl-cock is hanging over her testicles, so you prod it with your foot " +
                            "to push it out of the way. She becomes slightly aroused at your touch and your attention, not realizing you're planning something extremely " +
                            "painful. You're going to feel a bit bad about this, but probably not nearly as much as she will. You jump up, lifting both legs and coming " +
                            "down in a double legdrop directly hitting her unprotected jewels."
                } else {
                    "You push " + target.name + "'s legs apart, fully exposing her womanhood. She flushes in shame, apparently misunderstanding you intentions, which " +
                            "compels you to mutter a quick apology before you jump up and slam your heel into her vulnerable quim."
                }
            }
            Result.weak -> {
                "You step between " + target.name + "'s legs and stomp down on her groin. Your foot hits something solid and she doesn't seem terribly affected."
            }
            else -> {
                if (target.hasBalls) {
                    "You pull " + target.name + "'s legs open and stomp on her vulnerable balls. She cries out in pain and curls up in the fetal position."
                } else {
                    "You step between " + target.name + "'s legs and stomp down on her sensitive pussy. She cries out in pain and rubs her injured girl bits."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " forces your legs open and begins prodding your genitals with her foot. You're slightly aroused by her attention, but she's not giving " +
                        "you a proper footjob, she's mostly just playing with your balls. Too late, you realize that she's actually lining up her targets. Two torrents of pain " +
                        "erupt from your delicates as her feet crash down on them."
            }
            Result.weak -> {
                user.name + " grabs your ankles and stomps down on your armored groin, doing little damage."
            }
            else -> {
                user.name + " grabs your ankles and stomps down on your unprotected jewels. You curl up in the fetal position, groaning in agony."
            }
        }
    }

    override fun describe(): String {
        return "Stomps on your opponent's groin for extreme damage: 20 mojo"
    }
}

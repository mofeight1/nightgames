package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import stance.Behind

class FlashStep(user: Character) : Skill("Flash Step", user) {
    init {
        addTag(Attribute.Ki)
        addTag(SkillTag.OUTMANEUVER)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                !c.stance.prone(target) &&
                !c.stance.behind(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun describe(): String {
        return "Use lightning speed to get behind your opponent before she can react: 10 stamina"
    }

    override fun resolve(c: Combat, target: Character) {
        user.weaken(10.0)
        writeOutput(c, target)
        c.stance = Behind(user, target)
    }

    override fun copy(user: Character): Skill {
        return FlashStep(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override var speed = 99

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You channel your ki into your feet and dash behind " + target.name + " faster than her eyes can follow."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " starts to move and suddenly vanishes. You hesitate for a second and feel her grab you from behind."
    }
}

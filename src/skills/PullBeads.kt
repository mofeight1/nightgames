package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Beaded
import status.Stsflag

class PullBeads(user: Character) : Skill("Pull Out Beads", user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.reachBottom(user) &&
                target.isPantsless &&
                target.has(Stsflag.beads)
    }

    override fun describe(): String {
        return "Pull all the anal beads out of your opponent, dealing pleasure based on how many are inside"
    }

    override fun resolve(c: Combat, target: Character) {
        val beads: Int = (target.getStatus(Stsflag.beads) as Beaded).amount
        var m = (10 + (target[Attribute.Perception] / 2.0) + (user[Attribute.Science] / 2.0)) * beads
        m = user.bonusProficiency(Anatomy.toy, m)
        writeOutput(c, target, Result.normal, beads)
        target.removeStatus(Stsflag.beads, c)
        target.pleasure(m, Anatomy.ass, combat = c)
    }

    override fun copy(user: Character): Skill {
        return PullBeads(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (damage > 1) {
            "You pull the string of " + damage + " beads out of " + target.name + "'s anus."
        } else {
            "You yank the bead out of " + target.name + "'s ass with an audible 'pop'."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (damage > 1) {
            user.name + " grabs the string of anal beads sticking out of your ass and yanks them all out at once. Your " +
                    "mind goes blank ass you're assaulted by the sensation of your asshole repeatedly getting forced open."
        } else {
            user.name + " pulls the anal bead out of your ass, causing an indescribable sensation."
        }
    }
}

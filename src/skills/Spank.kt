package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import items.Toy
import status.Shamed
import status.Stsflag

class Spank(user: Character) : Skill("Spank", user) {
    init {
        addTag(SkillTag.PAINFUL)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                !c.stance.prone(target) &&
                c.stance.reachBottom(user) &&
                user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = Global.random(user[Attribute.Power] / 4) + 4 + target[Attribute.Perception] / 2.0
        var item = 0
        if (user.has(Toy.Paddle)) {
            m *= 1.5
            item = 1
            user.buildMojo(20)
            user.emote(Emotion.dominant, 30)
        }
        val disciplinarian = user.has(Trait.disciplinarian) && !c.stance.penetration(user)
        if (disciplinarian) {
            writeOutput(c, target, Result.special, item)
        } else writeOutput(c, target, Result.normal, item)
        if (user.human()) {
            c.offerImage("Spank.jpg", "Art by AimlessArt")
        }

        if (!disciplinarian) {
            target.pain(m, Anatomy.ass, c)
        } else {
            m *= 1.5
            if (Global.random(10) >= 5 || !target.has(Stsflag.shamed) && user.canSpend(Pool.MOJO, 5)) {
                target.add(Shamed(target), c)
                user.spendMojo(5)
                target.emote(Emotion.angry, 10)
                target.emote(Emotion.nervous, 15)
            }
            target.pain(m, Anatomy.genitals, c)
        }
        target.emote(Emotion.angry, 45)
        target.emote(Emotion.nervous, 15)
        user.buildMojo(10)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 8)
    }

    override fun copy(user: Character): Skill {
        return Spank(user)
    }

    override var speed = 8

    override var accuracy = 4

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You try to spank " + target.name + ", but she dodges away."
        }
        else if (modifier == Result.special) {
            if (damage == 1) {
                "You swing your paddle between " + target.name + "'s legs, hitting her sensitive genitals with a painful sounding smack."
            } else {
                if (target.has(Trait.imagination) && Global.random(3) == 0) {
                    return "You pull " + target.name + " over your knees and begin spanking her ass while whispering in her ears about just how bad of a girl she's been, " +
                            "and what you're going to have to do to her later. Suddenly, you place a hard slap on her delicate pussy, snapping her back to reality with a jolt."
                }
                when (Global.random(2)) {
                    1 -> "You bend " + target.name + " over your knee and spank her, alternating between hitting her soft butt cheek and her sensitive pussy."
                    2 -> "Pulling " + target.name + " across your knees, you firmly spank her and tell her how bad she's been until her face turns red."
                    else -> "You bend " + target.name + " over your knee and spank her, alternating between hitting her soft butt cheek and her sensitive pussy."
                }
            }
        } else {
            if (damage == 1) {
                "You paddle " + target.name + "sharply on the ass, leaving a bright red mark."
            } else {
                when (Global.random(3)) {
                    1 -> "You spank " + target.name + " on the ass, causing a satisfying slapping sound to ring out."
                    2 -> "You slap " + target.name + "'s butt, admiring the way it shakes as your hand rebounds off it."
                    3 -> "You spank " + target.name + " on her naked butt cheek."
                    else -> "You spank " + target.name + " on her naked butt cheek."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " aims a slap at your ass, but you dodge it."
        } else if (modifier == Result.special) {
            if (damage == 1) {
                user.name + " delivers an eye watering paddle blow to your exposed balls."
            } else {
                if (target[Attribute.Submissive] >= 15 && Global.random(3) == 0) {
                    user.name + " pulls you across her knees and rests her hand on your ass, and you instantly go limp as you realise what she's about to do - " +
                            "tensing up will only make it sting more. No slap comes, though, and instead you hear a whisper in your ear: <i>\"Trying to avoid your punishment? " +
                            " That won't do. Now show me your ass properly like a good boy.\"</i> Without thinking, you tense up again and push out your buttcheeks. " + user.name + " " +
                            "gives you a few hard spanks before letting you go, and your cheeks burn with shame when you realise what you just submitted to in the middle of a sex fight."
                }
                when (Global.random(2)) {
                    0 -> user.name + " pulls you across her knees and starts spanking you, hard. Stinging pain spreads all across your ass, and every blow is accompanied " +
                            "by a matching one to your pride as you are treated like nothing more than a naughty boy. After a firm slap to the testicles, " + user.name + " releases you."

                    else -> user.name + " bends you over like a misbehaving child and spanks your ass twice. The third spank aims lower and connects solidly with your ballsack, " +
                            "injuring your manhood along with your pride."
                }
            }
        } else {
            if (damage == 1) {
                user.name + " hits your ass with a loud, painful smack of her paddle."
            } else {
                when (Global.random(2)) {
                    0 -> user.name + " slaps you on the ass, leaving a red mark and a stinging feeling."
                    else -> user.name + " lands a stinging slap on your bare ass."
                }
            }
        }
    }

    override fun describe(): String {
        return "Slap opponent on the ass"
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import items.Toy
import status.Sensitive
import status.Sore

class UseClamps(user: Character) : Skill("Nipple Clamps", user) {
    init {
        addTag(SkillTag.TOY)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.reachTop(user) && target.isTopless &&
                user.has(Toy.nippleclamp)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = 4 + target[Attribute.Perception] + user[Attribute.Science] / 2.0
        m = user.bonusProficiency(Anatomy.toy, m)
        writeOutput(c, target)
        target.add(Sore(target, 4, Anatomy.chest, 2.0), c)
        target.add(Sensitive(target, 4, Anatomy.chest, 2.0), c)
    }

    override fun copy(user: Character): Skill {
        return UseClamps(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You try to attach your nipple clamps to " + target.name + " but she blocks them"
        } else {
            "Apply your golden clamps to " + target.name + "'s nipples, leaving them swollen and extra sensitive."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " tries to put some gold clamps on your nipples, but you push her away."
        } else {
            user.name + " sticks a pair of ornate nipple clamps onto your chest and yanks them off. You " +
                    "grit your teeth at the sharp pain that leaves your nipples feeling sore and sensitive."
        }
    }

    override fun describe(): String {
        return "Use your clamps to sensitize your opponents' nipples"
    }
}

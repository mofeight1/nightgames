package skills

import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Unreadable

class Nothing(user: Character) : Skill("Wait", user) {
    init {
        addTag(SkillTag.NOTHING)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        if (bluff()) {
            val m = Global.random(25)
            writeOutput(c, target, Result.special)

            user.spendMojo(20)
            user.add(Unreadable(user), c)
            user.heal(m.toDouble(), c)
            user.calm(25.0 - m, c)
        } else if (focused() && !c.stance.sub(user)) {
            writeOutput(c, target, Result.strong)
            user.heal(Global.random(4).toDouble(), c)
            user.calm(Global.random(8).toDouble(), c)
            user.buildMojo(20)
        } else {
            writeOutput(c, target)
            user.buildMojo(10)
            user.heal(Global.random(4).toDouble(), c)
        }
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Nothing(user)
    }

    override var speed = 0

    override fun type(): Tactics {
        return if (bluff() || focused()) {
            Tactics.calming
        } else {
            Tactics.misc
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                "You force yourself to look less tired and horny than you actually are. You even start to believe it yourself."
            }
            Result.strong -> {
                "You take a moment to clear your thoughts, focusing your mind and calming your body."
            }
            else -> {
                "You bide your time, waiting to see what " + target.name + " will do."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                "Despite your best efforts, " + user.name + " is still looking as calm and composed as ever. Either you aren't getting to her at all, or she's good at hiding it."
            }
            Result.strong -> {
                user.name + " closes her eyes and takes a deep breath. When she opens her eyes, she seems more composed."
            }
            else -> {
                user.name + " hesitates, watching you closely."
            }
        }
    }

    override fun toString(): String {
        return if (bluff()) {
            "Bluff"
        } else if (focused()) {
            "Focus"
        } else {
            name
        }
    }

    override fun describe(): String {
        return if (bluff()) {
            "Regain some stamina and lower arousal. Hides current status from opponent."
        } else if (focused()) {
            "Calm yourself and gain some mojo"
        } else {
            "Do nothing"
        }
    }

    private fun focused(): Boolean {
        return user.getPure(Attribute.Cunning) >= 15 && !user.has(Trait.undisciplined)
    }

    private fun bluff(): Boolean {
        return user.has(Trait.pokerface) && user.getPure(Attribute.Cunning) >= 9 && user.canSpend(Pool.MOJO, 20)
    }
}

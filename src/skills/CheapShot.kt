package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Behind

class CheapShot(user: Character) : Skill("Cheap Shot", user) {
    init {
        addTag(Attribute.Temporal)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 2
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 2)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !c.stance.prone(target) &&
                !c.stance.behind(user) &&
                !c.stance.penetration(user) &&
                user.canSpend(Pool.TIME, 3)
    }

    override fun describe(): String {
        return "Stop time long enough to get in an unsportsmanlike attack from behind: 3 charges"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.TIME, 3)
        writeOutput(c, target)
        if (target.human() && Global.random(5) >= 3) {
            c.write(user, user.bbLiner())
        }
        c.stance = Behind(user, target)
        target.pain(8.0 + Global.random(16) + user[Attribute.Power], Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(Global.random(6).toDouble(), c)
        } else {
            target.calm(Global.random(10).toDouble(), c)
        }
        user.buildMojo(10)

        user.emote(Emotion.confident, 15)
        user.emote(Emotion.dominant, 15)
        target.emote(Emotion.nervous, 10)
        target.emote(Emotion.angry, 40)
    }

    override fun copy(user: Character): Skill {
        return CheapShot(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (target.isNude) {
            if (target.hasBalls) {
                String.format(
                    "You freeze time briefly, giving you a chance to circle around %s. When time resumes, %s looks around in "
                            + "confusion, completely unguarded. You capitalize on your advantage by crouching behind %s and delivering a decisive "
                            + "uppercut to %s dangling balls.",
                    target.name, target.pronounSubject(false), target.pronounTarget(false), target.possessive(false)
                )
            } else {
                String.format(
                    "You freeze time briefly, giving you a chance to circle around %s. When time resumes, %s looks around in "
                            + "confusion, completely unguarded. You capitalize on your advantage by delivering a swift, but "
                            + "painful cunt punt.", target.name, target.pronounSubject(false)
                )
            }
        } else {
            String.format(
                "You freeze time briefly, giving you a chance to circle around %s. When time resumes, %s looks around in "
                        + "confusion, completely unguarded. You capitalize on your advantage by crouching behind %s and delivering a decisive "
                        + "uppercut to the groin.",
                target.name,
                target.pronounSubject(false),
                target.pronounTarget(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (target.isNude) {
            if (target.hasBalls) {
                String.format(
                    "%s suddenly vanishes right in front of your eyes. That wasn't just fast, %s completely disappeared. Before "
                            + "you can react, you're hit from behind with a devastating punch to your unprotected balls.",
                    user.name,
                    user.pronounSubject(false)
                )
            } else {
                String.format(
                    "%s suddenly vanishes right in front of your eyes. That wasn't just fast, %s completely disappeared. Before "
                            + "you can react, you're hit from behind with a devastating punch to your bare vulva.",
                    user.name,
                    user.pronounSubject(false)
                )
            }
        } else {
            String.format(
                "%s suddenly vanishes right in front of your eyes. That wasn't just fast, %s completely disappeared. You hear something "
                        + "that sounds like 'Za Worldo' before you suffer a painful groin hit from behind.",
                user.name,
                user.pronounSubject(false)
            )
        }
    }
}

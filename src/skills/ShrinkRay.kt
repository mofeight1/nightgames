package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Shamed

class ShrinkRay(user: Character) : Skill("Shrink Ray", user) {
    init {
        addTag(Attribute.Science)
        addTag(SkillTag.MISCHIEF)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                target.isNude &&
                user.canSpend(Pool.BATTERY, 2)
    }

    override fun describe(): String {
        return "Briefly shrink your opponent's 'assets' to damage her ego: 2 Batteries"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 2)
        if (target.hasDick) {
            writeOutput(c, target, Result.special)
        } else {
            writeOutput(c, target, Result.normal)
        }
        target.add(Shamed(target), c)
        target.spendMojo(15)
        target.emote(Emotion.nervous, 20)
        target.emote(Emotion.desperate, 20)
    }

    override fun copy(user: Character): Skill {
        return ShrinkRay(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.special) {
            "You aim your shrink ray at " + target.name + "'s cock, shrinking her male anatomy. She turns red and glares at you in humiliation."
        } else {
            "You use your shrink ray to turn " + target.name + "'s breasts into A cups. She whimpers and covers her chest in shame."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " points a device at your groin and giggles as your genitals shrink. You flush in shame and cover yourself. The effect wears off quickly, " +
                "but the damage to your dignity lasts much longer."
    }
}

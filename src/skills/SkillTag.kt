package skills

import combat.Tag

enum class SkillTag : Tag {
    PAINFUL,
    STRIPPING,
    CRIPPLING,
    PEGGING,
    DRESSING,
    DEFENSIVE,
    BUFF,
    BEAST,
    DOM,
    OUTMANEUVER,
    MISCHIEF,
    CHARMING,
    ULTIMATE,
    CLONE,
    CROP,
    TOY,
    TICKLE,
    CONSUMABLE,
    BIND,
    KNOCKDOWN,
    COMMAND,
    ESCAPE,
    MASTURBATION,
    NOTHING,
    PET,
    TAIL,
}

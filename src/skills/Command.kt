package skills

import characters.Character
import combat.Combat
import combat.Result
import global.Global
import stance.Cowgirl
import stance.Mount
import stance.ReverseMount
import status.Flatfooted
import status.Stsflag

class Command(user: Character) : Skill("Command", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun requirements(user: Character): Boolean {
        return !user.human()
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !user.human() && target.has(Stsflag.enthralled)
    }

    override fun describe(): String {
        return "Order your thrall around"
    }

    override fun resolve(c: Combat, target: Character) {
        if (!target.isNude) { // Undress self
            c.write(user, receive(target, Result.miss, 0))
            Undress(target).resolve(c, user)
        } else if (!user.isNude) { // Undress me
            c.write(user, receive(target, Result.weak, 0))
            if (user.isTopless) c.write(
                user, "Like a hungry beast, you rip off " + user.name
                        + "'s " + user.bottoms.removeLast() + "."
            )
            else c.write(
                user, "Like a hungry beast, you rip off " + user.name
                        + "'s " + user.tops.removeLast() + "."
            )
        } else if (target.arousal.current <= 15) { // Masturbate
            c.write(user, receive(target, Result.normal, 0))
            Masturbate(target).resolve(c, user)
        } else if (!c.stance.penetration(user) && c.stance.dom(target)) { // Fuck
            // me
            c.stance = Mount(target, user)
            c.write(user, receive(target, Result.special, 0))
            Fuck(target).resolve(c, user)
        } else if (c.stance.penetration(user)) { // I drain you
            if (Global.random(5) >= 4) {
                c.write(user, receive(target, Result.critical, 0))
                Drain(user).resolve(c, target)
            } else {
                c.write(user, receive(target, Result.critical, 0))
                Piston(user).resolve(c, target)
            }
        } else if (!c.stance.penetration(user) && user.arousal.current <= 15) { // Pleasure
            // me
            c.write(user, receive(target, Result.critical, 1))
            c.stance = ReverseMount(target, user)
            Cunnilingus(target).resolve(c, user)
        } else if (!c.stance.penetration(user)) { // Lay down
            c.write(user, receive(target, Result.critical, 2))
            c.stance = Cowgirl(user, target)
            Thrust(user).resolve(c, target)
        } else { // Confused
            c.write(user, receive(target, null, 0))
            target.removeStatus(Stsflag.enthralled, c)
            target.add(Flatfooted(target, 1))
        }
    }

    override fun copy(user: Character): Skill {
        return Command(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            null -> {
                user.name + "'s order confuses you for a moment, snapping her control over you."
            }
            Result.critical -> {
                when (damage) {
                    0 -> "While commanding you to be still, " + user.name + " starts bouncing wildly on your dick."

                    1 -> "The scent of her juices overwhelms you, " +
                            "leaving you wanting nothing more than to taste her"

                    2 -> "You feel an irresistible compulsion to lay down on your back"
                    else -> ""
                }
            }
            Result.miss -> "You feel an uncontrollable desire to undress yourself"
            Result.normal -> user.name + "' eyes tell you to pleasure yourself for her benefit"

            Result.special -> user.name + "'s voice pulls you in and you cannot resist fucking her"

            Result.weak -> "You are desperate to see more of " + user.name + "'s body"

            else -> ""
        }
    }
}

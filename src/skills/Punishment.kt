package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Punisher
import status.Stsflag

class Punishment(user: Character) : Skill("Punishment", user) {
    init {
        addTag(Attribute.Discipline)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Discipline) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Discipline to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.has(Stsflag.composed) &&
                user.canSpend(Pool.MOJO, 80) && !user.has(Stsflag.punisher)
    }

    override fun describe(): String {
        return "Ready yourself to counter all your opponents attacks: 80 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(80)
        writeOutput(c, target)
        user.add(Punisher(user, target), c)
        user.emote(Emotion.confident, 20)
        user.emote(Emotion.dominant, 20)
    }

    override fun copy(user: Character): Skill {
        return Punishment(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You raise your riding crop into a readied position, making it abundantly clear to " + target.name + " that she’d better not try anything against you, or else punishment will quickly be coming her way."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s raises her riding crop into a readied position, and she gives you a stern smile. That smile makes it abundantly clear to you exactly what’s coming if you dare try to attack her right now.",
            user.name
        )
    }
}

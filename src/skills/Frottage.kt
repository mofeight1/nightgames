package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class Frottage(user: Character) : Skill("Frottage", user) {
    init {
        addTag(Attribute.Seduction)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 26
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 26)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.sub(user) &&
                !c.stance.penetration(user) &&
                target.isPantsless &&
                ((user.hasDick && user.isPantsless) || user.has(Trait.strapped))
    }

    override fun describe(): String {
        return "Rub yourself against your opponent"
    }

    override fun resolve(c: Combat, target: Character) {
        val strapped = user.has(Trait.strapped)
        val userDick = user.hasDick
        val targetDick = target.hasDick
        val res: Result
        if (strapped) {
            res = if (targetDick) {
                Result.strong
            } else Result.strapon
        } else if (userDick) {
            res = if (targetDick) {
                Result.special
            } else Result.normal
        } else res = Result.normal
        writeOutput(c, target, res)

        var x = Global.random(3 + user[Attribute.Seduction] / 2 + target[Attribute.Perception]).toDouble()
        var y = x
        if (strapped) {
            x = user.bonusProficiency(Anatomy.toy, x)
            target.buildMojo(-10)
        } else {
            x = user.bonusProficiency(Anatomy.genitals, x)
            y = target.bonusProficiency(Anatomy.genitals, y)
            user.pleasure(y / 2, Anatomy.genitals, combat = c)
        }
        target.pleasure(x, Anatomy.genitals, combat = c)
        user.buildMojo(20)
        user.emote(Emotion.horny, 15)
        target.emote(Emotion.horny, 15)
    }

    override fun copy(user: Character): Skill {
        return Frottage(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.special) {
            "You tease " + target.name + "'s penis with your own, dueling her like a pair of fencers."
        } else {
            "You press your hips against " + target.name + "'s, rubbing her nether lips and clit with your dick."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.strong -> {
                user.name + " thrusts her hips to prod your delicate jewels with her strap-on dildo. As you flinch and pull your hips back, she presses the toy against your cock, teasing your sensitive parts."
            }
            Result.special -> {
                user.name + " pushes her girl-cock against your the sensitive head of your member, dominating your manhood by pleasuring it with her own."
            }
            else -> ""
        }
    }
}

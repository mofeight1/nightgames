package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Masochistic
import status.Stsflag

class Masochism(user: Character) : Skill("Masochism", user) {
    init {
        addTag(Attribute.Fetish)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user)
                && user.arousal.current >= 15
                && !user.has(Stsflag.masochism)
    }

    override fun describe(): String {
        return "You and your opponent become aroused by pain: Arousal at least 15"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(Masochistic(user), c)
        target.add(Masochistic(target), c)
    }

    override fun copy(user: Character): Skill {
        return Masochism(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You fantasize about the pleasure that exquisite pain can bring. You share this pleasure with " + target.name + "."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " shivers in arousal. You're suddenly bombarded with thoughts of letting her hurt you in wonderful ways."
    }
}

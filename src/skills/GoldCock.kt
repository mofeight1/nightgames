package skills

import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import status.GoldenCock
import status.Stsflag

class GoldCock(user: Character) : Skill("Golden Cock", user) {
    override fun requirements(user: Character): Boolean {
        return user.has(Trait.goldcock)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.canSpend(Pool.MOJO, 30) &&
                !user.has(Stsflag.goldencock) &&
                user.isPantsless
    }

    override fun describe(): String {
        return "Power up your cock: 30 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(GoldenCock(user), c)
    }

    override fun copy(user: Character): Skill {
        return GoldCock(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You focus for a moment to release the extraordinary power of your cock. Your member emits a bright golden glow."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " "
    }
}

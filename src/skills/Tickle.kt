package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import items.Attachment
import items.Toy
import status.Horny
import status.Hypersensitive
import kotlin.math.max

class Tickle(user: Character) : Skill("Tickle", user) {
    init {
        addTag(SkillTag.TICKLE)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                (c.stance.mobile(user) || c.stance.dom(user)) &&
                (c.stance.reachTop(user) || c.stance.reachBottom(user))
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        val mod: Result
        var level = 0
        var t = max(
            (2 + Global.random(3 + target[Attribute.Perception]) - (target.tops.size + target.bottoms.size)).toDouble(),
            1.0
        )
        var w = max(
            (Global.random(3 + target[Attribute.Perception]) - (target.tops.size + target.bottoms.size)).toDouble(),
            1.0
        )
        if (target.has(Trait.ticklish)) {
            t *= 1.3
            w += 1.3 * t
        }
        if (user.has(Trait.ticklemonster) && target.isNude) {
            mod = Result.special
            t *= 1.5
            w *= 1.5
        } else if (hastickler() && Global.random(2) == 1 && (!user.human() || c.isAllowed(SkillTag.TOY, user))) {
            level++
            if (user.has(Attachment.TicklerFluffy)) {
                level++
                t *= 1.5
                w *= 1.5
            }
            if (target.isPantsless && c.stance.reachBottom(user)) {
                mod = Result.strong
                t *= 1.3
                w *= 1.3
            } else if (target.isTopless && c.stance.reachTop(user)) {
                mod = Result.item
                t *= 1.2
                w *= 1.2
            } else {
                mod = Result.weak
                t *= 1.1
                w *= 1.1
            }
        } else {
            mod = Result.normal
        }
        writeOutput(c, target, mod, level)
        if (user.has(Toy.Tickler2) && Global.random(2) == 1 && user.canSpend(Pool.MOJO, 10) && (!user.human() || c.isAllowed(SkillTag.TOY, user))) {
            user.spendMojo(10)
            target.add(Hypersensitive(target), c)
            writeOutput(c, target, Result.critical, 0)
        }
        if (user.has(Attachment.TicklerPheromones) && Global.random(4) == 1 && (!user.human() || c.isAllowed(SkillTag.TOY, user))) {
            target.add(Horny(target, 6.0, 2), c)
            writeOutput(c, target, Result.critical, 1)
        }
        if (mod == Result.special) {
            target.tempt(t, user.bonusTemptation(), combat = c)
            target.pleasure(1.0, Anatomy.genitals, combat = c)
            target.weaken(w, c)
            user.buildMojo(15)
        } else {
            target.tempt(t, user.bonusTemptation(), Result.foreplay, c)
            target.weaken(w, c)
            user.buildMojo(10)
        }
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                c.offerImage("Tickle.jpg", "Art by AimlessArt")
            }
        }
    }

    override fun requirements(user: Character): Boolean {
        return !user.has(Trait.cursed)
    }

    override fun copy(user: Character): Skill {
        return Tickle(user)
    }

    override var speed = 7

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You try to tickle " + target.name + ", but she squirms away."
            }
            Result.special -> {
                "You work your fingers across " + target.name + "'s most ticklish and most erogenous zones until she's a writhing in pleasure and can't even make coherent words."
            }
            Result.critical -> {
                when (damage) {
                    1 -> "She looks confused as her face gradually flushes. The pheromones leaking from your tickler must be affecting her"
                    else -> "You brush your tickler over " + target.name + "'s body, causing her to shiver and retreat. When you tickle her again, she yelps and almost falls down. " +
                            "It seems like your special feathers made her more sensitive than usual."
                }
            }
            Result.strong -> {
                when (damage) {
                    2 -> "You attack " + target.name + "'s sensitive vulva with your fluffy tickler. She shivers and moans at the supernaturally soft sensation."
                    else -> "You run your tickler across " + target.name + "'s sensitive thighs and pussy. She can't help but let out a quiet whimper of pleasure."
                }
            }
            Result.item -> {
                when (damage) {
                    2 -> "You tease " + target.name + "'s bare breasts with your fluffy tickler, causing her to whimper quietly."
                    else -> "You tease " + target.name + "'s naked upper body with your feather tickler, paying close attention to her nipples."
                }
            }
            Result.weak -> {
                when (damage) {
                    2 -> "You use your tickler to tease " + target.name + "'s sensitive ears and neck."
                    else -> "You catch " + target.name + " off guard by tickling her neck and ears."
                }
            }
            else -> {
                when (Global.random(2)) {
                    1 -> "You give " + target.name + " a playful smile before launching an all-out tickle attack."
                    else -> "You tickle " + target.name + "'s sides as she giggles and squirms."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " tries to tickle you, but fails to find a sensitive spot."
            }
            Result.special -> {
                when (damage) {
                    1 -> user.name + " tickles your nude body mercilessly, gradually working her way to your dick and balls. As her fingers start tormenting you privates, you struggle to " +
                            "clear your head enough to keep from ejaculating immediately."

                    else -> user.name + " tickles your nude body mercilessly, gradually working her way to your dick and balls. As her fingers start tormenting you privates, you struggle to " +
                            "clear your head enough to keep from ejaculating immediately."
                }
            }
            Result.critical -> {
                when (damage) {
                    1 -> "You gradually feel a soft heat spread from where you were tickled. You must be affected by an aphrodisiac."
                    else -> "After she stops, you feel an unnatural sensitivity where it touched touched you."
                }
            }
            Result.strong -> {
                when (damage) {
                    2 -> user.name + " rubs her mystically soft tickler over your sensitive dick and balls."
                    else -> user.name + " brushes her tickler over your balls and teases the sensitive head of your penis."
                }
            }
            Result.item -> {
                when (damage) {
                    2 -> user.name + " teases your bare upper body with her fluffy tickler."
                    else -> user.name + " runs her feather tickler across your nipples and abs."
                }
            }
            Result.weak -> {
                when (damage) {
                    2 -> user.name + " attacks your exposed skin with a fluffy tickler."
                    else -> user.name + " pulls out a feather tickler and teases any exposed skin she can reach."
                }
            }
            else -> {
                user.name + " suddenly springs toward you and tickles you relentlessly until you can barely breathe."
            }
        }
    }

    override fun describe(): String {
        return "Tickles opponent, weakening and arousing her. More effective if she's nude"
    }

    private fun hastickler(): Boolean {
        return user.has(Toy.Tickler) || user.has(Toy.Tickler2)
    }
}

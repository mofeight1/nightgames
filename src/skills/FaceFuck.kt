package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import items.Toy
import status.Shamed

class FaceFuck(user: Character) : Skill("Face Fuck", user) {
    init {
        addTag(Attribute.Fetish)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(user) &&
                c.stance.reachTop(user) &&
                ((user.isPantsless && user.hasDick) || user.has(Trait.strapped)) &&
                !c.stance.penetration(user) &&
                !c.stance.penetration(target) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                user.arousal.current >= 15
    }

    override fun describe(): String {
        return "Force your opponent to orally pleasure you."
    }

    override fun resolve(c: Combat, target: Character) {
        val res = if (user.has(Trait.strapped)) {
            if (user.has(Toy.Strapon2)) {
                Result.upgrade
            } else Result.special
        } else Result.normal
        writeOutput(c, target, res)
        if (res == Result.normal) {
            var m = Global.random(user[Attribute.Perception] / 2) + 1.0
            m = target.bonusProficiency(Anatomy.mouth, m)
            user.pleasure(m, Anatomy.genitals, combat = c)
        }
        if (res == Result.upgrade) {
            target.tempt(Global.random(3).toDouble(), combat = c)
        }
        target.add(Shamed(target, user[Attribute.Fetish] / 5.0), c)
        user.buildMojo(15)
        target.spendMojo(2 * user[Attribute.Seduction])
    }

    override fun copy(user: Character): Skill {
        return FaceFuck(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You grab hold of " + target.name + "'s head and push your cock into her mouth. She flushes in shamed and anger, but still dutifully services you with her lips " +
                "and tongue while you thrust your hips."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " forces her strap-on cock into your mouth and fucks your face with it. It's only rubber, but your position is still humiliating. You struggle not " +
                        "to gag on the artificial member while " + user.name + " revels in her dominance."
            }
            Result.upgrade -> {
                user.name + " slightly moves forward on you, pushing her Strap-On against your lips. You try to keep your mouth closed but " + user.name + " holds your nose together, " +
                        "forcing you to eventually part your lips and suck on the rubbery invader. After a few sucks, you manage to push it out, although you're still shivering " +
                        "with a mix of arousal and humiliation."
            }
            Result.normal -> {
                user.name + " forces your mouth open and shoves her sizable girl-cock into it. You're momentarily overwhelmed by the strong, musky smell and the taste, but " +
                        "she quickly starts moving her hips, fucking your mouth like a pussy. You feel your cheeks redden in shame, but you still do what you can to pleasure her. " +
                        "She may be using you like a sex toy, but you're going to try to scrounge whatever advantage you can get."
            }
            else -> ""
        }
    }
}

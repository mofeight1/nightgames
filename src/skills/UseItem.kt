package skills

import characters.Character
import combat.Combat
import items.Item

abstract class UseItem(private val consumable: Item, user: Character) :
    Skill(consumable.getFullName(user), user) {
    init {
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && user.has(consumable)
    }

    override fun describe(): String {
        return consumable.getFullDesc(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }
}

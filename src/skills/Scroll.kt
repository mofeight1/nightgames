package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import items.Consumable

class Scroll(self: Character) : UseItem(Consumable.FaeScroll, self) {
    init {
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 2
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 2)
    }

    override fun resolve(c: Combat, target: Character) {
        user.consume(Consumable.FaeScroll, 1)
        if (target.isNude) {
            writeOutput(c, target)
            target.pleasure(25.0 + user[Attribute.Arcane], Anatomy.genitals, combat = c)
        } else {
            writeOutput(c, target, Result.weak)
            target.undress(c)
        }
    }

    override fun copy(user: Character): Skill {
        return Scroll(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.weak) {
            "You unroll the summoning scroll and unleash a cloud of cute, naked faeries. They immediately swarm around " + target.name + ", grabbing and pulling at her " +
                    "clothes. By the time they disappear, she's left completely naked."
        } else {
            "You unroll the summoning scroll and unleash a cloud of cute, naked faeries. They eagerly take advantage of " + target.name + "s naked body, teasing and tickling " +
                    "every exposed erogenous zone. She tries in vain to defend herself, but there are too many of them and they're too quick. She's reduced to writhing and giggling " +
                    "in pleasure until the brief summoning spell expires."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.weak) {
            user.name + " pulls out a scroll and a swarm of butterfly-winged faeries burst forth to attack you. They mischievously grab at your clothes, using magical assistance " +
                    "to efficiently strip you naked."
        } else {
            user.name + " pulls out a scroll and a swarm of butterfly-winged faeries burst forth to attack you. A couple of them fly into your face to distract you with naked girl " +
                    "parts, while the rest play with your naked body. They focus especially on your dick and balls, dozens of tiny hands playfully immobilizing you with ticklish pleasure. The spell " +
                    "doesn't actually last very long, but from your perspective, it feels like minutes of delightful torture."
        }
    }
}

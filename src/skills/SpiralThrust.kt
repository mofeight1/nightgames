package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import stance.Stance
import kotlin.math.min
import kotlin.math.roundToInt

class SpiralThrust(user: Character) : Skill("Spiral Thrust", user) {
    init {
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.has(Trait.spiral)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.dom(user) &&
                c.stance.penetration(user) &&
                user.canSpend(Pool.MOJO, 10)
    }

    override fun resolve(c: Combat, target: Character) {
        var x = user.mojo.current / 2.0
        x = min(x, 20.0 + user.level)
        val m = user.bonusProficiency(Anatomy.genitals, x)
        if (c.stance.en == Stance.anal) {
            writeOutput(c, target, Result.anal)
            target.pleasure(m, Anatomy.ass, combat = c)
        } else {
            writeOutput(c, target, Result.normal)
            target.pleasure(m, Anatomy.genitals, combat = c)
        }

        user.spendMojo((2 * x).roundToInt())
        var r = target.getSexPleasure(3, Attribute.Seduction)
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }

        user.pleasure(r, Anatomy.genitals, combat = c)
        user.emote(Emotion.horny, 20)
        c.stance.pace = 2
    }

    override fun copy(user: Character): Skill {
        return SpiralThrust(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            "You unleash your strongest technique into " + target.name + "'s ass, spiraling your hips and stretching her tight sphincter."
        } else {
            "As you thrust into " + target.name + "'s hot pussy, you feel a power welling up inside you. You put everything you have into moving your hips circularly " +
                    "while you continue to drill into her."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            user.name + " drills into your ass with extraordinary power. Your head seems to go blank and you fall face down to the ground as your arms turn to jelly and give out."
        } else {
            user.name + " begins to move her hips wildly in circles, rubbing every inch of your cock with her hot, slippery pussy walls, bringing you more pleasure " +
                    "than you thought possible."
        }
    }

    override fun describe(): String {
        return "Converts your mojo into fucking: All Mojo"
    }
}

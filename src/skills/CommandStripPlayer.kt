package skills

import characters.Character
import combat.Combat
import combat.Result

class CommandStripPlayer(user: Character) : PlayerCommand("Force Strip Player", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return super.usable(c, target) && !user.isNude
    }

    override fun describe(): String {
        return "Make your thrall undress you."
    }

    override fun resolve(c: Combat, target: Character) {
        user.undress(c)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return CommandStripPlayer(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "With an elated gleam in her eyes, " + target.name +
                " moves her hands with nigh-inhuman dexterity, stripping all" +
                " of your clothes in just a second."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "<<This should not be displayed, please inform The" +
                " Silver Bard: CommandStripPlayer-receive>>"
    }
}

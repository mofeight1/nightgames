package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Charmed
import status.Enthralled
import status.Stsflag

class Suggestion(user: Character) : Skill("Suggestion", user) {
    init {
        addTag(Attribute.Hypnosis)
        addTag(SkillTag.CHARMING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Hypnosis) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Hypnosis to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.mobile(user) && !c.stance.behind(user) &&
                !c.stance.behind(target) && !c.stance.sub(user) && !target.has(Stsflag.charmed) && !target.has(Stsflag.enthralled) &&
                user.canSpend(Pool.MOJO, 5)
    }

    override fun describe(): String {
        return "Hypnotize your opponent so she can't defend herself"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(5)
        if (!target.has(Stsflag.cynical)) {
            if (user.getPure(Attribute.Hypnosis) >= 10) {
                writeOutput(c, target, Result.strong)
                target.add(Enthralled(target, user), c)
            } else {
                writeOutput(c, target, Result.normal)
                target.add(Charmed(target, 2 + user.bonusCharmDuration()), c)
            }
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return Suggestion(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format(
                "You attempt to put %s under hypnotic suggestion, but %s doesn't appear to be affected.",
                target.name,
                target.pronounSubject(false)
            )
        } else {
            String.format(
                "You speak in a calm, rhythmic tone, lulling %s into a hypnotic trance. Her eyes seem to glaze over slightly, momentarily "
                        + "slipping under your influence.", target.name
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " attempts to put you under hypnotic suggestion, but you manage to regain control of your consciousness."
        } else {
            (user.name + " speaks in a firm, but relaxing tone, attempting to put you into a trance. Obviously you wouldn't let yourself be "
                    + "hypnotized in the middle of a match, right? ...Right? ...Why were you fighting her again?")
        }
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class StripTop(user: Character) : Skill("Strip Top", user) {
    init {
        addTag(SkillTag.STRIPPING)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachTop(user) && !target.isTopless && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        val strip = user[Attribute.Cunning]
        if (!target.stripAttempt(strip, user, c, target.tops.last().item)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target)
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Jewel")) {
                c.offerImage("Strip Top Female.jpg", "Art by AimlessArt")
            }
        }

        target.strip(0, c)
        if (user.getPure(Attribute.Cunning) >= 30 && !target.isTopless) {
            if (target.stripAttempt(strip, user, c, target.tops.last().item)) {
                writeOutput(c, target, Result.strong)
                target.strip(0, c)
                target.emote(Emotion.nervous, 10)
            }
        }
        if (target.isNude) {
            c.write(target, target.nakedLiner())
        }
        target.emote(Emotion.nervous, 10)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return StripTop(user)
    }

    override var speed = 3

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                when (Global.random(2)) {
                    1 -> "${target.name} blocks your attempt to remove her ${target.tops.last()}."
                    else -> "You attempt to strip off ${target.name}'s ${target.tops.last()}, but ${target.pronounSubject(false)} shoves you away."
                }
            }
            Result.strong -> {
                "Taking advantage of the situation, you also manage to snag ${target.possessive(false)} ${target.tops.last()}"
            }
            else -> {
                if (target.canAct()) {
                    when (Global.random(4)) {
                        3 -> "After a brief struggle, you manage to pull off ${target.name}'s ${target.tops.last()}."
                        2 -> "With some resistance, you take off ${target.name}'s ${target.tops.last()}."
                        1 -> "You pull ${target.name}'s ${target.tops.last()} up and over her head as she flails around trying to stop you."
                        else -> "${target.name} can't even put up a fight while you take off her ${target.tops.last()}."
                    }
                } else {
                    "You remove ${target.name}'s ${target.tops.last()} without any resistance."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                when (Global.random(2)) {
                    1 -> "${user.name} pulls on your ${target.tops.last()}, but you push her off before she can take it."
                    else -> "${user.name} tries to yank off your ${target.tops.last()}, but you manage to hang onto it."
                }
            }
            Result.strong -> {
                "Before you can react, ${user.pronounSubject(false)} also strips off your ${target.tops.last()}"
            }
            else -> {
                if (target.canAct()) {
                    when (Global.random(2)) {
                        1 -> "${user.name} gets a hold of your ${target.tops.last()} and pulls hard, almost knocking you over as she takes it off."
                        else -> "${user.name} grabs a hold of your ${target.tops.last()} and yanks it off before you can stop her."
                    }
                } else {
                    when (Global.random(2)) {
                        1 -> "${user.name} casually pulls off your ${target.tops.last()} while you're incapacitated."
                        else -> "${user.name} easily strips off your ${target.tops.last()} while you are unable to fight back."
                    }
                }
            }
        }
    }


    override fun describe(): String {
        return "Attempt to remove opponent's top. More likely to succeed if she's weakened and aroused"
    }
}

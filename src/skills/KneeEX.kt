package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class KneeEX(user: Character) : Skill("Mighty Knee", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.PAINFUL)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                !c.stance.behind(target) &&
                !c.stance.penetration(target) &&
                user.canSpend(Pool.MOJO, 20) &&
                !user.has(Trait.sportsmanship)
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m: Double
        var style = Result.powerful
        if (user[Attribute.Seduction] > user[Attribute.Cunning] && user[Attribute.Seduction] > user[Attribute.Power]) {
            style = Result.seductive
        } else if (user[Attribute.Cunning] > user[Attribute.Power]) {
            style = Result.clever
        }
        writeOutput(c, target, style)
        if (target.human()) {
            if (Global.random(5) >= 3) {
                c.write(user, user.bbLiner())
            }
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.CASSIE) {
                c.offerImage("Knee.png", "Art by AimlessArt")
            }
        }
        when (style) {
            Result.powerful -> {
                m = 4.0 + Global.random(11) + user[Attribute.Power]
                m *= 1.3
            }

            Result.clever -> {
                m = 4.0 + Global.random(11) + user[Attribute.Cunning]
            }

            else -> {
                m = 4.0 + Global.random(11) + user[Attribute.Seduction]
            }
        }

        target.pain(m, Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 2, c)
        } else {
            target.calm(m, c)
        }
        target.emote(Emotion.angry, 40)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 10 && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 10)
    }

    override fun copy(user: Character): Skill {
        return KneeEX(user)
    }

    override var speed = 4

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            return target.name + " blocks your knee strike."
        }
        return if (target.hasBalls) {
            when (modifier) {
                Result.powerful -> {
                    String.format(
                        "You brace yourself for a moment, before unleashing your strongest knee strike to %s's vulnerable ballsack. "
                                + "Her eyes water, and her mouth drops open in silent agony.", target.name
                    )
                }
                Result.clever -> {
                    String.format(
                        "You distract %s with a cunning feint, before striking with a knee-shaped sneak attack to her dangling balls. "
                                + "You don't have a lot of power behind it, but accuracy and surprise do the job just fine.",
                        target.name
                    )
                }
                else -> {
                    String.format(
                        "You seductively pull %s into a heated kiss. You feel her erection poking you, which helps you locate your real "
                                + "target without looking. You deliver a quick, decisive knee strike to her defenseless balls. You feel her completely freeze in shock and "
                                + "pain, but hold the kiss for another couple seconds before releasing her.", target.name
                    )
                }
            }
        } else {
            when (modifier) {
                Result.powerful -> {
                    "You brace yourself for a moment, before unleashing your strongest knee strike to ${target.name}'s unguarded groin. " +
                        "With no testicles to protect, she probably wasn't expecting a low blow. Judging by her pained expression, " +
                        "her pussy is a pretty effective target."
                }
                Result.clever -> {
                    "You distract ${target.name} with a cunning feint, before delivering a sharp knee strike to her sensitive vulva. It's a " +
                        "quick attack without much power, but she was completely unprepared for it. A true critical hit."
                }
                Result.seductive -> {
                    "You tease ${target.name} with multiple quick kisses, until you're sure she's completely focused on your lips. While she's " +
                        "distracted and defenseless, you knee her firmly in the pussy. Her eyes go wide in shock for a moment, before they start to " +
                        "water from the pain."
                }
                else -> ""
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> "${user.name} tries to knee you in the balls, but you block it."

            Result.powerful -> "${user.name} grabs you by the shoulders and slams her knee into your groin with devastating force. The pain is " +
                "indescribable, and for a second you wonder if your testicles were pushed into your body by the force."

            Result.clever -> "${user.name}'s eyes momentarily dart past you. She tries to hide her reaction, but not quite good enough. You reflexively " +
                "look behind you, but there's nothing there. Suddenly, a burst of pain from your groin hits you. You left your guard down just " +
                "long enough for a sneaky knee to the balls."

            Result.seductive -> "${user.name} licks her lips seductively, before leaning in to claim yours. Despite the feeling that she has the " +
                "advantage here, you can't resist accepting the kiss eagerly. The moment your lips meet, she plants her knee painfully " +
                "between your legs. The kiss was just a diversion. Her real target was your delicate balls."

            else -> ""
        }
    }

    override fun toString(): String {
        return if (user[Attribute.Seduction] > user[Attribute.Cunning] && user[Attribute.Seduction] > user[Attribute.Power]) {
            "Kiss and Knee"
        } else if (user[Attribute.Cunning] > user[Attribute.Power]) {
            "Tricky Knee"
        } else {
            name
        }
    }

    override fun describe(): String {
        return if (user[Attribute.Seduction] > user[Attribute.Cunning] && user[Attribute.Seduction] > user[Attribute.Power]) {
            "Knee strike after lowering your target's guard with your charms: 15 Mojo"
        } else if (user[Attribute.Cunning] > user[Attribute.Power]) {
            "Knee strike while misdirecting your target: 15 Mojo"
        } else {
            "Mojo boosted Knee strike: 15 Mojo"
        }
    }
}

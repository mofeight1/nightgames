package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result

class Rewind(user: Character) : Skill("Rewind", user) {
    init {
        addTag(Attribute.Temporal)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 10
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 10)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                user.canSpend(Pool.TIME, 8)
    }

    override fun describe(): String {
        return "Rewind your personal time to undo all damage you've taken: 8 charges"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.TIME, 8)
        user.arousal.empty()
        user.stamina.fill()
        user.clearStatus()
        writeOutput(c, target)
        user.emote(Emotion.confident, 25)
        user.emote(Emotion.dominant, 20)
        target.emote(Emotion.nervous, 10)
        target.emote(Emotion.desperate, 10)
    }

    override fun copy(user: Character): Skill {
        return Rewind(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "It takes a lot of time energy, but you manage to rewind your physical condition back to the very start "
                    + "of the match, removing all damage you've taken."
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s hits a button on %s wristband and suddenly seems to completely recover. It's like nothing "
                    + "you've done even happened.", user.name, user.possessive(false)
        )
    }
}

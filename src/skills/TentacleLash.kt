package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Stsflag

class TentacleLash(user: Character) : Skill("Tentacle Lash", user) {
    init {
        addTag(Attribute.Eldritch)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Eldritch) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Eldritch to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !user.stunned() && !user.distracted() && !user.has(Stsflag.enthralled)
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
    }

    override fun copy(user: Character): Skill {
        return TentacleLash(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

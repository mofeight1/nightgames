package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import items.Flask

class GoodnightKiss(user: Character) : Skill("Goodnight Kiss", user) {
    init {
        addTag(Attribute.Ninjutsu)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.kiss(user) &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 20) &&
                user.has(Flask.Sedative)
    }

    override fun describe(): String {
        return "Deliver a powerful knockout drug via a kiss: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        user.consume(Flask.Sedative, 1)
        writeOutput(c, target)
        target.tempt(Global.random(4).toDouble(), combat = c)
        target.weaken(target.stamina.current.toDouble())
        target.stamina.set(1)
    }

    override fun copy(user: Character): Skill {
        return GoodnightKiss(user)
    }

    override var speed = 7

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You surreptitiously coat your lips with a powerful sedative, careful not to accidently ingest any. As soon as you see an opening, "
                    + "you dart in and kiss %s softly. Only a small amount of the drug is actually transferred by the kiss, but it's enough. %s immediately staggers "
                    + "as the strength leaves %s body.",
            target.name,
            target.pronounSubject(true),
            target.possessive(false)
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s lunges toward you and kisses you energetically. You respond immediately, but before you can deepen the kiss, %s dodges back with a "
                    + "wink. A second later, you realize something is very wrong. A chill spreads through your body as your strength drains away. %s quickly wipes the "
                    + "remaining drug from her lips with a mischievous grin.",
            user.name,
            user.pronounSubject(false),
            user.name
        )
    }
}

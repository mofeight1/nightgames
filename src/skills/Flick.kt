package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class Flick(user: Character) : Skill("Flick", user) {
    init {
        addTag(SkillTag.MISCHIEF)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                c.stance.reachBottom(user) &&
                user.canAct() &&
                !c.stance.penetration(target) &&
                !user.has(Trait.shy)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = Global.random(6) + 5.0
        writeOutput(c, target)
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.ANGEL) {
                c.offerImage("Flick.jpg", "Art by AimlessArt")
            }
        }
        if (target.has(Trait.achilles)) {
            m += 2 + Global.random(target[Attribute.Perception] / 2)
        }
        target.pain(m, Anatomy.genitals, c)
        target.mojo.reduce(15)
        user.buildMojo(15)
        user.emote(Emotion.dominant, 10)
        target.emote(Emotion.angry, 40)
        target.emote(Emotion.nervous, 15)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 17 && !user.has(Trait.softheart)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 17)
    }

    override fun copy(user: Character): Skill {
        return Flick(user)
    }

    override var speed = 6

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You flick your finger between " + target.name + "'s legs, but don't hit anything sensitive."
        } else {
            if (target.hasBalls) {
                "You use two fingers to simultaneously flick both of " + target.name + " dangling balls. She tries to stifle a yelp and jerks her hips away reflexively. " +
                        "You feel a twinge of empathy, but she's done far worse."
            } else {
                "You flick your finger sharply across " + target.name + "'s sensitive clit, causing her to yelp in surprise and pain. She quickly covers her girl parts " +
                        "and glares at you in indignation."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " flicks at your balls, but hits only air."
        } else {
            when (Global.random(2)) {
                1 -> (user.name + "  flicks your balls lightly with one finger, hitting both in a single flick.  It doesn't hurt very much, but you still let out a "
                        + "quick and high-pitched yelp of surprise as your body flinches forward.")

                else -> user.name + " gives you a mischievous grin and flicks each of your balls with her finger. It startles you more than anything, but it does hurt and " +
                        "her seemingly carefree abuse of your jewels destroys your confidence."
            }
        }
    }

    override fun describe(): String {
        return "Flick opponent's genitals, which is painful and embarrassing"
    }
}

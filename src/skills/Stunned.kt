package skills

import characters.Character
import combat.Combat
import combat.Result
import global.Global

class Stunned(user: Character) : Skill("Stunned", user) {
    override fun usable(c: Combat, target: Character): Boolean {
        return user.stunned()
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.human()) {
            c.write(user, deal(target, Result.normal, 0))
        } else if (target.human()) {
            if (Global.random(3) >= 2) {
                c.write(user, user.stunLiner())
            } else {
                c.write(user, receive(target, Result.normal, 0))
            }
        }
    }

    override fun requirements(user: Character): Boolean {
        return false
    }

    override fun copy(user: Character): Skill {
        return Stunned(user)
    }

    override var speed = 0

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You're unable to move."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " is on the floor, trying to catch her breath."
    }

    override fun describe(): String {
        return "You're stunned"
    }
}

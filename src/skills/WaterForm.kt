package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Stsflag
import status.WaterStance

class WaterForm(user: Character) : Skill("Water Form", user) {
    init {
        addTag(Attribute.Ki)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                !c.stance.sub(user) &&
                !user.has(Stsflag.form)
    }

    override fun describe(): String {
        return "Improves evasion and counterattack rate at expense of Power"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(WaterStance(user), c)
    }

    override fun copy(user: Character): Skill {
        return WaterForm(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You relax your muscles, prepared to flow with and counter " + target.name + "'s attacks."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " takes a deep breath and her movements become much more fluid."
    }
}

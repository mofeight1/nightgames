package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import stance.Behind
import stance.Stance
import status.Braced
import status.Stsflag

class Turnover(user: Character) : Skill("Turn Over", user) {
    init {
        addTag(SkillTag.OUTMANEUVER)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Power) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.en == Stance.standingover && c.stance.dom(user)
    }

    override fun describe(): String {
        return "Turn your opponent over and get behind her"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        if (!target.has(Stsflag.braced)) {
            target.add(Braced(target))
        }
        c.stance = Behind(user, target)
        target.emote(Emotion.dominant, 20)
    }

    override fun copy(user: Character): Skill {
        return Turnover(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You turn " + target.name + " onto her hands and knees. You moved behind her while she slowly gets up."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " rolls you onto your stomach. You push yourself back up, but she takes the opportunity to get behind you."
    }
}

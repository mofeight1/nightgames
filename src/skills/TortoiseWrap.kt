package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import items.Component
import status.Hypersensitive
import status.Stsflag
import status.Tied

class TortoiseWrap(user: Character) : Skill("Tortoise Wrap", user) {
    init {
        addTag(Attribute.Fetish)
        addTag(SkillTag.CONSUMABLE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Fetish) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Fetish to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.reachTop(user) &&
                !c.stance.reachTop(target) &&
                user.has(Component.Rope)
                && c.stance.dom(user) &&
                !target.has(Stsflag.tied) &&
                user.has(Stsflag.bondage)
    }

    override fun describe(): String {
        return "User your bondage skills to wrap your opponent to increase her sensitivity"
    }

    override fun resolve(c: Combat, target: Character) {
        user.consume(Component.Rope, 1)
        writeOutput(c, target)
        target.add(Tied(target), c)
        target.add(Hypersensitive(target), c)
    }

    override fun copy(user: Character): Skill {
        return TortoiseWrap(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You skillfully tie a rope around " + target.name + "'s torso in a traditional bondage wrap. She moans softly as the rope digs into her supple skin."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " ties you up with a complex series of knots. Surprisingly, instead of completely incapacitating you, she wraps you in a way that only " +
                "slightly hinders your movement. However the discomfort of the rope wrapping around you seems to make your sense of touch more pronounced."
    }
}

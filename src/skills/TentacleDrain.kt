package skills

import characters.Attribute
import characters.Character
import characters.ID
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Stsflag

class TentacleDrain(user: Character) : Skill("Tentacle Drain", user) {
    init {
        addTag(Attribute.Eldritch)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Eldritch) >= 24
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Eldritch to 24)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !user.stunned() &&
                !user.distracted() &&
                !user.has(Stsflag.enthralled) &&
                target.isPantsless
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target, damage = 1)
        if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.SELENE) {
                c.offerImage("Selene Drain.jpg", "Art by AimlessArt")
            }
        }
    }

    override fun copy(user: Character): Skill {
        return TentacleDrain(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

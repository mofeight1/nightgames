package skills

import characters.Anatomy
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result

class Masturbate(user: Character) : Skill("Masturbate", user) {
    init {
        addTag(SkillTag.MASTURBATION)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.mobile(user) && !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        if (user.hasDick && user.arousal.current <= 15)
            writeOutput(c, target, Result.weak)
        else {
            writeOutput(c, target, Result.normal)
            if (user.human()) {
                c.offerImage("Masturbation.jpg", "Art by AimlessArt")
            }
        }
        user.pleasure(20.0, Anatomy.genitals, combat = c)
        if (user.has(Trait.showoff)) {
            user.buildMojo(80)
        } else {
            user.buildMojo(20)
        }
        user.emote(Emotion.horny, 20)
    }

    override fun copy(user: Character): Skill {
        return Masturbate(user)
    }

    override fun type(): Tactics {
        return Tactics.misc
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.weak) {
            "You take hold of your flaccid dick, tugging and rubbing it into a full erection."
        } else {
            "You jerk off, building up your own arousal."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " slips her hand between her legs and starts fingering herself."
    }

    override fun describe(): String {
        return "Raises your own Arousal and boosts your Mojo"
    }
}

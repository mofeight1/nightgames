package skills

import characters.Character
import combat.Combat
import combat.Result

class CommandStrip(user: Character) : PlayerCommand("Force Strip Self", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return super.usable(c, target) && !target.isNude
    }

    override fun describe(): String {
        return "Force your opponent to strip naked."
    }

    override fun resolve(c: Combat, target: Character) {
        target.undress(c)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return CommandStrip(user)
    }


    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You look " + target.name + " in the eye, sending a psychic command for" +
                " her to strip. She complies without question, standing before you nude only" +
                " seconds later."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "<<This should not be displayed, please inform The Silver Bard: CommandStrip-receive>>"
    }
}

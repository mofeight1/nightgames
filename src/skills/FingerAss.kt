package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Stsflag

class FingerAss(user: Character) : Skill("Finger Ass", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.touch)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                !c.stance.prone(target) &&
                c.stance.reachBottom(user) &&
                user.canAct() &&
                (target.has(Stsflag.oiled) || user.has(Trait.slipperyfingers))
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m = 4.0 + target[Attribute.Perception]
        var type = Result.normal
        m += Global.random(user[Attribute.Seduction] / 2)
        if (user.has(Trait.slipperyfingers)) {
            type = Result.special
        }
        writeOutput(c, target, type)
        m = user.bonusProficiency(Anatomy.fingers, m)
        target.pleasure(m, Anatomy.ass, combat = c)
    }

    override var accuracy = 6

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Seduction) >= 28
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 28)
    }

    override fun copy(user: Character): Skill {
        return FingerAss(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You grope at " + target.name + "'s ass, but miss."
            }
            Result.special -> {
                "You slip your hand into the crack of " + target.name + "’s ass. Before she can react to tense up, you slide your finger expertly into her anus. By the time she clenches in to try and keep you out, it’s too late. You wiggle your finger teasingly around within her ass for a moment before she manages to pull free.\n"
            }
            else -> {
                "With the lubricant coating " + target.name + "’s ass, it’s all too easy to slip your finger inside her anus. You press it as deep as you can and wiggle it around for a bit before she manages to pull free."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                String.format(
                    "%s gropes at your anus, but misses the mark.",
                    user.name
                )
            }
            Result.special -> {
                user.name + " slips her hand into the crack of your ass, and before you know what’s happening she plunges her finger directly into her asshole. You instinctively clench to try to keep her out, but it’s too late. She wiggles her finger around teasingly, the feeling of it inside your ass oddly arousing. You manage to shake off the stun though and pull away before she works too much magic on your asshole."
            }
            else -> {
                String.format(
                    "With lubricant coating your ass, %s has no trouble slipping her finger inside your anus. She presses it in as deep as she can, the feeling of your ass being penetrated like this oddly arousing. You manage to shake it off though and pull away before you let it feel too good.",
                    user.name
                )
            }
        }
    }

    override fun describe(): String {
        return "Digitally stimulate opponent's anus"
    }
}

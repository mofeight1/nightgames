package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import pet.Slime

class SpawnSlime(user: Character) : Skill("Create Slime", user) {
    init {
        addTag(Attribute.Science)
        addTag(SkillTag.PET)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.pet == null &&
                user.canSpend(Pool.BATTERY, 1)
    }

    override fun describe(): String {
        return "Creates a mindless, but living slime to attack your opponent: 1 Battery"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 1)
        val power = 3 + user.bonusPetPower() + (user[Attribute.Science] / 10)
        val ac = 3 + user.bonusPetEvasion() + (user[Attribute.Science] / 10)
        writeOutput(c, target)
        user.pet = Slime(user, power, ac)
        c.offerImage("Slime.png", "Art by AimlessArt")
    }

    override fun copy(user: Character): Skill {
        return SpawnSlime(user)
    }

    override fun type(): Tactics {
        return Tactics.summoning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You dispense blue slime on the floor and send a charge through it to animate it. The slime itself is not technically alive, but an extension of a larger " +
                "creature kept in Jett's lab."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " points a device at the floor and releases a blob of blue slime. The blob starts to move like a living thing and briefly takes on a vaguely humanoid shape " +
                "and smiles at you."
    }
}

package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.StoneStance
import status.Stsflag

class StoneForm(user: Character) : Skill("Stone Form", user) {
    init {
        addTag(Attribute.Ki)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                !c.stance.sub(user) &&
                !user.has(Stsflag.form)
    }

    override fun describe(): String {
        return "Improves Pain Resistance rate at expense of Speed"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(StoneStance(user), c)
    }

    override fun copy(user: Character): Skill {
        return StoneForm(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You tense your body to absorb and shrug off attacks."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " braces heruser to resist your attacks."
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Global
import stance.Mount

class Blindside(user: Character) : Skill("Blindside", user) {
    init {
        addTag(Attribute.Professional)
        addTag(SkillTag.KNOCKDOWN)
    }

    override fun requirements(user: Character): Boolean {
        return user[Attribute.Professional] >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Professional to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && !c.stance.prone(target)
    }

    override fun describe(): String {
        return "Distract your opponent and take them down."
    }

    override fun resolve(c: Combat, target: Character) {
        if (c.effectRoll(this, user, target, user[Attribute.Professional] - target.knockdownDC())) {
            writeOutput(c, target)
            c.stance = Mount(user, target)
            user.emote(Emotion.confident, 15)
            user.emote(Emotion.dominant, 15)
            target.emote(Emotion.nervous, 10)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override fun copy(user: Character): Skill {
        return Blindside(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You try to trip " + target.name + ", but she keeps her balance."
        } else {
            if (Global.random(2) == 0) {
                String.format(
                    "You run your hands along %s's back and "
                            + "whisper something extremely lewd, even for the games, in her ear. "
                            + "While she is distracted, you wrap your calf behind hers and trip her on to the "
                            + "ground, landing on top of her waist. She looks up into your eyes, surprised by what happened.",
                    target.name
                )
            } else String.format(
                "You move up to %s and kiss %s strongly. "
                        + "While %s is distracted, you throw %s down and plant "
                        + "yourself on top of %s.",
                target.name, target.pronounTarget(false), target.pronounSubject(false),
                target.pronounTarget(false), target.pronounTarget(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " hooks your ankle, but you recover without falling."
        } else {
            "Seductively swaying her hips, " + user.name + " sashays over to you. " +
                    "Her eyes fix you in place as she leans in and firmly kisses you, shoving her tongue down" +
                    " your mouth. You are so absorbed in kissing back, that you only notice her ulterior motive" +
                    " once she has already swept your legs out from under you and she has landed on top of you."
        }
    }
}

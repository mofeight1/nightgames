package skills

import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result

class DecayClothes(user: Character) : Skill("Decay Clothes", user) {
    init {
        addTag(Attribute.Unknowable)
        addTag(SkillTag.STRIPPING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Unknowable) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Unknowable to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && !target.isNude && user.canSpend(Pool.ENIGMA, 2)
    }

    override fun describe(): String {
        return "Accelerates time to decay opponents' clothes off their body: 2 Enigma"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.ENIGMA, 2)
        if (target[Attribute.Temporal] >= 2) {
            writeOutput(c, target, Result.miss)
        } else if (c.isWatching(ID.MARA)) {
            writeOutput(c, target, Result.defended)
        } else {
            target.nudify()
            writeOutput(c, target, Result.normal)
        }
    }

    override fun copy(user: Character): Skill {
        return DecayClothes(user)
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                ""
            }
            Result.defended -> {
                ""
            }
            else -> {
                ""
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "The world seems to distort around you, and your clothes seem to suddenly be much older and brittler. " +
                        "You don't know how " + user.name + " is manipulating time around you, but some quick adjustments to your " +
                        "Procrastinator undo the whole process."
            }
            Result.defended -> {
                "[Placeholder: Mara assists]"
            }
            else -> {
                "The world seems to distort around you, and your clothes seem to suddenly be much older and brittler. " +
                        "Within moments, they seem to literally crumble into dust, leaving you completely naked."
            }
        }
    }
}

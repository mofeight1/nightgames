package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import status.Stsflag

class CleanseMind(user: Character) : Skill("Cleanse Mind", user) {
    init {
        addTag(Attribute.Spirituality)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Spirituality) >= 2
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Spirituality to 2)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.has(Stsflag.mindaffecting) &&
                !user.has(Stsflag.stunned) &&
                user.canSpend(Pool.FOCUS, 2)
    }

    override fun describe(): String {
        return "Remove all mind-affecting states: 2 Focus"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.FOCUS, 2)
        writeOutput(c, target)
        user.removeStatus(Stsflag.mindaffecting, c)
        user.emote(Emotion.confident, 100)
    }

    override fun copy(user: Character): Skill {
        return CleanseMind(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Something deep in your soul rings like an alarm clock, snapping your mind from its haze."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + "'s eyes suddenly regain clarity. She seems to have cleansed all distortion from her mind."
    }
}

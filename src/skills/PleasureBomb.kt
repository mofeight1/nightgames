package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import global.Global

class PleasureBomb(user: Character) : Skill("Pleasure Bomb", user) {
    init {
        addTag(Attribute.Ki)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return target.isPantsless &&
                c.stance.reachBottom(user) &&
                c.stance.dom(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun describe(): String {
        return "Consumes all your stamina to deliver a slow, but powerful pleasure attack. More effective against stunned opponents."
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            target.emote(Emotion.nervous, 10)
            return
        }
        var m = user.stamina.current.toDouble() + user[Attribute.Seduction] + user[Attribute.Ki] + Global.random(target[Attribute.Perception])
        writeOutput(c, target)
        user.stamina.empty()
        if (!target.canAct()) {
            m *= 2
        }
        m = user.bonusProficiency(Anatomy.fingers, m)
        target.pleasure(m, Anatomy.genitals, combat = c)
        target.emote(Emotion.horny, 20)
    }

    override fun copy(user: Character): Skill {
        return PleasureBomb(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override var speed = 1

    override var accuracy = 1

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format(
                "You charge up your hand with all of your Ki and aim for %s's groin. "
                        + "At the last second %s jerks %s hips out of the way and the energy disperses harmlessly, leaving you drained.",
                target.name, target.pronounSubject(false), target.possessive(false)
            )
        } else if (target.hasDick) {
            String.format(
                "You charge up your hand with all of your Ki and aim for %s's groin. You grab %s penis and pour your energy "
                        + "into it while stroking rapidly. %s can't help moaning as you overwhelm %s with intense pleasure.",
                target.name, target.possessive(false), target.pronounSubject(true), target.pronounTarget(false)
            )
        } else {
            String.format(
                "You charge up your hand with all of your Ki and aim for %s's groin. You focus the massive amount of energy "
                        + "into your fingertips as you rub her clit directly. She can't help moaning as you overwhelm %s with intense pleasure.",
                target.name, target.pronounTarget(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            String.format(
                "%s raises %s hand and it begins to glow with tremendous power. %s grabs for your dick, but you manage to "
                        + "dodge out of the way at the last moment.",
                user.name, user.possessive(false), user.pronounSubject(true)
            )
        } else {
            String.format(
                "%s raises %s hand and it begins to glow with tremendous power. %s grabs your cock, pumping energetically. "
                        + "Your hips buck involuntarily as you're suddenly assaulted with intense pleasure. The sensation is so overwhelming, it's "
                        + "almost painful.",
                user.name, user.possessive(false), user.pronounSubject(true)
            )
        }
    }
}

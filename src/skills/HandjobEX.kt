package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Stance
import status.ProfMod
import status.ProfessionalMomentum

class HandjobEX(user: Character) : Skill("Sensual Handjob", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.touch)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachBottom(user) &&
                (target.isPantsless || (user.has(Trait.dexterous) && target.bottoms.size <= 1)) &&
                target.hasDick &&
                user.canAct() &&
                (!c.stance.penetration(target) || c.stance.en == Stance.anal) &&
                user.canSpend(Pool.MOJO, 20)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        var m: Double
        var style = Result.seductive
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            style = Result.powerful
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            style = Result.clever
        }
        user.spendMojo(20)
        when (style) {
            Result.powerful -> {
                m = 4.0 + Global.random(user[Attribute.Power] + user[Attribute.Professional]) + target[Attribute.Perception]
            }

            Result.clever -> {
                m = 4.0 + Global.random(user[Attribute.Cunning] + user[Attribute.Professional]) + target[Attribute.Perception]
            }

            else -> {
                m = 4.0 + Global.random(user[Attribute.Seduction] + user[Attribute.Professional]) + target[Attribute.Perception]
                m *= 1.3
            }
        }

        if (user.getPure(Attribute.Professional) >= 3) {
            if (user.has(Trait.sexuallyflexible)) {
                user.add(ProfessionalMomentum(user, user[Attribute.Professional] * 5.0), c)
            } else {
                user.add(ProfMod("Dexterous Momentum", user, Anatomy.fingers, user[Attribute.Professional] * 5.0), c)
            }
        }
        writeOutput(c, target, style)
        if (target.human()) {
            c.offerImage("Handjob.jpg", "Art by AimlessArt")
            c.offerImage("Handjob2.jpg", "Art by AimlessArt")
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.JEWEL) {
                c.offerImage("Jewel Handjob.png", "Art by Fujin Hitokiri")
            }
        }
        m = user.bonusProficiency(Anatomy.fingers, m)
        target.pleasure(m, Anatomy.genitals, Result.finisher, c)
        if (user.has(Trait.roughhandling)) {
            target.weaken(m / 2, c)
            writeOutput(c, target, Result.unique)
        }
    }

    override fun requirements(user: Character): Boolean {
        return user[Attribute.Seduction] >= 8
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Seduction to 8)
    }

    override fun copy(user: Character): Skill {
        return HandjobEX(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.unique -> {
                "You opportunistically give her balls some rough treatment."
            }
            Result.miss -> {
                "You reach for " + target.name + "'s dick but miss."
            }
            Result.powerful -> {
                String.format(
                    "You forcefully grab %s's cock and jerk it with all your power. %s moans loudly as the intense sensation hits %s",
                    target.name, target.pronounSubject(true), target.pronounTarget(false)
                )
            }
            Result.clever -> {
                String.format(
                    "You gently tease %s's dick with your fingertips. Your featherlight strokes probably don't provide much actual "
                            + "stimulation, but you effectively stoke %s arousal by making %s yearn for more.",
                    target.name, target.possessive(false), target.pronounTarget(false)
                )
            }
            else -> {
                String.format(
                    "You skillfully service %s's penis with slow, tender strokes. This is more complicated than your basic jerk off technique, "
                            + "but you take inspiration from some of the more seductive handjobs you've received.",
                    target.name
                )
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.unique -> {
                "She roughly womanhandles your balls, sapping some of your strength."
            }
            Result.miss -> {
                user.name + " grabs for your dick and misses."
            }
            Result.powerful -> {
                String.format(
                    "%s grabs your cock and roughly jerks you off. Her forceful handjob is almost painful, but it's also "
                            + "surprisingly effective.", user.name
                )
            }
            Result.clever -> {
                String.format(
                    "%s plays with your dick, using light teasing touches. Her caress is pretty effective on its own, "
                            + "but the teasing is so frustrating that you involuntary thrust against her hand, arousing yourself further.",
                    user.name
                )
            }
            else -> {
                String.format(
                    "%s takes hold of your manhood with a sensual grip and begins to play you like an instrument. Her talented "
                            + "fingers seem to know all the most sensitive spots on your penis and balls. It's so good, you almost forget to fight back.",
                    user.name
                )
            }
        }
    }

    override fun toString(): String {
        if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            return "Rough Handjob"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            return "Teasing Handjob"
        }
        return name
    }

    override fun describe(): String {
        return if (user[Attribute.Power] > user[Attribute.Cunning] && user[Attribute.Power] > user[Attribute.Seduction]) {
            "Intense handjob using Power: 20 Mojo"
        } else if (user[Attribute.Cunning] > user[Attribute.Seduction]) {
            "Careful handjob using Cunning: 20 Mojo"
        } else {
            "Mojo boosted erotic handjob: 20 Mojo"
        }
    }
}

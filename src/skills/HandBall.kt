package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import status.Masochistic

class HandBall(user: Character) : Skill("Hand Ball", user) {
    init {
        addTag(Attribute.Footballer)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachBottom(user) &&
                (target.isPantsless || (user.has(Trait.dexterous) && target.bottoms.size <= 1)) &&
                target.hasBalls &&
                user.canAct() &&
                c.stance.behind(user)
    }

    override fun describe(): String {
        return "Grab your opponent's bits and teach them the pleasures of masochism."
    }

    override fun resolve(c: Combat, target: Character) {
        val m = Global.random(user[Attribute.Footballer]) + target[Attribute.Perception] - (2 * target.bottoms.size)
        val type = if (target.hasBalls) Result.strong
        else Result.normal
        writeOutput(c, target, type)
        target.tempt(Global.random(4) + user[Attribute.Seduction] / 4.0, combat = c)
        target.pain(m.toDouble(), Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 4.0, c)
        } else {
            target.calm(m / 2.0, c)
        }
        user.buildMojo(10)
        target.emote(Emotion.angry, 15)
        // user.add(Masochistic(user), c)
        if (c.effectRoll(this, user, target, user[Attribute.Footballer])) {
            target.add(Masochistic(target), c)
        }
    }

    override fun copy(user: Character): Skill {
        return HandBall(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.strong) {
            return "You grab " + target.name + "'s balls roughly, taking the fight out of her. While she's being cooperative, you pull her into a passionate kiss. " +
                    "After swapping spit for a while, " + (if (Global.random(2) == 0) "you give her testicles a hard squeeze for good measure." else "You give her sack a couple light slaps.")
        }
        return "You grab " + target.name + "'s vulva and squeeze hard enough that she knows you aren't about to pleasure her. She doesn't resist when you pull her into a passionate kiss. " +
                "After swapping spit for a while, you isolate her sensitive clitoris and " + (if (Global.random(2) == 0) "give it a painful pinch." else "give it a few light smacks.")
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " grabs your ballsack firmly, and you freeze instinctively. <i>\"You know what I could do to these tender things? It would hurt so good!\"</i> " +
                "Before you can reply, she kisses you deeply. " +
                (if (Global.random(2) == 0) "You groan in pain as she tightens her grip on your testicles." else "Without breaking the kiss, she slaps your tender balls three times.")
    }
}

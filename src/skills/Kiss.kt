package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import status.Charmed

class Kiss(user: Character) : Skill("Kiss", user) {
    init {
        addTag(Attribute.Seduction)
        addTag(Result.kiss)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.kiss(user) && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        val result = if (user.has(Trait.greatkiss) && user.canSpend(Pool.MOJO, 10) && Global.random(4) == 3) {
            Result.special
        } else if (user[Attribute.Seduction] >= 9) {
            Result.normal
        } else {
            Result.weak
        }


        writeOutput(c, target, result)
        val exact = Global.checkFlag(Flag.exactimages)
        if (user.human()) {
            if (!exact || target.name.startsWith("Jewel")) {
                c.offerImage("Kiss.jpg", "Art by AimlessArt")
            }
        } else if (target.human()) {
            if (!exact || user.name.startsWith("Jewel")) {
                c.offerImage("Kissed.jpg", "Art by AimlessArt")
            }
        }
        if (!exact || user.id == ID.ANGEL) {
            c.offerImage("Angel Kiss.jpg", "Art by AimlessArt")
        }
        if (!exact || user.id == ID.MARA) {
            c.offerImage("Mara kiss.jpg", "Art by AimlessArt")
        }
        if (/*false &&*/ /*!exact ||*/ user.id == ID.SELENE) {
            c.offerImage("Selene Kiss.png", "Art by AimlessArt")
        }
        if (!exact || target.name.startsWith("Cassie")) {
            c.offerImage("Cassie Kiss.jpg", "Art by AimlessArt")
        }
        if (!exact || target.id == ID.ANGEL) {
            c.offerImage("Angel Kiss.jpg", "Art by AimlessArt")
        }
        if (/*false &&*/ /*!exact ||*/ target.id == ID.SELENE) {
            c.offerImage("Selene Kissed.png", "Art by AimlessArt")
        }


        var m = Global.random(4) + user[Attribute.Seduction] / 4.0
        when (result) {
            Result.special -> {
                m += Global.random(3)
                target.add(Charmed(target, 2 + user.bonusCharmDuration()), c)
                user.spendMojo(10)
            }
            Result.normal -> {
                user.buildMojo(10)
            }
            else -> {
                user.buildMojo(5)
            }
        }
        target.tempt(m, user.bonusTemptation(), Result.foreplay, c)
        if (user[Attribute.Seduction] >= 9) {
            target.pleasure(1.0, Anatomy.mouth, combat = c)
        }
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return Kiss(user)
    }

    override var speed: Int = 6

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                "You pull " + target.name + " to you and kiss her passionately. You run your tongue over her lips until her opens them and immediately invade her mouth. " +
                        "You tangle your tongue around hers and probe the sensitive insides her mouth. As you finally break the kiss, she leans against you, looking kiss-drunk and needy."
            }
            Result.weak -> {
                "You aggressively kiss " + target.name + " on the lips. It catches her off guard for a moment, but she soon responds approvingly."
            }
            else -> {
                when (Global.random(3)) {
                    0 -> "You pull " + target.name + " close and capture her lips. She returns the kiss enthusiastically and lets out a soft noise of approval when you " +
                            "push your tongue into her mouth."

                    1 -> "You press your lips to " + target.name + "'s in a romantic kiss. You tease out her tongue and meet it with your own."
                    else -> "You kiss " + target.name + " deeply, overwhelming her senses and swapping quite a bit of saliva."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " seductively pulls you into a deep kiss. As first you try to match her enthusiastic tongue with your own, but you're quickly overwhelmed. She draws " +
                        "your tongue into her mouth and sucks on it in a way that seems to fill your mind with a pleasant, but intoxicating fog."
            }
            Result.weak -> {
                user.name + " presses her lips against yours in a passionate, if not particularly skillful, kiss."
            }
            else -> {
                when (Global.random(3)) {
                    0 -> user.name + " grabs you and kisses you passionately on the mouth. As you break for air, she gently nibbles on your bottom lip."
                    1 -> user.name + " peppers quick little kisses around your mouth before suddenly taking your lips forcefully and invading your mouth with her tongue."
                    else -> user.name + " kisses you softly and romantically, slowly drawing you into her embrace. As you part, she teasingly brushes her lips against yours."
                }
            }
        }
    }

    override fun describe(): String {
        return "Kiss your opponent"
    }
}

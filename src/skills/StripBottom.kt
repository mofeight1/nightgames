package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import combat.Combat
import combat.Result
import global.Flag
import global.Global

class StripBottom(user: Character) : Skill("Strip Bottoms", user) {
    init {
        addTag(SkillTag.STRIPPING)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.reachBottom(user) && !target.isPantsless && user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        val strip = user[Attribute.Cunning]
        if (!target.stripAttempt(strip, user, c, target.bottoms.last().item)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target)
        if (user.human()) {
            if (!Global.checkFlag(Flag.exactimages) || target.id === ID.YUI) {
                c.offerImage("Strip Bottom Female.jpg", "Art by AimlessArt")
            }
            if (!Global.checkFlag(Flag.exactimages) || target.id === ID.MARA) {
                c.offerImage("Mara StripBottom.jpg", "Art by AimlessArt")
            }
        } else if (target.human()) {
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.MARA) {
                c.offerImage("Strip Bottom Male.jpg", "Art by AimlessArt")
            }
            if (!Global.checkFlag(Flag.exactimages) || user.id === ID.ANGEL) {
                c.offerImage("Angel StripBottom.jpg", "Art by AimlessArt")
            }
        }

        target.strip(1, c)
        if (user.getPure(Attribute.Cunning) >= 30 && !target.isPantsless) {
            if (target.stripAttempt(strip, user, c, target.bottoms.last().item)) {
                writeOutput(c, target, Result.strong)
                target.strip(1, c)
                target.emote(Emotion.nervous, 10)
            }
        }
        if (target.isNude) {
            c.write(target, target.nakedLiner())
        }
        if (target.human() && target.isPantsless) {
            if (target.arousal.current >= 15) {
                c.write("Your boner springs out, no longer restrained by your pants.")
            } else {
                c.write(user.name + " giggles as your flacid dick is exposed")
            }
        }
        target.emote(Emotion.nervous, 10)
    }

    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun copy(user: Character): Skill {
        return StripBottom(user)
    }

    override var speed = 3

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            when (Global.random(2)) {
                1 -> "You try to take off ${target.name}'s ${target.bottoms.last()}" +
                        ", but she slaps away your hands in a flurry of movement."

                else -> "You grab ${target.name}'s ${target.bottoms.last()}, but ${target.pronounSubject(false)} scrambles away before you can strip ${target.pronounTarget(false)}."
            }
        } else if (modifier == Result.strong) {
            "Taking advantage of the situation, you also manage to snag ${target.possessive(false)} ${target.bottoms.last()}"
        } else {
            if (target.canAct()) {
                when (Global.random(2)) {
                    1 -> "${target.name}'s ${target.bottoms.last()} comes off in your hands, despite her efforts to stop you."

                    else -> "After a brief struggle, you manage to pull off ${target.name}'s ${target.bottoms.last()}."
                }
            } else {
                when (Global.random(2)) {
                    1 -> "${target.name} gives you a mean look as you effortlessly pull off her ${target.bottoms.last()}."

                    else -> "You strip off ${target.name}'s ${target.bottoms.last()} without much difficulty."
                }
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            when (Global.random(2)) {
                1 -> "${user.name} grabs your ${target.bottoms.last()}, but you jump back before she can pull them off."

                else -> "${user.name} tries to pull down your ${target.bottoms.last()}, but you hold them up."
            }
        } else if (modifier == Result.strong) {
            "Before you can react, ${user.pronounSubject(false)} also strips off your ${target.bottoms.last()}"
        } else {
            if (target.canAct()) {
                when (Global.random(2)) {
                    1 -> "You try to block ${user.name} from grabbing your ${target.bottoms.last()}" +
                            ", but she manages to steal them away from you."

                    else -> "${user.name} grabs the waistband of your ${target.bottoms.last()} and pulls them down."
                }
            } else {
                "You have no way to stop ${user.name} as she yanks off your ${target.bottoms.last()}."
            }
        }
    }

    override fun describe(): String {
        return "Attempt to remove opponent's pants. More likely to succeed if she's weakened and aroused"
    }
}

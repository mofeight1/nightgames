package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.OrderedStrip
import status.Stsflag

class MastersOrder(user: Character) : Skill("Master's Order", user) {
    init {
        addTag(Attribute.Discipline)
        addTag(SkillTag.DOM)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Discipline) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Discipline to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                user.has(Stsflag.composed) &&
                target.canAct() &&
                !target.isNude &&
                user.canSpend(Pool.MOJO, 20) &&
                !target.has(Stsflag.orderedstrip)
    }

    override fun describe(): String {
        return "Order your opponent to undress for you or be punished: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.spendMojo(20)
        target.add(OrderedStrip(target), c)
    }

    override fun copy(user: Character): Skill {
        return MastersOrder(user)
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You catch " + target.name + "'s gaze with your own and move toward her menacingly. You briefly glance toward her apparel, then meet her gaze once more, mentioning that she seems to be wearing far too much clothing right now. She'll have to deal with soon, or else face the consequences of her disobedience."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return (user.name + " suddenly manages to catch your gaze. She moves toward you, somehow manage to loom menacingly over you as she does so. <i>\"Now now,\"</i> " + user.name + " says. <i>\"Don't you think you're wearing far too much for an event like this. I recommend you remedy this immediately.\"</i> She gives you a grin with just a hint of a threat behind it. You wouldn't want to disobey and make me upset with you, now would you?<p>" +
                "You get a strong feeling that if you don't strip yourself very soon, you're in deep trouble")
    }
}

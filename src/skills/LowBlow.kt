package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result

class LowBlow(user: Character) : Skill("Low Blow", user) {
    init {
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.rank >= 2 &&
                (user.id == ID.CASSIE ||
                        user.id == ID.ANGEL ||
                        user.id == ID.YUI ||
                        user.id == ID.MAYA ||
                        user.id == ID.SAMANTHA ||
                        user.id == ID.EVE ||
                        user.id == ID.JEWEL)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) && user.canSpend(Pool.MOJO, 30)
    }

    override fun describe(): String {
        return "A specialized low attack: 30 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(30)
        val m = user.level.toDouble() + user[Attribute.Power]
        writeOutput(c, target)
        target.pain(m, Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 4, c)
        } else {
            target.calm(m / 2, c)
        }
        target.emote(Emotion.angry, 50)
    }

    override fun copy(user: Character): Skill {
        return LowBlow(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (user.id) {
            ID.CASSIE -> ("Cassie begins casting a spell, bringing her fingers in front of her and pointing them at your groin.<br>"
                    + "<i>\"Trench Run formation! Launch!\"</i>"
                    + "With an arcane flash, two tiny female faeries appear and speed towards you along the ground like torpedoes. "
                    + "When they reach the ground at your feet, the faeries fly straight upward between your legs "
                    + "and deliver powerful uppercuts to each of your testicles.<br>"
                    + "Cassie pumps her fist triumphantly. <i>\"Great shot kids! That was two in a million!\"</i>")

            ID.EVE -> ("Eve puckers her lips and moves her face towards yours.  Expecting a kiss, you are caught off guard when she instead "
                    + "spits directly into your eyes.  Momentarily blinded, you step back and wipe your eyes, but Eve uses the "
                    + "opportunity to wind up for a well-placed kick that connects solidly with both your balls.  "
                    + "<i>\"I sure hope you didn't think this was going to be a fair fight,\"</i> she says, while you try to both "
                    + "restore your vision and also clutch helplessly at your testicles.")

            ID.JEWEL -> ("Jewel steps in close to you and does an exaggerated wind-up for a big right haymaker.  "
                    + "You raise your hands to block the well-telegraphed punch, but she catches you completely off-guard when she quickly "
                    + "lashes her left arm out and, with a lightning-fast flick of her wrist, firmly slaps your balls with the back of her hand. ")

            ID.SAMANTHA -> ("Samantha turns around and seductively backs her perfect ass up into your crotch.  She arches her back and bends forward "
                    + "slightly as she grinds herself firmly against your dick and balls.  You start to get distracted by her marvelous body, "
                    + "when she suddenly kicks her heel up backwards, hitting you squarely between the legs.  She turns back around and "
                    + "whispers in your ear, <i>\"You know what they say; all is fair in love and war.\"</i>")

            ID.YUI -> ("Faster than you can react, Yui drops a tiny bomb, and you lose sight of her in a cloud of smoke.  You glance around looking for her, "
                    + "but you appear to be alone.  You suddenly feel a hand swing upward between your legs with an open-palm strike, "
                    + "and you hear the clear, slapping sound of your ballsack being forcefully bounced against your pelvis.  "
                    + "<i>\"That is a very old technique,\"</i> Yui says, silently crouching behind you, \"<i>It is called 'Ringing the Temple Bell.'\"</i>")

            ID.MAYA -> ("Maya gracefully drops to the ground in a perfect split, with one of her legs in front of her and one behind her.  "
                    + "Her new position leaves her at the perfect height to punch you in the crotch, and she plants her knuckles firmly "
                    + "into your nutsack.  You clutch at your tenderized balls as she gracefully gets up from her split and says: "
                    + "<i>\"Not protecting your testicles in an amateur mistake.  I would expect that from a freshman.\"</i>")

            else -> String.format(
                "%s brings you close to her, grabs your hands and places them forcefully on her breasts.  "
                        + "She gazes hypnotically into your eyes, and she softly moans as you feel her nipples slowly stiffen against your palms.  "
                        + "You are losing yourself in her gaze, and you lower your guard, as if in a trance.  %s takes advantage of your "
                        + "distracted state to grab you by the shoulders and raise her knee into your groin, crushing your unprotected testicles.",
                user.name,
                user.name
            )
        }
    }
}

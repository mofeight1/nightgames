package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result

class Purge(user: Character) : Skill("Purge", user) {
    init {
        addTag(Attribute.Spirituality)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Spirituality) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Spirituality to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canSpend(Pool.FOCUS, 2) &&
                user.canActNormally(c) &&
                target.status.isNotEmpty()
    }

    override fun describe(): String {
        return "Convert your opponent's abnormal statuses into temptation."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.FOCUS, 2)

        val statusCopy = target.status.toList()
        val count = statusCopy.size
        target.tempt((20 * count).toDouble(), combat = c)
        writeOutput(c, target)
        for (s in statusCopy) {
            target.removeStatus(s, c)
        }
    }

    override fun copy(user: Character): Skill {
        return Purge(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You use a spiritual cleansing ritual to purge all the abnormalities from " + target.name + ". " +
                "As a pleasant side-effect, it should be highly arousing."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " waves her staff at you. You feel... clean... like everything has been forcefully purged from your spirit. " +
                "For whatever reason, you also feel weirdly turned on."
    }
}

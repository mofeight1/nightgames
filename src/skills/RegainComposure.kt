package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Composed
import status.Stsflag

class RegainComposure(user: Character) : Skill("Regain Composure", user) {
    init {
        addTag(Attribute.Discipline)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Discipline) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Discipline to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.has(Stsflag.broken) &&
                !target.canAct() &&
                user.canSpend(Pool.MOJO, 40)
    }

    override fun describe(): String {
        return "Take a moment to restore your composed state: 40 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(40)
        writeOutput(c, target)
        user.removeStatus(Stsflag.broken, c)
        user.add(Composed(user, 5 + (user[Attribute.Discipline] / 2) / 2, user[Attribute.Discipline].toDouble()), c)
    }

    override fun copy(user: Character): Skill {
        return RegainComposure(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "While it's tempting to take advantage of " + target.name + " in her moment of vulnerability, you decide that your time right now would be better spend trying to get ahold of yourself. You take a few deep breaths, trying to calm yourself from the heat of battle and focus your thoughts once more. With your emotions under control again, you turn your mind back to the match."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "As you struggle to break free, " + user.name + " seems to stop paying attention to you for a few seconds. She closes her eyes and breathes deeply, and her body noticeably relaxes. When her eyes reopen, she's regained her prior focus."
    }
}

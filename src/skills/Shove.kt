package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Mount
import stance.Neutral
import stance.ReverseMount
import stance.StandingOver
import status.Braced
import status.Stsflag

class Shove(user: Character) : Skill("Shove", user) {
    init {
        addTag(Attribute.Power)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.dom(user) &&
                !c.stance.prone(target) &&
                c.stance.reachTop(user) &&
                user.canAct() &&
                !c.stance.penetration(user)
    }

    override fun resolve(c: Combat, target: Character) {
        val m = Global.random(4) + user[Attribute.Power] / 3.0
        var r = Result.normal
        if (c.stance is Mount || c.stance is ReverseMount) {
            if (user.check(Attribute.Power, target.knockdownDC() + 5)) {
                r = Result.powerful
                if (!user.has(Stsflag.braced)) {
                    user.add(Braced(user))
                }
                c.stance = Neutral(user, target)
            } else {
                r = Result.weak
            }
        } else {
            if (user.check(Attribute.Power, target.knockdownDC())) {
                r = Result.strong
                c.stance = StandingOver(user, target)
            }
        }
        writeOutput(c, target, r)
        if (user.getPure(Attribute.Ki) >= 1 && !target.isTopless) {
            target.shred(Character.OUTFITTOP)
        }
        user.buildMojo(5)
        target.emote(Emotion.angry, 15)
        target.pain(m, Anatomy.chest, c)
    }

    override fun requirements(user: Character): Boolean {
        return !user.has(Trait.cursed)
    }

    override fun copy(user: Character): Skill {
        return Shove(user)
    }

    override var speed = 7

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun toString(): String {
        return if (user[Attribute.Ki] >= 1) {
            "Shredding Palm"
        } else {
            name
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.powerful -> "You shove ${target.name} off of you and get to your feet before she can retaliate."
            Result.weak -> "You push ${target.name}, but you're unable to dislodge her."
            Result.special -> "You channel your ki into your hands and strike ${target.name} in the chest, destroying her ${target.tops.last()}"
            Result.strong -> "You shove ${target.name} hard enough to knock her flat on her back."
            Result.miss -> "You shove ${target.name} back a step, but she keeps her footing."
            else -> "You shove ${target.name} back a step, but she keeps her footing."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.powerful -> "${user.name} shoves you hard enough to free herself and jump up."
            Result.weak -> "${user.name} shoves you weakly."
            Result.special -> "${user.name} strikes you in the chest with her palm, staggering you a step. Suddenly your ${target.tops.last()} tears and falls off you in pieces"
            Result.strong -> "${user.name} knocks you off balance and you fall at her feet."
            Result.miss -> "${user.name} pushes you back, but you're able to maintain your balance."
            else -> "${user.name} pushes you back, but you're able to maintain your balance."
        }
    }

    override fun describe(): String {
        return "Slightly damage opponent and try to knock her down"
    }
}

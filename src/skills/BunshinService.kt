package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import kotlin.math.min

class BunshinService(user: Character) : Skill("Bunshin Service", user) {
    init {
        addTag(Attribute.Ninjutsu)
        addTag(Result.touch)
        addTag(SkillTag.CLONE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !c.stance.behind(target) &&
                !c.stance.penetration(target) &&
                user.canSpend(Pool.MOJO, 4) &&
                target.isNude
    }

    override fun describe(): String {
        return "Pleasure your opponent with shadow clones: 4 mojo per attack (min 2))"
    }

    override fun resolve(c: Combat, target: Character) {
        val clones = min(
            min(user.mojo.current / 4, user[Attribute.Ninjutsu] / 3), 5
        )
        var m: Double
        user.spendMojo(clones * 4)
        var r: Result
        if (user.human()) {
            c.write(String.format("You form %d shadow clones and rush forward.", clones))
        } else if (target.human()) {
            c.write(
                String.format(
                    "%s moves in a blur and suddenly you see %d of %s approaching you.",
                    user.name,
                    clones,
                    user.pronounTarget(false)
                )
            )
            if (!Global.checkFlag(Flag.exactimages) || user.id == ID.YUI) {
                c.offerImage("Yui bunshin service.jpg", "Art by AimlessArt")
            }
        }
        for (i in 0 until clones) {
            if (c.attackRoll(this, user, target)) {
                when (Global.random(4)) {
                    0 -> {
                        r = Result.weak
                        writeOutput(c, target, r)
                        target.tempt(
                            Global.random(3) + user[Attribute.Seduction] / 4.0,
                            user.bonusTemptation(),
                            Result.foreplay,
                            c
                        )
                    }

                    1 -> {
                        r = Result.normal
                        writeOutput(c, target, r)
                        target.pleasure(
                            Global.random(3 + user[Attribute.Seduction] / 2) + target[Attribute.Perception] / 2.0,
                            Anatomy.chest,
                            combat = c
                        )
                    }

                    2 -> {
                        r = Result.strong
                        writeOutput(c, target, r)
                        m = Global.random(4 + user[Attribute.Seduction]) + target[Attribute.Perception] / 2.0
                        m = user.bonusProficiency(Anatomy.fingers, m)
                        target.pleasure(m, Anatomy.genitals, combat = c)
                    }

                    else -> {
                        r = Result.critical
                        writeOutput(c, target, r)
                        m = Global.random(6) + user[Attribute.Seduction] / 2.0 + target[Attribute.Perception]
                        m = user.bonusProficiency(Anatomy.mouth, m)
                        target.pleasure(m, Anatomy.genitals, combat = c)
                    }
                }
            } else {
                writeOutput(c, target, Result.miss)
            }
        }
    }

    override fun copy(user: Character): Skill {
        return BunshinService(user)
    }

    override var speed =  4

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                String.format("%s dodges your clone's groping hands.", target.name)
            }
            Result.weak -> {
                String.format(
                    "Your clone darts close to %s and kisses %s on the lips.",
                    target.name,
                    target.pronounTarget(false)
                )
            }
            Result.strong -> {
                if (target.hasDick) {
                    String.format("Your shadow clone grabs %s's dick and strokes it.", target.name)
                } else {
                    String.format("Your shadow clone fingers and caresses %s's pussy lips.", target.name)
                }
            }
            Result.critical -> {
                if (target.hasDick) {
                    String.format(
                        "Your clone attacks %s's sensitive penis, rubbing and stroking %s glans.",
                        target.name,
                        target.possessive(false)
                    )
                } else {
                    String.format(
                        "Your clone slips between %s's legs to lick and suck %s swollen clit.",
                        target.name,
                        target.possessive(false)
                    )
                }
            }
            else -> {
                String.format("A clone pinches and teases %s's nipples.", target.name)
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                String.format("You manage to avoid one of the shadow clones.")
            }
            Result.weak -> {
                String.format("One of the %ss grabs you and kisses you enthusiastically.", user.name)
            }
            Result.strong -> {
                if (target.hasBalls) {
                    String.format("A clone gently grasps and massages your sensitive balls.")
                } else {
                    String.format("A clone teases and tickles your inner thighs and labia.")
                }
            }
            Result.critical -> {
                if (target.hasDick) {
                    String.format(
                        "One of the %s clones kneels between your legs to lick and suck your cock.",
                        user.name
                    )
                } else {
                    String.format("One of the %s clones kneels between your legs to lick your nether lips.", user.name)
                }
            }
            else -> {
                if (user.hasBreasts) {
                    String.format("A %s clone presses her boobs against you and teases your nipples.", user.name)
                } else {
                    String.format("A %s clone caresses your chest and teases your nipples.", user.name)
                }
            }
        }
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.Mount

class Tackle(user: Character) : Skill("Tackle", user) {
    init {
        addTag(Attribute.Power)
        addTag(SkillTag.KNOCKDOWN)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                c.stance.mobile(target) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                !user.has(Trait.petite)
    }

    override fun resolve(c: Combat, target: Character) {
        if (!c.attackRoll(this, user, target) ||
            !user.check(Attribute.Power, target.knockdownDC() - user[Attribute.Animism])) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (user[Attribute.Animism] >= 1) {
            writeOutput(c, target, Result.special)
            target.pain(4.0 + Global.random(6), Anatomy.chest, c)
            c.stance = Mount(user, target)
        } else if (target.has(Trait.reflexes)) {
            writeOutput(c, target, Result.unique)
            user.pain(target.level.toDouble(), Anatomy.chest)
            user.emote(Emotion.nervous, 10)
            target.emote(Emotion.dominant, 15)
        } else {
            writeOutput(c, target, Result.normal)
            target.pain(3.0 + Global.random(4), Anatomy.chest, c)
            c.stance = Mount(user, target)
        }
    }

    override fun requirements(user: Character): Boolean {
        return (user.getPure(Attribute.Power) >= 26 || user.getPure(Attribute.Animism) >= 1) && !user.has(Trait.petite)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Power to 26, Attribute.Animism to 1)
    }

    override fun copy(user: Character): Skill {
        return Tackle(user)
    }

    override var speed :Int = if (user[Attribute.Animism] >= 1) {
        3
    } else {
        1
    }

    override var accuracy = if (user[Attribute.Animism] >= 1) {
        3
    } else {
        1
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun toString(): String {
        return if (user[Attribute.Animism] >= 1) {
            "Pounce"
        } else {
            name
        }
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.unique -> {
                "You lunge at " + target.name + ", but she rolls backwards, using your momentum to throw you behind her. "+
                "You land hard on the floor, taking the wind out of you."
            }
            Result.special -> {
                "You let your instincts take over and you pounce on " + target.name + " like a predator catching your prey."
            }
            Result.normal -> {
                "You tackle " + target.name + " to the ground and straddle her."
            }
            else -> {
                "You lunge at " + target.name + ", but she dodges out of the way."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.special -> {
                user.name + " wiggles her butt cutely before leaping at you and pinning you to the floor."
            }
            Result.miss -> {
                user.name + " tries to tackle you, but you sidestep out of the way."
            }
            else -> {
                user.name + " bowls you over and sits triumphantly on your chest."
            }
        }
    }

    override fun describe(): String {
        return "Knock opponent to ground and get on top of her"
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result

class KittyKick(user: Character) : Skill("Kitty Kick", user) {
    init {
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.id === ID.KAT && user.rank >= 2
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canSpend(Pool.MOJO, 30) &&
                c.stance.reachTop(user) &&
                user.canAct() &&
                c.stance.sub(user)
    }

    override fun describe(): String {
        return "A specialized low attack from a disadvantageous position: 30 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(30)
        val m = user.level + user[Attribute.Power].toDouble()
        writeOutput(c, target)
        target.pain(m, Anatomy.genitals, c)
        if (user.has(Trait.wrassler)) {
            target.calm(m / 4, c)
        } else {
            target.calm(m / 2, c)
        }
        target.emote(Emotion.angry, 50)
    }

    override fun copy(user: Character): Skill {
        return KittyKick(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "Kat firmly grabs both of your wrists and then rolls backwards, pulling you over her.  She begins rapidly bicycle-kicking her feet out, aiming at your lower stomach and crotch.  The kicks don't hurt very much individually, but they are so fast that she manages to get several quick hits in before you can free your hands and block the attacks."
    }
}

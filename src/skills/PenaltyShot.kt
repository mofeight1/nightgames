package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import stance.Mount
import stance.Stance

class PenaltyShot(user: Character) : Skill("Penalty Shot", user) {
    init {
        addTag(Attribute.Footballer)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return (c.stance.en == Stance.mount || c.stance.en == Stance.pin)
                && c.stance.sub(user)
                && user.canAct()
                && target.canAct()
                && !c.stance.penetration(user)
                && !c.stance.penetration(target)
                && user.canSpend(Pool.MOJO,10)
    }

    override fun describe(): String {
        return "Kiss your opponent to distract them, then surprise them with a knee strike: 10 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        target.tempt(Global.random(4) + user[Attribute.Seduction] / 4.0, combat = c)
        if (!c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        writeOutput(c, target)
        target.pain(
            4.0 + Global.random(11) + user[Attribute.Footballer], Anatomy.genitals, c
        )
        if (c.stance.en == Stance.pin) {
            c.stance = Mount(target, user)
        }
        if (c.stance.en == Stance.pin) {
            c.stance = Mount(target, user)
        }
    }

    override var speed = 2

    override var accuracy =7

    override fun copy(user: Character): Skill {
        return PenaltyShot(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            return "You pull " + target.name + " down into a passionate kiss.  Your tongue tangles with hers, but when you try to move your legs, she's not as " +
                    "distracted as you hoped. She braces her body and holds you in place."
        }
        return "You pull " + target.name + " down into a passionate kiss.  Your skilled tongue completely captures her attention, and she seems too distracted to " +
                "notice that you are slowly parting her thighs with your knees.  You catch her by surprise and drive your knee upwards into her groin."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        if (modifier == Result.miss) {
            return "With an alluring twinkle in her eye, " + user.name + " grabs your shoulders and pulls you down into a passionate kiss.  " +
                    "Your mind almost goes blank, but you still feel her try to push your legs open. You quickly clamp your thighs together to stop her."
        }
        return "With an alluring twinkle in her eye, " + user.name + " grabs your shoulders and pulls you down into a passionate kiss.  " +
                "Your dick is rock hard and throbbing, and you see this as an excellent opportunity to reposition your legs to penetrate her.  " +
                "The moment you open your thighs, however, she pumps her knee straight upwards between your legs, smashing your balls as well as your erection."
    }
}

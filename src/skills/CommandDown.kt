package skills

import characters.Character
import combat.Combat
import combat.Result
import stance.Mount
import stance.Stance

class CommandDown(user: Character) : PlayerCommand("Force Down", user) {
    override fun usable(c: Combat, target: Character): Boolean {
        return super.usable(c, target) && c.stance.en == Stance.neutral
    }

    init {
        addTag(SkillTag.COMMAND)
    }

    override fun describe(): String {
        return "Command your opponent to lay down on the ground."
    }

    override fun resolve(c: Combat, target: Character) {
        c.stance = Mount(user, target)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return CommandDown(user)
    }


    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Trembling under the weight of your command, " + target.name +
                " lies down. You follow her down and mount her, facing her head."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ("<<This should not be displayed, please inform The"
                + " Silver Bard: CommandDown-receive>>")
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import stance.Neutral
import status.Stsflag

class Burst(user: Character) : Skill("Burst", user) {
    init {
        addTag(Attribute.Ki)
        addTag(SkillTag.ESCAPE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 24
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 24)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return ((!c.stance.mobile(user) && c.stance.sub(user)) || user.bound()) &&
                !user.stunned() &&
                !user.distracted() &&
                !user.has(Stsflag.enthralled) &&
                user.canSpend(Pool.STAMINA, 30)
    }

    override fun describe(): String {
        return "Release a burst of Ki to break out of a hold: 30 Stamina"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.STAMINA, 30)
        writeOutput(c, target)

        c.stance = Neutral(user, target)
        target.emote(Emotion.nervous, 10)
        user.free()
    }

    override fun copy(user: Character): Skill {
        return Burst(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You release an intense burst of Ki, exhausting yourself, but knocking " + target.name + " away."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " grunts with exertion, and you are suddenly knocked away by a burst of energy."
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global

class Focus(user: Character) : Skill("Focus", user) {
    init {
        addTag(Attribute.Cunning)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && !c.stance.sub(user)
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.calm(Global.random(8).toDouble())
        user.buildMojo(20)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 15 && !user.has(Trait.undisciplined)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 15)
    }

    override fun copy(user: Character): Skill {
        return Focus(user)
    }

    override var speed = 0

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You take a moment to clear your thoughts, focusing your mind and calming your body."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " closes her eyes and takes a deep breath. When she opens her eyes, she seems more composed."
    }

    override fun describe(): String {
        return "Calm yourself and gain some mojo"
    }
}

package skills

import characters.Character
import combat.Combat
import combat.Result
import items.Consumable
import status.PrismaticStance

class UsePowerband(self: Character) : UseItem(Consumable.powerband, self) {
    init {
        addTag(SkillTag.CONSUMABLE)
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.consume(Consumable.powerband, 1)
        user.add(PrismaticStance(user), c)
    }

    override fun copy(user: Character): Skill {
        return UsePowerband(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You put on the Power Band and feel tremendous power flow through you."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return (user.name + " puts on a strange headband. The atmosphere immediately changes. You can practically feel the energy flowing out "
                + "of her.")
    }
}

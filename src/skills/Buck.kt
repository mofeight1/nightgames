package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result

class Buck(user: Character) : Skill("Buck", user) {
    init {
        addTag(Attribute.Submissive)
        addTag(Result.intercourse)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 18)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.penetration(user) && c.stance.sub(user)
    }

    override fun resolve(c: Combat, target: Character) {
        val inside: Anatomy
        if (c.stance.anal()) {
            writeOutput(c, target, Result.anal)
            inside = Anatomy.ass
        } else {
            writeOutput(c, target)
            inside = Anatomy.genitals
        }
        val m = user.getSexPleasure(2, Attribute.Submissive)
        target.pleasure(user.bonusProficiency(inside, m), inside, Result.finisher, c)
        var r = target.getSexPleasure(1, Attribute.Seduction, inside) / 3.0
        r += target.bonusRecoilPleasure(r)
        if (user.has(Trait.experienced)) {
            r /= 2
        }
        user.pleasure(r, inside, combat = c)

        c.stance.pace = 2
    }

    override fun copy(user: Character): Skill {
        return Buck(user)
    }

    override fun type(): Tactics {
        return Tactics.fucking
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            "You do your best to thrust back against " + target.name + ", as " + target.pronounSubject(false) + " pegs you."
        } else {
            "You buck your hips, thrusting into " + target.name + " from below. She moans in pleasure as she tries to retain control."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.anal) {
            user.name + " presses her ass against your hips, hitting you with unexpected pleasure despite the awkward position."
        } else {
            (user.name + " pushes her hips against you, matching you thrust for thrust. The sudden change of pace catches you off-guard "
                    + "and hits you with a jolt of pleasure.")
        }
    }

    override fun describe(): String {
        return "Buck against your opponent from the bottom, giving some pleasure"
    }
}


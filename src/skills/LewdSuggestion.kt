package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import status.Horny
import status.Stsflag

class LewdSuggestion(self: Character) : Skill("Lewd Suggestion", self) {
    init {
        addTag(Attribute.Hypnosis)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Hypnosis) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Hypnosis to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                !c.stance.sub(user) &&
                target.has(Stsflag.charmed)
    }

    override fun describe(): String {
        return "Plant an erotic suggestion in your hypnotized target."
    }

    override fun resolve(c: Combat, target: Character) {
        var x = 1.0
        val sts = target.getStatus(Stsflag.horny)
        if (sts != null) {
            writeOutput(c, target, Result.strong)
            x += sts.magnitude
        } else {
            writeOutput(c, target, Result.normal)
        }
        target.add(Horny(target, x, 4), c)
        target.emote(Emotion.horny, 30)
    }

    override fun copy(user: Character): Skill {
        return LewdSuggestion(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            String.format(
                "You take advantage of the erotic fantasies already swirling through %s's head, whispering ideas that fan the flame of %s lust.",
                target.name, target.possessive(false)
            )
        } else {
            String.format(
                "You plant an erotic suggestion in %s's mind, distracting %s with lewd fantasies.",
                target.name, target.pronounTarget(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            String.format(
                "%s whispers a lewd suggestion to you, intensifying the fantasies you were trying to ignore and enflaming your arousal.",
                user.name
            )
        } else {
            String.format(
                "%s gives you a hypnotic suggestion and your head is immediately filled with erotic possibilities.",
                user.name
            )
        }
    }
}

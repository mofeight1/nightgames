package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Global
import stance.Mount
import stance.ReverseMount
import stance.Stance

class Stumble(user: Character) : Skill("Stumble", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Submissive) >= 9
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Submissive to 9)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.en == Stance.neutral
    }

    override fun describe(): String {
        return "An accidental pervert classic"
    }

    override fun resolve(c: Combat, target: Character) {
        if (Global.random(2) == 0) {
            c.stance = Mount(target, user)
        } else {
            c.stance = ReverseMount(target, user)
        }
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return Stumble(user)
    }

    override fun type(): Tactics {
        return Tactics.negative
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You slip and fall to the ground, pulling " + target.name + " awkwardly on top of you."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " stumbles and falls, grabbing you to catch herself. Unfortunately, you can't keep your balance and you fall on top of her. Maybe that's not so unfortunate."
    }
}

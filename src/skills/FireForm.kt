package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.FireStance
import status.Stsflag

class FireForm(user: Character) : Skill("Fire Form", user) {
    init {
        addTag(Attribute.Ki)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                !c.stance.sub(user) &&
                !user.has(Stsflag.form)
    }

    override fun describe(): String {
        return "Boost Mojo gain at the expense of Stamina regeneration."
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(FireStance(user), c)
    }

    override fun copy(user: Character): Skill {
        return FireForm(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You let your ki burn, wearing down your body, but enhancing your spirit."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " powers up and you can almost feel the energy radiating from her."
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Bound

class MatterConverter(user: Character) : Skill("Matter Converter", user) {
    init {
        addTag(Attribute.Science)
        addTag(SkillTag.ULTIMATE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Science) >= 30
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Science to 30)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.reachTop(user) &&
                !c.stance.reachTop(target) &&
                c.stance.dom(user) &&
                !user.isNude &&
                user.canSpend(Pool.BATTERY, 10)
    }

    override fun describe(): String {
        return "Converts all your remaining clothing to a powerful binding material: 10 Battery"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.BATTERY, 10)
        val m = user.tops.size + user.bottoms.size
        user.nudify()

        writeOutput(c, target)
        target.add(Bound(target, 20 + m * 10, "Super-web"), c)
    }

    override fun copy(user: Character): Skill {
        return MatterConverter(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You vacuum up your own clothing into your multitool, where it is converted into Super-web material. "
                    + "Grabbing %s's wrists, you cover them in the webbing, binding them in place.",
            target.name
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s's clothes quickly vanish off %s body as they're sucked into %s wrist device. "
                    + "Said device immediately spits a clump of sticky string that binds your wrists tightly in place. "
                    + "You pull against the tangled binding, but you can't get loose.",
            user.name, user.possessive(false), user.possessive(false)
        )
    }
}

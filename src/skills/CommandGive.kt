package skills

import characters.Character
import combat.Combat
import combat.Result
import global.Global
import items.Component
import items.Consumable
import items.Flask
import items.Item
import items.Potion

class CommandGive(user: Character) : PlayerCommand("Take Item", user) {
    private lateinit var transfer: Item

    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        if (!super.usable(c, target)) return false
        for (transferable in TRANSFERABLES) if (target.has(transferable)) return true
        return false
    }

    override fun describe(): String {
        return "Make your opponent give you an item."
    }

    override fun resolve(c: Combat, target: Character) {
        val transferable = TRANSFERABLES.filter { target.has(it) }
        transfer = transferable.random()
        target.consume(transfer, 1)
        user.gain(transfer)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return CommandGive(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return target.name + " takes out " + transfer.prefix + transfer + " and hands it to you."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "<<This should not be displayed, please inform The" +
                " Silver Bard: CommandGive-receive>>"
    }

    companion object {
        val TRANSFERABLES: List<Item> = listOf(
            Potion.EnergyDrink,
            Flask.SPotion,
            Flask.Aphrodisiac,
            Flask.Sedative,
            Potion.Beer,
            Flask.Lubricant,
            Component.Rope,
            Consumable.ZipTie,
            Component.Tripwire,
            Component.Spring,
            Consumable.smoke,
            Consumable.Handcuffs,
            Consumable.Talisman,
            Consumable.FaeScroll,
            Component.Phone,
            Potion.SuperEnergyDrink,
            Potion.Bull,
            Potion.Cat,
            Potion.Fox,
            Potion.Furlixir,
            Potion.Nymph
        )
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result

class NakedBloom(user: Character) : Skill("Naked Bloom", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.STRIPPING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                !target.isNude &&
                user.canSpend(Pool.MOJO, 20)
    }

    override fun describe(): String {
        return "Cast a spell to transform your opponents clothes into flower petals: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        writeOutput(c, target)
        target.nudify()
        if (target.isNude) target.nakedLiner()
        target.emote(Emotion.nervous, 10)
    }

    override fun copy(user: Character): Skill {
        return NakedBloom(user)
    }

    override fun type(): Tactics {
        return Tactics.stripping
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You chant a short spell and turn " + target.name + "'s clothes into a burst of flowers. The cloud of flower petals flutters to the ground, exposing her nude body."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " mumbles a spell and you're suddenly surrounded by an eruption of flower petals. As the petals settle, you realize you've been stripped completely " +
                "naked."
    }
}

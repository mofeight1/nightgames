package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import items.Clothing

class MageArmor(user: Character) : Skill("Mage Armor", user) {
    init {
        addTag(Attribute.Arcane)
        addTag(SkillTag.DRESSING)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Arcane) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Arcane to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.sub(user) &&
                user.isPantsless &&
                user.canSpend(Pool.MOJO, 20) &&
                !c.stance.penetration(user) &&
                !c.stance.penetration(target)
    }

    override fun describe(): String {
        return "Craft some replacement underwear out of pure magic: 20 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(20)
        if (user.canWear(Clothing.magethong)) {
            user.wear(Clothing.magethong)
        }
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return MageArmor(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(2)) {
            1 -> "You conjure a meager thong to cover your genitals. It's not much, but it will have to do for now."
            else -> "You channel your magic and evoke a glowing magic thong. It doesn't cover much, but it provides some basic protection."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(2)) {
            1 -> user.name + " channels her magic for a moment to magically make a magic thong appear on her crotch. Magically."
            else -> user.name + " goes into what appears to be a brief Magical Girl transformation sequence and a thong magically appears on her."
        }
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.Pool
import combat.Combat
import combat.Result
import stance.Behind
import status.Stsflag

class Substitute(user: Character) : Skill("Substitution", user) {
    init {
        addTag(Attribute.Ninjutsu)
        addTag(SkillTag.ESCAPE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ninjutsu) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ninjutsu to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.mobile(user) &&
                c.stance.sub(user) &&
                !user.bound() &&
                !user.stunned() &&
                !user.distracted() &&
                user.canSpend(Pool.MOJO, 10) &&
                !user.has(Stsflag.enthralled)
    }

    override fun describe(): String {
        return "Use a decoy to slip behind your opponent: 10 Mojo."
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        writeOutput(c, target)

        c.stance = Behind(user, target)
        target.emote(Emotion.nervous, 10)
        user.emote(Emotion.dominant, 10)
    }

    override fun copy(user: Character): Skill {
        return Substitute(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "By the time %s realizes %s's pinning a dummy, you're already behind %s.",
            target.name, target.pronounSubject(false), target.pronounTarget(false)
        )
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "You straddle %s's unresisting body. Wait... This is a blow-up doll. "
                    + "The real %s is standing behind you. When-How- did %s make the switch?",
            user.name, user.name, user.pronounSubject(false)
        )
    }
}

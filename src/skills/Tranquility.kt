package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Stsflag
import status.Tranquil

class Tranquility(user: Character) : Skill("Tranquility", user) {
    init {
        addTag(Attribute.Spirituality)
        addTag(SkillTag.DEFENSIVE)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Spirituality) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Spirituality to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                !user.has(Stsflag.horny) &&
                !user.has(Stsflag.mindaffecting) &&
                !user.has(Stsflag.tranquil) &&
                user.canSpend(Pool.FOCUS, 3)
    }

    override fun describe(): String {
        return "Resist pleasure with spiritual tranquility: 3 Focus"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.FOCUS, 3)
        writeOutput(c, target)
        user.add(Tranquil(user), c)
    }

    override fun copy(user: Character): Skill {
        return Tranquility(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You let a wave of tranquility wash over you, dulling your senses."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " takes a deep breath. You can practically see an oasis of tranquility around " +
                user.pronounTarget(false) + "."
    }
}

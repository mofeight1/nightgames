package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Stsflag

class TentacleCaress(user: Character) : Skill("Tentacle Caress", user) {
    init {
        addTag(Attribute.Eldritch)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Eldritch) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Eldritch to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !user.stunned() &&
                !user.distracted() &&
                !user.has(Stsflag.enthralled) &&
                target.isPantsless
    }

    override fun describe(): String {
        return ""
    }

    override fun resolve(c: Combat, target: Character) {
    }

    override fun copy(user: Character): Skill {
        return TentacleCaress(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ""
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.ID
import characters.Pool
import combat.Combat
import combat.Result
import status.Horny

class LustRelease(user: Character) : Skill("Lust Release", user) {
    init {
        addTag(Attribute.Unknowable)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Unknowable) >= 21
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Unknowable to 21)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canActNormally(c) &&
                user.canSpend(Pool.ENIGMA, 2) &&
                user.arousal.percent() >= 50
    }

    override fun describe(): String {
        return "Transfer your arousal to your opponent: 2 Enigma"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.ENIGMA, 2)
        if (target[Attribute.Dark] >= 10) {
            writeOutput(c, target, Result.miss)
            return
        } else if (c.isWatching(ID.REYKA)) {
            writeOutput(c, target, Result.defended, 0)
            return
        } else if (c.isWatching(ID.ANGEL)) {
            writeOutput(c, target, Result.defended, 1)
            return
        }
        val starting = user.arousal.current
        val lust = user[Attribute.Unknowable] * 3.0
        user.calm(lust)
        val amountLost = starting - user.arousal.current
        target.add(Horny(target, amountLost / 4.0, 4))
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return LustRelease(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            ""
        } else if (modifier == Result.defended) {
            if (damage == 1) {
                ""
            } else {
                ""
            }
        } else {
            ""
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " takes a deep breath and you feel a haze of energy radiate out from her. An aura of pure arousal " +
                    "leaks out, so dense you can see it. Fortunately, you have enough Dark knowledge to manipulate such tangible lust. " +
                    "You force the aura back into " + user.name + " making " + user.pronounSubject(false) + " shiver " +
                    "with need."
        } else if (modifier == Result.defended) {
            if (damage == 1) {
                "[Placeholder: Angel Assists]"
            } else {
                "[Placeholder: Reyka Assists]"
            }
        } else {
            user.name + " takes a deep breath and you feel a haze of energy radiate out from her. An aura of pure arousal " +
                    "leaks out, so dense you can see it. As it hits you, you find yourself becoming supernaturally horny, while " +
                    user.name + " looks much calmer."
        }
    }
}

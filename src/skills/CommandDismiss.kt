package skills

import characters.Character
import combat.Combat
import combat.Result

class CommandDismiss(user: Character) : PlayerCommand("Force Dismiss", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return super.usable(c, target) && target.pet != null
    }

    override fun describe(): String {
        return "Have your thrall dismiss their pet."
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        target.pet!!.remove()
    }

    override fun copy(user: Character): Skill {
        return CommandDismiss(user)
    }


    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return ("You think you briefly see a pang of regret in " + target.name
                + "'s eyes, but she quickly dismisses her "
                + target.pet.toString().lowercase() + ".")
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ("<<This should not be displayed, please inform The"
                + " Silver Bard: CommandDismiss-receive>>")
    }
}

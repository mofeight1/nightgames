package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import stance.Neutral
import status.Stsflag

class SecondWind(user: Character) : Skill("2nd Wind", user) {
    init {
        addTag(Attribute.Contender)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Contender) >= 3
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Contender to 3)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.stunned() && user.canSpend(Pool.MOJO, 10)
    }

    override fun describe(): String {
        return "Quickly recover from being Winded"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        user.removeStatus(Stsflag.stunned)
        writeOutput(c, target)
        user.heal(user.stamina.max * .3, c)
        if (c.stance.mobile(user)) {
            c.stance = Neutral(user, target)
        }
    }

    override fun copy(user: Character): Skill {
        return SecondWind(user)
    }

    override fun type(): Tactics {
        return Tactics.recovery
    }

    override var speed = 0

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Your body screams at you to stay down and catch your breath, but you shake off your exhaustion and continue fighting."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "You thought " + user.name + " was too winded to keep fighting, but she recovers almost immediately."
    }
}

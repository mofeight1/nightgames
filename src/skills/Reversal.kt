package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import characters.ID
import characters.Pool
import characters.Trait
import combat.Combat
import combat.Result
import global.Flag
import global.Global
import stance.Cowgirl
import stance.Doggy
import stance.Flying
import stance.Missionary
import stance.Neutral
import stance.Pin
import stance.ReverseCowgirl
import stance.Stance

class Reversal(user: Character) : Skill("Reversal", user) {
    init {
        addTag(Attribute.Cunning)
        addTag(SkillTag.ESCAPE)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.mobile(user) &&
                c.stance.sub(user) &&
                user.canSpend(Pool.MOJO, 15) &&
                user.canAct()
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(15)
        if (!c.effectRoll(this, user, target, user[Attribute.Cunning] / 4 - (c.stance.escapeDC(target, user)))) {
            writeOutput(c, target, Result.miss)
            c.stance.struggle(c)
            return
        }
        writeOutput(c, target)
        if (c.stance.penetration(user)) {
            if (user.hasDick || user.has(Trait.strapped)) {
                if (c.stance.behind(user)) {
                    if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Kat")) {
                        c.offerImage("Doggy Style.jpg", "Art by AimlessArt")
                    }
                    if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Mara")) {
                        c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt")
                    }
                    c.stance = Doggy(user, target)
                } else if (c.stance.en == Stance.flying) {
                    if (user.getPure(Attribute.Dark) >= 18) {
                        c.stance = Flying(user, target)
                    } else if (user.getPure(Attribute.Ninjutsu) >= 5) {
                        c.stance = Neutral(user, target)
                    } else {
                        c.stance = c.stance.insert(user)
                    }
                } else {
                    if (!Global.checkFlag(Flag.exactimages) || target.name.startsWith("Angel")) {
                        c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri")
                        c.offerImage("Angel_Sex.jpg", "Art by AimlessArt")
                    }
                    c.stance = Missionary(user, target)
                }
            } else {
                if (c.stance.prone(user)) {
                    if (!Global.checkFlag(Flag.exactimages) || user.name.startsWith("Cassie")) {
                        c.offerImage("Cowgirl.jpg", "Art by AimlessArt")
                    }
                    if (!Global.checkFlag(Flag.exactimages) || user.id == ID.SAMANTHA) {
                        c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt")
                    }
                    c.stance = Cowgirl(user, target)
                } else if (c.stance.en == Stance.flying) {
                    if (user.getPure(Attribute.Dark) >= 18) {
                        c.stance = Flying(user, target)
                    } else if (user.getPure(Attribute.Ninjutsu) >= 5) {
                        c.stance = Neutral(user, target)
                    } else {
                        c.stance = c.stance.insert(user)
                    }
                } else {
                    c.stance = ReverseCowgirl(user, target)
                }
            }
        } else {
            c.stance = Pin(user, target)
        }
        target.emote(Emotion.nervous, 10)
        user.emote(Emotion.dominant, 10)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Cunning) >= 24
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Cunning to 24)
    }

    override fun copy(user: Character): Skill {
        return Reversal(user)
    }

    override var speed = 4

    override var accuracy = 4

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You try to get on top of " + target.name + ", but she's apparently more ready for it than you realized."
        } else {
            "You take advantage of " + target.name + "'s distraction and put her in a pin."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " tries to reverse your hold, but you stop her."
        } else {
            user.name + " rolls you over and ends up on top."
        }
    }

    override fun describe(): String {
        return "Take dominant position: 15 Mojo"
    }
}

package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import global.Global
import items.Consumable
import status.Enthralled

class DarkTalisman(user: Character) : UseItem(Consumable.Talisman, user) {
    init {
        addTag(Attribute.Dark)
        addTag(SkillTag.CONSUMABLE)
    }

    override fun resolve(c: Combat, target: Character) {
        user.consume(Consumable.Talisman, 1)
        writeOutput(c, target)
        target.add(Enthralled(target, user), c)
    }

    override fun copy(user: Character): Skill {
        return DarkTalisman(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(2)) {
            1 -> "You hold the talisman in front of " + target.name + "'s face and watch her eyes lose focus as she falls under your control."
            else -> "You brandish the dark talisman, which seems to glow with power. The trinket crumbles to dust, but you see the image remain in the reflection of " + target.name + "'s eyes."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (Global.random(3)) {
            2 -> "You find your eyes locked on the talisman in " + user.name + "'s hand, unsure of when exactly it got there."
            1 -> user.name + " pulls out an evil looking talisman. Your eyes are drawn to it against your will."
            else -> user.name + " holds up a strange talisman. You feel compelled to look at the thing, captivated by its unholy nature."
        }
    }
}

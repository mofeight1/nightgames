package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result

class Sacrifice(user: Character) : Skill("Sacrifice", user) {
    init {
        addTag(Attribute.Dark)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 15
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 15)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                !c.stance.sub(user) &&
                user.arousal.percent() >= 70 &&
                user.canSpend(Pool.MOJO, 25)
    }

    override fun describe(): String {
        return "Damage yourself to reduce arousal: 25 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(25)
        writeOutput(c, target)
        user.weaken((15 + user[Attribute.Dark]).toDouble(), c)
        user.calm((2 * user[Attribute.Dark]).toDouble(), c)
    }

    override fun copy(user: Character): Skill {
        return Sacrifice(user)
    }

    override fun type(): Tactics {
        return Tactics.calming
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You feed your own lifeforce and pleasure to the darkness inside you. Your legs threaten to give out, but you've regained some user control."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " pinches her nipples hard while screaming in pain. You see her stagger in exhaustion, but she seems much less aroused."
    }
}

package skills

import characters.Character
import characters.Pool
import combat.Combat
import combat.Result

class CommandInsult(user: Character) : PlayerCommand("Insult", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun describe(): String {
        return "Temporarily destroy your thrall's user-image, draining their mojo."
    }

    override fun resolve(c: Combat, target: Character) {
        target.spendMojo(15)
        user.buildMojo(10)
        writeOutput(c, target)
    }

    override fun copy(user: Character): Skill {
        return CommandInsult(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "Your words nearly drive " + target.name +
                " to tears with their ferocity and psychic backup. Luckily," +
                " she won't remember any of it later."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return "<<This should not be displayed, please inform The" +
                " Silver Bard: CommandInsult-receive>>"
    }
}

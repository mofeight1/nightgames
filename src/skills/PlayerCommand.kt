package skills

import characters.Character
import combat.Combat
import status.Enthralled
import status.Stsflag

abstract class PlayerCommand(name: String, user: Character) : Skill(name, user) {
    override fun requirements(user: Character): Boolean {
        return user.human()
    }

    override fun type(): Tactics {
        return Tactics.demand
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.human() &&
                target.has(Stsflag.enthralled) &&
                (target.getStatus(Stsflag.enthralled) as Enthralled).master == user &&
                !c.stance.penetration(user)
    }
}

package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.Horny
import status.Sore

class SpreadThighs(user: Character) : Skill("Spread Thighs", user) {
    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 6
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 6)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.isPantsless && user.canActNormally(c)
    }

    override fun describe(): String {
        return "Spread your legs to tempt your opponent. Can cause Horniness, but makes you more vulnerable to low blows."
    }

    override fun resolve(c: Combat, target: Character) {
        user.add(Sore(user, 2, Anatomy.genitals, 2.0))
        target.tempt(user[Attribute.Footballer] / 2.0, combat = c)
        if (c.effectRoll(this, user, target, user[Attribute.Footballer])) {
            writeOutput(c, target)
            var duration = 1
            if (user.getPure(Attribute.Footballer) >= 12) {
                duration++
            }
            target.add(Horny(target, user[Attribute.Footballer] / 2.0, duration), c)
        } else {
            writeOutput(c, target, Result.miss)
        }
    }

    override var speed = 9

    override fun copy(user: Character): Skill {
        return SpreadThighs(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            "You spread your legs and flaunt your package with as much flair as you can manage. " + target.name + " grins approvingly at the sight, " +
                    "but doesn't seem to be overcome with lust."
        } else {
            "You spread your legs and flaunt your package with as much flair as you can manage. " + target.name + " flushes and her eyes stay glued " +
                    "to your wagging member."
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.miss) {
            user.name + " spreads her thighs with a seductive smile. She runs her hands down her belly and " +
                    "gives her crotch a sensual rub. You do your best to ignore the temptation. If you win, you'll " +
                    "claim that prize soon anyway."
        } else {
            user.name + " spreads her thighs with a seductive smile. She runs her hands down her belly and " +
                    "gives her crotch a sensual rub. You catch yourself drooling at the sight. Your straining cock " +
                    "insists that you fill her as soon as possible."
        }
    }
}

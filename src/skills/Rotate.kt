package skills

import characters.Character
import combat.Combat
import combat.Result
import stance.Anal
import stance.AnalM
import stance.Cowgirl
import stance.Doggy
import stance.Missionary
import stance.ReverseCowgirl
import stance.Stance

class Rotate(user: Character) : Skill("Rotate", user) {
    override fun requirements(user: Character): Boolean {
        return true
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && c.stance.dom(user) &&
                (
                        c.stance.en == Stance.missionary ||
                                c.stance.en == Stance.doggy ||
                                c.stance.en == Stance.cowgirl ||
                                c.stance.en == Stance.reversecowgirl ||
                                c.stance.en == Stance.anal ||
                                c.stance.en == Stance.analm
                        )
    }

    override fun describe(): String {
        return "Move your partner to a different sexual position."
    }

    override fun resolve(c: Combat, target: Character) {
        when (c.stance.en) {
            Stance.missionary -> {
                writeOutput(c, target, Result.normal)
                c.stance = Doggy(user, target)
            }

            Stance.doggy -> {
                writeOutput(c, target, Result.strong)
                c.stance = Missionary(user, target)
            }

            Stance.anal -> {
                writeOutput(c, target, Result.anal)
                c.stance = AnalM(user, target)
            }

            Stance.analm -> {
                writeOutput(c, target, Result.critical)
                c.stance = Anal(user, target)
            }

            Stance.cowgirl -> {
                writeOutput(c, target, Result.normal)
                c.stance = ReverseCowgirl(user, target)
            }

            Stance.reversecowgirl -> {
                writeOutput(c, target, Result.strong)
                c.stance = Cowgirl(user, target)
            }

            else -> {}
        }
    }

    override fun copy(user: Character): Skill {
        return Rotate(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    // Will need fixes for some situations
    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.strong, Result.anal -> "You flip " + target.name + " onto her back and continue fucking her."
            Result.normal, Result.critical -> "You turn " + target.name + " onto her hands and knees to fuck her from behind."
            else -> "You turn " + target.name + " onto her hands and knees to fuck her from behind."
        }
    }

    // Might need fixes for some situations
    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.anal -> user.name + " flips you onto your back and continues to peg you."
            Result.critical -> user.name + " turns you onto your hands and knees so she can peg you from behind."
            Result.strong -> user.name + " turns around to face you."
            else -> user.name + " turns to face your feet."
        }
    }
}

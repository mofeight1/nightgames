package skills

import characters.Character
import combat.Combat
import combat.Result
import global.Global
import items.Flask
import status.Hypersensitive
import status.Oiled
import status.Stsflag

class CommandUse(user: Character) : PlayerCommand("Force Item Use", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        if (!super.usable(c, target) || !target.isNude) return false
        var usable = false
        for (candidate in CANDIDATES)
            if (target.has(candidate)) when (candidate) {
                Flask.Lubricant -> usable = usable || !target.has(Stsflag.oiled)
                Flask.SPotion -> usable = usable || !target.has(Stsflag.hypersensitive)
                else -> {}
            }
        return usable
    }

    override fun describe(): String {
        return "Force your thrall to use a harmful item on themselves"
    }

    override fun resolve(c: Combat, target: Character) {
        val usable = CANDIDATES.filter {
            target.has(it) && when(it) {
                Flask.Lubricant -> !target.has(Stsflag.oiled)
                Flask.SPotion -> !target.has(Stsflag.hypersensitive)
                else -> false
            }
        }
        if (usable.isEmpty()) return
        val used = usable.random()

        when (used) {
            Flask.Lubricant -> {
                target.add(Oiled(target))
            }
            Flask.SPotion -> {
                target.add(Hypersensitive(target))
            }
            else -> {}
        }
        writeOutput(c, target)
        target.consume(used, 1)
    }

    override fun copy(user: Character): Skill {
        return CommandUse(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.normal -> (target.name
                    + " coats herself in a shiny lubricant at your 'request'.")

            Result.special -> ("Obediently, " + target.name
                    + " smears a sensitivity potion on herself.")

            else -> ("<<This should not be displayed, please inform The"
                    + " Silver Bard: CommandUse-deal>>")
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ("<<This should not be displayed, please inform The"
                + " Silver Bard: CommandUse-receive>>")
    }

    companion object {
        val CANDIDATES: List<Flask> = listOf(
            Flask.Lubricant,
            Flask.SPotion
        )
    }
}

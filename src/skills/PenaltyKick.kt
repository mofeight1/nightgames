package skills

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import global.Global
import stance.Neutral
import stance.Stance
import status.Stsflag

class PenaltyKick(user: Character) : Skill("Penalty Shot", user) {
    init {
        addTag(Attribute.Footballer)
        addTag(SkillTag.PAINFUL)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Footballer) >= 12
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Footballer to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.en == Stance.behind &&
                c.stance.sub(user) &&
                user.canAct() &&
                user.canSpend(Pool.MOJO, 10)
    }

    override fun describe(): String {
        return "Distract your opponent by grinding on them and leave them open to a knee strike. More effective against Horny targets: 10 Mojo"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendMojo(10)
        target.pleasure(
            3 + user[Attribute.Seduction] / 2.0 + target[Attribute.Perception],
            Anatomy.genitals,
            Result.finisher,
            c
        )
        val horny = target.has(Stsflag.horny)
        if (!horny && !c.attackRoll(this, user, target)) {
            writeOutput(c, target, Result.miss)
            return
        }
        if (horny) writeOutput(c, target, Result.strong)
        else writeOutput(c, target, Result.normal)
        if (target.human()) {
            if (Global.random(5) >= 3) {
                c.write(user, user.bbLiner())
            }
        }
        var m = Global.random(6) + user[Attribute.Footballer].toDouble()
        if (horny) {
            m *= 1.2
        }
        target.pain(m, Anatomy.genitals, c)
        c.stance = Neutral(user, target)
    }

    override var speed = 2

    override var accuracy = 3

    override fun copy(user: Character): Skill {
        return PenaltyKick(user)
    }

    override fun type(): Tactics {
        return Tactics.damage
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.strong -> {
                "You grind your ass against " + target.name + "'s crotch to distract her. She's so horny that she enthusiastically grinds back. " +
                        "When she's distracted you kick backwards, catching her completely off guard. You turn to face her while she's busy holding her groin."
            }
            Result.miss -> {
                "ou grind your ass against " + target.name + "'s crotch to distract her. Unfortunately she only tightens her grip on you."
            }
            else -> {
                "You grind your ass against " + target.name + "'s crotch to distract her. You feel her grip weaken as she loses focus. " +
                        "When she's distracted you kick backwards, catching her completely off guard. You turn to face her while she's busy holding her groin."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.strong -> {
                user.name + " squirms in your grip, grinding her ass against your groin. In your lust-fogged state, you can't resist grinding back and " +
                        "rubbing your dick between her firm cheeks. She suddenly slams her heel into your unprotected balls. The pain is devastating, " +
                        "and you release your hold on her to hold your sore bits."
            }
            Result.miss -> {
                user.name + " squirms in your grip, grinding her ass against your groin. It feels good, but you suspect this is just a diversion. When she suddenly " +
                        "tries to kick at you, you're ready to block it with your leg."
            }
            else -> {
                user.name + " squirms in your grip, grinding her ass against your groin. Despite your better judgement, you let yourself indulge in the feeling " +
                        "of her generous ass rubbing your cock for a long moment. She suddenly slams her heel into your unprotected balls. The pain is devastating, " +
                        "and you release your hold on her to hold your sore bits."
            }
        }
    }
}

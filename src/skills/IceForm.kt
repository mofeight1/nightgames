package skills

import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result
import status.IceStance
import status.Stsflag

class IceForm(user: Character) : Skill("Ice Form", user) {
    init {
        addTag(Attribute.Ki)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Ki) >= 18
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Ki to 10)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() && !c.stance.sub(user) && !user.has(Stsflag.form)
    }

    override fun describe(): String {
        return "Improves resistance to pleasure, reduces mojo gain to zero."
    }

    override fun resolve(c: Combat, target: Character) {
        writeOutput(c, target)
        user.add(IceStance(user), c)
    }

    override fun copy(user: Character): Skill {
        return IceForm(user)
    }

    override fun type(): Tactics {
        return Tactics.status
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return "You visualize youruser at the center of a raging snow storm. You can already feel youruser start to go numb."
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return user.name + " takes a deep breath and her expression turns so frosty that you're not sure you can ever thaw her out."
    }
}

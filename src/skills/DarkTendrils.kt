package skills

import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import combat.Result
import global.Global
import stance.StandingOver
import status.Bound
import status.ShadowFingers

class DarkTendrils(user: Character) : Skill("Dark Tendrils", user) {
    init {
        addTag(Attribute.Dark)
        addTag(SkillTag.BIND)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Dark) >= 12 && !user.has(Trait.cursed)
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Dark to 12)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return !c.stance.sub(user) && !c.stance.prone(user) && !c.stance.prone(target) && user.canAct()
    }

    override fun describe(): String {
        return "Summon shadowy tentacles to grab or trip your opponent: 15 Arousal"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spendArousal(15)
        if (!c.effectRoll(this, user, target, user[Attribute.Dark] / 2)) {
            writeOutput(c, target, Result.miss)
        } else {
            if (Global.random(2) == 1) {
                writeOutput(c, target, Result.normal)
                target.add(Bound(target, 10 + user[Attribute.Dark], "shadows"), c)
            } else if (user.check(Attribute.Dark, target.knockdownDC() - user.mojo.current)) {
                writeOutput(c, target, Result.weak)
                c.stance = StandingOver(user, target)
            } else {
                writeOutput(c, target, Result.miss)
            }
        }
        if (user.getPure(Attribute.Dark) >= 24) {
            user.add(ShadowFingers(user))
            writeOutput(c, target, Result.special)
        }
    }

    override fun copy(user: Character): Skill {
        return DarkTendrils(user)
    }

    override fun type(): Tactics {
        return Tactics.positioning
    }

    override var accuracy = 8

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                "You summon dark tentacles to hold " + target.name + ", but she twists away."
            }
            Result.weak -> {
                "You summon dark tentacles that take " + target.name + " feet out from under her."
            }
            Result.special -> {
                "With your advanced mastery of the dark arts, you add smaller tentacles to the fingers on your right hand. These should be useful."
            }
            else -> {
                "You summon a mass of shadow tendrils that entangle " + target.name + " and pin her arms in place."
            }
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return when (modifier) {
            Result.miss -> {
                user.name + " makes a gesture and evil looking tentacles pop up around you. You dive out of the way as they try to grab you."
            }
            Result.weak -> {
                "Your shadow seems to come to life as dark tendrils wrap around your legs and bring you to the floor."
            }
            Result.special -> {
                "A small mass of shadows wrap around " + user.possessive(false) + " hand, covering " + user.possessive(false) + " fingers."
            }
            else -> {
                user.name + " summons shadowy tentacles that snare your arms and hold you in place."
            }
        }
    }
}

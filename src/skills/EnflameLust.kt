package skills

import characters.Attribute
import characters.Character
import characters.Emotion
import combat.Combat
import combat.Result
import status.Horny
import status.Stsflag

class EnflameLust(user: Character) : Skill("Enflame Lust", user) {
    init {
        addTag(Attribute.Hypnosis)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Hypnosis) >= 2
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Hypnosis to 2)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return user.canAct() &&
                c.stance.mobile(user) &&
                !c.stance.behind(user) &&
                !c.stance.behind(target) &&
                !c.stance.sub(user) &&
                (target.has(Stsflag.charmed) || target.has(Stsflag.enthralled))
    }

    override fun describe(): String {
        return "Use hypnotic suggestion to fuel your opponent's dirtiest fantasies."
    }

    override fun resolve(c: Combat, target: Character) {
        val sts = target.getStatus(Stsflag.horny)
        if (sts == null) {
            writeOutput(c, target)
            target.add(Horny(target, 4.0, 6), c)
        }
        else {
            writeOutput(c, target, Result.strong)
            target.add(Horny(target, sts.magnitude * 2, 6), c)
        }
        target.emote(Emotion.horny, 30)
    }

    override fun copy(user: Character): Skill {
        return EnflameLust(user)
    }

    override fun type(): Tactics {
        return Tactics.pleasure
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            String.format(
                "Your hypnosis builds on %s's existing fantasies to shape %s perception of this fight into %s "
                        + "ideal sexual scenario.",
                target.name, target.possessive(false), target.possessive(false)
            )
        } else {
            String.format(
                "You use a suggestion to plant a dirty thought in %s's head. The vague fantasy will be "
                        + "shaped by %s secret desires and make %s hornier than usual.",
                target.name, target.possessive(false), target.pronounTarget(false)
            )
        }
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return if (modifier == Result.strong) {
            String.format(
                "%s elaborates in glorious, erotic detail on the fantasy that was running through your head. You have "
                        + "no idea how %s knew what you were thinking about, but %s story is so intensely sexual that you have trouble thinking straight.",
                user.name, user.pronounSubject(false), user.possessive(false)
            )
        } else {
            String.format(
                "In a brief lull in the fight, %s shares the story of %s favorite sexual experience with you. Thinking "
                        + "back, you have trouble remembering all the details, but you do know it was hot enough that you can't help "
                        + "fantasizing about it.", user.name, user.possessive(false)
            )
        }
    }
}

package skills

import characters.Attribute
import characters.Character
import characters.Pool
import combat.Combat
import combat.Result
import status.Abuff

class Haste(user: Character) : Skill("Haste", user) {
    init {
        addTag(Attribute.Temporal)
    }

    override fun requirements(user: Character): Boolean {
        return user.getPure(Attribute.Temporal) >= 1
    }

    override fun attRequired(): Map<Attribute, Int> {
        return hashMapOf(Attribute.Temporal to 1)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return c.stance.mobile(user) &&
                !c.stance.prone(user) &&
                user.canAct() &&
                user.canSpend(Pool.TIME, 1) &&
                user.getStatusMagnitude("Speed buff") < 10
    }

    override fun describe(): String {
        return "Temporarily buffs your speed: 1 charge"
    }

    override fun resolve(c: Combat, target: Character) {
        user.spend(Pool.TIME, 1)
        writeOutput(c, target)
        user.add(Abuff(user, Attribute.Speed, 10.0, 6), c)
    }

    override fun copy(user: Character): Skill {
        return Haste(user)
    }

    override fun type(): Tactics {
        return Tactics.preparation
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        return String.format("You spend a stored time charge. The world around you appears to slow down as your personal time accelerates.")
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return String.format(
            "%s hits a button on %s wristwatch and suddenly speeds up. %s is moving so fast that %s seems to blur.",
            user.name, user.possessive(false), user.pronounSubject(true), user.pronounSubject(false)
        )
    }
}

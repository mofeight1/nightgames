package skills

import characters.Anatomy
import characters.Character
import combat.Combat
import combat.Result
import global.Global

class CommandMasturbate(user: Character) : PlayerCommand("Force Masturbation", user) {
    init {
        addTag(SkillTag.COMMAND)
    }

    override fun usable(c: Combat, target: Character): Boolean {
        return super.usable(c, target) && target.isPantsless
    }

    override fun describe(): String {
        return "Convince your opponent to pleasure themselves for your viewing pleasure"
    }

    override fun resolve(c: Combat, target: Character) {
        val lowStart = target.arousal.current < 15
        target.pleasure(5.0 + Global.random(10), Anatomy.genitals, combat = c)
        val lowEnd = target.arousal.current < 15
        val res = if (lowStart) {
            if (lowEnd) Result.weak
            else Result.strong
        } else Result.normal
        writeOutput(c, target, res)
    }

    override fun copy(user: Character): Skill {
        return CommandMasturbate(user)
    }

    override fun deal(target: Character, modifier: Result?, damage: Int): String {
        var story: String

        when (modifier) {
            Result.normal -> story = if (target.hasDick) {
                "" + target.name + " seems more than happy to do as you tell %her%. " +
                        "%She% starts stroking %her% cock with abandon."
            } else {
                "" + target.name + " seems more than happy to do as you tell %her%. " +
                        "%She% starts rubbing %her% clit with abandon."
            }

            Result.special -> story = if (target.hasDick) {
                "" + target.name + " jerks off %her% rigid cock while looking " +
                        "at you lustily."
            } else {
                "Looking at you lustily, " + target.name +
                        " rubs her clit as %she% gets wet."
            }

            Result.weak -> story = "" + target.name + " follows your command to the letter, but" +
                    " it doesn't seem to have that much of an effect on %her%."

            else -> story = "<<This should not be displayed, please inform The" +
                    " Silver Bard: CommandMasturbate-deal>>"
        }
        story = story.replace("%her%".toRegex(), target.possessive(false))
        story = story.replace("%Her%".toRegex(), target.possessive(true))
        story = story.replace("%she%".toRegex(), target.pronounSubject(false))
        story = story.replace("%She%".toRegex(), target.pronounSubject(true))

        return story
    }

    override fun receive(target: Character, modifier: Result?, damage: Int): String {
        return ("<<This should not be displayed, please inform The"
                + " Silver Bard: CommandMasturbate-receive>>")
    }
}

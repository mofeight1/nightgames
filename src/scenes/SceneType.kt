package scenes

enum class SceneType {
    VICTORY,
    DEFEAT,
    DRAW,
    INTERVENTION,
    DAYTIME,
    EVENT,
    THREESOME,
}

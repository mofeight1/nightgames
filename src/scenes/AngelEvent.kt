package scenes

import characters.Character
import global.Flag
import global.Global
import global.Scheduler
import java.time.LocalTime

class AngelEvent(private val player: Character) : Event {
    override fun respond(response: String) {
        Global.gui.clearText()
        Scheduler.advanceTime(LocalTime.of(1, 0))
        Scheduler.day!!.plan()
    }

    override fun play(response: String): Boolean {
        if (response.equals("Sarah's Flower", ignoreCase = true)) {
            SceneManager.play(SceneFlag.AngelSarahFlower)
            Global.flag(Flag.CorruptSarah)
        }
        Global.current = this
        Global.gui.choose("Next")
        return true
    }

    override fun morning(): String {
        return ""
    }

    override fun mandatory(): String {
        return ""
    }

    override fun addAvailable(available: HashMap<String, Int>) {
        if (Global.checkFlag(Flag.CorruptCaroline) && !Global.checkFlag(Flag.CorruptSarah)) {
            available["Sarah's Flower"] = 10
        }
    }
}

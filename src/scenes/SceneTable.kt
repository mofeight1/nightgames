package scenes

abstract class SceneTable {
    fun getScene(flag: SceneFlag): Scene? {
        when (flag.type) {
            SceneType.VICTORY -> return getVictoryScene(flag)
            SceneType.DEFEAT -> return getDefeatScene(flag)
            SceneType.DRAW -> return getDrawScene(flag)
            SceneType.INTERVENTION -> return getInterventionScene(flag)
            SceneType.EVENT -> return getEventScene(flag)
            SceneType.DAYTIME -> return getDayScene(flag)
            SceneType.THREESOME -> {}
        }
        return Scene("Error! Scene " + flag.label + " not found.")
    }

    abstract fun getVictoryScene(flag: SceneFlag): Scene
    abstract fun getDefeatScene(flag: SceneFlag): Scene
    abstract fun getDrawScene(flag: SceneFlag): Scene
    abstract fun getInterventionScene(flag: SceneFlag): Scene
    abstract fun getEventScene(flag: SceneFlag): Scene
    abstract fun getDayScene(flag: SceneFlag): Scene
}

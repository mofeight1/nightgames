package scenes

import characters.Character
import characters.Dummy
import characters.Emotion
import global.Flag
import global.Global
import global.Scheduler
import java.time.LocalTime

class MaraEvent(private val player: Character) : Event {
    private var scene = 1

    override fun respond(response: String) {
        if (response.equals("Next", ignoreCase = true)) {
            scene++
            play("")
        } else if (response.equals("Leave", ignoreCase = true)) {
            Scheduler.time = LocalTime.of(15, 0)
            Global.gui.clearText()
            Scheduler.day!!.plan()
        }
    }

    override fun play(response: String): Boolean {
        Global.current = this
        Global.gui.clearText()
        Global.gui.clearCommand()
        val mara = Dummy("Mara")
        val cassie = Dummy("Cassie")
        when (scene) {
            1 -> {
                mara.undress()
                mara.blush = 1
                Global.gui.loadPortrait(player, mara)
                SceneManager.play(SceneFlag.MaraSickDayMorning)
                Global.gui.choose("Next")
            }

            2 -> {
                mara.undress()
                mara.blush = 3
                mara.mood = Emotion.desperate
                Global.gui.loadPortrait(player, mara)
                SceneManager.play(SceneFlag.MaraSickDayShower)
                Global.gui.choose("Next")
            }

            3 -> {
                mara.undress()
                mara.blush = 1
                mara.mood = Emotion.angry
                Global.gui.loadPortrait(player, mara)
                SceneManager.play(SceneFlag.MaraSickDayInterrogation)
                Global.gui.choose("Next")
            }

            4 -> {
                mara.undress()
                mara.blush = 2
                mara.mood = Emotion.horny
                cassie.undress()
                cassie.blush = 3
                cassie.mood = Emotion.desperate
                Global.gui.loadTwoPortraits(mara, cassie)
                SceneManager.play(SceneFlag.MaraSickDayLunch)
                Global.gui.choose("Next")
            }

            5 -> {
                mara.undress()
                mara.blush = 1
                mara.mood = Emotion.confident
                cassie.undress()
                cassie.blush = 1
                cassie.mood = Emotion.confident
                Global.gui.loadTwoPortraits(cassie, mara)
                SceneManager.play(SceneFlag.MaraSickDayFinale)
                Global.gui.choose("Leave")
                Global.flag(Flag.MaraTemporal)
            }
        }
        return true
    }

    override fun morning(): String {
        return if (Global.checkFlag(Flag.MaraDayOff) && !Global.checkFlag(Flag.MaraTemporal)) {
            "DayOff"
        } else {
            ""
        }
    }

    override fun mandatory(): String {
        return ""
    }

    override fun addAvailable(available: HashMap<String, Int>) {
        return
    }
}

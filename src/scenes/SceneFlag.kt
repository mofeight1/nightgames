package scenes

import characters.ID

enum class SceneFlag(val label: String, val type: SceneType, val star: ID) {
    //CASSIE
    CassieAfterMatch("Cassie After Match", SceneType.EVENT, ID.CASSIE),

    CassieForeplayDefeatBasic("Cassie Foreplay Defeat Basic", SceneType.DEFEAT, ID.CASSIE),
    CassieForeplayDefeatAlt("Cassie Foreplay Defeat Alternate", SceneType.DEFEAT, ID.CASSIE),
    CassieForeplayDefeatManaDrain("Cassie Foreplay Defeat Mana Drain", SceneType.DEFEAT, ID.CASSIE),
    CassieForeplayDefeatEasy("Cassie Foreplay Defeat Easy", SceneType.DEFEAT, ID.CASSIE),
    CassieForeplayDefeatClothed("Cassie Foreplay Defeat Clothed", SceneType.DEFEAT, ID.CASSIE),
    CassieForeplayDefeatRoleplay("Cassie Foreplay Defeat Roleplay", SceneType.DEFEAT, ID.CASSIE),
    CassieSexDefeat("Cassie Sex Defeat", SceneType.DEFEAT, ID.CASSIE),
    CassieAnalDefeat("Cassie Anal Defeat", SceneType.DEFEAT, ID.CASSIE),

    CassieForeplayVictoryBasic("Cassie Foreplay Victory Basic", SceneType.VICTORY, ID.CASSIE),
    CassieForeplayVictoryAlt("Cassie Foreplay Victory Alt", SceneType.VICTORY, ID.CASSIE),
    CassieForeplayVictoryEasy("Cassie Foreplay Victory Easy", SceneType.VICTORY, ID.CASSIE),
    CassieForeplayVictoryEasyAlt("Cassie Foreplay Victory Easy Alternate", SceneType.VICTORY, ID.CASSIE),
    CassieMagicVictory("Cassie Magic Victory", SceneType.VICTORY, ID.CASSIE),
    CassieHornyVictory("Cassie Horny Victory", SceneType.VICTORY, ID.CASSIE),
    CassieFaerieVictory("Cassie Faerie Victory", SceneType.VICTORY, ID.CASSIE),
    CassieSexVictory("Cassie Sex Victory", SceneType.VICTORY, ID.CASSIE),
    CassiePeggingVictory("Cassie Pegging Victory", SceneType.VICTORY, ID.CASSIE),

    CassieForeplayDraw("Cassie Foreplay Draw", SceneType.DRAW, ID.CASSIE),
    CassieSexDraw("Cassie Sex Draw", SceneType.DRAW, ID.CASSIE),
    CassieMagicDraw("Cassie Magic Draw", SceneType.DRAW, ID.CASSIE),

    CassieWatch("Cassie Watch", SceneType.INTERVENTION, ID.CASSIE),
    CassieWatchMagic("Cassie Watch Magic", SceneType.INTERVENTION, ID.CASSIE),
    CassieWatchPenis("Cassie Watch Magic (versus penis)", SceneType.INTERVENTION, ID.CASSIE),

    CassieSparring3("Cassie Sparring 3", SceneType.DAYTIME, ID.CASSIE),

    //ANGEL
    AngelAfterMatch("Angel After Match", SceneType.EVENT, ID.ANGEL),

    AngelForeplayDefeat("Angel Foreplay Defeat", SceneType.DEFEAT, ID.ANGEL),
    AngelSexDefeat("Angel Sex Defeat", SceneType.DEFEAT, ID.ANGEL),
    AngelForeplayDefeatAlt("Angel Foreplay Defeat Alt", SceneType.DEFEAT, ID.ANGEL),
    AngelSuccubusDefeat("Angel Succubus Defeat", SceneType.DEFEAT, ID.ANGEL),
    AngelMasochismDefeat("Angel Masochism Defeat", SceneType.DEFEAT, ID.ANGEL),
    AngelImpDefeat("Angel Imp Defeat", SceneType.DEFEAT, ID.ANGEL),
    AngelAnalDefeat("Angel Anal Defeat", SceneType.DEFEAT, ID.ANGEL),
    AngelKissDefeat("Angel Kiss Defeat", SceneType.DEFEAT, ID.ANGEL),

    AngelForeplayVictory("Angel Foreplay Victory", SceneType.VICTORY, ID.ANGEL),
    AngelForeplayVictoryAlt("Angel Foreplay Victory Alt", SceneType.VICTORY, ID.ANGEL),
    AngelSexVictory("Angel Sex Victory", SceneType.VICTORY, ID.ANGEL),
    AngelImpVictory("Angel Imp Victory", SceneType.VICTORY, ID.ANGEL),
    AngelSuccubusVictory("Angel Succubus Victory", SceneType.VICTORY, ID.ANGEL),
    AngelHornyVictory("Angel Horny Victory", SceneType.VICTORY, ID.ANGEL),
    AngelFlyingVictory("Angel Flying Victory", SceneType.VICTORY, ID.ANGEL),
    AngelPeggingVictoryFirst("Angel Pegging Victory First", SceneType.VICTORY, ID.ANGEL),
    AngelPeggingVictory("Angel Pegging Victory", SceneType.VICTORY, ID.ANGEL),
    AngelPeggingVictorySubmission("Angel Pegging Victory Submission", SceneType.VICTORY, ID.ANGEL),

    AngelForeplayDraw("Angel Foreplay Draw", SceneType.DRAW, ID.ANGEL),
    AngelSexDraw("Angel Sex Draw", SceneType.DRAW, ID.ANGEL),
    AngelSuccubusDraw("Angel Succubus Draw", SceneType.DRAW, ID.ANGEL),

    AngelWatch("Angel Watch", SceneType.INTERVENTION, ID.ANGEL),
    AngelWatchDark("Angel Watch Dark", SceneType.INTERVENTION, ID.ANGEL),

    AngelMeiPleasure("Mei's Pleasure", SceneType.DAYTIME, ID.ANGEL),
    AngelCarolineKiss("Caroline's Kiss", SceneType.DAYTIME, ID.ANGEL),
    AngelSarahFlower("Sarah's Flower", SceneType.DAYTIME, ID.ANGEL),
    AngelRevelation("Angel's Revelation", SceneType.DAYTIME, ID.ANGEL),

    //MARA
    MaraAfterMatch("Mara After Match", SceneType.EVENT, ID.MARA),
    MaraSickDayAfterMatch("Mara Day Off After Match", SceneType.EVENT, ID.MARA),
    MaraSickDayNight("Mara Day Off Night", SceneType.EVENT, ID.MARA),
    MaraSickDayMorning("Mara Day Off Morning", SceneType.EVENT, ID.MARA),
    MaraSickDayShower("Mara Day Off Shower", SceneType.EVENT, ID.MARA),
    MaraSickDayInterrogation("Mara Day Off Interrogation", SceneType.EVENT, ID.MARA),
    MaraSickDayLunch("Mara Day Off Lunch", SceneType.EVENT, ID.MARA),
    MaraSickDayFinale("Mara Day Off Finale", SceneType.EVENT, ID.MARA),

    MaraForeplayDefeat("Mara Foreplay Defeat", SceneType.DEFEAT, ID.MARA),
    MaraForeplayDefeatAlt("Mara Foreplay Defeat Alt", SceneType.DEFEAT, ID.MARA),
    MaraForeplayDefeatSensitive("Mara Foreplay Defeat Sensitive", SceneType.DEFEAT, ID.MARA),
    MaraHornyDefeat("Mara Horny Defeat", SceneType.DEFEAT, ID.MARA),
    MaraForeplayDefeatEasy("Mara Foreplay Defeat Easy", SceneType.DEFEAT, ID.MARA),
    MaraTickleDefeat("Mara Tickle Defeat", SceneType.DEFEAT, ID.MARA),
    MaraSexDefeat("Mara Sex Defeat", SceneType.DEFEAT, ID.MARA),
    MaraPinDefeat("Mara Pin Defeat", SceneType.DEFEAT, ID.MARA),
    MaraBoundDefeat("Mara Bound Defeat", SceneType.DEFEAT, ID.MARA),
    MaraAnalDefeat("Mara Anal Defeat", SceneType.DEFEAT, ID.MARA),

    MaraForeplayVictory("Mara Foreplay Victory", SceneType.VICTORY, ID.MARA),
    MaraSexVictory("Mara Sex Victory", SceneType.VICTORY, ID.MARA),
    MaraPeggingVictory("Mara Pegging Victory", SceneType.VICTORY, ID.MARA),
    MaraSlimeVictory("Mara Slime Victory", SceneType.VICTORY, ID.MARA),
    MaraShockVictory("Mara Shock Victory", SceneType.VICTORY, ID.MARA),
    MaraFootjobVictory("Mara Footjob Victory", SceneType.VICTORY, ID.MARA),
    MaraLubeVictory("Mara Lube Victory", SceneType.VICTORY, ID.MARA),
    MaraOnaholeVictory("Mara Onahole Victory", SceneType.VICTORY, ID.MARA),

    MaraSexDraw("Mara Sex Draw", SceneType.DRAW, ID.MARA),
    MaraForeplayDraw("Mara Foreplay Draw", SceneType.DRAW, ID.MARA),
    MaraAphrodisiacDraw("Mara Aphrodisiac Draw", SceneType.DRAW, ID.MARA),
    MaraTemporalDraw("Mara Temporal Draw", SceneType.DRAW, ID.MARA),

    MaraWatch("Mara Watch", SceneType.INTERVENTION, ID.MARA),
    MaraWatchScience("Mara Watch Science", SceneType.INTERVENTION, ID.MARA),

    //JEWEL
    JewelAfterMatch("Jewel After Match", SceneType.EVENT, ID.JEWEL),

    JewelForeplayVictory("Jewel Foreplay Victory Easy", SceneType.VICTORY, ID.JEWEL),
    JewelForeplayVictoryAlt("Jewel Foreplay Victory", SceneType.VICTORY, ID.JEWEL),
    JewelFireVictory("Jewel Fire Form Victory", SceneType.VICTORY, ID.JEWEL),
    JewelPinVictory("Jewel Pin Victory", SceneType.VICTORY, ID.JEWEL),
    JewelHornyVictory("Jewel Horny Victory", SceneType.VICTORY, ID.JEWEL),
    JewelUltVictory("Jewel Pleasure Bomb Victory", SceneType.VICTORY, ID.JEWEL),
    JewelSexVictory("Jewel Sex Victory", SceneType.VICTORY, ID.JEWEL),
    JewelPeggingVictory("Jewel Pegging Victory", SceneType.VICTORY, ID.JEWEL),

    JewelForeplayDefeat("Jewel Foreplay Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelForeplayDefeatAlt("Jewel Foreplay Defeat Alt", SceneType.DEFEAT, ID.JEWEL),
    JewelForeplayDefeatEasy("Jewel Foreplay Defeat Easy", SceneType.DEFEAT, ID.JEWEL),
    JewelChallengeDefeat("Jewel Challenge Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelSexDefeat("Jewel Sex Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelReversalDefeat("Jewel Sex Reversal Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelAnalDefeat("Jewel Anal Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelMasochismDefeat("Jewel Masochism Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelPinDefeat("Jewel Pin Defeat", SceneType.DEFEAT, ID.JEWEL),
    JewelHornyDefeat("Jewel Horny Defeat", SceneType.DEFEAT, ID.JEWEL),

    JewelForeplayDraw("Jewel Foreplay Draw", SceneType.DRAW, ID.JEWEL),
    JewelSexDraw("Jewel Sex Draw", SceneType.DRAW, ID.JEWEL),

    JewelWatch("Jewel Watch", SceneType.INTERVENTION, ID.JEWEL),

    //REYKA
    ReykaAfterMatch("Reyka After Match", SceneType.EVENT, ID.REYKA),

    ReykaForeplayVictory("Reyka Foreplay Victory", SceneType.VICTORY, ID.REYKA),
    ReykaSexVictory("Reyka Sex Victory", SceneType.VICTORY, ID.REYKA),
    ReykaPeggingVictory("Reyka Pegging Victory", SceneType.VICTORY, ID.REYKA),
    ReykaImpVictory("Reyka Imp Victory", SceneType.VICTORY, ID.REYKA),
    ReykaEntralledVictory("Reyka Enthralled Victory", SceneType.VICTORY, ID.REYKA),
    ReykaFrustratedVictory("Reyka Frustrated Victory", SceneType.VICTORY, ID.REYKA),

    ReykaForeplayDefeat("Reyka Foreplay Defeat", SceneType.DEFEAT, ID.REYKA),
    ReykaForeplayDefeatAlt("Reyka Frustrated Defeat", SceneType.DEFEAT, ID.REYKA),
    ReykaSexDefeat("Reyka Sex Defeat", SceneType.DEFEAT, ID.REYKA),
    ReykaAnalDefeat("Reyka Anal Defeat", SceneType.DEFEAT, ID.REYKA),

    ReykaForeplayDraw("Reyka Foreplay Draw", SceneType.DRAW, ID.REYKA),
    ReykaSexDraw("Reyka Sex Draw", SceneType.DRAW, ID.REYKA),

    ReykaWatch("Reyka Watch", SceneType.INTERVENTION, ID.REYKA),

    //YUI
    YuiAfterMatch("Yui After Match", SceneType.EVENT, ID.YUI),
    YuiFirstAfterMatch("Yui's First Time After Match", SceneType.EVENT, ID.YUI),
    YuiFirstUndress("Yui's First Time Undressing", SceneType.EVENT, ID.YUI),
    YuiFirstService("Yui's First Time Service", SceneType.EVENT, ID.YUI),
    YuiFirstLick("Yui's First Time Eat Out", SceneType.EVENT, ID.YUI),
    YuiFirstSex("Yui's First Time Sex", SceneType.EVENT, ID.YUI),

    YuiForeplayVictory("Yui Foreplay Victory", SceneType.VICTORY, ID.YUI),
    YuiSexVictory("Yui Sex Victory", SceneType.VICTORY, ID.YUI),
    YuiBunshinVictory("Yui Bunshin Victory", SceneType.VICTORY, ID.YUI),
    YuiBoundVictory("Yui Bound Victory", SceneType.VICTORY, ID.YUI),
    YuiPeggingVictory("Yui Pegging Victory", SceneType.VICTORY, ID.YUI),

    YuiForeplayDefeat("Yui Foreplay Defeat", SceneType.DEFEAT, ID.YUI),
    YuiForeplayDefeatAlt("Yui Foreplay Defeat Alt", SceneType.DEFEAT, ID.YUI),
    YuiSexDefeat("Yui Sex Defeat", SceneType.DEFEAT, ID.YUI),
    YuiAnalDefeat("Yui Anal Defeat", SceneType.DEFEAT, ID.YUI),
    YuiClothedDefeat("Yui Clothed Defeat", SceneType.DEFEAT, ID.YUI),
    YuiShamedDefeat("Yui Shamed Defeat", SceneType.DEFEAT, ID.YUI),

    YuiForeplayDraw("Yui Foreplay Draw", SceneType.DRAW, ID.YUI),
    YuiSexDraw("Yui Sex Draw", SceneType.DRAW, ID.YUI),

    YuiWatch("Yui Watch", SceneType.INTERVENTION, ID.YUI),

    //KAT
    KatAfterMatch("Kat After Match", SceneType.EVENT, ID.KAT),

    KatForeplayVictory("Kat Foreplay Victory", SceneType.VICTORY, ID.KAT),
    KatForeplayVictoryEasy("Kat Easy Foreplay Victory", SceneType.VICTORY, ID.KAT),
    KatFootjobVictory("Kat Footjob Victory", SceneType.VICTORY, ID.KAT),
    KatSexVictory("Kat Sex Victory", SceneType.VICTORY, ID.KAT),
    KatSexVictoryAlt("Kat Alternate Sex Victory", SceneType.VICTORY, ID.KAT),
    KatTailjobVictory("Kat Tailjob Victory", SceneType.VICTORY, ID.KAT),
    KatPeggingVictory("Kat Pegging Victory", SceneType.VICTORY, ID.KAT),

    KatForeplayDefeat("Kat Foreplay Defeat", SceneType.DEFEAT, ID.KAT),
    KatForeplayDefeatAlt("Kat Foreplay Defeat Alternate", SceneType.DEFEAT, ID.KAT),
    KatBoundDefeat("Kat Bound Defeat", SceneType.DEFEAT, ID.KAT),
    KatSexDefeat("Kat Sex Defeat", SceneType.DEFEAT, ID.KAT),
    KatSexDefeatCarry("Kat Sex Carry Defeat", SceneType.DEFEAT, ID.KAT),
    KatFeralDefeat("Kat Feral Defeat", SceneType.DEFEAT, ID.KAT),
    KatAnalDefeat("Kat Anal Defeat", SceneType.DEFEAT, ID.KAT),

    KatForeplayDraw("Kat Foreplay Draw", SceneType.DRAW, ID.KAT),
    KatSexDraw("Kat Sex Draw", SceneType.DRAW, ID.KAT),

    KatWatch("Kat Watch", SceneType.INTERVENTION, ID.KAT),
    KatWatchPenis("Kat Watch (versus penis)", SceneType.INTERVENTION, ID.KAT),

    //EVE
    EveInititation("Eve Initiation", SceneType.DAYTIME, ID.EVE),
    EveZoeLowWager("Zoe Low Stakes Wager", SceneType.DAYTIME, ID.EVE),
    EveZoeLowWin("Zoe Low Stakes Win", SceneType.DAYTIME, ID.EVE),
    EveZoeLowLoss("Zoe Low Stakes Loss", SceneType.DAYTIME, ID.EVE),
    EveZoeHighWager("Zoe High Stakes Wager", SceneType.DAYTIME, ID.EVE),
    EveZoeHighWin("Zoe High Stakes Win", SceneType.DAYTIME, ID.EVE),
    EveZoeHighLoss("Zoe High Stakes Loss", SceneType.DAYTIME, ID.EVE),

    EveForeplayVictory("Eve Foreplay Victory", SceneType.VICTORY, ID.EVE),
    EveForeplayVictoryAlt("Eve Alternate Foreplay Victory", SceneType.VICTORY, ID.EVE),
    EvePeggingVictory("Eve Pegging Victory", SceneType.VICTORY, ID.EVE),

    EveForeplayDefeat("Eve Foreplay Defeat", SceneType.DEFEAT, ID.EVE),
    EveForeplayDefeatAlt("Eve Foreplay Defeat Alt", SceneType.DEFEAT, ID.EVE),
    EveSexDefeat("Eve Sex Defeat", SceneType.DEFEAT, ID.EVE),
    EveAnalDefeat("Eve Anal Defeat", SceneType.DEFEAT, ID.EVE),

    EveForeplayDraw("Eve Foreplay Draw", SceneType.DRAW, ID.EVE),
    EveSexDraw("Eve Sex Draw", SceneType.DRAW, ID.EVE),

    EveWatch("Eve Watch", SceneType.INTERVENTION, ID.EVE),

    //SAMANTHA
    SamanthaAfterMatch("Samantha After Match", SceneType.EVENT, ID.SAMANTHA),

    SamanthaForeplayVictory("Samantha Foreplay Victory", SceneType.VICTORY, ID.SAMANTHA),
    SamanthaSexVictory("Samantha Sex Victory", SceneType.VICTORY, ID.SAMANTHA),
    SamanthaPeggingVictory("Samantha Pegging Victory", SceneType.VICTORY, ID.SAMANTHA),
    SamanthaHornyVictory("Samantha Horny Victory", SceneType.VICTORY, ID.SAMANTHA),

    SamanthaForeplayDefeat("Samantha Foreplay Defeat", SceneType.DEFEAT, ID.SAMANTHA),
    SamanthaForeplayDefeatAlt("Samantha Foreplay Defeat Alt", SceneType.DEFEAT, ID.SAMANTHA),
    SamanthaSexDefeat("Samantha Sex Defeat", SceneType.DEFEAT, ID.SAMANTHA),
    SamanthaAnalDefeat("Samantha Anal Defeat", SceneType.DEFEAT, ID.SAMANTHA),

    SamanthaForeplayDraw("Samantha Foreplay Draw", SceneType.DRAW, ID.SAMANTHA),
    SamanthaSexDraw("Samantha Sex Draw", SceneType.DRAW, ID.SAMANTHA),
    SamanthaSexDraw2("Samantha Alternate Sex Draw", SceneType.DRAW, ID.SAMANTHA),

    SamanthaWatch("Samantha Watch", SceneType.INTERVENTION, ID.SAMANTHA),

    SamanthaPoker("Samantha Strip Poker", SceneType.DAYTIME, ID.SAMANTHA),

    //VALERIE
    ValerieAfterMatch("Valerie After Match", SceneType.EVENT, ID.VALERIE),
    ValerieAfterMatchSub("Valerie After Match Submissive", SceneType.EVENT, ID.VALERIE),

    ValerieComposedForeplayVictory("Valerie Composed Foreplay Victory", SceneType.VICTORY, ID.VALERIE),
    ValerieComposedForeplayVictoryAlt("Valerie Composed Alternate Foreplay Victory", SceneType.VICTORY, ID.VALERIE),
    ValerieComposedSexVictory("Valerie Composed Sex Victory", SceneType.VICTORY, ID.VALERIE),
    ValerieBrokenForeplayVictory("Valerie Broken Foreplay Victory", SceneType.VICTORY, ID.VALERIE),
    ValerieBrokenSexVictory("Valerie Broken Sex Victory", SceneType.VICTORY, ID.VALERIE),
    ValeriePeggingVictory("Valerie Pegging Victory", SceneType.VICTORY, ID.VALERIE),

    ValerieComposedForeplayDefeat("Valerie Composed Foreplay Defeat", SceneType.DEFEAT, ID.VALERIE),
    ValerieComposedForeplayDefeatAlt("Valerie Composed Alternate Foreplay Defeat", SceneType.DEFEAT, ID.VALERIE),
    ValerieComposedSexDefeat("Valerie Composed Sex Defeat", SceneType.DEFEAT, ID.VALERIE),
    ValerieComposedAnalDefeat("Valerie Composed Anal Defeat", SceneType.DEFEAT, ID.VALERIE),
    ValerieBrokenForeplayDefeat("Valerie Broken Foreplay Defeat", SceneType.DEFEAT, ID.VALERIE),
    ValerieBrokenSexDefeat("Valerie Broken Sex Defeat", SceneType.DEFEAT, ID.VALERIE),
    ValerieBrokenAnalDefeat("Valerie Broken Anal Defeat", SceneType.DEFEAT, ID.VALERIE),

    ValerieComposedForeplayDraw("Valerie Composed Foreplay Draw", SceneType.DRAW, ID.VALERIE),
    ValerieComposedSexDraw("Valerie Composed Sex Draw", SceneType.DRAW, ID.VALERIE),
    ValerieBrokenForeplayDraw("Valerie Foreplay Draw", SceneType.DRAW, ID.VALERIE),
    ValerieBrokenSexDraw("Valerie Sex Draw", SceneType.DRAW, ID.VALERIE),

    ValerieWatch("Valerie Watch", SceneType.INTERVENTION, ID.VALERIE),

    //SOFIA
    SofiaAfterMatch("Sofia After Match", SceneType.EVENT, ID.SOFIA),

    SofiaForeplayVictory("Sofia Foreplay Victory", SceneType.VICTORY, ID.SOFIA),
    SofiaSexVictory("Sofia Sex Victory", SceneType.VICTORY, ID.SOFIA),

    SofiaForeplayDefeat("Sofia Foreplay Defeat", SceneType.DEFEAT, ID.SOFIA),
    SofiaForeplayDefeatAlt("Sofia Foreplay Defeat Alt", SceneType.DEFEAT, ID.SOFIA),
    SofiaSexDefeat("Sofia Sex Defeat", SceneType.DEFEAT, ID.SOFIA),
    SofiaAnalDefeat("Sofia Anal Defeat", SceneType.DEFEAT, ID.SOFIA),

    SofiaForeplayDraw("Sofia Foreplay Draw", SceneType.DRAW, ID.SOFIA),
    SofiaSexDraw("Sofia Sex Draw", SceneType.DRAW, ID.SOFIA),

    SofiaWatch("Sofia Watch", SceneType.INTERVENTION, ID.SOFIA),

    //MAYA
    MayaVictory("Maya Victory", SceneType.VICTORY, ID.MAYA),

    MayaDefeat("Maya Defeat", SceneType.DEFEAT, ID.MAYA),
    MayaAnalDefeat("Maya Anal Defeat", SceneType.DEFEAT, ID.MAYA),

    MayaDraw("Maya Draw", SceneType.DRAW, ID.MAYA),

    MayaWatch("Maya Watch", SceneType.INTERVENTION, ID.MAYA),

    //Other
    CassieMaraThreesome("Cassie and Mara Threesome", SceneType.DAYTIME, ID.PLAYER),
    CassieJewelThreesome("Cassie and Jewel Threesome", SceneType.DAYTIME, ID.PLAYER),
    CassieAngelThreesome("Cassie and Angel Threesome", SceneType.DAYTIME, ID.PLAYER),
    CassieYuiThreesome("Cassie and Yui Threesome", SceneType.DAYTIME, ID.PLAYER),
    MaraJewelThreesome("Mara and Jewel Threesome", SceneType.DAYTIME, ID.PLAYER),
    AngelMaraThreesome("Angel and Mara Threesome", SceneType.DAYTIME, ID.PLAYER),
    AngelReykaThreesome("Angel and Reyka Threesome", SceneType.DAYTIME, ID.PLAYER),
    AngelReykaThreesome2("Angel and Reyka 2nd Threesome", SceneType.DAYTIME, ID.PLAYER),
    AngelReykaThreesome2a("Angel and Reyka 2nd Threesome alt", SceneType.DAYTIME, ID.PLAYER),
    MaraKatThreesome("Mara and Kat Threesome", SceneType.DAYTIME, ID.PLAYER),
    CassieKatThreesome("Cassie and Kat Threesome", SceneType.DAYTIME, ID.PLAYER),
    AngelJewelThreesome("Angel and Jewel Threesome", SceneType.DAYTIME, ID.PLAYER),
}

package scenes

import characters.Character
import global.Global
import kotlin.math.min

class Scene() {
    private var text: ArrayList<String>
    private val images: ArrayList<List<String>>

    init {
        this.text = ArrayList()
        this.images = ArrayList()
    }

    constructor(text: String) : this() {
        this.text.add(text)
    }

    constructor(text: String, images: List<List<String>>) : this(text) {
        this.images.addAll(images)
    }

    constructor(text: List<String>) : this() {
        this.text.addAll(text)
    }

    constructor(text: List<String>, images: List<List<String>>) : this(text) {
        this.images.addAll(images)
    }

    fun addText(text: String) {
        this.text.add(text)
    }

    fun addImage(path: String, author: String) {
        images.add(listOf(path, author))
    }

    val pages: Int
        get() = min(1, text.size - images.size)

    fun play(page: Int) {
        var index = (page + min(page, images.size))
        if (text.size > index) {
            Global.gui.message(text[index])
            if (images.size > page) {
                Global.gui.displayImage(images[page][0], images[page][1])
            }
            index++
            if (text.size > index) {
                Global.gui.message(text[index])
            }
        } else {
            Global.gui.message("Error in scene: page " + index + " of " + text.size + " requested.")
        }
    }

    fun parse(player: Character, npc: Character? = null, npc2: Character? = null) {
        val parsed: ArrayList<String> = ArrayList()
        for (rawtext in text) {
            var raw = rawtext
            raw = raw.replace("\\[PLAYER]".toRegex(), player.name)
            raw = raw.replace("\\[Player]".toRegex(), player.name)
            if (npc != null) {
                raw = raw.replace("\\[NPC]".toRegex(), npc.name)
                raw = raw.replace("\\[OPPONENT]".toRegex(), npc.name)
                raw = raw.replace("\\[heshe]".toRegex(), npc.pronounSubject(false))
                raw = raw.replace("\\[HeShe]".toRegex(), npc.pronounSubject(true))
                raw = raw.replace("\\[hisher]".toRegex(), npc.pronounSubject(false))
                raw = raw.replace("\\[HisHer]".toRegex(), npc.pronounSubject(true))
                raw = raw.replace("\\[himher]".toRegex(), npc.pronounTarget(false))
                raw = raw.replace("\\[HimHer]".toRegex(), npc.pronounTarget(true))
            }
            if (npc2 != null) {
                raw = raw.replace("\\[NPC2]".toRegex(), npc2.name)
                raw = raw.replace("\\[heshe2]".toRegex(), npc2.pronounSubject(false))
                raw = raw.replace("\\[HeShe2]".toRegex(), npc2.pronounSubject(true))
                raw = raw.replace("\\[hisher2]".toRegex(), npc2.pronounSubject(false))
                raw = raw.replace("\\[HisHer2]".toRegex(), npc2.pronounSubject(true))
                raw = raw.replace("\\[himher2]".toRegex(), npc2.pronounTarget(false))
                raw = raw.replace("\\[HimHer2]".toRegex(), npc2.pronounTarget(true))
            }
            raw = raw.replace(" \"".toRegex(), " <i>\\\"")
            raw = raw.replace("\" ".toRegex(), "\\\"</i> ")
            raw = raw.replace("<p>\"".toRegex(), "<p><i>\\\"")
            raw = raw.replace("\"<p>".toRegex(), "\\\"</i><p>")
            raw = raw.replace("\\n".toRegex(), "<p>")
            raw = raw.replace("“".toRegex(), "<i>\\\"")
            raw = raw.replace("”".toRegex(), "\\\"</i>")
            raw = raw.replace("…".toRegex(), "...")
            parsed.add(raw)
        }
        text = parsed
    }
}

package scenes

class SeleneScenes : SceneTable() {
    override fun getVictoryScene(flag: SceneFlag): Scene {
        return Scene()
    }

    override fun getDefeatScene(flag: SceneFlag): Scene {
        return Scene()
    }

    override fun getDrawScene(flag: SceneFlag): Scene {
        return Scene()
    }

    override fun getInterventionScene(flag: SceneFlag): Scene {
        return Scene()
    }

    override fun getEventScene(flag: SceneFlag): Scene {
        return Scene()
    }

    override fun getDayScene(flag: SceneFlag): Scene {
        return Scene()
    }
}

package scenes

import characters.Character
import characters.ID
import global.Global
import global.Roster
import utilities.EnumMap
import java.util.EnumMap

object SceneManager {
    private var scenedb: EnumMap<ID, SceneTable> = EnumMap()
    init {
        scenedb[ID.CASSIE] = CassieScenes()
        scenedb[ID.MARA] = MaraScenes()
        scenedb[ID.ANGEL] = AngelScenes()
        scenedb[ID.JEWEL] = JewelScenes()
        scenedb[ID.YUI] = YuiScenes()
        scenedb[ID.KAT] = KatScenes()
        scenedb[ID.REYKA] = ReykaScenes()
        scenedb[ID.EVE] = EveScenes()
        scenedb[ID.VALERIE] = ValerieScenes()
        scenedb[ID.SAMANTHA] = SamanthaScenes()
        scenedb[ID.SOFIA] = SofiaScenes()
        scenedb[ID.SELENE] = SeleneScenes()
        scenedb[ID.PLAYER] = MiscScenes()
        scenedb[ID.MAYA] = MayaScenes()
        scenedb[ID.AESOP] = AesopScenes()
    }

    fun play(scene: SceneFlag, npc: Character? = null) {
        val playing = scenedb[scene.star]?.getScene(scene)
        if (playing != null) {
            playing.parse(Roster[ID.PLAYER], npc)
            playing.play(0)
        } else {
            Global.gui.message("Error: scene table not found for ID: " + scene.star)
        }
        Global.watched(scene)
    }

    fun getTotalCount(star: ID, type: SceneType): Int {
        return SceneFlag.entries.count { it.star == star && it.type == type }
    }
}


package scenes

import characters.Character
import characters.Dummy
import characters.Emotion
import characters.ID
import global.Flag
import global.Global
import global.Roster
import global.SaveManager

class YuiEvent(private val player: Character) : Event {
    private val scene = 1

    override fun respond(response: String) {
        if (response.startsWith("Take her virginity")) {
            play("VirginitySex")
        } else if (response.startsWith("Let her service you")) {
            play("VirginityHandjob")
        } else if (response.startsWith("Focus on her")) {
            play("VirginityOral")
        }
    }

    override fun play(response: String): Boolean {
        val sprite = Dummy("Yui", 1, false)
        sprite.blush = 2
        sprite.mood = Emotion.confident
        Global.gui.clearCommand()
        if (response.startsWith("VirginityUndressing")) {
            Global.gui.loadPortrait(player, sprite)
            SceneManager.play(SceneFlag.YuiFirstUndress)
            Global.gui.choose("Let her service you")
            Global.gui.choose("Focus on her")
            return true
        } else if (response.startsWith("VirginityHandjob")) {
            sprite.blush = 3
            sprite.mood = Emotion.dominant
            Global.gui.loadPortrait(player, sprite)
            SceneManager.play(SceneFlag.YuiFirstService)
            Global.gui.choose("Take her virginity")
            return true
        } else if (response.startsWith("VirginityOral")) {
            sprite.blush = 3
            sprite.mood = Emotion.desperate
            Global.gui.loadPortrait(player, sprite)
            SceneManager.play(SceneFlag.YuiFirstLick)
            Global.gui.choose("Take her virginity")
            return true
        } else if (response.startsWith("VirginitySex")) {
            sprite.blush = 3
            Global.gui.loadPortrait(player, sprite)
            SceneManager.play(SceneFlag.YuiFirstSex)

            if (Global.checkFlag(Flag.autosave)) {
                SaveManager.save(true)
            }
            Global.flag(Flag.Yui)
            Roster.scaleOpponent(ID.YUI)
            Global.gui.message("<b>You gained affection with Yui.</b>")
            Global.gui.endMatch()
            Roster.gainAffection(ID.PLAYER, ID.YUI, 3)
            return true
        }
        return false
    }

    override fun morning(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun mandatory(): String {
        // TODO Auto-generated method stub
        return ""
    }

    override fun addAvailable(available: HashMap<String, Int>) {
        // TODO Auto-generated method stub
    }
}

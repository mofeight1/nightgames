package scenes

interface Event {
    fun respond(response: String)
    fun play(response: String): Boolean
    fun morning(): String
    fun mandatory(): String
    fun addAvailable(available: HashMap<String, Int>)
}

package stance

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import kotlin.math.max

class Standing(top: Character, bottom: Character) : Position(top, bottom, Stance.standing) {
    override fun describe(): String {
        return if (top.human()) {
            "You are holding " + bottom.name + " in the air while buried deep in her pussy"
        } else {
            top.name + " is clinging to your shoulders and gripping your waist with her thighs while she uses the leverage to ride you."
        }
    }

    override fun mobile(c: Character): Boolean {
        return false
    }

    override fun kiss(c: Character): Boolean {
        return true
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return false
    }

    override fun reachBottom(c: Character): Boolean {
        return false
    }

    override fun prone(c: Character): Boolean {
        return false
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return true
    }

    override fun insert(c: Character): Position {
        return Neutral(top, bottom)
    }

    override fun decay() {
        time++
    }

    override fun checkOngoing(c: Combat) {
        if (top.human()) {
            c.write("Maintaining this position is a bit tiring.")
        }
        top.weaken(2.0, c)
        if (top.stamina.current < 10) {
            if (top.human()) {
                c.write("Your legs give out and you fall on the floor. " + bottom.name + " lands heavily on your lap.")
                c.stance = Mount(bottom, top)
            } else {
                c.write(top.name + " loses her balance and falls, pulling you down on top of her.")
                c.stance = Mount(bottom, top)
            }
        } else {
            val m = 6.0 + (2 * pace)
            var r = max(1.0, 3.0 - pace)
            if (top.has(Trait.experienced)) {
                r *= 2
            }
            if (pace > 1) {
                if (top.human()) {
                    c.write(top, "Your intense fucking continues to drive you both closer to ecstasy.")
                } else {
                    c.write(top, "Her rapid bouncing on your cock gives you intense pleasure.")
                }
            } else if (pace == 1) {
                if (top.human()) {
                    c.write(top, "Your steady thrusting pleasures you both.")
                } else {
                    c.write(top, "Her steady lovemaking continues to erode your resistance.")
                }
            } else {
                if (top.human()) {
                    c.write(top, "You slowly, but steadily grind against her.")
                } else {
                    c.write(top, "She continues to stimulate your penis with slow, deliberate movements.")
                }
            }
            bottom.pleasure(top.bonusProficiency(Anatomy.genitals, m), Anatomy.genitals, combat = c)
            top.pleasure(bottom.bonusProficiency(Anatomy.genitals, m / r), Anatomy.genitals, combat = c)
        }
    }
}

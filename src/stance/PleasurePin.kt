package stance

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat
import combat.Result


class PleasurePin(top: Character, bottom: Character) : Position(top, bottom, Stance.pin) {
    init {
        strength = top[Attribute.Ki] * 5
        pleasure = true
    }

    override fun describe(): String {
        return if (top.human()) {
            "You're sitting on " + bottom.name + ", pressing your leg into her crotch."
        } else {
            top.name + " is pinning you down, grinding her knee into your groin."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return c === top
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return c === top
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return if (top.hasDick && bottom.hasPussy) {
            Missionary(top, bottom)
        } else if (top.hasPussy && bottom.hasDick) {
            Cowgirl(top, bottom)
        } else {
            this
        }
    }

    override fun checkOngoing(c: Combat) {
        if (bottom.human()) {
            c.write(top, top.name + "'s leg continues grinding against your genitals.")
        } else if (top.human()) {
            c.write(top, "You can feel " + bottom.name + "'s arousal on your leg.")
        }
        bottom.pleasure(5.0, Anatomy.genitals, Result.foreplay, c)
    }
}

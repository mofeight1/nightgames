package stance

import characters.Character


class Mount(top: Character, bottom: Character) : Position(top, bottom, Stance.mount) {
    init {
        strength = 30
    }

    override fun describe(): String {
        return if (top.human()) {
            "You're on top of " + bottom.name + "."
        } else {
            top.name + " is straddling you, with her enticing breasts right in front of you."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return true
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return true
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return if (top.hasDick && bottom.hasPussy) {
            Missionary(top, bottom)
        } else if (top.hasPussy && bottom.hasDick) {
            Cowgirl(top, bottom)
        } else {
            this
        }
    }
}

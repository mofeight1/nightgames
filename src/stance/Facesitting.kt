package stance

import characters.Anatomy
import characters.Attribute
import characters.Character
import characters.Trait
import combat.Combat
import global.Global
import status.Enthralled
import status.Horny
import status.Shamed
import kotlin.math.max


class Facesitting(top: Character, bottom: Character) : Position(top, bottom, Stance.facesitting) {
    init {
        strength = 50
    }

    override fun describe(): String {
        return if (top.human()) {
            "You are sitting on " + bottom.name + "'s face, her mouth forced to pleasure your genitals while you have free access to her body."
        } else {
            top.name + " is sitting on your face, forcing you to service her with your mouth while she enjoys free access to the rest of your body."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return false
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return c === top
    }

    override fun reachBottom(c: Character): Boolean {
        return true
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return c === top
    }

    override fun oral(c: Character): Boolean {
        return true
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return ReverseMount(top, bottom)
    }

    override fun checkOngoing(c: Combat) {
        val m = 2.0 + (2 * pace)
        var r = max(1.0, 2.0 - pace) // top recoil damage is harder to mitigate
        if (top.has(Trait.experienced)) {
            r *= 2
        }
        if (top.human()) {
            if (pace > 1) {
                c.write(top, "Your intense facefucking continues to drive you both closer to ecstasy.")
            } else if (pace == 1) {
                c.write(
                    top,
                    "Your shaft rests inside her mouth, and her submissive licking and kissing turns you both on."
                )
            } else {
                c.write(top, "You kneel with your balls in her mouth, while she submissively sucks on them.")
            }
        } else if (!top.human() && top.hasDick) {
            if (pace > 1) {
                if (top.arousal.percent() >= 75) {
                    c.write(
                        top,
                        "Her furious facefucking of your mouth continues to arouse both of you as she leaks precum from her tip."
                    )
                } else {
                    c.write(top, "Her furious facefucking of your mouth continues to arouse both of you.")
                }
            } else if (pace == 1) {
                c.write(
                    top,
                    "Her steady alternating grinding of her pussy and cock into your mouth continues to erode your resistance."
                )
            } else {
                c.write(
                    top,
                    "She slowly but powerfully grinds her pussy and balls into your mouth, dominating you intensely."
                )
            }
        } else {
            if (pace > 1) {
                c.write(top, "Her furious back-and-forth grinding on your face continues to arouse both of you.")
            } else if (pace == 1) {
                c.write(top, "Her steady grinding into your face continues to erode your resistance.")
            } else {
                c.write(top, "She slowly but powerfully grinds her pussy into your mouth, dominating you intensely.")
            }
        }
        if (top.has(Trait.succubus) || top.has(Trait.lacedjuices)) {
            if (Global.random(4) == 0) {
                if (top[Attribute.Dark] >= 6 && Global.random(3) == 0) {
                    bottom.tempt(m + 4, combat = c)
                    bottom.add(Enthralled(bottom, top), c)
                    if (top.human()) {
                        c.write(top, "Your incubus penis enthralls " + bottom.name + ".")
                    } else {
                        c.write(
                            top,
                            top.name + "'s juices are simply too much for you, and you feel your mind being enthralled by the pussy above you."
                        )
                    }
                } else {
                    bottom.tempt(m + 4, combat = c)
                    if (top.human()) {
                        c.write(top, "Your incubus penis is extra tempting to " + bottom.name)
                    } else {
                        c.write(
                            top,
                            top.name + "'s aphrodisiac-laced juices flow into your mouth and you eagerly lap them up."
                        )
                    }
                }
            } else { // didn't get succubus crit
                bottom.tempt(m, combat = c)
            }
        } else if (top.has(Trait.pheromones)) {
            if (Global.random(6) == 0) {
                bottom.tempt(m + 1, combat = c)
                bottom.add(Horny(bottom, 4.0, 3), c)
                if (bottom.human()) {
                    c.write(
                        top,
                        "Smelling " + top.name + "'s pheromones close up awakens a primal need in you, though you'll have trouble fulfilling it from down here."
                    )
                }
            }
        } else {
            bottom.tempt(m, combat = c)
        }
        bottom.add(Shamed(bottom), c)
        bottom.spendMojo(3 + pace * 2)
        top.buildMojo(3 + pace * 6)
        top.pleasure(bottom.bonusProficiency(Anatomy.mouth, m / r), Anatomy.genitals, combat = c)
    }
}

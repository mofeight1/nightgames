package stance

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import kotlin.math.max


class Missionary(top: Character, bottom: Character) : Position(top, bottom, Stance.missionary) {
    init {
        strength = 50
    }

    override fun describe(): String {
        return if (top.human()) {
            "You are penetrating " + bottom.name + " in traditional Missionary position."
        } else {
            top.name
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return true
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return true
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return true
    }

    override fun insert(c: Character): Position {
        return Mount(top, bottom)
    }

    override fun checkOngoing(c: Combat) {
        var m = 6.0 + (2 * pace)
        var r = max(1, 3 - pace).toDouble()
        if (top.has(Trait.experienced)) {
            r *= 2
        }
        if (pace > 1) {
            if (top.human()) {
                c.write(top, "Your intense fucking continues to drive you both closer to ecstasy.")
            } else {
                c.write(top, "Her rapid bouncing on your cock gives you intense pleasure.")
            }
        } else if (pace == 1) {
            if (top.human()) {
                c.write(top, "Your steady thrusting pleasures you both.")
            } else {
                c.write(top, "Her steady lovemaking continues to erode your resistance.")
            }
        } else {
            if (top.human()) {
                c.write(top, "You slowly, but steadily grind against her.")
            } else {
                c.write(top, "She continues to stimulate your penis with slow, deliberate movements.")
            }
        }
        bottom.pleasure(top.bonusProficiency(Anatomy.genitals, m),
            Anatomy.genitals, combat = c)
        m /= r
        m = bottom.bonusProficiency(Anatomy.genitals, m)
        m += bottom.bonusRecoilPleasure(m)
        top.pleasure(m,
            Anatomy.genitals, combat = c)
    }
}
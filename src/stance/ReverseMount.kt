package stance

import characters.Character


class ReverseMount(top: Character, bottom: Character) : Position(top, bottom, Stance.reversemount) {
    init {
        strength = 30
    }

    override fun describe(): String {
        return if (top.human()) {
            "You are straddling " + bottom.name + ", with your back to her."
        } else {
            top.name + " is sitting on your chest, facing your groin."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return false
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return c === bottom
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return c === top
    }

    override fun oral(c: Character): Boolean {
        return c === top
    }

    override fun behind(c: Character): Boolean {
        return c === bottom
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return if (top.hasDick && bottom.hasPussy) {
            Missionary(top, bottom)
        } else if (top.hasPussy && bottom.hasDick) {
            ReverseCowgirl(top, bottom)
        } else {
            this
        }
    }
}

package stance

import characters.Character

class StandingOver(top: Character, bottom: Character) : Position(top, bottom, Stance.standingover) {
    init {
        contact = false
    }

    override fun describe(): String {
        return if (top.human()) {
            "You are standing over " + bottom.name + ", who is helpless on the ground."
        } else {
            "You are flat on your back, while " + top.name + " stands over you."
        }
    }

    override fun mobile(c: Character): Boolean {
        return true
    }

    override fun kiss(c: Character): Boolean {
        return c === top
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return c === top
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return true
    }

    override fun oral(c: Character): Boolean {
        return c === top
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return this
    }
}

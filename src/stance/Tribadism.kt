package stance

import characters.Anatomy
import characters.Character
import characters.Trait
import combat.Combat
import kotlin.math.max

class Tribadism(top: Character, bottom: Character) : Position(top, bottom, Stance.trib) {
    init {
        strength = 40
    }

    override fun describe(): String {
        return "Trib position: this should never display"
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return true
    }

    override fun dom(c: Character): Boolean { // should be return false
        return c === top
    }

    override fun sub(c: Character): Boolean { // should be return false
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return true
    }

    override fun reachBottom(c: Character): Boolean {
        return false
    }

    override fun prone(c: Character): Boolean {
        return true
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return Tribadism(bottom, top)
    }

    override fun checkOngoing(c: Combat) {
        var m = 6.0 + (2 * pace)
        var r = max(1, 3 - pace).toDouble()
        if (top.has(Trait.experienced)) {
            r *= 2
        }
        if (pace > 1) {
            if (top.human()) {
                c.write(top, "Your intense fucking continues to drive you both closer to ecstasy.")
            } else {
                c.write(top, "Her rapid scissoring gives you intense pleasure.")
            }
        } else if (pace == 1) {
            if (top.human()) {
                c.write(top, "Your steady thrusting pleasures you both.")
            } else {
                c.write(top, "Her steady lovemaking continues to erode your resistance.")
            }
        } else {
            if (top.human()) {
                c.write(top, "You slowly, but steadily grind against her.")
            } else {
                c.write(top, "She continues to stimulate your clit with slow, deliberate movements.")
            }
        }
        bottom.pleasure(top.bonusProficiency(Anatomy.genitals, m),
            Anatomy.genitals, combat = c)
        m /= r
        m = bottom.bonusProficiency(Anatomy.genitals, m)
        m += bottom.bonusRecoilPleasure(m)
        top.pleasure(bottom.bonusProficiency(Anatomy.genitals, m / r),
            Anatomy.genitals, combat = c)
    }
}

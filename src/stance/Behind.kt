package stance

import characters.Character


class Behind(top: Character, bottom: Character) : Position(top, bottom, Stance.behind) {
    init {
        strength = 30
    }

    override fun describe(): String {
        return if (top.human()) {
            "You are holding " + bottom.name + " from behind."
        } else {
            top.name + " is holding you from behind."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return c === top
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return c === top
    }

    override fun reachBottom(c: Character): Boolean {
        return true
    }

    override fun prone(c: Character): Boolean {
        return false
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return c === top
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return Doggy(top, bottom)
    }
}

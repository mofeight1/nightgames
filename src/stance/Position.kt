package stance

import characters.Anatomy
import characters.Character
import combat.Combat
import kotlin.math.roundToInt

abstract class Position(var top: Character, var bottom: Character, var en: Stance) : Cloneable {

    var time: Int = 0
    var pace: Int = 0
    var strength: Int = 0
    var contact: Boolean = true
        protected set
    protected var submission: Boolean = false
    protected var pleasure: Boolean = false

    open fun decay() {
        time++
    }

    open fun checkOngoing(c: Combat) {
        return
    }

    fun anal(): Boolean {
        return this.en == Stance.anal || this.en == Stance.analm
    }

    abstract fun describe(): String
    abstract fun mobile(c: Character): Boolean
    abstract fun kiss(c: Character): Boolean
    abstract fun dom(c: Character): Boolean
    abstract fun sub(c: Character): Boolean
    abstract fun reachTop(c: Character): Boolean
    abstract fun reachBottom(c: Character): Boolean
    abstract fun prone(c: Character): Boolean
    abstract fun feet(c: Character): Boolean
    abstract fun oral(c: Character): Boolean
    abstract fun behind(c: Character): Boolean
    abstract fun penetration(c: Character): Boolean
    abstract fun insert(c: Character): Position
    open fun flying(c: Character): Boolean = false

    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Position {
        return super.clone() as Position
    }

    fun struggle(c: Combat) {
        if (submission) {
            if (bottom.human()) {
                c.write(top, "Your struggles are met with an even tighter grip on your balls.")
            } else if (top.human()) {
                c.write(top, "You apply more pressure to " + bottom.name + "'s genitals to stop her from struggling.")
            }
            bottom.pain(5.0, Anatomy.genitals, c)
        }
        if (pleasure) {
            if (bottom.human()) {
                c.write(top, "The friction from struggling stimulates your penis even more.")
            } else if (top.human()) {
                c.write(top, bottom.name + " grinds against your leg, trying to free herself.")
            }
            bottom.pleasure(5.0, Anatomy.genitals, combat = c)
        }
    }

    fun escapeDC(dom: Character, sub: Character): Int {
        return (dom.bonusPin(strength.toDouble()) - ((5 * time) + sub.escape())).roundToInt()
    }
}

package stance

import characters.Anatomy
import characters.Character
import characters.ID
import characters.Trait
import combat.Combat
import global.Flag
import global.Global
import kotlin.math.max

class Flying(succ: Character, target: Character) : Position(succ, target, Stance.flying) {
    init {
        strength = 65
    }

    override fun describe(): String {
        return "You are flying some twenty feet up in the air, joined to your partner by your hips."
    }

    override fun mobile(c: Character): Boolean {
        return top == c
    }

    override fun kiss(c: Character): Boolean {
        return true
    }

    override fun dom(c: Character): Boolean {
        return top == c
    }

    override fun sub(c: Character): Boolean {
        return top != c
    }

    override fun reachTop(c: Character): Boolean {
        return true
    }

    override fun reachBottom(c: Character): Boolean {
        return top == c
    }

    override fun prone(c: Character): Boolean {
        return top != c
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return true
    }


    override fun flying(c: Character): Boolean {
        return true
    }

    override fun decay() {
        time++
    }

    override fun checkOngoing(c: Combat) {
        if (top.human()) {
            c.write("The strain of flying while carrying another person saps a bit of your stamina.")
        }
        top.weaken(3.0, c)
        if (top.stamina.current < 5) {
            if (top.human()) {
                c.write("You're too tired to stay in the air. You plummet to the ground and " + bottom.name + " drops on you heavily, knocking the wind out of you.")
            } else {
                c.write(top.name + " falls to the ground and so do you. Fortunately, her body cushions your fall, but you're not sure she appreciates that as much as you do.")
            }
            top.pain(5.0, Anatomy.chest, c)
            c.stance = Mount(bottom, top)
        } else {
            var m = 6.0 + (2 * pace)
            var r = max(1, 3 - pace).toDouble()
            if (top.has(Trait.experienced)) {
                r *= 2
            }
            if (pace > 1) {
                if (top.human()) {
                    c.write(top, "Your intense fucking continues to drive you both closer to ecstasy.")
                } else {
                    c.write(top, "Her rapid bouncing on your cock gives you intense pleasure.")
                }
            } else if (pace == 1) {
                if (top.human()) {
                    c.write(top, "Your steady thrusting pleasures you both.")
                } else {
                    c.write(top, "Her steady lovemaking continues to erode your resistance.")
                }
            } else {
                if (top.human()) {
                    c.write(top, "You slowly, but steadily grind against her.")
                } else {
                    c.write(top, "She continues to stimulate your penis with slow, deliberate movements.")
                }
            }
            bottom.pleasure(
                top.bonusProficiency(Anatomy.genitals, m),
                Anatomy.genitals,
                combat = c)
            m /= r
            m = bottom.bonusProficiency(Anatomy.genitals, m)
            m += bottom.bonusRecoilPleasure(m)
            top.pleasure(
                m,
                Anatomy.genitals,
                combat = c)
            if (!Global.checkFlag(Flag.exactimages) || top.id == ID.SELENE) {
                c.offerImage("Selene Blowjob.jpg", "Art by AimlessArt")
            }
        }
    }

    override fun insert(c: Character): Position {
        c.weaken(10.0)
        return StandingOver(top, bottom)
    }
}

package stance

import characters.Character


class Pin(top: Character, bottom: Character) : Position(top, bottom, Stance.pin) {
    init {
        strength = 60
    }

    override fun describe(): String {
        return if (top.human()) {
            "You're sitting on " + bottom.name + ", holding her arms in place."
        } else {
            top.name + " is pinning you down, leaving you helpless."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return c === top
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return c === top
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return if (top.hasDick && bottom.hasPussy) {
            Missionary(top, bottom)
        } else if (top.hasPussy && bottom.hasDick) {
            Cowgirl(top, bottom)
        } else {
            this
        }
    }
}

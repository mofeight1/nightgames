package stance

enum class Stance {
    behind,
    cowgirl,
    doggy,
    missionary,
    mount,
    neutral,
    pin,
    reversepin,
    reversecowgirl,
    reversemount,
    sixnine,
    standing,
    standingover,
    anal,
    flying,
    trib,
    analm,
    facesitting,
}

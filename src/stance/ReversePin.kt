package stance

import characters.Character

class ReversePin(top: Character, bottom: Character) : Position(top, bottom, Stance.reversepin) {
    init {
        strength = 60
    }

    override fun describe(): String {
        return if (top.human()) {
            "You're sitting on " + bottom.name + "'s upper body, holding " + bottom.possessive(false) + " legs in the air."
        } else {
            top.name + " is sitting on your chest, holding up your lower body in an undignified pose."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return false
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return false
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return c === top
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return if (top.hasDick && bottom.hasPussy) {
            Missionary(top, bottom)
        } else if (top.hasPussy && bottom.hasDick) {
            ReverseCowgirl(top, bottom)
        } else {
            this
        }
    }
}

package stance

import characters.Anatomy
import characters.Attribute
import characters.Character
import combat.Combat

class SubmissionPin(top: Character, bottom: Character) : Position(top, bottom, Stance.reversepin) {
    init {
        strength = top[Attribute.Ki] * 5
        submission = true
    }

    override fun describe(): String {
        return if (top.human()) {
            if (bottom.hasBalls) {
                "You're sitting on " + bottom.name + "'s upper body, putting pressure on " + bottom.possessive(false) + " testicles."
            } else {
                "You're sitting on " + bottom.name + "'s upper body, putting pressure on " + bottom.possessive(false) + " sensitive genitals."
            }
        } else {
            top.name + " is sitting on your chest, holding you in place by your family jewels."
        }
    }

    override fun mobile(c: Character): Boolean {
        return c === top
    }

    override fun kiss(c: Character): Boolean {
        return false
    }

    override fun dom(c: Character): Boolean {
        return c === top
    }

    override fun sub(c: Character): Boolean {
        return c === bottom
    }

    override fun reachTop(c: Character): Boolean {
        return false
    }

    override fun reachBottom(c: Character): Boolean {
        return c === top
    }

    override fun prone(c: Character): Boolean {
        return c === bottom
    }

    override fun feet(c: Character): Boolean {
        return false
    }

    override fun oral(c: Character): Boolean {
        return c === top
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return if (top.hasDick && bottom.hasPussy) {
            Missionary(top, bottom)
        } else if (top.hasPussy && bottom.hasDick) {
            ReverseCowgirl(top, bottom)
        } else {
            this
        }
    }

    override fun checkOngoing(c: Combat) {
        if (bottom.human()) {
            c.write(top, top.name + " continues to put painful pressure on your genitals.")
        } else if (top.human()) {
            c.write(top, bottom.name + " seems gradually worn down by your submission hold.")
        }
        bottom.pain(5.0, Anatomy.genitals, c)
    }
}

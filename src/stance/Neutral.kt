package stance

import characters.Character


class Neutral(top: Character, bottom: Character) : Position(top, bottom, Stance.neutral) {
    init {
        contact = false
    }

    override fun describe(): String {
        return if (top.human()) {
            "You and " + bottom.name + " circle each other cautiously."
        } else {
            "You and " + top.name + " circle each other cautiously."
        }
    }

    override fun mobile(c: Character): Boolean {
        return true
    }

    override fun kiss(c: Character): Boolean {
        return true
    }

    override fun dom(c: Character): Boolean {
        return false
    }

    override fun sub(c: Character): Boolean {
        return false
    }

    override fun reachTop(c: Character): Boolean {
        return true
    }

    override fun reachBottom(c: Character): Boolean {
        return true
    }

    override fun prone(c: Character): Boolean {
        return false
    }

    override fun feet(c: Character): Boolean {
        return true
    }

    override fun oral(c: Character): Boolean {
        return false
    }

    override fun behind(c: Character): Boolean {
        return false
    }

    override fun penetration(c: Character): Boolean {
        return false
    }

    override fun insert(c: Character): Position {
        return this
    }
}
